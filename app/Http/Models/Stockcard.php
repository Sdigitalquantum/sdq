<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Stockcard extends Model
{
    protected $fillable = [
	    'product',
	    'year',
	    'month',
	    'qty',
	    'price'
  	];

  	public function fkProduct(){
	    return $this->belongsTo('\App\Http\Models\Product', 'product', 'id');
	}
}
