<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Supplieraccount extends Model
{
    protected $fillable = [
	    'supplier',
	    'currency',
	    'bank',
	    'account_no',
	    'account_swift',
	    'account_name',
	    'alias',
	    'created_user',
	    'updated_user'
  	];

  	public function fkSupplier(){
	    return $this->belongsTo('\App\Http\Models\Supplier', 'supplier', 'id');
	}

	public function fkCurrency(){
	    return $this->belongsTo('\App\Http\Models\Currency', 'currency', 'id');
	}

	public function fkBank(){
	    return $this->belongsTo('\App\Http\Models\Bank', 'bank', 'id');
	}
}
