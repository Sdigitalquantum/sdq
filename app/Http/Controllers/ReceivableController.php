<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Exports\ReceivablesExport;
use App\Http\Models\Receivable;
use App\Http\Models\Receivablecard;
use App\Http\Models\Quotationsplit;
use App\Http\Models\Customer;
use App\Http\Models\Customerdetail;
use App\Http\Models\Employeeset;

class ReceivableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $receivables = Receivable::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/receivable.index', compact('receivables','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $receivables = Receivable::orderBy('code_customer','asc')->get();

        return view('vendor/adminlte/receivable.list', compact('receivables','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function payment(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $receivable = Receivable::find($id);
        $amount = $receivable->total-$receivable->payment-$receivable->adjustment;

        $request->validate([
            'payment'       => 'numeric|min:0',
            'adjustment'    => 'numeric|min:0'
        ]);

        if ($request->get('payment') > $amount) {
            return redirect()->back()->withErrors('Payment greater than Total');
        } elseif ($request->get('adjustment') > ($receivable->total-$receivable->payment)) {
            return redirect()->back()->withErrors('Adjustment greater than Total');
        } else {
            $receivable->payment           = $receivable->payment+$request->get('payment');
            $receivable->adjustment        = $receivable->adjustment+$request->get('adjustment');
            if ($request->get('payment') == $amount) {
                $receivable->status = 1;
            }
            $receivable->updated_user      = $session_id;
            $receivable->save();

            $customer = Customer::where('code', $receivable->code_customer)->first();
            $customerdetail = Customerdetail::where([['customer', $customer->id], ['status', 1]])->first();
            $quotationsplit = Quotationsplit::where('no_split', $receivable->no_sp)->first();

            $receivablecard = Receivablecard::where([['customerdetail', $customerdetail->id], ['year', date('Y', strtotime($quotationsplit->fkQuotation->date_sp))], ['month', date('m', strtotime($quotationsplit->fkQuotation->date_sp))]])->first();
            $receivablecard->kredit = $receivablecard->kredit+($receivable->payment);
            $receivablecard->save();

            return redirect('/receivable')->with('success', 'Receivable Transaction has been paid');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function card(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $customerdetails = Customerdetail::where('status', 1)->get();

        return view('vendor/adminlte/receivable.card', compact('customerdetails','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $customerdetails = Customerdetail::where('status', 1)->get();

        $request->validate([
            'year'  => 'numeric|min:0|not_in:0'
        ]);

        $date_begin = $request->get('year').'-'.$request->get('month').'-01';
        if ($request->get('month') == 2) {
            $date_end = $request->get('year').'-'.$request->get('month').'-28';
        } elseif ($request->get('month') == 1 || $request->get('month') == 3 || $request->get('month') == 5 || $request->get('month') == 7 || $request->get('month') == 8 || $request->get('month') == 10 || $request->get('month') == 12) {
            $date_end = $request->get('year').'-'.$request->get('month').'-31';
        } elseif ($request->get('month') == 4 || $request->get('month') == 6 || $request->get('month') == 9 || $request->get('month') == 11) {
            $date_end = $request->get('year').'-'.$request->get('month').'-30';
        }
        
        $customer = Customerdetail::find($request->get('customerdetail')); 
        $receivables = Receivable::where([['code_customer', $customer->fkcustomer->code], ['date_sp','>=',$date_begin], ['date_sp','<=',$date_end]])->orderBy('date_sp','asc')->get();
        $receivableold = Receivablecard::where([['customerdetail', $request->get('customerdetail')], ['year', $request->get('year')], ['month', $request->get('month')-1]])->first();
        $receivablecard = Receivablecard::where([['customerdetail', $request->get('customerdetail')], ['year', $request->get('year')], ['month', $request->get('month')]])->first();
        $year = $request->get('year');
        $month = $request->get('month');

        return view('vendor/adminlte/receivable.filter', compact('customerdetails','customer','receivables','receivableold','receivablecard','year','month','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export() 
    {
        return Excel::download(new ReceivablesExport, 'Receivable.xlsx');
    }
}
