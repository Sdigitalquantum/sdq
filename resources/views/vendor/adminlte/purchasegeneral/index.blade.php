@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
                <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Purchase General &nbsp;<a href="{{ route('purchasegeneral.create')}}" class="btn btn-xs btn-success" style="text-decoration: none;">New</a></h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="15%">No. PR</th>
						          	<th width="10%">Date</th>
						          	<th width="15%">Note</th>
						          	<th width="45%">Detail Request</th>
						          	<th width="5%">Approval</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
					    		@php ($array = [])
						    	@foreach($employeewarehouses as $employeewarehouse)
						    		@php ($array[] = $employeewarehouse->warehouse)
						    	@endforeach

						    	@php ($no = 0)
						    	@foreach($purchasegenerals as $purchasegeneral)
						    		@if(in_array($purchasegeneral->warehouse, $array) || $purchasegeneral->warehouse == 0)
							    		@php ($no++)
							        	<tr>
								            <td>{{$no}}</td>
								            <td>{{$purchasegeneral->no_prg}}</td>
								            <td>{{$purchasegeneral->date_order}}</td>
								            <td><pre>{{$purchasegeneral->notice}}</pre></td>
								            <td>
								            	<ul>
									            	@foreach($purchasedetailprgs as $purchasedetailprg)
									            		@if($purchasedetailprg->purchasegeneral == $purchasegeneral->id)
									            			<li>
									            				[{{ $purchasedetailprg->fkProduct->name }}]
									            				@if($purchasedetailprg->supplierdetail == 0)
									            					All Supplier
									            				@else
									            					{{ $purchasedetailprg->fkSupplierdetail->name }}
								            					@endif

									            				<br>[Qty : {{ number_format($purchasedetailprg->qty_request, 0, ',' , '.') }} {{ $purchasedetailprg->fkProduct->fkUnit->name }}]
									            				@if($purchasedetailprg->warehouse == 0)
									            					All Warehouse
									            				@else
									            					{{ $purchasedetailprg->fkWarehouse->name }}
								            					@endif
									            			</li>
									            		@endif
									            	@endforeach
								            	</ul>
								            </td>
								            <td>
								            	@if($purchasegeneral->status == 0)
								            		<font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
								            	@else
									            	@if($purchasegeneral->approve == 1 and $purchasegeneral->status_approve == 0)
									            		@if($purchasegeneral->status_release == 1)
											            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal-set{{$purchasegeneral->id}}">Set None</button>

											            	<form action="{{ url('purchasegeneral/set', $purchasegeneral->id)}}" method="post">
											                  	@csrf
											                  	@method('DELETE')
											                  	<div class="modal fade bs-example-modal-set{{$purchasegeneral->id}}" tabindex="-1" role="dialog" aria-hidden="true">
								                                    <div class="modal-dialog modal-sm">
								                                        <div class="modal-content">
								                                            <div class="modal-header btn-danger">
								                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
								                                                </button>
								                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
								                                            </div>
								                                            <div class="modal-body">
								                                                Are you sure to set none approval this data?
								                                            </div>
								                                            <div class="modal-footer">
								                                                <button type="submit" class="btn btn-success">Yes</button>
								                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
								                                            </div>
								                                        </div>
								                                    </div>
								                                </div>
											                </form>
										                @else
									            			<font color="red">None <i class="glyphicon glyphicon-remove"></i></font>
									            		@endif
									                @elseif($purchasegeneral->approve == 0 and $purchasegeneral->status_approve == 1)
									                	<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal-set{{$purchasegeneral->id}}">Set Used</button>

										            	<form action="{{ url('purchasegeneral/set', $purchasegeneral->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal-set{{$purchasegeneral->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-success">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to set used approval this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
									                @else
									                	<font color="blue">Yes <i class="glyphicon glyphicon-ok"></i></font>
									                @endif
								                @endif
								            </td>

								            @if($purchasegeneral->status_approve == 1)
			            						<td>
			            							<font color="green">Approved <i class="glyphicon glyphicon-ok"></i></font>
			            						</td>
		            						@elseif($purchasegeneral->status_approve == 2)
				            					<td>
				            						<font color="red">Rejected <i class="glyphicon glyphicon-remove"></i></font>
			            						</td>
		            						@else
												@if($purchasegeneral->status == 1)
										            <td>
										            	@if($purchasegeneral->status_release == 1)
										            		<font color="green">Released <i class="glyphicon glyphicon-ok"></i></font> <br>
										            		<font color="blue">Waiting Approval</font>
										            	@else
										            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal-rl{{$purchasegeneral->id}}">Release</button> &nbsp;

											            	<a href="{{ route('purchasegeneral.edit',$purchasegeneral->id)}}" class="btn btn-xs btn-warning">Edit</a> &nbsp;
											            	
											            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$purchasegeneral->id}}">Delete</button>

											            	<form action="{{ url('purchasegeneral/release', $purchasegeneral->id) }}" method="post">
											                  	@csrf
											                  	@method('DELETE')
											                  	<div class="modal fade bs-example-modal-rl{{$purchasegeneral->id}}" tabindex="-1" role="dialog" aria-hidden="true">
								                                    <div class="modal-dialog modal-sm">
								                                        <div class="modal-content">
								                                            <div class="modal-header btn-success">
								                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
								                                                </button>
								                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Release</h4>
								                                            </div>
								                                            <div class="modal-body">
								                                                Are you sure to release this data?
								                                            </div>
								                                            <div class="modal-footer">
								                                                <button type="submit" class="btn btn-success">Yes</button>
								                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
								                                            </div>
								                                        </div>
								                                    </div>
								                                </div>
											                </form>

											            	<form action="{{ route('purchasegeneral.destroy', $purchasegeneral->id)}}" method="post">
											                  	@csrf
											                  	@method('DELETE')
											                  	<div class="modal fade bs-example-modal{{$purchasegeneral->id}}" tabindex="-1" role="dialog" aria-hidden="true">
								                                    <div class="modal-dialog modal-sm">
								                                        <div class="modal-content">
								                                            <div class="modal-header btn-danger">
								                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
								                                                </button>
								                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
								                                            </div>
								                                            <div class="modal-body">
								                                                Are you sure to delete this data?
								                                            </div>
								                                            <div class="modal-footer">
								                                                <button type="submit" class="btn btn-success">Yes</button>
								                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
								                                            </div>
								                                        </div>
								                                    </div>
								                                </div>
											                </form>
									                	@endif
										            </td>
									            @else
									            	<td>
									            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$purchasegeneral->id}}">Activated</button>

									            		<form action="{{ route('purchasegeneral.destroy', $purchasegeneral->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$purchasegeneral->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-success">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to active this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
									            	</td>
									            @endif
								            @endif
								        </tr>
							        @endif
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>No. PR</td>
						          	<td>Date</td>
						          	<td>Note</td>
						          	<td>Detail Request</td>
						          	<td class="noShow"></td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop