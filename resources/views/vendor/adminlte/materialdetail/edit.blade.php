@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="active"><a href="#tab_raw" data-toggle="tab">Raw Material</a></li>
            <li class="disabled"><a>Excess</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_raw">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Raw Material</h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<div class="row">
				  			<div class="col-md-3">
				  				 <form id="materialdetailFrm"> <!--method="post" action="{{ route('materialdetail.update', $materialdetail->id) }}"> -->
						      		@method('PATCH')
						      		@csrf
						      		<input type="hidden" class="form-control" name="material" value="{{ $materialdetail->material }}">
						      		<div class="form-group">
						              	<label for="product">Product</label>
						              	<select class="form-control select2" name="product" required>
						              		@foreach($products as $product)
											    @if ($materialdetail->product == $product->id)
												    <option value="{{ $product->id }}" selected>{{ $product->name }}</option>
												@else
												    <option value="{{ $product->id }}">{{ $product->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="unit">Unit</label>
						              	<select class="form-control select2" name="unit" required>
						              		@foreach($units as $unit)
											    @if ($materialdetail->unit == $unit->id)
												    <option value="{{ $unit->id }}" selected>{{ $unit->name }}</option>
												@else
												    <option value="{{ $unit->id }}">{{ $unit->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty">Qty</label>
						              	<input type="number" class="form-control" name="qty" value="{{ $materialdetail->qty }}" required>
						          	</div>
					          		<button id="updateBtn" value="{{ route('materialdetail.update', $materialdetail->id) }}" class="btn btn-sm btn-success">Update</button>
				          			<button id="cancelBtn" value="{{ url('/materialdetail/'.$materialdetail->material) }}"class="btn btn-sm btn-danger">Cancel</button>
						          	<!-- <button type="submit" class="btn btn-sm btn-success">Update</button>
					          		<a href="{{ url('/materialdetail/'.$materialdetail->material) }}" class="btn btn-sm btn-danger">Cancel</a> -->
						      	</form>
			  				</div>

			  				<div class="col-md-9">
			  					<br><table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="30%">Product</th>
								          	<th width="30%">Unit</th>
								          	<th width="20%">Qty</th>
								          	<th width="20%">Status</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($materialdetails as $materialdetail)
								    		<tr>
									            <td>{{$materialdetail->fkProduct->name}}</td>
									            <td>{{$materialdetail->fkUnit->name}}</td>
									            <td>{{$materialdetail->qty}}</td>
									            <td>
									            	@if($materialdetail->status == 1) <a href="{{ route('materialdetail.edit',$materialdetail->id)}}" class="btn btn-xs btn-warning">Edit</a>
									            	@else <font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
									            	@endif
									            </td>
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
			  				</div>
		  				</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('content_js')
	<script>
    	$(document).ready(sdq.material.prepare_editdetail());
    </script>
@stop