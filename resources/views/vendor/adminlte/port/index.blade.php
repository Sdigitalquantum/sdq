@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
                <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #6b6e70 !important; font-weight: bold; color: white;">
				  		<!-- <h4 class="box-title">Port &nbsp;<a href="#" onclick="sdq.core.load('{{ route('country.create') }}');return false;" class="btn btn-xs btn-success" style="text-decoration: none;">New</a></h4> -->
				  		<h4 class="box-title">PORT &nbsp;
				  			<button id="portNewBtn" class="btn btn-xs btn-primary">NEW</button>
				  		</h4>
			  		</div>
				  	
				  	<!-- <div class="box-body"> -->
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #86c232 !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="10%">Code</th>
						          	<th width="55%">Name</th>
						          	<th width="10%">Country</th>
						          	<th width="10%">Status</th>
						          	<th width="10%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($ports as $port)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>{{$port->code}}</td>
							            <td>{{$port->name}}</td>
							            <td>{{$port->fkCountry->name}}</td>
							            <td>
							            	@if($port->status == 1) <font color="blue">Active <i class="glyphicon glyphicon-ok"></i></font>
							            	@else <font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
							            	@endif
							            </td>

							            @if($port->status == 1)
								            <td>
								            	<!-- <a href="{{ route('port.edit',$port->id)}}" class="btn btn-xs btn-warning">Edit</a> &nbsp;
								            	 -->
								            	<button class="btn btn-xs btn-warning" value="{{ route('port.edit',$port->id) }}">Edit</button>
								            	&nbsp;
								            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$port->id}}">Delete</button>

								            	<form id="portFrm" > <!-- action="{{ route('port.destroy', $port->id)}}" method="post" -->
								                  	@csrf
								                  	@method('DELETE')
								                  	<div class="modal fade bs-example-modal{{$port->id}}" tabindex="-1" role="dialog" aria-hidden="true">
					                                    <div class="modal-dialog modal-sm">
					                                        <div class="modal-content">
					                                            <div class="modal-header btn-danger">
					                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
					                                                </button>
					                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
					                                            </div>
					                                            <div class="modal-body">
					                                                Are you sure to delete this data?
					                                            </div>
					                                            <div class="modal-footer">
					                                                <!-- <button type="submit" class="btn btn-success">Yes</button>
					                                                 -->
					                                                <button class="btn btn-success" data-dismiss="modal" value="{{ route('port.destroy',$port->id) }}">Yes</button>
					                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
					                                            </div>
					                                        </div>
					                                    </div>
					                                </div>
								                </form>
								            </td>
							            @else
							            	<td>
							            		<button type="button" class="btn btn-xs btn-primary" data-toggle='modal' data-target=".bs-example-modal{{$port->id}}">Activated</button>

							            		<form id="portFrm" > <!-- action="{{ route('port.destroy', $port->id)}}" method="post" -->

								                  	@csrf
								                  	@method('DELETE')
								                  	<div class="modal fade bs-example-modal{{$port->id}}" tabindex="-1" role="dialog" aria-hidden="true">
					                                    <div class="modal-dialog modal-sm">
					                                        <div class="modal-content">
					                                            <div class="modal-header btn-success">
					                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
					                                                </button>
					                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
					                                            </div>
					                                            <div class="modal-body">
					                                                Are you sure to activate this data?
					                                            </div>
					                                            <div class="modal-footer">
					                                                <button class="btn btn-success" data-dismiss="modal" value="{{ route('port.destroy',$port->id) }}">Yes</button>
					                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
					                                            </div>
					                                        </div>
					                                    </div>
					                                </div>
								                </form>
							            	</td>
							            @endif
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr style="background: #86c232 !important; font-weight: bold; text-align: left; color: white;">
						          	<td class="noShow"></td>
						          	<td>Code</td>
						          	<td>Name</td>
						          	<td>Country</td>
						          	<td>Status</td>
						          	<td class="noShow"></td>
						        </tr>
						        <tr style="background: #6b6e70 !important; font-weight: bold; text-align: left; color: white;">
						        	<td class="noShow" colspan="6"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	<!-- </div> -->
				<div>
            </div>
        </div>
  	</div>
@stop
@section('content_js')
    <script>
        // sdq.namespace('sdq.port');
        sdq.port = function()
        {   // do NOT access DOM from here; elements don't exist yet
            // execute at the first time
            // private variables
            this.target;
            this.action;

            // private functions
            //**********************
            // public space
            //**********************
            return {
                // execute at the very last time
                // public properties, e.g. strings to translate
                // public methods
                server_date: new Date().getFullYear(),
                start_date : new Date(this.server_date, 0, 1),
                end_date   : new Date(this.server_date, 11, 31),
                page_limit : 75,
                page_start : 0,
                sid        : '{{ csrf_token() }}',
                initialize: function()
                {   //console.log('initialize core');
                    this.prepare_component();
                    this.build_layout();
                    this.finalize_comp_and_layout();
                },
                prepare_component : function()
                {   this.prepare_list();   
                },
                build_layout: function()
                {   },
                finalize_comp_and_layout : function()
                {   },
                prepare_list : function()
                {   sdq.core.setSearchBar();
                	// NEW button
                    $('#portNewBtn').click( 
                        function(){ sdq.core.load('{{ route('port.create') }}'); } );
                    // EDIT button
                    $(".btn.btn-xs.btn-warning").each( 
                        function()
                        {   $(this).click( 
                            function(event)
                            {   sdq.core.load(event.currentTarget.value);
                            }) 
                        } );
                    // DELETE button
                    $(".btn.btn-success").each( 
                        function()
                        {   $(this).click( function(event)
                            {   sdq.core.submitForm('portFrm', event.currentTarget.value, 'port' );
                            }) 
                        } );   
                },
                prepare_create : function()
                {   // SAVE button
                    $('#saveBtn').click( function(button)
                        {   sdq.core.submitForm('portFrm', button.currentTarget.value, 'port' ); 
                        } );
                    // CANCEL button
                    $('#cancelBtn').click( 
                        function(){ sdq.core.load('port'); return false;} );
                },
                prepare_edit : function()
                {   // UPDATE button
                    $('#updateBtn').click( function(event)
                        {   sdq.core.submitForm('portFrm', event.currentTarget.value, 'port' ); 
                        } );
                    // CANCEL button
                    $('#cancelBtn').click( 
                        function(){ sdq.core.load('port'); return false;} );
                },
            };
        }();
        $(document).ready(sdq.port.initialize());
    </script>
@stop