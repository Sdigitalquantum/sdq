@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
            <li class="active"><a href="#tab_account" data-toggle="tab">Account</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_account">
            	<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Supplier Account &nbsp;<a href="{{ route('supplier.edit', $id) }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4>
			  		</div>
				  	
				  	<div class="box-body">
				  		<div class="row">
				  			<div class="col-md-3">
				  				<form method="post" action="{{ route('supplieraccount.store') }}">
						          	@csrf
						          	<input type="hidden" class="form-control" name="supplier" value="{{ $id }}">
						          	<div class="form-group">
						              	<label for="currency">Currency</label>
						              	<select class="form-control select2" name="currency" required>
						              		<option value="{{ old('currency') }}">Choose One</option>
					                    	@foreach($currencys as $currency)
											    <option value="{{ $currency->id }}">{{ $currency->code }} - {{ $currency->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="bank">Bank</label>
						              	<select class="form-control select2" name="bank" required>
						              		<option value="{{ old('bank') }}">Choose One</option>
					                    	@foreach($banks as $bank)
											    <option value="{{ $bank->id }}">{{ $bank->alias }} - {{ $bank->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="account_no">No. Rek</label>
						              	<input type="number" class="form-control" name="account_no" value="{{ old('account_no') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="account_swift">Swift Code</label>
						              	<input type="text" class="form-control" name="account_swift" value="{{ old('account_swift') }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="account_name">Name Rek</label>
						              	<input type="text" class="form-control" name="account_name" value="{{ old('account_name') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="alias">Alias</label>
						              	<input type="text" class="form-control" name="alias" value="{{ old('alias') }}">
						          	</div>
						          	<button type="submit" class="btn btn-sm btn-success">Save</button>
						      	</form>
			  				</div>

			  				<div class="col-md-9">
			  					<br><table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="20%">Bank</th>
								          	<th width="25%">Account</th>
								          	<th width="20%">Name</th>
								          	<th width="20%">Alias</th>
								          	<th width="15%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($supplieraccounts as $supplieraccount)
								    		<tr>
									            <td>{{$supplieraccount->fkBank->name}}</td>
									            <td>
									            	{{$supplieraccount->fkCurrency->code}} : {{$supplieraccount->account_no}} <br>
									            	Swift : {{$supplieraccount->account_swift}}
									            </td>
									            <td>{{$supplieraccount->account_name}}</td>
									            <td>{{$supplieraccount->alias}}</td>

									            @if($supplieraccount->status == 1)
										            <td>
										            	<a href="{{ route('supplieraccount.edit',$supplieraccount->id)}}" class="btn btn-xs btn-warning">Edit</a> &nbsp;
										            	
										            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$supplieraccount->id}}">Delete</button>

										            	<form action="{{ route('supplieraccount.destroy', $supplieraccount->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$supplieraccount->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-danger">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to delete this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
										            </td>
									            @else
									            	<td>
									            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$supplieraccount->id}}">Activated</button>

									            		<form action="{{ route('supplieraccount.destroy', $supplieraccount->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$supplieraccount->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-success">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to active this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
									            	</td>
									            @endif
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
			  				</div>
		  				</div>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop