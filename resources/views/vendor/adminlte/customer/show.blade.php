@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
            <li class="disabled"><a>Account</a></li>
            <li class="active"><a href="#tab_show" data-toggle="tab">Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_show">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Show Customer 
			    			&nbsp;<a href="javascript:window.print()" class="btn btn-xs btn-default" style="text-decoration: none;"><i class="glyphicon glyphicon-print"></i></a>
			    			&nbsp;<a href="{{ url('/customer') }}" class="btn btn-xs btn-danger">Back</a>
			    		</h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<div id="printable">
					      	<div class="row">
					      		<center>
				  					<b>{{$company->name}}</b> <br>
					  				CUSTOMER ACCOUNT <br>
					  				DOCUMENT INTERNAL <br>
					  				DATE : {{ date('d F Y', strtotime("now")) }} <br>
					  			</center><br>

								<div class="col-md-6">
									<div class="form-group">
						              	<label for="code">Code</label>
						              	<input type="text" class="form-control" name="code" value="{{ $customer->code }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="account">Accounting</label>
						              	@foreach($accounts as $account)
										    @if ($customer->accounting == $account->id)
											    <input type="text" class="form-control" name="account" value="{{ $account->code }} - {{ $account->name }}" disabled>
											@endif
										@endforeach
						          	</div>
						          	<div class="form-group">
						          		<label for="email">Email</label>
						              	<input type="text" class="form-control" name="email" value="{{ $customer->email }}" disabled>
						          	</div>
						          	<div class="form-group">
						          		<label for="fax">Fax</label>
						              	<input type="number" class="form-control" name="fax" value="{{ $customer->fax }}" disabled>
						          	</div>
						          	<div class="form-group">
						          		<label for="check">&nbsp;</label>
						          		@if($customer->tax == 1)
						          			<input type="checkbox" name="tax" value="1" checked disabled> Tax &nbsp;&nbsp;&nbsp;
						          		@else
						          			<input type="checkbox" name="tax" value="1" disabled> Tax &nbsp;&nbsp;&nbsp;
						          		@endif

						          		@if($customer->status_group == 1)
						          			<input type="checkbox" name="status_group" value="1" checked disabled> Status Group &nbsp;&nbsp;&nbsp;
						          		@else
						          			<input type="checkbox" name="status_group" value="1" disabled> Status Group &nbsp;&nbsp;&nbsp;
						          		@endif

						          		@if($customer->status_record == 1)
						          			<input type="checkbox" name="status_record" value="1" checked disabled> Status Record
						          		@else
						          			<input type="checkbox" name="status_record" value="1" disabled> Status Record
						          		@endif
						          	</div>
									<div class="form-group">
						          		<label for="npwp_no">No. NPWP</label>
						              	<input type="number" class="form-control" name="npwp_no" value="{{ $customer->npwp_no }}" disabled>
						          	</div>
						          	<div class="form-group">
						          		<label for="npwp_name">Name NPWP</label>
						              	<input type="text" class="form-control" name="npwp_name" value="{{ $customer->npwp_name }}" disabled>
						          	</div>
						          	<div class="form-group">
						          		<label for="npwp_address">Address NPWP</label>
						              	<input type="text" class="form-control" name="npwp_address" value="{{ $customer->npwp_address }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="npwp_city">City NPWP</label>
						              	@foreach($citys as $city)
										    @if ($customer->npwp_city == $city->id)
											    <input type="text" class="form-control" name="npwp_city" value="{{ $city->name }} - {{ $city->fkCountry->name }}" disabled>
											@endif
										@endforeach
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						          		<label for="limit">Limit</label>
						              	<input type="number" class="form-control" name="limit" value="{{ $customer->limit }}" disabled>
						          	</div>
						          	<div class="form-group">
						          		<label for="note1">Note 1</label>
						          		<textarea class="form-control" name="note1" rows="5" disabled>{{ $customer->note1 }}</textarea>
						          	</div>
						          	<div class="form-group">
						          		<label for="note2">Note 2</label>
						          		<textarea class="form-control" name="note2" rows="4" disabled>{{ $customer->note2 }}</textarea>
						          	</div>
						          	<div class="form-group">
						          		<label for="note3">Note 3</label>
						          		<textarea class="form-control" name="note3" rows="4" disabled>{{ $customer->note3 }}</textarea>
						          	</div>
						          	<div class="form-group">
						          		<label for="notice">Notice</label>
						          		<textarea class="form-control" name="notice" rows="4" disabled>{{ $customer->notice }}</textarea>
						          	</div>
								</div>
							</div>

							<table class="table table-bordered table-hover nowrap" width="100%">
							    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
							        <tr>
							          	<th width="20%">Name</th>
							          	<th width="30%">Address</th>
							          	<th width="20%">Mobile</th>
							          	<th width="20%">Alias</th>
							          	<th width="10%">Status</th>
							        </tr>
							    </thead>

							    <tbody>
							    	@foreach($customerdetails as $customerdetail)
							    		<tr>
							    			<td>{{$customerdetail->name}}</td>
								            <td>
								            	{{$customerdetail->address}} <br>
								            	{{$customerdetail->fkCity->name}} - {{$customerdetail->fkCity->fkCountry->name}}
								            </td>
								            <td>{{$customerdetail->mobile}}</td>
								            <td>
								            	{{$customerdetail->alias_name}} <br> {{$customerdetail->alias_mobile}}
								            </td>
								            <td>@if($customerdetail->status == 1) <font color="blue">Active</font> @else <font color="red">Not Active</font> @endif</td>
								        </tr>
							        @endforeach
							    </tbody>
						  	</table>

						  	<table class="table table-bordered table-hover nowrap" width="100%">
							    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
							        <tr>
							        	<th width="20%">Bank</th>
							          	<th width="30%">Account</th>
							          	<th width="20%">Name</th>
							          	<th width="20%">Alias</th>
							          	<th width="10%">Status</th>
							        </tr>
							    </thead>

							    <tbody>
							    	@foreach($customeraccounts as $customeraccount)
							    		<tr>
								            <td>{{$customeraccount->fkBank->name}}</td>
								            <td>
								            	{{$customeraccount->fkCurrency->code}} : {{$customeraccount->account_no}} <br>
								            	Swift : {{$customeraccount->account_swift}}
								            </td>
								            <td>{{$customeraccount->account_name}}</td>
								            <td>{{$customeraccount->alias}}</td>
								            <td>@if($customeraccount->status == 1) <font color="blue">Active</font> @else <font color="red">Not Active</font> @endif</td>
								        </tr>
							        @endforeach
							    </tbody>
						  	</table>
					  	</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop