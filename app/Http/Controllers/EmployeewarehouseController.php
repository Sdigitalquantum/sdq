<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Employeewarehouse;
use App\Http\Models\Employee;
use App\Http\Models\Warehouse;
use App\Http\Models\Employeeset;

class EmployeewarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $warehouses = Warehouse::orderBy('created_at','desc')->get();
        $employeewarehouses = Employeewarehouse::orderBy('warehouse','asc')->get();

        return view('vendor/adminlte/employeewarehouse.index', compact('warehouses','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $warehouse = Warehouse::find($id);
        $employees = Employee::orderBy('name','asc')->get();
        $employeewarehouses = Employeewarehouse::where('warehouse', $id)->orderBy('warehouse','asc')->get();

        return view('vendor/adminlte/employeewarehouse.edit', compact('warehouse','employees','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $cek = Employeewarehouse::where([['employee', $request->get('employee')], ['warehouse', $request->get('warehouse')]])->first();

        if (!empty($cek)) {
            return redirect()->back()->withErrors('User Warehouse already exist');
        } else {
            $employeewarehouse = new Employeewarehouse([
                'employee'      => $request->get('employee'),
                'warehouse'     => $request->get('warehouse'),
                'created_user'  => $session_id,
                'updated_user'  => $session_id
            ]);
            $employeewarehouse->save();

            return redirect('/employeewarehouse/'.$request->get('warehouse').'/edit')->with('success', 'User Warehouse has been added');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $employeewarehouse = employeewarehouse::find($id);
        $status = $employeewarehouse->status;

        if ($status == 1) {
            $employeewarehouse->status       = 0;
            $employeewarehouse->updated_user = $session_id;
            $employeewarehouse->save();

            return redirect('/employeewarehouse/'.$employeewarehouse->warehouse.'/edit')->with('error', 'User Warehouse has been deleted');
        } elseif ($status == 0) {
        	$employeewarehouse->status       = 1;
            $employeewarehouse->updated_user = $session_id;
	        $employeewarehouse->save();

		    return redirect('/employeewarehouse/'.$employeewarehouse->warehouse.'/edit')->with('success', 'User Warehouse has been activated');
        }
    }
}
