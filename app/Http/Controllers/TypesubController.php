<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Typesub;
use App\Http\Models\Type;
use App\Http\Models\Employeeset;

class TypesubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $typesubs = Typesub::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/typesub.index', compact('typesubs','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $types = Type::where('status', 1)->get();

        return view('vendor/adminlte/typesub.create', compact('types','disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	$json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'name' => 'unique:typesubs'
                ]);

                $typesub = new Typesub([
                    'type'          => $json->type,
                    'name'          => $json->name,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $typesub->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $typesub = Typesub::find($id);
        $types = Type::where('status', 1)->get();

        return view('vendor/adminlte/typesub.edit', compact('typesub','types','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $typesub = Typesub::find($id);
                $name = $typesub->name;

                if ($name != $json->name) {
                    $request->validate([
                        'name' => 'unique:typesubs'
                    ]);
                }
                
                $typesub->type          = $json->type;
                $typesub->name          = $json->name;
                $typesub->updated_user  = $session_id;
                $result = $typesub->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $typesub = Typesub::find($id);
        $status = $typesub->status;

        if ($status == 1) {
        	$typesub->status       = 0;
            $typesub->updated_user = $session_id;
	        $result = $typesub->save();

        } elseif ($status == 0) {
        	$typesub->status       = 1;
            $typesub->updated_user = $session_id;
	        $result = $typesub->save();

        };
        return $this->jsonSuccess( $result );
    }
}
