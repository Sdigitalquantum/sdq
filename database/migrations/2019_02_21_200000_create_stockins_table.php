<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stockins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction')->unsigned();
            $table->integer('product')->unsigned();
            $table->integer('warehouse')->unsigned();
            $table->integer('supplierdetail')->unsigned();
            $table->integer('no_inc');
            $table->string('nomor');
            $table->date('date_in');
            $table->date('date_move')->nullable();
            $table->date('date_out')->nullable();
            $table->date('date_opname')->nullable();
            $table->string('noref_in')->nullable();
            $table->string('noref_out')->nullable();
            $table->string('price');
            $table->integer('qty_bag');
            $table->integer('qty_pcs');
            $table->integer('qty_kg');
            $table->integer('qty_qc')->default('0');
            $table->integer('qty_validate')->default('0');
            $table->integer('qty_available');
            $table->integer('qty_booked')->default('0');
            $table->integer('qty_transit')->default('0');
            $table->integer('qty_move')->default('0');
            $table->integer('qty_out')->default('0');
            $table->integer('qty_opname')->default('0');
            $table->integer('transaction_out')->default('0');
            $table->integer('transaction_prod');
            $table->tinyInteger('status_qc')->default('0');
            $table->tinyInteger('status_validate')->default('0');
            $table->tinyInteger('status_move');
            $table->tinyInteger('status_out')->default('0');
            $table->tinyInteger('status_opname')->default('0');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->timestamp('validated_at')->nullable();
            $table->timestamp('moved_at')->nullable();
            $table->timestamp('outed_at')->nullable();
            $table->timestamp('opnamed_at')->nullable();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->integer('validated_user')->nullable();
            $table->integer('moved_user')->nullable();
            $table->integer('outed_user')->nullable();
            $table->integer('opnamed_user')->nullable();
            $table->foreign('transaction')->references('id')->on('transactions');
            $table->foreign('product')->references('id')->on('products');
            $table->foreign('warehouse')->references('id')->on('warehouses');
            $table->foreign('supplierdetail')->references('id')->on('supplierdetails');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stockins');
    }
}
