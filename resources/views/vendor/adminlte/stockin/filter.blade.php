@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Stock Card &nbsp;<a href="{{ url('stockcard')}}" class="btn btn-xs btn-danger" style="text-decoration: none;">Back</a></h4>
  		</div>
	  	
	  	<div class="box-body">
	  		<form method="post" action="{{ url('stockfilter') }}">
	          	@csrf
	          	<div class="row">
					<div class="col-md-6">
						<div class="form-group">
			              	<label for="product">Product</label>
			              	<select class="form-control select2" name="product" required>
			              		<option value="{{ old('product') }}">Choose One</option>
		                    	@foreach($products as $product)
								    <option value="{{ $product->id }}">{{ $product->code }} - {{ $product->name }}</option>
								@endforeach
		                	</select>
			          	</div>
			          	<div class="form-group">
			              	<label for="year">Year</label>
			              	<input type="number" class="form-control" name="year" value="{{ old('year') }}" required>
			          	</div>
			          	<div class="form-group">
			              	<label for="month">Month</label>
			              	<select class="form-control select2" name="month" required>
			              		<option value="{{ old('month') }}">Choose One</option>
			              		<option value="1">January</option>
			              		<option value="2">February</option>
			              		<option value="3">March</option>
			              		<option value="4">April</option>
			              		<option value="5">May</option>
			              		<option value="6">June</option>
			              		<option value="7">July</option>
			              		<option value="8">August</option>
			              		<option value="9">September</option>
			              		<option value="10">October</option>
			              		<option value="11">November</option>
			              		<option value="12">Desember</option>
		                	</select>
			          	</div>
			          	<div class="form-group">
			          		@if($employee->fkGroup->name != 'User Inventory')
			          			<input type="checkbox" name="price" value="1"> Price &nbsp;
		          			@endif
			              	<button type="submit" class="btn btn-sm btn-success">Filter</button>
			          	</div>
					</div>

					<div class="col-md-6">
						Product : 
							{{ $stock->name }} <br>
						Periode : 
							@if($month == 1) January
							@elseif($month == 2) February
							@elseif($month == 3) March
							@elseif($month == 4) April
							@elseif($month == 5) May
							@elseif($month == 6) June
							@elseif($month == 7) July
							@elseif($month == 8) August
							@elseif($month == 9) September
							@elseif($month == 10) October
							@elseif($month == 11) November
							@elseif($month == 12) Desember
							@endif
							{{ $year }} <br><br>
						Beginning Balance : 
							@if(!empty($stockold))
								{{number_format($stockold->qty-$stockold->qty_out, 0, ',' , '.')}}
							@else 0
							@endif <br><br>
						Total In &nbsp;&nbsp;&nbsp;:
							@if(!empty($stockcard))
								{{number_format($stockcard->qty, 0, ',' , '.')}}
							@else 0
							@endif <br><br>
						Total Out :
							@if(!empty($stockcard))
								{{number_format($stockcard->qty_out, 0, ',' , '.')}}
							@else 0
							@endif <br><br>
						Ending Balance &nbsp;&nbsp;&nbsp;&nbsp; : 
							@if(!empty($stockcard))
								@if(!empty($stockold))
									{{number_format($stockcard->qty-$stockcard->qty_out-($stockold->qty-$stockold->qty_out), 0, ',' , '.')}}
								@else
									{{number_format($stockcard->qty-$stockcard->qty_out, 0, ',' , '.')}}
								@endif
							@else 0
							@endif <br><br>
					</div>
				</div>
	      	</form>

		  	<table class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="15%">Date</th>
			          	<th width="25%">Supplier / Customer</th>
			          	<th width="25%">Reference</th>
			          	<th width="10%">In (Kg)</th>
			          	<th width="10%">Out (Kg)</th>
			          	@if($price == 1)
			          		<th width="25%">Price @Kg</th>
		          		@endif
			        </tr>
			    </thead>

			    <tbody>
			    	@foreach($stockins as $stockin)
			        	<tr>
				            <td>{{$stockin->date_in}}</td>
				            <td>
				            	@if($stockin->noref_in == 'PRODUCTION')
				            		INTERNAL
				            	@else
				            		{{$stockin->fkSupplierdetail->name}}
			            		@endif
				            </td>
				            <td>{{$stockin->noref_in}}</td>
				            <td style="text-align: right;">{{number_format($stockin->qty_validate, 0, ',' , '.')}} &nbsp;</td>
				            <td style="text-align: right;">- &nbsp;</td>

			            	@if($price == 1)
				          		<td style="text-align: right;">{{number_format($stockin->price, 0, ',' , '.')}} &nbsp;</td>
			          		@endif
				        </tr>
			        @endforeach

			        @foreach($stockouts as $stockout)
			        	<tr>
				            <td>{{$stockout->date_out}}</td>
				            <td>{{$stockout->fkSupplierdetail->name}}</td>
				            <td>{{$stockout->noref_out}}</td>
				            <td style="text-align: right;">- &nbsp;</td>
				            <td style="text-align: right;">{{number_format($stockout->qty_out, 0, ',' , '.')}} &nbsp;</td>

				            @if($price == 1)
				          		<td style="text-align: right;">{{number_format($stockout->price, 0, ',' , '.')}} &nbsp;</td>
			          		@endif
				        </tr>
			        @endforeach
			    </tbody>
		  	</table>
	  	</div>
	<div>
@stop