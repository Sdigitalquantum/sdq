@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #6b6e70 !important; font-weight: bold; color: white;">
			    		<h4 class="box-title">Edit Warehouse</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form id="warehouseFrm">
				      		@method('PATCH')
				      		@csrf
				          	<div class="form-group">
				              	<label for="code">Code</label>
				              	<input type="text" class="form-control" name="code" value="{{ $warehouse->code }}" disabled>
				          	</div>
				          	<div class="form-group">
				              	<label for="name">Name</label>
				              	<input type="text" class="form-control" name="name" value="{{ $warehouse->name }}" required>
				          	</div>
				          	<div class="form-group">
				              	<label for="address">Address</label>
				              	<input type="text" class="form-control" name="address" value="{{ $warehouse->address }}" required>
				          	</div>
				          	<div class="form-group">
				              	<label for="city">City</label>
				              	<select name="city" class="form-control select2" required>
				              		@foreach($cities as $city)
				              			@if ($warehouse->city == $city->id)
										    <option value="{{ $city->id }}" selected>{{ $city->name }} - {{$city->fkCountry->name}}</option>
										@else
										    <option value="{{ $city->id }}">{{ $city->name }} - {{$city->fkCountry->name}}</option>
										@endif
									@endforeach
								</select>
				          	</div>
				          	<button id="updateBtn" value="{{ route('warehouse.update', $warehouse->id) }}" class="btn btn-sm btn-success">Update</button>
				          	<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.warehouse.prepare_edit());
    </script>
@stop