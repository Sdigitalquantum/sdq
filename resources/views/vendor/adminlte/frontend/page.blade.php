@extends('adminlte::frontend/master')

@section('body')
    @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}  
        </div>
    @elseif(session()->get('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}  
        </div>
    @elseif ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
        
    <section class="header bg-lightgray header-small">
        <div class="topbar" data-effect="fadeIn">
            <div class="menu">
                <div class="primary inviewport animated delay4" data-effect="fadeInRightBig">
                    <div class='cssmenu'>
                        <ul class='menu-ul'>
                            <li class='has-sub'>
                                <a href="{{ url('/') }}">Home</a>
                            </li>
                            <li class='has-sub'>
                                <a href="{{ url('order') }}">Order</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="black inviewport animated delay4" data-effect="fadeInLeftBig">
                    <div class='cssmenu'>
                        <ul class='menu-ul'>
                            <li class='has-sub'>
                                <a href="{{ url('contact') }}">Contact</a>
                            </li>
                            <li class='has-sub'>
                                <a href="{{ url('login') }}">Login</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class='header-logo-wrap'>
            <div class="container">
                <div class="logo col-xs-2">
                    <span>S.D.Q</span>
                </div>
                <div class="menu-mobile col-xs-10 pull-right cssmenu">
                    <i class="mdi mdi-menu menu-toggle"></i>
                    <ul class="menu" id='parallax-mobile-menu'>
                    </ul>
                </div>
            </div>
        </div>

        <div class="header-slide" style="position:relative;">
            <img alt="header-banner-image" src="{{ asset('vendor/caliber/img/sdq.jpg') }}" class='header-img'>
            <div class="overlay overlay2">
                <div class="black inviewport animated delay4" data-effect="fadeInLeftOpacity"></div>
                <div class="primary inviewport animated delay4" data-effect="fadeInRightOpacity"></div>
                
                <div class="maintext">
                    <div class="primary-text inviewport animated delay4" data-effect="fadeInRightBig">
                        <div class="left-line">
                            <h4>PT.</h4>
                            <h1>STARINDO</h1>
                        </div>
                    </div>
                    <div class="black-text inviewport animated delay4" data-effect="fadeInLeftBig">
                        <div>
                            <h1>ANUGERAH <br>ABADI</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        @yield('content')
    </section>

    <section class='footer bg-black padding-top-75 padding-bottom-25'>
        <div class="angled_down_inside black">
            <div class="slope upleft"></div>
            <div class="slope upright"></div>
        </div>
        
        <div class="container">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 message animated delay1" data-effect="fadeInUp" style="color: white;">
                <span>
                    <table width="100%">
                        <tr>
                            <td colspan="2">PT. STARINDO ANUGERAH ABADI</td>
                        </tr>
                        <tr>
                            <td width="10%">Address</td>
                            <td width="90%">&nbsp;: Jalan Raya Gading Rejo No. 49,</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp; Pasuruan East Java-Indonesia</td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td>&nbsp;: +6281 23025009</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>&nbsp;: info@sdq.org</td>
                        </tr>
                    </table>
                </span>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 social"><img src="{{ asset('vendor/caliber/img/sdq0.jpg') }}" width="100%"></div>
        </div>
    </section>
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $(".alert").alert();
            window.setTimeout(function () {
                $(".alert").alert('close');
            }, 3000);
        });
    </script>
    
    @stack('js')
    @yield('js')
@stop
