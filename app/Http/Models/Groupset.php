<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Groupset extends Model
{
    protected $fillable = [
	    'group',
	    'menuroot',
	    'created_user',
	    'updated_user'
  	];

  	public function fkGroup(){
	    return $this->belongsTo('\App\Http\Models\Group', 'group', 'id');
	}

  	public function fkMenuroot(){
	    return $this->belongsTo('\App\Http\Models\Menuroot', 'menuroot', 'id');
	}
}
