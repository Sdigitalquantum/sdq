@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Purchase Derivative</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('purchasederivative.update', $stockqc->id) }}">
				      		@method('PATCH')
				      		@csrf
				      		<div class="row">
    							<div class="col-md-6">
    								<div class="form-group">
						              	<label for="no_po">No. PO</label>
						              	<input type="hidden" name="purchasedetail" value="{{ $stockqc->purchasedetail }}">
						              	<input type="text" class="form-control" name="no_po" value="{{ $stockqc->no_po }}" disabled>
						          	</div>
									<div class="form-group">
						              	<label for="product">Product</label>
						              	<select class="form-control select2" name="product" disabled>
						              		@foreach($products as $product)
											    @if ($stockqc->product == $product->id)
												    <option value="{{ $product->id }}" selected>{{ $product->code }} - {{ $product->name }}</option>
												@else
												    <option value="{{ $product->id }}">{{ $product->code }} - {{ $product->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="warehouse">Warehouse</label>
						              	<select class="form-control select2" name="warehouse" disabled>
						              		@foreach($warehouses as $warehouse)
											    @if ($stockqc->warehouse == $warehouse->id)
												    <option value="{{ $warehouse->id }}" selected>{{ $warehouse->code }} - {{ $warehouse->name }}</option>
												@else
												    <option value="{{ $warehouse->id }}">{{ $warehouse->code }} - {{ $warehouse->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="supplierdetail">Supplier</label>
						              	<select class="form-control select2" name="supplierdetail" disabled>
						              		@foreach($supplierdetails as $supplierdetail)
											    @if ($stockqc->supplierdetail == $supplierdetail->id)
												    <option value="{{ $supplierdetail->id }}" selected>{{ $supplierdetail->name }}</option>
												@else
												    <option value="{{ $supplierdetail->id }}">{{ $supplierdetail->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="date_in">Date</label>
						              	<input type="text" class="form-control datepicker" name="date_in" value="{{ $stockqc->date_in }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="price">Price @Kg</label>
						              	<input type="number" class="form-control" name="price" value="{{ $stockqc->price }}" required>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						              	<label for="qty_bag">Qty Bag</label>
						              	<input type="number" class="form-control" name="qty_bag" value="{{ $stockqc->qty_bag }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="qty_pcs">Qty Pcs</label>
						              	<input type="number" class="form-control" name="qty_pcs" value="{{ $stockqc->qty_pcs }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="qty_kg">Qty Kg</label>
						              	<input type="number" class="form-control" name="qty_kg" value="{{ $stockqc->qty_kg }}" disabled>
						          	</div>
						          	<div class="form-group">
						          		<label for="notice">Note</label>
						          		<textarea class="form-control" name="notice" maxlength="150" rows="6" required>{{ $stockqc->notice }}</textarea>
						          	</div>
						          	<div class="form-group">
						          		<button type="submit" class="btn btn-sm btn-success">Update</button>
			          					<a href="{{ url('purchasederivative') }}" class="btn btn-sm btn-danger">Cancel</a>
						          	</div>
								</div>
							</div>
						</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop