<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceivablecardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receivablecards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customerdetail')->unsigned();
            $table->integer('year');
            $table->tinyInteger('month');
            $table->bigInteger('debit');
            $table->bigInteger('kredit');
            $table->timestamps();
            $table->foreign('customerdetail')->references('id')->on('customerdetails');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receivablecards');
    }
}
