@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
            <li class="disabled"><a>Payment</a></li>
            <li class="disabled"><a>Charges</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">New Quotation</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('quotation.store') }}">
				          	@csrf
				          	<div class="row">
    							<div class="col-md-6">
    								<div class="form-group">
						          		<label for="date_sp">Date</label>
						              	<input type="text" class="form-control datepicker" name="date_sp" value="{{ old('date_sp') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="status_flow">Flow</label> <br>
						              	<input type="radio" name="status_flow" value="1" required> Export &nbsp;&nbsp;&nbsp;
						              	<input type="radio" name="status_flow" value="2"> Local &nbsp;&nbsp;&nbsp;
						              	<input type="radio" name="status_flow" value="3"> Internal
						          	</div>
				  					<div class="form-group">
						              	<label for="sales">Sales</label>
						              	<select class="form-control select2" name="sales" required>
						              		<option value="{{ old('sales') }}">Choose One</option>
					                    	@foreach($saless as $sales)
											    <option value="{{ $sales->id }}">{{ $sales->code }} - {{ $sales->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="customerdetail">Customer</label> &nbsp;&nbsp;&nbsp;
						              	<input type="checkbox" name="tax" value="1"> Tax
						              	<select class="form-control select2" name="customerdetail" id="ajaxcustomerdetail" required>
						              		<option value="{{ old('customerdetail') }}">Choose One</option>
					                    	@foreach($customerdetails as $customerdetail)
											    <option value="{{ $customerdetail->id }}">{{ $customerdetail->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="no_poc">PO Customer</label>
						              	<input type="text" class="form-control" name="no_poc" value="{{ old('no_poc') }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="kurs">Kurs</label>
						              	<input type="number" class="form-control" name="kurs" value="{{ old('kurs') }}" step=".01">
						          	</div>
				  					<div class="form-group">
						              	<label for="currency">Currency</label>
						              	<select class="form-control select2" name="currency" required>
						              		@foreach($currencys as $currency)
											    <option value="{{ $currency->id }}">{{ $currency->code }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="disc_type">Discount</label> <br>
						              	<input type="radio" name="disc_type" value="1"> % &nbsp;&nbsp;&nbsp;
						              	<input type="radio" name="disc_type" value="2"> Rp / $ &nbsp;&nbsp;&nbsp;
						              	<input type="number" class="form-control" name="disc_value" value="{{ old('disc_value') }}" step=".01">
						          	</div>
						          	<div class="form-group">
						              	<label for="paymentterm">Payment Term</label>
						              	<select class="form-control select2" name="paymentterm" required>
						              		<option value="{{ old('paymentterm') }}">Choose One</option>
					                    	@foreach($paymentterms as $paymentterm)
											    <option value="{{ $paymentterm->id }}">{{ $paymentterm->code }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="companyaccount">Company Bank</label>
						              	<select class="form-control select2" name="companyaccount" required>
						              		<option value="{{ old('companyaccount') }}">Choose One</option>
					                    	@foreach($companyaccounts as $companyaccount)
											    <option value="{{ $companyaccount->id }}">({{ $companyaccount->fkCurrency->code }}) {{ $companyaccount->account_no }} - {{ $companyaccount->fkBank->name }}</option>
											@endforeach
					                	</select>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						              	<label for="container">Vehicle</label>
						              	<select class="form-control select2" name="container" required>
						              		<option value="{{ old('container') }}">Choose One</option>
					                    	@foreach($containers as $container)
					                    		@if($container->size == 0)
					                    			<option value="{{ $container->id }}">{{ $container->name }}</option>
											    @else
											    	<option value="{{ $container->id }}">{{ $container->size }}" {{ $container->name }}</option>
										    	@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="vessel">Vessel</label>
						              	<input type="text" class="form-control" name="vessel" value="{{ old('vessel') }}">
						          	</div>
									<div class="form-group">
						              	<label for="pol">Port Loading</label>
						              	<select class="form-control select2" name="pol" required>
						              		<option value="{{ old('pol') }}">Choose One</option>
					                    	@foreach($ports as $port)
											    <option value="{{ $port->id }}">{{ $port->name }} - {{ $port->fkCountry->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="pod">Port Discharge</label>
						              	<select class="form-control select2" name="pod" required>
						              		<option value="{{ old('pod') }}">Choose One</option>
					                    	@foreach($ports as $port)
											    <option value="{{ $port->id }}">{{ $port->name }} - {{ $port->fkCountry->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="etd">Time Departure</label>
						              	<input type="text" class="form-control datepicker" name="etd" value="{{ old('etd') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="eta">Time Arrival</label>
						              	<input type="text" class="form-control datepicker" name="eta" value="{{ old('eta') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_gross">Gross Weight (Kg)</label>
						              	<input type="number" class="form-control" name="qty_gross" value="{{ old('qty_gross') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_cbm">CBM</label>
						              	<input type="number" class="form-control" name="qty_cbm" value="{{ old('qty_cbm') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="notice">Note</label>
						          		<textarea class="form-control" name="notice" id="ajaxnotice" maxlength="150" rows="3"></textarea>
						          	</div>
						          	<div class="form-group">
						          		<button type="submit" class="btn btn-sm btn-success">Save</button>
			          					<a href="{{ url('/quotation') }}" class="btn btn-sm btn-danger">Cancel</a>
						          	</div>
								</div>
							</div>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop