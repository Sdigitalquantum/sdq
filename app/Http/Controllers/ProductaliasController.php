<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Productalias;
use App\Http\Models\Customerdetail;
use App\Http\Models\Employeeset;

class ProductaliasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $cek = Productalias::where([
                        ['customerdetail', $json->customerdetail], 
                        ['product', $json->product]]
                    )->first();

                if (!empty($cek)) { $result = false; } 
                else {
                    $productalias = new Productalias([
                        'product'           => $json->product,
                        'customerdetail'    => $json->customerdetail,
                        'alias_name'        => !empty($json->alias_name) ? $json->alias_name : '',
                        'created_user'      => $session_id,
                        'updated_user'      => $session_id
                    ]);
                    $result = $productalias->save();
                };
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $productaliass = Productalias::where('product', $id)->orderBy('created_at','desc')->get();
        $customerdetails = Customerdetail::where('status', 1)->get();

        return view('vendor/adminlte/productalias.show', compact('id','productaliass','customerdetails','disable'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $productalias = Productalias::find($id);
        $productaliass = Productalias::where('product', $productalias->product)->orderBy('created_at','desc')->get();
        $customerdetails = Customerdetail::where('status', 1)->get();

        return view('vendor/adminlte/productalias.edit', compact('productalias','productaliass','customerdetails','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            $productalias = Productalias::find($id);
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                if ($productalias->customerdetail != $json->customerdetail) 
                {
                    $cek = Productalias::where([
                            ['customerdetail', $json->customerdetail], 
                            ['product', $json->product]]
                        )->first();

                    if (!empty($cek)) { $result = false; } 
                    else {
                        $productalias->product          = $json->product;
                        $productalias->customerdetail   = $json->customerdetail;
                        $productalias->alias_name       = !empty($json->alias_name) ? $json->alias_name : '';
                        $productalias->updated_user     = $session_id;
                        $result = $productalias->save(); 
                    }
                } else {
                    $productalias->product          = $json->product;
                    $productalias->customerdetail   = $json->customerdetail;
                    $productalias->alias_name       = !empty($json->alias_name) ? $json->alias_name : '';
                    $productalias->updated_user     = $session_id;
                    $result = $productalias->save();
                }
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $productalias = Productalias::find($id);
        $status = $productalias->status;

        if ($status == 1) {
        	$productalias->status       = 0;
            $productalias->updated_user = $session_id;
	        $result = $productalias->save();

        } elseif ($status == 0) {
        	$productalias->status       = 1;
            $productalias->updated_user = $session_id;
	        $result = $productalias->save();
            
        };
        return $this->jsonSuccess( $result );
    }
}
