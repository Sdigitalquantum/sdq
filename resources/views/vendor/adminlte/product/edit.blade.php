@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
            <li class="disabled"><a>Alias</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
	      
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Product &nbsp;<a href="{{ route('productalias.edit', $id) }}" class="btn btn-xs btn-primary" style="text-decoration: none;">Alias</a></h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<!-- <form method="post" action="{{ route('product.update', $product->id) }}"> -->
						<form id="productFrm">
							@method('PATCH')
							@csrf
				      		<div class="row">
				          		<div class="col-md-6">
						      		<div class="form-group">
						              	<label for="account">Accounting</label>
						              	<select class="form-control select2" name="account" required>
						              		@foreach($accounts as $account)
											    @if ($product->account == $account->id)
												    <option value="{{ $account->id }}" selected>{{ $account->code }} - {{ $account->name }}</option>
												@else
												    <option value="{{ $account->id }}">{{ $account->code }} - {{ $account->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						      		<div class="form-group">
						              	<label for="code">Code</label>
						              	<input type="text" class="form-control" name="code" value="{{ $product->code }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ $product->name }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="department">Department</label>
						              	<select class="form-control select2" name="department" required>
						              		@foreach($departments as $department)
											    @if ($product->department == $department->id)
												    <option value="{{ $department->id }}" selected>{{ $department->name }}</option>
												@else
												    <option value="{{ $department->id }}">{{ $department->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="variant">Variant</label>
						              	<select class="form-control select2" name="variant">
						              		@foreach($variants as $variant)
											    @if ($product->variant == $variant->id)
												    <option value="{{ $variant->id }}" selected>{{ $variant->name }}</option>
												@else
												    <option value="{{ $variant->id }}">{{ $variant->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
					          	</div>

					          	<div class="col-md-6">
						          	<div class="form-group">
						              	<label for="merk">Merk</label>
						              	<select class="form-control select2" name="merk">
						              		@foreach($merks as $merk)
											    @if ($product->merk == $merk->id)
												    <option value="{{ $merk->id }}" selected>{{ $merk->name }}</option>
												@else
												    <option value="{{ $merk->id }}">{{ $merk->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="typesub">Type Sub</label>
						              	<select class="form-control select2" name="typesub">
						              		@foreach($typesubs as $typesub)
											    @if ($product->typesub == $typesub->id)
												    <option value="{{ $typesub->id }}" selected>{{ $typesub->name }}</option>
												@else
												    <option value="{{ $typesub->id }}">{{ $typesub->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="size">Size</label>
						              	<select class="form-control select2" name="size">
						              		@foreach($sizes as $size)
											    @if ($product->size == $size->id)
												    <option value="{{ $size->id }}" selected>{{ $size->name }}</option>
												@else
												    <option value="{{ $size->id }}">{{ $size->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="unit">Unit</label>
						              	<select class="form-control select2" name="unit" required>
						              		@foreach($units as $unit)
											    @if ($product->unit == $unit->id)
												    <option value="{{ $unit->id }}" selected>{{ $unit->name }}</option>
												@else
												    <option value="{{ $unit->id }}">{{ $unit->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="check">&nbsp;</label>
						          		@if($product->general == 1)
						          			<input type="checkbox" name="general" value="1" checked> General &nbsp;&nbsp;&nbsp;
						          		@else
						          			<input type="checkbox" name="general" value="1"> General &nbsp;&nbsp;&nbsp;
						          		@endif

						          		@if($product->status_group == 1)
						          			<input type="checkbox" name="status_group" value="1" checked> Status Group &nbsp;&nbsp;&nbsp;
						          		@else
						          			<input type="checkbox" name="status_group" value="1"> Status Group &nbsp;&nbsp;&nbsp;
						          		@endif

						          		@if($product->status_record == 1)
						          			<input type="checkbox" name="status_record" value="1" checked> Status Record
						          		@else
						          			<input type="checkbox" name="status_record" value="1"> Status Record
						          		@endif
									</div>
									<div class="form-group">
										<button id="updateBtn" value="{{ route('product.update', $product->id) }}" class="btn btn-sm btn-success">Update</button>
				          				<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>										 										  
						          		<!-- <br><button type="submit" class="btn btn-sm btn-success">Update</button>
						          		<a href="{{ url('/product') }}" class="btn btn-sm btn-danger">Cancel</a> -->
								  	</div>
				          		</div>
			          		</div>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('content_js')
	<script>
    	$(document).ready(sdq.product.prepare_edit());
    </script>
@stop
