<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulestuffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedulestuffs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('schedulebook')->unsigned();
            $table->date('date');
            $table->string('person');
            $table->text('notice')->nullable();
            $table->text('reason')->nullable();
            $table->string('vessel');
            $table->integer('freight');
            $table->integer('qty_fcl');
            $table->integer('qty_approve')->default('0');
            $table->integer('qty_amount')->default('0');
            $table->integer('qty_print')->default('0');
            $table->integer('qty_invoice')->default('0');
            $table->tinyInteger('status_approve')->default('0');
            $table->tinyInteger('status_request')->default('0');
            $table->tinyInteger('status_load')->default('0');
            $table->tinyInteger('status_stuff')->default('0');
            $table->tinyInteger('status_delivery')->default('0');
            $table->tinyInteger('status_send')->default('0');
            $table->tinyInteger('status_pack')->default('0');
            $table->tinyInteger('status_document')->default('0');
            $table->tinyInteger('status_reschedule')->default('0');
            $table->tinyInteger('status_confirm')->default('0');
            $table->tinyInteger('status_reroute')->default('0');
            $table->tinyInteger('status_journal')->default('0');
            $table->timestamps();
            $table->timestamp('sent_at')->nullable();
            $table->timestamp('packed_at')->nullable();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->integer('sent_user')->nullable();
            $table->integer('packed_user')->nullable();
            $table->foreign('schedulebook')->references('id')->on('schedulebooks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedulestuffs');
    }
}
