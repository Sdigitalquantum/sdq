<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Container;
use App\Http\Models\Employeeset;

class ContainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $containers = Container::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/container.index', compact('containers','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $containers = new Container();

        return view('vendor/adminlte/container.create', compact('containers', 'disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'size' => 'numeric|min:0',
                    'qty'  => 'numeric|min:0|not_in:0'
                ]);
                
                $container = new Container([
                    'size'          => $json->size,
                    'name'          => $json->name,
                    'qty'           => $json->qty,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $container->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $container = Container::find($id);

        return view('vendor/adminlte/container.edit', compact('container','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'size' => 'numeric|min:0',
                    'qty'  => 'numeric|min:0|not_in:0'
                ]);

                $container = Container::find($id);
                $container->size           = $json->size;
                $container->name           = $json->name;
                $container->qty            = $json->qty;
                $container->updated_user   = $session_id;
                $result = $container->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $container = Container::find($id);
        $status = $container->status;

        if ($status == 1) {
        	$container->status       = 0;
            $container->updated_user = $session_id;
	        $result = $container->save();

        } elseif ($status == 0) {
        	$container->status       = 1;
            $container->updated_user = $session_id;
	        $result = $container->save();
            
        };
        return $this->jsonSuccess( $result );
    }
}
