@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Stuffing</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
				<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Split Proforma <b>{{ $schedulestuff->fkSchedulebook->fkQuotationsplit->no_split }}</b>
				  			&nbsp;<a href="{{ url('stuff') }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a>				  			
				  		</h4>
			  		</div>
				  	
				  	<div class="box-body">
				  		<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="15%">Container</th>
						          	<th width="15%">Quantity</th>
						          	<th width="20%">Vehicle</th>
						          	<th width="35%">Note</th>
						          	<th width="10%">Seal</th>
						          	<th width="5%" style="text-align: center;">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@foreach($stuffs as $stuff)
						    		<tr>
							            <td>
							            	@if($stuff->fkContainer->size == 0)
				                    			{{ $stuff->fkContainer->name }}
										    @else
										    	{{ $stuff->fkContainer->size }}" {{ $stuff->fkContainer->name }}
									    	@endif
							            	
							            	<br>{{$stuff->vehicle}}
							            </td>
							            <td style="text-align: right;">
							            	Qty Bag : {{$stuff->qty_bag}} &nbsp; <br>
							            	Qty Pcs : {{$stuff->qty_pcs}} &nbsp; <br>
							            	Qty Kg : {{$stuff->qty_kg}} &nbsp;&nbsp;
							            </td>
							            <td>
							            	<table>
							            		<tr>
							            			<td>Nopol</td>
							            			<td>&nbsp;: {{$stuff->nopol}}</td>
						            			</tr>
						            			<tr>
							            			<td>Driver</td>
							            			<td>&nbsp;: {{$stuff->driver}}</td>
						            			</tr>
						            			<tr>
							            			<td>Phone</td>
							            			<td>&nbsp;: {{$stuff->mobile}}</td>
						            			</tr>
						            		</table>
							            </td>
							            <td><pre>{{$stuff->notice}}</pre></td>

							            <form method="post" action="{{ route('stuff.store') }}">
								      		@csrf
								      		<input type="hidden" name="stuff" value="{{ $stuff->id }}">
						                  	<td>
						                  		<input type="text" class="form-control" name="seal" value="{{ $stuff->seal }}" required>
						                  	</td>
						                  	<td>
								              	<button type="submit" class="btn btn-sm btn-success">Save</button>
								          	</td>
						                </form>
									</tr>
					    		@endforeach
						    </tbody>

						    <tfoot>
						    	<tr>
						    		<td>Container</td>
						    		<td>Quantity</td>
						    		<td>Vehicle</td>
						    		<td>Note</td>
						    		<td class="noShow"></td>
						    		<td class="noShow"></td>
					    		</tr>
					    	</tfoot>
					    </table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop