@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">New Purchase Request</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('purchaserequest.store') }}">
				          	@csrf
				          	<div class="row">
    							<div class="col-md-6">
    								<div class="form-group">
						          		<label for="date_request">Date</label>
						              	<input type="text" class="form-control datepicker" name="date_request" value="{{ old('date_request') }}" required>
						          	</div>
									<div class="form-group">
						              	<label for="product">Product</label>
						              	<select class="form-control select2" name="product" required>
						              		<option value="{{ old('product') }}">Choose One</option>
					                    	@foreach($products as $product)
											    <option value="{{ $product->id }}">{{ $product->code }} - {{ $product->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="supplierdetail">Supplier</label>
						              	<select class="form-control select2" name="supplierdetail">
						              		<option value="0">All Supplier</option>
					                    	@foreach($supplierdetails as $supplierdetail)
											    <option value="{{ $supplierdetail->id }}">{{ $supplierdetail->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="warehouse">Warehouse</label>
						              	<select class="form-control select2" name="warehouse">
						              		@php ($array = [])
									    	@foreach($employeewarehouses as $employeewarehouse)
									    		@php ($array[] = $employeewarehouse->warehouse)
									    	@endforeach

						              		<option value="0">All Warehouse</option>
					                    	@foreach($warehouses as $warehouse)
					                    		@if(in_array($warehouse->id, $array))
											    	<option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
										    	@endif
											@endforeach
					                	</select>
						          	</div>
								</div>

								<div class="col-md-6">
						          	<div class="form-group">
						          		<label for="qty_request">Qty Request (Kg)</label>
						              	<input type="number" class="form-control" name="qty_request" value="{{ old('qty_request') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="notice">Note</label>
						          		<textarea class="form-control" name="notice" maxlength="150" rows="5"></textarea>
						          	</div>
						          	<div class="form-group">
						          		<label for="button"></label> <br>
						          		<button type="submit" class="btn btn-sm btn-success">Save</button>
						          		<a href="{{ url('/purchaserequest') }}" class="btn btn-sm btn-danger">Cancel</a>
						          	</div>
								</div>
							</div>
						</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop