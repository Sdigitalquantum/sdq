<?php

namespace App\Exports;

use App\Http\Models\Debt;
use Maatwebsite\Excel\Concerns\FromCollection;

class DebtsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Debt::select('no_po','code_supplier','name_supplier','code_product','name_product','date_po','date_qc','qty','price','total','payment','adjustment')->get();
    }
}
