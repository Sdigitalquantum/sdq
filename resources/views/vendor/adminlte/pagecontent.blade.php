<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title_prefix', config('sdq.title_prefix', ''))
    @yield('title', config('sdq.title', 'S.D.Q'))
    @yield('title_postfix', config('sdq.title_postfix', ''))</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">    
</head>
<body class="hold-transition @yield('body_class')">

@yield('content')
@yield('content_js')
</body>
</html>
