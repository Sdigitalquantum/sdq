<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    /**
    * Public Properties
    */
    public $server_message;
    
    // Generate Json data success failed message.
    public function jsonSuccess($rs)
    {   if ($rs)
        {   $result= array("success" => true, "message" => "Succeed"); }
        else
        {
            $result= array("success" => false,
                           "message" => "Failed to save records.",
                           "server_message" => $this->server_message
                        );
        };
        return json_encode($result);
    }
}
