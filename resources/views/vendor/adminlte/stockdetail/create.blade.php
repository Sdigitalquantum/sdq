@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>Quality Control</a></li>
            <li class="active"><a href="#tab_detail" data-toggle="tab">Detail</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_detail">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Detail Quality Control &nbsp;<a href="{{ url('/stockqc/create', $stockqc->purchasedetail) }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4>
			    	</div>
			    	
				  	<div class="box-body">
				      		<div class="row">
    							<div class="col-md-6">
    								<div class="form-group">
						              	<label for="no_po">No. PO</label>
						              	<input type="text" class="form-control" name="no_po" value="{{ $stockqc->no_po }}" disabled>
						          	</div>
									<div class="form-group">
						              	<label for="product">Product</label>
						              	<select class="form-control select2" name="product" disabled>
						              		@foreach($products as $product)
											    @if ($stockqc->product == $product->id)
												    <option value="{{ $product->id }}" selected>{{ $product->code }} - {{ $product->name }}</option>
												@else
												    <option value="{{ $product->id }}">{{ $product->code }} - {{ $product->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="warehouse">Warehouse</label>
						              	<select class="form-control select2" name="warehouse" disabled>
						              		@foreach($warehouses as $warehouse)
											    @if ($stockqc->warehouse == $warehouse->id)
												    <option value="{{ $warehouse->id }}" selected>{{ $warehouse->code }} - {{ $warehouse->name }}</option>
												@else
												    <option value="{{ $warehouse->id }}">{{ $warehouse->code }} - {{ $warehouse->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="supplierdetail">Supplier</label>
						              	<select class="form-control select2" name="supplierdetail" disabled>
						              		@foreach($supplierdetails as $supplierdetail)
											    @if ($stockqc->supplierdetail == $supplierdetail->id)
												    <option value="{{ $supplierdetail->id }}" selected>{{ $supplierdetail->name }}</option>
												@else
												    <option value="{{ $supplierdetail->id }}">{{ $supplierdetail->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="date_in">Date</label>
						              	<input type="text" class="form-control datepicker" name="date_in" value="{{ $stockqc->date_in }}" disabled>
						          	</div>
								</div>

						<form method="post" action="{{ route('stockdetail.store') }}">
				      		@csrf
								<div class="col-md-6">
									<div class="form-group">
						              	<label for="no_bag">No. Bag</label>
						              	<input type="hidden" name="stockqc" value="{{ $stockqc->id }}">
						              	<input type="hidden" name="no_inc" value="{{ $no_inc }}">
						              	<input type="hidden" name="no_bag" value="{{ $no_bag }}">
						              	<input type="text" class="form-control" name="bag" value="{{ $no_bag }}" disabled>
						          	</div>
									<div class="form-group">
						              	<label for="qty_bag">Qty Bag</label>
						              	<input type="number" class="form-control" name="qty_bag" value="{{ old('qty_bag') }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="qty_pcs">Qty Pcs</label>
						              	<input type="number" class="form-control" name="qty_pcs" value="{{ old('qty_pcs') }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="qty_kg">Qty Kg</label>
						              	<input type="number" class="form-control" name="qty_kg" value="{{ old('qty_kg') }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="button">&nbsp;</label> <br>
						              	<button type="submit" class="btn btn-sm btn-success">Save</button>
						          	</div>
								</div>
							</div>
						</form>

						<br><table class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #001F3F !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="30%">No. Bag</th>
						          	<th width="10%">Qty Bag</th>
						          	<th width="10%">Qty Pcs</th>
						          	<th width="10%">Qty Kg</th>
						          	<th width="25%">Barcode</th>
						          	<th width="10%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($tot_bag = 0)
						    	@php ($tot_pcs = 0)
						    	@php ($tot_kg = 0)
						    	@foreach($stockdetails as $stockdetail)
						    		<tr>
						    			<td>{{$stockdetail->no_bag}}</td>
						    			<td style="text-align: right;">{{number_format($stockdetail->qty_bag, 0, ',' , '.')}} &nbsp;</td>
						    			<td style="text-align: right;">{{number_format($stockdetail->qty_pcs, 0, ',' , '.')}} &nbsp;</td>
						    			<td style="text-align: right;">{{number_format($stockdetail->qty_kg, 0, ',' , '.')}} &nbsp;</td>
						    			<td>{{$stockdetail->barcode}}</td>
						    			<td>
						    				<a href="{{ route('stockdetail.edit',$stockdetail->id)}}" class="btn btn-xs btn-warning">Edit</a>&nbsp;

						    				<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$stockdetail->id}}">Delete</button></a>

						            		<form action="{{ route('stockdetail.destroy', $stockdetail->id)}}" method="post">
							                  	@csrf
							                  	@method('DELETE')
							                  	<div class="modal fade bs-example-modal{{$stockdetail->id}}" tabindex="-1" role="dialog" aria-hidden="true">
				                                    <div class="modal-dialog modal-sm">
				                                        <div class="modal-content">
				                                            <div class="modal-header btn-danger">
				                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
				                                                </button>
				                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
				                                            </div>
				                                            <div class="modal-body">
				                                                Are you sure to delete this data?
				                                            </div>
				                                            <div class="modal-footer">
				                                                <button type="submit" class="btn btn-success">Yes</button>
				                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
				                                            </div>
				                                        </div>
				                                    </div>
				                                </div>
							                </form>
						    			</td>
					    			</tr>

					    			@php ($tot_bag = $tot_bag + $stockdetail->qty_bag)
							    	@php ($tot_pcs = $tot_pcs + $stockdetail->qty_pcs)
							    	@php ($tot_kg = $tot_kg + $stockdetail->qty_kg)
						    	@endforeach
						    </tbody>
						    	<tr>
						    		<td style="text-align: right; font-weight: bold;">Total Qty &nbsp;</td>
						    		<td style="text-align: right;">{{number_format($tot_bag, 0, ',' , '.')}} &nbsp;</td>
					    			<td style="text-align: right;">{{number_format($tot_pcs, 0, ',' , '.')}} &nbsp;</td>
					    			<td style="text-align: right;">{{number_format($tot_kg, 0, ',' , '.')}} &nbsp;</td>
					    			<td colspan="2">&nbsp;</td>
					    		</tr>
					  	</table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop