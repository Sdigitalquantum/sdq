@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Debt Card &nbsp;<a href="{{ url('debtcard')}}" class="btn btn-xs btn-danger" style="text-decoration: none;">Back</a></h4>
  		</div>
	  	
	  	<div class="box-body">
	  		<form method="post" action="{{ url('debtfilter') }}">
	          	@csrf
	          	<div class="row">
					<div class="col-md-6">
						<div class="form-group">
			              	<label for="supplierdetail">Supplier</label>
			              	<select class="form-control select2" name="supplierdetail" required>
			              		<option value="{{ old('supplierdetail') }}">Choose One</option>
		                    	@foreach($supplierdetails as $supplierdetail)
								    <option value="{{ $supplierdetail->id }}">{{ $supplierdetail->fkSupplier->code }} - {{ $supplierdetail->name }}</option>
								@endforeach
		                	</select>
			          	</div>
			          	<div class="form-group">
			              	<label for="year">Year</label>
			              	<input type="number" class="form-control" name="year" value="{{ old('year') }}" required>
			          	</div>
			          	<div class="form-group">
			              	<label for="month">Month</label>
			              	<select class="form-control select2" name="month" required>
			              		<option value="{{ old('month') }}">Choose One</option>
			              		<option value="1">January</option>
			              		<option value="2">February</option>
			              		<option value="3">March</option>
			              		<option value="4">April</option>
			              		<option value="5">May</option>
			              		<option value="6">June</option>
			              		<option value="7">July</option>
			              		<option value="8">August</option>
			              		<option value="9">September</option>
			              		<option value="10">October</option>
			              		<option value="11">November</option>
			              		<option value="12">Desember</option>
		                	</select>
			          	</div>
			          	<div class="form-group">
			              	<button type="submit" class="btn btn-sm btn-success">Filter</button>
			          	</div>
					</div>

					<div class="col-md-6">
						Supplier : 
							{{ $supplier->name }} <br>
						Periode : 
							@if($month == 1) January
							@elseif($month == 2) February
							@elseif($month == 3) March
							@elseif($month == 4) April
							@elseif($month == 5) May
							@elseif($month == 6) June
							@elseif($month == 7) July
							@elseif($month == 8) August
							@elseif($month == 9) September
							@elseif($month == 10) October
							@elseif($month == 11) November
							@elseif($month == 12) Desember
							@endif
							{{ $year }} <br><br>
						Beginning Balance : 
							@if(!empty($debtold))
								{{number_format($debtold->debit-$debtold->kredit, 0, ',' , '.')}}
							@else 0
							@endif <br><br>
						Total In &nbsp;&nbsp;&nbsp;: 
							@if(!empty($debtcard))
								{{number_format($debtcard->debit, 0, ',' , '.')}}
							@else 0
							@endif <br><br>
						Total Out :
							@if(!empty($debtcard))
								{{number_format($debtcard->kredit, 0, ',' , '.')}}
							@else 0
							@endif <br><br>
						Ending Balance &nbsp;&nbsp;&nbsp;&nbsp; : 
							@if(!empty($debtcard))
								@if(!empty($debtold))
									{{number_format($debtcard->debit-$debtcard->kredit-($debtold->debit-$debtold->kredit), 0, ',' , '.')}}
								@else
									{{number_format($debtcard->debit-$debtcard->kredit, 0, ',' , '.')}}
								@endif
							@else 0
							@endif <br><br>
					</div>
				</div>
	      	</form>

		  	<table class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="15%">Date</th>
			          	<th width="30%">No. PO</th>
			          	<th width="25%">Total</th>
			          	<th width="20%">Payment</th>
			          	<th width="20%">Saldo</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@foreach($debts as $debt)
			        	<tr>
				            <td>{{$debt->date_po}}</td>
				            <td>{{$debt->no_po}}</td>
				            <td style="text-align: right;">{{number_format($debt->total, 0, ',' , '.')}} &nbsp;</td>
				            <td style="text-align: right;">{{number_format($debt->payment, 0, ',' , '.')}} &nbsp;</td>
				            <td style="text-align: right;">{{number_format($debt->total-$debt->payment, 0, ',' , '.')}} &nbsp;</td>
				        </tr>
			        @endforeach
			    </tbody>
		  	</table>
	  	</div>
	<div>
@stop