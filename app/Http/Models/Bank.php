<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $fillable = [
	    'no_inc',
	    'code',
	    'name',
	    'alias',
	    'created_user',
	    'updated_user'
  	];
}
