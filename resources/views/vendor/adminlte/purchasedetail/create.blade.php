@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
          	<li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="active"><a href="#tab_detail" data-toggle="tab">Detail</a></li>
            <li class="disabled"><a>Request</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_detail">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Detail Purchase Order &nbsp;<a href="{{ url('purchaseorder') }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('purchasedetail.store') }}">
				          	@csrf
				          	<div class="row">
    							<div class="col-md-6">
    								<div class="form-group">
						              	<label for="no_po">No. PO</label>
						              	<input type="hidden" name="purchaseorder" value="{{ $purchaseorder->id }}">
						              	<input type="text" class="form-control" name="no_po" value="{{ $purchaseorder->no_po }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="product">Product</label>
						              	<select class="form-control select2" name="product" required>
						              		<option value="{{ old('product') }}">Choose One</option>
					                    	@foreach($products as $product)
											    <option value="{{ $product->id }}">{{ $product->code }} - {{ $product->name }}</option>
											@endforeach
					                	</select>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						              	<label for="supplierdetail">Supplier</label>
						              	@if($supplier == 0)
							              	<select class="form-control select2" name="supplierdetail" required>
							              		<option value="{{ old('supplierdetail') }}">Choose One</option>
						                    	@foreach($supplierdetails as $supplierdetail)
												    <option value="{{ $supplierdetail->id }}">{{ $supplierdetail->name }}</option>
												@endforeach
						                	</select>
					                	@else
					                		<input type="hidden" name="supplierdetail" value="{{ $supplierdetails->id }}">
					                		<input type="text" class="form-control" value="{{ $supplierdetails->name }}" disabled>
					                	@endif
						          	</div>
						          	<div class="form-group">
						              	<label for="warehouse">Warehouse Destination</label>
						              	<select class="form-control select2" name="warehouse" required>
						              		@php ($array = [])
									    	@foreach($employeewarehouses as $employeewarehouse)
									    		@php ($array[] = $employeewarehouse->warehouse)
									    	@endforeach

						              		<option value="{{ old('warehouse') }}">Choose One</option>
					                    	@foreach($warehouses as $warehouse)
					                    		@if(in_array($warehouse->id, $array))
											    	<option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
										    	@endif
											@endforeach
					                	</select>
						          	</div>
								</div>
							</div>
				          	
				          	<button type="submit" class="btn btn-sm btn-success">Request</button>
						</form>

						<br><table class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #001F3F !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="20%">Product</th>
						          	<th width="20%">Supplier</th>
						          	<th width="20%">Warehouse Destination</th>
						          	<th width="10%">Qty (Kg)</th>
						          	<th width="10%">Price</th>
						          	<th width="10%">Total</th>
						          	<th width="10%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($total = 0)
						    	@foreach($purchasedetails as $purchasedetail)
						    		<tr>
							            <td>{{$purchasedetail->fkProduct->name}}</td>
							            <td>{{$purchasedetail->fkSupplierdetail->name}}</td>
							            <td>{{$purchasedetail->fkWarehouse->name}}</td>
							            <td style="text-align: right;">{{number_format($purchasedetail->qty_order+$purchasedetail->qty_buffer, 0, ',' , '.')}} &nbsp;</td>
							            <td style="text-align: right;">Rp. {{number_format($purchasedetail->price, 0, ',' , '.')}} &nbsp;</td>
							            <td style="text-align: right;">Rp. {{number_format(($purchasedetail->qty_order+$purchasedetail->qty_buffer)*$purchasedetail->price, 0, ',' , '.')}} &nbsp;</td>

							            @if($purchasedetail->status == 1)
								            <td>
								            	<a href="{{ route('purchasedetail.edit',$purchasedetail->id)}}" class="btn btn-xs btn-warning">Edit</a>&nbsp;

								            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$purchasedetail->id}}">Delete</button></a>

							            		<form action="{{ route('purchasedetail.destroy', $purchasedetail->id)}}" method="post">
								                  	@csrf
								                  	@method('DELETE')
								                  	<div class="modal fade bs-example-modal{{$purchasedetail->id}}" tabindex="-1" role="dialog" aria-hidden="true">
					                                    <div class="modal-dialog modal-sm">
					                                        <div class="modal-content">
					                                            <div class="modal-header btn-danger">
					                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
					                                                </button>
					                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
					                                            </div>
					                                            <div class="modal-body">
					                                                Are you sure to delete this data?
					                                            </div>
					                                            <div class="modal-footer">
					                                                <button type="submit" class="btn btn-success">Yes</button>
					                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
					                                            </div>
					                                        </div>
					                                    </div>
					                                </div>
								                </form>
								            </td>
							            @else
							            	<td>
							            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$purchasedetail->id}}">Activated</button></a>

							            		<form action="{{ route('purchasedetail.destroy', $purchasedetail->id)}}" method="post">
								                  	@csrf
								                  	@method('DELETE')
								                  	<div class="modal fade bs-example-modal{{$purchasedetail->id}}" tabindex="-1" role="dialog" aria-hidden="true">
					                                    <div class="modal-dialog modal-sm">
					                                        <div class="modal-content">
					                                            <div class="modal-header btn-success">
					                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
					                                                </button>
					                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
					                                            </div>
					                                            <div class="modal-body">
					                                                Are you sure to active this data?
					                                            </div>
					                                            <div class="modal-footer">
					                                                <button type="submit" class="btn btn-success">Yes</button>
					                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
					                                            </div>
					                                        </div>
					                                    </div>
					                                </div>
								                </form>
							            	</td>
							            @endif

							            @php ($total = $total + ($purchasedetail->qty_order+$purchasedetail->qty_buffer)*$purchasedetail->price)
							        </tr>
						        @endforeach
						        	<tr>
						        		<td colspan="5" style="text-align: right; font-weight: bold;">Grand Total &nbsp;</td>
						        		<td style="text-align: right;">Rp. {{number_format($total, 0, ',' , '.')}} &nbsp;</td>
						        		<td>&nbsp;</td>
					        		</tr>
						    </tbody>
					  	</table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop