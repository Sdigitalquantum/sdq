<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Supplieraccount;
use App\Http\Models\Currency;
use App\Http\Models\Bank;
use App\Http\Models\Employeeset;

class SupplieraccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $supplieraccount = new Supplieraccount([
                    'supplier'      => $json->supplier,
                    'currency'      => $json->currency,
                    'bank'          => $json->bank,
                    'account_no'    => $json->account_no,
                    'account_swift' => !empty($json->account_swift) ? $json->account_swift : '-',
                    'account_name'  => $json->account_name,
                    'alias'         => !empty($json->alias) ? $json->alias : '',
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $supplieraccount->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $supplieraccounts = Supplieraccount::where('supplier', $id)->orderBy('created_at','desc')->get();
        $currencys = Currency::where('status', 1)->get();
        $banks = Bank::where('status', 1)->get();

        return view('vendor/adminlte/supplieraccount.show', compact('id','supplieraccounts','currencys','banks','disable'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $supplieraccount = Supplieraccount::find($id);
        $supplieraccounts = Supplieraccount::where('supplier', $supplieraccount->supplier)->orderBy('created_at','desc')->get();
        $currencys = Currency::where('status', 1)->get();
        $banks = Bank::where('status', 1)->get();

        return view('vendor/adminlte/supplieraccount.edit', compact('supplieraccount','supplieraccounts','currencys','banks','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $supplieraccount = Supplieraccount::find($id);
                $supplieraccount->supplier      = $json->supplier;
                $supplieraccount->currency      = $json->currency;
                $supplieraccount->bank          = $json->bank;
                $supplieraccount->account_no    = $json->account_no;
                $supplieraccount->account_swift = !empty($json->account_swift) ? $json->account_swift : '-';
                $supplieraccount->account_name  = $json->account_name;
                $supplieraccount->alias         = !empty($json->alias) ? $json->alias : '';
                $supplieraccount->updated_user  = $session_id;
                $result = $supplieraccount->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $supplieraccount = Supplieraccount::find($id);
        $status = $supplieraccount->status;

        if ($status == 1) {
        	$supplieraccount->status       = 0;
            $supplieraccount->updated_user = $session_id;
	        $result = $supplieraccount->save();

        } elseif ($status == 0) {
        	$supplieraccount->status       = 1;
            $supplieraccount->updated_user = $session_id;
	        $result = $supplieraccount->save();

        };
        return $this->jsonSuccess( $result );
    }
}
