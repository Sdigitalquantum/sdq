@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>Plan</a></li>
            <li class="active"><a href="#tab_booking" data-toggle="tab">Booking</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_booking">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Booking Order &nbsp;<a href="{{ url('plan')}}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('purchaserequest.store') }}">
				          	@csrf
				          	<input type="hidden" name="quotationdetail" value="{{ $id }}">
				          	<div class="row">
    							<div class="col-md-3">
    								<div class="form-group">
						          		<label for="no_reference">Quotation</label>
						              	<input type="text" class="form-control" name="no_reference" value="{{ $quotationdetail->fkQuotation->no_sp }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="product">Product</label>
						              	@if(!empty($quotationdetail->alias_name))
						              		<input type="text" class="form-control" name="product" value="{{ $quotationdetail->alias_name }}" disabled>
						              	@else
						              		<input type="text" class="form-control" name="product" value="{{ $quotationdetail->fkProduct->name }}" disabled>
						              	@endif
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_amount">Qty Remained (Kg)</label>
						              	<input type="number" class="form-control" name="qty_amount" value="{{ $quotationdetail->qty_amount-$quotationdetail->qty_book }}" disabled>
						          	</div>
						          	<div class="form-group">
						          		<label for="date_request">Date</label>
						              	<input type="text" class="form-control datepicker" name="date_request" value="{{ old('date_request') }}" required>
						          	</div>
    								<div class="form-group">
						              	<label for="supplierdetail">Supplier</label>
						              	<select class="form-control select2" name="supplierdetail">
						              		<option value="0">All Supplier</option>
					                    	@foreach($supplierdetails as $supplierdetail)
											    <option value="{{ $supplierdetail->id }}">{{ $supplierdetail->name }}</option>
											@endforeach
					                	</select>
						          	</div>
    								<div class="form-group">
						              	<label for="warehouse">Warehouse</label>
						              	<select class="form-control select2" name="warehouse">
						              		@php ($array = [])
									    	@foreach($employeewarehouses as $employeewarehouse)
									    		@php ($array[] = $employeewarehouse->warehouse)
									    	@endforeach

						              		<option value="0">All Warehouse</option>
					                    	@foreach($warehouses as $warehouse)
					                    		@if(in_array($warehouse->id, $array))
											    	<option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
										    	@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_request">Qty Request (Kg)</label>
						              	<input type="number" class="form-control" name="qty_request" value="{{ $quotationdetail->qty_amount-$quotationdetail->qty_book }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="notice">Note</label>
						          		<textarea class="form-control" name="notice" maxlength="150" rows="3"></textarea>
						          	</div>
						          	<div class="form-group">
						          		<button type="submit" class="btn btn-sm btn-success">Save</button>
						          	</div>
								</div>
						</form>

								<div class="col-md-9">
									<table class="table table-bordered table-hover nowrap" width="100%">
									    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
									        <tr>
									          	<th width="15%" style="text-align: center;">No. PR</th>
									          	<th width="10%" style="text-align: center;">Date</th>
									          	<th width="25%">Information</th>
									          	<th width="25%">Note</th>
									          	<th width="10%" style="text-align: center;">Qty (Kg)</th>
									          	<th width="10%">Action</th>
									        </tr>
									    </thead>

									    <tbody>
									    	@php ($total = 0)
									    	@foreach($purchaserequests as $purchaserequest)
									    		<tr>
										            <td style="text-align: center;">{{$purchaserequest->no_pr}}</td>
										            <td style="text-align: center;">{{$purchaserequest->date_request}}</td>
										            <td>
										            	@if(empty($purchaserequest->fkQuotationdetail->alias_name))
										            		{{$purchaserequest->fkQuotationdetail->fkProduct->name}}
									            		@else
									            			{{$purchaserequest->fkQuotationdetail->alias_name}}
									            		@endif
									            		<br>
										            	@if($purchaserequest->supplierdetail == 0)
										            		All Supplier
										            	@else
										            		{{$purchaserequest->fkSupplierdetail->name}}
									            		@endif
								            			<br>
									            		@if($purchaserequest->warehouse == 0)
										            		All Warehouse
										            	@else
										            		{{$purchaserequest->fkWarehouse->name}}
									            		@endif
										            </td>
										            <td><pre>{{$purchaserequest->notice}}</pre></td>
										            <td style="text-align: right;">{{number_format($purchaserequest->qty_request, 0, ',' , '.')}} &nbsp;</td>
										            <td>
										            	@if($purchaserequest->status_release == 1)
										            		<font color="blue">Released <i class="glyphicon glyphicon-ok"></i></font>
										            	@else
										            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal-rl{{$purchaserequest->id}}">Release</button> <br><br>
															<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$purchaserequest->id}}">Delete</button></a>

															<form action="{{ url('plan/release', $purchaserequest->id) }}" method="post">
											                  	@csrf
											                  	@method('DELETE')
											                  	<div class="modal fade bs-example-modal-rl{{$purchaserequest->id}}" tabindex="-1" role="dialog" aria-hidden="true">
								                                    <div class="modal-dialog modal-sm">
								                                        <div class="modal-content">
								                                            <div class="modal-header btn-success">
								                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
								                                                </button>
								                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Release</h4>
								                                            </div>
								                                            <div class="modal-body">
								                                                Are you sure to release this data?
								                                            </div>
								                                            <div class="modal-footer">
								                                                <button type="submit" class="btn btn-success">Yes</button>
								                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
								                                            </div>
								                                        </div>
								                                    </div>
								                                </div>
											                </form>

											            	<form action="{{ url('plan/book', $purchaserequest->id)}}" method="post">
											                  	@csrf
											                  	@method('DELETE')
											                  	<div class="modal fade bs-example-modal{{$purchaserequest->id}}" tabindex="-1" role="dialog" aria-hidden="true">
								                                    <div class="modal-dialog modal-sm">
								                                        <div class="modal-content">
								                                            <div class="modal-header btn-danger">
								                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
								                                                </button>
								                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
								                                            </div>
								                                            <div class="modal-body">
								                                                Are you sure to delete this data?
								                                            </div>
								                                            <div class="modal-footer">
								                                                <button type="submit" class="btn btn-success">Yes</button>
								                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
								                                            </div>
								                                        </div>
								                                    </div>
								                                </div>
											                </form>
										                @endif
													</td>
										        </tr>
										        @php ($total = $total + $purchaserequest->qty_request)
									        @endforeach
									        <tr>
									        	<td colspan="4" style="text-align: right; font-weight: bold;">Total Qty</td>
									        	<td style="text-align: right; font-weight: bold;">{{$total}} &nbsp;</td>
									        	<td>&nbsp;</td>
								        	</tr>
									    </tbody>
								  	</table>
								</div>
							</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop