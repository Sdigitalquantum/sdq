@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">User</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">User &nbsp;<a href="{{ url('employeewarehouse')}}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<div class="row">
							<div class="col-md-6">
								<table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="10%" style="text-align: center;">No</th>
								          	<th width="40%">User</th>
								          	<th width="30%">Email</th>
								          	<th width="20%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@php($no = 0)
								    	@foreach($employees as $employee)
								    		@php($no++)
								        	<tr>
								        		<td style="text-align: center;">{{$no}}</td>
									            <td>{{$employee->name}}</td>
									            <td>{{$employee->email}}</td>

									            <td>
									            	<form method="post" action="{{ route('employeewarehouse.update', $warehouse->id) }}">
											      		@method('PATCH')
											      		@csrf
											      		<input type="hidden" name="warehouse" value="{{$warehouse->id}}">
											      		<input type="hidden" name="employee" value="{{$employee->id}}">
											          	<button type="submit" class="btn btn-xs btn-success">Choose</button>
											      	</form>
									            </td>
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
							</div>

							<div class="col-md-6">
								<table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #00a65a !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="30%">Warehouse</th>
								          	<th width="30%">User</th>
								          	<th width="30%">Email</th>
								          	<th width="10%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($employeewarehouses as $employeewarehouse)
								    		@php($no++)
								        	<tr>
								        		<td>{{$employeewarehouse->fkWarehouse->name}}</td>
								        		<td>{{$employeewarehouse->fkEmployee->name}}</td>
								        		<td>{{$employeewarehouse->fkEmployee->email}}</td>

									            @if($employeewarehouse->status == 1)
										            <td>
										            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$employeewarehouse->id}}">Delete</button>

										            	<form action="{{ route('employeewarehouse.destroy', $employeewarehouse->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$employeewarehouse->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-danger">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to delete this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
										            </td>
									            @else
									            	<td>
									            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$employeewarehouse->id}}">Activated</button>

									            		<form action="{{ route('employeewarehouse.destroy', $employeewarehouse->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$employeewarehouse->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-success">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to active this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
									            	</td>
									            @endif
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
							</div>
						</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop