<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Employeeset extends Model
{
    protected $fillable = [
	    'employee',
	    'menuroot',
	    'nomorroot',
	    'menu',
	    'nomormenu',
	    'menusub',
	    'nomorsub',
	    'created_user'
  	];

  	public function fkEmployee(){
	    return $this->belongsTo('\App\Http\Models\Employee', 'employee', 'id');
	}

  	public function fkMenuroot(){
	    return $this->belongsTo('\App\Http\Models\Menuroot', 'menuroot', 'id');
	}

  	public function fkMenu(){
	    return $this->belongsTo('\App\Http\Models\Menu', 'menu', 'id');
	}

  	public function fkMenusub(){
	    return $this->belongsTo('\App\Http\Models\Menusub', 'menusub', 'id');
	}
}
