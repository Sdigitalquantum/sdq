@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>Weighing</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
				<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Weighing - Shipping</h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="15%">Split Proforma</th>
						          	<th width="15%">Port</th>
						          	<th width="10%">Date</th>
						          	<th width="15%">Stuffing</th>
						          	<th width="30%">Note</th>
						          	<th width="10%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($schedulestuffs as $schedulestuff)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>{{$schedulestuff->fkSchedulebook->fkQuotationsplit->no_split}}</td>
							            <td>
							            	POL : {{$schedulestuff->fkSchedulebook->pol}} <br>
							            	POD : {{$schedulestuff->fkSchedulebook->pod}}
							            </td>
							            <td>
							            	ETD : {{$schedulestuff->fkSchedulebook->etd}} <br>
							            	ETA : {{$schedulestuff->fkSchedulebook->eta}}
							            </td>
							            <td>
							            	Date : {{$schedulestuff->date}} <br>
							            	PIC &nbsp;: {{$schedulestuff->person}} <br>
							            	Qty Fcl : {{$schedulestuff->qty_approve}} Container
							            </td>
							            <td>
							            	sdq &nbsp;&nbsp;&nbsp;: {{$schedulestuff->notice}} <br>
							            	EMKL : {{$schedulestuff->reason}}
							            </td>
										<td>
											@if($schedulestuff->status_reroute == 1)
												<font color="red">Re-Route <i class="glyphicon glyphicon-remove"></i></font>
											@elseif($schedulestuff->status_reschedule == 1)
												<font color="red">Canceled <i class="glyphicon glyphicon-remove"></i></font>
											@else
												<a href="{{ route('weigh.edit',$schedulestuff->id)}}" class="btn btn-xs btn-success">Input Weigh</a>
				            				@endif
										</td>
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Split Proforma</td>
						          	<td>Port</td>
						          	<td>Date</td>
						          	<td>Stuffing</td>
						          	<td>Note</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop