@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
          	<li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
            <li class="active"><a href="#tab_request" data-toggle="tab">Request</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_request">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Purchase Request &nbsp;<a href="{{ url('purchasedetail/create', $purchasedetail->purchaseorder) }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<div class="row">
							<div class="col-md-3">
								<table class="table table-bordered table-hover nowrap" width="100%">
								    <tr style="background: #001F3F !important; font-weight: bold; text-align: center; color: white;">
							          	<th colspan="3">No. PO : {{ $purchasedetail->fkPurchaseorder->no_po }}</th>
							        </tr>

							        @php ($no = 0)
							        @foreach($purchasedetailprs as $purchasedetailpr)
							        	@php ($no++)
							        	<tr>
								            <td style="text-align: center;">{{$no}}</td>
								            <td>
								            	@if($purchasedetailpr->purchaserequest != 0)
								            		{{$purchasedetailpr->fkPurchaserequest->no_pr}}
								            	@else
								            		{{$purchasedetailpr->fkPurchasedetailprg->fkPurchasegeneral->no_prg}}
								            	@endif
								            </td>
								            <td>
								            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$purchasedetailpr->id}}">Delete</button>

								            	<form action="{{ route('purchasedetailpr.destroy', $purchasedetailpr->id)}}" method="post">
								                  	@csrf
								                  	@method('DELETE')
								                  	<div class="modal fade bs-example-modal{{$purchasedetailpr->id}}" tabindex="-1" role="dialog" aria-hidden="true">
					                                    <div class="modal-dialog modal-sm">
					                                        <div class="modal-content">
					                                            <div class="modal-header btn-danger">
					                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
					                                                </button>
					                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
					                                            </div>
					                                            <div class="modal-body">
					                                                Are you sure to delete this data?
					                                            </div>
					                                            <div class="modal-footer">
					                                                <button type="submit" class="btn btn-success">Yes</button>
					                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
					                                            </div>
					                                        </div>
					                                    </div>
					                                </div>
								                </form>
								            </td>
								        </tr>
							        @endforeach
							  	</table>

							  	<form method="post" action="{{ route('purchasedetail.update', $purchasedetail->id) }}">
						      		@method('PATCH')
						      		@csrf
						      		<div class="form-group">
						          		<label for="qty_order">Qty PR (Kg)</label>
						          		<input type="hidden" class="form-control" name="qty_order" value="{{ $purchasedetail->qty_order }}">
						              	<input type="number" class="form-control" name="qty_request" value="{{ $purchasedetail->qty_order }}" disabled>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_buffer">Qty Buffer (Kg)</label>
						              	<input type="number" class="form-control" name="qty_buffer" value="{{ $purchasedetail->qty_buffer }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="price">Price @Kg</label>
						              	<input type="number" class="form-control" name="price" value="{{ $purchasedetail->price }}" required>
						          	</div>
						          	<div class="form-group">
						          		<button type="submit" class="btn btn-sm btn-success">Save</button>
						          	</div>
					      		</form>
							</div>

							<div class="col-md-9">
								<table id="example" class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="5%">No</th>
								          	<th width="20%" style="text-align: center;">No. PR</th>
								          	<th width="15%">Date</th>
								          	<th width="30%">Product</th>
								          	<th width="10%" style="text-align: center;">Qty (Kg)</th>
								          	<th width="10%" style="text-align: center;">Qty PR (Kg)</th>
								          	<th width="5%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@php ($array = [])
								    	@foreach($employeewarehouses as $employeewarehouse)
								    		@php ($array[] = $employeewarehouse->warehouse)
								    	@endforeach

								    	@php ($no = 0)
								        @foreach($purchaserequests as $purchaserequest)
								        	@if(in_array($purchaserequest->warehouse, $array) || $purchaserequest->warehouse == 0)
									        	@php ($no++)
									        	<tr>
										            <td style="text-align: center;">{{$no}}</td>
										            <td>{{$purchaserequest->no_pr}}</td>
										            <td>{{$purchaserequest->date_request}}</td>
										            <td>{{$purchaserequest->fkProduct->name}}</td>
										            <td style="text-align: right;">{{$purchaserequest->qty_remain}} &nbsp;</td>
										            
									            	<form method="post" action="{{ route('purchasedetailpr.update', $purchaserequest->id) }}">
											      		@method('PATCH')
											      		@csrf
											      		<input type="hidden" name="purchasedetail" value="{{ $purchasedetail->id }}">
											      		<input type="hidden" name="purchaserequest" value="{{ $purchaserequest->id }}">
											      		<td>
											      			<input type="number" class="form-control" name="qty_remain" value="{{ old('qty_remain') }}" required>
											      		</td>
											      		<td>
											      			<button type="submit" class="btn btn-xs btn-success">Choose</button>
											      		</td>
								            		</form>
										        </tr>
									        @endif
								        @endforeach

								        @foreach($purchasedetailprgs as $purchasedetailprg)
								        	@if(in_array($purchasedetailprg->warehouse, $array) || $purchasedetailprg->warehouse == 0)
									        	@php ($no++)
									        	<tr>
										            <td style="text-align: center;">{{$no}}</td>
										            <td>{{$purchasedetailprg->fkPurchasegeneral->no_prg}}</td>
										            <td>{{$purchasedetailprg->fkPurchasegeneral->date_order}}</td>
										            <td>{{$purchasedetailprg->fkProduct->name}}</td>
										            <td style="text-align: right;">{{$purchasedetailprg->qty_remain}} &nbsp;</td>
										            
									            	<form method="post" action="{{ route('purchasedetailpr.update', $purchasedetailprg->id) }}">
											      		@method('PATCH')
											      		@csrf
											      		<input type="hidden" name="purchasedetail" value="{{ $purchasedetail->id }}">
											      		<input type="hidden" name="purchasedetailprg" value="{{ $purchasedetailprg->id }}">
											      		<td>
											      			<input type="number" class="form-control" name="qty_remain" value="{{ old('qty_remain') }}" required>
											      		</td>
											      		<td>
											      			<button type="submit" class="btn btn-xs btn-success">Choose</button>
											      		</td>
								            		</form>
										        </tr>
									        @endif
								        @endforeach
								    </tbody>

								    <tfoot>
								        <tr>
								          	<td class="noShow"></td>
								          	<td>No. PR</td>
								          	<td>Date</td>
								          	<td>Product</td>
								          	<td>Qty (Kg)</td>
								          	<td>Qty PR (Kg)</td>
								          	<td class="noShow"></td>
								        </tr>
								    </tfoot>
							  	</table>
							</div>
						</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop