<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Account;
use App\Http\Models\Accountgroup;
use App\Http\Models\Accounttype;
use App\Http\Models\Currency;
use App\Http\Models\Employeeset;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $accounts = Account::orderBy('code','Asc')->get();

        return view('vendor/adminlte/account.index', compact('accounts','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $groups = Accountgroup::where('status', 1)->get();
        $types  = Accounttype::where('status', 1)->get();
        $currencys = Currency::where('status', 1)->get();

        return view('vendor/adminlte/account.create', compact('groups','types','currencys', 'disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	$json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'code' => 'unique:accounts'
                ]);

                $account = new Account([
                    'currency'      => $json->currency,
                    'group'         => $json->group,
                    'type'          => $json->type,
                    'code'          => $json->code,
                    'name'          => $json->name,
                    'level1'        => !empty($json->level1) ? $json->level1 : 0,
                    'level2'        => !empty($json->level2) ? $json->level2 : 0,
                    'level3'        => !empty($json->level3) ? $json->level3 : 0,
                    'level4'        => !empty($json->level4) ? $json->level4 : 0,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $account->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $account = Account::find($id);
        $groups  = Accountgroup::where('status', 1)->get();
        $types   = Accounttype::where('status', 1)->get();
        $currencys = Currency::where('status', 1)->get();

        return view('vendor/adminlte/account.edit', compact('account','groups','types','currencys','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $account = Account::find($id);
                $code = $account->code;

                if ($code != $request->get('code')) {
                    $request->validate([
                        'code' => 'unique:accounts'
                    ]);
                }
                
                $account->currency      = $json->currency;
                $account->group         = $json->group;
                $account->type          = $json->type;
                $account->code          = $json->code;
                $account->name          = $json->name;
                $account->level1        = !empty($json->level1) ? $json->level1 : 0;
                $account->level2        = !empty($json->level2) ? $json->level2 : 0;
                $account->level3        = !empty($json->level3) ? $json->level3 : 0;
                $account->level4        = !empty($json->level4) ? $json->level4 : 0;
                $account->updated_user  = $session_id;
                $result = $account->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $account = Account::find($id);
        $status = $account->status;

        if ($status == 1) {
        	$account->status       = 0;
            $account->updated_user = $session_id;
	        $result = $account->save();

        } elseif ($status == 0) {
        	$account->status       = 1;
            $account->updated_user = $session_id;
	        $result = $account->save();

        };
        return $this->jsonSuccess( $result );
    }
}
