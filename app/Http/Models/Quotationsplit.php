<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Quotationsplit extends Model
{
    protected $fillable = [
	    'quotation',
	    'pol',
	    'pod',
	    'no_inc',
	    'no_split',
	    'etd',
	    'eta',
	    'created_user',
	    'updated_user',
	    'sent_at',
	    'sent_user',
	    'packed_at',
	    'packed_user'
  	];

  	public function fkQuotation(){
	    return $this->belongsTo('\App\Http\Models\Quotation', 'quotation', 'id');
	}

	public function fkPortl(){
	    return $this->belongsTo('\App\Http\Models\Port', 'pol', 'id');
	}

	public function fkPortd(){
	    return $this->belongsTo('\App\Http\Models\Port', 'pod', 'id');
	}
}
