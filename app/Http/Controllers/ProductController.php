<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Product;
use App\Http\Models\Productalias;
use App\Http\Models\Account;
use App\Http\Models\Department;
use App\Http\Models\Variant;
use App\Http\Models\Merk;
use App\Http\Models\Typesub;
use App\Http\Models\Size;
use App\Http\Models\Unit;
use App\Http\Models\Company;
use App\Http\Models\Employeeset;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $products = Product::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/product.index', compact('products','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $accounts = Account::where('status', 1)->orderBy('name')->get();
        $departments = Department::where('status', 1)->orderBy('name')->get();
        $variants = Variant::where('status', 1)->orderBy('name')->get();
        $merks    = Merk::where('status', 1)->orderBy('name')->get();
        $typesubs = Typesub::where('status', 1)->orderBy('name')->get();
        $sizes    = Size::where('status', 1)->orderBy('name')->get();
        $units    = Unit::where('status', 1)->orderBy('name')->get();

        return view('vendor/adminlte/product.create', compact('accounts','departments','variants','merks','typesubs','sizes','units','disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'code' => 'unique:products'
                ]);

                $general        = 0; if (isset($json->general))      { $general       = 1; };
                $status_group   = 0; if (isset($json->status_group)) { $status_group  = 1; };
                $status_record  = 0; if (isset($json->status_record)){ $status_record = 1; };
                
                $product = new Product([
                    'code'          => $json->code,
                    'name'          => $json->name,
                    'accounting'    => $json->account,
                    'department'    => $json->department,
                    'variant'       => !empty($json->variant) ? $json->variant : 0,
                    'merk'          => !empty($json->merk) ? $json->merk : 0,
                    'typesub'       => !empty($json->typesub) ? $json->typesub : 0,
                    'size'          => !empty($json->size) ? $json->size : 0,
                    'unit'          => $json->unit,
                    'general'       => $general,
                    'status_group'  => $status_group,
                    'status_record' => $status_record,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $product->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $product  = Product::find($id);
        $productaliass = Productalias::where('product', $id)->get();
        $accounts = Account::where('status', 1)->orderBy('name')->get();
        $departments = Department::where('status', 1)->orderBy('name')->get();
        $variants = Variant::where('status', 1)->orderBy('name')->get();
        $merks    = Merk::where('status', 1)->orderBy('name')->get();
        $typesubs = Typesub::where('status', 1)->orderBy('name')->get();
        $sizes    = Size::where('status', 1)->orderBy('name')->get();
        $units    = Unit::where('status', 1)->orderBy('name')->get();
        $company = Company::first();

        return view('vendor/adminlte/product.show', compact('id','product','productaliass','accounts','departments','variants','merks','typesubs','sizes','units','company','disable'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $product  = Product::find($id);
        $accounts = Account::where('status', 1)->orderBy('name')->get();
        $departments = Department::where('status', 1)->orderBy('name')->get();
        $variants = Variant::where('status', 1)->orderBy('name')->get();
        $merks    = Merk::where('status', 1)->orderBy('name')->get();
        $typesubs = Typesub::where('status', 1)->orderBy('name')->get();
        $sizes    = Size::where('status', 1)->orderBy('name')->get();
        $units    = Unit::where('status', 1)->orderBy('name')->get();

        return view('vendor/adminlte/product.edit', compact('id','product','accounts','departments','variants','merks','typesubs','sizes','units','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $product = Product::find($id);
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                if ($product->code != $json->code) {
                    $request->validate([
                        'code' => 'unique:products'
                    ]);
                };

                $general        = 0; if (isset($json->general))      { $general       = 1; };
                $status_group   = 0; if (isset($json->status_group)) { $status_group  = 1; };
                $status_record  = 0; if (isset($json->status_record)){ $status_record = 1; };

                $product->code          = $json->code;
                $product->name          = $json->name;
                $product->accounting    = $json->account;
                $product->department    = $json->department;
                $product->variant       = $json->variant;
                $product->merk          = $json->merk;
                $product->typesub       = $json->typesub;
                $product->size          = $json->size;
                $product->unit          = $json->unit;
                $product->general       = $general;
                $product->status_group  = $status_group;
                $product->status_record = $status_record;
                $product->updated_user  = $session_id;
                $result = $product->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $product = Product::find($id);
        $status = $product->status;

        if ($status == 1) {
        	$product->status       = 0;
            $product->updated_user = $session_id;
	        $result = $product->save();

        } elseif ($status == 0) {
        	$product->status       = 1;
            $product->updated_user = $session_id;
	        $result = $product->save();

        };
        return $this->jsonSuccess( $result );
    }
}
