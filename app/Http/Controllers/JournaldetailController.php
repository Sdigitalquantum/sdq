<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Journaldetail;
use App\Http\Models\Journal;
use App\Http\Models\Account;
use App\Http\Models\Employeeset;

class JournaldetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $journals = Journal::where('id', $id)->get();
        $accounts = Account::where('status', 1)->orderBy('name')->get();
        $journaldetails = Journaldetail::where('journal', $id)->get();

        return view('vendor/adminlte/journaldetail.create', compact('journals','accounts','journaldetails','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $cek = Journaldetail::where([['journal', $request->get('journal')], ['accounting', $request->get('account')]])->first();

        if (!empty($cek)) {
            return redirect()->back()->withErrors('Journal Detail already exist');
        } else {
            $journaldetail = new Journaldetail([
                'journal'       => $request->get('journal'),
                'accounting'    => $request->get('account'),
                'debit'         => $request->get('debit'),
                'status_group'  => ($request->get('status_group') == 1) ? 1 : 0,
                'status_record' => ($request->get('status_record') == 1) ? 1 : 0,
                'created_user'  => $session_id,
                'updated_user'  => $session_id
            ]);
            $journaldetail->save();

            return redirect('/journaldetail/create/'.$request->get('journal'))->with('success', 'Journal Detail has been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $journaldetail  = Journaldetail::find($id);
        $journals = Journal::where('id', $journaldetail->journal)->orderBy('name')->get();
        $accounts = Account::where('status', 1)->orderBy('name')->get();
        $journaldetails = Journaldetail::where('journal', $journaldetail->journal)->get();

        return view('vendor/adminlte/journaldetail.edit', compact('id','journaldetail','journals','accounts','journaldetails','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $journaldetail = Journaldetail::find($id);

        if ($journaldetail->accounting == $request->get('account')) {
            $journaldetail->debit         = $request->get('debit');
            $journaldetail->status_group  = ($request->get('status_group') == 1) ? 1 : 0;
            $journaldetail->status_record = ($request->get('status_record') == 1) ? 1 : 0;
            $journaldetail->updated_user  = $session_id;
            $journaldetail->save();

            return redirect('/journaldetail/create/'.$journaldetail->journal)->with('success', 'Journal Detail has been updated');
        } else {
            $cek = Journaldetail::where([['journal', $request->get('journal')], ['accounting', $request->get('account')]])->first();

            if (!empty($cek)) {
                return redirect()->back()->withErrors('Journal Detail already exist');
            } else {
                $journaldetail->accounting    = $request->get('account');
                $journaldetail->debit         = $request->get('debit');
                $journaldetail->status_group  = ($request->get('status_group') == 1) ? 1 : 0;
                $journaldetail->status_record = ($request->get('status_record') == 1) ? 1 : 0;
                $journaldetail->updated_user  = $session_id;
                $journaldetail->save();

                return redirect('/journaldetail/create/'.$journaldetail->journal)->with('success', 'Journal Detail has been updated');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $journaldetail = Journaldetail::find($id);
        $status = $journaldetail->status;

        if ($status == 1) {
        	$journaldetail->status       = 0;
            $journaldetail->updated_user = $session_id;
	        $journaldetail->save();

		    return redirect('/journaldetail/create/'.$journaldetail->journal)->with('error', 'Journal Detail has been deleted');
        } elseif ($status == 0) {
        	$journaldetail->status       = 1;
            $journaldetail->updated_user = $session_id;
	        $journaldetail->save();

		    return redirect('/journaldetail/create/'.$journaldetail->journal)->with('success', 'Journal Detail has been activated');
        }
    }
}
