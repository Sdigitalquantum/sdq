<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Transaction;
use App\Http\Models\Department;
use App\Http\Models\Account;
use App\Http\Models\Employeeset;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $transactions = Transaction::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/transaction.index', compact('transactions','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $departments = Department::where('status', 1)->orderBy('name')->get();
        $accounts = Account::where('status', 1)->orderBy('name')->get();

        return view('vendor/adminlte/transaction.create', compact('departments','accounts','disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'code' => 'unique:transactions'
                ]);

                $status_group   = 0; if (isset($json->status_group)) { $status_group  = 1; };
                $status_record  = 0; if (isset($json->status_record)){ $status_record = 1; };
                
                $transaction = new Transaction([
                    'department'    => $json->department,
                    'accounting'    => $json->account,
                    'code'          => $json->code,
                    'name'          => $json->name,
                    'status_group'  => $status_group,
                    'status_record' => $status_record,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $transaction->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $transaction  = Transaction::find($id);
        $departments = Department::where('status', 1)->orderBy('name')->get();
        $accounts = Account::where('status', 1)->orderBy('name')->get();

        return view('vendor/adminlte/transaction.edit', compact('id','transaction','departments','accounts','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   $transaction = Transaction::find($id);
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                if ($transaction->code != $json->code) {
                    $request->validate([
                        'code' => 'unique:transactions'
                    ]);
                };

                $status_group   = 0; if (isset($json->status_group)) { $status_group  = 1; };
                $status_record  = 0; if (isset($json->status_record)){ $status_record = 1; };

                $transaction->department    = $json->department;
                $transaction->accounting    = $json->account;
                $transaction->code          = $json->code;
                $transaction->name          = $json->name;
                $transaction->status_group  = $status_group;
                $transaction->status_record = $status_record;
                $transaction->updated_user  = $session_id;
                $result = $transaction->save();
            };
        };
        return $this->jsonSuccess( $result );

        

	    return redirect('/transaction')->with('success', 'Transaction has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $transaction = Transaction::find($id);
        $status = $transaction->status;

        if ($status == 1) {
        	$transaction->status       = 0;
            $transaction->updated_user = $session_id;
	        $result = $transaction->save();

        } elseif ($status == 0) {
        	$transaction->status       = 1;
            $transaction->updated_user = $session_id;
	        $result = $transaction->save();

        };
        return $this->jsonSuccess( $result );
    }
}
