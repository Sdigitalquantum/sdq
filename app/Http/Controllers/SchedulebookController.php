<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Schedulebook;
use App\Http\Models\Scheduleship;
use App\Http\Models\Quotationsplit;
use App\Http\Models\Employeeset;

class SchedulebookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $scheduleships = Scheduleship::orderBy('id')->get();

        return view('vendor/adminlte/schedulebook.index', compact('scheduleships','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $quotationsplits = Quotationsplit::where([['status_book', 0], ['status_request', 0], ['status_release', 1]])->orderBy('no_split')->get();
        $scheduleship = Scheduleship::find($id);
        $schedulebooks = Schedulebook::where([['pol', $scheduleship->pol], ['pod', $scheduleship->pod], ['lines', $scheduleship->lines], ['etd', $scheduleship->etd], ['eta', $scheduleship->eta]])->get();

        return view('vendor/adminlte/schedulebook.create', compact('id','quotationsplits','schedulebooks','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $scheduleship = Scheduleship::find($request->get('scheduleship'));

        $schedulebook = new Schedulebook([
            'quotationsplit'    => $request->get('quotationsplit'),
            'pol'               => $scheduleship->pol,
            'pod'               => $scheduleship->pod,
            'lines'             => $scheduleship->lines,
            'etd'               => $scheduleship->etd,
            'eta'               => $scheduleship->eta,
            'notice'            => !empty($request->get('notice')) ? $request->get('notice') : '',
            'reason'            => '',
            'created_user'      => $session_id,
            'updated_user'      => $session_id
        ]);
        $schedulebook->save();

        $scheduleship->status_book = 1;
        $scheduleship->save();

        $quotationsplit = Quotationsplit::where('id', $request->get('quotationsplit'))->first();
        $quotationsplit->status_book = 1;
        $quotationsplit->save();

        return redirect('/schedulebook/create/'.$request->get('scheduleship'))->with('success', 'Shipping Schedule has been booked');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $schedulebook = Schedulebook::find($id);
        $scheduleships = Scheduleship::where('status_book', 0)->orderBy('id')->get();

        return view('vendor/adminlte/schedulebook.edit', compact('schedulebook','scheduleships','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $schedulebook = Schedulebook::find($id);
        $schedulebook->delete();

        $scheduleship = Scheduleship::find($request->get('scheduleship'));
        $scheduleship->status_book = 0;
        $scheduleship->save();

        $quotationsplit = Quotationsplit::where('id', $schedulebook->quotationsplit)->first();
        $quotationsplit->status_book = 0;
        $quotationsplit->save();

        return redirect('/schedulebook/create/'.$request->get('scheduleship'))->with('error', 'Booking Schedule has been deleted');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $schedulebooks = Schedulebook::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/schedulebook.list', compact('schedulebooks','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $schedulebook = Schedulebook::find($id);
        
        $scheduleship = Scheduleship::find($request->get('scheduleship'));
        $scheduleship->status_book       = 1;
        $scheduleship->save();

        $scheduleshipold = Scheduleship::where([['pol', $schedulebook->pol], ['pod', $schedulebook->pod], ['lines', $schedulebook->lines], ['etd', $schedulebook->etd], ['eta', $schedulebook->eta]])->first();
        $scheduleshipold->status_book    = 0;
        $scheduleshipold->save();

        $schedulebook->pol               = $scheduleship->pol;
        $schedulebook->pod               = $scheduleship->pod;
        $schedulebook->lines             = $scheduleship->lines;
        $schedulebook->etd               = $scheduleship->etd;
        $schedulebook->eta               = $scheduleship->eta;
        $schedulebook->status_approve    = 0;
        $schedulebook->notice            = $request->get('notice');
        $schedulebook->reason            = '';
        $schedulebook->updated_user      = $session_id;
        $schedulebook->save();

        return redirect('/schedulebooklist')->with('success', 'Booking Schedule has been rescheduled');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function approval(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $schedulebooks = Schedulebook::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/schedulebook.approval', compact('schedulebooks','disable','employeeroots','employeemenus','employeesubs'));
    }
    
    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $schedulebook = Schedulebook::find($id);
        $schedulebook->status_approve    = 1;
        $schedulebook->reason            = $request->get('reason');
        $schedulebook->updated_user      = $session_id;
        $schedulebook->save();

        return redirect('/approvalbook')->with('success', 'Booking Schedule has been approved');
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reject(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $schedulebook = Schedulebook::find($id);
        $schedulebook->status_approve    = 2;
        $schedulebook->reason            = $request->get('reason');
        $schedulebook->updated_user      = $session_id;
        $schedulebook->save();

        return redirect('/approvalbook')->with('error', 'Booking Schedule has been rejected');
    }
}
