<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@index');
Route::get('/contact', 'FrontendController@contact')->name('contact');
Route::resource('mailcontact', 'MailcontactController');
Route::get('/order', 'FrontendController@order')->name('order');
Route::resource('mailorder', 'OrderController');
Route::resource('orderdetail', 'OrderdetailController');
Route::any('/orderdetail/create/{id}', 'OrderdetailController@create');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/register', 'HomeController@index')->name('register');
Route::get('/login', 'HomeController@login')->name('login');
Route::any('/backend', 'HomeController@backend')->name('backend');

Route::resource('country', 'CountryController');
Route::resource('city', 'CityController');
Route::resource('port', 'PortController');	
Route::resource('bank', 'BankController');
Route::resource('currency', 'CurrencyController');
Route::resource('kurs', 'KursController');
Route::resource('payment', 'PaymentController');
Route::resource('paymentterm', 'PaymenttermController');
Route::resource('container', 'ContainerController');

Route::resource('branch', 'BranchController');
Route::resource('department', 'DepartmentController');
Route::resource('position', 'PositionController');
Route::resource('warehouse', 'WarehouseController');
Route::resource('document', 'DocumentController');
Route::resource('letter', 'LetterController');
Route::resource('company', 'CompanyController');
Route::resource('companyaccount', 'CompanyaccountController');

Route::resource('variant', 'VariantController');
Route::resource('merk', 'MerkController');
Route::resource('size', 'SizeController');
Route::resource('unit', 'UnitController');
Route::resource('type', 'TypeController');
Route::resource('typesub', 'TypesubController');
Route::resource('pricelist', 'PricelistController');

Route::resource('accountgroup', 'AccountgroupController');
Route::resource('accounttype', 'AccounttypeController');
Route::resource('account', 'AccountController');
Route::resource('customer', 'CustomerController');
Route::resource('customerdetail', 'CustomerdetailController');
Route::resource('customeraccount', 'CustomeraccountController');
Route::resource('supplier', 'SupplierController');
Route::resource('supplierdetail', 'SupplierdetailController');
Route::resource('supplieraccount', 'SupplieraccountController');
Route::resource('product', 'ProductController');
Route::resource('productalias', 'ProductaliasController');
Route::resource('transaction', 'TransactionController');
Route::resource('material', 'MaterialController');
Route::resource('materialdetail', 'MaterialdetailController');
Route::resource('materialother', 'MaterialotherController');
Route::resource('sales', 'SalesController');

Route::resource('quotation', 'QuotationController');
Route::any('/quotation/{id_url}/ajax/{id}', 'QuotationController@ajax');
Route::get('/dummy', 'QuotationController@dummy')->name('dummy');
Route::any('/quotation/release/{id}', 'QuotationController@release');
Route::resource('quotationdetail', 'QuotationdetailController');
Route::any('/quotationdetail/{id_url}/ajax/{id}', 'QuotationdetailController@ajax');
Route::resource('quotationpayment', 'QuotationpaymentController');
Route::resource('quotationcharge', 'QuotationchargeController');
Route::resource('plan', 'PlanController');
Route::any('/plan/create/{id}', 'PlanController@create');
Route::any('/plan/book/{id}', 'PlanController@book');
Route::any('/plan/release/{id}', 'PlanController@release');
Route::any('/plan/other/{id}', 'PlanController@other');
Route::get('/planreport', 'PlanController@report')->name('planreport');
Route::resource('quotationsplit', 'QuotationsplitController');
Route::any('/quotationsplit/create/{id}', 'QuotationsplitController@create');
Route::any('/quotationsplit/release/{id}', 'QuotationsplitController@release');
Route::resource('quotationplan', 'QuotationplanController');

Route::resource('scheduleship', 'ScheduleshipController');
Route::post('import', 'ScheduleshipController@import')->name('import');
Route::resource('schedulebook', 'SchedulebookController');
Route::any('/schedulebook/create/{id}', 'SchedulebookController@create');
Route::get('/schedulebooklist', 'SchedulebookController@list')->name('schedulebooklist');
Route::any('/schedulebook/change/{id}', 'SchedulebookController@change');
Route::get('/approvalbook', 'SchedulebookController@approval')->name('approvalbook');
Route::any('/schedulebook/approve/{id}', 'SchedulebookController@approve');
Route::any('/schedulebook/reject/{id}', 'SchedulebookController@reject');
Route::resource('schedulestuff', 'SchedulestuffController');
Route::any('/schedulestuff/change/{id}', 'SchedulestuffController@change');
Route::get('/schedulestufflist', 'SchedulestuffController@list')->name('schedulestufflist');
Route::get('/approvalstuff', 'SchedulestuffController@approval')->name('approvalstuff');
Route::any('/schedulestuff/approve/{id}', 'SchedulestuffController@approve');
Route::any('/schedulestuff/reject/{id}', 'SchedulestuffController@reject');
Route::resource('stuff', 'StuffController');
Route::any('/stuff/create/{id}', 'StuffController@create');
Route::get('/stufforder', 'StuffController@list')->name('stufforder');
Route::any('/stuff/change/{id}', 'StuffController@change');
Route::resource('weigh', 'WeighController');
Route::get('/pack', 'WeighController@list')->name('pack');
Route::get('/approvalpack', 'WeighController@approval')->name('approvalpack');
Route::any('/weigh/change/{id}', 'WeighController@change');
Route::get('/packlist', 'WeighController@pack')->name('packlist');
Route::any('/weigh/packlist/preview/{id}', 'WeighController@packpreview');
Route::get('/invoice', 'WeighController@invoice')->name('invoice');
Route::any('/weigh/invoice/journal/{id}', 'WeighController@invoicejournal');
Route::any('/weigh/invoice/preview/{id}', 'WeighController@invoicepreview');
Route::resource('documentcheck', 'DocumentcheckController');
Route::resource('stuffreschedule', 'StuffrescheduleController');
Route::get('/stuffconfirm', 'StuffController@confirm')->name('stuffconfirm');
Route::resource('stuffnon', 'StuffnonController');
Route::any('/stuffnon/create/{id}', 'StuffnonController@create');
Route::get('/stuffordernon', 'StuffnonController@list')->name('stuffordernon');
Route::any('/stuffnon/change/{id}', 'StuffnonController@change');
Route::get('/stuffconfirmnon', 'StuffnonController@confirm')->name('stuffconfirmnon');
Route::resource('weighnon', 'WeighnonController');
Route::get('/packnon', 'WeighnonController@list')->name('packnon');
Route::get('/approvalpacknon', 'WeighnonController@approval')->name('approvalpacknon');
Route::any('/weighnon/change/{id}', 'WeighnonController@change');
Route::get('/packlistnon', 'WeighnonController@pack')->name('packlistnon');
Route::any('/weighnon/packlist/preview/{id}', 'WeighnonController@packpreview');
Route::get('/invoicenon', 'WeighnonController@invoice')->name('invoicenon');
Route::any('/weighnon/invoice/journal/{id}', 'WeighnonController@invoicejournal');
Route::any('/weighnon/invoice/preview/{id}', 'WeighnonController@invoicepreview');
Route::get('/weighemkl', 'WeighController@emkl')->name('weighemkl');
Route::get('/weighemklnon', 'WeighnonController@emkl')->name('weighemklnon');
Route::resource('documentchecknon', 'DocumentchecknonController');
Route::get('/stuffreroute', 'StuffController@reroute')->name('stuffreroute');
Route::any('/stuff/cancel/{id}', 'StuffController@cancel');
Route::get('/stuffreroutenon', 'StuffnonController@reroute')->name('stuffreroutenon');
Route::any('/stuffnon/cancel/{id}', 'StuffnonController@cancel');
Route::get('/stuffcheck', 'StuffController@check')->name('stuffcheck');
Route::get('/stuffchecknon', 'StuffnonController@check')->name('stuffchecknon');

Route::resource('purchaserequest', 'PurchaserequestController');
Route::any('/purchaserequest/set/{id}', 'PurchaserequestController@set');
Route::get('/approvalpr', 'PurchaserequestController@approval')->name('approvalpr');
Route::any('/purchaserequest/approve/{id}', 'PurchaserequestController@approve');
Route::any('/purchaserequest/reject/{id}', 'PurchaserequestController@reject');
Route::get('/purchaserequestlist', 'PurchaserequestController@list')->name('purchaserequestlist');
Route::any('/purchaserequest/release/{id}', 'PurchaserequestController@release');
Route::resource('purchasegeneral', 'PurchasegeneralController');
Route::any('/purchasegeneral/set/{id}', 'PurchasegeneralController@set');
Route::get('/approvalprg', 'PurchasegeneralController@approval')->name('approvalprg');
Route::any('/purchasegeneral/approve/{id}', 'PurchasegeneralController@approve');
Route::any('/purchasegeneral/reject/{id}', 'PurchasegeneralController@reject');
Route::any('/purchasegeneral/release/{id}', 'PurchasegeneralController@release');
Route::resource('purchasedetailprg', 'PurchasedetailprgController');
Route::any('/purchasedetailprg/create/{id}', 'PurchasedetailprgController@create');
Route::resource('purchaseorder', 'PurchaseorderController');
Route::any('/purchaseorder/set/{id}', 'PurchaseorderController@set');
Route::get('/approvalpo', 'PurchaseorderController@approval')->name('approvalpo');
Route::any('/purchaseorder/approve/{id}', 'PurchaseorderController@approve');
Route::any('/purchaseorder/reject/{id}', 'PurchaseorderController@reject');
Route::any('/purchaseorder/release/{id}', 'PurchaseorderController@release');
Route::resource('purchasedetail', 'PurchasedetailController');
Route::any('/purchasedetail/create/{id}', 'PurchasedetailController@create');
Route::resource('purchasedetailpr', 'PurchasedetailprController');
Route::resource('purchasederivative', 'PurchasederivativeController');
Route::any('/purchasederivative/set/{id}', 'PurchasederivativeController@set');
Route::get('/approvalpd', 'PurchasederivativeController@approval')->name('approvalpd');
Route::any('/purchasederivative/approve/{id}', 'PurchasederivativeController@approve');
Route::any('/purchasederivative/reject/{id}', 'PurchasederivativeController@reject');
Route::any('/purchasederivative/release/{id}', 'PurchasederivativeController@release');

Route::resource('stockrequest', 'StockrequestController');
Route::any('/stockrequest/create/{id}', 'StockrequestController@create');
Route::resource('stockrequestnon', 'StockrequestnonController');
Route::any('/stockrequestnon/create/{id}', 'StockrequestnonController@create');
Route::resource('stockload', 'StockloadController');
Route::any('/stockload/create/{id}', 'StockloadController@create');
Route::resource('stockin', 'StockinController');
Route::get('/stockproduct', 'StockinController@product')->name('stockproduct');
Route::get('/stockwarehouse', 'StockinController@show')->name('stockwarehouse');
Route::any('/stockin/validation/{id}', 'StockinController@validation');
Route::get('/stockmove', 'StockinController@list')->name('stockmove');
Route::any('/stockin/change/{id}', 'StockinController@change');
Route::get('/stockout', 'StockinController@detail')->name('stockout');
Route::any('/stockin/set/{id}', 'StockinController@set');
Route::get('/stockcard', 'StockinController@card')->name('stockcard');
Route::any('/stockfilter', 'StockinController@filter')->name('stockfilter');
Route::get('/stockopname', 'StockinController@opname')->name('stockopname');
Route::any('/stockin/adjust/{id}', 'StockinController@adjust');
Route::resource('stockqc', 'StockqcController');
Route::any('/stockqc/create/{id}', 'StockqcController@create');
Route::get('/purchaseincome', 'StockinController@price')->name('purchaseincome');
Route::any('/stockin/income/{id}', 'StockinController@income');
Route::resource('stockdetail', 'StockdetailController');
Route::any('/stockdetail/create/{id}', 'StockdetailController@create');
Route::resource('production', 'ProductionController');
Route::resource('productiondetail', 'ProductiondetailController');
Route::resource('cutoff', 'StockcardController');

Route::resource('menuroot', 'MenurootController');
Route::resource('menu', 'MenuController');
Route::any('/menu/create/{id}', 'MenuController@create');
Route::resource('menusub', 'MenusubController');
Route::any('/menusub/create/{id}', 'MenusubController@create');
Route::resource('group', 'GroupController');
Route::resource('groupset', 'GroupsetController');
Route::any('/groupset/sync/{id}', 'GroupsetController@sync');
Route::resource('employee', 'EmployeeController');
Route::resource('employeeset', 'EmployeesetController');
Route::resource('employeewarehouse', 'EmployeewarehouseController');

Route::resource('journal', 'JournalController');
Route::resource('journaldetail', 'JournaldetailController');
Route::any('/journaldetail/create/{id}', 'JournaldetailController@create');
Route::resource('journalset', 'JournalsetController');
Route::resource('debt', 'DebtController');
Route::any('/debt/payment/{id}', 'DebtController@payment');
Route::resource('debtcutoff', 'DebtcardController');
Route::get('/debtmonitor', 'DebtController@list')->name('debtmonitor');
Route::get('/debtcard', 'DebtController@card')->name('debtcard');
Route::any('/debtfilter', 'DebtController@filter')->name('debtfilter');
Route::resource('receivable', 'ReceivableController');
Route::any('/receivable/payment/{id}', 'ReceivableController@payment');
Route::resource('receivablecutoff', 'ReceivablecardController');
Route::get('/receivablemonitor', 'ReceivableController@list')->name('receivablemonitor');
Route::get('/receivablecard', 'ReceivableController@card')->name('receivablecard');
Route::any('/receivablefilter', 'ReceivableController@filter')->name('receivablefilter');

Route::any('exportplan', 'PlanController@export')->name('exportplan');
Route::any('exportjournal', 'JournalsetController@export')->name('exportjournal');
Route::any('exportdebt', 'DebtController@export')->name('exportdebt');
Route::any('exportdebtcard', 'DebtcardController@export')->name('exportdebtcard');
Route::any('exportreceivable', 'ReceivableController@export')->name('exportreceivable');
Route::any('exportreceivablecard', 'ReceivablecardController@export')->name('exportreceivablecard');

Route::resource('cashin', 'CashinController');
Route::resource('cashout', 'CashoutController');
Route::resource('bankin', 'BankinController');
Route::resource('bankout', 'BankoutController');