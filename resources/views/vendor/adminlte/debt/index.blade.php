@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Debt Transaction &nbsp;<a href="{{ url('exportdebt')}}" class="btn btn-xs btn-default" style="text-decoration: none;"><i class="fa fa-file-excel-o"></i></a></h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="5%">No</th>
			          	<th width="10%">No. PO</th>
			          	<th width="15%">Supplier</th>
			          	<th width="15%">Product</th>
			          	<th width="10%">Date PO</th>
			          	<th width="10%">Date QC</th>
			          	<th width="10%">Total</th>
			          	<th width="10%">Payment</th>
			          	<th width="10%">Adjustment</th>
			          	<th width="5%">Action</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@php ($no = 0)
			    	@foreach($debts as $debt)
			    		@php ($no++)
			        	<tr>
				            <td>{{$no}}</td>
				            <td>{{$debt->no_po}}</td>
				            <td>{{$debt->name_supplier}}</td>
				            <td>{{$debt->name_product}}</td>
				            <td>{{$debt->date_po}}</td>
				            <td>{{$debt->date_qc}}</td>
				            <td style="text-align: right;">{{number_format($debt->total, 0, ',' , '.')}} &nbsp;</td>
				            <td style="text-align: right;">{{number_format($debt->payment, 0, ',' , '.')}} &nbsp;</td>
				            <td style="text-align: right;">{{number_format($debt->adjustment, 0, ',' , '.')}} &nbsp;</td>

				            @if($debt->status == 1)
				            	<td><font color="green">Paid <i class="glyphicon glyphicon-ok"></i></font></td>
		            		@else
		            			<td>
					            	<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$debt->id}}">Payment</button>

					            	<form action="{{ url('debt/payment', $debt->id)}}" method="post">
					                  	@csrf
					                  	@method('DELETE')
					                  	<div class="modal fade bs-example-modal{{$debt->id}}" tabindex="-1" role="dialog" aria-hidden="true">
		                                    <div class="modal-dialog modal-sm">
		                                        <div class="modal-content">
		                                            <div class="modal-header btn-success">
		                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
		                                                </button>
		                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
		                                            </div>
		                                            <div class="modal-body">
		                                                Are you sure to pay this data?
		                                                
		                                                <br><br>Payment <br><input type="number" class="form-control" name="payment" value="0" required>
		                                                <br><br>Adjustment <br><input type="number" class="form-control" name="adjustment" value="0" required>
		                                            </div>
		                                            <div class="modal-footer">
		                                                <button type="submit" class="btn btn-success">Yes</button>
		                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
					                </form>
				                </td>
	            			@endif
				        </tr>
			        @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			          	<td class="noShow"></td>
			          	<td>No. PO</td>
			          	<td>Supplier</td>
			          	<td>Product</td>
			          	<td>Date PO</td>
			          	<td>Date QC</td>
			          	<td>Total</td>
			          	<td>Payment</td>
			          	<td>Adjustment</td>
			          	<td class="noShow"></td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	<div>
@stop