<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Schedulebook;
use App\Http\Models\Schedulestuff;
use App\Http\Models\Purchaserequest;
use App\Http\Models\Employee;
use App\Http\Models\Employeeset;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        if (empty($session_id)) {   return redirect('/login'); } 
        else {
            $goingorder = Schedulebook::where('status_approve', 1)->count();
            $scheduleorder = Schedulestuff::where([['status_approve', 1], ['status_delivery', 1], ['status_confirm', 0]])->count();
            $bookingorder = Purchaserequest::where([['status_plan', 1], ['status_release', 1]])->count();

            $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
            $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
            $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
                
            return view('vendor/adminlte/home', compact('goingorder','scheduleorder','bookingorder','disable','employeeroots','employeemenus','employeesubs'));
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $request->session()->forget('session_login');

        return view('vendor/adminlte/login');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function backend(Request $request)
    {
        $request->validate([
            'email' => 'email:true'
        ]);

        $cek = Employee::where(
                [['email', $request->get('email')], 
                ['password', md5($request->get('password'))]]
                )->first();

        if (empty($cek)) {
            return redirect()->back()->withErrors('User is not available');
        } else {
            if ($cek->status == 0) {
                return redirect()->back()->withErrors('User is not active');
            } else {
                $request->session()->put('session_login', $cek->id);
                $session_id = $request->session()->get('session_login');
                $disable = 0;

                $goingorder = Schedulebook::where('status_approve', 1)->count();
                $scheduleorder = Schedulestuff::where([['status_approve', 1], ['status_delivery', 1], ['status_confirm', 0]])->count();
                $bookingorder = Purchaserequest::where([['status_plan', 1], ['status_release', 1]])->count();

                $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
                $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
                $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

                return view('vendor/adminlte/home', 
                        compact('goingorder','scheduleorder','bookingorder','disable','employeeroots','employeemenus','employeesubs'));
            }
        }
    }
}
