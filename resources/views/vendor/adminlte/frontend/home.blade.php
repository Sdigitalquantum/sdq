@extends('adminlte::frontend/page')

@section('content')
    <section class='madeforyou padding-top-100 padding-bottom-0'>
        <div class="angled_down_inside white">
            <div class="slope upleft"></div>
            <div class="slope upright"></div>
        </div>
        
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-area">
                    <h1 class="heading left-align">Profile</h1>
                    <div class="headul left-align"></div>
                    <p>
                        <b>Visi</b> <br>
                        To become #1 Market Leader Worldwide in Indonesia Coconut Processing Related.
                    </p>
                    <p>
                        <b>Misi</b>
                        <ul>
                            <li>Memastikan sebuah sinergi terhadap managemen rantai produksi di Indonesia antara Perkebunan kelapa, petani, sistem pemprosesan dan Klien di seluruh dunia.</li>
                            <li>Menyediakan produk kelapa berkualitas bagus untuk kepuasan klien kami.</li>
                            <li>Meningkatkan kesejahteraan petani kelapa dan pegawai kami untuk memberikan dampak positif pada pertumbuhan ekonomi Indonesia.</li>
                        </ul>
                    </p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 slider-area">
                    <ul id="image-slider" class="owl-carousel">
                        <!-- <li class="">
                            <img alt="blog-image" class="lazyOwl img-responsive" data-src="{{ asset('vendor/caliber/img/sdq1.jpg') }}" src="#">
                        </li>
                        <li class="">
                            <img alt="blog-image" class="lazyOwl img-responsive" data-src="{{ asset('vendor/caliber/img/sdq2.jpg') }}" src="#">
                        </li>
                        <li class="">
                            <img alt="blog-image" class="lazyOwl img-responsive" data-src="{{ asset('vendor/caliber/img/sdq3.jpg') }}" src="#">
                        </li>
                        <li class="">
                            <img alt="blog-image" class="lazyOwl img-responsive" data-src="{{ asset('vendor/caliber/img/sdq4.jpg') }}" src="#">
                        </li>
                        <li class="">
                            <img alt="blog-image" class="lazyOwl img-responsive" data-src="{{ asset('vendor/caliber/img/sdq5.jpg') }}" src="#">
                        </li>
                        <li class="">
                            <img alt="blog-image" class="lazyOwl img-responsive" data-src="{{ asset('vendor/caliber/img/sdq6.jpg') }}" src="#">
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="angled_up_inside white">
            <div class="slope upleft"></div>
            <div class="slope upright"></div>
        </div>
    </section>

    <section class='bg-primary services max-width-100 padding-bottom-0' id='services'>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <h1 class="heading text-white left-align">Services</h1>
                    <div class="headul white left-align"></div>
                    <p><img src="{{ asset('vendor/caliber/img/sdq_kelapa.png') }}"></p>
                    <p class="text-white left-align">PEMBERDAYAAN KELAPA PILIHAN</p>
                    <p>Aneka produk Kelapa berkualitas tinggi untuk kepuasan customer</p>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <h1 class="heading text-white left-align">&nbsp;</h1>
                    <div class="white left-align"><br><br><br></div>
                    <p><img src="{{ asset('vendor/caliber/img/sdq_customer.png') }}"></p>
                    <p class="text-white left-align">CUSTOMER GLOBAL</p>
                    <p>Kami melayani permintaan dari seluruh penjuru dunia dengan standar tinggi serta sinergi rantai pemasok</p>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <h1 class="heading text-white left-align">&nbsp;</h1>
                    <div class="white left-align"><br><br><br></div>
                    <p><img src="{{ asset('vendor/caliber/img/sdq_sosial.png') }}"></p>
                    <p class="text-white left-align">TANGGUNG JAWAB SOSIAL PERUSAHAAN</p>
                    <p>Mendidik dan membantu masyarakat sekitar perkebunan mendapat kehidupan yang lebih baik</p>
                </div>
            </div>
        </div>

        <div class="angled_down_outside outside primary lightgray">
            <div class="slope downleft"></div>
            <div class="slope downright"></div>
        </div>
    </section>
@stop
