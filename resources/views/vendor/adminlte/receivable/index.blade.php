@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Receivable Transaction &nbsp;<a href="{{ url('exportreceivable')}}" class="btn btn-xs btn-default" style="text-decoration: none;"><i class="fa fa-file-excel-o"></i></a></h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="5%">No</th>
			          	<th width="10%">No. SP</th>
			          	<th width="15%">Customer</th>
			          	<th width="15%">Product</th>
			          	<th width="10%">Date SP</th>
			          	<th width="10%">Date SJ</th>
			          	<th width="10%">Total</th>
			          	<th width="10%">Payment</th>
			          	<th width="10%">Adjustment</th>
			          	<th width="5%">Action</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@php ($no = 0)
			    	@foreach($receivables as $receivable)
			    		@php ($no++)
			        	<tr>
				            <td>{{$no}}</td>
				            <td>{{$receivable->no_sp}}</td>
				            <td>{{$receivable->name_customer}}</td>
				            <td>{{$receivable->name_product}}</td>
				            <td>{{$receivable->date_sp}}</td>
				            <td>{{$receivable->date_sj}}</td>
				            <td style="text-align: right;">{{number_format($receivable->total, 0, ',' , '.')}} &nbsp;</td>
				            <td style="text-align: right;">{{number_format($receivable->payment, 0, ',' , '.')}} &nbsp;</td>
				            <td style="text-align: right;">{{number_format($receivable->adjustment, 0, ',' , '.')}} &nbsp;</td>

				            @if($receivable->status == 1)
				            	<td><font color="green">Paid <i class="glyphicon glyphicon-ok"></i></font></td>
		            		@else
		            			<td>
					            	<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$receivable->id}}">Payment</button>

					            	<form action="{{ url('receivable/payment', $receivable->id)}}" method="post">
					                  	@csrf
					                  	@method('DELETE')
					                  	<div class="modal fade bs-example-modal{{$receivable->id}}" tabindex="-1" role="dialog" aria-hidden="true">
		                                    <div class="modal-dialog modal-sm">
		                                        <div class="modal-content">
		                                            <div class="modal-header btn-success">
		                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
		                                                </button>
		                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
		                                            </div>
		                                            <div class="modal-body">
		                                                Are you sure to pay this data?
		                                                
		                                                <br><br>Payment <br><input type="number" class="form-control" name="payment" value="0" required>
		                                                <br><br>Adjustment <br><input type="number" class="form-control" name="adjustment" value="0" required>
		                                            </div>
		                                            <div class="modal-footer">
		                                                <button type="submit" class="btn btn-success">Yes</button>
		                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
					                </form>
				                </td>
	            			@endif
				        </tr>
			        @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			          	<td class="noShow"></td>
			          	<td>No. SP</td>
			          	<td>Customer</td>
			          	<td>Product</td>
			          	<td>Date SP</td>
			          	<td>Date SJ</td>
			          	<td>Total</td>
			          	<td>Payment</td>
			          	<td>Adjustment</td>
			          	<td class="noShow"></td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	<div>
@stop