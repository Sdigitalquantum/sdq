<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('accounting')->unsigned();
            $table->integer('no_inc');
            $table->string('code');
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->tinyInteger('tax');
            $table->string('npwp_no')->nullable();
            $table->string('npwp_name')->nullable();
            $table->string('npwp_address')->nullable();
            $table->integer('npwp_city')->nullable();
            $table->bigInteger('limit');
            $table->bigInteger('piutang_idr')->default('0');
            $table->bigInteger('piutang_usd')->default('0');
            $table->text('note1')->nullable();
            $table->text('note2')->nullable();
            $table->text('note3')->nullable();
            $table->text('notice')->nullable();
            $table->tinyInteger('status_group');
            $table->tinyInteger('status_record');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->foreign('accounting')->references('id')->on('accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
