<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Stuff;
use App\Http\Models\Stufforder;
use App\Http\Models\Schedulestuff;
use App\Http\Models\Schedulebook;
use App\Http\Models\Quotation;
use App\Http\Models\Quotationdetail;
use App\Http\Models\Quotationpayment;
use App\Http\Models\Quotationsplit;
use App\Http\Models\Quotationplan;
use App\Http\Models\Plan;
use App\Http\Models\Stockin;
use App\Http\Models\Stockcard;
use App\Http\Models\Company;
use App\Http\Models\Journal;
use App\Http\Models\Journaldetail;
use App\Http\Models\Journalset;
use App\Http\Models\Receivable;
use App\Http\Models\Receivablecard;
use App\Http\Models\Employeeset;

class WeighController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $schedulestuffs = Schedulestuff::where([['status_approve','<>',0], ['status_stuff', 1]])->orderBy('schedulebook','desc')->get();
        $stuffs = Stuff::where([['status', 1], ['schedulestuff','<>',0]])->orderBy('created_at','desc')->get();

        return view('vendor/adminlte/weigh.index', compact('schedulestuffs','stuffs','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $stuff = Stuff::where('schedulestuff', $id)->first();
        $stuffs = Stuff::where('schedulestuff', $id)->orderBy('created_at','desc')->get();

        return view('vendor/adminlte/weigh.edit', compact('stuff','stuffs','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'container_empty'   => 'numeric|min:0|not_in:0',
            'container_bruto'   => 'numeric|min:0|not_in:0'
        ]);

        $stuff = Stuff::find($id);
        $stuff->container_empty   = $request->get('container_empty');
        $stuff->container_bruto   = $request->get('container_bruto');
        $stuff->container_netto   = $request->get('container_bruto')-$request->get('container_empty');
        $stuff->save();

        return redirect('/weigh/'.$stuff->schedulestuff.'/edit')->with('success', 'Weighing has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $schedulestuff = Schedulestuff::find($id);
        $schedulestuff->status_send = 1;
        $schedulestuff->sent_at     = new \DateTime();
        $schedulestuff->sent_user   = $session_id;
        $schedulestuff->save();

        return redirect('/pack')->with('success', 'Packing List has been sent');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $schedulestuffs = Schedulestuff::where([['status_approve','<>',0], ['status_stuff', 1]])->orderBy('schedulebook','desc')->get();

        return view('vendor/adminlte/weigh.list', compact('schedulestuffs','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function approval(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $schedulestuffs = Schedulestuff::where([['status_approve','<>',0], ['status_stuff', 1], ['status_send', 1]])->orderBy('schedulebook','desc')->get();

        return view('vendor/adminlte/weigh.approval', compact('schedulestuffs','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $schedulestuff = Schedulestuff::find($id);
        $schedulestuff->status_pack = 1;
        $schedulestuff->packed_at   = new \DateTime();
        $schedulestuff->packed_user = $session_id;
        $schedulestuff->save();

        $schedulebook = Schedulebook::where('id', $schedulestuff->schedulebook)->first();
        $quotationsplit = Quotationsplit::where('id', $schedulebook->quotationsplit)->first();
        $quotation = Quotation::where('id', $quotationsplit->quotation)->first();
        
        $quotationplans = Quotationplan::where('quotationsplit', $schedulebook->quotationsplit)->get();
        foreach ($quotationplans as $quotationplan) {
            $plans = Plan::where('id', $quotationplan->plan)->get();
            foreach ($plans as $plan) {
                $stockins = Stockin::where('id', $plan->stockin)->get();
                foreach ($stockins as $stockin) {
                    $stockin->qty_out       = $stockin->qty_out+$stockin->qty_transit;
                    $stockin->qty_transit   = 0;
                    $stockin->date_out      = $quotationsplit->etd;
                    $stockin->noref_out     = $quotationsplit->no_split;
                    $stockin->status_out    = 1;
                    $stockin->save();

                    $stockcard = Stockcard::where([['product', $stockin->product], ['year', date('Y', strtotime($stockin->date_in))], ['month', date('m', strtotime($stockin->date_in))]])->first();
                    $stockcard->qty_out = $stockcard->qty_out+$stockin->qty_out;
                    $stockcard->save();
                }
            }
        }

        return redirect('/approvalpack')->with('success', 'Packing List has been confirmed');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function pack(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $schedulestuffs = Schedulestuff::where([['status_approve','<>',0], ['status_stuff', 1], ['status_send', 1], ['status_pack', 1]])->orderBy('schedulebook','desc')->get();

        return view('vendor/adminlte/weigh.pack', compact('schedulestuffs','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function packpreview(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $schedulestuff = Schedulestuff::find($id);
        $schedulestuff->qty_print = $schedulestuff->qty_print+1;
        $schedulestuff->save();

        $stuffs = Stuff::where('schedulestuff', $id)->orderBy('created_at','desc')->get();
        $quotationsplit = Quotationsplit::find($schedulestuff->fkSchedulebook->quotationsplit);
        $quotation = Quotation::where('id', $quotationsplit->quotation)->first();
        $quotationpayments = Quotationpayment::where([['quotation', $quotation->id], ['status', 1]])->orderBy('payment','desc')->get();
        
        $quotationplan = Quotationplan::where('quotationsplit', $quotationsplit->id)->first();
        $plan = Plan::where('id', $quotationplan->plan)->first();
        $quotationdetails = Quotationdetail::where('id', $plan->quotationdetail)->get();
        $company = Company::where('status', 1)->first();

        return view('vendor/adminlte/weigh.packpreview', compact('schedulestuff','stuffs','stufforder','quotation','quotationpayments','quotationdetails','company','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function invoice(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $schedulestuffs = Schedulestuff::where([['status_approve','<>',0], ['status_stuff', 1], ['status_send', 1], ['status_pack', 1]])->orderBy('schedulebook','desc')->get();
        $journals = Journal::where('status', 1)->get();

        return view('vendor/adminlte/weigh.invoice', compact('schedulestuffs','journals','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function invoicejournal(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        
        $schedulestuff = Schedulestuff::find($id);
        $schedulestuff->status_journal = 1;
        $schedulestuff->save();

        $stufforder = Stufforder::where('schedulestuff', $id)->first();
        $stuff = Stuff::where([['schedulestuff', $id], ['status', 1]])->first();
        $quotationsplit = Quotationsplit::find($schedulestuff->fkSchedulebook->quotationsplit);
        $quotationplan = Quotationplan::where('quotationsplit', $quotationsplit->id)->first();
        $plan = Plan::where('id', $quotationplan->plan)->first();
        $quotationdetail = Quotationdetail::where('id', $plan->quotationdetail)->first();

        $journaldetails = Journaldetail::where([['journal', $request->get('journal')], ['status', 1]])->get();
        foreach ($journaldetails as $journaldetail) {
            $journalset = new Journalset([
                'code_journal'      => $journaldetail->fkJournal->code,
                'name_journal'      => $journaldetail->fkJournal->name,
                'code_accounting'   => $journaldetail->fkAccount->code,
                'name_accounting'   => $journaldetail->fkAccount->name,
                'no_inv'            => $quotationsplit->no_split,
                'price'             => $stuff->qty_load*$quotationdetail->price*$quotationdetail->kurs,
                'status'            => ($journaldetail->debit == 1) ? 'D' : 'K',
                'created_user'      => $session_id
            ]);
            $journalset->save();
        }
        
        $receivable = new Receivable([
            'no_sp'             => $schedulestuff->fkSchedulebook->fkQuotationsplit->no_split,
            'code_customer'     => $schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->fkCustomerdetail->fkCustomer->code,
            'name_customer'     => $schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->fkCustomerdetail->name,
            'code_product'      => $quotationdetail->fkProduct->code,
            'name_product'      => $quotationdetail->fkProduct->name,
            'date_sp'           => $schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->date_sp,
            'date_sj'           => date('Y-m-d', strtotime($stufforder->created_at)),
            'qty'               => $stuff->qty_load,
            'price'             => $quotationdetail->price*$quotationdetail->kurs,
            'total'             => $stuff->qty_load*$quotationdetail->price*$quotationdetail->kurs,
            'payment'           => 0,
            'adjustment'        => 0,
            'created_user'      => $session_id,
            'updated_user'      => $session_id
        ]);
        $receivable->save();

        $saldocard = Receivablecard::where([['customerdetail', $schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->customerdetail], ['year', date('Y', strtotime($schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->date_sp))], ['month', date('m', strtotime($schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->date_sp))]])->first();
        if (empty($saldocard)) {
            $receivablecard = new Receivablecard([
                'customerdetail'    => $schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->customerdetail,
                'year'              => date('Y', strtotime($schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->date_sp)),
                'month'             => date('m', strtotime($schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->date_sp)),
                'debit'             => $stuff->qty_load*$quotationdetail->price*$quotationdetail->kurs,
                'kredit'            => 0
            ]);
            $receivablecard->save();
        } else {
            $saldocard->debit = $saldocard->debit+($stuff->qty_load*$quotationdetail->price*$quotationdetail->kurs);
            $saldocard->save();
        }

        return redirect('/invoice')->with('success', 'Journal has been processed');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function invoicepreview(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $schedulestuff = Schedulestuff::find($id);
        $schedulestuff->qty_invoice = $schedulestuff->qty_invoice+1;
        $schedulestuff->save();

        $stuffs = Stuff::where('schedulestuff', $id)->orderBy('created_at','desc')->get();
        $quotationsplit = Quotationsplit::find($schedulestuff->fkSchedulebook->quotationsplit);
        $quotation = Quotation::where('id', $quotationsplit->quotation)->first();
        $quotationpayments = Quotationpayment::where('quotation', $quotation->id)->orderBy('payment','desc')->get();
        
        $quotationplan = Quotationplan::where('quotationsplit', $quotationsplit->id)->first();
        $plan = Plan::where('id', $quotationplan->plan)->first();
        $quotationdetails = Quotationdetail::where('id', $plan->quotationdetail)->get();
        $company = Company::where('status', 1)->first();

        return view('vendor/adminlte/weigh.invoicepreview', compact('schedulestuff','stuffs','stufforder','quotation','quotationpayments','quotationdetails','company','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function emkl(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $schedulestuffs = Schedulestuff::where([['status_approve','<>',0], ['status_stuff', 1]])->orderBy('schedulebook','desc')->get();

        return view('vendor/adminlte/weigh.emkl', compact('schedulestuffs','disable','employeeroots','employeemenus','employeesubs'));
    }
}
