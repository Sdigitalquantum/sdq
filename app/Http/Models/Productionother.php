<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Productionother extends Model
{
    protected $fillable = [
	    'production',
	    'product',
	    'unit',
	    'qty',
	    'created_user',
	    'updated_user'
  	];

	public function fkProduction(){
	    return $this->belongsTo('\App\Http\Models\Production', 'production', 'id');
	}

  	public function fkProduct(){
	    return $this->belongsTo('\App\Http\Models\Product', 'product', 'id');
	}

	public function fkUnit(){
	    return $this->belongsTo('\App\Http\Models\Unit', 'unit', 'id');
	}
}
