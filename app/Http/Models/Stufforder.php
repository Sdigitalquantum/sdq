<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Stufforder extends Model
{
    protected $fillable = [
	    'schedulestuff',
	    'quotationsplit',
	    'no_inc',
	    'no_letter',
	    'check_quality',
	    'check_weigh',
	    'notice',
	    'created_user'
  	];

  	public function fkSchedulestuff(){
	    return $this->belongsTo('\App\Http\Models\Schedulestuff', 'schedulestuff', 'id');
	}

	public function fkQuotationsplit(){
	    return $this->belongsTo('\App\Http\Models\Quotationsplit', 'quotationsplit', 'id');
	}
}
