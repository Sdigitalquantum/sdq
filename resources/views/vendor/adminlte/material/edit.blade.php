@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
            <li class="disabled"><a>Raw Material</a></li>
            <li class="disabled"><a>Excess</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Bill of Material
						&nbsp;<button id="detailBtn" class="btn btn-xs btn-warning" value="{{ route('materialdetail.show', $id) }}">Raw Material</button>
						&nbsp;<button id="excessBtn" class="btn btn-xs btn-warning" value="{{ route('materialother.show', $id) }}">Excess</button>

			    			<!-- &nbsp;<a href="{{ route('materialdetail.show', $id) }}" class="btn btn-xs btn-primary" style="text-decoration: none;">Raw Material</a>
			    			&nbsp;<a href="{{ route('materialother.show', $id) }}" class="btn btn-xs btn-primary" style="text-decoration: none;">Excess</a> -->
			    		</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form id="customerFrm"> <!-- method="post" action="{{ route('material.update', $material->id) }}"> -->
				      		@method('PATCH')
				      		@csrf
				      		<div class="row">
				          		<div class="col-md-12">
						      		<div class="form-group">
						              	<label for="product">Product</label>
						              	<select class="form-control select2" name="product" required>
						              		@foreach($products as $product)
											    @if ($material->product == $product->id)
												    <option value="{{ $product->id }}" selected>{{ $product->code }} - {{ $product->name }}</option>
												@else
												    <option value="{{ $product->id }}">{{ $product->code }} - {{ $product->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						      		<div class="form-group">
						          		<label for="check">&nbsp;</label>
						          		@if($material->status_group == 1)
						          			<input type="checkbox" name="status_group" value="1" checked> Status Group &nbsp;&nbsp;&nbsp;
						          		@else
						          			<input type="checkbox" name="status_group" value="1"> Status Group &nbsp;&nbsp;&nbsp;
						          		@endif

						          		@if($material->status_record == 1)
						          			<input type="checkbox" name="status_record" value="1" checked> Status Record
						          		@else
						          			<input type="checkbox" name="status_record" value="1"> Status Record
						          		@endif
					          		</div>
									<div class="form-group">
									<button id="updateBtn" value="{{ route('material.update', $material->id) }}" class="btn btn-sm btn-success">Update</button>
				          				<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
										<!-- <button type="submit" class="btn btn-sm btn-success">Update</button>
						          		<a href="{{ url('/material') }}" class="btn btn-sm btn-danger">Cancel</a> -->
					          		</div>
				          		</div>
			          		</div>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.material.prepare_edit());
    </script>
@stop