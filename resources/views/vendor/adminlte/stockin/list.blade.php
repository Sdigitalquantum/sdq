@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Moving Stock</h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="5%">No</th>
			          	<th width="20%">Product</th>
			          	<th width="15%">Warehouse</th>
			          	<th width="15%">Supplier</th>
			          	<th width="10%">Date</th>
			          	<th width="10%">Qty Bag</th>
			          	<th width="10%">Qty Pcs</th>
			          	<th width="10%">Qty Kg</th>
			          	<th width="5%">Action</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@php ($array = [])
			    	@foreach($employeewarehouses as $employeewarehouse)
			    		@php ($array[] = $employeewarehouse->warehouse)
			    	@endforeach

			    	@php ($no = 0)
			    	@foreach($stockins as $stockin)
			    		@if(in_array($stockin->warehouse, $array))
				    		@php ($no++)
				        	<tr>
					            <td>{{$no}}</td>
					            <td>{{$stockin->fkProduct->name}}</td>
					            <td>{{$stockin->fkWarehouse->name}}</td>
					            <td>{{$stockin->fkSupplierdetail->name}}</td>
					            <td>{{$stockin->date_in}}</td>
					            <td style="text-align: right;">{{number_format($stockin->qty_bag, 0, ',' , '.')}} &nbsp;</td>
					            <td style="text-align: right;">{{number_format($stockin->qty_pcs, 0, ',' , '.')}} &nbsp;</td>
					            <td style="text-align: right;">{{number_format($stockin->qty_kg, 0, ',' , '.')}} &nbsp;</td>

					            <td>
					            	<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$stockin->id}}">Move</button>

					            	<form action="{{ url('stockin/change', $stockin->id)}}" method="post">
					                  	@csrf
					                  	@method('DELETE')
					                  	<div class="modal fade bs-example-modal{{$stockin->id}}" tabindex="-1" role="dialog" aria-hidden="true">
		                                    <div class="modal-dialog modal-sm">
		                                        <div class="modal-content">
		                                            <div class="modal-header btn-success">
		                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
		                                                </button>
		                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
		                                            </div>
		                                            <div class="modal-body">
		                                                Are you sure to move this data?
		                                                
		                                                <br><br>Warehouse
		                                                <br><select class="form-control select2" name="warehouse" required>
										              		@foreach($warehouses as $warehouse)
															    @if ($stockin->warehouse == $warehouse->id)
																    <option value="{{ $warehouse->id }}" selected>{{ $warehouse->code }} - {{ $warehouse->name }}</option>
																@else
																	@if(in_array($warehouse->id, $array))
																    	<option value="{{ $warehouse->id }}">{{ $warehouse->code }} - {{ $warehouse->name }}</option>
															    	@endif
																@endif
															@endforeach
									                	</select>
									                	
									                	<br><br>Date <br><input type="text" class="form-control datepicker" name="date_in" value="{{ old('date_in') }}" required>
									                	<br><br>Qty Bag <br><input type="number" class="form-control" name="qty_bag" value="{{ old('qty_bag') }}" required>
									                	<br><br>Qty Pcs <br><input type="number" class="form-control" name="qty_pcs" value="{{ old('qty_pcs') }}" required>
									                	<br><br>Qty Kg <br><input type="number" class="form-control" name="qty_kg" value="{{ old('qty_kg') }}" required>
		                                            </div>
		                                            <div class="modal-footer">
		                                                <button type="submit" class="btn btn-success">Yes</button>
		                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
					                </form>
					            </td>
					        </tr>
				        @endif
			        @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			          	<td class="noShow"></td>
			          	<td>Product</td>
			          	<td>Warehouse</td>
			          	<td>Supplier</td>
			          	<td>Date</td>
			          	<td>Qty Bag</td>
			          	<td>Qty Pcs</td>
			          	<td>Qty Kg</td>
			          	<td class="noShow"></td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	<div>
@stop