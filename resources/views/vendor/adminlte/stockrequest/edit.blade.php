@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Stuffing</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
				<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		@php($netto = 0)
  						@foreach($quotationplans as $quotationplan)
	  						@foreach($plans as $plan)
	  							@if($plan->id == $quotationplan->plan)
	  								@php($netto = $netto + $plan->qty_plan)
  								@endif
	  						@endforeach
  						@endforeach

				  		<h4 class="box-title">
				  			Split Proforma <b>{{ $stuff->fkSchedulestuff->fkSchedulebook->fkQuotationsplit->no_split }}</b>
				  			&nbsp;<font color="white">[ <b>{{ $schedulestuff->qty_approve }} Container - {{ $netto }} kg</b> ]</font>
				  		</h4>
			  		</div>
				  	
				  	<div class="box-body">
				  		<form method="post" action="{{ route('stockrequest.update', $stuff->id) }}">
				      		@method('PATCH')
				      		@csrf
				          	<div class="row">
    							<div class="col-md-6">
    								<div class="form-group">
						              	<label for="container">Vehicle</label>
						              	<select class="form-control select2" name="container" required>
						              		<option value="{{ old('container') }}">Choose One</option>
					                    	@foreach($containers as $container)
											    @if ($stuff->container == $container->id)
												    @if($container->size == 0)
						                    			<option value="{{ $container->id }}" selected>{{ $container->name }}</option>
												    @else
												    	<option value="{{ $container->id }}" selected>{{ $container->size }} - {{ $container->name }}</option>
											    	@endif
												@else
												    @if($container->size == 0)
						                    			<option value="{{ $container->id }}">{{ $container->name }}</option>
												    @else
												    	<option value="{{ $container->id }}">{{ $container->size }} - {{ $container->name }}</option>
											    	@endif
												@endif
											@endforeach
					                	</select>
						          	</div>
    								<div class="form-group">
						          		<label for="vehicle">Container</label>
						              	<input type="text" class="form-control" name="vehicle" value="{{ $stuff->vehicle }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_bag">Qty Bag</label>
						              	<input type="number" class="form-control" name="qty_bag" value="{{ $stuff->qty_bag }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_pcs">Qty Pcs</label>
						              	<input type="number" class="form-control" name="qty_pcs" value="{{ $stuff->qty_pcs }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_kg">Qty Kg</label>
						              	<input type="number" class="form-control" name="qty_kg" value="{{ $stuff->qty_kg }}" required>
						          	</div>
								</div>

								<div class="col-md-6">
						          	<div class="form-group">
						          		<label for="nopol">Nopol</label>
						              	<input type="text" class="form-control" name="nopol" value="{{ $stuff->nopol }}">
						          	</div>
									<div class="form-group">
						          		<label for="driver">Driver</label>
						              	<input type="text" class="form-control" name="driver" value="{{ $stuff->driver }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="mobile">Mobile</label>
						              	<input type="number" class="form-control" name="mobile" value="{{ $stuff->mobile }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="notice">Note</label>
						              	<textarea class="form-control" name="notice" rows="5" maxlength="150">{{ $stuff->notice }}</textarea>
						          	</div>
								</div>
							</div>

							<button type="submit" class="btn btn-sm btn-success">Update</button>
          					<a href="{{ url('/stockrequest/create', $stuff->schedulestuff) }}" class="btn btn-sm btn-danger">Cancel</a>
						</form>

		          		<br><table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="15%">Container</th>
						          	<th width="15%">Quantity</th>
						          	<th width="25%">Vehicle</th>
						          	<th width="35%">Note</th>
						          	<th width="10%" style="text-align: center;">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@foreach($stuffs as $stuff)
						    		<tr>
							            <td>
							            	@if($stuff->fkContainer->size == 0)
				                    			{{ $stuff->fkContainer->name }}
										    @else
										    	{{ $stuff->fkContainer->size }}" {{ $stuff->fkContainer->name }}
									    	@endif
							            	
							            	<br>{{$stuff->vehicle}}
							            </td>
							            <td style="text-align: right;">
							            	Qty Bag : {{$stuff->qty_bag}} &nbsp; <br>
							            	Qty Pcs : {{$stuff->qty_pcs}} &nbsp; <br>
							            	Qty Kg : {{$stuff->qty_kg}} &nbsp;&nbsp;
							            </td>
							            <td>
							            	<table>
							            		<tr>
							            			<td>Nopol</td>
							            			<td>&nbsp;: {{$stuff->nopol}}</td>
						            			</tr>
						            			<tr>
							            			<td>Driver</td>
							            			<td>&nbsp;: {{$stuff->driver}}</td>
						            			</tr>
						            			<tr>
							            			<td>Phone</td>
							            			<td>&nbsp;: {{$stuff->mobile}}</td>
						            			</tr>
						            		</table>
							            </td>
							            <td>{{$stuff->notice}}</td>
							            
							            @if($stuff->fkSchedulestuff->status_weigh == 1)
							            	<td><font color="green">Weighed <i class="glyphicon glyphicon-ok"></i></font></td>
							            @elseif($stuff->status == 1)
								            <td>
								            	<a href="{{ route('stockrequest.edit',$stuff->id)}}" class="btn btn-xs btn-warning">Edit</a>
								            </td>
							            @elseif($stuff->status == 0)
						            		<td><font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font></td>
					            		@elseif($stuff->status == 2)
						            		<td><font color="red">Canceled <i class="glyphicon glyphicon-remove"></i></font></td>
							            @endif
									</tr>
					    		@endforeach
						    </tbody>

						    <tfoot>
						    	<tr>
						    		<td>Container</td>
						    		<td>Quantity</td>
						    		<td>Vehicle</td>
						    		<td>Notice</td>
						    		<td class="noShow"></td>
					    		</tr>
					    	</tfoot>
					    </table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop