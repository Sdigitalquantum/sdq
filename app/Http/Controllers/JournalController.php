<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Journal;
use App\Http\Models\Journaldetail;
use App\Http\Models\Employeeset;

class JournalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $journals = Journal::orderBy('created_at','desc')->get();
        $journaldetails = Journaldetail::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/journal.index', compact('journals','journaldetails','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        return view('vendor/adminlte/journal.create', compact('journals','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	
        $request->validate([
		    'code'    => 'unique:journals'
		]);

        $journal = new Journal([
            'code'          => $request->get('code'),
	        'name' 		    => $request->get('name'),
	        'notice'	    => $request->get('notice'),
            'created_user'  => $session_id,
            'updated_user'  => $session_id
	    ]);
	    $journal->save();

	    return redirect('/journaldetail/create/'.$journal->id)->with('success', 'Journal has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $journal = Journal::find($id);

        return view('vendor/adminlte/journal.edit', compact('journal','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

    	$journal = Journal::find($id);
        $code   = $journal->code;

        if ($code != $request->get('code')) {
            $request->validate([
                'code'      => 'unique:journals'
            ]);
        }
        
        $journal->code          = $request->get('code');
        $journal->name 	        = $request->get('name');
	    $journal->notice        = $request->get('notice');
	    $journal->updated_user 	= $session_id;
	    $journal->save();

	    return redirect('/journal')->with('success', 'Journal has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $journal = Journal::find($id);
        $status = $journal->status;

        if ($status == 1) {
        	$journal->status       = 0;
            $journal->updated_user = $session_id;
	        $journal->save();

		    return redirect('/journal')->with('error', 'Journal has been deleted');
        } elseif ($status == 0) {
        	$journal->status       = 1;
            $journal->updated_user = $session_id;
	        $journal->save();

		    return redirect('/journal')->with('success', 'Journal has been activated');
        }
    }
}
