@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">New Purchase General</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('purchasegeneral.store') }}">
				          	@csrf
				          	<div class="form-group">
				          		<label for="date_order">Date</label>
				              	<input type="text" class="form-control datepicker" name="date_order" required>
				          	</div>
							<div class="form-group">
				          		<label for="notice">Note</label>
				          		<textarea class="form-control" name="notice" maxlength="150" rows="3"></textarea>
				          	</div>
				          	<div class="form-group">
				          		<button type="submit" class="btn btn-sm btn-success">Save</button>
				          		<a href="{{ url('/purchasegeneral') }}" class="btn btn-sm btn-danger">Cancel</a>
				          	</div>
						</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop