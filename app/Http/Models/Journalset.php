<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Journalset extends Model
{
    protected $fillable = [
	    'code_journal',
	    'name_journal',
	    'code_accounting',
	    'name_accounting',
	    'no_is',
	    'no_qc',
	    'no_inv',
	    'price',
	    'status',
	    'created_user'
  	];

  	public function fkEmployee(){
	    return $this->belongsTo('\App\Http\Models\Employee', 'created_user', 'id');
	}
}
