@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>User</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
                <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">User Warehouse</h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="45%">Warehouse</th>
						          	<th width="40%">User</th>
						          	<th width="10%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($warehouses as $warehouse)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>{{$warehouse->name}}</td>
							            <td>
							            	@foreach($employeewarehouses as $employeewarehouse)
							            		@if($employeewarehouse->warehouse == $warehouse->id)
							            			{{$employeewarehouse->fkEmployee->name}} - {{$employeewarehouse->fkEmployee->email}} <br>
							            		@endif
							            	@endforeach
							            </td>

							            <td>
							            	<a href="{{ route('employeewarehouse.edit',$warehouse->id)}}" class="btn btn-xs btn-success">Access Warehouse</a>
							            </td>
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Warehouse</td>
						          	<td>User</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop