<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;

class JournalsetsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $results = DB::table('journalsets')
            ->join('employees', 'journalsets.created_user', '=', 'employees.id')
            ->select('journalsets.no_is','journalsets.no_qc','journalsets.no_inv','journalsets.code_journal','journalsets.name_journal','journalsets.code_accounting','journalsets.name_accounting','journalsets.status','journalsets.price','journalsets.created_at','employees.name')
            ->get();

        return $results;
    }
}
