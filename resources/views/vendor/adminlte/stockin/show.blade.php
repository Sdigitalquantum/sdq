@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Monitoring Stock - Per Warehouse</h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="5%" rowspan="2" style="vertical-align: top; text-align: center;">No</th>
			          	<th width="20%" rowspan="2" style="vertical-align: top;">Product</th>
			          	<th width="15%" rowspan="2" style="vertical-align: top;">Warehouse</th>
			          	<th width="15%" rowspan="2" style="vertical-align: top;">Supplier</th>
			          	<th width="10%" rowspan="2" style="vertical-align: top; text-align: center;">Date</th>
			          	<th width="10%" rowspan="2" style="vertical-align: top;">Price</th>
			          	<th width="25%" colspan="5" style="text-align: center;">Quantity (Kg)</th>
			        </tr>
			        <tr>
			          	<th width="5%" style="text-align: center;">In</th>
			          	<th width="5%" style="text-align: center;">Out</th>
			          	<th width="5%" style="text-align: center;">Move</th>
			          	<th width="5%" style="text-align: center;">Transit</th>
			          	<th width="5%" style="text-align: center;">Booked</th>
			        </tr>
			    </thead>

			    <tbody>
		    		@php ($array = [])
			    	@foreach($employeewarehouses as $employeewarehouse)
			    		@php ($array[] = $employeewarehouse->warehouse)
			    	@endforeach

			    	@php ($no = 0)
			    	@foreach($stockins as $stockin)
			    		@if(in_array($stockin->warehouse, $array))
				    		@php ($no++)
				        	<tr>
					            <td style="text-align: center;">{{$no}}</td>
					            <td>[{{$stockin->fkProduct->code}}] {{$stockin->fkProduct->name}}</td>
					            <td>{{$stockin->fkWarehouse->name}}</td>
					            <td>{{$stockin->fkSupplierdetail->name}}</td>
					            <td style="text-align: center;">{{$stockin->date_in}}</td>
					            <td style="text-align: right;">{{number_format($stockin->price, 0, ',' , '.')}} &nbsp;</td>

					            @if ($stockin->qty_available == 0)
					            	<td style="text-align: right; background: red; color: white;">{{number_format($stockin->qty_validate, 0, ',' , '.')}} &nbsp;</td>
					            @else
					            	<td style="text-align: right; background: green; color: white;">{{number_format($stockin->qty_validate, 0, ',' , '.')}} &nbsp;</td>
					            @endif

					            <td style="text-align: right;">{{number_format($stockin->qty_out, 0, ',' , '.')}} &nbsp;</td>
					            <td style="text-align: right;">{{number_format($stockin->qty_move, 0, ',' , '.')}} &nbsp;</td>
					            <td style="text-align: right;">{{number_format($stockin->qty_transit, 0, ',' , '.')}} &nbsp;</td>
					            <td style="text-align: right;">{{number_format($stockin->qty_booked, 0, ',' , '.')}} &nbsp;</td>
					        </tr>
				        @endif
			        @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			          	<td class="noShow"></td>
			          	<td>Product</td>
			          	<td>Warehouse</td>
			          	<td>Supplier</td>
			          	<td>Date</td>
			          	<td>Price</td>
			          	<td class="noShow"></td>
			          	<td class="noShow"></td>
			          	<td class="noShow"></td>
			          	<td class="noShow"></td>
			          	<td class="noShow"></td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	<div>
@stop