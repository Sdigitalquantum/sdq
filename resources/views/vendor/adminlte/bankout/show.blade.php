@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #86c232 !important; font-weight: bold;">
			    		<h4 class="box-title">DETAIL &nbsp;<a href="{{ url('/bankout') }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action=" ">
				          	@csrf
				          	<div class="row">
    							<div class="col-md-6">
                                    <div class="form-group">
						              	<label for="">Account Number</label>
						              	<input type="text" class="form-control" name="" value="">
						          	</div>
									<div class="form-group">
						              	<label for="">Description/Item</label>
						              	<input type="text" class="form-control datepicker" name="" value="">
						          	</div>
								</div>
								<div class="col-md-6">
                                      <div class="form-group">
						              	<label for="">Amount</label>
						              	<input type="number" class="form-control" name="" value="">
						          	</div>
									  <div class="form-group">
						              	<label for="">Total</label>
						              	<input type="number" placeholder="Total dari list item" class="form-control" name="" value="" readonly>
						          	</div>
					          	</div>
							</div>
				          	<a href="#" class="btn btn-sm btn-success">Save</a>
			          		<a href="#" class="btn btn-sm btn-danger">Cancel</a>
				      	</form>
				  	</div>
				</div>
			</div>
			  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="10%">Account Number</th>
						          	<th width="20%">Account</th>
						          	<th width="25%">Description</th>
						          	<th width="5%">Status D/K</th>
						          	<th width="15%">Amount</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>
						    <tbody>

						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>No.</td>
						          	<td>COA</td>
						          	<td>Descrition</td>
						          	<td>Status D/K</td>
						          	<td>Ammount</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
		</div>
	</div>
@stop