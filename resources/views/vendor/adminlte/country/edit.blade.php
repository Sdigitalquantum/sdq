@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #6b6e70 !important; font-weight: bold; color: white;">
			    		<h4 class="box-title">Edit Country</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form id="countryFrm" > <!-- method="post" action="{{ route('country.update', $country->id) }}" -->
				      		@method('PATCH')
				      		@csrf
				          	<div class="form-group">
				              	<label for="code">Code</label>
				              	<input type="text" class="form-control" name="code" value="{{ $country->code }}" disabled>
				          	</div>
				          	<div class="form-group">
				              	<label for="name">Name</label>
				              	<input type="text" class="form-control" name="name" value="{{ $country->name }}" required>
				          	</div>
				          	<div class="form-group">
				              	<label for="mobile">Mobile (+)</label>
				              	<input type="number" class="form-control" name="mobile" value="{{ $country->mobile }}" required>
				          	</div>
				          	<!-- <button onclick="sdq.core.submitForm('countryFrm', '{{ route("country.update", $country->id) }}', 'country' );return false;" class="btn btn-sm btn-success">Update</button>
				          	<a href="#" onclick="sdq.core.load('country');return false;" class="btn btn-sm btn-danger">Cancel</a> -->

				          	<button id="updateBtn" value="{{ route('country.update', $country->id) }}" class="btn btn-sm btn-success">Update</button>

				          	<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('content_js')
	<script>
    	$(document).ready(sdq.country.prepare_edit());
    </script>
@stop