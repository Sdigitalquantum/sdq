<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduleships', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pol');
            $table->string('pod');
            $table->string('lines');
            $table->date('etd');
            $table->date('eta');
            $table->tinyInteger('status_book')->default('0');
            $table->timestamps();
            $table->integer('created_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheduleships');
    }
}
