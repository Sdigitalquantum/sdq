<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\City;
use App\Http\Models\Country;
use App\Http\Models\Employeeset;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $cities = City::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/city.index', compact('cities','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        // $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        // $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        // $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $countries = Country::where('status', 1)->get();
        $city = City::where('status', 1)->get();

        return view('vendor/adminlte/city.create', compact('countries','disable', 'city'));
        //,'employeeroots','employeemenus','employeesubs'
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
	    $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'name' => 'unique:cities'
                ]);

                $no = City::select('no_inc')->max('no_inc');
                if (empty($no)) {
                    $no_inc = 1;
                } else {
                    $no_inc = $no + 1;
                }
                $code = "K".sprintf("%05s", $no_inc);

                $city = new City([
                    'country'       => $json->country,
                    'no_inc'        => $no_inc,
                    'code'          => $code,
                    'name'          => $json->name,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $city->save();
            };
        };
        return $this->jsonSuccess( $result );
        // return redirect('/city')->with('success', 'City has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        // $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        // $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        // $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $city = City::find($id);
        $countrys = Country::where('status', 1)->get();

        return view('vendor/adminlte/city.edit', compact('city','countrys','disable'));
        //,'employeeroots','employeemenus','employeesubs'
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $city = City::find($id);
                $name = $city->name;

                if ($name != $json->name) {
                    $request->validate([
                        'name' => 'unique:cities'
                    ]);
                }
                
                $city->country         = $json->country;
                $city->name            = $json->name;
                $city->updated_user    = $session_id;
                $result = $city->save();
            };
        };
        return $this->jsonSuccess( $result );
	    // return redirect('/city')->with('success', 'City has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $city = City::find($id);
        $status = $city->status;

        if ($status == 1) {
        	$city->status       = 0;
            $city->updated_user = $session_id;
	        $result = $city->save();

		    // return redirect('/city')->with('error', 'City has been deleted');
        } elseif ($status == 0) {
        	$city->status       = 1;
            $city->updated_user = $session_id;
	        $result = $city->save();

		    // return redirect('/city')->with('success', 'City has been activated');
        };
        return $this->jsonSuccess( $result );
    }
}
