@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Purchase Request</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('purchaserequest.update', $purchaserequest->id) }}">
				      		@method('PATCH')
				      		@csrf
				      		<div class="row">
    							<div class="col-md-6">
    								<div class="form-group">
						          		<label for="date_request">Date</label>
						              	<input type="text" class="form-control datepicker" name="date_request" value="{{ $purchaserequest->date_request }}" required>
						          	</div>
									<div class="form-group">
						              	<label for="product">Product</label>
						              	<select class="form-control select2" name="product" required>
						              		@foreach($products as $product)
											    @if ($purchaserequest->product == $product->id)
												    <option value="{{ $product->id }}" selected>{{ $product->code }} - {{ $product->name }}</option>
												@else
												    <option value="{{ $product->id }}">{{ $product->code }} - {{ $product->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="supplierdetail">Supplier</label>
						              	<select class="form-control select2" name="supplierdetail" required>
						              		@if ($purchaserequest->supplierdetail == 0)
											    <option value="0" selected>All Supplier</option>
											@endif

											@foreach($supplierdetails as $supplierdetail)
											    @if ($purchaserequest->supplierdetail == $supplierdetail->id)
												    <option value="{{ $supplierdetail->id }}" selected>{{ $supplierdetail->name }}</option>
												@else
												    <option value="{{ $supplierdetail->id }}">{{ $supplierdetail->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="warehouse">Warehouse</label>
						              	<select class="form-control select2" name="warehouse" required>
						              		@php ($array = [])
									    	@foreach($employeewarehouses as $employeewarehouse)
									    		@php ($array[] = $employeewarehouse->warehouse)
									    	@endforeach

						              		@if ($purchaserequest->warehouse == 0)
											    <option value="0" selected>All Warehouse</option>
											@endif

											@foreach($warehouses as $warehouse)
											    @if ($purchaserequest->warehouse == $warehouse->id)
												    <option value="{{ $warehouse->id }}" selected>{{ $warehouse->name }}</option>
												@else
												    @if(in_array($warehouse->id, $array))
												    	<option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
											    	@endif
												@endif
											@endforeach
					                	</select>
						          	</div>
								</div>

								<div class="col-md-6">
						          	<div class="form-group">
						          		<label for="qty_request">Qty Request (Kg)</label>
						              	<input type="number" class="form-control" name="qty_request" value="{{ $purchaserequest->qty_request }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="notice">Note</label>
						          		<textarea class="form-control" name="notice" maxlength="150" rows="5">{{ $purchaserequest->notice }}</textarea>
						          	</div>
						          	<div class="form-group">
						          		<label for="button"></label> <br>
						          		<button type="submit" class="btn btn-sm btn-success">Update</button>
						          		<a href="{{ url('/purchaserequest') }}" class="btn btn-sm btn-danger">Cancel</a>
						          	</div>
								</div>
							</div>
						</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop