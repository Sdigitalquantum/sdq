<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialothersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materialothers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('material')->unsigned();
            $table->integer('product')->unsigned();
            $table->integer('unit')->unsigned();
            $table->integer('qty');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->foreign('material')->references('id')->on('materials');
            $table->foreign('product')->references('id')->on('products');
            $table->foreign('unit')->references('id')->on('units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materialothers');
    }
}
