@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_import" data-toggle="tab">Import</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_import">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Import Schedule</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('import') }}" enctype="multipart/form-data">
				          	@csrf
				          	<div class="form-group">
				              	<label for="file">Excel File</label>
				              	<input type="file" name="file" class="form-control" required>
				          	</div>
				          	<button type="submit" class="btn btn-sm btn-success">Upload</button>
				          	<a href="{{ url('/scheduleship') }}" class="btn btn-sm btn-danger">Cancel</a>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop