@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="active"><a href="#tab_detail" data-toggle="tab">Detail</a></li>
            <li class="disabled"><a>Payment</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_detail">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Quotation Detail <b>{{ $quotationdetail->fkQuotation->no_sp }}</b></h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<form method="post" action="{{ route('quotationdetail.update', $quotationdetail->id) }}">
				      		@method('PATCH')
				      		@csrf
				      		<input type="hidden" id="id_url" value="{{ $quotationdetail->id }}">
				      		<div class="row">
					  			<div class="col-md-6">
						          	<input type="hidden" class="form-control" name="quotation" value="{{ $quotationdetail->quotation }}">
						          	<div class="form-group">
						              	<label for="product">Product</label>
						              	<select class="form-control select2" name="product" id="ajaxproduct" required>
						              		<option value="{{ old('product') }}">Choose One</option>
					                    	@foreach($products as $product)
											    @if ($quotationdetail->product == $product->id)
												    <option value="{{ $product->id }}" selected>{{ $product->name }}</option>
												@else
												    <option value="{{ $product->id }}">{{ $product->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_fcl">Qty Fcl</label>
						              	<input type="number" class="form-control" name="qty_fcl" value="{{ $quotationdetail->qty_fcl }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_bag">Qty Bag</label>
						              	<input type="number" class="form-control" name="qty_bag" value="{{ $quotationdetail->qty_bag }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_pcs">Qty Pcs</label>
						              	<input type="number" class="form-control" name="qty_pcs" value="{{ $quotationdetail->qty_pcs }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_kg">Qty Kg</label>
						              	<input type="number" class="form-control" name="qty_kg" value="{{ $quotationdetail->qty_kg }}" required>
						          	</div>
				  				</div>

				  				<div class="col-md-6">
						          	<div class="form-group">
						          		<label for="alias_name">Alias Name</label>
						              	<input type="text" class="form-control" name="alias_name" value="{{ $quotationdetail->alias_name }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="alias_qty">Alias Unit</label>
						              	<input type="text" class="form-control" name="alias_qty" value="{{ $quotationdetail->alias_qty }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="price">{{ $quotationdetail->fkQuotation->fkCurrency->code }} Price @Kg</label>
						              	<textarea class="form-control" name="price" id="ajaxprice" rows="1" required>{{ $quotationdetail->price }}</textarea>
						          	</div>
						          	<div class="form-group">
						          		<label for="disc_type">Discount</label> <br>
						          		@if($quotationdetail->disc_type == 0)
						          			<input type="radio" name="disc_type" value="1"> % &nbsp;&nbsp;&nbsp;
						              		<input type="radio" name="disc_type" value="2"> Rp / $ &nbsp;&nbsp;&nbsp;
						          		@elseif($quotationdetail->disc_type == 1)
						          			<input type="radio" name="disc_type" value="1" checked> % &nbsp;&nbsp;&nbsp;
						              		<input type="radio" name="disc_type" value="2"> Rp / $ &nbsp;&nbsp;&nbsp;
					              		@elseif($quotationdetail->disc_type == 2)
						          			<input type="radio" name="disc_type" value="1"> % &nbsp;&nbsp;&nbsp;
						              		<input type="radio" name="disc_type" value="2" checked> Rp / $ &nbsp;&nbsp;&nbsp;
						          		@endif
						              	<input type="number" class="form-control" name="disc_value" value="{{ $quotationdetail->disc_value }}" step=".01">
						          	</div>
						          	<div class="form-group">
						          		<button type="submit" class="btn btn-sm btn-success">Update</button>
				          				<a href="{{ url('/quotationdetail/'.$quotationdetail->quotation) }}" class="btn btn-sm btn-danger">Cancel</a>
						          	</div>
				  				</div>
			  				</div>
		  				</form>

		  				<table class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="30%">Product</th>
						          	<th width="15%">Unit</th>
						          	<th width="10%">Qty</th>
						          	<th width="20%">Price @Kg</th>
						          	<th width="15%">Total</th>
						          	<th width="10%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($total = 0)
						    	@foreach($quotationdetails as $quotationdetail)
						    		<tr>
							            <td>
							            	Name : {{$quotationdetail->fkProduct->name}} <br>
							            	Alias &nbsp; : {{$quotationdetail->alias_name}}
							            </td>
							            <td>
							            	Name : {{$quotationdetail->fkUnit->name}} <br>
							            	Alias &nbsp; : {{$quotationdetail->alias_qty}}
							            </td>
							            <td style="text-align: right;">
							            	Bag : {{number_format($quotationdetail->qty_bag, 0, ',' , '.')}} &nbsp; <br>
							            	Pcs : {{number_format($quotationdetail->qty_pcs, 0, ',' , '.')}} &nbsp; <br>
							            	Kg : {{number_format($quotationdetail->qty_kg, 0, ',' , '.')}} &nbsp;&nbsp;
							            </td>

							            @if($quotationdetail->kurs == 0 || $quotationdetail->fkQuotation->kurs)
							            	@php ($kurs = 1)
							            @else
							            	@php ($kurs = $quotationdetail->kurs)
							            @endif

							            <td style="text-align: right;">
							            	[{{$quotationdetail->fkQuotation->fkCurrency->code}}] 
							            	@if($quotationdetail->fkQuotation->fkCurrency->code == 'IDR')
							            		{{number_format($quotationdetail->price, 0, ',' , '.')}} &nbsp;
							            	@else
							            		{{$quotationdetail->price}} &nbsp;
							            	@endif
							            	
							            	@if($quotationdetail->disc_type == 0)
							            		<br>Kurs : Rp. {{number_format($kurs, 0, ',' , '.')}} &nbsp;&nbsp;
							            	@else
							            		<br>Kurs : Rp. {{number_format($kurs, 0, ',' , '.')}} &nbsp;
							            		@if($quotationdetail->disc_type == 2)
							            			<br>Disc. {{round(($quotationdetail->disc_value*$kurs)/($quotationdetail->price*$kurs*$quotationdetail->qty_kg)*100,2)}}% : Rp. {{number_format($quotationdetail->disc_value*$kurs, 0, ',' , '.')}} &nbsp;&nbsp;
						            			@else
						            				<br>Disc. {{$quotationdetail->disc_value}}% : Rp. {{number_format($quotationdetail->disc_value/100*($quotationdetail->price*$kurs*$quotationdetail->qty_kg), 0, ',' , '.')}} &nbsp;&nbsp;
						            			@endif
							            	@endif
							            </td>
							            <td style="text-align: right;">
							            	@if($quotationdetail->disc_type == 0)
							            		Rp. {{number_format($quotationdetail->price*$kurs*$quotationdetail->qty_kg, 0, ',' , '.')}} &nbsp;
							            	@elseif($quotationdetail->disc_type == 2)
							            		Rp. {{number_format(($quotationdetail->price*$kurs*$quotationdetail->qty_kg)-($quotationdetail->disc_value*$kurs), 0, ',' , '.')}} &nbsp;
						            		@else
						            			Rp. {{number_format(($quotationdetail->price*$kurs*$quotationdetail->qty_kg)-($quotationdetail->disc_value/100*($quotationdetail->price*$kurs*$quotationdetail->qty_kg)), 0, ',' , '.')}} &nbsp;
							            	@endif
							            </td>
							            <td>
							            	@if($quotationdetail->status_plan == 1) <font color="blue">Plan <i class="glyphicon glyphicon-ok"></i></font>
							            	@else
								            	@if($quotationdetail->status == 1) <a href="{{ route('quotationdetail.edit',$quotationdetail->id)}}" class="btn btn-xs btn-warning">Edit</a>
								            	@elseif($quotationdetail->status == 0) <font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
								            	@elseif($quotationdetail->status == 2) <font color="red">Canceled <i class="glyphicon glyphicon-remove"></i></font>
						            			@endif
							            	@endif
							            </td>

							            @if($quotationdetail->disc_type == 0 && $quotationdetail->status == 1)
						            		@php ($total = $total + ($quotationdetail->price*$kurs*$quotationdetail->qty_kg))
						            	@elseif($quotationdetail->disc_type == 2 && $quotationdetail->status == 1)
						            		@php ($total = $total + ($quotationdetail->price*$kurs*$quotationdetail->qty_kg) - ($quotationdetail->disc_value*$kurs))
						            	@elseif($quotationdetail->disc_type == 1 && $quotationdetail->status == 1)
						            		@php ($total = $total + ($quotationdetail->price*$kurs*$quotationdetail->qty_kg) - ($quotationdetail->disc_value/100*($quotationdetail->price*$kurs*$quotationdetail->qty_kg)))
					            		@endif
							        </tr>
						        @endforeach
							        @if($total != 0)
								        <tr>
								        	<td colspan="4" style="text-align: right; font-weight: bold;">Grand Total &nbsp;&nbsp;</td>
								        	<td style="text-align: right;">Rp. {{number_format($total, 0, ',' , '.')}} &nbsp;</td>
								        	<td>&nbsp;</td>
							        	</tr>
						        	@endif
						    </tbody>
					  	</table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop