@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
            <li class="active"><a href="#tab_payment" data-toggle="tab">Payment</a></li>
            <li class="disabled"><a>Charges</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_payment">
            	<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Quotation Payment <b>{{ $quotation->no_sp }}</b>
				  			&nbsp;<a href="{{ route('quotationcharge.show', $quotation->id) }}" class="btn btn-xs btn-primary" style="text-decoration: none;">Charges</a>
				  			&nbsp;<a href="{{ route('quotation.edit', $quotation->id) }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a>
				  		</h4>
			  		</div>
				  	
				  	<div class="box-body">
				  		<div class="row">
				  			<div class="col-md-3">
				  				<form method="post" action="{{ route('quotationpayment.store') }}">
						          	@csrf
						          	<input type="hidden" class="form-control" name="quotation" value="{{ $quotation->id }}">
						          	<div class="form-group">
						              	<label for="payment">Payment</label>
						              	<select class="form-control select2" name="payment" required>
						              		<option value="{{ old('payment') }}">Choose One</option>
					                    	@foreach($payments as $payment)
											    <option value="{{ $payment->id }}">{{ $payment->value }}% - {{ $payment->condition }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<button type="submit" class="btn btn-sm btn-success">Save</button>
						      	</form>
			  				</div>

			  				<div class="col-md-9">
			  					<br><table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="20%">Value</th>
								          	<th width="40%">Condition</th>
								          	<th width="25%">Payment</th>
								          	<th width="15%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@php ($payment = 0)
								    	@foreach($quotationpayments as $quotationpayment)
								    		<tr>
									            <td>{{$quotationpayment->fkPayment->value}}%</td>
									            <td>{{$quotationpayment->fkPayment->condition}}</td>
									            <td style="text-align: right;">
									            	@php ($total = 0)
					    							@foreach($quotationdetails as $quotationdetail)
					    								@if($quotationdetail->disc_type == 0)
										            		@php ($total = $total + ($quotationdetail->price*$quotationdetail->kurs*$quotationdetail->qty_kg))
										            	@elseif($quotationdetail->disc_type == 2)
										            		@php ($total = $total + ($quotationdetail->price*$quotationdetail->kurs*$quotationdetail->qty_kg) - ($quotationdetail->disc_value*$quotationdetail->fkQuotation->kurs))
										            	@else
										            		@php ($total = $total + ($quotationdetail->price*$quotationdetail->kurs*$quotationdetail->qty_kg) - ($quotationdetail->disc_value/100*($quotationdetail->price*$quotationdetail->kurs*$quotationdetail->qty_kg)))
									            		@endif
					    							@endforeach

					    							Rp. {{number_format($total*$quotationpayment->fkPayment->value/100, 0, ',' , '.')}} &nbsp;
									            </td>

									            @if($quotationpayment->status == 1)
										            <td>
										            	<a href="{{ route('quotationpayment.edit',$quotationpayment->id)}}" class="btn btn-xs btn-warning">Edit</a> &nbsp;
										            	
										            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$quotationpayment->id}}">Delete</button>

										            	<form action="{{ route('quotationpayment.destroy', $quotationpayment->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$quotationpayment->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-danger">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to delete this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
										            </td>
									            @else
									            	<td>
									            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$quotationpayment->id}}">Activated</button>

									            		<form action="{{ route('quotationpayment.destroy', $quotationpayment->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$quotationpayment->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-success">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to active this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
									            	</td>
									            @endif

									            @if($quotationpayment->status == 1)
									            	@php ($payment = $payment + ($total*$quotationpayment->fkPayment->value/100))
								            	@endif
									        </tr>
								        @endforeach

								        <tr>
								        	<td colspan="2" style="text-align: right; font-weight: bold;">Total Payment &nbsp;</td>
								        	<td style="text-align: right;">Rp. {{number_format($payment, 0, ',' , '.')}} &nbsp;</td>
								        	<td>&nbsp;</td>
							        	</tr>
								    </tbody>
							  	</table>
			  				</div>
		  				</div>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop