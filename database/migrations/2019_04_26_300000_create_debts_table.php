<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDebtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_po');
            $table->string('code_supplier');
            $table->string('name_supplier');
            $table->string('code_product');
            $table->string('name_product');
            $table->date('date_po');
            $table->date('date_qc');
            $table->integer('qty');
            $table->bigInteger('price');
            $table->bigInteger('total');
            $table->bigInteger('payment');
            $table->bigInteger('adjustment');
            $table->tinyInteger('status')->default('0');
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debts');
    }
}
