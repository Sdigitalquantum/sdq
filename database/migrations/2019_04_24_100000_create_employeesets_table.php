<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employeesets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee')->unsigned();
            $table->integer('menuroot')->unsigned();
            $table->integer('nomorroot');
            $table->integer('menu');
            $table->integer('nomormenu');
            $table->integer('menusub');
            $table->integer('nomorsub');
            $table->timestamps();
            $table->integer('created_user');
            $table->foreign('employee')->references('id')->on('employees');
            $table->foreign('menuroot')->references('id')->on('menuroots');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employeesets');
    }
}
