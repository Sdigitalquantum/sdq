@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Approval Purchase General</h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="5%">No</th>
			          	<th width="15%">No. PR</th>
			          	<th width="10%">Date</th>
			          	<th width="20%">Note</th>
			          	<th width="25%">Detail Request</th>
			          	<th width="20%">Reason</th>
			          	<th width="5%">Action</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@php ($array = [])
			    	@foreach($employeewarehouses as $employeewarehouse)
			    		@php ($array[] = $employeewarehouse->warehouse)
			    	@endforeach

			    	@php ($no = 0)
			    	@foreach($purchasegenerals as $purchasegeneral)
			    		@if(in_array($purchasegeneral->warehouse, $array) || $purchasegeneral->warehouse == 0)
				    		@php ($no++)
				        	<tr>
					            <td>{{$no}}</td>
					            <td>{{$purchasegeneral->no_prg}}</td>
					            <td>{{$purchasegeneral->date_order}}</td>
					            <td><pre>{{$purchasegeneral->notice}}</pre></td>
					            <td>
					            	<ul>
						            	@foreach($purchasedetailprgs as $purchasedetailprg)
						            		@if($purchasedetailprg->purchasegeneral == $purchasegeneral->id)
						            			<li>
						            				[{{ $purchasedetailprg->fkProduct->name }}]
						            				@if($purchasedetailprg->supplierdetail == 0)
						            					All Supplier
						            				@else
						            					{{ $purchasedetailprg->fkSupplierdetail->name }}
					            					@endif

						            				<br>[Qty : {{ number_format($purchasedetailprg->qty_request, 0, ',' , '.') }} {{ $purchasedetailprg->fkProduct->fkUnit->name }}]
						            				@if($purchasedetailprg->warehouse == 0)
						            					All Warehouse
						            				@else
						            					{{ $purchasedetailprg->fkWarehouse->name }}
					            					@endif
						            			</li>
						            		@endif
						            	@endforeach
					            	</ul>
					            </td>
					            <td><pre>{{$purchasegeneral->reason}}</pre></td>
								
								@if($purchasegeneral->status_approve == 0)
									<td>
						            	<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal-approve{{$purchasegeneral->id}}">Approve</button> &nbsp;
						                <button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal-reject{{$purchasegeneral->id}}">Reject</button>

						            	<form action="{{ url('purchasegeneral/approve', $purchasegeneral->id)}}" method="post">
						                  	@csrf
						                  	@method('DELETE')
						                  	<div class="modal fade bs-example-modal-approve{{$purchasegeneral->id}}" tabindex="-1" role="dialog" aria-hidden="true">
			                                    <div class="modal-dialog modal-sm">
			                                        <div class="modal-content">
			                                            <div class="modal-header btn-success">
			                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
			                                                </button>
			                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Approve</h4>
			                                            </div>
			                                            <div class="modal-body">
			                                            	Are you sure to approve this data?
			                                            	<br><textarea class="form-control" name="reason" maxlength="150" cols="30" rows="3" required></textarea>
			                                            </div>
			                                            <div class="modal-footer">
			                                                <button type="submit" class="btn btn-success">Yes</button>
			                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
						                </form>

						            	<form action="{{ url('purchasegeneral/reject', $purchasegeneral->id)}}" method="post">
						                  	@csrf
						                  	@method('DELETE')
						                  	<div class="modal fade bs-example-modal-reject{{$purchasegeneral->id}}" tabindex="-1" role="dialog" aria-hidden="true">
			                                    <div class="modal-dialog modal-sm">
			                                        <div class="modal-content">
			                                            <div class="modal-header btn-danger">
			                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
			                                                </button>
			                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Reject</h4>
			                                            </div>
			                                            <div class="modal-body">
			                                            	Are you sure to reject this data?
			                                            	<br><textarea class="form-control" name="reason" maxlength="150" cols="30" rows="3" required></textarea>
			                                            </div>
			                                            <div class="modal-footer">
			                                                <button type="submit" class="btn btn-success">Yes</button>
			                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
						                </form>
					            	</td>
				            	@elseif($purchasegeneral->status_approve == 1)
				            		<td><font color="green">Approved <i class="glyphicon glyphicon-ok"></i></font></td>
			            		@elseif($purchasegeneral->status_approve == 2)
				            		<td><font color="red">Rejected <i class="glyphicon glyphicon-remove"></i></font></td>
								@endif
					        </tr>
				        @endif
			        @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			          	<td class="noShow"></td>
			          	<td>No. PR</td>
			          	<td>Date</td>
			          	<td>Note</td>
			          	<td>Detail Request</td>
			          	<td>Reason</td>
			          	<td class="noShow"></td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	</div>
@stop