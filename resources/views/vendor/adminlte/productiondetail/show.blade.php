@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="active"><a href="#tab_alias" data-toggle="tab">Detail</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_alias">
            	<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Detail Assembly / Production &nbsp;<a href="{{ url('production') }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4>
			  		</div>
				  	
				  	<div class="box-body">
				  		<table class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="40%">Product</th>
						          	<th width="20%">Unit</th>
						          	<th width="20%">Qty</th>
						          	<th width="20%">Status</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@foreach($productiondetails as $productiondetail)
						    		<tr>
							            <td>
							            	@if($productiondetail->product == 0)
							            		{{$productiondetail->product_other}}
							            	@else
							            		{{$productiondetail->fkProduct->name}}
						            		@endif
							            </td>
							            <td>
							            	@if($productiondetail->unit == 0)
							            		{{$productiondetail->unit_other}}
							            	@else
							            		{{$productiondetail->fkUnit->name}}
						            		@endif
							            </td>
							            <td style="text-align: right;">{{$productiondetail->qty}} &nbsp;</td>
						            	<td><font color="blue">Raw Material <i class="glyphicon glyphicon-ok"></i></font></td>
							        </tr>
						        @endforeach

						        @foreach($productionothers as $productionother)
						    		<tr>
							            <td>
							            	@if($productionother->product == 0)
							            		{{$productionother->product_other}}
							            	@else
							            		{{$productionother->fkProduct->name}}
						            		@endif
							            </td>
							            <td>
							            	@if($productionother->unit == 0)
							            		{{$productionother->unit_other}}
							            	@else
							            		{{$productionother->fkUnit->name}}
						            		@endif
							            </td>
							            <td style="text-align: right;">{{$productionother->qty}} &nbsp;</td>
						            	<td><font color="blue">Excess <i class="glyphicon glyphicon-ok"></i></font></td>
							        </tr>
						        @endforeach
						    </tbody>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop