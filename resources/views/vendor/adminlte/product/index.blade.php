@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Alias</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
                <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #6b6e70 !important; font-weight: bold; color: white;">
				  		<!-- <h4 class="box-title">PRODUCT
				  			@if(empty($product))
				  				&nbsp;<a href="{{ route('product.create')}}" class="btn btn-xs btn-primary" style="text-decoration: none;">New</a>
			  				@endif -->
							  <h4 class="box-title">PRODUCT &nbsp;
				  			<button id="productNewBtn" class="btn btn-xs btn-primary">NEW</button>
						  </h4>
			  		</div>

				  	
				  	<!-- <div class="box-body"> -->
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						  <thead style="background: #86c232 !important; font-weight: bold; text-align: center; color: white;">

						        <tr>
						          	<th width="5%">No</th>
						          	<th width="20%">Name</th>
						          	<th width="10%">Variant</th>
						          	<th width="10%">Merk</th>
						          	<th width="10%">Type Sub</th>
						          	<th width="10%">Size</th>
						          	<th width="10%">Unit</th>
						          	<th width="10%">Accounting</th>
						          	<th width="10%">Status</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($products as $product)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>
							            	Code : {{$product->code}} <br>
							            	{{$product->name}}
							            </td>
							            <td>
							            	@if($product->variant == 0) -
							            	@else {{$product->fkVariant->name}}
							            	@endif
						            	</td>
							            <td>
							            	@if($product->merk == 0) -
							            	@else {{$product->fkMerk->name}}
							            	@endif
						            	</td>
						            	<td>
							            	@if($product->typesub == 0) -
							            	@else {{$product->fkTypesub->name}}
							            	@endif
						            	</td>
						            	<td>
							            	@if($product->size == 0) -
							            	@else {{$product->fkSize->name}}
							            	@endif
						            	</td>
							            <td>{{$product->fkUnit->name}}</td>
							            <td style="text-align: right;">{{$product->fkAccount->code}} &nbsp;</td>
							            <td>
							            	@if($product->status == 1) <font color="blue">Active <i class="glyphicon glyphicon-ok"></i></font>
							            	@else <font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
							            	@endif
							            </td>

							            @if($product->status == 1)
								            <td>
								            	<a href="{{ route('product.show',$product->id)}}" class="btn btn-xs btn-primary">Show</a> &nbsp;
								            	<a href="{{ route('product.edit',$product->id)}}" class="btn btn-xs btn-warning">Edit</a> &nbsp;

								            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$product->id}}">Delete</button>

								            	<form action="{{ route('product.destroy', $product->id)}}" method="post">
												<!-- <form id="productFrm"> -->
								                  	@csrf
								                  	@method('DELETE')
								                  	<div class="modal fade bs-example-modal{{$product->id}}" tabindex="-1" role="dialog" aria-hidden="true">
					                                    <div class="modal-dialog modal-sm">
					                                        <div class="modal-content">
					                                            <div class="modal-header btn-danger">
					                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
					                                                </button>
					                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
					                                            </div>
					                                            <div class="modal-body">
					                                                Are you sure to delete this data?
					                                            </div>
					                                            <div class="modal-footer">
					                                                <button type="submit" class="btn btn-success">Yes</button>
					                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
					                                            </div>
					                                        </div>
					                                    </div>
					                                </div>
								                </form>
								            </td>
							            @else
							            	<td>
							            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$product->id}}">Activated</button>

							            		<form action="{{ route('product.destroy', $product->id)}}" method="post">
												<!-- <form id="productFrm"> -->
									              	@csrf
								                  	@method('DELETE')
								                  	<div class="modal fade bs-example-modal{{$product->id}}" tabindex="-1" role="dialog" aria-hidden="true">
					                                    <div class="modal-dialog modal-sm">
					                                        <div class="modal-content">
					                                            <div class="modal-header btn-success">
					                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
					                                                </button>
					                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
					                                            </div>
					                                            <div class="modal-body">
					                                                Are you sure to active this data?
					                                            </div>
					                                            <div class="modal-footer">
					                                                <button type="submit" class="btn btn-success">Yes</button>
					                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
					                                            </div>
					                                        </div>
					                                    </div>
					                                </div>
								                </form>
							            	</td>
							            @endif
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Name</td>
						          	<td>Department</td>
						          	<td>Variant</td>
						          	<td>Merk</td>
						          	<td>Type Sub</td>
						          	<td>Size</td>
						          	<td>Unit</td>
						          	<td>Status</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	<!-- </div> -->
				<div>
            </div>
        </div>
  	</div>
@stop

@section('content_js')
    <script>
        // sdq.namespace('sdq.product');
        sdq.product = function()
        {   // do NOT access DOM from here; elements don't exist yet
            // execute at the first time
            // private variables
            this.target;
            this.action;

            // private functions
            //**********************
            // public space
            //**********************
            return {
                // execute at the very last time
                // public properties, e.g. strings to translate
                // public methods
                server_date: new Date().getFullYear(),
                start_date : new Date(this.server_date, 0, 1),
                end_date   : new Date(this.server_date, 11, 31),
                page_limit : 75,
                page_start : 0,
                sid        : '{{ csrf_token() }}',
                initialize: function()
                {   //console.log('initialize core');
                    this.prepare_component();
                    this.build_layout();
                    this.finalize_comp_and_layout();
                },
                prepare_component : function()
                {   this.prepare_list();   
                },
                build_layout: function()
                {   },
                finalize_comp_and_layout : function()
                {   },
                prepare_list : function()
                {   sdq.core.setSearchBar();
                    // NEW button
                    $('#productNewBtn').click( 
                        function(){ sdq.core.load('{{ route('product.create') }}'); } );
                    // EDIT button
                    $(".btn.btn-xs.btn-warning").each( 
                        function()
                        {   $(this).click( 
                            function(button)
                            {   sdq.core.load(button.currentTarget.value);
                            }) 
                        } );
                    // DELETE button
                    $(".btn.btn-success").each( 
                        function()
                        {   $(this).click( function(button)
                            {   sdq.core.submitForm('productFrm', button.currentTarget.value, 'product' );
                            }) 
                        } );   
                },
                prepare_create : function()
                {   // SAVE button
                    $('#saveBtn').click( function(button)
                        {   sdq.core.submitForm('productFrm', button.currentTarget.value, 'product' ); 
                        } );
                    // CANCEL button
                    $('#cancelBtn').click( 
                        function(){ sdq.core.load('product'); return false;} );
                },
                prepare_edit : function()
                {   // UPDATE button
                    $('#updateBtn').click( function(button)
                        {   sdq.core.submitForm('productFrm', button.currentTarget.value, 'product' ); 
                        } );
                    // CANCEL button
                    $('#cancelBtn').click( 
                        function(){ sdq.core.load('product'); return false;} );
                },
            };
        }();
        $(document).ready(sdq.product.initialize());
    </script>
@stop