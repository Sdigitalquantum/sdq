<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Kurs;
use App\Http\Models\Currency;
use App\Http\Models\Employeeset;

class KursController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;
        // kurs is indonesia word, in english its will be exchange rate, koers = dutch word
        $koers = Kurs::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/kurs.index', compact('koers','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $kurs = new Kurs();
        $currencies = Currency::where('status', 1)->get();

        return view('vendor/adminlte/kurs.create', compact('kurs','currencies', 'disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	$json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                if ($json->end < $json->start) {
                    // return redirect()->back()->withErrors('End lower than start');
                    $result = false;
                } else {
                    $kurs = new Kurs([
                        'currency'      => $json->currency,
                        'value'         => $json->value,
                        'start'         => $json->start,
                        'end'           => $json->end,
                        'created_user'  => $session_id,
                        'updated_user'  => $session_id
                    ]);
                    $result = $kurs->save();
                    // return redirect('/kurs')->with('success', 'Kurs has been added');
                }
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $kurs = Kurs::find($id);
        $currencys = Currency::where('status', 1)->get();

        return view('vendor/adminlte/kurs.edit', compact('kurs','currencys','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                if ($json->end < $json->start) {
                    //return redirect()->back()->withErrors('End lower than start');
                    $result = false;
                } else {
                    $kurs = Kurs::find($id);
                    $kurs->currency        = $json->currency;
                    $kurs->value           = $json->value;
                    $kurs->start           = $json->start;
                    $kurs->end             = $json->end;
                    $kurs->updated_user    = $session_id;
                    $result = $kurs->save();
                };
            };
            return $this->jsonSuccess( $result );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $kurs = Kurs::find($id);
        $status = $kurs->status;

        if ($status == 1) {
        	$kurs->status       = 0;
            $kurs->updated_user = $session_id;
	        $result = $kurs->save();
        } elseif ($status == 0) {
        	$kurs->status       = 1;
            $kurs->updated_user = $session_id;
	        $result = $kurs->save();
        };
        return $this->jsonSuccess( $result );
    }
}
