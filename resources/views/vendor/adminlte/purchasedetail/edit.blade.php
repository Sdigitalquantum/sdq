@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="active"><a href="#tab_detail" data-toggle="tab">Detail</a></li>
            <li class="disabled"><a>Request</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_detail">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Detail Purchase Order</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('purchasedetail.update', $purchasedetail->id) }}">
				      		@method('PATCH')
				      		@csrf
				      		<div class="row">
    							<div class="col-md-6">
    								<input type="hidden" name="purchaseorder" value="{{ $purchasedetail->purchaseorder }}">
						      		<div class="form-group">
						              	<label for="no_po">No. PO</label>
						              	<input type="text" class="form-control" name="no_po" value="{{ $purchasedetail->fkPurchaseorder->no_po }}" disabled>
						          	</div>
						      		<div class="form-group">
						              	<label for="product">Product</label>
						              	<select class="form-control select2" name="product" required>
						              		@foreach($products as $product)
											    @if ($purchasedetail->product == $product->id)
												    <option value="{{ $product->id }}" selected>{{ $product->code }} - {{ $product->name }}</option>
												@else
												    <option value="{{ $product->id }}">{{ $product->code }} - {{ $product->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						              	<label for="supplierdetail">Supplier</label>
						              	<input type="hidden" name="supplierdetail" value="{{ $supplierdetails->id }}">
				                		<input type="text" class="form-control" value="{{ $supplierdetails->name }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="warehouse">Warehouse Destination</label>
						              	<select class="form-control select2" name="warehouse" required>
						              		@php ($array = [])
									    	@foreach($employeewarehouses as $employeewarehouse)
									    		@php ($array[] = $employeewarehouse->warehouse)
									    	@endforeach
									    	
						              		@foreach($warehouses as $warehouse)
											    @if ($purchasedetail->warehouse == $warehouse->id)
												    <option value="{{ $warehouse->id }}" selected>{{ $warehouse->name }}</option>
												@else
												    @if(in_array($warehouse->id, $array))
												    	<option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
											    	@endif
												@endif
											@endforeach
					                	</select>
						          	</div>
								</div>
							</div>
				          	
				          	<button type="submit" class="btn btn-sm btn-success">Request</button>
				          	<a href="{{ url('/purchasedetail/create', $purchasedetail->purchaseorder) }}" class="btn btn-sm btn-danger">Cancel</a>
						</form>

						<br><table class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #001F3F !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="20%">Product</th>
						          	<th width="20%">Supplier</th>
						          	<th width="20%">Warehouse Destination</th>
						          	<th width="10%">Qty (Kg)</th>
						          	<th width="10%">Price</th>
						          	<th width="10%">Total</th>
						          	<th width="10%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($total = 0)
						    	@foreach($purchasedetails as $purchasedetail)
						    		<tr>
							            <td>{{$purchasedetail->fkProduct->name}}</td>
							            <td>{{$purchasedetail->fkSupplierdetail->name}}</td>
							            <td>{{$purchasedetail->fkWarehouse->name}}</td>
							            <td style="text-align: right;">{{number_format($purchasedetail->qty_order+$purchasedetail->qty_buffer, 0, ',' , '.')}} &nbsp;</td>
							            <td style="text-align: right;">Rp. {{number_format($purchasedetail->price, 0, ',' , '.')}} &nbsp;</td>
							            <td style="text-align: right;">Rp. {{number_format(($purchasedetail->qty_order+$purchasedetail->qty_buffer)*$purchasedetail->price, 0, ',' , '.')}} &nbsp;</td>

							            @if($purchasedetail->status == 1)
								            <td>
								            	<a href="{{ route('purchasedetail.edit',$purchasedetail->id)}}" class="btn btn-xs btn-warning">Edit</a>
								            </td>
							            @else
							            	<td>
							            		<font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
							            	</td>
							            @endif

							            @php ($total = $total + ($purchasedetail->qty_order+$purchasedetail->qty_buffer)*$purchasedetail->price)
							        </tr>
						        @endforeach
						        	<tr>
						        		<td colspan="5" style="text-align: right; font-weight: bold;">Grand Total &nbsp;</td>
						        		<td style="text-align: right;">Rp. {{number_format($total, 0, ',' , '.')}} &nbsp;</td>
						        		<td>&nbsp;</td>
					        		</tr>
						    </tbody>
					  	</table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop