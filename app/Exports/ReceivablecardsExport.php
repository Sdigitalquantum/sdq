<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;

class ReceivablecardsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $results = DB::table('receivablecards')
            ->join('customerdetails', 'receivablecards.customerdetail', '=', 'customerdetails.id')
            ->join('customers', 'customerdetails.customer', '=', 'customers.id')
            ->select('customers.code','customerdetails.name','receivablecards.year','receivablecards.month','receivablecards.debit','receivablecards.kredit')
            ->get();

        return $results;
    }
}
