<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Materialother;
use App\Http\Models\Product;
use App\Http\Models\Unit;
use App\Http\Models\Employeeset;

class MaterialotherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            $cek = Materialother::where([
                ['material', $json->material], 
                ['product', $json->product]]
            )->first();

            if (!empty($cek)) { $result = false; } 
            else {
                $materialother = new Materialother([
                    'material'          => $json->material,
                    'product'           => $json->product,
                    'unit'              => $json->unit,
                    'qty'               => $json->qty,
                    'created_user'      => $session_id,
                    'updated_user'      => $session_id
                ]);
                $result = $materialother->save();
            }
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $materialothers = Materialother::where('material', $id)->orderBy('created_at','desc')->get();
        $products = Product::where('status', 1)->get();
        $units = Unit::where('status', 1)->get();

        return view('vendor/adminlte/materialother.show', compact('id','materialothers','products','units','disable'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $materialother = Materialother::find($id);
        $materialothers = Materialother::where('material', $materialother->material)->orderBy('created_at','desc')->get();
        $products = Product::where('status', 1)->get();
        $units = Unit::where('status', 1)->get();

        return view('vendor/adminlte/materialother.edit', compact('materialother','materialothers','products','units','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            $materialother = Materialother::find($id);

            if ($materialother->product != $json->product) {
                $cek = Materialother::where([
                    ['material', $json->material], 
                    ['product', $json->product]]
                )->first();

                if (!empty($cek)) { $result = false; } 
                else {
                    $materialother->material         = $json->material;
                    $materialother->product          = $json->product;
                    $materialother->unit             = $json->unit;
                    $materialother->qty              = $json->qty;
                    $materialother->updated_user     = $session_id;
                    $result = $materialother->save();
                }
            } else {
                $materialother->material         = $json->material;
                $materialother->product          = $json->product;
                $materialother->unit             = $json->unit;
                $materialother->qty              = $json->qty;
                $materialother->updated_user     = $session_id;
                $result = $materialother->save();
            }
        };
        return $this->jsonSuccess( $result );        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $materialother = Materialother::find($id);
        $status = $materialother->status;

        if ($status == 1) {
        	$materialother->status       = 0;
            $materialother->updated_user = $session_id;
	        $result = $materialother->save();

        } elseif ($status == 0) {
        	$materialother->status       = 1;
            $materialother->updated_user = $session_id;
	        $result = $materialother->save();
            
        };
        return $this->jsonSuccess( $result );
    }
}
