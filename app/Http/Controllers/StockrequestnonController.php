<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Schedulebook;
use App\Http\Models\Schedulestuff;
use App\Http\Models\Stuff;
use App\Http\Models\Port;
use App\Http\Models\Quotationsplit;
use App\Http\Models\Quotationplan;
use App\Http\Models\Plan;
use App\Http\Models\Container;
use App\Http\Models\Letter;
use App\Http\Models\Employeeset;

class StockrequestnonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $schedulestuffs = Quotationsplit::where([['status','<>',0], ['status_book', 0], ['status_release', 1]])->orderBy('created_at','desc')->get();

        return view('vendor/adminlte/stockrequestnon.index', compact('schedulestuffs','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $schedulestuff = Quotationsplit::find($id);
        $stuffs = Stuff::where('quotationsplit', $id)->orderBy('created_at','desc')->get();

        $quotationplans = Quotationplan::where('quotationsplit', $id)->get();
        $plans = Plan::where('status_split', 1)->get();
        $containers = Container::where('status', 1)->get();

        return view('vendor/adminlte/stockrequestnon.create', compact('schedulestuff','stuffs','quotationplans','plans','containers','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $schedulestuff = Quotationsplit::find($request->get('quotationsplit'));

        $request->validate([
            'qty_bag' => 'numeric|min:0|not_in:0',
            'qty_pcs' => 'numeric|min:0|not_in:0',
            'qty_kg'  => 'numeric|min:0|not_in:0'
        ]);

        $no = Stuff::select('no_inc')->whereYear('created_at', date('Y'))->max('no_inc');
        if (empty($no)) {
            $no_inc = 1;
        } else {
            $no_inc = $no + 1;
        }

        $letter = Letter::where('menu', 'Request Stock')->first();
        if (empty($letter->company)) {
            $company = "";
        } else {
            $company = $letter->company."/";
        }

        if (empty($letter->code)) {
            $code = "";
        } else {
            $code = $letter->code;
        }

        if ($letter->romawi == 0) {
            $romawi = "";
        } else {
            if (date("m") == 1) {
                $romawi = "I/";
            } elseif (date("m") == 2) {
                $romawi = "II/";
            } elseif (date("m") == 3) {
                $romawi = "III/";
            } elseif (date("m") == 4) {
                $romawi = "IV/";
            } elseif (date("m") == 5) {
                $romawi = "V/";
            } elseif (date("m") == 6) {
                $romawi = "VI/";
            } elseif (date("m") == 7) {
                $romawi = "VII/";
            } elseif (date("m") == 8) {
                $romawi = "VIII/";
            } elseif (date("m") == 9) {
                $romawi = "IX/";
            } elseif (date("m") == 10) {
                $romawi = "X/";
            } elseif (date("m") == 11) {
                $romawi = "XI/";
            } elseif (date("m") == 12) {
                $romawi = "XII/";
            }
        }

        if ($letter->year == 0) {
            $year = "";
        } else {
            $year = date("Y");
        }

        $nomor = $company.$code.sprintf("%05s", $no_inc)."/".$romawi.$year;

        $stuff = new Stuff([
            'schedulestuff'     => 0,
            'quotationsplit'    => $request->get('quotationsplit'),
            'pol'               => $schedulestuff->pol,
            'pod'               => $schedulestuff->pod,
            'container'         => $request->get('container'),
            'no_inc'            => $no_inc,
            'nomor'             => $nomor,
            'etd'               => $schedulestuff->etd,
            'eta'               => $schedulestuff->eta,
            'vehicle'           => $request->get('vehicle'),
            'seal'              => '',
            'qty_fcl'           => 1,
            'qty_bag'           => $request->get('qty_bag'),
            'qty_pcs'           => $request->get('qty_pcs'),
            'qty_kg'            => $request->get('qty_kg'),
            'nopol'             => $request->get('nopol'),
            'driver'            => $request->get('driver'),
            'mobile'            => $request->get('mobile'),
            'notice'            => !empty($request->get('notice')) ? $request->get('notice') : '',
            'container_empty'   => 0,
            'container_bruto'   => 0,
            'container_netto'   => 0,
            'created_user'      => $session_id,
            'updated_user'      => $session_id
        ]);
        $stuff->save();

        $schedulestuff->status_request  = 1;
        $schedulestuff->save();

        return redirect('/stockrequestnon/create/'.$request->get('quotationsplit'))->with('success', 'Request has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $stuff = Stuff::find($id);
        $stuffs = Stuff::where('quotationsplit', $stuff->quotationsplit)->orderBy('created_at','desc')->get();

        $schedulestuff = Quotationsplit::where('id', $stuff->quotationsplit)->first();
        $quotationplans = Quotationplan::where('quotationsplit', $schedulestuff->id)->get();
        $plans = Plan::where('status_split', 1)->get();
        $containers = Container::where('status', 1)->get();

        return view('vendor/adminlte/stockrequestnon.edit', compact('stuff','stuffs','schedulestuff','quotationplans','plans','containers','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        
        $request->validate([
            'qty_bag' => 'numeric|min:0|not_in:0',
            'qty_pcs' => 'numeric|min:0|not_in:0',
            'qty_kg'  => 'numeric|min:0|not_in:0'
        ]);

        $stuff = Stuff::find($id);
        $stuff->container         = $request->get('container');
        $stuff->vehicle           = $request->get('vehicle');
        $stuff->qty_bag           = $request->get('qty_bag');
        $stuff->qty_pcs           = $request->get('qty_pcs');
        $stuff->qty_kg            = $request->get('qty_kg');
        $stuff->nopol             = $request->get('nopol');
        $stuff->driver            = $request->get('driver');
        $stuff->mobile            = $request->get('mobile');
        $stuff->notice            = !empty($request->get('notice')) ? $request->get('notice') : '';
        $stuff->updated_user      = $session_id;
        $stuff->save();

        return redirect('/stockrequestnon/create/'.$stuff->quotationsplit)->with('success', 'Request has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $stuff = Stuff::find($id);
        $status = $stuff->status;

        if ($status == 1) {
            $stuff->status       = 0;
            $stuff->updated_user = $session_id;
            $stuff->save();

            return redirect('/stockrequestnon/create/'.$stuff->quotationsplit)->with('error', 'Request has been deleted');
        } elseif ($status == 0) {
            $stuff->status       = 1;
            $stuff->updated_user = $session_id;
            $stuff->save();

            return redirect('/stockrequestnon/create/'.$stuff->quotationsplit)->with('success', 'Request has been activated');
        }
    }
}
