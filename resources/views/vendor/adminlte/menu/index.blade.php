@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
                <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Menu</h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<div class="row">
							<div class="col-md-6">
								<table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #605ca8 !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="10%" style="text-align: center;">No</th>
								          	<th width="50%">Root Menu &nbsp;<a href="{{ route('menuroot.create')}}" class="btn btn-xs btn-success" style="text-decoration: none;">New</a></th>
								          	<th width="40%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@php($no = 0)
								    	@foreach($menuroots as $menuroot)
								    		@php($no++)
								        	<tr>
								        		<td style="text-align: center;">{{$no}}</td>
									            <td>
									            	<i class="fa fa-fw fa-{{$menuroot->icon}} text-{{$menuroot->icon_color}}"></i> {{$menuroot->name}}
									            </td>

									            @if($menuroot->status == 1)
										            <td>
										            	<a href="{{ url('menu/create/'.$menuroot->id)}}" class="btn btn-xs btn-primary" style="text-decoration: none;">Set Menu</a> &nbsp;
										            	<a href="{{ route('menuroot.edit',$menuroot->id)}}" class="btn btn-xs btn-warning">Edit</a> &nbsp;
										            	
										            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$menuroot->id}}">Delete</button>

										            	<form action="{{ route('menuroot.destroy', $menuroot->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$menuroot->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-danger">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to delete this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
										            </td>
									            @else
									            	<td>
									            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$menuroot->id}}">Activated</button>

									            		<form action="{{ route('menuroot.destroy', $menuroot->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$menuroot->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-success">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to active this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
									            	</td>
									            @endif
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
							</div>

							<div class="col-md-6">
								<table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="80%">Detail Menu</th>
								          	<th width="20%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($menuroots as $menuroot)
						    				<tr>
						    					<td colspan="2">
						    						<i class="fa fa-fw fa-{{$menuroot->icon}} text-{{$menuroot->icon_color}}"></i> {{$menuroot->name}}
						    					</td>
					    					</tr>

					    					@foreach($menus as $menu)
								    			@if($menuroot->id == $menu->menuroot)
										        	<tr>
											            <td>
											            	<i class="fa fa-fw fa-chevron-right text-{{$menu->icon_color}}" style="margin-left: 10%;"></i> {{$menu->name}}
											            </td>

											            <td>
											            	@if($menu->status == 1)
											            		@if($menu->fkMenuroot->status == 1)
											            			<a href="{{ url('menusub/create/'.$menu->id)}}" class="btn btn-xs btn-success" style="text-decoration: none;">Sub Menu</a>
										            			@else
											            			<font color="red">Root Menu <i class="glyphicon glyphicon-remove"></i></font>
											            		@endif
										            		@else
										            			<font color="red">Not Active</font>
										            		@endif
									            		</td>
											        </tr>

											        @foreach($menusubs as $menusub)
										    			@if($menu->id == $menusub->menu)
												        	<tr>
													            <td>
													            	<i class="fa fa-fw fa-circle-o text-{{$menusub->icon_color}}" style="margin-left: 20%;"></i> {{$menusub->name}}
													            </td>

													            <td>
													            	@if($menusub->status == 1)
													            		<font color="blue">Active</font>
												            		@else
												            			<font color="red">Not Active</font>
												            		@endif
											            		</td>
													        </tr>
												        @endif
										        	@endforeach
										        @endif
								        	@endforeach
								        @endforeach
								    </tbody>
							  	</table>
							</div>
						</div>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop