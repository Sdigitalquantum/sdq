@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #6b6e70 !important; font-weight: bold; color: white;">
			    		<h4 class="box-title">New account</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form id="accountFrm">
			              	@method('POST')
				          	@csrf
				          	<div class="row">
    							<div class="col-md-6">
    								<div class="form-group">
						              	<label for="code">Code</label>
						              	<input type="number" class="form-control" name="code" value="{{ old('code') }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="group">Group Header</label>
						              	<select class="form-control select2" name="group" required>
						              		<option value="{{ old('group') }}">Choose One</option>
					                    	@foreach($groups as $group)
											    <option value="{{ $group->id }}">{{ $group->code }} - {{ $group->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="type">Account Type</label>
						              	<select class="form-control select2" name="type" required>
						              		<option value="{{ old('type') }}">Choose One</option>
					                    	@foreach($types as $type)
											    <option value="{{ $type->id }}">{{ $type->code }} - {{ $type->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="currency">Currency</label>
						              	<select class="form-control select2" name="currency" required>
						              		<option value="{{ old('currency') }}">Choose One</option>
					                    	@foreach($currencys as $currency)
											    <option value="{{ $currency->id }}">{{ $currency->code }} - {{ $currency->name }}</option>
											@endforeach
					                	</select>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						              	<label for="level1">Level 1</label>
						              	<input type="number" class="form-control" name="level1" value="{{ old('level1') }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="level2">Level 2</label>
						              	<input type="number" class="form-control" name="level2" value="{{ old('level2') }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="level3">Level 3</label>
						              	<input type="number" class="form-control" name="level3" value="{{ old('level3') }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="level4">Level 4</label>
						              	<input type="number" class="form-control" name="level4" value="{{ old('level4') }}">
						          	</div>
						          	<div class="form-group">
						              	<button id="saveBtn" value="{{ route('account.store') }}" class="btn btn-sm btn-success">Save</button>
				          				<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
						          	</div>
								</div>
							</div>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.account.prepare_create());
    </script>
@stop