<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Customerdetail;
use App\Http\Models\City;
use App\Http\Models\Employeeset;

class CustomerdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $customerdetail = new Customerdetail([
                    'customer'      => $json->customer,
                    'city'          => $json->city,
                    'name'          => $json->name,
                    'address'       => $json->address,
                    'mobile'        => $json->mobile,
                    'alias_name'    => !empty($json->alias_name) ? $json->alias_name : '',
                    'alias_mobile'  => !empty($json->alias_mobile) ? $json->alias_mobile : '',
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $customerdetail->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $customerdetails = Customerdetail::where('customer', $id)->orderBy('created_at','desc')->get();
        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/customerdetail.show', compact('id','customerdetails','citys','disable'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $customerdetail = Customerdetail::find($id);
      //  dd($customerdetail);
        $customerdetails = Customerdetail::where('customer', $customerdetail->customer)->orderBy('created_at','desc')->get();
        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/customerdetail.edit', compact('customerdetail','customerdetails','citys','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $customerdetail = Customerdetail::find($id);
                $customerdetail->customer      = $json->customer;
                $customerdetail->city          = $json->city;
                $customerdetail->name          = $json->name;
                $customerdetail->address       = $json->address;
                $customerdetail->mobile        = $json->mobile;
                $customerdetail->alias_name    = !empty($json->alias_name) ? $json->alias_name : '';
                $customerdetail->alias_mobile  = !empty($json->alias_mobile) ? $json->alias_mobile : '';
                $customerdetail->updated_user  = $session_id;
                $result = $customerdetail->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $customerdetail = Customerdetail::find($id);
        $status = $customerdetail->status;

        if ($status == 1) {
        	$customerdetail->status       = 0;
            $customerdetail->updated_user = $session_id;
	        $result = $customerdetail->save();

        } elseif ($status == 0) {
        	$customerdetail->status       = 1;
            $customerdetail->updated_user = $session_id;
	        $result = $customerdetail->save();

        };
        return $this->jsonSuccess( $result );
    }
}
