@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Detail</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Journal Detail</h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<div class="row">
			          		<div class="col-md-4">
						      	<form method="post" action="{{ route('journaldetail.update', $journaldetail->id) }}">
						      		@method('PATCH')
						      		@csrf
						          	<div class="form-group">
						              	<label for="journal">Journal</label>
						              	<select class="form-control select2" name="journal" required>
						              		@foreach($journals as $journal)
											    @if ($journaldetail->journal == $journal->id)
												    <option value="{{ $journal->id }}" selected>{{ $journal->code }} [{{ $journal->name }}]</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="account">Accounting</label>
						              	<select class="form-control select2" name="account" required>
						              		@foreach($accounts as $account)
											    @if ($journaldetail->accounting == $account->id)
												    <option value="{{ $account->id }}" selected>{{ $account->code }} [{{ $account->name }}]</option>
												@else
												    <option value="{{ $account->id }}">{{ $account->code }} [{{ $account->name }}]</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="debit">&nbsp;</label>
						          		@if($journaldetail->debit == 1)
						          			<input type="radio" name="debit" value="1" checked> Debit &nbsp;&nbsp;&nbsp;
						          			<input type="radio" name="debit" value="0"> Kredit
						          		@else
						          			<input type="radio" name="debit" value="1"> Debit &nbsp;&nbsp;&nbsp;
						          			<input type="radio" name="debit" value="0" checked> Kredit
						          		@endif
					          		</div>
					              	<div class="form-group">
						          		<label for="check">&nbsp;</label>
						          		@if($journaldetail->status_group == 1)
						          			<input type="checkbox" name="status_group" value="1" checked> Status Group &nbsp;&nbsp;&nbsp;
						          		@else
						          			<input type="checkbox" name="status_group" value="1"> Status Group &nbsp;&nbsp;&nbsp;
						          		@endif

						          		@if($journaldetail->status_record == 1)
						          			<input type="checkbox" name="status_record" value="1" checked> Status Record
						          		@else
						          			<input type="checkbox" name="status_record" value="1"> Status Record
						          		@endif
					          		</div>					          		
					          		<div class="form-group">
						          		<button type="submit" class="btn btn-sm btn-success">Update</button>
						          		<a href="{{ url('/journaldetail/create/'.$journaldetail->journal) }}" class="btn btn-sm btn-danger">Cancel</a>
					          		</div>
						      	</form>
		          			</div>

		          			<div class="col-md-8">
		          				<table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="20%">Journal</th>
								          	<th width="20%">Accounting</th>
								          	<th width="40%">Name</th>
								          	<th width="5%">Status</th>
								          	<th width="15%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($journaldetails as $journaldetail)
								    		<tr>
									            <td>{{$journaldetail->fkJournal->code}}</td>
									            <td>{{$journaldetail->fkAccount->code}}</td>
									            <td>{{$journaldetail->fkAccount->name}}</td>
												<td style="text-align: center;">
									            	@if($journaldetail->debit == 1) D
									            	@else K
									            	@endif
									            </td>
												<td>
									            	@if($journaldetail->status == 1) <a href="{{ route('journaldetail.edit',$journaldetail->id)}}" class="btn btn-xs btn-warning">Edit</a>
									            	@else <font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
									            	@endif
									            </td>
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
	          				</div>
	          			</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop