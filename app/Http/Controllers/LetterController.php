<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Letter;
use App\Http\Models\Employeeset;

class LetterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $letters = Letter::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/letter.index', compact('letters','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $letter = new Letter();
        return view('vendor/adminlte/letter.create', compact('letter','disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	$json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'code' => 'unique:letters'
                ]);

                $romawi = 0; if (isset($json->romawi)) { $romawi = $json->romawi; };
                $year   = 0; if (isset($json->year))   { $year   = $json->year; };

                $letter = new Letter([
                    'menu'          => $json->menu,
                    'company'       => $json->company,
                    'code'          => $json->code,
                    'romawi'        => $romawi,
                    'year'          => $year,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $letter->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $letter = Letter::find($id);

        return view('vendor/adminlte/letter.edit', compact('letter','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $letter = Letter::find($id);
                $code = $letter->code;

                if ($code != $json->code) {
                    $request->validate([
                        'code' => 'unique:letters'
                    ]);
                }
                
                $letter->menu           = $json->menu;
                $letter->company        = $json->company;
                $letter->code           = $json->code;
                $letter->romawi         = ($json->romawi == 1) ? 1 : 0;
                $letter->year           = ($json->year == 1) ? 1 : 0;
                $letter->updated_user   = $session_id;
                $reult = $letter->save();

            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $letter = Letter::find($id);
        $status = $letter->status;

        if ($status == 1) {
        	$letter->status       = 0;
            $letter->updated_user = $session_id;
	        $result = $letter->save();

        } elseif ($status == 0) {
        	$letter->status       = 1;
            $letter->updated_user = $session_id;
	        $result = $letter->save();
        };
        return $this->jsonSuccess( $result );
    }
}
