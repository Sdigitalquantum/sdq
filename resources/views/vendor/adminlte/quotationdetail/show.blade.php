@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="active"><a href="#tab_detail" data-toggle="tab">Detail</a></li>
            <li class="disabled"><a>Payment</a></li>
            <li class="disabled"><a>Charges</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_detail">
            	<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Quotation Detail <b>{{ $quotation->no_sp }}</b>
				  			&nbsp;<a href="{{ route('quotation.edit', $id) }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a>
				  		</h4>
			  		</div>
				  	
				  	<div class="box-body">
				  		<form method="post" action="{{ route('quotationdetail.store') }}">
				          	@csrf
					  		<div class="row">
					  			<div class="col-md-6">
						          	<input type="hidden" class="form-control" name="quotation" value="{{ $id }}">
						          	<div class="form-group">
						              	<label for="product">Product</label>
						              	<select class="form-control select2" name="product" id="ajaxproduct" required>
						              		<option value="{{ old('product') }}">Choose One</option>
					                    	@foreach($products as $product)
											    <option value="{{ $product->id }}">{{ $product->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_fcl">Qty Fcl</label>
						              	<input type="number" class="form-control" name="qty_fcl" value="{{ old('qty_fcl') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_bag">Qty Bag</label>
						              	<input type="number" class="form-control" name="qty_bag" value="{{ old('qty_bag') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_pcs">Qty Pcs</label>
						              	<input type="number" class="form-control" name="qty_pcs" value="{{ old('qty_pcs') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_kg">Qty Kg</label>
						              	<input type="number" class="form-control" name="qty_kg" value="{{ old('qty_kg') }}" required>
						          	</div>
				  				</div>

				  				<div class="col-md-6">
						          	<div class="form-group">
						          		<label for="alias_name">Alias Name</label>
						              	<input type="text" class="form-control" name="alias_name" value="{{ old('alias_name') }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="alias_qty">Alias Unit</label>
						              	<input type="text" class="form-control" name="alias_qty" value="{{ old('alias_qty') }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="price">{{ $quotation->fkCurrency->code }} Price @Kg</label>
						              	<textarea class="form-control" name="price" id="ajaxprice" value="{{ old('price') }}" rows="1" required></textarea>
						          	</div>
						          	<div class="form-group">
						          		<label for="disc_type">Discount</label> <br>
						              	<input type="radio" name="disc_type" value="1"> % &nbsp;&nbsp;&nbsp;
						              	<input type="radio" name="disc_type" value="2"> Rp / $ &nbsp;&nbsp;&nbsp;
						              	<input type="number" class="form-control" name="disc_value" value="{{ old('disc_value') }}" step=".01">
						          	</div>
						          	<div class="form-group">
						          		<button type="submit" class="btn btn-sm btn-success">Save</button>
						          	</div>
				  				</div>
			  				</div>
		  				</form>

		  				<table class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="30%">Product</th>
						          	<th width="15%">Unit</th>
						          	<th width="10%">Qty</th>
						          	<th width="20%">Price @Kg</th>
						          	<th width="15%">Total</th>
						          	<th width="10%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($total = 0)
						    	@foreach($quotationdetails as $quotationdetail)
						    		<tr>
							            <td>
							            	Name : {{$quotationdetail->fkProduct->name}} <br>
							            	Alias &nbsp; : {{$quotationdetail->alias_name}}
							            </td>
							            <td>
							            	Name : {{$quotationdetail->fkUnit->name}} <br>
							            	Alias &nbsp; : {{$quotationdetail->alias_qty}}
							            </td>
							            <td style="text-align: right;">
							            	Bag : {{number_format($quotationdetail->qty_bag, 0, ',' , '.')}} &nbsp; <br>
							            	Pcs : {{number_format($quotationdetail->qty_pcs, 0, ',' , '.')}} &nbsp; <br>
							            	Kg : {{number_format($quotationdetail->qty_kg, 0, ',' , '.')}} &nbsp;&nbsp;
							            </td>

							            @if($quotationdetail->kurs == 0 || $quotationdetail->fkQuotation->kurs)
							            	@php ($kurs = 1)
							            @else
							            	@php ($kurs = $quotationdetail->kurs)
							            @endif

							            <td style="text-align: right;">
							            	[{{$quotationdetail->fkQuotation->fkCurrency->code}}]
							            	@if($quotationdetail->fkQuotation->fkCurrency->code == 'IDR')
							            		{{number_format($quotationdetail->price, 0, ',' , '.')}} &nbsp;
							            	@else
							            		{{$quotationdetail->price}} &nbsp;
							            	@endif
							            	
							            	@if($quotationdetail->disc_type == 0)
							            		<br>Kurs : Rp. {{number_format($kurs, 0, ',' , '.')}} &nbsp;&nbsp;
							            	@else
							            		<br>Kurs : Rp. {{number_format($kurs, 0, ',' , '.')}} &nbsp;
							            		@if($quotationdetail->disc_type == 2)
							            			<br>Disc. {{round(($quotationdetail->disc_value*$kurs)/($quotationdetail->price*$kurs*$quotationdetail->qty_kg)*100,2)}}% : Rp. {{number_format($quotationdetail->disc_value*$kurs, 0, ',' , '.')}} &nbsp;&nbsp;
						            			@else
						            				<br>Disc. {{$quotationdetail->disc_value}}% : Rp. {{number_format($quotationdetail->disc_value/100*($quotationdetail->price*$kurs*$quotationdetail->qty_kg), 0, ',' , '.')}} &nbsp;&nbsp;
						            			@endif
							            	@endif
							            </td>
							            <td style="text-align: right;">
							            	@if($quotationdetail->disc_type == 0)
							            		Rp. {{number_format($quotationdetail->price*$kurs*$quotationdetail->qty_kg, 0, ',' , '.')}} &nbsp;
							            	@elseif($quotationdetail->disc_type == 2)
							            		Rp. {{number_format(($quotationdetail->price*$kurs*$quotationdetail->qty_kg)-($quotationdetail->disc_value*$kurs), 0, ',' , '.')}} &nbsp;
						            		@else
						            			Rp. {{number_format(($quotationdetail->price*$kurs*$quotationdetail->qty_kg)-($quotationdetail->disc_value/100*($quotationdetail->price*$kurs*$quotationdetail->qty_kg)), 0, ',' , '.')}} &nbsp;
							            	@endif
							            </td>

							            @if($quotationdetail->status_plan == 1)
							            	<td><font color="blue">Plan <i class="glyphicon glyphicon-ok"></i></font></td>
							            @else
								            @if($quotationdetail->status == 1)
									            <td>
									            	<a href="{{ route('quotationdetail.edit',$quotationdetail->id)}}" class="btn btn-xs btn-warning">Edit</a> &nbsp;
									            	
									            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$quotationdetail->id}}">Delete</button>

									            	<form action="{{ route('quotationdetail.destroy', $quotationdetail->id)}}" method="post">
									                  	@csrf
									                  	@method('DELETE')
									                  	<div class="modal fade bs-example-modal{{$quotationdetail->id}}" tabindex="-1" role="dialog" aria-hidden="true">
						                                    <div class="modal-dialog modal-sm">
						                                        <div class="modal-content">
						                                            <div class="modal-header btn-danger">
						                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
						                                                </button>
						                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
						                                            </div>
						                                            <div class="modal-body">
						                                                Are you sure to delete this data?
						                                            </div>
						                                            <div class="modal-footer">
						                                                <button type="submit" class="btn btn-success">Yes</button>
						                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
						                                            </div>
						                                        </div>
						                                    </div>
						                                </div>
									                </form>
									            </td>
								            @else
								            	<td>
								            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$quotationdetail->id}}">Activated</button>

								            		<form action="{{ route('quotationdetail.destroy', $quotationdetail->id)}}" method="post">
									                  	@csrf
									                  	@method('DELETE')
									                  	<div class="modal fade bs-example-modal{{$quotationdetail->id}}" tabindex="-1" role="dialog" aria-hidden="true">
						                                    <div class="modal-dialog modal-sm">
						                                        <div class="modal-content">
						                                            <div class="modal-header btn-success">
						                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
						                                                </button>
						                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
						                                            </div>
						                                            <div class="modal-body">
						                                                Are you sure to active this data?
						                                            </div>
						                                            <div class="modal-footer">
						                                                <button type="submit" class="btn btn-success">Yes</button>
						                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
						                                            </div>
						                                        </div>
						                                    </div>
						                                </div>
									                </form>
								            	</td>
								            @endif
							            @endif

							            @if($quotationdetail->disc_type == 0 && $quotationdetail->status == 1)
						            		@php ($total = $total + ($quotationdetail->price*$kurs*$quotationdetail->qty_kg))
						            	@elseif($quotationdetail->disc_type == 2 && $quotationdetail->status == 1)
						            		@php ($total = $total + ($quotationdetail->price*$kurs*$quotationdetail->qty_kg) - ($quotationdetail->disc_value*$kurs))
						            	@elseif($quotationdetail->disc_type == 1 && $quotationdetail->status == 1)
						            		@php ($total = $total + ($quotationdetail->price*$kurs*$quotationdetail->qty_kg) - ($quotationdetail->disc_value/100*($quotationdetail->price*$kurs*$quotationdetail->qty_kg)))
					            		@endif
							        </tr>
						        @endforeach
						        	@if($total != 0)
								        <tr>
								        	<td colspan="4" style="text-align: right; font-weight: bold;">Grand Total &nbsp;&nbsp;</td>
								        	<td style="text-align: right;">Rp. {{number_format($total, 0, ',' , '.')}} &nbsp;</td>
								        	<td>&nbsp;</td>
							        	</tr>
						        	@endif
						    </tbody>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop