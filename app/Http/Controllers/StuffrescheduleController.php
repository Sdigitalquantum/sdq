<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Stuffreschedule;
use App\Http\Models\Schedulestuff;
use App\Http\Models\Schedulebook;
use App\Http\Models\Quotationsplit;
use App\Http\Models\Stuff;
use App\Http\Models\Stufforder;
use App\Http\Models\Stockload;
use App\Http\Models\Stockin;
use App\Http\Models\Employeeset;

class StuffrescheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $stuffreschedules = Stuffreschedule::all();

        return view('vendor/adminlte/stuffreschedule.index', compact('stuffreschedules','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $schedulestuffs = Schedulestuff::where([['status_approve','<>',0], ['status_stuff', 1], ['status_send', 1], ['status_pack', 1]])->orderBy('schedulebook','desc')->get();
        
        return view('vendor/adminlte/stuffreschedule.create', compact('schedulestuffs','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $cek = Stuffreschedule::where('schedulestuff', $request->get('schedulestuff'))->first();

        if (!empty($cek)) {
            return redirect()->back()->withErrors('Split Proforma already exist');
        } else {
            $schedulestuff = Schedulestuff::find($request->get('schedulestuff'));
            $stuffreschedule = new Stuffreschedule([
                'schedulestuff'     => $request->get('schedulestuff'),
                'reason'            => $request->get('reason'),
                'date_change'       => $request->get('date_change'),
                'date_old'          => $schedulestuff->date,
                'notice'            => $request->get('notice'),
                'created_user'      => $session_id,
                'updated_user'      => $session_id
            ]);
            $stuffreschedule->save();

            if ($request->get('reason') == 1) {
                $schedulestuff->date                = $request->get('date_change');
                $schedulestuff->notice              = $request->get('notice');
                $schedulestuff->reason              = '';
                $schedulestuff->status_approve      = 0;
                $schedulestuff->status_reschedule   = 1;
                $schedulestuff->save();

                return redirect('/stuffreschedule')->with('success', 'Stuffing has been rescheduled');
            } else {
                $schedulestuff->notice              = $request->get('notice');
                $schedulestuff->status_approve      = 2;
                $schedulestuff->status_reschedule   = 1;
                $schedulestuff->save();

                $schedulebook = Schedulebook::where('id', $schedulestuff->schedulebook)->first();
                $schedulebook->notice               = $request->get('notice');
                $schedulebook->status_approve       = 2;
                $schedulebook->status_stuff_acc     = 2;
                $schedulebook->save();

                $quotationsplit = Quotationsplit::where('id', $schedulebook->quotationsplit)->first();
                $quotationsplit->status_book = 0;
                $quotationsplit->save();

                $stufforder = Stufforder::where('schedulestuff', $schedulestuff->id)->first();
                $stufforder->status = 2;
                $stufforder->save();

                $stuffs = Stuff::where('schedulestuff', $schedulestuff->id)->get();
                foreach ($stuffs as $stuff) {
                    $stuff->status          = 0;
                    $stuff->status_load     = 0;
                    $stuff->qty_load        = 0;
                    $stuff->save();

                    $stockloads = Stockload::where('stuff', $stuff->id)->get();
                    foreach ($stockloads as $stockload) {
                        $stockin = Stockin::where('id', $stockload->stockin)->first();
                        $stockin->qty_booked    = $stockin->qty_booked+$stockin->qty_out;
                        $stockin->qty_out       = 0;
                        $stockin->save();

                        $stockload->delete();
                    }
                }

                return redirect('/quotationsplit')->with('success', 'Stuffing has been canceled');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
    }
}
