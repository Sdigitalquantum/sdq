<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Menu;
use App\Http\Models\Menusub;
use App\Http\Models\Employeeset;

class MenusubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $menu = Menu::where('id', $id)->first();
        $menusubs = Menusub::where('menu', $id)->orderBy('nomor','asc')->get();

        $no = Menusub::select('nomor')->where('menu', $id)->max('nomor');
        if (empty($no)) {
            $nomor = 1;
        } else {
            $nomor = $no + 1;
        }

        return view('vendor/adminlte/menusub.create', compact('menu','menusubs','nomor','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $cek = Menusub::where([['menu', $request->get('menu')], ['nomor', $request->get('nomor')]])->first();

        if ($request->get('url') != '#') {
            $request->validate([
                'url' => 'unique:menusubs'
            ]);
        }

        $request->validate([
            'nomor' => 'numeric|min:0|not_in:0'
        ]);

        if (!empty($cek)) {
            return redirect()->back()->withErrors('Nomor already exist');
        } else {
            $menusub = new Menusub([
                'menu'          => $request->get('menu'),
                'name'          => $request->get('name'),
                'url'           => !empty($request->get('url')) ? $request->get('url') : '#',
                'icon'          => 'circle-o',
                'icon_color'    => !empty($request->get('icon_color')) ? $request->get('icon_color') : 'red',
                'nomor'         => $request->get('nomor'),
                'created_user'  => $session_id,
                'updated_user'  => $session_id
            ]);
            $menusub->save();

            return redirect('/menusub/create/'.$request->get('menu'))->with('success', 'Sub Menu has been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $menusub = Menusub::find($id);
        $menus = Menu::where([['status', 1], ['menuroot', $menusub->fkMenu->menuroot]])->get();
        $menusubs = Menusub::where('menu', $menusub->menu)->orderBy('nomor','asc')->get();

        return view('vendor/adminlte/menusub.edit', compact('menusub','menus','menusubs','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
    	$menusub = Menusub::find($id);

        if ($menusub->url != $request->get('url')) {
            if ($request->get('url') != '#') {
                $request->validate([
                    'url' => 'unique:menus'
                ]);
            }
        }

        $request->validate([
            'nomor' => 'numeric|min:0|not_in:0'
        ]);

        if ($request->get('menu') == $menusub->menu) {
            if ($menusub->nomor != $request->get('nomor')) {
                $ceks = Menusub::where([['menu', $menusub->menu], ['id','<>',$id], ['nomor','>=',$request->get('nomor')]])->orderBy('nomor','asc')->get();
                foreach ($ceks as $cek) {
                    $cek->nomor = $cek->nomor+1;
                    $cek->save();
                }

                $menusub->name            = $request->get('name');
                $menusub->url             = !empty($request->get('url')) ? $request->get('url') : '#';
                $menusub->icon            = 'circle-o';
                $menusub->icon_color      = !empty($request->get('icon_color')) ? $request->get('icon_color') : 'red';
                $menusub->nomor           = $request->get('nomor');
                $menusub->updated_user    = $session_id;
                $menusub->save();

                return redirect('/menusub/create/'.$menusub->menu)->with('success', 'Sub Menu has been updated');
            } else {
                $menusub->name            = $request->get('name');
                $menusub->url             = !empty($request->get('url')) ? $request->get('url') : '#';
                $menusub->icon            = 'circle-o';
                $menusub->icon_color      = !empty($request->get('icon_color')) ? $request->get('icon_color') : 'red';
                $menusub->updated_user    = $session_id;
                $menusub->save();

                return redirect('/menusub/create/'.$menusub->menu)->with('success', 'Sub Menu has been updated');
            }
        } else {
            $cek = Menusub::where('menu', $request->get('menu'))->orderBy('nomor','desc')->first();
            $nomor = $cek->nomor+1;

            $menusub->menu          = $request->get('menu');
            $menusub->nomor         = $nomor;
            $menusub->updated_user  = $session_id;
            $menusub->save();

            $employeesets = Employeeset::where('menusub', $menusub->id)->get();
            foreach ($employeesets as $employeeset) {
                $employeeset->delete();
            }

            return redirect('/menusub/create/'.$menusub->menu)->with('success', 'Sub Menu has been updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $menusub = Menusub::find($id);
        $status = $menusub->status;

        if ($status == 1) {
            $cek = Employeeset::where('menusub', $id)->first();
            if (!empty($cek)) {
                return redirect()->back()->withErrors('Sub Menu already used');
            } else {
                $menusub->status       = 0;
                $menusub->updated_user = $session_id;
                $menusub->save();

                return redirect('/menusub/create/'.$menusub->menu)->with('error', 'Sub Menu has been deleted');
            }
        } elseif ($status == 0) {
        	$menusub->status       = 1;
            $menusub->updated_user = $session_id;
	        $menusub->save();

		    return redirect('/menusub/create/'.$menusub->menu)->with('success', 'Sub Menu has been activated');
        }
    }
}
