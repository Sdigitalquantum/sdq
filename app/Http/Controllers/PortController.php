<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Port;
use App\Http\Models\Country;
use App\Http\Models\Employeeset;

class PortController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        // $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        // $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        // $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $ports = Port::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/port.index', compact('ports','disable'));
        //,'employeeroots','employeemenus','employeesubs'
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        // $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        // $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        // $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $countrys = Country::where('status', 1)->get();
        $port = new Port();

        return view('vendor/adminlte/port.create', compact('countrys', 'port','disable'));
        //,'employeeroots','employeemenus','employeesubs'
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $session_id = $request->session()->get('session_login');
    	
	    $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'name' => 'unique:ports'
                ]);

                $no = Port::select('no_inc')->max('no_inc');
                if (empty($no)) {
                    $no_inc = 1;
                } else {
                    $no_inc = $no + 1;
                }
                $code = "P".sprintf("%05s", $no_inc);

                $port = new Port([
                    'country'       => $json->country,
                    'no_inc'        => $no_inc,
                    'code'          => $code,
                    'name'          => $json->name,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $port->save();
            };
        };
        return $this->jsonSuccess( $result );
        // return redirect('/port')->with('success', 'Port has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        // $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        // $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        // $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $port = Port::find($id);
        $countrys = Country::where('status', 1)->get();

        return view('vendor/adminlte/port.edit', compact('port','countrys','disable'));
        // ,'employeeroots','employeemenus','employeesubs'
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $port = Port::find($id);
                $name = $port->name;

                if ($name != $request->get('name')) {
                    $request->validate([
                        'name' => 'unique:ports'
                    ]);
                }
                
                $port->country         = $json->country;
                $port->name            = $json->name;
                $port->updated_user    = $session_id;
                $result = $port->save();
            };
        };
        return $this->jsonSuccess( $result );
        // return redirect('/port')->with('success', 'Port has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $port = Port::find($id);
        $status = $port->status;

        if ($status == 1) {
        	$port->status       = 0;
            $port->updated_user = $session_id;
	        $result = $port->save();

		    // return redirect('/port')->with('error', 'Port has been deleted');
        } elseif ($status == 0) {
        	$port->status       = 1;
            $port->updated_user = $session_id;
	        $result = $port->save();

		    // return redirect('/port')->with('success', 'Port has been activated');
        };
        return $this->jsonSuccess( $result );
    }
}
