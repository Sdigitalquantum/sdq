@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">New User</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('employee.store') }}">
				          	@csrf
				          	<div class="row">
    							<div class="col-md-6">
    								<div class="form-group">
						              	<label for="group">Group</label>
						              	<select class="form-control select2" name="group" required>
						              		<option value="{{ old('group') }}">Choose One</option>
					                    	@foreach($groups as $group)
											    <option value="{{ $group->id }}">{{ $group->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="branch">Branch</label>
						              	<select class="form-control select2" name="branch" required>
						              		<option value="{{ old('branch') }}">Choose One</option>
					                    	@foreach($branchs as $branch)
											    <option value="{{ $branch->id }}">{{ $branch->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="department">Department</label>
						              	<select class="form-control select2" name="department" required>
						              		<option value="{{ old('department') }}">Choose One</option>
					                    	@foreach($departments as $department)
											    <option value="{{ $department->id }}">{{ $department->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="position">Position</label>
						              	<select class="form-control select2" name="position" required>
						              		<option value="{{ old('position') }}">Choose One</option>
					                    	@foreach($positions as $position)
											    <option value="{{ $position->id }}">{{ $position->name }}</option>
											@endforeach
					                	</select>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						          		<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="email">Email</label>
						              	<input type="text" class="form-control" name="email" value="{{ old('email') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="password">Password</label>
						              	<input type="password" class="form-control" name="password" value="{{ old('password') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="button">&nbsp;</label> <br>
						              	<button type="submit" class="btn btn-sm btn-success">Save</button>
			          					<a href="{{ url('/employee') }}" class="btn btn-sm btn-danger">Cancel</a>
						          	</div>
								</div>
							</div>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop