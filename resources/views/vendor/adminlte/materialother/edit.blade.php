@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Raw Material</a></li>
            <li class="active"><a href="#tab_excess" data-toggle="tab">Excess</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_excess">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Excess</h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<div class="row">
				  			<div class="col-md-3">
				  				<form id="materialotherFrm"> <!-- method="post" action="{{ route('materialother.update', $materialother->id) }}"> -->
						      		@method('PATCH')
						      		@csrf
						      		<input type="hidden" class="form-control" name="material" value="{{ $materialother->material }}">
						      		<div class="form-group">
						              	<label for="product">Product</label>
						              	<select class="form-control select2" name="product" required>
						              		@foreach($products as $product)
											    @if ($materialother->product == $product->id)
												    <option value="{{ $product->id }}" selected>{{ $product->name }}</option>
												@else
												    <option value="{{ $product->id }}">{{ $product->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="unit">Unit</label>
						              	<select class="form-control select2" name="unit" required>
						              		@foreach($units as $unit)
											    @if ($materialother->unit == $unit->id)
												    <option value="{{ $unit->id }}" selected>{{ $unit->name }}</option>
												@else
												    <option value="{{ $unit->id }}">{{ $unit->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty">Qty</label>
						              	<input type="number" class="form-control" name="qty" value="{{ $materialother->qty }}" required>
						          	</div>
					          		<button id="updateBtn" value="{{ route('materialother.update', $materialother->id) }}" class="btn btn-sm btn-success">Update</button>
				          			<button id="cancelBtn" value="{{ url('/materialother/'.$materialother->material) }}"class="btn btn-sm btn-danger">Cancel</button>									  
						          	<!-- <button type="submit" class="btn btn-sm btn-success">Update</button>
					          		<a href="{{ url('/materialother/'.$materialother->material) }}" class="btn btn-sm btn-danger">Cancel</a> -->
						      	</form>
			  				</div>

			  				<div class="col-md-9">
			  					<br><table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="30%">Product</th>
								          	<th width="30%">Unit</th>
								          	<th width="20%">Qty</th>
								          	<th width="20%">Status</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($materialothers as $materialother)
								    		<tr>
									            <td>{{$materialother->fkProduct->name}}</td>
									            <td>{{$materialother->fkUnit->name}}</td>
									            <td>{{$materialother->qty}}</td>
									            <td>
									            	@if($materialother->status == 1) <a href="{{ route('materialother.edit',$materialother->id)}}" class="btn btn-xs btn-warning">Edit</a>
									            	@else <font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
									            	@endif
									            </td>
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
			  				</div>
		  				</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('content_js')
	<script>
    	$(document).ready(sdq.material.prepare_editother());
    </script>
@stop