@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Delivery Order Confirmation - Non Shipping</h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="5%">No</th>
			          	<th width="15%">Nomor</th>
			          	<th width="15%">Total Qty (Kg)</th>
			          	<th width="40%">Quality Check</th>
			          	<th width="15%">Weigh Check (Kg)</th>
			          	<th width="10%">Status Confirm</th>
			          	<th width="10%">Action</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@php ($no = 0)
			    	@foreach($stufforders as $stufforder)
			    		@php ($no++)
			        	<tr>
				            <td>{{$no}}</td>
				            <td>{{$stufforder->no_letter}}</td>
				            <td>{{$stufforder->check_quality}}</td>
				            <td style="text-align: right;">
				            	@php ($total = 0)
				            	@foreach($stuffs as $stuff)
				            		@if($stuff->quotationsplit == $stufforder->quotationsplit)
				            			@php ($total = $total + $stuff->qty_load)
				            		@endif
				            	@endforeach
				            	{{$total}} &nbsp;
			            	</td>
				            <td style="text-align: right;">{{$stufforder->check_weigh}} &nbsp;</td>
				            
				            <td>
					            @if($stufforder->status_confirm == 1)
				            		<font color="blue">Confirmed <i class="glyphicon glyphicon-ok"></i></font>
			            		@endif
		            		</td>
							
							<td>
								@if($stufforder->status_confirm == 0)
									<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$stufforder->id}}">Confirmation</button>
					                
					                <form action="{{ route('stuffnon.destroy', $stufforder->id)}}" method="post">
					                  	@csrf
					                  	@method('DELETE')
					                  	<div class="modal fade bs-example-modal{{$stufforder->id}}" tabindex="-1" role="dialog" aria-hidden="true">
		                                    <div class="modal-dialog modal-sm">
		                                        <div class="modal-content">
		                                            <div class="modal-header btn-success">
		                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
		                                                </button>
		                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
		                                            </div>
		                                            <div class="modal-body">
		                                            	Are you sure to confirm this data?
		                                            </div>
		                                            <div class="modal-footer">
		                                                <button type="submit" class="btn btn-success">Yes</button>
		                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
					                </form>
				            	@elseif($stufforder->status_confirm == 1)
				            		<button type="button" class="btn btn-xs btn-warning" data-toggle='modal' data-target=".bs-example-modal{{$stufforder->id}}">Customer Check</button>
					                
					                <form action="{{ route('stuffnon.destroy', $stufforder->id)}}" method="post">
					                  	@csrf
					                  	@method('DELETE')
					                  	<div class="modal fade bs-example-modal{{$stufforder->id}}" tabindex="-1" role="dialog" aria-hidden="true">
		                                    <div class="modal-dialog modal-sm">
		                                        <div class="modal-content">
		                                            <div class="modal-header btn-success">
		                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
		                                                </button>
		                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
		                                            </div>
		                                            <div class="modal-body">
		                                            	Are you sure to check this data?
		                                            	
		                                            	<br><br>Quality Check
		                                            	<br><textarea class="form-control" name="check_quality" cols="30" rows="3" required>{{ old('check_quality') }}</textarea>

		                                            	<br><br>Weigh Check (Kg)
		                                            	<br><input type="number" class="form-control" name="check_weigh" value="{{ old('check_weigh') }}" required>
		                                            	<input type="hidden" name="total" value="{{ $total }}">
		                                            </div>
		                                            <div class="modal-footer">
		                                                <button type="submit" class="btn btn-success">Yes</button>
		                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
					                </form>
								@endif
							</td>
				        </tr>
			        @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			          	<td class="noShow"></td>
			          	<td>Nomor</td>
			          	<td>Total Qty (Kg)</td>
			          	<td>Check Quality</td>
			          	<td>Check Weigh (Kg)</td>
			          	<td>Status Confirm</td>
			          	<td class="noShow"></td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	</div>
@stop