<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'S.D.Q',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => 'sdq_logo.png',

    'logo_mini' => 'sdq_logo_mini.png',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'black',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => 'fixed',

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'home',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => 'register',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */
    // 'menu' => [
    //     [
    //         'text'          => 'Dashboard',
    //         'url'           => 'home',
    //         'icon'          => 'home',
    //         'icon_color'    => 'purple'
    //     ],
    //     [
    //         'text'          => 'Master Data',
    //         'icon'          => 'book',
    //         'icon_color'    => 'purple',
    //         'submenu' => [
    //             [
    //                 'text'        => 'General',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',  
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Country',
    //                         'url'         => 'country',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'City',
    //                         'url'         => 'city',
    //                         'icon_color'  => 'red'
    //                     ],
    //                     [
    //                         'text'        => 'Port',
    //                         'url'         => 'port',
    //                         'icon_color'  => 'red'
    //                     ],
    //                     [
    //                         'text'        => 'Bank',
    //                         'url'         => 'bank',
    //                         'icon_color'  => 'red'
    //                     ],
    //                     [
    //                         'text'        => 'Currency',
    //                         'url'         => 'currency',
    //                         'icon_color'  => 'red'
    //                     ],
    //                     [
    //                         'text'        => 'Kurs',
    //                         'url'         => 'kurs',
    //                         'icon_color'  => 'red'
    //                     ],
    //                     [
    //                         'text'        => 'Payment',
    //                         'url'         => 'payment',
    //                         'icon_color'  => 'red'
    //                     ],
    //                     [
    //                         'text'        => 'Payment Term',
    //                         'url'         => 'paymentterm',
    //                         'icon_color'  => 'red'
    //                     ],
    //                     [
    //                         'text'        => 'Container',
    //                         'url'         => 'container',
    //                         'icon_color'  => 'red'
    //                     ]
    //                 ]
    //             ],
    //             [
    //                 'text'        => 'Internal',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',  
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Branch',
    //                         'url'         => 'branch',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Department',
    //                         'url'         => 'department',
    //                         'icon_color'  => 'red'
    //                     ],
    //                     [
    //                         'text'        => 'Position',
    //                         'url'         => 'position',
    //                         'icon_color'  => 'red'
    //                     ],
    //                     [
    //                         'text'        => 'Warehouse',
    //                         'url'         => 'warehouse',
    //                         'icon_color'  => 'red'
    //                     ],
    //                     [
    //                         'text'        => 'Document',
    //                         'url'         => 'document',
    //                         'icon_color'  => 'red'
    //                     ],
    //                     [
    //                         'text'        => 'Letter',
    //                         'url'         => 'letter',
    //                         'icon_color'  => 'red'
    //                     ],
    //                     [
    //                         'text'        => 'Company',
    //                         'url'         => 'company',
    //                         'icon_color'  => 'red'
    //                     ]
    //                 ]
    //             ],
    //             [
    //                 'text'        => 'Product',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',  
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Variant',
    //                         'url'         => 'variant',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Merk',
    //                         'url'         => 'merk',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Size',
    //                         'url'         => 'size',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Unit',
    //                         'url'         => 'unit',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Type',
    //                         'url'         => 'type',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Type Sub',
    //                         'url'         => 'typesub',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Price List',
    //                         'url'         => 'pricelist',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]
    //             ],
    //             [
    //                 'text'        => 'Accounting',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',  
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Group Header',
    //                         'url'         => 'accountgroup',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Account Type',
    //                         'url'         => 'accounttype',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Accounting',
    //                         'url'         => 'account',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Customer',
    //                         'url'         => 'customer',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Supplier',
    //                         'url'         => 'supplier',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Product',
    //                         'url'         => 'product',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Transaction',
    //                         'url'         => 'transaction',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Bill of Material',
    //                         'url'         => 'material',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Sales',
    //                         'url'         => 'sales',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]
    //             ]
    //         ]
    //     ],
    //     [
    //         'text'        => 'Sales Order',
    //         'icon'        => 'shopping-cart',
    //         'icon_color'  => 'purple',
    //         'submenu' => [
    //             [
    //                 'text'        => 'Quotation',
    //                 'url'         => 'quotation',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'  
    //             ],
    //             [
    //                 'text'        => 'Dummy Proforma',
    //                 'url'         => 'dummy',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'  
    //             ],
    //             [
    //                 'text'        => 'Delivery Plan',
    //                 'url'         => 'plan',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'  
    //             ],
    //             [
    //                 'text'        => 'Report Plan',
    //                 'url'         => 'planreport',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'  
    //             ],
    //             [
    //                 'text'        => 'Split Proforma',
    //                 'url'         => 'quotationsplit',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'  
    //             ],
    //             [
    //                 'text'        => 'Verify Order',
    //                 'url'         => 'quotationplan',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'  
    //             ],
    //             [
    //                 'text'        => 'Packing List',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Shipping',
    //                         'url'         => 'packlist',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Non Shipping',
    //                         'url'         => 'packlistnon',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]
    //             ],
    //             [
    //                 'text'        => 'Invoice',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Shipping',
    //                         'url'         => 'invoice',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Non Shipping',
    //                         'url'         => 'invoicenon',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]  
    //             ],
    //             [
    //                 'text'        => 'DO Confirmation',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Shipping',
    //                         'url'         => 'stuffconfirm',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Non Shipping',
    //                         'url'         => 'stuffconfirmnon',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]
    //             ],
    //             [
    //                 'text'        => 'DO Re-Route',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Shipping',
    //                         'url'         => 'stuffreroute',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Non Shipping',
    //                         'url'         => 'stuffreroutenon',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]
    //             ]
    //         ]
    //     ],
    //     [
    //         'text'        => 'EMKL',
    //         'icon'        => 'ship',
    //         'icon_color'  => 'purple',
    //         'submenu' => [
    //             [
    //                 'text'        => 'Shipping Schedule',
    //                 'url'         => 'scheduleship',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Approval',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Booking Schedule',
    //                         'url'         => 'approvalbook',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Stuffing Schedule',
    //                         'url'         => 'approvalstuff',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]
    //             ],
    //             [
    //                 'text'        => 'Weighing',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Shipping',
    //                         'url'         => 'weighemkl',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Non Shipping',
    //                         'url'         => 'weighemklnon',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]
    //             ]          
    //         ]
    //     ],
    //     [
    //         'text'        => 'Delivery',
    //         'icon'        => 'folder-open',
    //         'icon_color'  => 'purple',
    //         'submenu' => [
    //             [
    //                 'text'        => 'Booking Schedule',
    //                 'url'         => 'schedulebook',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Stuffing Schedule',
    //                 'url'         => 'schedulestuff',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Stuffing',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Shipping',
    //                         'url'         => 'stuff',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Non Shipping',
    //                         'url'         => 'stuffnon',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]
    //             ],
    //             [
    //                 'text'        => 'Delivery Order',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Shipping',
    //                         'url'         => 'stufforder',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Non Shipping',
    //                         'url'         => 'stuffordernon',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]
    //             ],
    //             [
    //                 'text'        => 'Weighing',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Shipping',
    //                         'url'         => 'weigh',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Non Shipping',
    //                         'url'         => 'weighnon',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]
    //             ],
    //             [
    //                 'text'        => 'Packing List',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Shipping',
    //                         'url'         => 'pack',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Non Shipping',
    //                         'url'         => 'packnon',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]
    //             ],
    //             [
    //                 'text'        => 'Document Check',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Shipping',
    //                         'url'         => 'documentcheck',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Non Shipping',
    //                         'url'         => 'documentchecknon',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]
    //             ],
    //             [
    //                 'text'        => 'Stuffing Re-Schedule',
    //                 'url'         => 'stuffreschedule',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ]
    //         ]
    //     ],
    //     [
    //         'text'        => 'Purchasing',
    //         'icon'        => 'tags',
    //         'icon_color'  => 'purple',
    //         'submenu' => [
    //             [
    //                 'text'        => 'Purchase Request',
    //                 'url'         => 'purchaserequest',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Purchase General',
    //                 'url'         => 'purchasegeneral',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Purchase Order',
    //                 'url'         => 'purchaseorder',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Purchase Derivative',
    //                 'url'         => 'purchasederivative',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Incoming Stock',
    //                 'url'         => 'purchaseincome',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ]
    //         ]
    //     ],
    //     [
    //         'text'        => 'Inventory',
    //         'icon'        => 'edit',
    //         'icon_color'  => 'purple',
    //         'submenu' => [
    //             [
    //                 'text'        => 'Request Stock',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Shipping',
    //                         'url'         => 'stockrequest',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Non Shipping',
    //                         'url'         => 'stockrequestnon',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]
    //             ],
    //             [
    //                 'text'        => 'Loading Stock',
    //                 'url'         => 'stockload',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Quality Control',
    //                 'url'         => 'stockqc',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Incoming Stock',
    //                 'url'         => 'stockin',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Moving Stock',
    //                 'url'         => 'stockmove',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Outgoing Stock',
    //                 'url'         => 'stockout',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Stock Card',
    //                 'url'         => 'stockcard',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Stock Opname',
    //                 'url'         => 'stockopname',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Assembly / Production',
    //                 'url'         => 'production',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Cut Off',
    //                 'url'         => 'cutoff',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ]
    //         ]
    //     ],
    //     [
    //         'text'        => 'Monitoring',
    //         'icon'        => 'tasks',
    //         'icon_color'  => 'purple',
    //         'submenu' => [
    //             [
    //                 'text'        => 'Stock',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Per Product',
    //                         'url'         => 'stockproduct',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Per Warehouse',
    //                         'url'         => 'stockwarehouse',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]
    //             ],
    //             [
    //                 'text'        => 'Finance',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Debt',
    //                         'url'         => 'debtmonitor',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Receivable',
    //                         'url'         => 'receivablemonitor',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]
    //             ]
    //         ]
    //     ],
    //     [
    //         'text'        => 'Approval',
    //         'icon'        => 'pencil',
    //         'icon_color'  => 'purple',
    //         'submenu' => [
    //             [
    //                 'text'        => 'Packing List',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Shipping',
    //                         'url'         => 'approvalpack',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Non Shipping',
    //                         'url'         => 'approvalpacknon',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]
    //             ],
    //             [
    //                 'text'        => 'Purchase Request',
    //                 'url'         => 'approvalpr',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Purchase General',
    //                 'url'         => 'approvalprg',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Purchase Order',
    //                 'url'         => 'approvalpo',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Purchase Derivative',
    //                 'url'         => 'approvalpd',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ]            
    //         ]
    //     ],
    //     [
    //         'text'        => 'User Management',
    //         'icon'        => 'user',
    //         'icon_color'  => 'purple',
    //         'submenu' => [
    //             [
    //                 'text'        => 'Menu',
    //                 'url'         => 'menu',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Group',
    //                 'url'         => 'group',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Group Access',
    //                 'url'         => 'groupset',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'User',
    //                 'url'         => 'employee',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'User Access',
    //                 'url'         => 'employeeset',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'User Warehouse',
    //                 'url'         => 'employeewarehouse',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ]
    //         ]
    //     ],
    //     [
    //         'text'        => 'Mailbox',
    //         'icon'        => 'envelope',
    //         'icon_color'  => 'purple',
    //         'submenu' => [
    //             [
    //                 'text'        => 'Contact',
    //                 'url'         => 'mailcontact',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Order',
    //                 'url'         => 'mailorder',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ]
    //         ]
    //     ],
    //     [
    //         'text'        => 'Finance',
    //         'icon'        => 'file',
    //         'icon_color'  => 'purple',
    //         'submenu' => [
    //             [
    //                 'text'        => 'Journal',
    //                 'url'         => 'journal',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Journal Process',
    //                 'url'         => 'journalset',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow'
    //             ],
    //             [
    //                 'text'        => 'Transaction',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Debt',
    //                         'url'         => 'debt',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Receivable',
    //                         'url'         => 'receivable',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]
    //             ],
    //             [
    //                 'text'        => 'Card',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Debt',
    //                         'url'         => 'debtcard',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Receivable',
    //                         'url'         => 'receivablecard',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]
    //             ],
    //             [
    //                 'text'        => 'Cut Off',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Debt',
    //                         'url'         => 'debtcutoff',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Receivable',
    //                         'url'         => 'receivablecutoff',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]
    //             ],
    //             [
    //                 'text'        => 'DO Confirmation',
    //                 'icon'        => 'chevron-right',
    //                 'icon_color'  => 'yellow',
    //                 'submenu' => [
    //                     [
    //                         'text'        => 'Shipping',
    //                         'url'         => 'stuffcheck',
    //                         'icon_color'  => 'red'  
    //                     ],
    //                     [
    //                         'text'        => 'Non Shipping',
    //                         'url'         => 'stuffchecknon',
    //                         'icon_color'  => 'red'  
    //                     ]
    //                 ]
    //             ]
    //         ]
    //     ]
    // ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => true,
        'select2'    => true,
        'chartjs'    => true,
    ],

    /*
    |--------------------------------------------------------------------------
    | APPLICATION SERVER SETTING
    |--------------------------------------------------------------------------
    |
    | This array of parameter belong to ALFALAH System
    |
    */
    'system_company' => env('APP_LOG', 'ALFALAH'),
    'system_site' => env('APP_LOG', 'SBY'),
    'system_skin' => env('APP_SKIN', 'Ext'),
];
