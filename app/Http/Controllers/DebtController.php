<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Exports\DebtsExport;
use App\Http\Models\Debt;
use App\Http\Models\Debtcard;
use App\Http\Models\Stockin;
use App\Http\Models\Supplier;
use App\Http\Models\Supplierdetail;
use App\Http\Models\Employeeset;

class DebtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $debts = Debt::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/debt.index', compact('debts','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $debts = Debt::orderBy('code_supplier','asc')->get();

        return view('vendor/adminlte/debt.list', compact('debts','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function payment(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $debt = Debt::find($id);
        $amount = $debt->total-$debt->payment-$debt->adjustment;

        $request->validate([
            'payment'       => 'numeric|min:0',
            'adjustment'    => 'numeric|min:0'
        ]);

        if ($request->get('payment') > $amount) {
            return redirect()->back()->withErrors('Payment greater than Total');
        } elseif ($request->get('adjustment') > ($debt->total-$debt->payment)) {
            return redirect()->back()->withErrors('Adjustment greater than Total');
        } else {
            $debt->payment           = $debt->payment+$request->get('payment');
            $debt->adjustment        = $debt->adjustment+$request->get('adjustment');
            if ($request->get('payment') == $amount) {
                $debt->status = 1;
            }
            $debt->updated_user      = $session_id;
            $debt->save();

            $supplier = Supplier::where('code', $debt->code_supplier)->first();
            $supplierdetail = Supplierdetail::where([['supplier', $supplier->id], ['status', 1]])->first();
            $stockin = Stockin::where('noref_in', $debt->no_po)->first();

            $debtcard = Debtcard::where([['supplierdetail', $supplierdetail->id], ['year', date('Y', strtotime($stockin->date_in))], ['month', date('m', strtotime($stockin->date_in))]])->first();
            $debtcard->kredit = $debtcard->kredit+($debt->payment);
            $debtcard->save();

            return redirect('/debt')->with('success', 'Debt Transaction has been paid');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function card(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $supplierdetails = Supplierdetail::where('status', 1)->get();

        return view('vendor/adminlte/debt.card', compact('supplierdetails','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $supplierdetails = Supplierdetail::where('status', 1)->get();

        $request->validate([
            'year'  => 'numeric|min:0|not_in:0'
        ]);

        $date_begin = $request->get('year').'-'.$request->get('month').'-01';
        if ($request->get('month') == 2) {
            $date_end = $request->get('year').'-'.$request->get('month').'-28';
        } elseif ($request->get('month') == 1 || $request->get('month') == 3 || $request->get('month') == 5 || $request->get('month') == 7 || $request->get('month') == 8 || $request->get('month') == 10 || $request->get('month') == 12) {
            $date_end = $request->get('year').'-'.$request->get('month').'-31';
        } elseif ($request->get('month') == 4 || $request->get('month') == 6 || $request->get('month') == 9 || $request->get('month') == 11) {
            $date_end = $request->get('year').'-'.$request->get('month').'-30';
        }
        
        $supplier = Supplierdetail::find($request->get('supplierdetail')); 
        $debts = Debt::where([['code_supplier', $supplier->fkSupplier->code], ['date_po','>=',$date_begin], ['date_po','<=',$date_end]])->orderBy('date_po','asc')->get();
        $debtold = Debtcard::where([['supplierdetail', $request->get('supplierdetail')], ['year', $request->get('year')], ['month', $request->get('month')-1]])->first();
        $debtcard = Debtcard::where([['supplierdetail', $request->get('supplierdetail')], ['year', $request->get('year')], ['month', $request->get('month')]])->first();
        $year = $request->get('year');
        $month = $request->get('month');

        return view('vendor/adminlte/debt.filter', compact('supplierdetails','supplier','debts','debtold','debtcard','year','month','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export() 
    {
        return Excel::download(new DebtsExport, 'Debt.xlsx');
    }
}
