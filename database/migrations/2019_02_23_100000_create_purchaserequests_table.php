<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaserequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchaserequests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product')->unsigned();
            $table->integer('quotationdetail');
            $table->integer('supplierdetail');
            $table->integer('warehouse');
            $table->integer('no_inc');
            $table->string('no_pr');
            $table->date('date_request');
            $table->integer('qty_request');
            $table->integer('qty_remain');
            $table->string('no_reference')->nullable();
            $table->text('notice')->nullable();
            $table->text('reason')->nullable();
            $table->tinyInteger('approve');
            $table->tinyInteger('status_approve');
            $table->tinyInteger('status_plan');
            $table->tinyInteger('status_order')->default('0');
            $table->tinyInteger('status_release')->default('0');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->timestamp('approved_at')->nullable();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->integer('approved_user')->nullable();
            $table->foreign('product')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchaserequests');
    }
}
