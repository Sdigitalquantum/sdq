<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $fillable = [
	    'product',
	    'status_group',
	    'status_record',
	    'created_user',
	    'updated_user'
  	];

  	public function fkProduct(){
	    return $this->belongsTo('\App\Http\Models\Product', 'product', 'id');
	}
}
