<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
	    'customer',
	    'no_po',
	    'email',
	    'phone',
	    'address',
	    'notice'
  	];
}
