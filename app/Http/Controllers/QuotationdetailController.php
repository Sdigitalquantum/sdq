<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Quotation;
use App\Http\Models\Quotationdetail;
use App\Http\Models\Product;
use App\Http\Models\Productalias;
use App\Http\Models\Pricelist;
use App\Http\Models\Currency;
use App\Http\Models\Kurs;
use App\Http\Models\Unit;
use App\Http\Models\Employeeset;

class QuotationdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $cek = Quotationdetail::where([['quotation', $request->get('quotation')], ['product', $request->get('product')]])->first();
        $cekproduct = Product::where('id', $request->get('product'))->first();

        $request->validate([
            'qty_fcl' => 'numeric|min:0|not_in:0',
            'qty_bag' => 'numeric|min:0|not_in:0',
            'qty_kg'  => 'numeric|min:0|not_in:0',
            'qty_pcs' => 'numeric|min:0|not_in:0',
            'price'   => 'numeric|min:0|not_in:0',
            'disc_value'   => 'nullable|numeric|min:0'
        ]);
        
        if (!empty($cek)) {
            return redirect()->back()->withErrors('Quotation Detail already exists');
        } else {
            $quotation = Quotation::where('id', $request->get('quotation'))->first();
            $currency = Currency::where('id', $quotation->currency)->first();

            if ($currency->code == 'IDR') {
                $kurs = 1;
            } else {
                if ($quotation->kurs != 0) {
                    $kurs = $quotation->kurs;
                } else {
                    $kurscek = Kurs::where('currency', $quotation->currency)->whereDate('start', '<=', $quotation->date_sp)->whereDate('end', '>=', $quotation->date_sp)->first();
                    if (!empty($kurscek)) {
                        $kurs = $kurscek->value;
                    } else {
                        $kurs = 0;
                    }            
                }
            }

            $quotationdetail = new Quotationdetail([
                'quotation'     => $request->get('quotation'),
                'product'       => $request->get('product'),
                'unit'          => $cekproduct->unit,
                'qty_fcl'       => $request->get('qty_fcl'),
                'qty_bag'       => $request->get('qty_bag'),
                'qty_kg'        => $request->get('qty_kg'),
                'qty_pcs'       => $request->get('qty_pcs'),
                'price'         => $request->get('price'),
                'kurs'          => $kurs,
                'disc_type'     => !empty($request->get('disc_type')) ? $request->get('disc_type') : 0,
                'disc_value'    => !empty($request->get('disc_value')) ? $request->get('disc_value') : '',
                'alias_name'    => !empty($request->get('alias_name')) ? $request->get('alias_name') : '',
                'alias_qty'     => !empty($request->get('alias_qty')) ? $request->get('alias_qty') : '',
                'qty_amount'    => $request->get('qty_kg'),
                'created_user'  => $session_id,
                'updated_user'  => $session_id
            ]);
            $quotationdetail->save();

            $quotation->qty_product     = $quotation->qty_product+1;
            $quotation->status_product  = 1;
            $quotation->save();

            if (!empty($request->get('alias_name'))) {
                $productalias = Productalias::where([['product', $request->get('product')], ['customerdetail', $quotation->customerdetail]])->first();
                $productalias->alias_name = $request->get('alias_name');
                $productalias->save();
            }

            return redirect('/quotationdetail/'.$request->get('quotation'))->with('success', 'Quotation Detail has been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $quotation = Quotation::find($id);
        $quotationdetails = Quotationdetail::where('quotation', $id)->orderBy('created_at','desc')->get();
        $products = Product::where('status', 1)->get();
        $units = Unit::where('status', 1)->get();

        return view('vendor/adminlte/quotationdetail.show', compact('id','quotation','quotationdetails','products','units','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $quotationdetail = Quotationdetail::find($id);
        $quotationdetails = Quotationdetail::where('quotation', $quotationdetail->quotation)->orderBy('created_at','desc')->get();
        $products = Product::where('status', 1)->get();
        $units = Unit::where('status', 1)->get();

        return view('vendor/adminlte/quotationdetail.edit', compact('quotationdetail','quotationdetails','products','units','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $quotationdetail = Quotationdetail::find($id);
        $product = $quotationdetail->product;
        $quotation = Quotation::where('id', $request->get('quotation'))->first();

        $request->validate([
            'qty_fcl' => 'numeric|min:0|not_in:0',
            'qty_bag' => 'numeric|min:0|not_in:0',
            'qty_kg'  => 'numeric|min:0|not_in:0',
            'qty_pcs' => 'numeric|min:0|not_in:0',
            'price'   => 'numeric|min:0|not_in:0',
            'disc_value'   => 'nullable|numeric|min:0'
        ]);

        if ($product != $request->get('product')) {
            $cek = Quotationdetail::where([['quotation', $request->get('quotation')], ['product', $request->get('product')]])->first();
            $cekproduct = Product::where('id', $request->get('product'))->first();

            if (!empty($cek)) {
                return redirect()->back()->withErrors('Quotation Detail already exists');
            } else {
                if ($quotation->kurs != 0) {
                    $kurs = $quotation->kurs;
                } else {
                    $kurscek = Kurs::where('currency', $quotation->currency)->whereDate('start', '<=', $quotation->date_sp)->whereDate('end', '>=', $quotation->date_sp)->first();
                    if (!empty($kurscek)) {
                        $kurs = $kurscek->value;
                    } else {
                        $kurs = 0;
                    }            
                }

                $quotationdetail->quotation     = $request->get('quotation');
                $quotationdetail->product       = $request->get('product');
                $quotationdetail->unit          = $cekproduct->unit;
                $quotationdetail->qty_fcl       = $request->get('qty_fcl');
                $quotationdetail->qty_bag       = $request->get('qty_bag');
                $quotationdetail->qty_kg        = $request->get('qty_kg');
                $quotationdetail->qty_pcs       = $request->get('qty_pcs');
                $quotationdetail->price         = $request->get('price');
                $quotationdetail->kurs          = $kurs;
                $quotationdetail->disc_type     = !empty($request->get('disc_type')) ? $request->get('disc_type') : 0;
                $quotationdetail->disc_value    = !empty($request->get('disc_value')) ? $request->get('disc_value') : '';
                $quotationdetail->alias_name    = !empty($request->get('alias_name')) ? $request->get('alias_name') : '';
                $quotationdetail->alias_qty     = !empty($request->get('alias_qty')) ? $request->get('alias_qty') : '';
                $quotationdetail->qty_amount    = $request->get('qty_kg');
                $quotationdetail->updated_user  = $session_id;
                $quotationdetail->save();

                if (!empty($request->get('alias_name'))) {
                    $productalias = Productalias::where([['product', $request->get('product')], ['customerdetail', $quotation->customerdetail]])->first();
                    $productalias->alias_name = $request->get('alias_name');
                    $productalias->save();
                }

                return redirect('/quotationdetail/'.$request->get('quotation'))->with('success', 'Quotation Detail has been updated');
            }
        } else {
            if ($quotation->kurs != 0) {
                $kurs = $quotation->kurs;
            } else {
                $kurscek = Kurs::where('currency', $quotation->currency)->whereDate('start', '<=', $quotation->date_sp)->whereDate('end', '>=', $quotation->date_sp)->first();
                if (!empty($kurscek)) {
                    $kurs = $kurscek->value;
                } else {
                    $kurs = 0;
                }            
            }

            $quotationdetail->quotation     = $request->get('quotation');
            $quotationdetail->product       = $request->get('product');
            $quotationdetail->qty_fcl       = $request->get('qty_fcl');
            $quotationdetail->qty_bag       = $request->get('qty_bag');
            $quotationdetail->qty_kg        = $request->get('qty_kg');
            $quotationdetail->qty_pcs       = $request->get('qty_pcs');
            $quotationdetail->price         = $request->get('price');
            $quotationdetail->kurs          = $kurs;
            $quotationdetail->disc_type     = !empty($request->get('disc_type')) ? $request->get('disc_type') : 0;
            $quotationdetail->disc_value    = !empty($request->get('disc_value')) ? $request->get('disc_value') : '';
            $quotationdetail->alias_name    = !empty($request->get('alias_name')) ? $request->get('alias_name') : '';
            $quotationdetail->alias_qty     = !empty($request->get('alias_qty')) ? $request->get('alias_qty') : '';
            $quotationdetail->qty_amount    = $request->get('qty_kg');
            $quotationdetail->updated_user  = $session_id;
            $quotationdetail->save();

            if (!empty($request->get('alias_name'))) {
                $productalias = Productalias::where([['product', $request->get('product')], ['customerdetail', $quotation->customerdetail]])->first();
                $productalias->alias_name = $request->get('alias_name');
                $productalias->save();
            }

            return redirect('/quotationdetail/'.$request->get('quotation'))->with('success', 'Quotation Detail has been updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $quotationdetail = Quotationdetail::find($id);
        $status = $quotationdetail->status;

        if ($status == 1) {
        	$quotationdetail->status       = 0;
            $quotationdetail->updated_user = $session_id;
	        $quotationdetail->save();

            $quotation = Quotation::where('id', $quotationdetail->quotation)->first();
            $quotation->qty_product = $quotation->qty_product-1;
            $quotation->save();

		    return redirect('/quotationdetail/'.$quotationdetail->quotation)->with('error', 'Quotation Detail has been deleted');
        } elseif ($status == 0) {
        	$quotationdetail->status       = 1;
            $quotationdetail->updated_user = $session_id;
	        $quotationdetail->save();

            $quotation = Quotation::where('id', $quotationdetail->quotation)->first();
            $quotation->qty_product = $quotation->qty_product+1;
            $quotation->save();

		    return redirect('/quotationdetail/'.$quotationdetail->quotation)->with('success', 'Quotation Detail has been activated');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ajax($id_url, $id)
    {
        $price = Pricelist::where('product', $id)->first();
        
        return json_encode($price);
    }
}
