<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Stockqc extends Model
{
    protected $fillable = [
	    'purchasedetail',
	    'product',
	    'warehouse',
	    'supplierdetail',
	    'no_inc',
	    'no_po',
	    'date_in',
	    'qty_bag',
	    'qty_pcs',
	    'qty_kg',
	    'qty_avg',
	    'price',
	    'notice',
	    'reason',
	    'created_user',
	    'updated_user',
	    'approved_at',
	    'approved_user'
  	];

	public function fkPurchasedetail(){
	    return $this->belongsTo('\App\Http\Models\Purchasedetail', 'purchasedetail', 'id');
	}

  	public function fkProduct(){
	    return $this->belongsTo('\App\Http\Models\Product', 'product', 'id');
	}

	public function fkWarehouse(){
	    return $this->belongsTo('\App\Http\Models\Warehouse', 'warehouse', 'id');
	}

	public function fkSupplierdetail(){
	    return $this->belongsTo('\App\Http\Models\Supplierdetail', 'supplierdetail', 'id');
	}
}
