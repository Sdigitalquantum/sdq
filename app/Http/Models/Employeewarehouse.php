<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Employeewarehouse extends Model
{
    protected $fillable = [
	    'employee',
	    'warehouse',
	    'created_user',
	    'updated_user'
  	];

  	public function fkEmployee(){
	    return $this->belongsTo('\App\Http\Models\Employee', 'employee', 'id');
	}

  	public function fkWarehouse(){
	    return $this->belongsTo('\App\Http\Models\Warehouse', 'warehouse', 'id');
	}
}
