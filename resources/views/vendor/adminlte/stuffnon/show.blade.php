@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
            <li class="disabled"><a>List</a></li>
          	<li class="active"><a href="#tab_preview" data-toggle="tab">Print</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_preview">
			    <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Preview Delivery Order - Non Shipping
				  			&nbsp;<a href="javascript:window.print()" class="btn btn-xs btn-default" style="text-decoration: none;"><i class="glyphicon glyphicon-print"></i></a>
				  			&nbsp;<a href="{{ url('stuffordernon') }}" class="btn btn-xs btn-danger" style="text-decoration: none;">Back</a>
				  		</h4>
			  		</div>
				  	
				  	<div class="box-body">
				  		<div id="printable">
						  	<table width="100%">
						  		<tr>
						  			<td rowspan="2" width="20%" style="text-align: right;"><img src="{{ asset('sdq_logo.jpg') }}" width="100"></td>
						  			<td width="70%" style="text-align: center; font-weight: bold; font-size: 20pt;">{{ $company->name }}</td>
						  			<td width="10%">&nbsp;</td>
					  			</tr>
					  			<tr>
						  			<td style="text-align: center; font-weight: bold;">
						  				{{ $company->address }} <br>
						  				{{ $company->fkCity->name }} - {{ $company->fkCity->fkCountry->name }}
						  			</td>
						  			<td>&nbsp;</td>
					  			</tr>
				  			</table><hr>

				  			<font style="font-size: 1.2vw;">
				  				<center>
				  					<b>DELIVERY ORDER</b> <br>
					  				PO : @if(!empty($schedulestuff->fkQuotation->no_poc)) {{ $schedulestuff->fkQuotation->no_poc }} @else N/A @endif <br>
					  				DATE : {{ date('d F Y', strtotime($stufforder->created_at)) }} <br>
					  				NO : {{ $stufforder->no_letter }}
					  			</center><br>

					  			<table class="border" width="100%">
					  				<tr>
					  					<td width="30%" valign="top" style="font-weight: bold;">Shipper</td>
					  					<td width="70%">
					  						{{ $company->name }} <br>
					  						{{ $company->address }} <br>
					  						{{ $company->fkCity->name }} - {{ $company->fkCity->fkCountry->name }}
					  					</td>
				  					</tr>
				  					<tr>
				  						<td colspan="2">&nbsp;</td>
									</tr>
				  					<tr>
					  					<td valign="top" style="font-weight: bold;">Consignee</td>
					  					<td>
					  						{{ $schedulestuff->fkQuotation->fkCustomerdetail->name }} <br>
					  						{{ $schedulestuff->fkQuotation->fkCustomerdetail->address }} <br>
					  						{{ $schedulestuff->fkQuotation->fkCustomerdetail->fkCity->name }} - {{ $schedulestuff->fkQuotation->fkCustomerdetail->fkCity->fkCountry->name }}
					  					</td>
				  					</tr>
				  				</table><br>

				  				<table class="border" width="100%">
					  				<tr>
					  					<td width="30%" valign="top" style="font-weight: bold;">
					  						BENEFICIARY NAME <br>
					  						BENEFICIARY BANK <br>
					  						BANK ACCOUNT ({{ $schedulestuff->fkQuotation->fkCompanyaccount->fkCurrency->code }}) <br>
					  						SWIFT CODE <br>
					  						BANK ADDRESS
					  					</td>
					  					<td width="70%">
					  						{{ $schedulestuff->fkQuotation->fkCompanyaccount->account_name }} <br>
					  						{{ $schedulestuff->fkQuotation->fkCompanyaccount->fkBank->name }} <br>
					  						{{ $schedulestuff->fkQuotation->fkCompanyaccount->account_no }} <br>
					  						{{ $schedulestuff->fkQuotation->fkCompanyaccount->account_swift }} <br>
					  						{{ $schedulestuff->fkQuotation->fkCompanyaccount->account_address }} <br>
					  						{{ $schedulestuff->fkQuotation->fkCompanyaccount->fkCity->name }} - {{ $schedulestuff->fkQuotation->fkCompanyaccount->fkCity->fkCountry->name }}
					  					</td>
				  					</tr>
				  				</table><br>

				  				<table class="border" width="100%">
					  				<tr>
					  					<td width="30%" valign="top" style="font-weight: bold;">Item Description</td>
					  					<td width="70%">
					  						@foreach($quotationdetails as $quotationdetail)
					  							@if(empty($quotationdetail->alias_name))
								            		{{$quotationdetail->fkProduct->name}} <br>
							            		@else
							            			{{$quotationdetail->alias_name}} <br>
							            		@endif
					  						@endforeach
					  					</td>
				  					</tr>
				  					<tr>
					  					<td valign="top" style="font-weight: bold;">Container / Seal</td>
					  					<td>
					  						@foreach($stuffs as $stuff)
					  							{{$stuff->vehicle}} / {{$stuff->seal}} <br>
					  						@endforeach
					  					</td>
				  					</tr>
				  					<tr>
					  					<td style="font-weight: bold;">Vessel</td>
					  					<td>{{ $stuff->fkQuotationsplit->fkQuotation->vessel }}</td>
				  					</tr>
				  					<tr>
					  					<td style="font-weight: bold;">Date of Shipment</td>
					  					<td>{{ date('d F Y', strtotime($stuff->etd)) }}</td>
				  					</tr>
				  					<tr>
					  					<td style="font-weight: bold;">Port of Loading</td>
					  					<td>{{ $stuff->fkPortl->name }} - {{ $stuff->fkPortl->fkCountry->name }}</td>
				  					</tr>
				  					<tr>
					  					<td style="font-weight: bold;">Port of Discharge</td>
					  					<td>{{ $stuff->fkPortd->name }} - {{ $stuff->fkPortd->fkCountry->name }}</td>
				  					</tr>
				  					<tr>
					  					<td colspan="2" style="font-weight: bold;">Detail item</td>
				  					</tr>
				  					<tr>
					  					<td colspan="2">
					  						<table border="3" width="100%" style="text-align: center; font-weight: bold;">
					  							<tr>
					  								<td width="30%" style="text-align: left;" valign="top">
					  									COMMODITY <br>
					  									@foreach($quotationdetails as $quotationdetail)
								  							@if(empty($quotationdetail->alias_name))
											            		{{$quotationdetail->fkProduct->name}} <br>
										            		@else
										            			{{$quotationdetail->alias_name}} <br>
										            		@endif
								  						@endforeach
					  								</td>
					  								<td width="10%" valign="top">QTY <br>(Fcl)</td>
					  								<td width="20%" valign="top">QTY <br>(Bags)</td>
					  								<td width="20%" valign="top">NUT QTY <br>(Pcs)</td>
					  								<td width="20%" valign="top">NUT QTY <br>(Kgs)</td>
				  								</tr>

				  								@php ($fcl = 0)
				  								@php ($bag = 0)
				  								@php ($pcs = 0)
				  								@php ($kg = 0)
				  								@foreach($stuffs as $stuff)
						  							<tr>
						  								<td style="text-align: left;">{{ $stuff->vehicle }}</td>
						  								<td style="text-align: right;">{{ $stuff->qty_fcl }} &nbsp;</td>
						  								<td style="text-align: right;">{{ $stuff->qty_bag }} &nbsp;</td>
						  								<td style="text-align: right;">{{number_format($stuff->qty_pcs, 0, ',' , '.')}} &nbsp;</td>
						  								<td style="text-align: right;">{{number_format($stuff->qty_kg, 0, ',' , '.')}} &nbsp;</td>
					  								</tr>

					  								@php ($fcl = $fcl + $stuff->qty_fcl)
					  								@php ($bag = $bag + $stuff->qty_bag)
					  								@php ($pcs = $pcs + $stuff->qty_pcs)
					  								@php ($kg  = $kg + $stuff->qty_kg)
						  						@endforeach

						  						<tr>
					  								<td colspan="5">&nbsp;</td>
				  								</tr>
				  								<tr>
					  								<td style="text-align: left;">TOTAL</td>
					  								<td style="text-align: right;">{{ $fcl }} &nbsp;</td>
					  								<td style="text-align: right;">{{ $bag }} &nbsp;</td>
					  								<td style="text-align: right;">{{number_format($pcs, 0, ',' , '.')}} &nbsp;</td>
					  								<td style="text-align: right;">{{number_format($kg, 0, ',' , '.')}} &nbsp;</td>
				  								</tr>
				  							</table>
					  					</td>
				  					</tr>
				  				</table><br>

				  				<table width="100%">
					  				<tr>
					  					<td width="30%" style="font-weight: bold;">{{ $quotation->fkPaymentterm->code }}</td>
					  					<td width="40%">{{ $stuff->fkPortd->name }} - {{ $stuff->fkPortd->fkCountry->name }}</td>
					  					<td width="30%">&nbsp;</td>
				  					</tr>
				  					<tr>
					  					<td style="font-weight: bold;">ETD</td>
					  					<td>{{ date('d F Y', strtotime($stuff->etd)) }}</td>
					  					<td style="text-align: center;">Surabaya, {{ date('d F Y', strtotime($stufforder->created_at)) }}</td>
				  					</tr>
				  					<tr>
					  					<td style="font-weight: bold;">ETA</td>
					  					<td>{{ date('d F Y', strtotime($stuff->eta)) }}</td>
					  					<td style="text-align: center;">{{ $company->name }}</td>
				  					</tr>
				  					<tr>
					  					<td valign="top" style="font-weight: bold;">Payment Term</td>
					  					<td>
					  						@foreach($quotationpayments as $quotationpayment)
					  							{{ $quotationpayment->fkPayment->value }}% - {{ $quotationpayment->fkPayment->condition }} <br>
					  						@endforeach	
					  					</td>
					  					<td rowspan="3" style="text-align: center;"><img src="{{ asset('sdq_logo.jpg') }}" width="75"></td>
				  					</tr>
				  					<tr>
					  					<td style="font-weight: bold;">GROSS WEIGHT (Kgs)</td>
					  					<td>{{ $schedulestuff->fkQuotation->qty_gross }}</td>
				  					</tr>
				  					<tr>
					  					<td style="font-weight: bold;">NETT WEIGHT (Kgs)</td>
					  					<td>{{ $kg }}</td>
				  					</tr>
				  					<tr>
					  					<td style="font-weight: bold;">CBM</td>
					  					<td>{{ $schedulestuff->fkQuotation->qty_cbm }} CBM</td>
					  					<td style="text-align: center;">AUTHORIZED SIGNATURE</td>
				  					</tr>
								</table>
							</font>

							<br><font style="font-size: 1.0vw;">[ Cetakan ke - {{ $stufforder->qty_print }} ]</font>
			  			</div>
				  	</div>
				<div>
			</div>
		</div>
	</div>
@stop