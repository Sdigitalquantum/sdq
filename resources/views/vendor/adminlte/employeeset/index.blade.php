@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>Menu</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
                <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">User Access</h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="20%">Group</th>
						          	<th width="20%">Name</th>
						          	<th width="20%">Email</th>
						          	<th width="25%">Modules</th>
						          	<th width="10%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($employees as $employee)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>{{$employee->fkGroup->name}}</td>
							            <td>{{$employee->name}}</td>
							            <td>{{$employee->email}}</td>
							            <td>
							            	@foreach($employeesets as $employeeset)
							            		@if($employeeset->employee == $employee->id)
							            			<i class="fa fa-fw fa-{{$employeeset->fkMenuroot->icon}} text-{{$employeeset->fkMenuroot->icon_color}}"></i> {{$employeeset->fkMenuroot->name}} <br>
							            		@endif
							            	@endforeach
							            </td>

							            <td>
							            	<a href="{{ route('employeeset.edit',$employee->id)}}" class="btn btn-xs btn-success">Access Menu</a>
							            </td>
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Group</td>
						          	<td>Name</td>
						          	<td>Email</td>
						          	<td>Modules</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop