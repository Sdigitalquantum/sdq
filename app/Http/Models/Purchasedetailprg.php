<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Purchasedetailprg extends Model
{
    protected $fillable = [
	    'purchasegeneral',
	    'product',
	    'supplierdetail',
	    'warehouse',
	    'qty_request',
	    'qty_remain',
	    'created_user',
	    'updated_user'
  	];

	public function fkPurchasegeneral(){
	    return $this->belongsTo('\App\Http\Models\Purchasegeneral', 'purchasegeneral', 'id');
	}

  	public function fkProduct(){
	    return $this->belongsTo('\App\Http\Models\Product', 'product', 'id');
	}

	public function fkSupplierdetail(){
	    return $this->belongsTo('\App\Http\Models\Supplierdetail', 'supplierdetail', 'id');
	}

	public function fkWarehouse(){
	    return $this->belongsTo('\App\Http\Models\Warehouse', 'warehouse', 'id');
	}
}
