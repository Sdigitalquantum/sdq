<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyaccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companyaccounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company')->unsigned();
            $table->integer('currency')->unsigned();
            $table->integer('bank')->unsigned();
            $table->integer('city')->unsigned();
            $table->bigInteger('account_no');
            $table->string('account_swift');
            $table->string('account_name');
            $table->string('account_address');
            $table->string('alias')->nullable();
            $table->tinyInteger('export');
            $table->tinyInteger('local');
            $table->tinyInteger('internal');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->foreign('company')->references('id')->on('companies');
            $table->foreign('currency')->references('id')->on('currencies');
            $table->foreign('bank')->references('id')->on('banks');
            $table->foreign('city')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companyaccounts');
    }
}
