@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">New Menu &nbsp;<a href="{{ url('menu')}}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<form method="post" action="{{ route('menu.store') }}">
				          	@csrf
				          	<div class="row">
								<div class="col-md-6">
									<div class="form-group">
						          		<label for="root">Root Menu</label>
						              	<input type="text" class="form-control" value="{{ $menuroot->name }}" disabled>
						          	</div>
									<div class="form-group">
						              	<label for="nomor">Nomor</label>
						              	<input type="hidden" name="menuroot" value="{{ $menuroot->id }}">
						              	<input type="hidden" name="nomor" value="{{ $nomor }}">
						              	<input type="number" class="form-control" value="{{ $nomor }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						              	<label for="url">URL</label>
						              	<input type="text" class="form-control" name="url" value="{{ old('url') }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="icon_color">Icon Color</label>
						              	<input type="text" class="form-control" name="icon_color" value="{{ old('icon_color') }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="button">&nbsp;</label> <br>
						              	<button type="submit" class="btn btn-sm btn-success">Save</button>
						          	</div>
								</div>
							</div>
				      	</form>

				      	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="25%">Name</th>
						          	<th width="25%">URL</th>
						          	<th width="20%">Icon</th>
						          	<th width="20%">Icon Color</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@foreach($menus as $menu)
						    		<tr>
							            <td>{{$menu->nomor}}</td>
							            <td>{{$menu->name}}</td>
							            <td>{{$menu->url}}</td>
							            <td>{{$menu->icon}}</td>
							            <td>{{$menu->icon_color}}</td>

							            @if($menu->status == 1)
								            <td>
								            	<a href="{{ route('menu.edit',$menu->id)}}" class="btn btn-xs btn-warning">Edit</a> &nbsp;
								            	
								            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$menu->id}}">Delete</button>

								            	<form action="{{ route('menu.destroy', $menu->id)}}" method="post">
								                  	@csrf
								                  	@method('DELETE')
								                  	<div class="modal fade bs-example-modal{{$menu->id}}" tabindex="-1" role="dialog" aria-hidden="true">
					                                    <div class="modal-dialog modal-sm">
					                                        <div class="modal-content">
					                                            <div class="modal-header btn-danger">
					                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
					                                                </button>
					                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
					                                            </div>
					                                            <div class="modal-body">
					                                                Are you sure to delete this data?
					                                            </div>
					                                            <div class="modal-footer">
					                                                <button type="submit" class="btn btn-success">Yes</button>
					                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
					                                            </div>
					                                        </div>
					                                    </div>
					                                </div>
								                </form>
								            </td>
							            @else
							            	<td>
							            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$menu->id}}">Activated</button>

							            		<form action="{{ route('menu.destroy', $menu->id)}}" method="post">
								                  	@csrf
								                  	@method('DELETE')
								                  	<div class="modal fade bs-example-modal{{$menu->id}}" tabindex="-1" role="dialog" aria-hidden="true">
					                                    <div class="modal-dialog modal-sm">
					                                        <div class="modal-content">
					                                            <div class="modal-header btn-success">
					                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
					                                                </button>
					                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
					                                            </div>
					                                            <div class="modal-body">
					                                                Are you sure to active this data?
					                                            </div>
					                                            <div class="modal-footer">
					                                                <button type="submit" class="btn btn-success">Yes</button>
					                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
					                                            </div>
					                                        </div>
					                                    </div>
					                                </div>
								                </form>
							            	</td>
							            @endif
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Name</td>
						          	<td>URL</td>
						          	<td>Icon</td>
						          	<td>Icon Color</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop