<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasedetailprgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchasedetailprgs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchasegeneral')->unsigned();
            $table->integer('product')->unsigned();
            $table->integer('supplierdetail');
            $table->integer('warehouse');
            $table->integer('qty_request');
            $table->integer('qty_remain');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->foreign('purchasegeneral')->references('id')->on('purchasegenerals');
            $table->foreign('product')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchasedetailprgs');
    }
}
