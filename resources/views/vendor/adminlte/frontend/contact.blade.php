@extends('adminlte::frontend/page')

@section('content')
    <section class='section-heading padding-top-100 padding-bottom-0'>
        <div class="angled_down_inside white">
            <div class="slope upleft"></div>
            <div class="slope upright"></div>
        </div>
        
        <div class="container">
            <div class="row">
                <h1 class="heading">Contact Us</h1>
                <div class="headul"></div>
                <p class="subheading">Silahkan hubungi kami dengan cara mengisi form di bawah ini</p>
            </div>
        </div>
        
        <div class="angled_up_inside white">
            <div class="slope upleft"></div>
            <div class="slope upright"></div>
        </div>
    </section>
    
    <section class="parallax contact padding-top-150" id="contact">
        <div class="bg-overlay opacity-85"></div>
        <div class="container">
            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 animated delay1" data-effect="fadeInUp">
                <form method="post" action="{{ route('mailcontact.store') }}">
                    @csrf
                    <input type='text' placeholder='Name' class='col-xs-12 transition' name='name' value="{{ old('name') }}" required>
                    <input type='text' placeholder='Email' class='col-xs-12 transition' name='email' value="{{ old('email') }}" required>
                    <input type='number' placeholder='Phone' class='col-xs-12 transition' name='phone' value="{{ old('phone') }}" required>
                    <input type='text' placeholder='Subject' class='col-xs-12 transition' name='subject' value="{{ old('subject') }}" required>
                    <textarea class='col-xs-12 transition' placeholder='Message' name='message' required>{{ old('message') }}</textarea>
                    <button type='submit' class='btn btn-block btn-primary transition'>Send</button>
                </form>
            </div>
        </div>
    </section>
@stop
