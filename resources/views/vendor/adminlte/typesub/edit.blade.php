@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #6b6e70 !important; font-weight: bold; color: white;">
			    		<h4 class="box-title">Edit Type Sub</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form id="typesubFrm">
				      		@method('PATCH')
				      		@csrf
				          	<div class="form-group">
				              	<label for="type">Type</label>
				              	<select name="type" class="form-control select2" required>
				              		@foreach($types as $type)
				              			@if ($typesub->type == $type->id)
										    <option value="{{ $type->id }}" selected>{{ $type->name }}</option>
										@else
										    <option value="{{ $type->id }}">{{ $type->name }}</option>
										@endif
									@endforeach
								</select>
				          	</div>
				          	<div class="form-group">
				              	<label for="name">Name</label>
				              	<input type="text" class="form-control" name="name" value="{{ $typesub->name }}" required>
				          	</div>
				          	<button id="updateBtn" value="{{ route('typesub.update', $typesub->id) }}" class="btn btn-sm btn-success">Update</button>
				          	<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.typesub.prepare_edit());
    </script>
@stop