<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Exports\PlansExport;
use App\Http\Models\Plan;
use App\Http\Models\Quotationdetail;
use App\Http\Models\Quotation;
use App\Http\Models\Stockin;
use App\Http\Models\Supplierdetail;
use App\Http\Models\Product;
use App\Http\Models\Warehouse;
use App\Http\Models\Purchaserequest;
use App\Http\Models\Company;
use App\Http\Models\Employeeset;
use App\Http\Models\Employeewarehouse;

class PlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $quotationdetails = Quotationdetail::where('status', 1)->orderBy('created_at','desc')->get();

        return view('vendor/adminlte/plan.index', compact('quotationdetails','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $quotationdetail  = Quotationdetail::find($id);
        $supplierdetails  = Supplierdetail::where('status', 1)->get();
        $purchaserequests = Purchaserequest::where([['no_reference', $quotationdetail->fkQuotation->no_sp], ['product', $quotationdetail->product]])->get();
        $product  = Product::where('id', $quotationdetail->product)->first();
        $warehouses  = Warehouse::where('status', 1)->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/plan.create', compact('id','quotationdetail','supplierdetails','purchaserequests','product','warehouses','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $quotationdetail = Quotationdetail::where('id', $id)->first();
        $plans = Plan::where('quotationdetail', $id)->get();
        $stockins = Stockin::where([['qty_available', '<>', 0], ['product', $quotationdetail->product]])->orderBy('created_at')->get();
        $purchaserequests = Purchaserequest::where('quotationdetail', $id)->get();
        $company = Company::first();

        return view('vendor/adminlte/plan.show', compact('id','quotationdetail','plans','stockins','purchaserequests','company','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $quotationdetail = Quotationdetail::where('id', $id)->first();
        $plans = Plan::where('quotationdetail', $id)->get();
        $stockins = Stockin::where([['qty_available', '<>', 0], ['product', $quotationdetail->product]])->orderBy('created_at')->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/plan.edit', compact('id','quotationdetail','plans','stockins','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *'plan',
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $stockin = Stockin::where('id', $request->get('stockin'))->first();
        $quotationdetail = Quotationdetail::where('id', $id)->first();

        $request->validate([
            'qty_plan' => 'numeric|min:0|not_in:0'
        ]);

        if ($request->get('qty_plan') > $stockin->qty_available) {
            return redirect()->back()->withErrors('Qty Plan greater than Qty Available');
        } elseif ($request->get('qty_plan') > $quotationdetail->qty_amount) {
            return redirect()->back()->withErrors('Qty Plan greater than Qty Need');
        } else {
            $cek = Plan::where([['quotationdetail', $request->get('quotationdetail')], ['stockin', $request->get('stockin')]])->first();
            if (empty($cek)) {
                $no   = Plan::select('no_inc')->where('quotationdetail', $id)->max('no_inc');
                if (empty($no)) {
                    $no_inc = 1;
                } else {
                    $no_inc = $no + 1;
                }

                if ($request->get('qty_plan') == ($quotationdetail->qty_kg)) {
                    $no_batch = $quotationdetail->fkQuotation->no_sp."-0";
                } else {
                    $no_batch = $quotationdetail->fkQuotation->no_sp."-".$no_inc;
                }

                $plan = new Plan([
                    'quotationdetail'   => $request->get('quotationdetail'),
                    'stockin'           => $request->get('stockin'),
                    'no_inc'            => $no_inc,
                    'no_batch'          => $no_batch,
                    'qty_plan'          => $request->get('qty_plan'),
                    'created_user'      => $session_id,
                    'updated_user'      => $session_id
                ]);
                $plan->save();
            } else {
                $cek->qty_plan = $cek->qty_plan+$request->get('qty_plan');
                $cek->save();
            }

            $stockin->qty_available = $stockin->qty_available-$request->get('qty_plan');
            $stockin->qty_booked    = $stockin->qty_booked+$request->get('qty_plan');
            $stockin->save();

            $quotationdetail->status_plan   = 1;
            $quotationdetail->qty_plan      = $quotationdetail->qty_plan+$request->get('qty_plan');
            $quotationdetail->qty_amount    = $quotationdetail->qty_amount-$request->get('qty_plan');
            $quotationdetail->save();

            $quotation = Quotation::where('id', $quotationdetail->quotation)->first();
            $quotation->qty_amount = $quotation->qty_amount+1;
            if ($quotation->qty_product == $quotation->qty_amount) {
                $quotation->status_close = 1;
            }
            $quotation->save();

            return redirect('/plan/'.$request->get('quotationdetail').'/edit')->with('success', 'Delivery Plan has been added');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $plan = Plan::find($id);
        $batchs = Plan::where([['quotationdetail', $plan->quotationdetail], ['stockin', $plan->stockin], ['no_inc','>',$plan->no_inc]])->get();
        foreach ($batchs as $batch) {
            $batch->no_inc      = $batch->no_inc-1;
            $batch->no_batch    = $batch->fkQuotationdetail->fkQuotation->no_sp."-".$batch->no_inc;
            $batch->save();
        }
        $plan->delete();

        $stockin = Stockin::where('id', $plan->stockin)->first();
        $stockin->qty_available = $stockin->qty_available+$plan->qty_plan;
        $stockin->qty_booked    = $stockin->qty_booked-$plan->qty_plan;
        $stockin->save();

        $quotationdetail = Quotationdetail::where('id', $plan->quotationdetail)->first();
        $quotationdetail->qty_plan   = $quotationdetail->qty_plan-$plan->qty_plan;
        $quotationdetail->qty_amount = $quotationdetail->qty_amount+$plan->qty_plan;
        $quotationdetail->save();

        return redirect('/plan/'.$plan->quotationdetail.'/edit')->with('error', 'Delivery Plan has been deleted');
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function book($id)
    {
        $purchaserequest = Purchaserequest::find($id);
        $quotationdetail = Quotationdetail::where('id', $purchaserequest->quotationdetail)->first();
        $quotationdetail->qty_book   = $quotationdetail->qty_book-$purchaserequest->qty_request;
        $quotationdetail->save();
        $purchaserequest->delete();

        return redirect('/plan/create/'.$quotationdetail->id)->with('error', 'Booking Order has been deleted');
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function release(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $purchaserequest = Purchaserequest::find($id);
        $quotationdetail = Quotationdetail::where('id', $purchaserequest->quotationdetail)->first();
        $purchaserequest->status_release  = 1;
        $purchaserequest->updated_user    = $session_id;
        $purchaserequest->save();

        return redirect('/plan/create/'.$quotationdetail->id)->with('success', 'Booking Order has been released');
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function other($id)
    {
        $quotationdetail = Quotationdetail::find($id);
        $quotationdetail->status_other  = 1;
        $quotationdetail->qty_other     = $quotationdetail->qty_other+$quotationdetail->qty_amount-$quotationdetail->qty_book;
        $quotationdetail->qty_amount    = $quotationdetail->qty_amount-$quotationdetail->qty_other;
        $quotationdetail->save();

        return redirect('/plan')->with('success', 'Delivery Plan has been completed');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function report(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $plans = Plan::where('status_split', 0)->orderBy('created_at','desc')->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/plan.report', compact('plans','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export() 
    {
        return Excel::download(new PlansExport, 'ReportPlan.xlsx');
    }
}
