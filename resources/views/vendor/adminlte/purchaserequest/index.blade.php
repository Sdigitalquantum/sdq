@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
                <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Purchase Request &nbsp;<a href="{{ route('purchaserequest.create')}}" class="btn btn-xs btn-success" style="text-decoration: none;">New</a></h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="15%">No. PR</th>
						          	<th width="10%">Date</th>
						          	<th width="20%">Product</th>
						          	<th width="15%">Supplier</th>
						          	<th width="15%">Warehouse</th>
						          	<th width="10%">Qty (Kg)</th>
						          	<th width="5%">Approval</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($array = [])
						    	@foreach($employeewarehouses as $employeewarehouse)
						    		@php ($array[] = $employeewarehouse->warehouse)
						    	@endforeach

						    	@php ($no = 0)
						    	@foreach($purchaserequests as $purchaserequest)
						    		@if(in_array($purchaserequest->warehouse, $array) || $purchaserequest->warehouse == 0)
						    			@if(($purchaserequest->status_plan == 1 && $purchaserequest->status_release == 1) || $purchaserequest->status_plan == 0)
								    		@php ($no++)
								        	<tr>
									            <td>{{$no}}</td>
									            <td>{{$purchaserequest->no_pr}}</td>
									            <td>{{$purchaserequest->date_request}}</td>
									            <td>
									            	@if(empty($purchaserequest->fkQuotationdetail->alias_name))
									            		{{$purchaserequest->fkProduct->name}}
								            		@else
								            			{{$purchaserequest->fkQuotationdetail->alias_name}}
								            		@endif
								            	</td>
									            <td>
									            	@if($purchaserequest->supplierdetail == 0)
									            		All Supplier
									            	@else
									            		{{$purchaserequest->fkSupplierdetail->name}}
								            		@endif
									            </td>
									            <td>
									            	@if($purchaserequest->warehouse == 0)
									            		All Warehouse
									            	@else
									            		{{$purchaserequest->fkWarehouse->name}}
								            		@endif
									            </td>
									            <td style="text-align: right;">{{$purchaserequest->qty_request}} &nbsp;</td>
									            <td>
									            	@if($purchaserequest->status == 0)
									            		<font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
									            	@else
										            	@if($purchaserequest->approve == 1 and $purchaserequest->status_approve == 0)
										            		@if($purchaserequest->status_release == 1)
											            		<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal-set{{$purchaserequest->id}}">Set None</button>

												            	<form action="{{ url('purchaserequest/set', $purchaserequest->id)}}" method="post">
												                  	@csrf
												                  	@method('DELETE')
												                  	<div class="modal fade bs-example-modal-set{{$purchaserequest->id}}" tabindex="-1" role="dialog" aria-hidden="true">
									                                    <div class="modal-dialog modal-sm">
									                                        <div class="modal-content">
									                                            <div class="modal-header btn-danger">
									                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
									                                                </button>
									                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
									                                            </div>
									                                            <div class="modal-body">
									                                                Are you sure to set none approval this data?
									                                            </div>
									                                            <div class="modal-footer">
									                                                <button type="submit" class="btn btn-success">Yes</button>
									                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
									                                            </div>
									                                        </div>
									                                    </div>
									                                </div>
												                </form>
										            		@else
										            			<font color="red">None <i class="glyphicon glyphicon-remove"></i></font>
										            		@endif
										                @elseif($purchaserequest->approve == 0 and $purchaserequest->status_approve == 1)
										                	<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal-set{{$purchaserequest->id}}">Set Used</button>

											            	<form action="{{ url('purchaserequest/set', $purchaserequest->id)}}" method="post">
											                  	@csrf
											                  	@method('DELETE')
											                  	<div class="modal fade bs-example-modal-set{{$purchaserequest->id}}" tabindex="-1" role="dialog" aria-hidden="true">
								                                    <div class="modal-dialog modal-sm">
								                                        <div class="modal-content">
								                                            <div class="modal-header btn-success">
								                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
								                                                </button>
								                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
								                                            </div>
								                                            <div class="modal-body">
								                                                Are you sure to set used approval this data?
								                                            </div>
								                                            <div class="modal-footer">
								                                                <button type="submit" class="btn btn-success">Yes</button>
								                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
								                                            </div>
								                                        </div>
								                                    </div>
								                                </div>
											                </form>
										                @else
										                	<font color="blue">Yes <i class="glyphicon glyphicon-ok"></i></font>
										                	@if($purchaserequest->status_plan == 1)
										                		<br><font color="green">Booking Order</font>
										                	@endif
										                @endif
									                @endif
									            </td>

									            @if($purchaserequest->status_approve == 1)
				            						<td>
				            							<a href="{{ route('purchaserequest.show',$purchaserequest->id)}}" class="btn btn-xs btn-primary">Show</a> &nbsp;
				            							<font color="green">Approved <i class="glyphicon glyphicon-ok"></i></font>
				            						</td>
			            						@elseif($purchaserequest->status_approve == 2)
					            					<td>
					            						<a href="{{ route('purchaserequest.show',$purchaserequest->id)}}" class="btn btn-xs btn-primary">Show</a> &nbsp;
					            						<font color="red">Rejected <i class="glyphicon glyphicon-remove"></i></font>
				            						</td>
			            						@else
													@if($purchaserequest->status_plan == 1)
														<td>
											            	<a href="{{ route('purchaserequest.show',$purchaserequest->id)}}" class="btn btn-xs btn-primary">Show</a> &nbsp;
											            	@if($purchaserequest->status_release == 1)
											            		<font color="blue">Waiting Approval</font>
										            		@else
										            			<font color="red">Not Release</font>
										            		@endif
											            </td>
													@else
														@if($purchaserequest->status == 1)
												            <td>
												            	@if($purchaserequest->status_release == 1)
												            		<font color="green">Released <i class="glyphicon glyphicon-ok"></i></font> <br>
												            		<font color="blue">Waiting Approval</font>
												            	@else
												            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal-rl{{$purchaserequest->id}}">Release</button> &nbsp;

													            	<a href="{{ route('purchaserequest.edit',$purchaserequest->id)}}" class="btn btn-xs btn-warning">Edit</a> &nbsp;
													            	
													            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$purchaserequest->id}}">Delete</button>

													            	<form action="{{ url('purchaserequest/release', $purchaserequest->id) }}" method="post">
													                  	@csrf
													                  	@method('DELETE')
													                  	<div class="modal fade bs-example-modal-rl{{$purchaserequest->id}}" tabindex="-1" role="dialog" aria-hidden="true">
										                                    <div class="modal-dialog modal-sm">
										                                        <div class="modal-content">
										                                            <div class="modal-header btn-success">
										                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
										                                                </button>
										                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Release</h4>
										                                            </div>
										                                            <div class="modal-body">
										                                                Are you sure to release this data?
										                                            </div>
										                                            <div class="modal-footer">
										                                                <button type="submit" class="btn btn-success">Yes</button>
										                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
										                                            </div>
										                                        </div>
										                                    </div>
										                                </div>
													                </form>

													            	<form action="{{ route('purchaserequest.destroy', $purchaserequest->id)}}" method="post">
													                  	@csrf
													                  	@method('DELETE')
													                  	<div class="modal fade bs-example-modal{{$purchaserequest->id}}" tabindex="-1" role="dialog" aria-hidden="true">
										                                    <div class="modal-dialog modal-sm">
										                                        <div class="modal-content">
										                                            <div class="modal-header btn-danger">
										                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
										                                                </button>
										                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
										                                            </div>
										                                            <div class="modal-body">
										                                                Are you sure to delete this data?
										                                            </div>
										                                            <div class="modal-footer">
										                                                <button type="submit" class="btn btn-success">Yes</button>
										                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
										                                            </div>
										                                        </div>
										                                    </div>
										                                </div>
													                </form>
												                @endif
												            </td>
											            @else
											            	<td>
											            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$purchaserequest->id}}">Activated</button>

											            		<form action="{{ route('purchaserequest.destroy', $purchaserequest->id)}}" method="post">
												                  	@csrf
												                  	@method('DELETE')
												                  	<div class="modal fade bs-example-modal{{$purchaserequest->id}}" tabindex="-1" role="dialog" aria-hidden="true">
									                                    <div class="modal-dialog modal-sm">
									                                        <div class="modal-content">
									                                            <div class="modal-header btn-success">
									                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
									                                                </button>
									                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
									                                            </div>
									                                            <div class="modal-body">
									                                                Are you sure to active this data?
									                                            </div>
									                                            <div class="modal-footer">
									                                                <button type="submit" class="btn btn-success">Yes</button>
									                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
									                                            </div>
									                                        </div>
									                                    </div>
									                                </div>
												                </form>
											            	</td>
											            @endif
										            @endif
									            @endif
									        </tr>
								        @endif
							        @endif
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>No. PR</td>
						          	<td>Date</td>
						          	<td>Product</td>
						          	<td>Supplier</td>
						          	<td>Warehouse</td>
						          	<td>Qty (Kg)</td>
						          	<td>Approval</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop