@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Document</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
				<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Split Proforma <b>{{ $schedulestuff->fkSchedulebook->fkQuotationsplit->no_split }}</b>
				  			&nbsp;<a href="{{ url('documentcheck') }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a>
				  		</h4>
			  		</div>
				  	
				  	<div class="box-body">
				  		<table class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="15%">Code</th>
						          	<th width="25%">Document</th>
						          	<th width="30%">Note</th>
						          	<th width="20%">Checking</th>
						          	<th width="10%" style="vertical-align: top; text-align: center;">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@foreach($documentchecks as $documentcheck)
						    		<tr>
							            <td>{{$documentcheck->fkDocument->code}}</td>
							            <td>{{$documentcheck->fkDocument->name}}</td>

							            <form method="post" action="{{ route('documentcheck.update', $documentcheck->id) }}">
								      		@method('PATCH')
								      		@csrf
							            	<td>
								            	<input type="text" class="form-control" name="notice" value="{{ $documentcheck->notice }}">
								            </td>
								            <td>
								            	@if($documentcheck->status == 1)
								          			<input type="checkbox" name="status" value="1" checked> Available
								          		@else
								          			<input type="checkbox" name="status" value="1"> Available
								          		@endif
								            </td>
								            <td style="text-align: center;">
								            	<button type="submit" class="btn btn-sm btn-success">Save</button>
								            </td>
							            </form>
									</tr>
					    		@endforeach
						    </tbody>
					    </table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop