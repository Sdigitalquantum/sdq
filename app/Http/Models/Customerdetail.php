<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Customerdetail extends Model
{
    protected $fillable = [
	    'customer',
	    'city',
	    'name',
	    'address',
	    'mobile',
	    'alias_name',
	    'alias_mobile',
	    'created_user',
	    'updated_user'
  	];

  	public function fkCustomer(){
	    return $this->belongsTo('\App\Http\Models\Customer', 'customer', 'id');
	}

	public function fkCity(){
	    return $this->belongsTo('\App\Http\Models\City', 'city', 'id');
	}
}
