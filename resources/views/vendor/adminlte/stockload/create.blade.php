@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_load" data-toggle="tab">Loading</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_load">
				<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Loading Stock &nbsp;<a href="{{ url('stockload') }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4>
			  		</div>
				  	
				  	<div class="box-body">
				  		<div class="row">
							<div class="col-md-4">
								<table class="table table-bordered table-hover nowrap" width="100%">
								    <tr style="background: #001F3F !important; font-weight: bold; text-align: center; color: white;">
							          	<th colspan="2">Split Proforma</th>
							        </tr>
								    <tr>
							            @if($stuff->schedulestuff != 0)
							            	<td colspan="2">{{$stuff->fkSchedulestuff->fkSchedulebook->fkQuotationsplit->no_split}}</td>
						            	@else
						            		<td colspan="2">{{$stuff->fkQuotationsplit->no_split}}</td>
						            	@endif
							        </tr>
							        <tr>
							            <td width="50%">POL : {{$stuff->fkPortl->name}} - {{$stuff->fkPortl->fkCountry->name}}</td>
							            <td width="50%">POD : {{$stuff->fkPortd->name}} - {{$stuff->fkPortd->fkCountry->name}}</td>
							        </tr>
							        <tr>
							            <td>ETD : {{$stuff->etd}}</td>
							            <td>ETA &nbsp;: {{$stuff->eta}}</td>
							        </tr>
							        <tr>
							            <td>Vehicle : {{$stuff->vehicle}}</td>
							            <td>Qty Kg : {{$stuff->qty_kg}}</td>
							        </tr>
							        <tr>
							            <td>Qty Load :
							            	{{number_format($stuff->qty_load, 0, ',' , '.')}}
							            	@if($stuff->qty_kg-$stuff->qty_load == 0)
							            		<i class="glyphicon glyphicon-ok"></i>
							            	@endif
							            </td>
							            <td>Qty Remained :
							            	@if($stuff->qty_kg-$stuff->qty_load == 0)
							            		<font color="green"><b>{{number_format($stuff->qty_kg-$stuff->qty_load, 0, ',' , '.')}}</b></font>
							            	@else
							            		<font color="red"><b>{{number_format($stuff->qty_kg-$stuff->qty_load, 0, ',' , '.')}}</b></font>
							            	@endif
							            </td>
							        </tr>
							  	</table>
							</div>

							<div class="col-md-8">
								<table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #00a65a !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="30%" style="text-align: center;">Product</th>
								          	<th width="25%">Warehouse</th>
								          	<th width="25%">Supplier</th>
								          	<th width="10%" style="text-align: center;">Load (Kg)</th>
								          	<th width="5%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@php ($total = 0)
								    	@foreach($stockloads as $stockload)
								    		<tr>
									            <td>{{$stockload->fkStockin->fkProduct->name}}</td>
									            <td>{{$stockload->fkStockin->fkWarehouse->name}}</td>
									            <td>
									            	{{$stockload->fkStockin->fkSupplierdetail->name}} <br>
									            	Inc. Date : {{$stockload->fkStockin->date_in}}
									            </td>
									            <td style="text-align: right;">{{number_format($stockload->qty_load, 0, ',' , '.')}} &nbsp;</td>
												<td>
													<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$stockload->id}}">Delete</button>

									            	<form action="{{ route('stockload.destroy', $stockload->id)}}" method="post">
									                  	@csrf
									                  	@method('DELETE')
									                  	<div class="modal fade bs-example-modal{{$stockload->id}}" tabindex="-1" role="dialog" aria-hidden="true">
						                                    <div class="modal-dialog modal-sm">
						                                        <div class="modal-content">
						                                            <div class="modal-header btn-danger">
						                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
						                                                </button>
						                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
						                                            </div>
						                                            <div class="modal-body">
						                                                Are you sure to delete this data?
						                                            </div>
						                                            <div class="modal-footer">
						                                                <button type="submit" class="btn btn-success">Yes</button>
						                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
						                                            </div>
						                                        </div>
						                                    </div>
						                                </div>
									                </form>
												</td>
									        </tr>
									        @php ($total = $total + $stockload->qty_load)
								        @endforeach
								        <tr>
								        	<td colspan="3" style="text-align: right; font-weight: bold;">Total Qty</td>
								        	<td style="text-align: right; font-weight: bold;">{{$total}} &nbsp;</td>
								        	<td>&nbsp;</td>
							        	</tr>
								    </tbody>
							  	</table>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<table id="example" class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								        	<th width="5%">No</th>
								          	<th width="15%" style="text-align: center;">No. Batch</th>
								          	<th width="20%">Product</th>
								          	<th width="15%">Warehouse</th>
								          	<th width="20%">Supplier</th>
								          	<th width="10%" style="text-align: center;">Plan (Kg)</th>
								          	<th width="5%" style="text-align: center;">Real (Kg)</th>
								          	<th width="5%" style="text-align: center;">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@php ($array = [])
								    	@foreach($employeewarehouses as $employeewarehouse)
								    		@php ($array[] = $employeewarehouse->warehouse)
								    	@endforeach

								    	@php ($no = 0)
								    	@foreach($plans as $plan)
								    		@if(in_array($plan->fkStockin->warehouse, $array))
									    		@php ($no++)
									    		@if($stuff->schedulestuff != 0)
									    			@if($stuff->fkSchedulestuff->fkSchedulebook->fkQuotationsplit->quotation == $plan->fkQuotationdetail->quotation)
									    				@if($plan->fkStockin->status == 1 && $plan->fkStockin->qty_booked != 0)
												    		<tr>
												    			<td style="text-align: center;">{{$no}}</td>
													            <td style="text-align: center;">{{$plan->no_batch}}</td>
													            <td>{{$plan->fkStockin->fkProduct->name}}</td>
													            <td>{{$plan->fkStockin->fkWarehouse->name}}</td>
													            <td>
													            	{{$plan->fkStockin->fkSupplierdetail->name}} <br>
													            	Inc. Date : {{$plan->fkStockin->date_in}}
													            </td>
													            <td style="text-align: right;">{{number_format($plan->qty_plan, 0, ',' , '.')}} &nbsp;</td>
													            <form method="post" action="{{ route('stockload.store') }}">
														      		@csrf
														      		<input type="hidden" name="stuff" value="{{ $stuff->id }}">
														      		<td>
													      				<input type="hidden" name="stockin" value="{{ $plan->fkStockin->id }}">
														              	<input type="number" class="form-control" name="qty_load" required>
														          	</td>
														          	<td>
														              	<button type="submit" class="btn btn-xs btn-success">Load</button>
														          	</td>
														      	</form>
															</tr>
														@endif
									    			@endif
									    		@else
									    			@if($stuff->fkQuotationsplit->quotation == $plan->fkQuotationdetail->quotation)
									    				@if($plan->fkStockin->status == 1 && $plan->fkStockin->qty_booked != 0)
												    		<tr>
												    			<td style="text-align: center;">{{$no}}</td>
													            <td style="text-align: center;">{{$plan->no_batch}}</td>
													            <td>{{$plan->fkStockin->fkProduct->name}}</td>
													            <td>{{$plan->fkStockin->fkWarehouse->name}}</td>
													            <td>[{{$plan->fkStockin->date_in}}] {{$plan->fkStockin->fkSupplierdetail->name}}</td>
													            <td style="text-align: right;">{{number_format($plan->qty_plan, 0, ',' , '.')}} &nbsp;</td>
													            <form method="post" action="{{ route('stockload.store') }}">
														      		@csrf
														      		<input type="hidden" name="stuff" value="{{ $stuff->id }}">
														      		<td>
														          		<input type="hidden" name="stockin" value="{{ $plan->fkStockin->id }}">
														              	<input type="number" class="form-control" name="qty_load" required>
														          	</td>
														          	<td>
														              	<button type="submit" class="btn btn-xs btn-success">Load</button>
														          	</td>
														      	</form>
															</tr>
														@endif
									    			@endif
									    		@endif
								    		@endif
										@endforeach
								    </tbody>

								    <tfoot>
								        <tr>
								        	<td class="noShow"></td>
								          	<td>No. Batch</td>
								          	<td>Product</td>
								          	<td>Warehouse</td>
								          	<td>Supplier</td>
								          	<td>Plan (Kg)</td>
								          	<td class="noShow"></td>
								          	<td class="noShow"></td>
								        </tr>
								    </tfoot>
							  	</table>
							</div>
						</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop