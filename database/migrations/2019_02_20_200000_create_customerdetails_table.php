<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customerdetails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer')->unsigned();
            $table->integer('city')->unsigned();
            $table->string('name');
            $table->string('address');
            $table->string('mobile');
            $table->string('alias_name')->nullable();
            $table->string('alias_mobile')->nullable();
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->foreign('customer')->references('id')->on('customers');
            $table->foreign('city')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customerdetails');
    }
}
