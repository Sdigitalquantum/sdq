@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Approval Purchase Request</h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="5%">No</th>
			          	<th width="15%">No. PR</th>
			          	<th width="10%">Date</th>
			          	<th width="20%">Detail Request</th>
			          	<th width="15%">Note</th>
			          	<th width="10%">Qty (Kg)</th>
			          	<th width="15%">Reason</th>
			          	<th width="10%">Action</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@php ($array = [])
			    	@foreach($employeewarehouses as $employeewarehouse)
			    		@php ($array[] = $employeewarehouse->warehouse)
			    	@endforeach

			    	@php ($no = 0)
			    	@foreach($purchaserequests as $purchaserequest)
			    		@if(in_array($purchaserequest->warehouse, $array) || $purchaserequest->warehouse == 0)
				    		@php ($no++)
				        	<tr>
					            <td>{{$no}}</td>
					            <td>
					            	{{$purchaserequest->no_pr}}

					            	@if(!empty($purchaserequest->no_reference))
					            		<br><br>No. Reference : <br>{{$purchaserequest->no_reference}}
					            	@endif
					            </td>
					            <td>{{$purchaserequest->date_request}}</td>
					            <td>
					            	@if(empty($purchaserequest->fkQuotationdetail->alias_name))
					            		[{{$purchaserequest->fkProduct->name}}]
				            		@else
				            			[{{$purchaserequest->fkQuotationdetail->alias_name}}]
				            		@endif
				            		<br>
				            		@if($purchaserequest->supplierdetail == 0)
					            		All Supplier
					            	@else
					            		{{$purchaserequest->fkSupplierdetail->name}}
				            		@endif
				            		<br>
				            		@if($purchaserequest->warehouse == 0)
					            		All Warehouse
					            	@else
					            		{{$purchaserequest->fkWarehouse->name}}
				            		@endif
					            </td>
					            <td><pre>{{$purchaserequest->notice}}</pre></td>
					            <td style="text-align: right;">{{$purchaserequest->qty_request}} &nbsp;</td>
					            <td><pre>{{$purchaserequest->reason}}</pre></td>
								
								@if($purchaserequest->status_approve == 0)
									<td>
						            	<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal-approve{{$purchaserequest->id}}">Approve</button> &nbsp;
						                <button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal-reject{{$purchaserequest->id}}">Reject</button>

						            	<form action="{{ url('purchaserequest/approve', $purchaserequest->id)}}" method="post">
						                  	@csrf
						                  	@method('DELETE')
						                  	<div class="modal fade bs-example-modal-approve{{$purchaserequest->id}}" tabindex="-1" role="dialog" aria-hidden="true">
			                                    <div class="modal-dialog modal-sm">
			                                        <div class="modal-content">
			                                            <div class="modal-header btn-success">
			                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
			                                                </button>
			                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Approve</h4>
			                                            </div>
			                                            <div class="modal-body">
			                                            	Are you sure to approve this data?
			                                            	<br><textarea class="form-control" name="reason" maxlength="150" cols="30" rows="3" required></textarea>
			                                            </div>
			                                            <div class="modal-footer">
			                                                <button type="submit" class="btn btn-success">Yes</button>
			                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
						                </form>

						            	<form action="{{ url('purchaserequest/reject', $purchaserequest->id)}}" method="post">
						                  	@csrf
						                  	@method('DELETE')
						                  	<div class="modal fade bs-example-modal-reject{{$purchaserequest->id}}" tabindex="-1" role="dialog" aria-hidden="true">
			                                    <div class="modal-dialog modal-sm">
			                                        <div class="modal-content">
			                                            <div class="modal-header btn-danger">
			                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
			                                                </button>
			                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Reject</h4>
			                                            </div>
			                                            <div class="modal-body">
			                                            	Are you sure to reject this data?
			                                            	<br><textarea class="form-control" name="reason" maxlength="150" cols="30" rows="3" required></textarea>
			                                            </div>
			                                            <div class="modal-footer">
			                                                <button type="submit" class="btn btn-success">Yes</button>
			                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
						                </form>
					            	</td>
				            	@elseif($purchaserequest->status_approve == 1)
				            		<td><font color="green">Approved <i class="glyphicon glyphicon-ok"></i></font></td>
			            		@elseif($purchaserequest->status_approve == 2)
				            		<td><font color="red">Rejected <i class="glyphicon glyphicon-remove"></i></font></td>
								@endif
					        </tr>
				        @endif
			        @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			          	<td class="noShow"></td>
			          	<td>No. PR</td>
			          	<td>Date</td>
			          	<td>Detail Request</td>
			          	<td>No. Reference</td>
			          	<td>Qty (Kg)</td>
			          	<td>Reason</td>
			          	<td class="noShow"></td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	</div>
@stop