<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Branch;
use App\Http\Models\Employeeset;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $branchs = Branch::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/branch.index', compact('branchs','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;
        $branch = new Branch();
        
        return view('vendor/adminlte/branch.create', compact('branch','disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	$json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'name' => 'unique:branches'
                ]);

                $no = Branch::select('no_inc')->max('no_inc');
                if (empty($no)) {
                    $no_inc = 1;
                } else {
                    $no_inc = $no + 1;
                }
                $code = "CAB".sprintf("%05s", $no_inc);

                $branch = new Branch([
                    'no_inc'        => $no_inc,
                    'code'          => $code,
                    'name'          => $json->name,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $branch->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $branch = Branch::find($id);

        return view('vendor/adminlte/branch.edit', compact('branch','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $branch = Branch::find($id);
                $name   = $branch->name;

                if ($name != $json->name) {
                    $request->validate([
                        'name' => 'unique:branches'
                    ]);
                };

                $branch->name           = $json->name;
                $branch->updated_user   = $session_id;
                $result = $branch->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $branch = Branch::find($id);
        $status = $branch->status;

        if ($status == 1) {
        	$branch->status       = 0;
            $branch->updated_user = $session_id;
	        $result = $branch->save();
        } elseif ($status == 0) {
        	$branch->status       = 1;
            $branch->updated_user = $session_id;
	        $result = $branch->save();
        };
        return $this->jsonSuccess( $result );
    }
}
