@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
            <li class="active"><a href="#tab_detail" data-toggle="tab">Account</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_detail">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #6b6e70 !important; font-weight: bold; color: white;">
			    		<h4 class="box-title">Edit Customer Account</h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<div class="row">
				  			<div class="col-md-3">
				  				<form id="customeraccountFrm">
						      		@method('PATCH')
						      		@csrf
						      		<input type="hidden" class="form-control" name="customer" value="{{ $customeraccount->customer }}">
						      		<div class="form-group">
						              	<label for="currency">Currency</label>
						              	<select class="form-control select2" name="currency" required>
						              		@foreach($currencys as $currency)
											    @if ($customeraccount->currency == $currency->id)
												    <option value="{{ $currency->id }}" selected>{{ $currency->code }} - {{ $currency->name }}</option>
												@else
												    <option value="{{ $currency->id }}">{{ $currency->code }} - {{ $currency->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="bank">Bank</label>
						              	<select class="form-control select2" name="bank" required>
						              		@foreach($banks as $bank)
											    @if ($customeraccount->bank == $bank->id)
												    <option value="{{ $bank->id }}" selected>{{ $bank->alias }} - {{ $bank->name }}</option>
												@else
												    <option value="{{ $bank->id }}">{{ $bank->alias }} - {{ $bank->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="account_no">No. Rek</label>
						              	<input type="number" class="form-control" name="account_no" value="{{ $customeraccount->account_no }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="account_swift">Swift Code</label>
						              	<input type="text" class="form-control" name="account_swift" value="{{ $customeraccount->account_swift }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="account_name">Name Rek</label>
						              	<input type="text" class="form-control" name="account_name" value="{{ $customeraccount->account_name }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="alias">Alias</label>
						              	<input type="text" class="form-control" name="alias" value="{{ $customeraccount->alias }}">
						          	</div>
					          		<button id="updateBtn" value="{{ route('customeracocunt.update', $customeraccount->id) }}" class="btn btn-sm btn-success">Update</button>
				          			<button id="cancelBtn" value="{{ url('/customeraccount/'.$customeraccount->customer) }}"class="btn btn-sm btn-danger">Cancel</button>
						      	</form>
			  				</div>

			  				<div class="col-md-9">
			  					<br><table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="20%">Bank</th>
								          	<th width="25%">Account</th>
								          	<th width="20%">Name</th>
								          	<th width="20%">Alias</th>
								          	<th width="15%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($customeraccounts as $customeraccount)
								    		<tr>
									            <td>{{$customeraccount->fkBank->name}}</td>
									            <td>
									            	{{$customeraccount->fkCurrency->code}} : {{$customeraccount->account_no}} <br>
									            	Swift : {{$customeraccount->account_swift}}
									            </td>
									            <td>{{$customeraccount->account_name}}</td>
									            <td>{{$customeraccount->alias}}</td>
												<td>
									            	@if($customeraccount->status == 1) <a href="{{ route('customeraccount.edit',$customeraccount->id)}}" class="btn btn-xs btn-warning">Edit</a>
									            	@else <font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
									            	@endif
									            </td>
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
			  				</div>
		  				</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.customer.prepare_editaccount());
    </script>
@stop