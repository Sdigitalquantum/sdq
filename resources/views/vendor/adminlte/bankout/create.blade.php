@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Detail</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #86c232 !important; font-weight: bold;">
			    		<h4 class="box-title">New Transaction</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action=" ">
				          	@csrf
				          	<div class="row">
    							<div class="col-md-6">
                                    <div class="form-group">
						              	<label for="">No.</label>
						              	<input type="text" class="form-control" name="" value="">
						          	</div>
									<div class="form-group">
						              	<label for="">Tanggal</label>
						              	<input type="date" class="form-control datepicker" name="" value="">
						          	</div>
                                      <div class="form-group">
						              	<label for="">No. Referensi</label>
						              	<input type="text" class="form-control" name="" value="">
						          	</div>
						          	<div class="form-group">
						              	<label for="">Currency</label>
						              	<select class="form-control select2" name="" >
						              		<option value="">Choose One</option>
					                	</select>
						          	</div>
                                      <div class="form-group">
						              	<label for="">Kurs</label>
						              	<input type="number" class="form-control" name="" value="">
						          	</div>
                                </div>
                                <div class="col-md-6">
									<div class="form-group">
						              	<label for="">Kepada</label>
						              	<input type="text" class="form-control" name="" value="">
						          	</div>
                                      <div class="form-group">
						              	<label for="">Alamat</label>
						              	<input type="text" class="form-control" name="" value="">
						          	</div>
                                      <div class="form-group">
						              	<label for="">Keterangan</label>
						              	<input type="text" class="form-control" name="" value="">
						          	</div>
									  <div class="form-group">
						              	<label for="">Bank Code</label>
						              	<select class="form-control select2" name="" >
						              		<option value="">Choose One</option>
					                	</select>
						          	<div class="form-group">
						              	<label for="">Bank Balance</label>
						              	<input type="number" class="form-control" name="" value="" readonly>
						          	</div>
								</div>
							</div>

				          	<a href="{{ url('/bankout/show') }}" class="btn btn-sm btn-success">Save</a>
			          		<a href="{{ url('/bankout') }}" class="btn btn-sm btn-danger">Cancel</a>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop