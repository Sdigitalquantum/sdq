<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Scheduleship extends Model
{
    protected $fillable = [
	    'pol',
	    'pod',
	    'lines',
	    'etd',
	    'eta',
	    'created_user'
  	];
}
