@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_list" data-toggle="tab">Booking</a></li>
          	<li class="disabled"><a>Schedule</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
				<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Booking Schedule &nbsp;<a href="{{ url('schedulebook') }}" class="btn btn-xs btn-primary" style="text-decoration: none;">Shipping Schedule</a></h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="15%">Split Proforma</th>
						          	<th width="15%">Schedule</th>
						          	<th width="10%">Lines</th>
						          	<th width="25%">Note sdq</th>
						          	<th width="20%">Reason EMKL</th>
						          	<th width="10%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($schedulebooks as $schedulebook)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>{{$schedulebook->fkQuotationsplit->no_split}}</td>
							            <td>
							            	POL : {{$schedulebook->pol}} <br>
							            	POD : {{$schedulebook->pod}} <br>
							            	ETD : {{$schedulebook->etd}} <br>
							            	ETA : {{$schedulebook->eta}}
							            </td>
							            <td>{{$schedulebook->lines}}</td>
							            <td><pre>{{$schedulebook->notice}}</pre></td>
							            <td><pre>{{$schedulebook->reason}}</pre></td>
										
										@if($schedulebook->status_approve == 0)
											<td><font color="blue">Waiting Approval</font></td>
						            	@elseif($schedulebook->status_approve == 1)
						            		<td><font color="green">Approved <i class="glyphicon glyphicon-ok"></i></font></td>
					            		@elseif($schedulebook->status_approve == 2)
						            		<td>
						            			@if($schedulebook->status_stuff_acc != 2)
						            				<font color="red">Rejected <i class="glyphicon glyphicon-remove"></i></font> <br>
						            				<a href="{{ route('schedulebook.edit', $schedulebook->id) }}" class="btn btn-xs btn-success">Change Schedule</a>
					            				@else
					            					<font color="red">Stuffing Rejected <i class="glyphicon glyphicon-remove"></i></font>
					            				@endif
						            		</td>
										@endif
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Split Proforma</td>
						          	<td>Schedule</td>
						          	<td>Lines</td>
						          	<td>Note sdq</td>
						          	<td>Reason EMKL</td>
						          	<td>Action</td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop