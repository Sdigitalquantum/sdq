<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Accountgroup;
use App\Http\Models\Employeeset;

class AccountgroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $accountgroups = Accountgroup::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/accountgroup.index', compact('accountgroups','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $accountgroup = new Accountgroup();
        return view('vendor/adminlte/accountgroup.create', compact('accountgroup','disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	$json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'code' => 'unique:accountgroups'
                ]);

                $accountgroup = new Accountgroup([
                    'code'          => $json->code,
                    'name'          => $json->name,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $accountgroup->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $accountgroup = Accountgroup::find($id);

        return view('vendor/adminlte/accountgroup.edit', compact('accountgroup','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $accountgroup = Accountgroup::find($id);
                $code = $accountgroup->code;

                if ($code != $json->code) {
                    $request->validate([
                        'code' => 'unique:accountgroups'
                    ]);
                }
                
                $accountgroup->code             = $json->code;
                $accountgroup->name             = $json->name;
                $accountgroup->updated_user     = $session_id;
                $result = $accountgroup->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $accountgroup = Accountgroup::find($id);
        $status = $accountgroup->status;

        if ($status == 1) {
        	$accountgroup->status       = 0;
            $accountgroup->updated_user = $session_id;
	        $result = $accountgroup->save();

        } elseif ($status == 0) {
        	$accountgroup->status       = 1;
            $accountgroup->updated_user = $session_id;
	        $result = $accountgroup->save();

        };
        return $this->jsonSuccess( $result );
    }
}
