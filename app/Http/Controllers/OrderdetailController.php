<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Orderdetail;
use App\Http\Models\Product;
use App\Http\Models\Employeeset;

class OrderdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $orderdetails = Orderdetail::where('order', $id)->get();
        $products = Product::where('status', 1)->get();

        return view('vendor/adminlte/frontend.orderdetail', compact('id','orderdetails','products','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cek = Orderdetail::where([['order', $request->get('order')], ['product', $request->get('product')]])->first();

        $request->validate([
            'qty_order' => 'numeric|min:0|not_in:0'
        ]);

        if (!empty($cek)) {
            return redirect()->back()->withErrors('Product already exist');
        } else {
            $orderdetail = new Orderdetail([
                'order'         => $request->get('order'),
                'product'       => $request->get('product'),
                'qty_order'     => $request->get('qty_order')
            ]);
            $orderdetail->save();

            return redirect('/orderdetail/create/'.$request->get('order'))->with('success', 'Product has been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $orderdetail = Orderdetail::find($id);
        $orderdetails = Orderdetail::where('order', $orderdetail->order)->get();
        $products = Product::where('status', 1)->get();

        return view('vendor/adminlte/frontend.orderedit', compact('orderdetail','orderdetails','products','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $orderdetail = Orderdetail::find($id);
        $orderdetail->product   = $request->get('product');
        $orderdetail->qty_order = $request->get('qty_order');
        $orderdetail->save();

        return redirect('/orderdetail/create/'.$orderdetail->order)->with('success', 'Product has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
