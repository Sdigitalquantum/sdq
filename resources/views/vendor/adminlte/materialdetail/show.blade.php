@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="active"><a href="#tab_raw" data-toggle="tab">Raw Material</a></li>
            <li class="disabled"><a>Excess</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_raw">
            	<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<!-- <h4 class="box-title">Raw Material &nbsp;<a href="{{ url('material') }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4> -->
				  		<h4 class="box-title">Raw Material &nbsp;
				  			<button id="completeBtn" class="btn btn-xs btn-success" value="{{ route('material.edit', $id)}}">Complete</button>
				  		</h4>
			  		</div>
				  	
				  	<div class="box-body">
				  		<div class="row">
				  			<div class="col-md-3">
				  				<form method="post" action="{{ route('materialdetail.store') }}">
						          	@csrf
						          	<input type="hidden" class="form-control" name="material" value="{{ $id }}">
						          	<div class="form-group">
						              	<label for="product">Product</label>
						              	<select class="form-control select2" name="product" required>
						              		<option value="{{ old('product') }}">Choose One</option>
					                    	@foreach($products as $product)
											    <option value="{{ $product->id }}">{{ $product->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="unit">Unit</label>
						              	<select class="form-control select2" name="unit" required>
						              		<option value="{{ old('unit') }}">Choose One</option>
					                    	@foreach($units as $unit)
											    <option value="{{ $unit->id }}">{{ $unit->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty">Qty</label>
						              	<input type="number" class="form-control" name="qty" value="{{ old('qty') }}" required>
						          	</div>
						          	<button type="submit" class="btn btn-sm btn-success">Save</button>
						      	</form>
			  				</div>

			  				<div class="col-md-9">
			  					<br><table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="30%">Product</th>
								          	<th width="30%">Unit</th>
								          	<th width="20%">Qty</th>
								          	<th width="20%">Status</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($materialdetails as $materialdetail)
								    		<tr>
									            <td>{{$materialdetail->fkProduct->name}}</td>
									            <td>{{$materialdetail->fkUnit->name}}</td>
									            <td>{{$materialdetail->qty}}</td>

									            @if($materialdetail->status == 1)
										            <td>
										            	<a href="{{ route('materialdetail.edit',$materialdetail->id)}}" class="btn btn-xs btn-warning">Edit</a> &nbsp;
										            	
										            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$materialdetail->id}}">Delete</button>

										            	<form action="{{ route('materialdetail.destroy', $materialdetail->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$materialdetail->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-danger">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to delete this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
										            </td>
									            @else
									            	<td>
									            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$materialdetail->id}}">Activated</button>

									            		<form action="{{ route('materialdetail.destroy', $materialdetail->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$materialdetail->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-success">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to active this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
									            	</td>
									            @endif
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
			  				</div>
		  				</div>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop

@section('content_js')
	<script>
    	$(document).ready(sdq.material.prepare_showdetail());
    </script>
@stop