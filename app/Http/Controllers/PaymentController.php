<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Payment;
use App\Http\Models\Employeeset;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $payments = Payment::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/payment.index', compact('payments','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;
        $payments = new Payment();
        
        return view('vendor/adminlte/payment.create', compact('payments', 'disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $payment = new Payment([
                    'value'         => $json->value,
                    'condition'     => $json->condition,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $payment->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $payment = Payment::find($id);

        return view('vendor/adminlte/payment.edit', compact('payment','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $payment = Payment::find($id);
                $payment->value         = $json->value;
                $payment->condition     = $json->condition;
                $payment->updated_user  = $session_id;
                $result = $payment->save();
            };
            return $this->jsonSuccess( $result );
        };
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $payment = Payment::find($id);
        $status = $payment->status;

        if ($status == 1) {
        	$payment->status       = 0;
            $payment->updated_user = $session_id;
	        $result = $payment->save();

		    return redirect('/payment')->with('error', 'Payment has been deleted');
        } elseif ($status == 0) {
        	$payment->status       = 1;
            $payment->updated_user = $session_id;
	        $result = $payment->save();

		    return $this->jsonSuccess( $result );
        }
    }
}
