@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
            <li class="disabled"><a>Account</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #6b6e70 !important; font-weight: bold; color: white;">
			    		<h4 class="box-title">New Customer</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form id="customerFrm">
			              	@method('POST')
				          	@csrf
				          	<div class="row">
    							<div class="col-md-6">
    								<div class="form-group">
						              	<label for="code">Code</label>
						              	<input type="text" class="form-control" name="code" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="account">Accounting</label>
						              	<select class="form-control select2" name="account" required>
						              		<option value="{{ old('account') }}">Choose One</option>
					                    	@foreach($accounts as $account)
											    <option value="{{ $account->id }}">{{ $account->code }} - {{ $account->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="email">Email</label>
						              	<input type="text" class="form-control" name="email" value="{{ old('email') }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="fax">Fax</label>
						              	<input type="number" class="form-control" name="fax" value="{{ old('fax') }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="check">&nbsp;</label>
						              	<input type="checkbox" name="tax" value="1"> Tax &nbsp;&nbsp;&nbsp;
						              	<input type="checkbox" name="status_group" value="1"> Status Group &nbsp;&nbsp;&nbsp;
						              	<input type="checkbox" name="status_record" value="1"> Status Record
						          	</div>
									<div class="form-group">
						          		<label for="npwp_no">No. NPWP</label>
						              	<input type="number" class="form-control" name="npwp_no" value="{{ old('npwp_no') }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="npwp_name">Name NPWP</label>
						              	<input type="text" class="form-control" name="npwp_name" value="{{ old('npwp_name') }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="npwp_address">Address NPWP</label>
						              	<input type="text" class="form-control" name="npwp_address" value="{{ old('npwp_address') }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="npwp_city">City NPWP</label>
						              	<select class="form-control select2" name="npwp_city">
						              		<option value="{{ old('npwp_city') }}">Choose One</option>
					                    	@foreach($citys as $city)
											    <option value="{{ $city->id }}">{{ $city->name }} - {{ $city->fkCountry->name }}</option>
											@endforeach
					                	</select>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						          		<label for="limit">Limit</label>
						              	<input type="number" class="form-control" name="limit" value="{{ old('limit') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="note1">Note 1</label>
						          		<textarea class="form-control" name="note1" rows="4"></textarea>
						          	</div>
						          	<div class="form-group">
						          		<label for="note2">Note 2</label>
						          		<textarea class="form-control" name="note2" rows="4"></textarea>
						          	</div>
						          	<div class="form-group">
						          		<label for="note3">Note 3</label>
						          		<textarea class="form-control" name="note3" rows="4"></textarea>
						          	</div>
						          	<div class="form-group">
						          		<label for="notice">Notice</label>
						          		<textarea class="form-control" name="notice" rows="3"></textarea>
						          	</div>
						          	<div class="form-group">
						          		<button id="saveBtn" value="{{ route('customer.store') }}" class="btn btn-sm btn-success">Save</button>
				          				<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
						          	</div>
								</div>
							</div>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.customer.prepare_create());
    </script>
@stop