@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Monitoring Finance - Receivable</h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="5%">No</th>
			          	<th width="20%">Customer</th>
			          	<th width="20%">No. SP</th>
			          	<th width="10%">Date SP</th>
			          	<th width="15%">Total</th>
			          	<th width="15%">Payment</th>
			          	<th width="15%">Saldo</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@php ($no = 0)
			    	@foreach($receivables as $receivable)
			    		@php ($no++)
			        	<tr>
				            <td>{{$no}}</td>
				            <td>{{$receivable->name_customer}}</td>
				            <td>{{$receivable->no_sp}}</td>
				            <td>{{$receivable->date_sp}}</td>
				            <td style="text-align: right;">{{number_format($receivable->total, 0, ',' , '.')}} &nbsp;</td>
				            <td style="text-align: right;">{{number_format($receivable->payment, 0, ',' , '.')}} &nbsp;</td>
				            <td style="text-align: right;">{{number_format($receivable->total-$receivable->payment, 0, ',' , '.')}} &nbsp;</td>
				        </tr>
			        @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			          	<td class="noShow"></td>
			          	<td>customer</td>
			          	<td>No. SP</td>
			          	<td>Date SP</td>
			          	<td>Total</td>
			          	<td>Payment</td>
			          	<td>Saldo</td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	<div>
@stop