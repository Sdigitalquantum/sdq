<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Purchasedetailprg;
use App\Http\Models\Purchasegeneral;
use App\Http\Models\Product;
use App\Http\Models\Supplierdetail;
use App\Http\Models\Warehouse;
use App\Http\Models\Employeeset;
use App\Http\Models\Employeewarehouse;

class PurchasedetailprgController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $purchasegeneral = Purchasegeneral::find($id);
        $purchasedetailprgs = Purchasedetailprg::where('purchasegeneral', $id)->get();
        $products = Product::where([['status', 1], ['general', 1]])->get();
        $supplierdetails = Supplierdetail::where('status', 1)->get();
        $warehouses = Warehouse::where('status', 1)->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/purchasedetailprg.create', compact('purchasegeneral','purchasedetailprgs','products','supplierdetails','warehouses','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $cek = Purchasedetailprg::where([['purchasegeneral', $request->get('purchasegeneral')], ['product', $request->get('product')], ['supplierdetail', $request->get('supplierdetail')], ['warehouse', $request->get('warehouse')]])->first();
        
        $request->validate([
            'qty_request' => 'numeric|min:0|not_in:0'
        ]);

        if (!empty($cek)) {
            return redirect()->back()->withErrors('Detail Purchase General already exist');
        } else {
            $purchasedetailprg = new Purchasedetailprg([
                'purchasegeneral'   => $request->get('purchasegeneral'),
                'product'           => $request->get('product'),
                'supplierdetail'    => $request->get('supplierdetail'),
                'warehouse'         => $request->get('warehouse'),
                'qty_request'       => $request->get('qty_request'),
                'qty_remain'        => $request->get('qty_request'),
                'created_user'      => $session_id,
                'updated_user'      => $session_id
            ]);
            $purchasedetailprg->save();

            return redirect('/purchasedetailprg/create/'.$request->get('purchasegeneral'))->with('success', 'Detail Purchase General has been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $purchasedetailprg = Purchasedetailprg::find($id);
        $purchasedetailprgs = Purchasedetailprg::where('purchasegeneral', $purchasedetailprg->purchasegeneral)->get();
        $products = Product::where([['status', 1], ['general', 1]])->get();
        $supplierdetails = Supplierdetail::where('status', 1)->get();
        $warehouses = Warehouse::where('status', 1)->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/purchasedetailprg.edit', compact('purchasedetailprg','purchasedetailprgs','products','supplierdetails','warehouses','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $purchasedetailprg = Purchasedetailprg::find($id);
        $cek = Purchasedetailprg::where([['purchasegeneral', $request->get('purchasegeneral')], ['product', $request->get('product')], ['supplierdetail', $request->get('supplierdetail')], ['warehouse', $request->get('warehouse')]])->first();
        
        $request->validate([
            'qty_request' => 'numeric|min:0|not_in:0'
        ]);

        if ($purchasedetailprg->product == $request->get('product')) {
            $purchasedetailprg->product           = $request->get('product');
            $purchasedetailprg->supplierdetail    = $request->get('supplierdetail');
            $purchasedetailprg->warehouse         = $request->get('warehouse');
            $purchasedetailprg->qty_remain        = $purchasedetailprg->qty_remain-$purchasedetailprg->qty_request+$request->get('qty_request');
            $purchasedetailprg->qty_request       = $request->get('qty_request');
            $purchasedetailprg->updated_user      = $session_id;
            $purchasedetailprg->save();

            return redirect('/purchasedetailprg/create/'.$purchasedetailprg->purchasegeneral)->with('success', 'Detail Purchase General has been updated');
        } else {
            if (!empty($cek)) {
                return redirect()->back()->withErrors('Detail Purchase General already exist');
            } else {
                $purchasedetailprg->product           = $request->get('product');
                $purchasedetailprg->supplierdetail    = $request->get('supplierdetail');
                $purchasedetailprg->warehouse         = $request->get('warehouse');
                $purchasedetailprg->qty_remain        = $purchasedetailprg->qty_remain-$purchasedetailprg->qty_request+$request->get('qty_request');
                $purchasedetailprg->qty_request       = $request->get('qty_request');
                $purchasedetailprg->updated_user      = $session_id;
                $purchasedetailprg->save();

                return redirect('/purchasedetailprg/create/'.$purchasedetailprg->purchasegeneral)->with('success', 'Detail Purchase General has been updated');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $purchasedetailprg = Purchasedetailprg::find($id);
        $status = $purchasedetailprg->status;

        if ($status == 1) {
            $purchasedetailprg->status       = 0;
            $purchasedetailprg->updated_user = $session_id;
            $purchasedetailprg->save();

            return redirect('/purchasedetailprg/create/'.$purchasedetailprg->purchasegeneral)->with('error', 'Detail Purchase General has been deleted');
        } elseif ($status == 0) {
            $purchasedetailprg->status       = 1;
            $purchasedetailprg->updated_user = $session_id;
            $purchasedetailprg->save();

            return redirect('/purchasedetailprg/create/'.$purchasedetailprg->purchasegeneral)->with('success', 'Detail Purchase General has been activated');
        }
    }
}
