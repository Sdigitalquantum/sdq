<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotationdetails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quotation')->unsigned();
            $table->integer('product')->unsigned();
            $table->integer('unit')->unsigned();
            $table->integer('qty_fcl');
            $table->integer('qty_bag');
            $table->integer('qty_kg');
            $table->integer('qty_pcs');
            $table->string('price');
            $table->string('kurs')->nullable();
            $table->tinyInteger('disc_type');
            $table->string('disc_value')->nullable();
            $table->string('alias_name')->nullable();
            $table->string('alias_qty')->nullable();
            $table->integer('qty_amount');
            $table->tinyInteger('status_plan')->default('0');
            $table->integer('qty_plan')->default('0');
            $table->tinyInteger('status_book')->default('0');
            $table->integer('qty_book')->default('0');
            $table->tinyInteger('status_other')->default('0');
            $table->integer('qty_other')->default('0');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->foreign('quotation')->references('id')->on('quotations');
            $table->foreign('product')->references('id')->on('products');
            $table->foreign('unit')->references('id')->on('units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotationdetails');
    }
}
