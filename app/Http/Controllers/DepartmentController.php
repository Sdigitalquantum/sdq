<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Department;
use App\Http\Models\Employeeset;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $departments = Department::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/department.index', compact('departments','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $department = new Department();

        return view('vendor/adminlte/department.create', compact('department','disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'name' => 'unique:departments'
                ]);

                $no = Department::select('no_inc')->max('no_inc');
                if (empty($no)) {
                    $no_inc = 1;
                } else {
                    $no_inc = $no + 1;
                }
                $code = "DEP".sprintf("%05s", $no_inc);

                $department = new Department([
                    'no_inc'        => $no_inc,
                    'code'          => $code,
                    'name'          => $json->name,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $department->save();
            };
        };
        return $this->jsonSuccess( $result );
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $department = Department::find($id);

        return view('vendor/adminlte/department.edit', compact('department','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $department = Department::find($id);
                $name   = $department->name;

                if ($name != $request->get('name')) {
                    $request->validate([
                        'name' => 'unique:departments'
                    ]);
                }

                $department->name           = $json->name;
                $department->updated_user   = $session_id;
                $result = $department->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $department = Department::find($id);
        $status = $department->status;

        if ($status == 1) {
            $department->status       = 0;
            $department->updated_user = $session_id;
            $result = $department->save();

        } elseif ($status == 0) {
            $department->status       = 1;
            $department->updated_user = $session_id;
            $result = $department->save();

        };
        return $this->jsonSuccess( $result );
    }
}
