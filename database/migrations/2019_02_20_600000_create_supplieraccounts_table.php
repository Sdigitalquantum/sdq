<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplieraccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplieraccounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('supplier')->unsigned();
            $table->integer('currency')->unsigned();
            $table->integer('bank')->unsigned();
            $table->bigInteger('account_no');
            $table->string('account_swift');
            $table->string('account_name');
            $table->string('alias')->nullable();
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->foreign('supplier')->references('id')->on('suppliers');
            $table->foreign('currency')->references('id')->on('currencies');
            $table->foreign('bank')->references('id')->on('banks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplieraccounts');
    }
}
