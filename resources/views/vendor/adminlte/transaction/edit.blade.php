@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Transaction</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	 <form id="transactionFrm">  <!-- method="post" action="{{ route('transaction.update', $transaction->id) }}"> -->
				      		@method('PATCH')
				      		@csrf
				      		<div class="form-group">
				              	<label for="department">Department</label>
				              	<select class="form-control select2" name="department" required>
				              		@foreach($departments as $department)
									    @if ($transaction->department == $department->id)
										    <option value="{{ $department->id }}" selected>{{ $department->code }} - {{ $department->name }}</option>
										@else
										    <option value="{{ $department->id }}">{{ $department->code }} - {{ $department->name }}</option>
										@endif
									@endforeach
			                	</select>
				          	</div>
				          	<div class="form-group">
				              	<label for="account">Accounting</label>
				              	<select class="form-control select2" name="account" required>
				              		@foreach($accounts as $account)
									    @if ($transaction->accounting == $account->id)
										    <option value="{{ $account->id }}" selected>{{ $account->code }} [{{ $account->name }}]</option>
										@else
										    <option value="{{ $account->id }}">{{ $account->code }} [{{ $account->name }}]</option>
										@endif
									@endforeach
			                	</select>
				          	</div>
				      		<div class="form-group">
				              	<label for="code">Code</label>
				              	<input type="text" class="form-control" name="code" value="{{ $transaction->code }}" required>
				          	</div>
				          	<div class="form-group">
				              	<label for="name">Name</label>
				              	<input type="text" class="form-control" name="name" value="{{ $transaction->name }}" required>
				          	</div>
				          	<div class="form-group">
				          		<label for="check">&nbsp;</label>
				          		@if($transaction->status_group == 1)
				          			<input type="checkbox" name="status_group" value="1" checked> Status Group &nbsp;&nbsp;&nbsp;
				          		@else
				          			<input type="checkbox" name="status_group" value="1"> Status Group &nbsp;&nbsp;&nbsp;
				          		@endif

				          		@if($transaction->status_record == 1)
				          			<input type="checkbox" name="status_record" value="1" checked> Status Record
				          		@else
				          			<input type="checkbox" name="status_record" value="1"> Status Record
				          		@endif
			          		</div>									
				          	<!-- <div class="form-group">	
								<button type="submit" class="btn btn-sm btn-success">Update</button>
				          		<a href="{{ url('/transaction') }}" class="btn btn-sm btn-danger">Cancel</a> -->
								  <button id="updateBtn" value="{{ route('transaction.update', $transaction->id) }}" class="btn btn-sm btn-success">Update</button>
				          			<button id="cancelBtn" value="{{ url('/transaction/'.$transaction->transaction) }}"class="btn btn-sm btn-danger">Cancel</button>									  
			          		</div>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.transaction.prepare_edit());
    </script>
@stop
