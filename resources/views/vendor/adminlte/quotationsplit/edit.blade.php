@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_split" data-toggle="tab">Split</a></li>
            <li class="disabled"><a>Plan</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_split">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Split Proforma &nbsp;<a href="{{ route('quotationplan.edit', $quotationsplit->id) }}" class="btn btn-xs btn-primary" style="text-decoration: none;">Plan</a></h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('quotationsplit.update', $quotationsplit->id) }}">
				      		@method('PATCH')
				      		@csrf
				          	<div class="row">
    							<div class="col-md-4">
    								<div class="form-group">
						              	<label for="no_split">No. Split</label>
						              	<input type="text" class="form-control" name="no_split" value="{{ $quotationsplit->no_split }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="pol">Port Loading</label>
						              	<select class="form-control select2" name="pol" required>
						              		<option value="{{ old('pol') }}">Choose One</option>
					                    	@foreach($ports as $port)
											    @if ($quotationsplit->pol == $port->id)
												    <option value="{{ $port->id }}" selected>{{ $port->name }} - {{ $port->fkCountry->name }}</option>
												@else
												    <option value="{{ $port->id }}">{{ $port->name }} - {{ $port->fkCountry->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="pod">Port Discharge</label>
						              	<select class="form-control select2" name="pod" required>
						              		<option value="{{ old('pod') }}">Choose One</option>
					                    	@foreach($ports as $port)
											    @if ($quotationsplit->pod == $port->id)
												    <option value="{{ $port->id }}" selected>{{ $port->name }} - {{ $port->fkCountry->name }}</option>
												@else
												    <option value="{{ $port->id }}">{{ $port->name }} - {{ $port->fkCountry->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="etd">Time Departure</label>
						              	<input type="text" class="form-control datepicker" name="etd" value="{{ $quotationsplit->etd }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="eta">Time Arrival</label>
						              	<input type="text" class="form-control datepicker" name="eta" value="{{ $quotationsplit->eta }}" required>
						          	</div>
						          	<div class="form-group">
						          		<button type="submit" class="btn btn-sm btn-success">Update</button>
						          		<a href="{{ url('/quotationsplit/create',$quotationsplit->quotation) }}" class="btn btn-sm btn-danger">Cancel</a>
						          	</div>
								</div>
						</form>

								<div class="col-md-8">
						          	<table class="table table-bordered table-hover nowrap" width="100%">
						          		@foreach($quotationsplits as $quotationsplit)
										    <thead style="background: #111111 !important; font-weight: bold; text-align: center; color: white;">
										        <tr>
										          	<th width="30%" style="text-align: center;">No. Split</th>
										          	<th width="30%">Port</th>
										          	<th width="20%">Date</th>
										          	<th width="15%" style="text-align: center;">Action</th>
										        </tr>
										    </thead>

										    <tbody>
									    		<tr>
										            <td style="text-align: center;">{{$quotationsplit->no_split}}</td>
										            <td>
										            	POL : {{$quotationsplit->fkPortl->name}} - {{$quotationsplit->fkPortl->fkCountry->name}} <br>
										            	POD : {{$quotationsplit->fkPortd->name}} - {{$quotationsplit->fkPortd->fkCountry->name}}
										            </td>
										            <td>
										            	ETD : {{$quotationsplit->etd}} <br>
										            	ETA : {{$quotationsplit->eta}}
										            </td>

										            @if($quotationsplit->status == 1)
											            <td style="text-align: center;">
											            	@if($quotationsplit->status_release == 1)
											            		<font color="blue">Released <i class="glyphicon glyphicon-ok"></i></font>
										            		@else
											            		<a href="{{ route('quotationsplit.edit',$quotationsplit->id)}}" class="btn btn-xs btn-warning">Edit</a>
										            		@endif
											            </td>
										            @elseif($quotationsplit->status == 0)
										            	<td style="text-align: center;">
										            		<font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
										            	</td>
									            	@else
									            		<td>
									            			<font color="red">Re-Route <i class="glyphicon glyphicon-remove"></i></font>
									            		</td>
										            @endif
												</tr>
												<tr>
													<td colspan="4">
														<table class="table table-bordered table-hover nowrap" width="100%">
														    <thead style="background: #605ca8 !important; font-weight: bold; text-align: center; color: white;">
														        <tr>
														          	<th width="20%" style="text-align: center;">No. Batch</th>
														          	<th width="30%">Warehouse</th>
														          	<th width="30%">Supplier</th>
														          	<th width="15%" style="text-align: center;">Qty (Kg)</th>
														        </tr>
														    </thead>

														    <tbody>
														    	@foreach($quotationplans as $quotationplan)
														    		@if($quotationplan->quotationsplit == $quotationsplit->id)
															    		<tr>
																            <td style="text-align: center;">{{$quotationplan->fkPlan->no_batch}}</td>
																            <td>
																            	{{$quotationplan->fkPlan->fkStockin->fkWarehouse->name}} <br>
																            	{{$quotationplan->fkPlan->fkStockin->fkProduct->name}}
																            </td>
																            <td>
																            	{{$quotationplan->fkPlan->fkStockin->fkSupplierdetail->name}} <br>
																            	Incoming Date : {{$quotationplan->fkPlan->fkStockin->date_in}}
																            </td>
																            <td style="text-align: right;">{{number_format($quotationplan->fkPlan->qty_plan, 0, ',' , '.')}} &nbsp;</td>
																		</tr>
																	@endif
														        @endforeach
														    </tbody>
													  	</table>
													</td>
												</tr>
										    </tbody>
									    @endforeach
								  	</table>
								</div>
							</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop