@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Weighing - Non Shipping</h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="5%">No</th>
			          	<th width="20%">Split Proforma</th>
			          	<th width="15%">POL</th>
			          	<th width="15%">POD</th>
			          	<th width="15%">ETD</th>
			          	<th width="15%">ETA</th>
			          	<th width="15%">Container / Seal</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@php ($no = 0)
			    	@foreach($schedulestuffs as $schedulestuff)
			    		@php ($no++)
			        	<tr>
				            <td>{{$no}}</td>
				            <td>{{$schedulestuff->no_split}}</td>
				            <td>{{$schedulestuff->fkPortl->name}} - {{$schedulestuff->fkPortl->fkCountry->name}}</td>
				            <td>{{$schedulestuff->fkPortd->name}} - {{$schedulestuff->fkPortd->fkCountry->name}}</td>
				            <td>{{$schedulestuff->etd}}</td>
				            <td>{{$schedulestuff->eta}}</td>
							<td>
								@foreach($stuffs as $stuff)
									@if($stuff->quotationsplit == $schedulestuff->id)
										<ul>
											<li>
												{{$stuff->container}} / {{$stuff->seal}} <br>
												Bruto &nbsp;&nbsp;: {{$stuff->container_bruto}} <br>
												Empty : {{$stuff->container_empty}} <br>
												Netto &nbsp;&nbsp;: {{$stuff->container_netto}}
											</li>
										</ul>
									@endif
								@endforeach
							</td>
				        </tr>
			        @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			          	<td class="noShow"></td>
			          	<td>Split Proforma</td>
			          	<td>POL</td>
			          	<td>POD</td>
			          	<td>ETD</td>
			          	<td>ETA</td>
			          	<td>Container / Seal</td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	</div>
@stop