@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>Menu</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
                <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Group Access</h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="45%">Name</th>
						          	<th width="40%">Modules</th>
						          	<th width="10%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($groups as $group)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>{{$group->name}}</td>
							            <td>
							            	@foreach($groupsets as $groupset)
							            		@if($groupset->group == $group->id)
							            			<i class="fa fa-fw fa-{{$groupset->fkMenuroot->icon}} text-{{$groupset->fkMenuroot->icon_color}}"></i> {{$groupset->fkMenuroot->name}} <br>
							            		@endif
							            	@endforeach
							            </td>

							            <td>
							            	<a href="{{ route('groupset.edit',$group->id)}}" class="btn btn-xs btn-success">Access Menu</a> &nbsp;
							            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$group->id}}">Synchronize</button>

							            	<form action="{{ url('groupset/sync', $group->id)}}" method="post">
							                  	@method('PATCH')
				      							@csrf
							                  	<div class="modal fade bs-example-modal{{$group->id}}" tabindex="-1" role="dialog" aria-hidden="true">
				                                    <div class="modal-dialog modal-sm">
				                                        <div class="modal-content">
				                                            <div class="modal-header btn-danger">
				                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
				                                                </button>
				                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Synchronization</h4>
				                                            </div>
				                                            <div class="modal-body">
				                                                Are you sure to synchronize this data?
				                                            </div>
				                                            <div class="modal-footer">
				                                                <button type="submit" class="btn btn-success">Yes</button>
				                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
				                                            </div>
				                                        </div>
				                                    </div>
				                                </div>
							                </form>
							            </td>
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Name</td>
						          	<td>Modules</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop