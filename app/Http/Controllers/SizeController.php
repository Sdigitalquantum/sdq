<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Size;
use App\Http\Models\Employeeset;

class SizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $sizes = Size::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/size.index', compact('sizes','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $size = new Size();

        return view('vendor/adminlte/size.create', compact('size','disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	$json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'name' => 'unique:sizes'
                ]);

                $size = new Size([
                    'name'          => $json->name,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $size->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $size = Size::find($id);

        return view('vendor/adminlte/size.edit', compact('size','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $size = Size::find($id);
                $name = $size->name;

                if ($name != $json->name) {
                    $request->validate([
                        'name' => 'unique:sizes'
                    ]);
                }
                
                $size->name             = $json->name;
                $size->updated_user     = $session_id;
                $result = $size->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $size = Size::find($id);
        $status = $size->status;

        if ($status == 1) {
        	$size->status       = 0;
            $size->updated_user = $session_id;
	        $result = $size->save();

        } elseif ($status == 0) {
        	$size->status       = 1;
            $size->updated_user = $session_id;
	        $result = $size->save();

        };
        return $this->jsonSuccess( $result );
    }
}
