<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Materialdetail;
use App\Http\Models\Product;
use App\Http\Models\Unit;
use App\Http\Models\Employeeset;

class MaterialdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            $sales = Sales::find($id);
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $cek = Materialdetail::where([['material', $json->material], ['product', $json->product]])->first();

                if (!empty($cek)) { $result = false; } 
                else {
                    $materialdetail = new Materialdetail([
                        'material'          => $json->material,
                        'product'           => $json->product,
                        'unit'              => $json->unit,
                        'qty'               => $json->qty,
                        'created_user'      => $session_id,
                        'updated_user'      => $session_id
                    ]);
                    $result = $materialdetail->save();
                }
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $materialdetails = Materialdetail::where('material', $id)->orderBy('created_at','desc')->get();
        $products = Product::where('status', 1)->get();
        $units = Unit::where('status', 1)->get();

        return view('vendor/adminlte/materialdetail.show', compact('id','materialdetails','products','units','disable',));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $materialdetail = Materialdetail::find($id);
        $materialdetails = Materialdetail::where('material', $materialdetail->material)->orderBy('created_at','desc')->get();
        $products = Product::where('status', 1)->get();
        $units = Unit::where('status', 1)->get();

        return view('vendor/adminlte/materialdetail.edit', compact('materialdetail','materialdetails','products','units','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            $materialdetail = Materialdetail::find($id);
            if ($materialdetail->product != $json->product) {
                $cek = Materialdetail::where([
                    ['material', $json->material], 
                    ['product', $json->product]]
                )->first();

                if (!empty($cek)) { $result = false; } 
                else {
                    $materialdetail->material         = $json->material;
                    $materialdetail->product          = $json->product;
                    $materialdetail->unit             = $json->unit;
                    $materialdetail->qty              = $json->qty;
                    $materialdetail->updated_user     = $session_id;
                    $result = $materialdetail->save();

                }
            } else {
                $materialdetail->material         = $json->material;
                $materialdetail->product          = $json->product;
                $materialdetail->unit             = $json->unit;
                $materialdetail->qty              = $json->qty;
                $materialdetail->updated_user     = $session_id;
                $result = $materialdetail->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $materialdetail = Materialdetail::find($id);
        $status = $materialdetail->status;

        if ($status == 1) {
        	$materialdetail->status       = 0;
            $materialdetail->updated_user = $session_id;
	        $result = $materialdetail->save();

        } elseif ($status == 0) {
        	$materialdetail->status       = 1;
            $materialdetail->updated_user = $session_id;
	        $result = $materialdetail->save();
        };
        return $this->jsonSuccess( $result );
    }
}
