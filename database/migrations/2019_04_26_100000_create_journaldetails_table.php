<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournaldetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journaldetails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('journal')->unsigned();
            $table->integer('accounting')->unsigned();
            $table->tinyInteger('debit');
            $table->tinyInteger('status_group');
            $table->tinyInteger('status_record');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->foreign('journal')->references('id')->on('journals');
            $table->foreign('accounting')->references('id')->on('accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journaldetails');
    }
}
