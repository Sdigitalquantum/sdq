<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = [
	    'name',
	    'created_user',
	    'updated_user'
  	];
}
