@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #6b6e70 !important; font-weight: bold; color: white;">
			    		<h4 class="box-title">Edit City</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form id="cityFrm" > <!-- method="post" action="{{ route('city.update', $city->id) }}" -->
				      		@method('PATCH')
				      		@csrf
				          	<div class="form-group">
				              	<label for="code">Code</label>
				              	<input type="text" class="form-control" name="code" value="{{ $city->code }}" disabled>
				          	</div>
				          	<div class="form-group">
				              	<label for="name">Name</label>
				              	<input type="text" class="form-control" name="name" value="{{ $city->name }}" required>
				          	</div>
				          	<div class="form-group">
				              	<label for="country">Country</label>
				              	<select name="country" class="form-control select2" required>
				              		@foreach($countrys as $country)
				              			@if ($city->country == $country->id)
										    <option value="{{ $country->id }}" selected>{{ $country->name }}</option>
										@else
										    <option value="{{ $country->id }}">{{ $country->name }}</option>
										@endif
									@endforeach
								</select>
				          	</div>
				          	<!-- <button onclick="sdq.core.submitForm('cityFrm', '{{ route("city.update", $city->id) }}', 'city' );return false;" class="btn btn-sm btn-success">Update</button>
				          	<a href="#" onclick="sdq.core.load('city');return false;" class="btn btn-sm btn-danger">Cancel</a> -->
				          	<button id="updateBtn" value="{{ route('city.update', $city->id) }}" class="btn btn-sm btn-success">Update</button>

				          	<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.city.prepare_edit());
    </script>
@stop