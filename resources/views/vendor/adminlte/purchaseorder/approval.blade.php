@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Approval Purchase Order</h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="5%">No</th>
			          	<th width="15%">No. PO</th>
			          	<th width="10%">Date</th>
			          	<th width="15%">Note</th>
			          	<th width="35%">Detail Request</th>
			          	<th width="15%">Reason</th>
			          	<th width="5%">Action</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@php ($no = 0)
			    	@foreach($purchaseorders as $purchaseorder)
			    		@php ($no++)
			        	<tr>
				            <td>{{$no}}</td>
				            <td>{{$purchaseorder->no_po}}</td>
				            <td>{{$purchaseorder->date_order}}</td>
				            <td><pre>{{$purchaseorder->notice}}</pre></td>
				            <td>
				            	<ul>
					            	@foreach($purchasedetails as $purchasedetail)
					            		@if($purchasedetail->purchaseorder == $purchaseorder->id)
					            			<li>
					            				[{{ $purchasedetail->fkProduct->name }}] {{ $purchasedetail->fkSupplierdetail->name }} <br>
					            				[Qty Kg : {{ number_format($purchasedetail->qty_order+$purchasedetail->qty_buffer, 0, ',' , '.') }}, Price : Rp. {{ number_format($purchasedetail->price, 0, ',' , '.') }}] Total : Rp. {{ number_format(($purchasedetail->qty_order+$purchasedetail->qty_buffer)*$purchasedetail->price, 0, ',' , '.') }}
					            			</li>
				            				<ol>
					            				@foreach($purchasedetailprs as $purchasedetailpr)
								            		@if($purchasedetailpr->purchasedetail == $purchasedetail->id)
								            			@if($purchasedetailpr->purchaserequest != 0)
								            				<li>No. PR : {{ $purchasedetailpr->fkPurchaserequest->no_pr }}</li>
							            				@else
							            					<li>No. PG : {{ $purchasedetailpr->fkPurchasedetailprg->fkPurchasegeneral->no_prg }}</li>
							            				@endif
								            		@endif
								            	@endforeach
							            	</ol>
					            		@endif
					            	@endforeach
				            	</ul>
				            </td>
				            <td><pre>{{$purchaseorder->reason}}</pre></td>
							
							@if($purchaseorder->status_approve == 0)
								<td>
					            	<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal-approve{{$purchaseorder->id}}">Approve</button> &nbsp;
					                <button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal-reject{{$purchaseorder->id}}">Reject</button>

					            	<form action="{{ url('purchaseorder/approve', $purchaseorder->id)}}" method="post">
					                  	@csrf
					                  	@method('DELETE')
					                  	<div class="modal fade bs-example-modal-approve{{$purchaseorder->id}}" tabindex="-1" role="dialog" aria-hidden="true">
		                                    <div class="modal-dialog modal-sm">
		                                        <div class="modal-content">
		                                            <div class="modal-header btn-success">
		                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
		                                                </button>
		                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Approve</h4>
		                                            </div>
		                                            <div class="modal-body">
		                                            	Are you sure to approve this data?
		                                            	<br><textarea class="form-control" name="reason" maxlength="150" cols="30" rows="3" required></textarea>
		                                            </div>
		                                            <div class="modal-footer">
		                                                <button type="submit" class="btn btn-success">Yes</button>
		                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
					                </form>

					            	<form action="{{ url('purchaseorder/reject', $purchaseorder->id)}}" method="post">
					                  	@csrf
					                  	@method('DELETE')
					                  	<div class="modal fade bs-example-modal-reject{{$purchaseorder->id}}" tabindex="-1" role="dialog" aria-hidden="true">
		                                    <div class="modal-dialog modal-sm">
		                                        <div class="modal-content">
		                                            <div class="modal-header btn-danger">
		                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
		                                                </button>
		                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Reject</h4>
		                                            </div>
		                                            <div class="modal-body">
		                                            	Are you sure to reject this data?
		                                            	<br><textarea class="form-control" name="reason" maxlength="150" cols="30" rows="3" required></textarea>
		                                            </div>
		                                            <div class="modal-footer">
		                                                <button type="submit" class="btn btn-success">Yes</button>
		                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
					                </form>
				            	</td>
			            	@elseif($purchaseorder->status_approve == 1)
			            		<td><font color="green">Approved <i class="glyphicon glyphicon-ok"></i></font></td>
		            		@elseif($purchaseorder->status_approve == 2)
			            		<td><font color="red">Rejected <i class="glyphicon glyphicon-remove"></i></font></td>
							@endif
				        </tr>
			        @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			          	<td class="noShow"></td>
			          	<td>No. PO</td>
			          	<td>Date</td>
			          	<td>Note</td>
			          	<td>Detail Request</td>
			          	<td>Reason</td>
			          	<td class="noShow"></td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	</div>
@stop