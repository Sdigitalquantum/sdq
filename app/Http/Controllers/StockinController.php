<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Stockin;
use App\Http\Models\Stockqc;
use App\Http\Models\Stockcard;
use App\Http\Models\Transaction;
use App\Http\Models\Product;
use App\Http\Models\Warehouse;
use App\Http\Models\Supplierdetail;
use App\Http\Models\Production;
use App\Http\Models\Journal;
use App\Http\Models\Journaldetail;
use App\Http\Models\Journalset;
use App\Http\Models\Debt;
use App\Http\Models\Debtcard;
use App\Http\Models\Letter;
use App\Http\Models\Employee;
use App\Http\Models\Employeeset;
use App\Http\Models\Employeewarehouse;

class StockinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $stockins = Stockin::orderBy('created_at','desc')->get();
        $journals = Journal::where('status', 1)->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/stockin.index', compact('stockins','journals','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $transactions = Transaction::where('status', 1)->get();
        $products = Product::where('status', 1)->get();
        $warehouses = Warehouse::where('status', 1)->get();
        $supplierdetails = Supplierdetail::where('status', 1)->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/stockin.create', compact('transactions','products','warehouses','supplierdetails','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        $request->validate([
            'qty_bag' => 'numeric|min:0|not_in:0',
            'qty_pcs' => 'numeric|min:0|not_in:0',
            'qty_kg'  => 'numeric|min:0|not_in:0'
        ]);

        $no = Stockin::select('no_inc')->whereYear('created_at', date('Y'))->max('no_inc');
        if (empty($no)) {
            $no_inc = 1;
        } else {
            $no_inc = $no + 1;
        }

        $letter = Letter::where('menu', 'Incoming Stock')->first();
        if (empty($letter->company)) {
            $company = "";
        } else {
            $company = $letter->company."/";
        }

        if (empty($letter->code)) {
            $code = "";
        } else {
            $code = $letter->code;
        }

        if ($letter->romawi == 0) {
            $romawi = "";
        } else {
            if (date("m") == 1) {
                $romawi = "I/";
            } elseif (date("m") == 2) {
                $romawi = "II/";
            } elseif (date("m") == 3) {
                $romawi = "III/";
            } elseif (date("m") == 4) {
                $romawi = "IV/";
            } elseif (date("m") == 5) {
                $romawi = "V/";
            } elseif (date("m") == 6) {
                $romawi = "VI/";
            } elseif (date("m") == 7) {
                $romawi = "VII/";
            } elseif (date("m") == 8) {
                $romawi = "VIII/";
            } elseif (date("m") == 9) {
                $romawi = "IX/";
            } elseif (date("m") == 10) {
                $romawi = "X/";
            } elseif (date("m") == 11) {
                $romawi = "XI/";
            } elseif (date("m") == 12) {
                $romawi = "XII/";
            }
        }

        if ($letter->year == 0) {
            $year = "";
        } else {
            $year = date("Y");
        }

        $nomor = $company.$code.sprintf("%05s", $no_inc)."/".$romawi.$year;
    	
        $stockin = new Stockin([
            'transaction'       => $request->get('transaction'),
            'product'           => $request->get('product'),
            'warehouse'         => $request->get('warehouse'),
            'supplierdetail'    => $request->get('supplierdetail'),
            'no_inc'            => $no_inc,
            'nomor'             => $nomor,
            'date_in'           => $request->get('date_in'),
            'price'             => 0,
	        'qty_bag' 		    => $request->get('qty_bag'),
            'qty_pcs'           => $request->get('qty_pcs'),
            'qty_kg'            => $request->get('qty_kg'),
            'qty_available'     => $request->get('qty_kg'),
            'noref_in'          => 'PRODUCTION',
            'transaction_prod'  => 0,
            'status_move'       => 0,
            'status_validate'   => 1,
            'created_user'      => $session_id,
            'updated_user'      => $session_id
	    ]);
	    $stockin->save();

	    return redirect('/stockin')->with('success', 'Incoming Stock has been added');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $stockins = Stockin::where('status', 1)->orderBy('date_in','desc')->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/stockin.show', compact('stockins','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function product(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $stocks = Stockcard::select('product','year')->groupBy('product','year')->get();
        $stockcards = Stockcard::select('product','year','month','qty','qty_out')->groupBy('product','year','month','qty','qty_out')->get();

        return view('vendor/adminlte/stockin.product', compact('stocks','stockcards','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function validation(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $stockin = Stockin::find($id);

        if (!empty($request->get('qty_bag'))) {
            $request->validate([
                'qty_bag' => 'numeric|min:0|not_in:0'
            ]);
        } elseif (!empty($request->get('qty_pcs'))) {
            $request->validate([
                'qty_pcs' => 'numeric|min:0|not_in:0'
            ]);
        } elseif (!empty($request->get('qty_kg'))) {
            $request->validate([
                'qty_kg'  => 'numeric|min:0|not_in:0'
            ]);
        }

        if (!empty($request->get('date_in'))) {
            if ($request->get('date_in') < $request->get('date_qc')) {
                return redirect()->back()->withErrors('Date lower than QC Date');
            }
        }

        if ($request->get('qty_kg') > $stockin->qty_available) {
            return redirect()->back()->withErrors('Qty Kg greater than Qty Available');
        } else {
            $stockin->date_in           = !empty($request->get('date_in')) ? $request->get('date_in') : date('Y-m-d');
            $stockin->qty_bag           = !empty($request->get('qty_bag')) ? $request->get('qty_bag') : $stockin->qty_bag;
            $stockin->qty_pcs           = !empty($request->get('qty_pcs')) ? $request->get('qty_pcs') : $stockin->qty_pcs;
            $stockin->qty_kg            = !empty($request->get('qty_kg')) ? $request->get('qty_kg') : $stockin->qty_kg;
            $stockin->qty_available     = !empty($request->get('qty_kg')) ? $request->get('qty_kg') : $stockin->qty_available;
            $stockin->status_validate   = 1;
            $stockin->qty_validate      = !empty($request->get('qty_kg')) ? $request->get('qty_kg') : $stockin->qty_kg;
            $stockin->validated_at      = new \DateTime();
            $stockin->validated_user    = $session_id;
            $stockin->save();

            if (!empty($stockin->transaction_prod)) {
                $production = Production::where('id', $stockin->transaction_prod)->first();
                $production->status = 0;
                $production->save();
            }

            if ($stockin->status_move == 0) {
                if (empty($request->get('date_in'))) {
                    $date = $stockin->date_in;
                } else {
                    $date = $request->get('date_in');
                }

                if (empty($request->get('qty_kg'))) {
                    $qty = $stockin->qty_kg;
                } else {
                    $qty = $request->get('qty_kg');
                }

                $card = Stockcard::where([['product', $stockin->product], ['year', date('Y', strtotime($date))], ['month', date('m', strtotime($date))]])->first();
                if (empty($card)) {
                    $stockcard = new Stockcard([
                        'product'       => $stockin->product,
                        'year'          => date('Y', strtotime($date)),
                        'month'         => date('m', strtotime($date)),
                        'qty'           => $qty,
                        'price'         => $stockin->price
                    ]);
                    $stockcard->save();
                } else {
                    $card->qty = $card->qty+$qty;
                    if ($stockin->price != 0) {
                        $card->price = ($card->price+$stockin->price)/2;
                    }
                    $card->save();
                }

                if ($stockin->transaction_prod == 0) {
                    $journaldetails = Journaldetail::where([['journal', $request->get('journal')], ['status', 1]])->get();
                    foreach ($journaldetails as $journaldetail) {
                        $journalset = new Journalset([
                            'code_journal'      => $journaldetail->fkJournal->code,
                            'name_journal'      => $journaldetail->fkJournal->name,
                            'code_accounting'   => $journaldetail->fkAccount->code,
                            'name_accounting'   => $journaldetail->fkAccount->name,
                            'no_is'             => $stockin->nomor,
                            'no_qc'             => $stockin->noref_in,
                            'price'             => $qty*$stockin->price,
                            'status'            => ($journaldetail->debit == 1) ? 'D' : 'K',
                            'created_user'      => $session_id
                        ]);
                        $journalset->save();
                    }

                    $stockqc = Stockqc::where('no_po', $stockin->noref_in)->first();
                    if (!empty($stockqc)) {
                        $date_po = $stockqc->fkPurchasedetail->fkPurchaseorder->date_order;
                        $date_qc = $stockqc->date_in;
                    } else {
                        $date_po = $date;
                        $date_qc = $date;
                    }

                    $debt = new Debt([
                        'no_po'             => $stockin->noref_in,
                        'code_supplier'     => $stockin->fkSupplierdetail->fkSupplier->code,
                        'name_supplier'     => $stockin->fkSupplierdetail->name,
                        'code_product'      => $stockin->fkProduct->code,
                        'name_product'      => $stockin->fkProduct->name,
                        'date_po'           => $date_po,
                        'date_qc'           => $date_qc,
                        'qty'               => $qty,
                        'price'             => $stockin->price,
                        'total'             => $qty*$stockin->price,
                        'payment'           => 0,
                        'adjustment'        => 0,
                        'created_user'      => $session_id,
                        'updated_user'      => $session_id
                    ]);
                    $debt->save();

                    $saldocard = Debtcard::where([['supplierdetail', $stockin->supplierdetail], ['year', date('Y', strtotime($date))], ['month', date('m', strtotime($date))]])->first();
                    if (empty($saldocard)) {
                        $debtcard = new Debtcard([
                            'supplierdetail'    => $stockin->supplierdetail,
                            'year'              => date('Y', strtotime($date)),
                            'month'             => date('m', strtotime($date)),
                            'debit'             => $qty*$stockin->price,
                            'kredit'            => 0
                        ]);
                        $debtcard->save();
                    } else {
                        $saldocard->debit = $saldocard->debit+($qty*$stockin->price);
                        $saldocard->save();
                    }
                }
            }

            return redirect('/stockin')->with('success', 'Stock has been validated');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $stockins = Stockin::where([['qty_available','<>',0], ['status', 1], ['status_validate', 1]])->orderBy('created_at','desc')->get();
        $warehouses = Warehouse::where('status', 1)->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/stockin.list', compact('stockins','warehouses','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $stockin = Stockin::find($id);

        $request->validate([
            'qty_bag' => 'numeric|min:0|not_in:0',
            'qty_pcs' => 'numeric|min:0|not_in:0',
            'qty_kg'  => 'numeric|min:0|not_in:0'
        ]);

        if ($request->get('qty_kg') > $stockin->qty_available) {
            return redirect()->back()->withErrors('Qty Kg greater than Qty Available');
        } else {
            $stockin->qty_bag           = $stockin->qty_bag-$request->get('qty_bag');
            $stockin->qty_pcs           = $stockin->qty_pcs-$request->get('qty_pcs');
            $stockin->qty_kg            = $stockin->qty_kg-$request->get('qty_kg');
            $stockin->qty_available     = $stockin->qty_available-$request->get('qty_kg');
            $stockin->status_move       = 1;
            $stockin->qty_move          = $stockin->qty_move+$request->get('qty_kg');
            $stockin->date_move         = $request->get('date_in');
            $stockin->updated_user      = $session_id;
            $stockin->save();

            if (date('m', strtotime($stockin->date_in)) != date('m', strtotime($request->get('date_in')))) {
                $stockcard = Stockcard::where([['product', $stockin->product], ['year', date('Y', strtotime($stockin->date_in))], ['month', date('m', strtotime($stockin->date_in))]])->first();
                $stockcard->qty_out = $stockcard->qty_out+$request->get('qty_kg');
                $stockcard->save();

                $card = Stockcard::where([['product', $stockin->product], ['year', date('Y', strtotime($request->get('date_in')))], ['month', date('m', strtotime($request->get('date_in')))]])->first();
                if (empty($card)) {
                    $stocknew = new Stockcard([
                        'product'       => $stockin->product,
                        'year'          => date('Y', strtotime($request->get('date_in'))),
                        'month'         => date('m', strtotime($request->get('date_in'))),
                        'qty'           => $request->get('qty_kg'),
                        'price'         => $stockin->price
                    ]);
                    $stocknew->save();
                } else {
                    $card->qty = $card->qty+$request->get('qty_kg');
                    if ($stockin->price != 0) {
                        $card->price = ($card->price+$stockin->price)/2;
                    }
                    $card->save();
                }
            }

            $no = Stockin::select('no_inc')->whereYear('created_at', date('Y'))->max('no_inc');
            if (empty($no)) {
                $no_inc = 1;
            } else {
                $no_inc = $no + 1;
            }

            $letter = Letter::where('menu', 'Incoming Stock')->first();
            if (empty($letter->company)) {
                $company = "";
            } else {
                $company = $letter->company."/";
            }

            if (empty($letter->code)) {
                $code = "";
            } else {
                $code = $letter->code;
            }

            if ($letter->romawi == 0) {
                $romawi = "";
            } else {
                if (date("m") == 1) {
                    $romawi = "I/";
                } elseif (date("m") == 2) {
                    $romawi = "II/";
                } elseif (date("m") == 3) {
                    $romawi = "III/";
                } elseif (date("m") == 4) {
                    $romawi = "IV/";
                } elseif (date("m") == 5) {
                    $romawi = "V/";
                } elseif (date("m") == 6) {
                    $romawi = "VI/";
                } elseif (date("m") == 7) {
                    $romawi = "VII/";
                } elseif (date("m") == 8) {
                    $romawi = "VIII/";
                } elseif (date("m") == 9) {
                    $romawi = "IX/";
                } elseif (date("m") == 10) {
                    $romawi = "X/";
                } elseif (date("m") == 11) {
                    $romawi = "XI/";
                } elseif (date("m") == 12) {
                    $romawi = "XII/";
                }
            }

            if ($letter->year == 0) {
                $year = "";
            } else {
                $year = date("Y");
            }

            $nomor = $company.$code.sprintf("%05s", $no_inc)."/".$romawi.$year;

            $stockmove = new Stockin([
                'transaction'       => $stockin->transaction,
                'product'           => $stockin->product,
                'warehouse'         => $request->get('warehouse'),
                'supplierdetail'    => $stockin->supplierdetail,
                'no_inc'            => $no_inc,
                'nomor'             => $nomor,
                'date_in'           => $request->get('date_in'),
                'price'             => $stockin->price,
                'qty_bag'           => $request->get('qty_bag'),
                'qty_pcs'           => $request->get('qty_pcs'),
                'qty_kg'            => $request->get('qty_kg'),
                'qty_available'     => $request->get('qty_kg'),
                'noref_in'          => $stockin->noref_in,
                'transaction_prod'  => 0,
                'status_move'       => 1,
                'created_user'      => $session_id,
                'updated_user'      => $session_id
            ]);
            $stockmove->save();

            return redirect('/stockmove')->with('success', 'Stock has been moved');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function detail(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $stockins = Stockin::where([['qty_available','<>',0], ['status', 1], ['status_validate', 1]])->orderBy('created_at','desc')->get();
        $transactions = Transaction::where('status', 1)->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/stockin.detail', compact('stockins','transactions','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function set(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $stockin = Stockin::find($id);

        $letter = Letter::where('menu', 'Outgoing Stock')->first();
        if (empty($letter->company)) {
            $company = "";
        } else {
            $company = $letter->company."/";
        }

        if (empty($letter->code)) {
            $code = "";
        } else {
            $code = $letter->code;
        }

        if ($letter->romawi == 0) {
            $romawi = "";
        } else {
            if (date("m") == 1) {
                $romawi = "I/";
            } elseif (date("m") == 2) {
                $romawi = "II/";
            } elseif (date("m") == 3) {
                $romawi = "III/";
            } elseif (date("m") == 4) {
                $romawi = "IV/";
            } elseif (date("m") == 5) {
                $romawi = "V/";
            } elseif (date("m") == 6) {
                $romawi = "VI/";
            } elseif (date("m") == 7) {
                $romawi = "VII/";
            } elseif (date("m") == 8) {
                $romawi = "VIII/";
            } elseif (date("m") == 9) {
                $romawi = "IX/";
            } elseif (date("m") == 10) {
                $romawi = "X/";
            } elseif (date("m") == 11) {
                $romawi = "XI/";
            } elseif (date("m") == 12) {
                $romawi = "XII/";
            }
        }

        if ($letter->year == 0) {
            $year = "";
        } else {
            $year = date("Y");
        }

        $nomor = $company.$code.sprintf("%05s", $stockin->no_inc)."/".$romawi.$year;

        $request->validate([
            'qty_bag' => 'numeric|min:0|not_in:0',
            'qty_pcs' => 'numeric|min:0|not_in:0',
            'qty_kg'  => 'numeric|min:0|not_in:0'
        ]);

        if ($request->get('qty_kg') > $stockin->qty_available) {
            return redirect()->back()->withErrors('Qty Kg greater than Qty Available');
        } else {
            $transaction = Transaction::find($request->get('transaction'));

            $stockin->transaction_out   = $request->get('transaction');
            $stockin->qty_bag           = $stockin->qty_bag-$request->get('qty_bag');
            $stockin->qty_pcs           = $stockin->qty_pcs-$request->get('qty_pcs');
            $stockin->qty_available     = $stockin->qty_available-$request->get('qty_kg');
            $stockin->status_out        = 1;
            $stockin->qty_out           = $stockin->qty_out+$request->get('qty_kg');
            $stockin->date_out          = $request->get('date_in');
            $stockin->noref_out         = $nomor;
            $stockin->updated_user      = $session_id;
            $stockin->save();

            $stockcard = Stockcard::where([['product', $stockin->product], ['year', date('Y', strtotime($stockin->date_in))], ['month', date('m', strtotime($stockin->date_in))]])->first();
            $stockcard->qty_out = $stockcard->qty_out+$request->get('qty_kg');
            $stockcard->save();

            return redirect('/stockout')->with('success', 'Stock has been outed');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function card(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $products = Product::where('status', 1)->get();
        $employee = Employee::find($session_id);

        return view('vendor/adminlte/stockin.card', compact('products','employee','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $products = Product::where('status', 1)->get();
        $employee = Employee::find($session_id);

        $request->validate([
            'year'  => 'numeric|min:0|not_in:0'
        ]);

        $date_begin = $request->get('year').'-'.$request->get('month').'-01';
        if ($request->get('month') == 2) {
            $date_end = $request->get('year').'-'.$request->get('month').'-28';
        } elseif ($request->get('month') == 1 || $request->get('month') == 3 || $request->get('month') == 5 || $request->get('month') == 7 || $request->get('month') == 8 || $request->get('month') == 10 || $request->get('month') == 12) {
            $date_end = $request->get('year').'-'.$request->get('month').'-31';
        } elseif ($request->get('month') == 4 || $request->get('month') == 6 || $request->get('month') == 9 || $request->get('month') == 11) {
            $date_end = $request->get('year').'-'.$request->get('month').'-30';
        }
        
        $stock = Product::find($request->get('product')); 
        $stockins = Stockin::where([['product', $request->get('product')], ['date_in','>=',$date_begin], ['date_in','<=',$date_end], ['status_out', 0]])->orderBy('date_in','asc')->get();
        $stockouts = Stockin::where([['product', $request->get('product')], ['date_out','>=',$date_begin], ['date_out','<=',$date_end], ['status_out', 1]])->orderBy('date_out','asc')->get();
        $stockold = Stockcard::where([['product', $request->get('product')], ['year', $request->get('year')], ['month', $request->get('month')-1]])->first();
        $stockcard = Stockcard::where([['product', $request->get('product')], ['year', $request->get('year')], ['month', $request->get('month')]])->first();
        
        $year = $request->get('year');
        $month = $request->get('month');
        $price = $request->get('price');

        return view('vendor/adminlte/stockin.filter', compact('products','employee','stock','stockins','stockouts','stockold','stockcard','year','month','price','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function opname(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $stockins = Stockin::where([['qty_available','<>',0], ['status', 1], ['status_validate', 1]])->orderBy('created_at','desc')->get();
        $journals = Journal::where('status', 1)->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/stockin.opname', compact('stockins','journals','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function adjust(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $stockin = Stockin::find($id);
            
        $request->validate([
            'qty_kg'  => 'numeric|min:0|not_in:0'
        ]);

        if ($request->get('qty_kg') == $stockin->qty_available) {
            $stockin->status_opname     = 1;
            $stockin->qty_opname        = $request->get('qty_kg');
            $stockin->date_opname       = $request->get('date_in');
            $stockin->updated_user      = $session_id;
            $stockin->save();

            return redirect('/stockopname')->with('success', 'Stock card has been adjusted');
        } elseif ($request->get('qty_kg') < $stockin->qty_available) {
            $journaldetails = Journaldetail::where([['journal', $request->get('journal')], ['status', 1]])->get();
            foreach ($journaldetails as $journaldetail) {
                $journalset = new Journalset([
                    'code_journal'      => $journaldetail->fkJournal->code,
                    'name_journal'      => $journaldetail->fkJournal->name,
                    'code_accounting'   => $journaldetail->fkAccount->code,
                    'name_accounting'   => $journaldetail->fkAccount->name,
                    'no_is'             => $stockin->nomor,
                    'no_qc'             => $stockin->noref_in,
                    'price'             => ($stockin->qty_available-$request->get('qty_kg'))*$stockin->price,
                    'status'            => ($journaldetail->debit == 1) ? 'D' : 'K',
                    'created_user'      => $session_id
                ]);
                $journalset->save();
            }

            $stockin->qty_out           = $stockin->qty_out+($stockin->qty_available-$request->get('qty_kg'));
            $stockin->qty_available     = $request->get('qty_kg');
            $stockin->status_opname     = 1;
            $stockin->qty_opname        = $request->get('qty_kg');
            $stockin->date_opname       = $request->get('date_in');
            $stockin->updated_user      = $session_id;
            $stockin->save();

            $stockcard = Stockcard::where([['product', $stockin->product], ['year', date('Y', strtotime($stockin->date_in))], ['month', date('m', strtotime($stockin->date_in))]])->first();
            $stockcard->qty = $stockcard->qty-($stockin->qty_available-$request->get('qty_kg'));
            $stockcard->save();

            return redirect('/stockopname')->with('success', 'Stock card has been adjusted');
        } elseif ($request->get('qty_kg') > $stockin->qty_available) {
            $journaldetails = Journaldetail::where([['journal', $request->get('journal')], ['status', 1]])->get();
            foreach ($journaldetails as $journaldetail) {
                $journalset = new Journalset([
                    'code_journal'      => $journaldetail->fkJournal->code,
                    'name_journal'      => $journaldetail->fkJournal->name,
                    'code_accounting'   => $journaldetail->fkAccount->code,
                    'name_accounting'   => $journaldetail->fkAccount->name,
                    'no_is'             => $stockin->nomor,
                    'no_qc'             => $stockin->noref_in,
                    'price'             => ($request->get('qty_kg')-$stockin->qty_available)*$stockin->price,
                    'status'            => ($journaldetail->debit == 1) ? 'D' : 'K',
                    'created_user'      => $session_id
                ]);
                $journalset->save();
            }
            
            $stockin->qty_kg            = $stockin->qty_kg+($request->get('qty_kg')-$stockin->qty_available);
            $stockin->qty_available     = $request->get('qty_kg');
            $stockin->status_opname     = 1;
            $stockin->qty_opname        = $request->get('qty_kg');
            $stockin->date_opname       = $request->get('date_in');
            $stockin->updated_user      = $session_id;
            $stockin->save();

            $stockcard = Stockcard::where([['product', $stockin->product], ['year', date('Y', strtotime($stockin->date_in))], ['month', date('m', strtotime($stockin->date_in))]])->first();
            $stockcard->qty = $stockcard->qty+($request->get('qty_kg')-$stockin->qty_available);
            $stockcard->save();

            return redirect('/stockopname')->with('success', 'Stock card has been adjusted');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function price(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $stockins = Stockin::where([['price', 0], ['qty_available','<>',0], ['status', 1], ['status_validate', 0]])->orderBy('created_at','desc')->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/stockin.price', compact('stockins','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function income(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $stockin = Stockin::find($id);
            
        $request->validate([
            'price'  => 'numeric|min:0|not_in:0'
        ]);

        $stockcard = Stockcard::where([['product', $stockin->product], ['year', date('Y', strtotime($stockin->date_in))], ['month', date('m', strtotime($stockin->date_in))]])->first();
        if (!empty($stockcard)) {
            $stockcard->price = ($stockcard->price+$request->get('price'))/2;
            $stockcard->save();
        } else {
            $stocknew = new Stockcard([
                'product'       => $stockin->product,
                'year'          => date('Y', strtotime($stockin->date_in)),
                'month'         => date('m', strtotime($stockin->date_in)),
                'qty'           => $stockin->qty_available,
                'price'         => $request->get('price')
            ]);
            $stocknew->save();
        }

        $stockin->price             = $request->get('price');
        $stockin->updated_user      = $session_id;
        $stockin->save();

        return redirect('/purchaseincome')->with('success', 'Price has been updated');
    }
}
