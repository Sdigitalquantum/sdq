@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Stock Card</a></h4>
  		</div>
	  	
	  	<div class="box-body">
	  		<form method="post" action="{{ url('stockfilter') }}">
	          	@csrf
	          	<div class="row">
					<div class="col-md-6">
						<div class="form-group">
			              	<label for="product">Product</label>
			              	<select class="form-control select2" name="product" required>
			              		<option value="{{ old('product') }}">Choose One</option>
		                    	@foreach($products as $product)
								    <option value="{{ $product->id }}">{{ $product->code }} - {{ $product->name }}</option>
								@endforeach
		                	</select>
			          	</div>
			          	<div class="form-group">
			              	<label for="year">Year</label>
			              	<input type="number" class="form-control" name="year" value="{{ old('year') }}" required>
			          	</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
			              	<label for="month">Month</label>
			              	<select class="form-control select2" name="month" required>
			              		<option value="{{ old('month') }}">Choose One</option>
			              		<option value="1">January</option>
			              		<option value="2">February</option>
			              		<option value="3">March</option>
			              		<option value="4">April</option>
			              		<option value="5">May</option>
			              		<option value="6">June</option>
			              		<option value="7">July</option>
			              		<option value="8">August</option>
			              		<option value="9">September</option>
			              		<option value="10">October</option>
			              		<option value="11">November</option>
			              		<option value="12">Desember</option>
		                	</select>
			          	</div>
			          	<div class="form-group">
			          		<label for="button">&nbsp;</label> <br>
			          		@if($employee->fkGroup->name != 'User Inventory')
			          			<input type="checkbox" name="price" value="1"> Price &nbsp;
		          			@endif
			              	<button type="submit" class="btn btn-sm btn-success">Filter</button>
			          	</div>
					</div>
				</div>
	      	</form>
	  	</div>
	<div>
@stop