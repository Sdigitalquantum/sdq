<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Companyaccount extends Model
{
    protected $fillable = [
	    'company',
	    'currency',
	    'bank',
	    'city',
	    'account_no',
	    'account_swift',
	    'account_name',
	    'account_address',
	    'alias',
	    'export',
	    'local',
	    'internal',
	    'created_user',
	    'updated_user'
  	];

  	public function fkCompany(){
	    return $this->belongsTo('\App\Http\Models\Company', 'company', 'id');
	}

	public function fkCurrency(){
	    return $this->belongsTo('\App\Http\Models\Currency', 'currency', 'id');
	}

	public function fkBank(){
	    return $this->belongsTo('\App\Http\Models\Bank', 'bank', 'id');
	}

	public function fkCity(){
	    return $this->belongsTo('\App\Http\Models\City', 'city', 'id');
	}
}
