<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Companyaccount;
use App\Http\Models\Currency;
use App\Http\Models\Bank;
use App\Http\Models\City;
use App\Http\Models\Employeeset;

class CompanyaccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        $companyaccount = new Companyaccount([
            'company'           => $request->get('company'),
            'currency'          => $request->get('currency'),
            'bank'              => $request->get('bank'),
            'city'              => $request->get('city'),
            'account_no'        => $request->get('account_no'),
            'account_swift'     => $request->get('account_swift'),
            'account_name'      => $request->get('account_name'),
            'account_address'   => $request->get('account_address'),
            'alias'             => !empty($request->get('alias')) ? $request->get('alias') : '',
            'export'            => ($request->get('export') == 1) ? 1 : 0,
            'local'             => ($request->get('local') == 1) ? 1 : 0,
            'internal'          => ($request->get('internal') == 1) ? 1 : 0,
            'created_user'      => $session_id,
            'updated_user'      => $session_id
	    ]);
	    $companyaccount->save();

	    return redirect('/companyaccount/'.$request->get('company'))->with('success', 'Company Account has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $companyaccounts = Companyaccount::where('company', $id)->orderBy('created_at','desc')->get();
        $currencys = Currency::where('status', 1)->get();
        $banks = Bank::where('status', 1)->get();
        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/companyaccount.show', compact('id','companyaccounts','currencys','banks','citys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $companyaccount = Companyaccount::find($id);
        $companyaccounts = Companyaccount::where('company', $companyaccount->company)->orderBy('created_at','desc')->get();
        $currencys = Currency::where('status', 1)->get();
        $banks = Bank::where('status', 1)->get();
        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/companyaccount.edit', compact('companyaccount','companyaccounts','currencys','banks','citys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $companyaccount = Companyaccount::find($id);
        $companyaccount->company            = $request->get('company');
        $companyaccount->currency           = $request->get('currency');
        $companyaccount->bank               = $request->get('bank');
        $companyaccount->city               = $request->get('city');
        $companyaccount->account_no         = $request->get('account_no');
        $companyaccount->account_swift      = $request->get('account_swift');
        $companyaccount->account_name       = $request->get('account_name');
        $companyaccount->account_address    = $request->get('account_address');
        $companyaccount->alias              = !empty($request->get('alias')) ? $request->get('alias') : '';
        $companyaccount->export             = ($request->get('export') == 1) ? 1 : 0;
        $companyaccount->local              = ($request->get('local') == 1) ? 1 : 0;
        $companyaccount->internal           = ($request->get('internal') == 1) ? 1 : 0;
	    $companyaccount->updated_user       = $session_id;
	    $companyaccount->save();

	    return redirect('/companyaccount/'.$request->get('company'))->with('success', 'Company Account has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $companyaccount = Companyaccount::find($id);
        $status = $companyaccount->status;

        if ($status == 1) {
        	$companyaccount->status       = 0;
            $companyaccount->updated_user = $session_id;
	        $companyaccount->save();

		    return redirect('/companyaccount/'.$companyaccount->company)->with('error', 'Company Account has been deleted');
        } elseif ($status == 0) {
        	$companyaccount->status       = 1;
            $companyaccount->updated_user = $session_id;
	        $companyaccount->save();

		    return redirect('/companyaccount/'.$companyaccount->company)->with('success', 'Company Account has been activated');
        }
    }
}
