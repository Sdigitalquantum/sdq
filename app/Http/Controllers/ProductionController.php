<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Production;
use App\Http\Models\Productiondetail;
use App\Http\Models\Productionother;
use App\Http\Models\Material;
use App\Http\Models\Materialdetail;
use App\Http\Models\Materialother;
use App\Http\Models\Account;
use App\Http\Models\Stockin;
use App\Http\Models\Pricelist;
use App\Http\Models\Letter;
use App\Http\Models\Employeeset;

class ProductionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $productions = Production::orderBy('created_at','desc')->get();
        $productiondetails = Productiondetail::where('status', 1)->get();
        $productionothers = Productionother::where('status', 1)->get();

        return view('vendor/adminlte/production.index', compact('productions','productiondetails','productionothers','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $accounts = Account::where('status', 1)->orderBy('name')->get();
        $materials = Material::where('status', 1)->get();

        return view('vendor/adminlte/production.create', compact('accounts','materials','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        $request->validate([
            'qty' => 'numeric|min:0|not_in:0'
        ]);

        $no = Production::select('no_inc')->whereYear('created_at', date('Y'))->max('no_inc');
        if (empty($no)) {
            $no_inc = 1;
        } else {
            $no_inc = $no + 1;
        }

        $letter = Letter::where('menu', 'Assembly / Production')->first();
        if (empty($letter->company)) {
            $company = "";
        } else {
            $company = $letter->company."/";
        }

        if (empty($letter->code)) {
            $code = "";
        } else {
            $code = $letter->code;
        }

        if ($letter->romawi == 0) {
            $romawi = "";
        } else {
            if (date("m") == 1) {
                $romawi = "I/";
            } elseif (date("m") == 2) {
                $romawi = "II/";
            } elseif (date("m") == 3) {
                $romawi = "III/";
            } elseif (date("m") == 4) {
                $romawi = "IV/";
            } elseif (date("m") == 5) {
                $romawi = "V/";
            } elseif (date("m") == 6) {
                $romawi = "VI/";
            } elseif (date("m") == 7) {
                $romawi = "VII/";
            } elseif (date("m") == 8) {
                $romawi = "VIII/";
            } elseif (date("m") == 9) {
                $romawi = "IX/";
            } elseif (date("m") == 10) {
                $romawi = "X/";
            } elseif (date("m") == 11) {
                $romawi = "XI/";
            } elseif (date("m") == 12) {
                $romawi = "XII/";
            }
        }

        if ($letter->year == 0) {
            $year = "";
        } else {
            $year = date("Y");
        }

        $nomor = $company.$code.sprintf("%05s", $no_inc)."/".$romawi.$year;
    	
        $production = new Production([
            'material'      => $request->get('material'),
            'no_inc'        => $no_inc,
            'nomor'         => $nomor,
            'qty'           => $request->get('qty'),
            'created_user'  => $session_id,
            'updated_user'  => $session_id
        ]);
	    $production->save();

        $total = 0;
        $materialdetails = Materialdetail::where([['material', $request->get('material')], ['status', 1]])->get();
        foreach ($materialdetails as $materialdetail) {
            $cek = Pricelist::where('product', $materialdetail->product)->orderBy('created_at', 'desc')->first();
            $productiondetail = new Productiondetail([
                'production'        => $production->id,
                'product'           => $materialdetail->product,
                'unit'              => $materialdetail->unit,
                'qty'               => $request->get('qty')*$materialdetail->qty,
                'price'             => $request->get('qty')*$materialdetail->qty*$cek->price,
                'created_user'      => $session_id,
                'updated_user'      => $session_id
            ]);
            $productiondetail->save();

            $total = $total + ($request->get('qty')*$materialdetail->qty*$cek->price);
        }

        $materialothers = Materialother::where([['material', $request->get('material')], ['status', 1]])->get();
        foreach ($materialothers as $materialother) {
            $productionother = new Productionother([
                'production'        => $production->id,
                'product'           => $materialother->product,
                'unit'              => $materialother->unit,
                'qty'               => $request->get('qty')*$materialother->qty,
                'created_user'      => $session_id,
                'updated_user'      => $session_id
            ]);
            $productionother->save();
        }

        $no = Stockin::select('no_inc')->whereYear('created_at', date('Y'))->max('no_inc');
        if (empty($no)) {
            $no_stockin = 1;
        } else {
            $no_stockin = $no + 1;
        }

        $letter = Letter::where('menu', 'Incoming Stock')->first();
        if (empty($letter->company)) {
            $company = "";
        } else {
            $company = $letter->company."/";
        }

        if (empty($letter->code)) {
            $code = "";
        } else {
            $code = $letter->code;
        }

        if ($letter->romawi == 0) {
            $romawi = "";
        } else {
            if (date("m") == 1) {
                $romawi = "I/";
            } elseif (date("m") == 2) {
                $romawi = "II/";
            } elseif (date("m") == 3) {
                $romawi = "III/";
            } elseif (date("m") == 4) {
                $romawi = "IV/";
            } elseif (date("m") == 5) {
                $romawi = "V/";
            } elseif (date("m") == 6) {
                $romawi = "VI/";
            } elseif (date("m") == 7) {
                $romawi = "VII/";
            } elseif (date("m") == 8) {
                $romawi = "VIII/";
            } elseif (date("m") == 9) {
                $romawi = "IX/";
            } elseif (date("m") == 10) {
                $romawi = "X/";
            } elseif (date("m") == 11) {
                $romawi = "XI/";
            } elseif (date("m") == 12) {
                $romawi = "XII/";
            }
        }

        if ($letter->year == 0) {
            $year = "";
        } else {
            $year = date("Y");
        }

        $nomor = $company.$code.sprintf("%05s", $no_stockin)."/".$romawi.$year;
        $material = Material::where('id', $request->get('material'))->first();

        $stockin = new Stockin([
            'transaction'       => 2,
            'product'           => $material->product,
            'warehouse'         => 1,
            'supplierdetail'    => 1,
            'no_inc'            => $no_stockin,
            'nomor'             => $nomor,
            'date_in'           => date('Y-m-d', strtotime($production->created_at)),
            'price'             => $total/$request->get('qty'),
            'qty_bag'           => 1,
            'qty_pcs'           => 1,
            'qty_kg'            => $request->get('qty'),
            'qty_available'     => $request->get('qty'),
            'noref_in'          => 'PRODUCTION',
            'transaction_prod'  => $production->id,
            'status_move'       => 0,
            'created_user'      => $session_id,
            'updated_user'      => $session_id
        ]);
        $stockin->save();

	    return redirect('/productiondetail/'.$production->id)->with('success', 'Assembly / Production has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $production  = Production::find($id);
        $materials = Material::where('status', 1)->orderBy('product')->get();

        return view('vendor/adminlte/production.edit', compact('id','production','materials','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $production = Production::find($id);

        $request->validate([
            'qty' => 'numeric|min:0|not_in:0'
        ]);

        $total = 0;
        $productiondetails = Productiondetail::where('production', $production->id)->get();
        foreach ($productiondetails as $productiondetail) {
            $productiondetail->qty      = $productiondetail->qty*$request->get('qty')/$production->qty;
            $productiondetail->price    = ($productiondetail->price/$productiondetail->qty)*($productiondetail->qty*$request->get('qty')/$production->qty);
            $productiondetail->save();

            $total = $total + (($productiondetail->price/$productiondetail->qty)*($productiondetail->qty*$request->get('qty')/$production->qty));
        }

        $productionothers = Productionother::where('production', $production->id)->get();
        foreach ($productionothers as $productionother) {
            $productionother->qty      = $productionother->qty*$request->get('qty')/$production->qty;
            $productionother->save();
        }

        $production->qty           = $request->get('qty');
        $production->updated_user  = $session_id;
        $production->save();

        $stockin = Stockin::where('transaction_prod', $production->id)->first();
        $stockin->price         = $total/$request->get('qty');
        $stockin->qty_kg        = $request->get('qty');
        $stockin->qty_available = $request->get('qty');
        $stockin->updated_user  = $session_id;
        $stockin->save();

	    return redirect('/production')->with('success', 'Assembly / Production has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
