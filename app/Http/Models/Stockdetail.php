<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Stockdetail extends Model
{
    protected $fillable = [
	    'stockqc',
	    'no_inc',
	    'no_bag',
	    'qty_bag',
	    'qty_pcs',
	    'qty_kg',
	    'barcode',
	    'created_user',
	    'updated_user'
  	];

	public function fkStockqc(){
	    return $this->belongsTo('\App\Http\Models\Stockqc', 'stockqc', 'id');
	}
}
