@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
            <li class="disabled"><a>List</a></li>
            <li class="disabled"><a>Booking</a></li>
          	<li class="active"><a href="#tab_schedule" data-toggle="tab">Schedule</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_schedule">
                <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Change Schedule <b>{{ $schedulebook->fkQuotationsplit->no_split }}</b>
				  			&nbsp;<a href="{{ url('schedulebooklist') }}" class="btn btn-xs btn-primary" style="text-decoration: none;">Booking Schedule</a>
				  		</h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="25%">Port Loading</th>
						          	<th width="25%">Port Discharge</th>
						          	<th width="10%">Lines</th>
						          	<th width="15%">Time Departure</th>
						          	<th width="15%">Time Arrival</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($scheduleships as $scheduleship)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>{{$scheduleship->pol}}</td>
							            <td>{{$scheduleship->pod}}</td>
							            <td>{{$scheduleship->lines}}</td>
							            <td>{{$scheduleship->etd}}</td>
							            <td>{{$scheduleship->eta}}</td>
										<td>
							            	<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$scheduleship->id}}">Booking</button>

							            	<form action="{{ url('schedulebook/change', $schedulebook->id)}}" method="post">
							                  	@csrf
							                  	@method('DELETE')
							                  	<input type="hidden" name="scheduleship" value="{{ $scheduleship->id }}">
							                  	<div class="modal fade bs-example-modal{{$scheduleship->id}}" tabindex="-1" role="dialog" aria-hidden="true">
				                                    <div class="modal-dialog modal-sm">
				                                        <div class="modal-content">
				                                            <div class="modal-header btn-success">
				                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
				                                                </button>
				                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Booking</h4>
				                                            </div>
				                                            <div class="modal-body">
				                                            	Are you sure to change this schedule?
				                                            	<br><textarea class="form-control" name="notice" cols="30" rows="3" required>{{ $schedulebook->notice }}</textarea>
				                                            </div>
				                                            <div class="modal-footer">
				                                                <button type="submit" class="btn btn-success">Yes</button>
				                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
				                                            </div>
				                                        </div>
				                                    </div>
				                                </div>
							                </form>
							            </td>
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Port Loading</td>
						          	<td>Port Discharge</td>
						          	<td>Lines</td>
						          	<td>Time Departure</td>
						          	<td>Time Arrival</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop