<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Material;
use App\Http\Models\Materialdetail;
use App\Http\Models\Materialother;
use App\Http\Models\Product;
use App\Http\Models\Company;
use App\Http\Models\Employeeset;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $materials = Material::orderBy('created_at','desc')->get();
        $materialdetails = Materialdetail::where('status', 1)->get();
        $materialothers = Materialother::where('status', 1)->get();

        return view('vendor/adminlte/material.index', compact('materials','materialdetails','materialothers','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $products = Product::where('status', 1)->orderBy('name')->get();

        return view('vendor/adminlte/material.create', compact('products','disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            $sales = Sales::find($id);
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $status_group   = 0; if (isset($json->status_group)) { $status_group  = 1; };
                $status_record  = 0; if (isset($json->status_record)){ $status_record = 1; };

                $material = new Material([
                    'product'       => $json->product,
                    'status_group'  => $status_group,
                    'status_record' => $status_record,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $material->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $material  = Material::find($id);
        $materialdetails = Materialdetail::where('material', $id)->get();
        $materialothers = Materialother::where('material', $id)->get();
        $products = Product::where('status', 1)->orderBy('name')->get();
        $company = Company::first();

        return view('vendor/adminlte/material.show', compact('id','material','materialdetails','materialothers','products','company','disable'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $material  = Material::find($id);
        $products = Product::where('status', 1)->orderBy('name')->get();

        return view('vendor/adminlte/material.edit', compact('id','material','products','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            $sales = Sales::find($id);
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $status_group   = 0; if (isset($json->status_group)) { $status_group  = 1; };
                $status_record  = 0; if (isset($json->status_record)){ $status_record = 1; };

                $material = Material::find($id);
                $material->product       = $json->product;
                $material->status_group  = $status_group;
                $material->status_record = $status_record;
                $material->updated_user  = $session_id;
                $result = $material->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $material = Material::find($id);
        $status = $material->status;

        if ($status == 1) {
        	$material->status       = 0;
            $material->updated_user = $session_id;
	        $result = $material->save();

        } elseif ($status == 0) {
        	$material->status       = 1;
            $material->updated_user = $session_id;
	        $result = $material->save();

        };
        return $this->jsonSuccess( $result );
    }
}
