@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>Import</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
                <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Shipping Schedule 
				  			&nbsp;<a href="{{ route('scheduleship.create') }}" class="btn btn-xs btn-success" style="text-decoration: none;">Import</a>
				  			&nbsp;<a href="{{ asset('storage/VesselSchedules.xlsx') }}" class="btn btn-xs btn-primary" style="text-decoration: none;" download>Example</a>
				  		</h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="25%">Port Loading</th>
						          	<th width="25%">Port Discharge</th>
						          	<th width="10%">Lines</th>
						          	<th width="15%">Time Departure</th>
						          	<th width="15%">Time Arrival</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($scheduleships as $scheduleship)
						    		@if($scheduleship->etd >= date('Y-m-d', strtotime("now")))
							    		@php ($no++)
							        	<tr>
								            <td>{{$no}}</td>
								            <td>{{$scheduleship->pol}}</td>
								            <td>{{$scheduleship->pod}}</td>
								            <td>{{$scheduleship->lines}}</td>
								            <td>{{$scheduleship->etd}}</td>
								            <td>{{$scheduleship->eta}}</td>

								            @if($scheduleship->status_book == 0)
									            <td>
									            	<font color="blue">Open <i class="glyphicon glyphicon-ok"></i></font>
									            </td>
								            @else
								            	<td>
								            		<font color="red">Booked <i class="glyphicon glyphicon-remove"></i></font>
								            	</td>
								            @endif
								        </tr>
							        @endif
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Port Loading</td>
						          	<td>Port Discharge</td>
						          	<td>Lines</td>
						          	<td>Time Departure</td>
						          	<td>Time Arrival</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop