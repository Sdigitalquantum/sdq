@extends('adminlte::frontend/page')

@section('content')
    <section class='section-heading padding-top-100 padding-bottom-0'>
        <div class="angled_down_inside white">
            <div class="slope upleft"></div>
            <div class="slope upright"></div>
        </div>
        
        <div class="container">
            <div class="row">
                <h1 class="heading">Edit Product</h1>
                <div class="headul"></div>
                <p class="subheading">Silahkan memesan produk kami dengan cara mengisi form di bawah ini</p>
            </div>
        </div>
        
        <div class="angled_up_inside white">
            <div class="slope upleft"></div>
            <div class="slope upright"></div>
        </div>
    </section>
    
    <section class="parallax contact padding-top-150" id="order">
        <div class="bg-overlay opacity-85"></div>
        <div class="container">
            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 animated delay1" data-effect="fadeInUp">
                <form method="post" action="{{ route('orderdetail.update', $orderdetail->id) }}">
                    @method('PATCH')
                    @csrf
                    <select class="col-xs-12 transition" name="product" required>
                        @foreach($products as $product)
                            @if ($orderdetail->product == $product->id)
                                <option value="{{ $product->id }}" selected>{{ $product->name }}</option>
                            @else
                                <option value="{{ $product->id }}">{{ $product->name }}</option>
                            @endif
                        @endforeach
                    </select><br><br>
                    <input type='number' placeholder='Qty' class='col-xs-12 transition' name='qty_order' value="{{ $orderdetail->qty_order }}" required>
                    <button type='submit' class='btn btn-block btn-primary transition'>Update</button>
                </form>
            </div>

            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 animated delay1" data-effect="fadeInUp">
                <table class="table table-bordered table-hover nowrap" width="100%">
                    <thead style="background: #00a65a !important; font-weight: bold; text-align: center; color: white;">
                        <tr>
                            <th width="70%">Product</th>
                            <th width="25%" style="text-align: center;">Qty</th>
                            <th width="5%" style="text-align: center;">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($orderdetails as $orderdetail)
                            <tr>
                                <td>{{$orderdetail->fkProduct->name}}</td>
                                <td style="text-align: center;">{{$orderdetail->qty_order}}</td>
                                <td style="text-align: center;">
                                    <a href="{{ route('orderdetail.edit',$orderdetail->id)}}" class="btn btn-xs btn-info">Edit</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@stop
