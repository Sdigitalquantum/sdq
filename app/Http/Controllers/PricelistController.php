<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Pricelist;
use App\Http\Models\Product;
use App\Http\Models\Currency;
use App\Http\Models\Employeeset;
use DB;

class PricelistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $pricelists = Pricelist::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/pricelist.index', compact('pricelists','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $products = Product::where('status', 1)->get();
        $currencys = Currency::where('status', 1)->get();

        return view('vendor/adminlte/pricelist.create', compact('products','currencys','disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   // begin transaction
            DB::beginTransaction();
            try 
            {   foreach ($json_data as $json) 
                {   //if ($json->created_date == ''){}else{};
                    $cek = Pricelist::where('product', $json->product)->first();
                    
                        // if (!empty($cek)) { $result = false; } 
                        // else { 
                            
                        // };

                    $pricelist = new Pricelist([
                            'product'       => $json->product,
                            'currency'      => $json->currency,
                            'price'         => $json->price,
                            'date'          => $json->date,
                            'created_user'  => $session_id,
                            'updated_user'  => $session_id
                        ]);
                        $result = $pricelist->save();
                };
                // commit transaction
                $result = [true, "Save succcefully"];
                DB::commit();
            } catch (\Illuminate\Database\QueryException $e) {
                // rollback transaction
                $result = [false, "Error Executed ---> ".$e];
                DB::rollback();
            };
        };
        // return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $pricelist = Pricelist::find($id);
        $products = Product::where('status', 1)->get();
        $currencys = Currency::where('status', 1)->get();

        return view('vendor/adminlte/pricelist.edit', compact('pricelist','products','currencys','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   // not yet finish need to check with the actual UI
        $session_id = $request->session()->get('session_login');
        $pricelist = Pricelist::find($id);
    	$json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                if ($pricelist->product == $json->product) {
                    $pricelist->currency        = $json->currency;
                    $pricelist->price           = $json->price;
                    $pricelist->date            = $json->date;
                    $pricelist->updated_user    = $session_id;
                    $result = $pricelist->save();

                } else {
                    $cek = Pricelist::where('product', $json->product)->first();
                
                    if (!empty($cek)) {
                        //return redirect()->back()->withErrors('Price List already exist');
                        $result = false;
                    } else {
                        $pricelist->product         = $json->product;
                        $pricelist->currency        = $json->currency;
                        $pricelist->price           = $json->price;
                        $pricelist->date            = $json->date;
                        $pricelist->updated_user    = $session_id;
                        $return = $pricelist->save();
                    }
                }
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $pricelist = Pricelist::find($id);
        $status = $pricelist->status;

        if ($status == 1) {
        	$pricelist->status       = 0;
            $pricelist->updated_user = $session_id;
	        $result = $pricelist->save();

        } elseif ($status == 0) {
        	$pricelist->status       = 1;
            $pricelist->updated_user = $session_id;
	        $result = $pricelist->save();

        };
        return $this->jsonSuccess( $result );
    }
}
