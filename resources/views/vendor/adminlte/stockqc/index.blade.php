@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>Quality Control</a></li>
            <li class="disabled"><a>Detail</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
                <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Detail Purchase Order</h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="15%">No. PO</th>
						          	<th width="20%">Product</th>
						          	<th width="20%">Supplier</th>
						          	<th width="15%">Warehouse</th>
						          	<th width="10%">Qty (Kg)</th>
						          	<th width="10%">Process</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($array = [])
						    	@foreach($employeewarehouses as $employeewarehouse)
						    		@php ($array[] = $employeewarehouse->warehouse)
						    	@endforeach

						    	@php ($no = 0)
						    	@foreach($purchasedetails as $purchasedetail)
						    		@if(in_array($purchasedetail->warehouse, $array))
							    		@php ($no++)
							    		<tr>
							    			<td>{{$no}}</td>
								            <td>{{$purchasedetail->fkPurchaseorder->no_po}}</td>
								            <td>{{$purchasedetail->fkProduct->name}}</td>
								            <td>{{$purchasedetail->fkSupplierdetail->name}}</td>
								            <td>{{$purchasedetail->fkWarehouse->name}}</td>
								            <td>
								            	{{number_format($purchasedetail->qty_order+$purchasedetail->qty_buffer, 0, ',' , '.')}}
								            	@if($purchasedetail->qty_order+$purchasedetail->qty_buffer-$purchasedetail->qty_qc != 0)
								            		<br>Remained : <font color="red">{{number_format($purchasedetail->qty_order+$purchasedetail->qty_buffer-$purchasedetail->qty_qc, 0, ',' , '.')}}</font>
								            	@endif
								            </td>
								            <td>
								            	@if($purchasedetail->status_qc == 1)
								            		@if($purchasedetail->qty_order+$purchasedetail->qty_buffer == $purchasedetail->qty_qc)
								            			<font color="green">Done <i class="glyphicon glyphicon-ok"></i></font>
							            			@else
							            				<font color="blue">In Progress</font>
							            			@endif
								            	@else
								            		<font color="red">Not Yet <i class="glyphicon glyphicon-remove"></i></font>
								            	@endif
							            	</td>
											<td>
												@if($purchasedetail->status_qc == 1)
													@if($purchasedetail->qty_order+$purchasedetail->qty_buffer == $purchasedetail->qty_qc)
														<font color="green">Complete <i class="glyphicon glyphicon-ok"></i></font>
								            		@else
							            				<a href="{{ url('stockqc/create',$purchasedetail->id)}}" class="btn btn-xs btn-warning">Quality Control</a>
							            			@endif
								            	@else
								            		<a href="{{ url('stockqc/create',$purchasedetail->id)}}" class="btn btn-xs btn-success">Quality Control</a>
								            	@endif
											</td>
								        </tr>
							        @endif
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						        	<td class="noShow"></td>
						          	<td>No. PO</td>
						          	<td>Product</td>
						          	<td>Supplier</td>
						          	<td>Warehouse</td>
						          	<td>Qty (Kg)</td>
						          	<td>Process</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop