@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Delivery Order Confirmation - Shipping</h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="5%">No</th>
			          	<th width="15%">Nomor</th>
			          	<th width="30%">Quality Check</th>
			          	<th width="15%">Weigh Check (Kg)</th>
			          	<th width="15%">Total Qty (Kg)</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@php ($no = 0)
			    	@foreach($stufforders as $stufforder)
			    		@php ($no++)
			        	<tr>
				            <td>{{$no}}</td>
				            <td>{{$stufforder->no_letter}}</td>
				            <td>{{$stufforder->check_quality}}</td>
				            <td style="text-align: right;">{{$stufforder->check_weigh}} &nbsp;</td>
				            <td style="text-align: right;">
				            	@php ($total = 0)
				            	@foreach($stuffs as $stuff)
				            		@if($stuff->schedulestuff == $stufforder->schedulestuff)
				            			@php ($total = $total + $stuff->qty_load)
				            		@endif
				            	@endforeach
				            	{{$total}} &nbsp;
			            	</td>
				        </tr>
			        @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			          	<td class="noShow"></td>
			          	<td>Nomor</td>
			          	<td>Check Quality</td>
			          	<td>Check Weigh (Kg)</td>
			          	<td>Total Qty (Kg)</td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	</div>
@stop