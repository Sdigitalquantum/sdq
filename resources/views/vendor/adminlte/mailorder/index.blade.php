@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Mailbox Order</h4>
  		</div>
	  	
	  	<div class="box-body">
	  		<div class="row">
	  			<div class="col-md-2">
		          	<div class="box box-solid">
		            	<div class="box-header with-border">
		              		<ul class="nav nav-pills nav-stacked">
		               		 	<li class="active">
		               		 		<a><i class="fa fa-inbox"></i> Inbox
		               		 			@if($inbox != 0)
				                  			<span class="label label-success pull-right">{{ $inbox }}</span>
			                  			@endif
		                  			</a>
		                  		</li>
		              		</ul>
		            	</div>
		          	</div>
		        </div>

		        <div class="col-md-10">
		        	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
					    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
					        <tr>
					          	<th width="5%">No</th>
					          	<th width="15%">Customer</th>
					          	<th width="15%">Email</th>
					          	<th width="15%">Phone</th>
					          	<th width="30%">Address</th>
					          	<th width="20%">Date</th>
					        </tr>
					    </thead>

					    <tbody>
					    	@php ($no = 0)
					    	@foreach($mailorders as $mailorder)
					    		@php ($no++)
					    		@if($mailorder->status == 1)
				            		<tr>
							            <td>{{$no}}</td>
							            <td><a href="{{ route('mailorder.show', $mailorder->id) }}">{{$mailorder->customer}}</a></td>
							            <td><a href="{{ route('mailorder.show', $mailorder->id) }}">{{$mailorder->email}}</a></td>
							            <td style="text-align: right;">{{$mailorder->phone}} &nbsp;</td>
							            <td><a href="{{ route('mailorder.show', $mailorder->id) }}">{{$mailorder->address}}</a></td>
							            <td>{{ date('d F Y - H:m:s', strtotime($mailorder->created_at)) }}</td>
							        </tr>
			            		@else
			            			<tr style="font-weight: bold;">
							            <td>{{$no}}</td>
							            <td><a href="{{ route('mailorder.show', $mailorder->id) }}">{{$mailorder->customer}}</a></td>
							            <td><a href="{{ route('mailorder.show', $mailorder->id) }}">{{$mailorder->email}}</a></td>
							            <td>{{$mailorder->phone}}</td>
							            <td>{{$mailorder->address}}</td>
							            <td>{{ date('d F Y - H:m:s', strtotime($mailorder->created_at)) }}</td>
							        </tr>
			            		@endif
					        @endforeach
					    </tbody>

					    <tfoot>
					        <tr>
					          	<td class="noShow"></td>
					          	<td>Name</td>
					          	<td>Email</td>
					          	<td>Phone</td>
					          	<td>Address</td>
					          	<td>Date</td>
					        </tr>
					    </tfoot>
				  	</table>
	        	</div>
  			</div>
	  	</div>
	</div>
@stop