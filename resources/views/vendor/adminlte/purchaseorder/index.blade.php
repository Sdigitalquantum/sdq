@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
            <li class="disabled"><a>Request</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
                <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Purchase Order &nbsp;<a href="{{ route('purchaseorder.create')}}" class="btn btn-xs btn-success" style="text-decoration: none;">New</a></h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="15%">No. PO</th>
						          	<th width="10%">Date</th>
						          	<th width="15%">Note</th>
						          	<th width="45%">Detail Request</th>
						          	<th width="5%">Approval</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($purchaseorders as $purchaseorder)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>{{$purchaseorder->no_po}}</td>
							            <td>{{$purchaseorder->date_order}}</td>
							            <td><pre>{{$purchaseorder->notice}}</pre></td>
							            <td>
							            	<ul>
								            	@foreach($purchasedetails as $purchasedetail)
								            		@if($purchasedetail->purchaseorder == $purchaseorder->id)
								            			<li>
								            				[{{ $purchasedetail->fkProduct->name }}] {{ $purchasedetail->fkSupplierdetail->name }} <br>
								            				[Qty Kg : {{ number_format($purchasedetail->qty_order+$purchasedetail->qty_buffer, 0, ',' , '.') }}, Price : Rp. {{ number_format($purchasedetail->price, 0, ',' , '.') }}] Total : Rp. {{ number_format(($purchasedetail->qty_order+$purchasedetail->qty_buffer)*$purchasedetail->price, 0, ',' , '.') }}
								            			</li>
							            				<ol>
								            				@foreach($purchasedetailprs as $purchasedetailpr)
											            		@if($purchasedetailpr->purchasedetail == $purchasedetail->id)
											            			@if($purchasedetailpr->purchaserequest != 0)
											            				<li>No. PR : {{ $purchasedetailpr->fkPurchaserequest->no_pr }}</li>
										            				@else
										            					<li>No. PG : {{ $purchasedetailpr->fkPurchasedetailprg->fkPurchasegeneral->no_prg }}</li>
										            				@endif
											            		@endif
											            	@endforeach
										            	</ol>
								            		@endif
								            	@endforeach
							            	</ul>
							            </td>
							            <td>
							            	@if($purchaseorder->status == 0)
							            		<font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
							            	@else
								            	@if($purchaseorder->approve == 1 and $purchaseorder->status_approve == 0)
								            		@if($purchaseorder->status_release == 1)
										            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal-set{{$purchaseorder->id}}">Set None</button>

										            	<form action="{{ url('purchaseorder/set', $purchaseorder->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal-set{{$purchaseorder->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-danger">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to set none approval this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
									                @else
								            			<font color="red">None <i class="glyphicon glyphicon-remove"></i></font>
								            		@endif
								                @elseif($purchaseorder->approve == 0 and $purchaseorder->status_approve == 1)
								                	<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal-set{{$purchaseorder->id}}">Set Used</button>

									            	<form action="{{ url('purchaseorder/set', $purchaseorder->id)}}" method="post">
									                  	@csrf
									                  	@method('DELETE')
									                  	<div class="modal fade bs-example-modal-set{{$purchaseorder->id}}" tabindex="-1" role="dialog" aria-hidden="true">
						                                    <div class="modal-dialog modal-sm">
						                                        <div class="modal-content">
						                                            <div class="modal-header btn-success">
						                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
						                                                </button>
						                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
						                                            </div>
						                                            <div class="modal-body">
						                                                Are you sure to set used approval this data?
						                                            </div>
						                                            <div class="modal-footer">
						                                                <button type="submit" class="btn btn-success">Yes</button>
						                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
						                                            </div>
						                                        </div>
						                                    </div>
						                                </div>
									                </form>
								                @else
								                	<font color="blue">Yes <i class="glyphicon glyphicon-ok"></i></font>
								                @endif
							                @endif
							            </td>

							            @if($purchaseorder->status_approve == 1)
		            						<td>
		            							<font color="green">Approved <i class="glyphicon glyphicon-ok"></i></font>
		            						</td>
	            						@elseif($purchaseorder->status_approve == 2)
			            					<td>
			            						<font color="red">Rejected <i class="glyphicon glyphicon-remove"></i></font>
		            						</td>
	            						@else
											@if($purchaseorder->status == 1)
									            <td>
									            	@if($purchaseorder->status_release == 1)
									            		<font color="green">Released <i class="glyphicon glyphicon-ok"></i></font> <br>
									            		<font color="blue">Waiting Approval</font>
									            	@else
									            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal-rl{{$purchaseorder->id}}">Release</button> &nbsp;

										            	<a href="{{ route('purchaseorder.edit',$purchaseorder->id)}}" class="btn btn-xs btn-warning">Edit</a> &nbsp;
										            	
										            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$purchaseorder->id}}">Delete</button>

										            	<form action="{{ url('purchaseorder/release', $purchaseorder->id) }}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal-rl{{$purchaseorder->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-success">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Release</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to release this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>

										            	<form action="{{ route('purchaseorder.destroy', $purchaseorder->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$purchaseorder->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-danger">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to delete this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
									                @endif
									            </td>
								            @else
								            	<td>
								            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$purchaseorder->id}}">Activated</button>

								            		<form action="{{ route('purchaseorder.destroy', $purchaseorder->id)}}" method="post">
									                  	@csrf
									                  	@method('DELETE')
									                  	<div class="modal fade bs-example-modal{{$purchaseorder->id}}" tabindex="-1" role="dialog" aria-hidden="true">
						                                    <div class="modal-dialog modal-sm">
						                                        <div class="modal-content">
						                                            <div class="modal-header btn-success">
						                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
						                                                </button>
						                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
						                                            </div>
						                                            <div class="modal-body">
						                                                Are you sure to active this data?
						                                            </div>
						                                            <div class="modal-footer">
						                                                <button type="submit" class="btn btn-success">Yes</button>
						                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
						                                            </div>
						                                        </div>
						                                    </div>
						                                </div>
									                </form>
								            	</td>
								            @endif
							            @endif
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>No. PO</td>
						          	<td>Date</td>
						          	<td>Note</td>
						          	<td>Detail Request</td>
						          	<td class="noShow"></td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop