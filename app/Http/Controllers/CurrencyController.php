<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Currency;
use App\Http\Models\Employeeset;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $currencys = Currency::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/currency.index', compact('currencys','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        return view('vendor/adminlte/currency.create', compact('disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	$json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'code' => 'unique:currencies'
                ]);

                $currency = new Currency([
                    'code'          => $json->code,
                    'name'          => $json->name,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $currency->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $currency = Currency::find($id);

        return view('vendor/adminlte/currency.edit', compact('currency','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $currency = Currency::find($id);
                $code = $currency->code;

                if ($code != $json->code) {
                    $request->validate([
                        'code' => 'unique:currencies'
                    ]);
                }
                
                $currency->code             = $json->code;
                $currency->name             = $json->name;
                $currency->updated_user     = $session_id;
                $result = $currency->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $currency = Currency::find($id);
        $status = $currency->status;

        if ($status == 1) {
        	$currency->status       = 0;
            $currency->updated_user = $session_id;
	        $result = $currency->save();

        } elseif ($status == 0) {
        	$currency->status       = 1;
            $currency->updated_user = $session_id;
	        $result = $currency->save();

        };
        return $this->jsonSuccess( $result );
    }
}
