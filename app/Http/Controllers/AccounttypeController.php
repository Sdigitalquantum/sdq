<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Accounttype;
use App\Http\Models\Employeeset;

class AccounttypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $accounttypes = Accounttype::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/accounttype.index', compact('accounttypes','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $accounttype = new Accounttype();

        return view('vendor/adminlte/accounttype.create', compact('accounttype','disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	$json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'code' => 'unique:accounttypes'
                ]);

                $accounttype = new Accounttype([
                    'code'          => $json->code,
                    'name'          => $json->name,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $accounttype->save();

            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $accounttype = Accounttype::find($id);

        return view('vendor/adminlte/accounttype.edit', compact('accounttype','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $accounttype = Accounttype::find($id);
                $code = $accounttype->code;

                if ($code != $request->get('code')) {
                    $request->validate([
                        'code' => 'unique:accounttypes'
                    ]);
                }
                
                $accounttype->code          = $json->code;
                $accounttype->name          = $json->name;
                $accounttype->updated_user  = $session_id;
                $result = $accounttype->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $accounttype = Accounttype::find($id);
        $status = $accounttype->status;

        if ($status == 1) {
        	$accounttype->status       = 0;
            $accounttype->updated_user = $session_id;
	        $result = $accounttype->save();

        } elseif ($status == 0) {
        	$accounttype->status       = 1;
            $accounttype->updated_user = $session_id;
	        $result = $accounttype->save();

        };
        return $this->jsonSuccess( $result );
    }
}
