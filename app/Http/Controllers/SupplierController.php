<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Supplier;
use App\Http\Models\Supplierdetail;
use App\Http\Models\Supplieraccount;
use App\Http\Models\Account;
use App\Http\Models\City;
use App\Http\Models\Company;
use App\Http\Models\Employeeset;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $suppliers = Supplier::orderBy('created_at','desc')->get();
        $supplierdetails = Supplierdetail::where('status', 1)->get();

        return view('vendor/adminlte/supplier.index', compact('suppliers','supplierdetails','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $accounts = Account::where('status', 1)->get();
        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/supplier.create', compact('accounts','citys','disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                if (!empty($json->email)) {
                    $request->validate([
                        'email' => 'email:true'
                    ]);
                }
                
                $no = Supplier::select('no_inc')->max('no_inc');
                if (empty($no)) {
                    $no_inc = 1;
                } else {
                    $no_inc = $no + 1;
                }
                $code           = "S".sprintf("%05s", $no_inc);
                $tax            = 0; if (isset($json->tax))          { $tax           = 1; };
                $status_group   = 0; if (isset($json->status_group)) { $status_group  = 1; };
                $status_record  = 0; if (isset($json->status_record)){ $status_record = 1; };

                $supplier = new Supplier([
                    'no_inc'        => $no_inc,
                    'code'          => $code,
                    'accounting'    => $json->account,
                    'fax'           => !empty($json->fax) ? $json->fax : '',
                    'email'         => !empty($json->email) ? $json->email : '',
                    'tax'           => $tax,
                    'npwp_no'       => !empty($json->npwp_no) ? $json->npwp_no : '',
                    'npwp_name'     => !empty($json->npwp_name) ? $json->npwp_name : '',
                    'npwp_address'  => !empty($json->npwp_address) ? $json->npwp_address : '',
                    'npwp_city'     => !empty($json->npwp_city) ? $json->npwp_city : 0,
                    'limit'         => $json->limit,
                    'note1'         => !empty($json->note1) ? $json->note1 : '',
                    'note2'         => !empty($json->note2) ? $json->note2 : '',
                    'note3'         => !empty($json->note3) ? $json->note3 : '',
                    'notice'        => !empty($json->notice) ? $json->notice : '',
                    'status_group'  => $status_group,
                    'status_record' => $status_record,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $supplier->save();

            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $supplier = Supplier::find($id);
        $supplierdetails = Supplierdetail::where('supplier', $id)->get();
        $supplieraccounts = Supplieraccount::where('supplier', $id)->get();
        $accounts = Account::where('status', 1)->get();
        $citys = City::where('status', 1)->get();
        $company = Company::first();

        return view('vendor/adminlte/supplier.show', compact('id','supplier','supplierdetails','supplieraccounts','accounts','citys','company','disable'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $supplier = Supplier::find($id);
        $accounts = Account::where('status', 1)->get();
        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/supplier.edit', compact('id','supplier','accounts','citys','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                if (!empty($json->email)) {
                    $request->validate([
                        'email' => 'email:true'
                    ]);
                }

                $tax            = 0; if (isset($json->tax))          { $tax           = 1; };
                $status_group   = 0; if (isset($json->status_group)) { $status_group  = 1; };
                $status_record  = 0; if (isset($json->status_record)){ $status_record = 1; };

                $supplier = Supplier::find($id);
                $supplier->accounting    = $json->account;
                $supplier->fax           = !empty($json->fax) ? $json->fax : '';
                $supplier->email         = !empty($json->email) ? $json->email : '';
                $supplier->tax           = $tax;
                $supplier->npwp_no       = !empty($json->npwp_no) ? $json->npwp_no : '';
                $supplier->npwp_name     = !empty($json->npwp_name) ? $json->npwp_name : '';
                $supplier->npwp_address  = !empty($json->npwp_address) ? $json->npwp_address : '';
                $supplier->npwp_city     = !empty($json->npwp_city) ? $json->npwp_city : 0;
                $supplier->limit         = $json->limit;
                $supplier->note1         = !empty($json->note1) ? $json->note1 : '';
                $supplier->note2         = !empty($json->note2) ? $json->note2 : '';
                $supplier->note3         = !empty($json->note3) ? $json->note3 : '';
                $supplier->notice        = !empty($json->notice) ? $json->notice : '';
                $supplier->status_group  = $status_group;
                $supplier->status_record = $status_record;
                $supplier->updated_user  = $session_id;
                $result = $supplier->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $supplier = Supplier::find($id);
        $status = $supplier->status;

        if ($status == 1) {
        	$supplier->status       = 0;
            $supplier->updated_user = $session_id;
	        $result = $supplier->save();

        } elseif ($status == 0) {
        	$supplier->status       = 1;
            $supplier->updated_user = $session_id;
	        $result = $supplier->save();

        };
        return $this->jsonSuccess( $result );
    }
}
