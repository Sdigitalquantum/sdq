@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #6b6e70 !important; font-weight: bold; color: white;">
			    		<h4 class="box-title">Edit Variant</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form id="variantFrm">
				      		@method('PATCH')
				      		@csrf
				          	<div class="form-group">
				              	<label for="name">Name</label>
				              	<input type="text" class="form-control" name="name" value="{{ $variant->name }}" required>
				          	</div>
				          	<button id="updateBtn" value="{{ route('variant.update', $variant->id) }}" class="btn btn-sm btn-success">Update</button>
				          	<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.variant.prepare_edit());
    </script>
@stop