<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
	    'accounting',
	    'department',
	    'variant',
	    'merk',
	    'typesub',
	    'size',
	    'unit',
	    'code',
	    'name',
	    'general',
	    'status_group',
	    'status_record',
	    'created_user',
	    'updated_user'
  	];

  	public function fkAccount(){
	    return $this->belongsTo('\App\Http\Models\Account', 'accounting', 'id');
	}

	public function fkDepartment(){
	    return $this->belongsTo('\App\Http\Models\Department', 'department', 'id');
	}

	public function fkVariant(){
	    return $this->belongsTo('\App\Http\Models\Variant', 'variant', 'id');
	}

	public function fkMerk(){
	    return $this->belongsTo('\App\Http\Models\Merk', 'merk', 'id');
	}

	public function fkTypesub(){
	    return $this->belongsTo('\App\Http\Models\Typesub', 'typesub', 'id');
	}

	public function fkSize(){
	    return $this->belongsTo('\App\Http\Models\Size', 'size', 'id');
	}

	public function fkUnit(){
	    return $this->belongsTo('\App\Http\Models\Unit', 'unit', 'id');
	}
}
