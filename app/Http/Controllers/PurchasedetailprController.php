<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Purchasedetailpr;
use App\Http\Models\Purchasedetail;
use App\Http\Models\Purchaserequest;
use App\Http\Models\Purchasedetailprg;
use App\Http\Models\Employeeset;
use App\Http\Models\Employeewarehouse;

class PurchasedetailprController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $purchasedetail = Purchasedetail::find($id);
        $purchasedetailprs = Purchasedetailpr::where('purchasedetail', $id)->get();
        $purchaserequests = Purchaserequest::where([['status', 1], ['product', $purchasedetail->product], ['status_approve', 1], ['status_release', 1], ['qty_remain','<>',0]])->get();
        $purchasedetailprgs = Purchasedetailprg::where([['status', 1], ['product', $purchasedetail->product], ['qty_remain','<>',0]])->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/purchasedetailpr.edit', compact('purchasedetail','purchasedetailprs','purchaserequests','purchasedetailprgs','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $request->validate([
            'qty_remain' => 'numeric|min:0|not_in:0'
        ]);
        
        if (!empty($request->get('purchaserequest'))) {
            $cek = Purchasedetailpr::where([['purchasedetail', $request->get('purchasedetail')], ['purchaserequest', $request->get('purchaserequest')]])->first();
            $purchaserequest = Purchaserequest::find($request->get('purchaserequest'));

            if ($request->get('qty_remain') > $purchaserequest->qty_remain) {
                $qty = $purchaserequest->qty_remain;
            } else {
                $qty = $request->get('qty_remain');
            }

            if (!empty($cek)) {
                $cek->qty = $cek->qty+$qty;
                $cek->save();

                $purchasedetail = Purchasedetail::where('id', $request->get('purchasedetail'))->first();
                $purchasedetail->qty_order = $purchasedetail->qty_order+$qty;
                $purchasedetail->save();

                $purchaserequest->qty_remain = $purchaserequest->qty_remain-$qty;
                $purchaserequest->save();

                return redirect('/purchasedetailpr/'.$request->get('purchasedetail').'/edit')->with('success', 'Purchase Request has been added');
            } else {
                $purchasedetailpr = new Purchasedetailpr([
                    'purchasedetail'    => $request->get('purchasedetail'),
                    'purchaserequest'   => $request->get('purchaserequest'),
                    'purchasedetailprg' => 0,
                    'qty'               => $qty,
                    'created_user'      => $session_id
                ]);
                $purchasedetailpr->save();

                $purchasedetail = Purchasedetail::where('id', $request->get('purchasedetail'))->first();
                $purchasedetail->qty_order = $purchasedetail->qty_order+$qty;
                $purchasedetail->save();

                $purchaserequest->qty_remain = $purchaserequest->qty_remain-$qty;
                $purchaserequest->save();

                return redirect('/purchasedetailpr/'.$request->get('purchasedetail').'/edit')->with('success', 'Purchase Request has been added');
            }
        } else {
            $cek = Purchasedetailpr::where([['purchasedetail', $request->get('purchasedetail')], ['purchasedetailprg', $request->get('purchasedetailprg')]])->first();
            $purchasedetailprg = Purchasedetailprg::find($request->get('purchasedetailprg'));

            if ($request->get('qty_remain') > $purchasedetailprg->qty_remain) {
                $qty = $purchasedetailprg->qty_remain;
            } else {
                $qty = $request->get('qty_remain');
            }

            if (!empty($cek)) {
                $cek->qty = $cek->qty+$qty;
                $cek->save();

                $purchasedetail = Purchasedetail::where('id', $request->get('purchasedetail'))->first();
                $purchasedetail->qty_order = $purchasedetail->qty_order+$qty;
                $purchasedetail->save();

                $purchasedetailprg->qty_remain = $purchasedetailprg->qty_remain-$qty;
                $purchasedetailprg->save();

                return redirect('/purchasedetailpr/'.$request->get('purchasedetail').'/edit')->with('success', 'Purchase Request has been added');
            } else {
                $purchasedetailpr = new Purchasedetailpr([
                    'purchasedetail'    => $request->get('purchasedetail'),
                    'purchaserequest'   => 0,
                    'purchasedetailprg' => $request->get('purchasedetailprg'),
                    'qty'               => $qty,
                    'created_user'      => $session_id
                ]);
                $purchasedetailpr->save();

                $purchasedetail = Purchasedetail::where('id', $request->get('purchasedetail'))->first();
                $purchasedetail->qty_order = $purchasedetail->qty_order+$qty;
                $purchasedetail->save();

                $purchasedetailprg->qty_remain = $purchasedetailprg->qty_remain-$qty;
                $purchasedetailprg->save();

                return redirect('/purchasedetailpr/'.$request->get('purchasedetail').'/edit')->with('success', 'Purchase Request has been added');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $purchasedetailpr = Purchasedetailpr::find($id);

        $purchasedetail = Purchasedetail::where('id', $purchasedetailpr->purchasedetail)->first();
        $purchasedetail->qty_order = $purchasedetail->qty_order-$purchasedetailpr->qty;
        $purchasedetail->save();

        if ($purchasedetailpr->purchaserequest == 0) {
            $purchasedetailprg = Purchasedetailprg::where('id', $purchasedetailpr->purchasedetailprg)->first();
            $purchasedetailprg->qty_remain = $purchasedetailprg->qty_remain+$purchasedetailpr->qty;
            $purchasedetailprg->save();
        } else {
            $purchaserequest = Purchaserequest::where('id', $purchasedetailpr->purchaserequest)->first();
            $purchaserequest->qty_remain = $purchaserequest->qty_remain+$purchasedetailpr->qty;
            $purchaserequest->save();
        }

        $purchasedetailpr->delete();

        return redirect('/purchasedetailpr/'.$purchasedetailpr->purchasedetail.'/edit')->with('error', 'Purchase Request has been deleted');
    }
}
