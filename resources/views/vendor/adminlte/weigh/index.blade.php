@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Weighing - Shipping</h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="5%">No</th>
			          	<th width="15%">Split Proforma</th>
			          	<th width="15%">Port</th>
			          	<th width="15%">Date</th>
			          	<th width="20%">Stuffing</th>
			          	<th width="30%">Container / Seal</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@php ($no = 0)
			    	@foreach($schedulestuffs as $schedulestuff)
			    		@php ($no++)
			        	<tr>
				            <td>{{$no}}</td>
				            <td>{{$schedulestuff->fkSchedulebook->fkQuotationsplit->no_split}}</td>
				            <td>
				            	POL : {{$schedulestuff->fkSchedulebook->pol}} <br>
				            	POD : {{$schedulestuff->fkSchedulebook->pod}}
				            </td>
				            <td>
				            	ETD : {{$schedulestuff->fkSchedulebook->etd}} <br>
				            	ETA : {{$schedulestuff->fkSchedulebook->eta}}
				            </td>
				            <td>
				            	Date : {{$schedulestuff->date}} <br>
				            	PIC &nbsp;: {{$schedulestuff->person}} <br>
				            	Qty Fcl : {{$schedulestuff->qty_approve}}
				            </td>
							<td>
								@foreach($stuffs as $stuff)
									@if($stuff->schedulestuff == $schedulestuff->id)
										<ul>
											<li>
												{{$stuff->vehicle}} / {{$stuff->seal}} <br>
												Bruto &nbsp;&nbsp;: {{$stuff->container_bruto}} <br>
												Empty : {{$stuff->container_empty}} <br>
												Netto &nbsp;&nbsp;: {{$stuff->container_netto}}
											</li>
										</ul>
									@endif
								@endforeach
							</td>
				        </tr>
			        @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			          	<td class="noShow"></td>
			          	<td>Split Proforma</td>
			          	<td>Port</td>
			          	<td>Date</td>
			          	<td>Stuffing</td>
			          	<td>Container / Seal</td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	</div>
@stop