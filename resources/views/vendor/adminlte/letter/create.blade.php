@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #6b6e70 !important; font-weight: bold; color: white;">
			    		<h4 class="box-title">New Letter</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form id="letterFrm">
				          	@csrf
				          	@method('POST')
				          	<div class="form-group">
				          		<label for="menu">Menu</label>
				              	<input type="text" class="form-control" name="menu" value="{{ old('menu') }}" required>
				          	</div>
				          	<div class="form-group">
				          		<label for="company">Company</label>
				              	<input type="text" class="form-control" name="company" value="{{ old('company') }}" required>
				          	</div>
				          	<div class="form-group">
				              	<label for="code">Code</label>
				              	<input type="text" class="form-control" name="code" value="{{ old('code') }}" required>
				          	</div>
				          	<div class="form-group">
				          		<input type="checkbox" name="romawi" value="1"> Romawi &nbsp;&nbsp;&nbsp;
				              	<input type="checkbox" name="year" value="1"> Year
				          	</div>
				          	<button id="saveBtn" value="{{ route('letter.store') }}" class="btn btn-sm btn-success">Save</button>
				          	<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.letter.prepare_create());
    </script>
@stop