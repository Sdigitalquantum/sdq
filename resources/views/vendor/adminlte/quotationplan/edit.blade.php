@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>Split</a></li>
            <li class="active"><a href="#tab_plan" data-toggle="tab">Plan</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_plan">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Quotation Plan &nbsp;<a href="{{ url('quotationsplit/create', $quotationsplit->quotation) }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4>
			    	</div>
			    	
				  	<div class="box-body">
			      		<div class="row">
							<div class="col-md-4">
								<table class="table table-bordered table-hover nowrap" width="100%">
								    <tr style="background: #001F3F !important; font-weight: bold; text-align: center; color: white;">
							          	<th colspan="2">Split Proforma</th>
							        </tr>
								    <tr>
							            <td width="50%">{{$quotationsplit->no_split}}</td>
							            <td width="50%">Date : {{$quotationsplit->fkQuotation->date_sp}}</td>
							        </tr>
							        <tr>
							            <td>POL : {{$quotationsplit->fkPortl->name}} - {{$quotationsplit->fkPortl->fkCountry->name}}</td>
							            <td>POD : {{$quotationsplit->fkPortd->name}} - {{$quotationsplit->fkPortd->fkCountry->name}}</td>
							        </tr>
							        <tr>
							            <td>ETD : {{$quotationsplit->etd}}</td>
							            <td>ETA &nbsp;: {{$quotationsplit->eta}}</td>
							        </tr>
							  	</table>
							</div>

							<div class="col-md-8">
								<table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #00a65a !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								        	<th width="20%" style="text-align: center;">No. Batch</th>
								          	<th width="30%">Warehouse</th>
								          	<th width="30%">Supplier</th>
								          	<th width="10%" style="text-align: center;">Qty (Kg)</th>
								          	<th width="5%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($quotationplans as $quotationplan)
								    		<tr>
									            <td style="text-align: center;">{{$quotationplan->fkPlan->no_batch}}</td>
									            <td>
									            	{{$quotationplan->fkPlan->fkStockin->fkWarehouse->name}} <br>
									            	{{$quotationplan->fkPlan->fkStockin->fkProduct->name}}
									            </td>
									            <td>
									            	{{$quotationplan->fkPlan->fkStockin->fkSupplierdetail->name}} <br>
									            	Incoming Date : {{$quotationplan->fkPlan->fkStockin->date_in}}
									            </td>
									            <td style="text-align: right;">{{number_format($quotationplan->fkPlan->qty_plan, 0, ',' , '.')}} &nbsp;</td>
									        	<td>
													<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$quotationplan->id}}">Delete</button>

									            	<form action="{{ route('quotationplan.destroy', $quotationplan->id)}}" method="post">
									                  	@csrf
									                  	@method('DELETE')
									                  	<div class="modal fade bs-example-modal{{$quotationplan->id}}" tabindex="-1" role="dialog" aria-hidden="true">
						                                    <div class="modal-dialog modal-sm">
						                                        <div class="modal-content">
						                                            <div class="modal-header btn-danger">
						                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
						                                                </button>
						                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
						                                            </div>
						                                            <div class="modal-body">
						                                                Are you sure to delete this data?
						                                            </div>
						                                            <div class="modal-footer">
						                                                <button type="submit" class="btn btn-success">Yes</button>
						                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
						                                            </div>
						                                        </div>
						                                    </div>
						                                </div>
									                </form>
												</td>
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<table id="example" class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								        	<th width="5%">No</th>
								          	<th width="15%" style="text-align: center;">No. Batch</th>
								          	<th width="25%">Product</th>
								          	<th width="20%">Warehouse</th>
								          	<th width="20%">Supplier</th>
								          	<th width="10%" style="text-align: center;">Qty (Kg)</th>
								          	<th width="5%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@php ($array = [])
								    	@foreach($employeewarehouses as $employeewarehouse)
								    		@php ($array[] = $employeewarehouse->warehouse)
								    	@endforeach

								    	@foreach($quotationdetails as $quotationdetail)
									    	@php ($no = 0)
									    	@foreach($plans as $plan)
									    		@if($plan->quotationdetail == $quotationdetail->id && in_array($plan->fkStockin->warehouse, $array))
										    		@php ($no++)
										    		<tr>
											            <td style="text-align: center;">{{$no}}</td>
											            <td style="text-align: center;">{{$plan->no_batch}}</td>
											            <td>{{$plan->fkStockin->fkProduct->name}}</td>
											            <td>{{$plan->fkStockin->fkWarehouse->name}}</td>
											            <td>
											            	{{$plan->fkStockin->fkSupplierdetail->name}} <br>
											            	Incoming Date : {{$plan->fkStockin->date_in}}
											            </td>
											            <td style="text-align: right;">{{number_format($plan->qty_plan, 0, ',' , '.')}} &nbsp;</td>
														<td>
															<form method="post" action="{{ route('quotationplan.update', $quotationsplit->id) }}">
													      		@method('PATCH')
													      		@csrf
													      		<input type="hidden" name="quotationsplit" value="{{ $quotationsplit->id }}">
													      		<input type="hidden" name="plan" value="{{ $plan->id }}">
													      		<button type="submit" class="btn btn-xs btn-success">Choose</button>
												      		</form>
														</td>
											        </tr>
										        @endif
									        @endforeach
								        @endforeach
								    </tbody>

								    <tfoot>
								    	<tr>
								    		<td class="noShow"></td>
								    		<td>No. Batch</td>
								    		<td>Product</td>
								    		<td>Warehouse</td>
								    		<td>Supplier</td>
								    		<td>Qty (Kg)</td>
								    		<td class="noShow"></td>
							    		</tr>
								    </tfoot>
							  	</table>
							</div>
						</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop