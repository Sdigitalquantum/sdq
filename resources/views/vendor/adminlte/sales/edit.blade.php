@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Sales</h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<div class="row">
				  			<div class="col-md-6">
						      	<form id="salesFrm" > 
						      		@method('PATCH')
						      		@csrf
						      		<div class="form-group">
						              	<label for="account">Accounting</label>
						              	<select class="form-control select2" name="account" required>
						              		@foreach($accounts as $account)
											    @if ($sales->accounting == $account->id)
												    <option value="{{ $account->id }}" selected>{{ $account->code }} [{{ $account->name }}]</option>
												@else
												    <option value="{{ $account->id }}">{{ $account->code }} [{{ $account->name }}]</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						      		<div class="form-group">
						              	<label for="code">Code</label>
						              	<input type="text" class="form-control" name="code" value="{{ $sales->code }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ $sales->name }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="mobile">Mobile</label>
						              	<input type="number" class="form-control" name="mobile" value="{{ $sales->mobile }}" required>
						          	</div>
					          	</div>

					          	<div class="col-md-6">
						          	<div class="form-group">
						              	<label for="address">Address</label>
						              	<input type="text" class="form-control" name="address" value="{{ $sales->address }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="city">City</label>
						              	<select class="form-control select2" name="city" required>
						              		@foreach($citys as $city)
											    @if ($sales->city == $city->id)
												    <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
												@else
												    <option value="{{ $city->id }}">{{ $city->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="birthday">Birthday</label>
						              	<input type="date" class="form-control datepicker" name="birthday" value="{{ $sales->birthday }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="check">&nbsp;</label>
						          		@if($sales->status_group == 1)
						          			<input type="checkbox" name="status_group" value="1" checked> Status Group &nbsp;&nbsp;&nbsp;
						          		@else
						          			<input type="checkbox" name="status_group" value="1"> Status Group &nbsp;&nbsp;&nbsp;
						          		@endif

						          		@if($sales->status_record == 1)
						          			<input type="checkbox" name="status_record" value="1" checked> Status Record <br>
						          		@else
						          			<input type="checkbox" name="status_record" value="1"> Status Record <br>
						          		@endif
										<!-- <button type="submit" class="btn btn-sm btn-success">Update</button>
						          		<a href="{{ url('/sales') }}" class="btn btn-sm btn-danger">Cancel</a> -->
										<button id="updateBtn" value="{{ route('sales.update', $sales->id) }}" class="btn btn-sm btn-success">Update</button>
				         			 	<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
					          		</div>
						      	</form>
					      	</div>
				      	</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.sales.prepare_edit());
    </script>
@stop