@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Alias</a></li>
            <li class="active"><a href="#tab_show" data-toggle="tab">Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_show">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Show Product 
			    			&nbsp;<a href="javascript:window.print()" class="btn btn-xs btn-default" style="text-decoration: none;"><i class="glyphicon glyphicon-print"></i></a>
			    			&nbsp;<a href="{{ url('/product') }}" class="btn btn-xs btn-danger">Back</a>
			    		</h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<div id="printable">
				  			<div class="row">
				  				<center>
				  					<b>{{$company->name}}</b> <br>
					  				PRODUCT DETAIL <br>
					  				DOCUMENT INTERNAL <br>
					  				DATE : {{ date('d F Y', strtotime("now")) }} <br>
					  			</center><br>

				          		<div class="col-md-6">
						  			<div class="form-group">
						              	<label for="account">Accounting</label>
						              	@foreach($accounts as $account)
										    @if ($product->accounting == $account->id)
												<input type="text" class="form-control" name="account" value="{{ $account->code }} - {{ $account->name }}" disabled>
											@endif
										@endforeach
						          	</div>
							      	<div class="form-group">
						              	<label for="code">Code</label>
						              	<input type="text" class="form-control" name="code" value="{{ $product->code }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ $product->name }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="department">Department</label>
						              	@foreach($departments as $department)
										    @if ($product->department == $department->id)
												<input type="text" class="form-control" name="department" value="{{ $department->name }}" disabled>
											@endif
										@endforeach
						          	</div>
						          	<div class="form-group">
						              	<label for="variant">Variant</label>
						              	@foreach($variants as $variant)
										    @if ($product->variant == $variant->id)
											    <input type="text" class="form-control" name="variant" value="{{ $variant->name }}" disabled>
											@endif
										@endforeach
						          	</div>
					          	</div>

					          	<div class="col-md-6">
						          	<div class="form-group">
						              	<label for="merk">Merk</label>
						              	@foreach($merks as $merk)
										    @if ($product->merk == $merk->id)
											    <input type="text" class="form-control" name="merk" value="{{ $merk->name }}" disabled>
											@endif
										@endforeach
						          	</div>
						          	<div class="form-group">
						              	<label for="typesub">Type Sub</label>
						              	@foreach($typesubs as $typesub)
										    @if ($product->typesub == $typesub->id)
											    <input type="text" class="form-control" name="typesub" value="{{ $typesub->name }}" disabled>
											@endif
										@endforeach
						          	</div>
						          	<div class="form-group">
						              	<label for="size">Size</label>
						              	@foreach($sizes as $size)
										    @if ($product->size == $size->id)
											    <input type="text" class="form-control" name="size" value="{{ $size->name }}" disabled>
											@endif
										@endforeach
						          	</div>
						          	<div class="form-group">
						              	<label for="unit">Unit</label>
						              	@foreach($units as $unit)
										    @if ($product->unit == $unit->id)
											    <input type="text" class="form-control" name="unit" value="{{ $unit->name }}" disabled>
											@endif
										@endforeach
						          	</div>
						          	<div class="form-group">
						          		<label for="check">&nbsp;</label>
						          		@if($product->general == 1)
						          			<input type="checkbox" name="general" value="1" checked disabled> General &nbsp;&nbsp;&nbsp;
						          		@else
						          			<input type="checkbox" name="general" value="1" disabled> General &nbsp;&nbsp;&nbsp;
						          		@endif

						          		<br><label for="check">&nbsp;</label>
						          		@if($product->status_group == 1)
						          			<input type="checkbox" name="status_group" value="1" checked disabled> Status Group &nbsp;&nbsp;&nbsp;
						          		@else
						          			<input type="checkbox" name="status_group" value="1" disabled> Status Group &nbsp;&nbsp;&nbsp;
						          		@endif
						          		
						          		@if($product->status_record == 1)
						          			<input type="checkbox" name="status_record" value="1" checked disabled> Status Record
						          		@else
						          			<input type="checkbox" name="status_record" value="1" disabled> Status Record
						          		@endif
						          	</div>
					          	</div>
				          	</div>

							<table class="table table-bordered table-hover nowrap" width="100%">
							    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
							        <tr>
							          	<th width="30%">Customer</th>
							          	<th width="30%">Alias</th>
							          	<th width="20%">Status</th>
							        </tr>
							    </thead>

							    <tbody>
							    	@foreach($productaliass as $productalias)
							    		<tr>
								            <td>{{$productalias->fkCustomerdetail->name}}</td>
								            <td>{{$productalias->alias_name}}</td>
								            <td>@if($productalias->status == 1) <font color="blue">Active</font> @else <font color="red">Not Active</font> @endif</td>
								        </tr>
							        @endforeach
							    </tbody>
						  	</table>
					  	</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop