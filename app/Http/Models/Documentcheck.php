<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Documentcheck extends Model
{
    protected $fillable = [
	    'schedulestuff',
	    'quotationsplit',
	    'document',
	    'notice',
	    'status',
	    'created_user',
	    'updated_user'
  	];

  	public function fkSchedulestuff(){
	    return $this->belongsTo('\App\Http\Models\Schedulestuff', 'schedulestuff', 'id');
	}

	public function fkQuotationsplit(){
	    return $this->belongsTo('\App\Http\Models\Quotationsplit', 'quotationsplit', 'id');
	}

	public function fkDocument(){
	    return $this->belongsTo('\App\Http\Models\Document', 'document', 'id');
	}
}
