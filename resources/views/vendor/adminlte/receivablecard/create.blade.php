@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
          	<li class="active"><a href="#tab_input" data-toggle="tab">Cut Off</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_input">
				<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Input Total</a></h4>
			  		</div>
				  	
				  	<div class="box-body">
				  		<form method="post" action="{{ route('receivablecutoff.store') }}">
				          	@csrf
				          	<div class="row">
								<div class="col-md-6">
									<div class="form-group">
						              	<label for="customerdetail">Customer</label>
						              	<select class="form-control select2" name="customerdetail" required>
						              		<option value="{{ old('customerdetail') }}">Choose One</option>
					                    	@foreach($customerdetails as $customerdetail)
											    <option value="{{ $customerdetail->id }}">{{ $customerdetail->fkcustomer->code }} - {{ $customerdetail->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="year">Year</label>
						              	<input type="number" class="form-control" name="year" value="{{ old('year') }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="month">Month</label>
						              	<select class="form-control select2" name="month" required>
						              		<option value="{{ old('month') }}">Choose One</option>
						              		<option value="1">January</option>
						              		<option value="2">February</option>
						              		<option value="3">March</option>
						              		<option value="4">April</option>
						              		<option value="5">May</option>
						              		<option value="6">June</option>
						              		<option value="7">July</option>
						              		<option value="8">August</option>
						              		<option value="9">September</option>
						              		<option value="10">October</option>
						              		<option value="11">November</option>
						              		<option value="12">Desember</option>
					                	</select>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						              	<label for="debit">Total</label>
						              	<input type="number" class="form-control" name="debit" value="{{ old('debit') }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="kredit">Payment</label>
						              	<input type="number" class="form-control" name="kredit" value="0" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="button">&nbsp;</label> <br>
						              	<button type="submit" class="btn btn-sm btn-success">Save</button>
				          				<a href="{{ url('/receivablecutoff') }}" class="btn btn-sm btn-danger">Cancel</a>
						          	</div>
								</div>
							</div>
				      	</form>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop