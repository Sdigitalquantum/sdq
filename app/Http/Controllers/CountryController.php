<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Country;
use App\Http\Models\Employeeset;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        // $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        // $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        // $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $countrys = Country::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/country.index', compact('countrys','disable'));
        //,'employeeroots','employeemenus','employeesubs'
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        // $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        // $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        // $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $country = new Country();

        return view('vendor/adminlte/country.create', compact('country','disable'));
        //,'employeeroots','employeemenus','employeesubs'
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   $session_id = $request->session()->get('session_login');
    	$json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'name'    => 'unique:countries'
                ]);

                $no = Country::select('no_inc')->max('no_inc');
                if ( empty ($no) ) { $no_inc = 1; } 
                else { $no_inc = $no + 1; };
                $code = "C".sprintf("%05s", $no_inc);

                $country = new Country([
                    'no_inc'        => $no_inc,
                    'code'          => $code,
                    'name'          => $json->name,
                    'mobile'        => $json->mobile,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $country->save();
            };
        };
        return $this->jsonSuccess( $result );
	    // return redirect('/country')->with('success', 'Country has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        // $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        // $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        // $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $country = Country::find($id);

        return view('vendor/adminlte/country.edit', compact('country','disable')); 
        // ,'employeeroots','employeemenus','employeesubs'
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $country = Country::find($id);
                $name   = $country->name;
                $mobile = $country->mobile;
                
                if ($name != $json->name) {
                    $request->validate([
                        'name'      => 'unique:countries'
                    ]);
                };
                
                $country->name          = $json->name;
                $country->mobile        = $json->mobile;
                $country->updated_user  = $session_id;
                $result = $country->save();
            };
        };
        return $this->jsonSuccess( $result );
        // return redirect('/country')->with('success', 'Country has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $country = Country::find($id);
        $status = $country->status;

        if ($status == 1) 
        {   $country->status       = 0;
            $country->updated_user = $session_id;
	        $result = $country->save();

		    // return redirect('/country')->with('error', 'Country has been deleted');
        } 
        elseif ($status == 0) 
        {   $country->status       = 1;
            $country->updated_user = $session_id;
	        $result = $country->save();

		    // return redirect('/country')->with('success', 'Country has been activated');
        };

        return $this->jsonSuccess( $result );
    }
}
