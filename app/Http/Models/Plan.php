<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = [
	    'quotationdetail',
	    'stockin',
	    'no_inc',
	    'no_batch',
	    'qty_plan',
	    'created_user',
	    'updated_user'
  	];

  	public function fkQuotationdetail(){
	    return $this->belongsTo('\App\Http\Models\Quotationdetail', 'quotationdetail', 'id');
	}

	public function fkStockin(){
	    return $this->belongsTo('\App\Http\Models\Stockin', 'stockin', 'id');
	}
}
