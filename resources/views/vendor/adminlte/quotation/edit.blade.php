@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
            <li class="disabled"><a>Payment</a></li>
            <li class="disabled"><a>Charges</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Quotation <b>{{ $quotation->no_sp }}</b>
			    			&nbsp;<a href="{{ route('quotationdetail.show', $id) }}" class="btn btn-xs btn-primary" style="text-decoration: none;">Detail</a>
			    			&nbsp;<a href="{{ route('quotationpayment.show', $id) }}" class="btn btn-xs btn-primary" style="text-decoration: none;">Payment</a>

			    			@if($quotation->payment_term == 100)
			    				&nbsp;<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal">Release</button>

				            	<form action="{{ url('quotation/release', $id) }}" method="post">
				                  	@csrf
				                  	@method('DELETE')
				                  	<div class="modal fade bs-example-modal" tabindex="-1" role="dialog" aria-hidden="true">
	                                    <div class="modal-dialog modal-sm">
	                                        <div class="modal-content">
	                                            <div class="modal-header btn-success">
	                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
	                                                </button>
	                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Release</h4>
	                                            </div>
	                                            <div class="modal-body">
	                                                Are you sure to release this data?
	                                            </div>
	                                            <div class="modal-footer">
	                                                <button type="submit" class="btn btn-success">Yes</button>
	                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
				                </form>
			    			@endif
			    		</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('quotation.update', $quotation->id) }}">
				      		@method('PATCH')
				      		@csrf
				      		<input type="hidden" id="id_url" value="{{ $quotation->id }}">
				      		<div class="row">
    							<div class="col-md-6">
    								<div class="form-group">
						          		<label for="date_sp">Date</label>
						              	<input type="text" class="form-control datepicker" name="date_sp" value="{{ $quotation->date_sp }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="status_flow">Flow</label> <br>
						          		@if($quotation->status_flow == 1)
						          			<input type="radio" name="status_flow" value="1" checked required> Export &nbsp;&nbsp;&nbsp;
						          			<input type="radio" name="status_flow" value="2"> Local &nbsp;&nbsp;&nbsp;
						              		<input type="radio" name="status_flow" value="3"> Internal
						          		@elseif($quotation->status_flow == 2)
						          			<input type="radio" name="status_flow" value="1" required> Export &nbsp;&nbsp;&nbsp;
						          			<input type="radio" name="status_flow" value="2" checked> Local &nbsp;&nbsp;&nbsp;
						              		<input type="radio" name="status_flow" value="3"> Internal
					              		@elseif($quotation->status_flow == 3)
						          			<input type="radio" name="status_flow" value="1" required> Export &nbsp;&nbsp;&nbsp;
						          			<input type="radio" name="status_flow" value="2"> Local &nbsp;&nbsp;&nbsp;
						              		<input type="radio" name="status_flow" value="3" checked> Internal
						          		@endif
						          	</div>
						          	<div class="form-group">
						              	<label for="sales">Sales</label>
						              	<select class="form-control select2" name="sales" required>
						              		<option value="{{ old('sales') }}">Choose One</option>
					                    	@foreach($saless as $sales)
											    @if ($quotation->sales == $sales->id)
												    <option value="{{ $sales->id }}" selected>{{ $sales->code }} - {{ $sales->name }}</option>
												@else
												    <option value="{{ $sales->id }}">{{ $sales->code }} - {{ $sales->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="customerdetail">Customer</label> &nbsp;&nbsp;&nbsp;
						              	@if($quotation->tax == 1)
						          			<input type="checkbox" name="tax" value="1" checked> Tax
						          		@else
						          			<input type="checkbox" name="tax" value="1"> Tax
						          		@endif
						              	<select class="form-control select2" name="customerdetail" id="ajaxcustomerdetail" required>
						              		<option value="{{ old('customerdetail') }}">Choose One</option>
					                    	@foreach($customerdetails as $customerdetail)
											    @if ($quotation->customerdetail == $customerdetail->id)
												    <option value="{{ $customerdetail->id }}" selected>{{ $customerdetail->name }}</option>
												@else
												    <option value="{{ $customerdetail->id }}">{{ $customerdetail->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="no_poc">PO Customer</label>
						              	<input type="text" class="form-control" name="no_poc" value="{{ $quotation->no_poc }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="kurs">Kurs</label>
						              	<input type="number" class="form-control" name="kurs" value="{{ $quotation->kurs }}" step=".01">
						          	</div>
						          	<div class="form-group">
						              	<label for="currency">Currency</label>
						              	<select class="form-control select2" name="currency" required>
						              		@foreach($currencys as $currency)
											    @if ($quotation->currency == $currency->id)
												    <option value="{{ $currency->id }}" selected>{{ $currency->code }}</option>
												@else
												    <option value="{{ $currency->id }}">{{ $currency->code }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="disc_type">Discount</label> <br>
						          		@if($quotation->disc_type == 0)
						          			<input type="radio" name="disc_type" value="1"> % &nbsp;&nbsp;&nbsp;
						              		<input type="radio" name="disc_type" value="2"> Rp / $ &nbsp;&nbsp;&nbsp;
						          		@elseif($quotation->disc_type == 1)
						          			<input type="radio" name="disc_type" value="1" checked> % &nbsp;&nbsp;&nbsp;
						              		<input type="radio" name="disc_type" value="2"> Rp / $ &nbsp;&nbsp;&nbsp;
					              		@elseif($quotation->disc_type == 2)
						          			<input type="radio" name="disc_type" value="1"> % &nbsp;&nbsp;&nbsp;
						              		<input type="radio" name="disc_type" value="2" checked> Rp / $ &nbsp;&nbsp;&nbsp;
						          		@endif
						              	<input type="number" class="form-control" name="disc_value" value="{{ $quotation->disc_value }}" step=".01">
						          	</div>
						          	<div class="form-group">
						              	<label for="paymentterm">Payment Term</label>
						              	<select class="form-control select2" name="paymentterm" required>
						              		<option value="{{ old('paymentterm') }}">Choose One</option>
					                    	@foreach($paymentterms as $paymentterm)
											    @if ($quotation->paymentterm == $paymentterm->id)
												    <option value="{{ $paymentterm->id }}" selected>{{ $paymentterm->code }}</option>
												@else
												    <option value="{{ $paymentterm->id }}">{{ $paymentterm->code }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="companyaccount">Company Bank</label>
						              	<select class="form-control select2" name="companyaccount" required>
						              		<option value="{{ old('companyaccount') }}">Choose One</option>
					                    	@foreach($companyaccounts as $companyaccount)
											    @if ($quotation->companyaccount == $companyaccount->id)
												    <option value="{{ $companyaccount->id }}" selected>({{ $companyaccount->fkCurrency->code }}) {{ $companyaccount->account_no }} - {{ $companyaccount->fkBank->name }}</option>
												@else
												    <option value="{{ $companyaccount->id }}">({{ $companyaccount->fkCurrency->code }}) {{ $companyaccount->account_no }} - {{ $companyaccount->fkBank->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						              	<label for="container">Vehicle</label>
						              	<select class="form-control select2" name="container" required>
						              		<option value="{{ old('container') }}">Choose One</option>
					                    	@foreach($containers as $container)
											    @if ($quotation->container == $container->id)
												    @if($container->size == 0)
						                    			<option value="{{ $container->id }}" selected>{{ $container->name }}</option>
												    @else
												    	<option value="{{ $container->id }}" selected>{{ $container->size }} - {{ $container->name }}</option>
											    	@endif
												@else
												    @if($container->size == 0)
						                    			<option value="{{ $container->id }}">{{ $container->name }}</option>
												    @else
												    	<option value="{{ $container->id }}">{{ $container->size }} - {{ $container->name }}</option>
											    	@endif
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="vessel">Vessel</label>
						              	<input type="text" class="form-control" name="vessel" value="{{ $quotation->vessel }}">
						          	</div>
									<div class="form-group">
						              	<label for="pol">Port Loading</label>
						              	<select class="form-control select2" name="pol" required>
						              		<option value="{{ old('pol') }}">Choose One</option>
					                    	@foreach($ports as $port)
											    @if ($quotation->pol == $port->id)
												    <option value="{{ $port->id }}" selected>{{ $port->name }} - {{ $port->fkCountry->name }}</option>
												@else
												    <option value="{{ $port->id }}">{{ $port->name }} - {{ $port->fkCountry->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="pod">Port Discharge</label>
						              	<select class="form-control select2" name="pod" required>
						              		<option value="{{ old('pod') }}">Choose One</option>
					                    	@foreach($ports as $port)
											    @if ($quotation->pod == $port->id)
												    <option value="{{ $port->id }}" selected>{{ $port->name }} - {{ $port->fkCountry->name }}</option>
												@else
												    <option value="{{ $port->id }}">{{ $port->name }} - {{ $port->fkCountry->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="etd">Time Departure</label>
						              	<input type="text" class="form-control datepicker" name="etd" value="{{ $quotation->etd }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="eta">Time Arrival</label>
						              	<input type="text" class="form-control datepicker" name="eta" value="{{ $quotation->eta }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_gross">Gross Weight (Kg)</label>
						              	<input type="number" class="form-control" name="qty_gross" value="{{ $quotation->qty_gross }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_cbm">CBM</label>
						              	<input type="number" class="form-control" name="qty_cbm" value="{{ $quotation->qty_cbm }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="notice">Note</label>
						          		<textarea class="form-control" name="notice" id="ajaxnotice" maxlength="150" rows="3">{{ $quotation->notice }}</textarea>
						          	</div>
						          	<div class="form-group">
						              	<button type="submit" class="btn btn-sm btn-success">Update</button>
		          						<a href="{{ url('/quotation') }}" class="btn btn-sm btn-danger">Cancel</a>
						          	</div>
								</div>
							</div>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop