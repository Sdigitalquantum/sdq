<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Orderdetail extends Model
{
    protected $fillable = [
	    'order',
	    'product',
	    'qty_order'
  	];

  	public function fkOrder(){
	    return $this->belongsTo('\App\Http\Models\Order', 'order', 'id');
	}

	public function fkProduct(){
	    return $this->belongsTo('\App\Http\Models\Product', 'product', 'id');
	}
}
