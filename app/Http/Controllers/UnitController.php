<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Unit;
use App\Http\Models\Employeeset;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $units = Unit::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/unit.index', compact('units','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $unit = new Unit();
        return view('vendor/adminlte/unit.create', compact('unit','disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	$json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'name' => 'unique:units'
                ]);

                $unit = new Unit([
                    'name'          => $json->name,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $unit->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $unit = Unit::find($id);

        return view('vendor/adminlte/unit.edit', compact('unit','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $unit = Unit::find($id);
                $name = $unit->name;

                if ($name != $json->name) {
                    $request->validate([
                        'name' => 'unique:units'
                    ]);
                }
                
                $unit->name             = $json->name;
                $unit->updated_user     = $session_id;
                $result = $unit->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $unit = Unit::find($id);
        $status = $unit->status;

        if ($status == 1) {
        	$unit->status       = 0;
            $unit->updated_user = $session_id;
	        $result = $unit->save();

        } elseif ($status == 0) {
        	$unit->status       = 1;
            $unit->updated_user = $session_id;
	        $result = $unit->save();

        };
        return $this->jsonSuccess( $result );
    }
}
