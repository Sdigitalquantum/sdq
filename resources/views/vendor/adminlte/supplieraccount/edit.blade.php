@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
            <li class="active"><a href="#tab_detail" data-toggle="tab">Account</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_detail">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Supplier Account</h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<div class="row">
				  			<div class="col-md-3">
				  				<form method="post" action="{{ route('supplieraccount.update', $supplieraccount->id) }}">
						      		@method('PATCH')
						      		@csrf
						      		<input type="hidden" class="form-control" name="supplier" value="{{ $supplieraccount->supplier }}">
						      		<div class="form-group">
						              	<label for="currency">Currency</label>
						              	<select class="form-control select2" name="currency" required>
						              		@foreach($currencys as $currency)
											    @if ($supplieraccount->currency == $currency->id)
												    <option value="{{ $currency->id }}" selected>{{ $currency->code }} - {{ $currency->name }}</option>
												@else
												    <option value="{{ $currency->id }}">{{ $currency->code }} - {{ $currency->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="bank">Bank</label>
						              	<select class="form-control select2" name="bank" required>
						              		@foreach($banks as $bank)
											    @if ($supplieraccount->bank == $bank->id)
												    <option value="{{ $bank->id }}" selected>{{ $bank->alias }} - {{ $bank->name }}</option>
												@else
												    <option value="{{ $bank->id }}">{{ $bank->alias }} - {{ $bank->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="account_no">No. Rek</label>
						              	<input type="number" class="form-control" name="account_no" value="{{ $supplieraccount->account_no }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="account_swift">Swift Code</label>
						              	<input type="text" class="form-control" name="account_swift" value="{{ $supplieraccount->account_swift }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="account_name">Name Rek</label>
						              	<input type="text" class="form-control" name="account_name" value="{{ $supplieraccount->account_name }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="alias">Alias</label>
						              	<input type="text" class="form-control" name="alias" value="{{ $supplieraccount->alias }}">
						          	</div>
						          	<button type="submit" class="btn btn-sm btn-success">Update</button>
					          		<a href="{{ url('/supplieraccount/'.$supplieraccount->supplier) }}" class="btn btn-sm btn-danger">Cancel</a>
						      	</form>
			  				</div>

			  				<div class="col-md-9">
			  					<br><table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="20%">Bank</th>
								          	<th width="25%">Account</th>
								          	<th width="20%">Name</th>
								          	<th width="20%">Alias</th>
								          	<th width="15%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($supplieraccounts as $supplieraccount)
								    		<tr>
									            <td>{{$supplieraccount->fkBank->name}}</td>
									            <td>
									            	{{$supplieraccount->fkCurrency->code}} : {{$supplieraccount->account_no}} <br>
									            	Swift : {{$supplieraccount->account_swift}}
									            </td>
									            <td>{{$supplieraccount->account_name}}</td>
									            <td>{{$supplieraccount->alias}}</td>
												<td>
									            	@if($supplieraccount->status == 1) <a href="{{ route('supplieraccount.edit',$supplieraccount->id)}}" class="btn btn-xs btn-warning">Edit</a>
									            	@else <font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
									            	@endif
									            </td>
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
			  				</div>
		  				</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop