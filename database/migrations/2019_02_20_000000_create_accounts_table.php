<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('currency')->unsigned();
            $table->integer('group')->unsigned();
            $table->integer('type')->unsigned();
            $table->integer('code')->unique();
            $table->string('name');
            $table->integer('level1')->nullable();
            $table->integer('level2')->nullable();
            $table->integer('level3')->nullable();
            $table->integer('level4')->nullable();
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->foreign('currency')->references('id')->on('currencies');
            $table->foreign('group')->references('id')->on('accountgroups');
            $table->foreign('type')->references('id')->on('accounttypes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
