@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Sub Menu</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('menusub.update', $menusub->id) }}">
				      		@method('PATCH')
				      		@csrf
				          	<div class="row">
								<div class="col-md-6">
									<div class="form-group">
						          		<label for="root">Menu</label>
						              	<select name="menu" class="form-control select2" required>
						              		@foreach($menus as $menu)
						              			@if ($menu->id == $menusub->menu)
												    <option value="{{ $menu->id }}" selected>{{ $menu->name }}</option>
												@else
												    <option value="{{ $menu->id }}">{{ $menu->name }}</option>
												@endif
											@endforeach
										</select>
						          	</div>
									<div class="form-group">
						              	<label for="nomor">Nomor</label>
						              	<input type="number" class="form-control" name="nomor" value="{{ $menusub->nomor }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ $menusub->name }}" required>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						              	<label for="url">URL</label>
						              	<input type="text" class="form-control" name="url" value="{{ $menusub->url }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="icon_color">Icon Color</label>
						              	<input type="text" class="form-control" name="icon_color" value="{{ $menusub->icon_color }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="button">&nbsp;</label> <br>
						              	<button type="submit" class="btn btn-sm btn-success">Update</button>
						          		<a href="{{ url('/menusub/create/'.$menusub->menu) }}" class="btn btn-sm btn-danger">Cancel</a>
						          	</div>
								</div>
							</div>
				      	</form>

				      	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="25%">Name</th>
						          	<th width="25%">URL</th>
						          	<th width="20%">Icon</th>
						          	<th width="20%">Icon Color</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@foreach($menusubs as $menusub)
						    		<tr>
							            <td>{{$menusub->nomor}}</td>
							            <td>{{$menusub->name}}</td>
							            <td>{{$menusub->url}}</td>
							            <td>{{$menusub->icon}}</td>
							            <td>{{$menusub->icon_color}}</td>

							            @if($menusub->status == 1)
								            <td>
								            	<a href="{{ route('menusub.edit',$menusub->id)}}" class="btn btn-xs btn-warning">Edit</a>
								            </td>
							            @else
							            	<td>
							            		<font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
							            	</td>
							            @endif
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Name</td>
						          	<td>URL</td>
						          	<td>Icon</td>
						          	<td>Icon Color</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop