<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Stuff;
use App\Http\Models\Stufforder;
use App\Http\Models\Schedulestuff;
use App\Http\Models\Quotation;
use App\Http\Models\Quotationdetail;
use App\Http\Models\Quotationplan;
use App\Http\Models\Quotationpayment;
use App\Http\Models\Quotationsplit;
use App\Http\Models\Plan;
use App\Http\Models\Stockin;
use App\Http\Models\Stockcard;
use App\Http\Models\Stockload;
use App\Http\Models\Letter;
use App\Http\Models\Company;
use App\Http\Models\Employeeset;

class StuffnonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $schedulestuffs = Quotationsplit::where([['status','<>',0], ['status_load', 1]])->orderBy('quotation','desc')->get();

        return view('vendor/adminlte/stuffnon.index', compact('schedulestuffs','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $schedulestuff = Quotationsplit::find($id);
        $stuffs = Stuff::where('quotationsplit', $id)->orderBy('created_at','desc')->get();

        return view('vendor/adminlte/stuffnon.create', compact('schedulestuff','stuffs','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        $stuff = Stuff::find($request->get('stuff'));
        $stuff->seal              = $request->get('seal');
        $stuff->updated_user      = $session_id;
        $stuff->save();

        $schedulestuff = Quotationsplit::where('id', $stuff->quotationsplit)->first();
        $schedulestuff->status_stuff = 1;
        $schedulestuff->save();

        return redirect('/stuffnon/create/'.$stuff->quotationsplit)->with('success', 'Stuffing has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $schedulestuff = Quotationsplit::find($id);
        $stuffs = Stuff::where('quotationsplit', $id)->orderBy('created_at','desc')->get();

        $stufforder = Stufforder::where('quotationsplit', $id)->first();
        $stufforder->qty_print = $stufforder->qty_print+1;
        $stufforder->save();

        $quotationsplit = Quotationsplit::find($id);
        $quotation = Quotation::where('id', $quotationsplit->quotation)->first();
        $quotationpayments = Quotationpayment::where([['quotation', $quotation->id], ['status', 1]])->orderBy('payment','desc')->get();

        $quotationplan = Quotationplan::where('quotationsplit', $quotationsplit->id)->first();
        $plan = Plan::where('id', $quotationplan->plan)->first();
        $quotationdetails = Quotationdetail::where('id', $plan->quotationdetail)->get();
        $company = Company::where('status', 1)->first();

        return view('vendor/adminlte/stuffnon.show', compact('schedulestuff','stuffs','stufforder','quotation','quotationpayments','quotationdetails','company','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $stufforder = Stufforder::find($id);

        if (!empty($request->get('check_weigh'))) {
            $request->validate([
                'check_weigh' => 'numeric|min:0|not_in:0'
            ]);

            if ($request->get('check_weigh') > $request->get('total')) {
                return redirect()->back()->withErrors('Weigh Check greater than Total Qty');
            } else {
                $stufforder->check_quality  = $request->get('check_quality');
                $stufforder->check_weigh    = $request->get('check_weigh');
                $stufforder->save();

                return redirect('/stuffconfirmnon')->with('success', 'Customer Check has been added');
            }
        } else {
            $stufforder->status_confirm = 1;
            $stufforder->save();

            $schedulestuff = Quotationsplit::where('id', $stufforder->quotationsplit)->first();
            $schedulestuff->status_confirm = 1;
            $schedulestuff->save();

            $quotationplans = Quotationplan::where('quotationsplit', $stufforder->quotationsplit)->get();
            foreach ($quotationplans as $quotationplan) {
                $plans = Plan::where('id', $quotationplan->plan)->get();
                foreach ($plans as $plan) {
                    $stockins = Stockin::where('id', $plan->stockin)->get();
                    foreach ($stockins as $stockin) {
                        $stockin->qty_out       = $stockin->qty_out+$stockin->qty_transit;
                        $stockin->qty_transit   = 0;
                        $stockin->date_out      = $schedulestuff->etd;
                        $stockin->noref_out     = $schedulestuff->no_split;
                        $stockin->status_out    = 1;
                        $stockin->save();

                        $stockcard = Stockcard::where([['product', $stockin->product], ['year', date('Y', strtotime($stockin->date_in))], ['month', date('m', strtotime($stockin->date_in))]])->first();
                        $stockcard->qty_out = $stockcard->qty_out+$stockin->qty_out;
                        $stockcard->save();
                    }
                }
            }

            return redirect('/stuffconfirmnon')->with('success', 'Delivery Order has been confirmed');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $schedulestuffs = Quotationsplit::where([['status','<>',0], ['status_stuff', 1]])->orderBy('quotation','desc')->get();

        return view('vendor/adminlte/stuffnon.list', compact('schedulestuffs','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $no = Stufforder::select('no_inc')->whereYear('created_at', date('Y'))->max('no_inc');
        if (empty($no)) {
            $no_inc = 1;
        } else {
            $no_inc = $no + 1;
        }

        $letter = Letter::where('menu', 'Delivery Order')->first();
        if (empty($letter->company)) {
            $company = "";
        } else {
            $company = $letter->company."/";
        }

        if (empty($letter->code)) {
            $code = "";
        } else {
            $code = $letter->code;
        }

        if ($letter->romawi == 0) {
            $romawi = "";
        } else {
            if (date("m") == 1) {
                $romawi = "I/";
            } elseif (date("m") == 2) {
                $romawi = "II/";
            } elseif (date("m") == 3) {
                $romawi = "III/";
            } elseif (date("m") == 4) {
                $romawi = "IV/";
            } elseif (date("m") == 5) {
                $romawi = "V/";
            } elseif (date("m") == 6) {
                $romawi = "VI/";
            } elseif (date("m") == 7) {
                $romawi = "VII/";
            } elseif (date("m") == 8) {
                $romawi = "VIII/";
            } elseif (date("m") == 9) {
                $romawi = "IX/";
            } elseif (date("m") == 10) {
                $romawi = "X/";
            } elseif (date("m") == 11) {
                $romawi = "XI/";
            } elseif (date("m") == 12) {
                $romawi = "XII/";
            }
        }

        if ($letter->year == 0) {
            $year = "";
        } else {
            $year = date("Y");
        }

        $no_letter = $company.$code.sprintf("%05s", $no_inc)."/".$romawi.$year;

        $stufforder = new Stufforder([
            'schedulestuff'     => 0,
            'quotationsplit'    => $id,
            'no_inc'            => $no_inc,
            'no_letter'         => $no_letter,
            'check_quality'     => '',
            'check_weigh'       => 0,
            'notice'            => '',
            'created_user'      => $session_id
        ]);
        $stufforder->save();

        $schedulestuff = Quotationsplit::find($id);
        $schedulestuff->status_delivery = 1;
        $schedulestuff->save();

        return redirect('/stuffordernon')->with('success', 'Delivery Order has been created');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function confirm(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $stufforders = Stufforder::where([['status', 1], ['status_reroute', 0], ['quotationsplit','<>',0]])->orderBy('quotationsplit','desc')->get();
        $stuffs = Stuff::where([['status', 1], ['status_load', 1]])->get();

        return view('vendor/adminlte/stuffnon.confirm', compact('stufforders','stuffs','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reroute(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $stufforders = Stufforder::where([['status_confirm', 0], ['quotationsplit','<>',0]])->orderBy('quotationsplit','desc')->get();

        return view('vendor/adminlte/stuffnon.reroute', compact('stufforders','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel(Request $request, $id)
    {
        $stufforder = Stufforder::find($id);
        $stufforder->status         = 2;
        $stufforder->status_reroute = 1;
        $stufforder->notice         = $request->get('notice');
        $stufforder->save();

        $schedulestuff = Quotationsplit::where('id', $stufforder->quotationsplit)->first();
        $schedulestuff->status          = 2;
        $schedulestuff->status_reroute  = 1;
        $schedulestuff->save();

        $quotation = Quotation::where('id', $schedulestuff->quotation)->first();
        $quotation->status_close = 0;
        $quotation->save();

        $stuffs = Stuff::where('quotationsplit', $stufforder->quotationsplit)->get();
        foreach ($stuffs as $stuff) {
            $stuff->status = 2;
            $stuff->save();

            $stockloads = Stockload::where('stuff', $stuff->id)->get();
            foreach ($stockloads as $stockload) {
                $stockload->delete();
            }
        }

        $quotationplans = Quotationplan::where('quotationsplit', $stufforder->quotationsplit)->get();
        foreach ($quotationplans as $quotationplan) {
            $plans = Plan::where('id', $quotationplan->plan)->get();
            foreach ($plans as $plan) {
                $stockins = Stockin::where('id', $plan->stockin)->get();
                foreach ($stockins as $stockin) {
                    $stockin->qty_available = $stockin->qty_available+$stockin->qty_transit;
                    $stockin->qty_transit   = 0;
                    $stockin->save();

                    $stockcard = Stockcard::where([['product', $stockin->product], ['year', date('Y', strtotime($stockin->date_in))], ['month', date('m', strtotime($stockin->date_in))]])->first();
                    $stockcard->qty_out = $stockcard->qty_out-$stockin->qty_available;
                    $stockcard->save();
                }

                $quotationdetails = Quotationdetail::where('id', $plan->quotationdetail)->get();
                foreach ($quotationdetails as $quotationdetail) {
                    $quotationdetail->qty_amount    = $quotationdetail->qty_kg;
                    $quotationdetail->status_plan   = 0;
                    $quotationdetail->qty_plan      = 0;
                    $quotationdetail->status_book   = 0;
                    $quotationdetail->qty_book      = 0;
                    $quotationdetail->status_other  = 0;
                    $quotationdetail->qty_other     = 0;
                    $quotationdetail->save();
                }
                
                $quotationplan->delete();
                $plan->delete();
            }
        }

        return redirect('/stuffreroutenon')->with('success', 'Delivery Order has been re-route');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function check(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $stufforders = Stufforder::where([['status', 1], ['status_reroute', 0], ['quotationsplit','<>',0], ['status_confirm', 1]])->orderBy('quotationsplit','desc')->get();
        $stuffs = Stuff::where([['status', 1], ['status_load', 1]])->get();

        return view('vendor/adminlte/stuffnon.check', compact('stufforders','stuffs','disable','employeeroots','employeemenus','employeesubs'));
    }
}
