<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Paymentterm extends Model
{
    protected $fillable = [
	    'code',
	    'name',
	    'created_user',
	    'updated_user'
  	];
}
