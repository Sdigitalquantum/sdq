<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Schedulestuff extends Model
{
    protected $fillable = [
	    'schedulebook',
	    'date',
	    'person',
	    'notice',
	    'reason',
	    'vessel',
	    'freight',
	    'qty_fcl',
	    'created_user',
	    'updated_user',
	    'sent_at',
	    'sent_user',
	    'packed_at',
	    'packed_user'
  	];

  	public function fkSchedulebook(){
	    return $this->belongsTo('\App\Http\Models\Schedulebook', 'schedulebook', 'id');
	}
}
