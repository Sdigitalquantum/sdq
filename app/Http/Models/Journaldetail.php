<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Journaldetail extends Model
{
    protected $fillable = [
	    'journal',
	    'accounting',
	    'debit',
	    'status_group',
	    'status_record',
	    'created_user',
	    'updated_user'
  	];

  	public function fkJournal(){
	    return $this->belongsTo('\App\Http\Models\Journal', 'journal', 'id');
	}

  	public function fkAccount(){
	    return $this->belongsTo('\App\Http\Models\Account', 'accounting', 'id');
	}
}
