<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Mailcontact extends Model
{
    protected $fillable = [
	    'name',
	    'email',
	    'phone',
	    'subject',
	    'message'
  	];
}
