<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Menuroot;
use App\Http\Models\Menu;
use App\Http\Models\Menusub;
use App\Http\Models\Employee;
use App\Http\Models\Employeeset;

class EmployeesetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $employees = Employee::orderBy('created_at','desc')->get();
        $employeesets = Employeeset::select('employee','menuroot')->groupBy('employee','menuroot')->orderBy('menuroot','asc')->get();

        return view('vendor/adminlte/employeeset.index', compact('employees','employeesets','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $employeesets = Employeeset::select('employee','menuroot')->groupBy('employee','menuroot')->orderBy('menuroot','asc')->get();

        $employee = Employee::find($id);
        $menuroots = Menuroot::where('status', 1)->orderBy('nomor','asc')->get();
        $menus = Menu::where('status', 1)->orderBy('nomor','asc')->get();
        $menusubs = Menusub::where('status', 1)->orderBy('nomor','asc')->get();

        $employeerootaccs = Employeeset::select('menuroot')->where('employee', $id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenuaccs = Employeeset::select('menuroot','menu')->where([['employee', $id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubaccs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        return view('vendor/adminlte/employeeset.edit', compact('employee','menuroots','menus','menusubs','employeesets','employeerootaccs','employeemenuaccs','employeesubaccs','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        if ($request->get('menu') == 0 && $request->get('menusub') == 0) {
            $cekroot = Employeeset::where([['menuroot', $request->get('menuroot')], ['employee', $request->get('employee')]])->first();

            if (!empty($cekroot)) {
                return redirect()->back()->withErrors('Access Menu already exist');
            } else {
                $employeeroot = new Employeeset([
                    'employee'      => $request->get('employee'),
                    'menuroot'      => $request->get('menuroot'),
                    'nomorroot'     => $request->get('nomorroot'),
                    'menu'          => 0,
                    'nomormenu'     => 0,
                    'menusub'       => 0,
                    'nomorsub'      => 0,
                    'created_user'  => $session_id
                ]);
                $employeeroot->save();

                $menus = Menu::where('menuroot', $request->get('menuroot'))->get();
                foreach ($menus as $menu) {
                    $employeemenu = new Employeeset([
                        'employee'      => $request->get('employee'),
                        'menuroot'      => $request->get('menuroot'),
                        'nomorroot'     => $request->get('nomorroot'),
                        'menu'          => $menu->id,
                        'nomormenu'     => $menu->nomor,
                        'menusub'       => 0,
                        'nomorsub'      => 0,
                        'created_user'  => $session_id
                    ]);
                    $employeemenu->save();

                    $menusubs = Menusub::where('menu', $menu->id)->get();
                    foreach ($menusubs as $menusub) {
                        $employeesub = new Employeeset([
                            'employee'      => $request->get('employee'),
                            'menuroot'      => $request->get('menuroot'),
                            'nomorroot'     => $request->get('nomorroot'),
                            'menu'          => $menu->id,
                            'nomormenu'     => $menu->nomor,
                            'menusub'       => $menusub->id,
                            'nomorsub'      => $menusub->nomor,
                            'created_user'  => $session_id
                        ]);
                        $employeesub->save();
                    }
                }

                return redirect('/employeeset/'.$request->get('employee').'/edit')->with('success', 'Access Menu has been added');
            }
        } elseif ($request->get('menusub') == 0) {
            $cekmenu = Employeeset::where([['menu', $request->get('menu')], ['employee', $request->get('employee')]])->first();

            if (!empty($cekmenu)) {
                return redirect()->back()->withErrors('Access Menu already exist');
            } else {
                $cekroot = Employeeset::where([['menuroot', $request->get('menuroot')], ['employee', $request->get('employee')]])->first();

                if (!empty($cekroot)) {
                    $employeemenu = new Employeeset([
                        'employee'      => $request->get('employee'),
                        'menuroot'      => $request->get('menuroot'),
                        'nomorroot'     => $request->get('nomorroot'),
                        'menu'          => $request->get('menu'),
                        'nomormenu'     => $request->get('nomormenu'),
                        'menusub'       => 0,
                        'nomorsub'      => 0,
                        'created_user'  => $session_id
                    ]);
                    $employeemenu->save();

                    $menusubs = Menusub::where('menu', $request->get('menu'))->get();
                    foreach ($menusubs as $menusub) {
                        $employeesub = new Employeeset([
                            'employee'      => $request->get('employee'),
                            'menuroot'      => $request->get('menuroot'),
                            'nomorroot'     => $request->get('nomorroot'),
                            'menu'          => $request->get('menu'),
                            'nomormenu'     => $request->get('nomormenu'),
                            'menusub'       => $menusub->id,
                            'nomorsub'      => $menusub->nomor,
                            'created_user'  => $session_id
                        ]);
                        $employeesub->save();
                    }

                    return redirect('/employeeset/'.$request->get('employee').'/edit')->with('success', 'Access Menu has been added');
                } else {
                    $employeeroot = new Employeeset([
                        'employee'      => $request->get('employee'),
                        'menuroot'      => $request->get('menuroot'),
                        'nomorroot'     => $request->get('nomorroot'),
                        'menu'          => 0,
                        'nomormenu'     => 0,
                        'menusub'       => 0,
                        'nomorsub'      => 0,
                        'created_user'  => $session_id
                    ]);
                    $employeeroot->save();

                    $employeemenu = new Employeeset([
                        'employee'      => $request->get('employee'),
                        'menuroot'      => $request->get('menuroot'),
                        'nomorroot'     => $request->get('nomorroot'),
                        'menu'          => $request->get('menu'),
                        'nomormenu'     => $request->get('nomormenu'),
                        'menusub'       => 0,
                        'nomorsub'      => 0,
                        'created_user'  => $session_id
                    ]);
                    $employeemenu->save();

                    $menusubs = Menusub::where('menu', $request->get('menu'))->get();
                    foreach ($menusubs as $menusub) {
                        $employeesub = new Employeeset([
                            'employee'      => $request->get('employee'),
                            'menuroot'      => $request->get('menuroot'),
                            'nomorroot'     => $request->get('nomorroot'),
                            'menu'          => $request->get('menu'),
                            'nomormenu'     => $request->get('nomormenu'),
                            'menusub'       => $menusub->id,
                            'nomorsub'      => $menusub->nomor,
                            'created_user'  => $session_id
                        ]);
                        $employeesub->save();
                    }

                    return redirect('/employeeset/'.$request->get('employee').'/edit')->with('success', 'Access Menu has been added');
                }
            }
        } else {
            $ceksub = Employeeset::where([['menusub', $request->get('menusub')], ['employee', $request->get('employee')]])->first();

            if (!empty($ceksub)) {
                return redirect()->back()->withErrors('Access Menu already exist');
            } else {
                $cekmenu = Employeeset::where([['menu', $request->get('menu')], ['employee', $request->get('employee')]])->first();
                
                if (!empty($cekmenu)) {
                    $employeesub = new Employeeset([
                        'employee'      => $request->get('employee'),
                        'menuroot'      => $request->get('menuroot'),
                        'nomorroot'     => $request->get('nomorroot'),
                        'menu'          => $request->get('menu'),
                        'nomormenu'     => $request->get('nomormenu'),
                        'menusub'       => $request->get('menusub'),
                        'nomorsub'      => $request->get('nomorsub'),
                        'created_user'  => $session_id
                    ]);
                    $employeesub->save();

                    return redirect('/employeeset/'.$request->get('employee').'/edit')->with('success', 'Access Menu has been added');
                } else {
                    $cekroot = Employeeset::where([['menuroot', $request->get('menuroot')], ['employee', $request->get('employee')]])->first();

                    if (!empty($cekroot)) {
                        $employeemenu = new Employeeset([
                            'employee'      => $request->get('employee'),
                            'menuroot'      => $request->get('menuroot'),
                            'nomorroot'     => $request->get('nomorroot'),
                            'menu'          => $request->get('menu'),
                            'nomormenu'     => $request->get('nomormenu'),
                            'menusub'       => 0,
                            'nomorsub'      => 0,
                            'created_user'  => $session_id
                        ]);
                        $employeemenu->save();

                        $employeesub = new Employeeset([
                            'employee'      => $request->get('employee'),
                            'menuroot'      => $request->get('menuroot'),
                            'nomorroot'     => $request->get('nomorroot'),
                            'menu'          => $request->get('menu'),
                            'nomormenu'     => $request->get('nomormenu'),
                            'menusub'       => $request->get('menusub'),
                            'nomorsub'      => $request->get('nomorsub'),
                            'created_user'  => $session_id
                        ]);
                        $employeesub->save();

                        return redirect('/employeeset/'.$request->get('employee').'/edit')->with('success', 'Access Menu has been added');
                    } else {
                        $employeeroot = new Employeeset([
                            'employee'      => $request->get('employee'),
                            'menuroot'      => $request->get('menuroot'),
                            'nomorroot'     => $request->get('nomorroot'),
                            'menu'          => 0,
                            'nomormenu'     => 0,
                            'menusub'       => 0,
                            'nomorsub'      => 0,
                            'created_user'  => $session_id
                        ]);
                        $employeeroot->save();

                        $employeemenu = new Employeeset([
                            'employee'      => $request->get('employee'),
                            'menuroot'      => $request->get('menuroot'),
                            'nomorroot'     => $request->get('nomorroot'),
                            'menu'          => $request->get('menu'),
                            'nomormenu'     => $request->get('nomormenu'),
                            'menusub'       => 0,
                            'nomorsub'      => 0,
                            'created_user'  => $session_id
                        ]);
                        $employeemenu->save();

                        $employeesub = new Employeeset([
                            'employee'      => $request->get('employee'),
                            'menuroot'      => $request->get('menuroot'),
                            'nomorroot'     => $request->get('nomorroot'),
                            'menu'          => $request->get('menu'),
                            'nomormenu'     => $request->get('nomormenu'),
                            'menusub'       => $request->get('menusub'),
                            'nomorsub'      => $request->get('nomorsub'),
                            'created_user'  => $session_id
                        ]);
                        $employeesub->save();

                        return redirect('/employeeset/'.$request->get('employee').'/edit')->with('success', 'Access Menu has been added');
                    }
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->get('menu') == 0 && $request->get('menusub') == 0) {
            $employeeroots = Employeeset::where([['employee', $request->get('employee')], ['menuroot', $id]])->get();
            foreach ($employeeroots as $employeeroot) {
                $employeeroot->delete();
            }

            return redirect('/employeeset/'.$request->get('employee').'/edit')->with('error', 'Access Menu has been deleted');
        } elseif ($request->get('menusub') == 0) {
            $employeemenus = Employeeset::where([['employee', $request->get('employee')], ['menu', $id]])->get();
            foreach ($employeemenus as $employeemenu) {
                $employeemenu->delete();
            }

            return redirect('/employeeset/'.$request->get('employee').'/edit')->with('error', 'Access Menu has been deleted');
        } else {
            $employeesub = Employeeset::where([['employee', $request->get('employee')], ['menusub', $id]])->first();
            $employeesub->delete();

            return redirect('/employeeset/'.$request->get('employee').'/edit')->with('error', 'Access Menu has been deleted');
        }
    }
}
