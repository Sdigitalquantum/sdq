@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_reschedule" data-toggle="tab">Re-Schedule</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_reschedule">
				<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Stuffing Re-Schedule</h4>
			  		</div>
				  	
				  	<div class="box-body">
				  		<form method="post" action="{{ route('stuffreschedule.store') }}">
				          	@csrf
				          	<div class="form-group">
				              	<label for="schedulestuff">Split Proforma</label>
				              	<select class="form-control select2" name="schedulestuff" required>
				              		<option value="">Choose One</option>
			                    	@foreach($schedulestuffs as $schedulestuff)
									    <option value="{{ $schedulestuff->id }}">{{ $schedulestuff->fkSchedulebook->fkQuotationsplit->no_split }}</option>
									@endforeach
			                	</select>
				          	</div>
				          	<div class="form-group">
				          		<label for="reason">Reason</label> <br>
				              	<input type="radio" name="reason" value="1" required> Change Schedule &nbsp;&nbsp;&nbsp;
				              	<input type="radio" name="reason" value="2"> Cancel Stuffing
				          	</div>
				          	<div class="form-group">
				          		<label for="date_change">Date</label>
				              	<input type="text" class="form-control datepicker" name="date_change" required>
				          	</div>
				          	<div class="form-group">
				          		<label for="notice">Note</label>
				          		<textarea class="form-control" name="notice" rows="6" maxlength="150" required></textarea>
				          	</div>
				          	<button type="submit" class="btn btn-sm btn-success">Save</button>
				          	<a href="{{ url('/stuffreschedule') }}" class="btn btn-sm btn-danger">Cancel</a>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop