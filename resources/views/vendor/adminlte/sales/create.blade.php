@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">New Sales</h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<div class="row">
				  			<div class="col-md-6">
						      	 <form id="countryFrm" > <!--method="post" action="{{ route('sales.store') }}"> -->
						          	@csrf
						          	<div class="form-group">
						              	<label for="account">Accounting</label>
						              	<select class="form-control select2" name="account" required>
						              		<option value="{{ old('account') }}">Choose One</option>
					                    	@foreach($accounts as $account)
											    <option value="{{ $account->id }}">{{ $account->code }} [{{ $account->name }}]</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="code">Code</label>
						              	<input type="text" class="form-control" name="code" value="{{ old('code') }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="mobile">Mobile</label>
						              	<input type="number" class="form-control" name="mobile" value="{{ old('mobile') }}" required>
						          	</div>
				          	</div>

				          	<div class="col-md-6">
						          	<div class="form-group">
						              	<label for="address">Address</label>
						              	<input type="text" class="form-control" name="address" value="{{ old('address') }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="city">City</label>
						              	<select class="form-control select2" name="city" required>
						              		<option value="{{ old('city') }}">Choose One</option>
					                    	@foreach($citys as $city)
											    <option value="{{ $city->id }}">{{ $city->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="birthday">Birthday</label>
						              	<input type="date" class="form-control datepicker" name="birthday" value="{{ old('birthday') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="check">&nbsp;</label>
						              	<input type="checkbox" name="status_group" value="1"> Status Group &nbsp;&nbsp;&nbsp;
						              	<input type="checkbox" name="status_record" value="1"> Status Record <br>
					              	
						          		<!-- <button type="submit" class="btn btn-sm btn-success">Save</button>
						          		<a href="{{ url('/sales') }}" class="btn btn-sm btn-danger">Cancel</a> -->
										<button id="saveBtn" value="{{ route('sales.store') }}" class="btn btn-sm btn-success">Save</button>
							          	<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
					          		</div>
						      	</form>
					      	</div>
				      	</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.sales.prepare_create());
    </script>
@stop
