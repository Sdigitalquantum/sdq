@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('sdq.skin', 'blue') . '.min.css')}} ">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/pnotify.custom.min.css') }}">
    <style type="text/css">
        #printable { visibility: visible; }

        @media print
        {
            body { visibility: hidden; }
            .noprint { visibility: hidden; }
            #printable { visibility: visible; margin-top: -19%; size: 21cm 29.7cm; }
        }

        hr { height: 5px; border: 1; color: #333; background-color: #333; }
        pre { border: 0; background-color: transparent; margin-left: -10px; margin-top: -10px; }
        .border { border: solid 1px #000; }
    </style>

    @stack('css')
    @yield('css')
@stop

@section('body_class', 'skin-' . config('sdq.skin', 'blue') . ' sidebar-mini ' . (config('sdq.layout') ? [
    'boxed' => 'layout-boxed',
    'fixed' => 'fixed',
    'top-nav' => 'layout-top-nav'
][config('sdq.layout')] : '') . (config('sdq.collapse_sidebar') ? ' sidebar-collapse ' : ''))

@section('body')
    <div class="wrapper">
        <header class="main-header">
            @if(config('sdq.layout') == 'top-nav')
            <nav class="navbar navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <a href="{{ url(config('sdq.dashboard_url', 'home')) }}" class="navbar-brand">
                            {!! config('sdq.logo', '<b>S.D.Q</b>') !!}
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            <!-- @each('adminlte::partials.menu-item-top-nav', $adminlte->menu(), 'item') -->
                        </ul>
                    </div>
            @else
            
            @if($disable == 0)
                    <a href="{{ url(config('sdq.dashboard_url', 'home')) }}" class="logo">
                        <span class="logo-mini">
                            <img src="{{ (config('sdq.logo_mini', '<b>/\</b>')) }}" width="35">
                        </span>
                        <span class="logo-lg" style="font-size:12pt;">
                            <img src="{{ (config('sdq.logo', '<b>S.D.Q</b>')) }}" width="50">
                            <!-- <b>&nbsp;&nbsp;&nbsp;S.D.Q</b> -->
                        </span>
                    </a>
            @elseif($disable == 1)
                    <a class="logo">
                        <span class="logo-mini">
                            <img src="{{ (config('sdq.logo_mini', '<b>/\</b>')) }}" width="35">
                        </span>
                        <span class="logo-lg" style="font-size:12pt;">
                            <img src="{{ (config('sdq.logo', '<b>S.D.Q</b>')) }}" width="50">
                            <!-- <b>&nbsp;&nbsp;&nbsp;S.D.Q</b> -->
                        </span>
                    </a>
            @endif
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">{{ trans('adminlte::adminlte.toggle_navigation') }}</span>
                </a>
            @endif
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="/">
                                <b>SINERGI DIGITAL QUANTUM </b>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li>
                            @if($disable == 0)
                                <a href="{{ url('login') }}">
                                    <i class="fa fa-fw fa-power-off"></i> Logout
                                </a>
                            @elseif($disable == 1)
                                <a>
                                    <i class="fa fa-fw fa-power-off"></i> Logout
                                </a>
                            @endif
                        </li>
                    </ul>
                </div>
                @if(config('sdq.layout') == 'top-nav')
                </div>
                @endif
            </nav>
        </header>

        @if(config('sdq.layout') != 'top-nav')
        <aside class="main-sidebar">
            <section class="sidebar">
                <ul class="sidebar-menu" data-widget="tree">
                    <!-- @each('adminlte::partials.menu-item', $adminlte->menu(), 'item') -->

                    @if($disable == 0)
                        @foreach($employeeroots as $employeeroot)
                            @if($employeeroot->fkMenuroot->url == '#')
                                <li class="treeview">
                            @else
                                <li class="">
                            @endif
                                <a href="{{ $employeeroot->fkMenuroot->url }}">
                                    <i class="fa fa-fw fa-{{$employeeroot->fkMenuroot->icon}} text-{{$employeeroot->fkMenuroot->icon_color}}"></i>
                                    <span>{{$employeeroot->fkMenuroot->name}}</span>

                                    @if($employeeroot->fkMenuroot->url == '#')
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    @endif
                                </a>
                                
                                <ul class="treeview-menu">
                                    @foreach($employeemenus as $employeemenu)
                                        @if($employeeroot->menuroot == $employeemenu->menuroot)
                                            @if($employeemenu->fkMenu->url == '#')
                                                <li class="treeview">
                                            @else
                                                <li class="">
                                            @endif
                                                <a href="{{ $employeemenu->fkMenu->url }}">
                                                    <i class="fa fa-fw fa-chevron-right text-{{$employeemenu->fkMenu->icon_color}}"></i>
                                                    <span>{{$employeemenu->fkMenu->name}}</span>

                                                    @if($employeemenu->fkMenu->url == '#')
                                                        <span class="pull-right-container">
                                                            <i class="fa fa-angle-left pull-right"></i>
                                                        </span>
                                                    @endif
                                                </a>
                                                
                                                <ul class="treeview-menu">
                                                    @foreach($employeesubs as $employeesub)
                                                        @if($employeemenu->menu == $employeesub->menu)
                                                            <li class="">
                                                                <a href="#" onclick="sdq.core.load('{{$employeesub->fkMenusub->url}}');return false;">
                                                                    <i class="fa fa-fw fa-circle-o text-{{$employeesub->fkMenusub->icon_color}}"></i>
                                                                    <span>{{$employeesub->fkMenusub->name}}</span>
                                                                </a>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                        @endforeach
                    @elseif($disable == 1)
                        @foreach($employeeroots as $employeeroot)
                            <li>
                                <a>
                                    <i class="fa fa-fw fa-{{$employeeroot->fkMenuroot->icon}} text-{{$employeeroot->fkMenuroot->icon_color}}"></i>
                                    <span>{{$employeeroot->fkMenuroot->name}}</span>

                                    @if($employeeroot->fkMenuroot->url == '#')
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    @endif
                                </a>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </section>
        </aside>
        @endif

        <div class="content-wrapper">
            @if(config('sdq.layout') == 'top-nav')
            <div class="container">
            @endif

            <section class="content">
                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}  
                    </div>
                @elseif(session()->get('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}  
                    </div>
                @elseif ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @yield('content')
            </section>

            @if(config('sdq.layout') == 'top-nav')
            </div>
            @endif
        </div>
    </div>
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/pnotify.custom.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/jquery.slimscroll.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            var url = window.location;

            $('ul.sidebar-menu a').filter(function() {
                return this.href == url;
            }).parent().addClass('active');

            $('ul.treeview-menu a').filter(function() {
                return this.href == url;
            }).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');

            $('#example tfoot td:not(.noShow)').each( function (){
                $(this).html( '<input type="text" placeholder="Search" class="form-control" style="background: #605ca8 !important; color: white; width: 100%;">' );
            });

            var table = $('#example').DataTable({
                responsive: true,
                dom: "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-2'l><'col-sm-3'i><'col-sm-7'p>>"
            });

            table.columns().every( function ( colIdx ){
                var that = this;
         
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                });
            });

            $(".alert").alert();
            window.setTimeout(function () {
                $(".alert").alert('close');
            }, 10000);

            $('.select2').select2();

            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd'
            });

            $('#ajaxcustomerdetail').on('change', function() {
                var url;
                var id_url = $('#id_url').val();
                var id = $(this).val();

                if (id_url === undefined) {
                    url = "0/ajax/"+id;
                } else {
                    url = "ajax/"+id;
                }

                if (id) {
                    $.ajax({
                        type: "get",
                        url: url,
                        dataType: "json",
                        success:function(data) {
                            $('#ajaxnotice').html(data.notice);
                        }
                    });
                } else {
                    $('#ajaxnotice').html('');
                }
            });

            $('#ajaxproduct').on('change', function() {
                var url;
                var id_url = $('#id_url').val();
                var id = $(this).val();

                if (id_url === undefined) {
                    url = "0/ajax/"+id;
                } else {
                    url = "ajax/"+id;
                }

                if (id) {
                    $.ajax({
                        type: "get",
                        url: url,
                        dataType: "json",
                        success:function(data) {
                            if (data) {
                                $('#ajaxprice').html(data.price);
                            } else {
                                $('#ajaxprice').html('0');
                            }
                        }
                    });
                } else {
                    $('#ajaxprice').html('0');
                }
            });

            var notice = $('#notice').val();
            if (notice) {
                new PNotify({
                    title: 'Note',
                    text: notice,
                    styling: 'bootstrap3',
                    icon: 'glyphicon glyphicon-comment',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
            }
        });

        // define namespace
        var sdq = {};

        sdq.namespace = function() 
        {   var ln = arguments.length, i, value, split, x, xln, parts, object;
            
            for (i = 0; i < ln; i++) 
            {   value = arguments[i];
                parts = value.split(".");
                object = window[parts[0]] = Object(window[parts[0]]);
              
                for (x = 1, xln = parts.length; x < xln; x++) 
                {   object = object[parts[x]] = Object(object[parts[x]]);   };
            };
            return object;
        };
        sdq.namespace('sdq.core');
        sdq.core = function()
        {   // do NOT access DOM from here; elements don't exist yet
            // execute at the first time
            // private variables
            this.image_url;
            this.init_mode;
            this.target;
            this.action;
            this.Grid;
            this.Columns;
            this.Records;
            this.DataStore;
            this.Searchs;
            this.menuPanel;
            this.viewport;

            // private functions
            //**********************
            // public space
            //**********************
            return {
                // execute at the very last time
                // public properties, e.g. strings to translate
                // public methods
                server_date: new Date().getFullYear(),
                start_date : new Date(this.server_date, 0, 1),
                end_date   : new Date(this.server_date, 11, 31),
                page_limit : 75,
                page_start : 0,
                sid        : '{{ csrf_token() }}',
                initialize: function()
                {   //console.log('initialize core');
                    this.prepare_component();
                    this.build_layout();
                    this.finalize_comp_and_layout();
                },
                prepare_component : function()
                {   },
                build_layout: function()
                {   },
                finalize_comp_and_layout : function()
                {   },
                load : function(the_href)
                {   $('.content').load(the_href);   },
                submitForm : function(the_form, the_url, the_redirect_url)
                {   the_form = $('#'+the_form);
                    the_form.submit(function (event) {
                        event.preventDefault();                        
                    });
                    var the_parameters = the_form.serializeArray();
                    
                    var v_param = {};
                    $.each(the_parameters, function() 
                    {   console.log(this.name +' = '+ this.value);
                        v_param[this.name] = this.value; 
                    });
                    the_parameters = [];
                    the_parameters.push(v_param);
                    the_type = the_parameters[0]._method;
                    
                    this.ajax(
                        the_url,            //the_url, 
                        {                   //the_parameters,
                          '_token' : this.sid,
                          json : JSON.stringify(the_parameters)
                        },                  
                        the_type,             //the_type, 
                        function(response)  //fn_success
                        {   sdq.core.load(the_redirect_url);    },
                        function(response)  //fn_fail, 
                        {   console.log("FAILED");
                            console.log(response);
                        },
                        null                //fn_always
                    );
                    return false;
                },
                ajax : function(the_url, the_parameters, the_type, fn_success, fn_fail, fn_always)
                {   $dataType = "json";
                    $contentType = "application/x-www-form-urlencoded; charset=UTF-8";
                    if (the_type == 'POST') 
                    {   $dataType = "text";
                        // $contentType = "application/json; charset=utf-8"; 
                    };
                    $.ajax({
                        // The URL for the request
                        url: the_url,
                        // The data parameter in array to send (will be converted to a query string)
                        data: the_parameters,
                        // Whether this is a POST or GET request
                        type: the_type, 
                        // The type of data we expect back, xml, json, script, text, html
                        dataType : $dataType,
                        contentType: $contentType,
                    })
                    // Code to run if the request succeeds (is done);
                    // The response is passed to the function
                    .done(fn_success)
                    // Code to run if the request fails; the raw request and
                    // status codes are passed to the function
                    .fail(fn_fail)
                    // Code to run regardless of success or failure;
                    .always(fn_always);
                },
                setSearchBar : function()
                {   console.log('setSearchBar');
                    $('#example tfoot td:not(.noShow)').each( function ()
                    {   console.log(this);
                        $(this).html( '<input type="text" placeholder="Search" class="form-control" style="background: #474B4F !important; color: white; width: 100%;">' );
                    });
                    console.log('tfoot done');
                    var table = $('#example').DataTable(
                    {
                        responsive: true,
                        dom: "<'row'<'col-sm-12'tr>>" +
                            "<'row'<'col-sm-2'l><'col-sm-3'i><'col-sm-7'p>>"
                    });
                    console.log('DataTable');
                    table.columns().every( function ( colIdx ){
                        var that = this;
                 
                        $( 'input', this.footer() ).on( 'keyup change', function () {
                            if ( that.search() !== this.value ) {
                                that
                                    .search( this.value )
                                    .draw();
                            }
                        });
                    });
                    console.log('columns');
                },
            };
        }();
        $(document).ready(sdq.core.initialize());
    </script>
    
    @stack('js')
    @yield('js')
@stop
