@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="active"><a href="#tab_account" data-toggle="tab">Account</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_account">
            	<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Company Account &nbsp;<a href="{{ url('company') }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4>
			  		</div>
				  	
				  	<div class="box-body">
				  		<form method="post" action="{{ route('companyaccount.store') }}">
				          	@csrf
				          	<input type="hidden" class="form-control" name="company" value="{{ $id }}">
					  		<div class="row">
					  			<div class="col-md-6">
						          	<div class="form-group">
						              	<label for="currency">Currency</label>
						              	<select class="form-control select2" name="currency" required>
						              		<option value="{{ old('currency') }}">Choose One</option>
					                    	@foreach($currencys as $currency)
											    <option value="{{ $currency->id }}">{{ $currency->code }} - {{ $currency->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="bank">Bank</label>
						              	<select class="form-control select2" name="bank" required>
						              		<option value="{{ old('bank') }}">Choose One</option>
					                    	@foreach($banks as $bank)
											    <option value="{{ $bank->id }}">{{ $bank->alias }} - {{ $bank->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="account_no">No. Rek</label>
						              	<input type="number" class="form-control" name="account_no" value="{{ old('account_no') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="account_swift">Swift Code</label>
						              	<input type="text" class="form-control" name="account_swift" value="{{ old('account_swift') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="account_name">Name Rek</label>
						              	<input type="text" class="form-control" name="account_name" value="{{ old('account_name') }}" required>
						          	</div>
				  				</div>

				  				<div class="col-md-6">
				  					<div class="form-group">
						          		<label for="account_address">Address Rek</label>
						              	<input type="text" class="form-control" name="account_address" value="{{ old('account_address') }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="city">City</label>
						              	<select class="form-control select2" name="city" required>
						              		<option value="{{ old('city') }}">Choose One</option>
					                    	@foreach($citys as $city)
											    <option value="{{ $city->id }}">{{ $city->name }} - {{ $city->fkCountry->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="alias">Alias</label>
						              	<input type="text" class="form-control" name="alias" value="{{ old('alias') }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="flow">Flow</label> <br>
						              	<input type="checkbox" name="export" value="1"> Export <br>
						              	<input type="checkbox" name="local" value="1"> Local <br>
						              	<input type="checkbox" name="internal" value="1"> Internal
						          	</div>
						          	<button type="submit" class="btn btn-sm btn-success">Save</button>
			  					</div>
		  					</div>
	  					</form>
		  					
	  					<table class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="20%">Bank</th>
						          	<th width="25%">Account</th>
						          	<th width="20%">Name</th>
						          	<th width="20%">Alias</th>
						          	<th width="15%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@foreach($companyaccounts as $companyaccount)
						    		<tr>
							            <td>
							            	{{$companyaccount->fkBank->name}} <br>
							            	@if($companyaccount->export == 1) Export <i class="glyphicon glyphicon-ok"></i> @else Export <i class="glyphicon glyphicon-remove"></i> @endif <br>
							            	@if($companyaccount->local == 1) Local <i class="glyphicon glyphicon-ok"></i> @else Local <i class="glyphicon glyphicon-remove"></i> @endif <br>
							            	@if($companyaccount->internal == 1) Internal <i class="glyphicon glyphicon-ok"></i> @else Internal <i class="glyphicon glyphicon-remove"></i> @endif
							            </td>
							            <td>
							            	{{$companyaccount->fkCurrency->code}} : {{$companyaccount->account_no}} <br>
							            	Swift : {{$companyaccount->account_swift}}
							            </td>
							            <td>
							            	{{$companyaccount->account_name}} <br>
							            	{{$companyaccount->account_address}} <br>
							            	{{$companyaccount->fkCity->name}} - {{$companyaccount->fkCity->fkCountry->name}}
							            </td>
							            <td>{{$companyaccount->alias}}</td>

							            @if($companyaccount->status == 1)
								            <td>
								            	<a href="{{ route('companyaccount.edit',$companyaccount->id)}}" class="btn btn-xs btn-warning">Edit</a> &nbsp;
								            	
								            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$companyaccount->id}}">Delete</button>

								            	<form action="{{ route('companyaccount.destroy', $companyaccount->id)}}" method="post">
								                  	@csrf
								                  	@method('DELETE')
								                  	<div class="modal fade bs-example-modal{{$companyaccount->id}}" tabindex="-1" role="dialog" aria-hidden="true">
					                                    <div class="modal-dialog modal-sm">
					                                        <div class="modal-content">
					                                            <div class="modal-header btn-danger">
					                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
					                                                </button>
					                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
					                                            </div>
					                                            <div class="modal-body">
					                                                Are you sure to delete this data?
					                                            </div>
					                                            <div class="modal-footer">
					                                                <button type="submit" class="btn btn-success">Yes</button>
					                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
					                                            </div>
					                                        </div>
					                                    </div>
					                                </div>
								                </form>
								            </td>
							            @else
							            	<td>
							            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$companyaccount->id}}">Activated</button>

							            		<form action="{{ route('companyaccount.destroy', $companyaccount->id)}}" method="post">
								                  	@csrf
								                  	@method('DELETE')
								                  	<div class="modal fade bs-example-modal{{$companyaccount->id}}" tabindex="-1" role="dialog" aria-hidden="true">
					                                    <div class="modal-dialog modal-sm">
					                                        <div class="modal-content">
					                                            <div class="modal-header btn-success">
					                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
					                                                </button>
					                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
					                                            </div>
					                                            <div class="modal-body">
					                                                Are you sure to active this data?
					                                            </div>
					                                            <div class="modal-footer">
					                                                <button type="submit" class="btn btn-success">Yes</button>
					                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
					                                            </div>
					                                        </div>
					                                    </div>
					                                </div>
								                </form>
							            	</td>
							            @endif
							        </tr>
						        @endforeach
						    </tbody>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop