<?php

namespace App\Exports;

use App\Http\Models\Receivable;
use Maatwebsite\Excel\Concerns\FromCollection;

class ReceivablesExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Receivable::select('no_sp','code_customer','name_customer','code_product','name_product','date_sp','date_sj','qty','price','total','payment','adjustment')->get();
    }
}
