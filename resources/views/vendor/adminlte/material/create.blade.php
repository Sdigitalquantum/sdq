@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Raw Material</a></li>
            <li class="disabled"><a>Excess</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">New Bill of Material</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	 <form id="materialFrm"> <!--method="post" action="{{ route('material.store') }}"> -->
						    @method('POST')
				          	@csrf
				          	<div class="row">
				          		<div class="col-md-12">
				          			<div class="form-group">
						              	<label for="product">Product</label>
						              	<select class="form-control select2" name="product" required>
						              		<option value="{{ old('product') }}">Choose One</option>
					                    	@foreach($products as $product)
											    <option value="{{ $product->id }}">{{ $product->code }} - {{ $product->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="check">&nbsp;</label>
						              	<input type="checkbox" name="status_group" value="1"> Status Group &nbsp;&nbsp;&nbsp;
						              	<input type="checkbox" name="status_record" value="1"> Status Record
					              	</div>
						          	<div class="form-group">
									  	<button id="saveBtn" value="{{ route('material.store') }}" class="btn btn-sm btn-success">Save</button>
				          				<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
						          		<!-- <button type="submit" class="btn btn-sm btn-success">Save</button>
						          		<a href="{{ url('/material') }}" class="btn btn-sm btn-danger">Cancel</a> -->
					          		</div>
			          			</div>
		          			</div>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('content_js')
	<script>
    	$(document).ready(sdq.material.prepare_create());
    </script>
@stop