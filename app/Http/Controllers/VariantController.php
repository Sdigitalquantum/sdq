<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Variant;
use App\Http\Models\Employeeset;

class VariantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $variants = Variant::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/variant.index', compact('variants','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $variant = new Variant();

        return view('vendor/adminlte/variant.create', compact('variant','disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	$json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'name' => 'unique:variants'
                ]);

                $variant = new Variant([
                    'name'          => $json->name,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $variant->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $variant = Variant::find($id);

        return view('vendor/adminlte/variant.edit', compact('variant','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $variant = Variant::find($id);
                $name = $variant->name;

                if ($name != $json->name) {
                    $request->validate([
                        'name' => 'unique:variants'
                    ]);
                }
                
                $variant->name          = $json->name;
                $variant->updated_user  = $session_id;
                $result = $variant->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $variant = Variant::find($id);
        $status = $variant->status;

        if ($status == 1) {
        	$variant->status       = 0;
            $variant->updated_user = $session_id;
	        $result = $variant->save();

        } elseif ($status == 0) {
        	$variant->status       = 1;
            $variant->updated_user = $session_id;
	        $result = $variant->save();
        };
        return $this->jsonSuccess( $result );
    }
}
