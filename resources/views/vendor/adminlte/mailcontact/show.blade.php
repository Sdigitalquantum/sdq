@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Mailbox Contact</h4>
  		</div>
	  	
	  	<div class="box-body">
	  		<div class="row">
	  			<div class="col-md-2">
		          	<div class="box box-solid">
		            	<div class="box-body no-padding">
		              		<ul class="nav nav-pills nav-stacked">
		               		 	<li>
		               		 		<a href="{{ url('mailcontact') }}"><i class="fa fa-inbox"></i> Inbox
		               		 			@if($inbox != 0)
				                  			<span class="label label-success pull-right">{{ $inbox }}</span>
			                  			@endif
		                  			</a>
		                  		</li>
		              		</ul>
		            	</div>
		          	</div>
		        </div>

		        <div class="col-md-10">
		        	<div class="box box-success">
		        		<div class="box-header with-border">
			              	<h3 class="box-title">Read Mail</h3>
			            </div>

			            <div class="box-body no-padding">
			              	<div class="mailbox-read-info">
				                <h3>{{ $mailcontact->subject }}</h3>
				                <h5>From &nbsp;&nbsp;: {{ $mailcontact->email }}
				                  	<span class="mailbox-read-time pull-right">{{ date('d F Y - H:m:s', strtotime($mailcontact->created_at)) }}</span>
			                  	</h5>
			                  	<h5>Phone : {{ $mailcontact->phone }}</h5>
			                  	<h5>Name &nbsp;: {{ $mailcontact->name }}</h5>
			              	</div>
			              	
			              	<div class="mailbox-read-message">
				                <pre>{{ $mailcontact->message }}</pre>
				            </div>
			            </div>
	        		</div>
	        	</div>
  			</div>
	  	</div>
	</div>
@stop