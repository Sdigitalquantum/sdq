<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;

class DebtcardsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $results = DB::table('debtcards')
            ->join('supplierdetails', 'debtcards.supplierdetail', '=', 'supplierdetails.id')
            ->join('suppliers', 'supplierdetails.supplier', '=', 'suppliers.id')
            ->select('suppliers.code','supplierdetails.name','debtcards.year','debtcards.month','debtcards.debit','debtcards.kredit')
            ->get();

        return $results;
    }
}
