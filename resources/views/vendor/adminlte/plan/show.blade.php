@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>Plan</a></li>
            <li class="disabled"><a>Booking</a></li>
            <li class="active"><a href="#tab_show" data-toggle="tab">Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_show">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Show Delivery Plan
			    		 	&nbsp;<a href="javascript:window.print()" class="btn btn-xs btn-default" style="text-decoration: none;"><i class="glyphicon glyphicon-print"></i></a>
			    			&nbsp;<a href="{{ url('/plan') }}" class="btn btn-xs btn-danger">Back</a>
			    		 </h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<div id="printable">
				  			@if(count($purchaserequests) != 0)
					      		<div class="row">
					      			<center>
					  					<b>{{$company->name}}</b> <br>
						  				DELIVERY PLAN <br>
						  				DOCUMENT INTERNAL <br>
						  				DATE : {{ date('d F Y', strtotime("now")) }} <br>
						  			</center><br>

									<div class="col-md-12">
										<table class="table table-bordered table-hover nowrap" width="100%">
										    <tr style="background: #001F3F !important; font-weight: bold; text-align: center; color: white;">
									          	<th colspan="4">Quotation</th>
									        </tr>
										    <tr>
									            <td width="25%">{{$quotationdetail->fkQuotation->no_sp}}</td>
									            <td width="25%">Date : {{$quotationdetail->fkQuotation->date_sp}}</td>
									            <td width="25%">Customer : {{$quotationdetail->fkQuotation->fkCustomerdetail->name}}</td>
									            <td width="25%">
									            	@if(empty($quotationdetail->alias_name))
									            		Product : {{$quotationdetail->fkProduct->name}}
								            		@else
								            			Product : {{$quotationdetail->alias_name}}
								            		@endif
								            	</td>
									        </tr>
									        <tr>
									            <td>POL : {{$quotationdetail->fkQuotation->fkPortl->name}} - {{$quotationdetail->fkQuotation->fkPortl->fkCountry->name}}</td>
									            <td>POD : {{$quotationdetail->fkQuotation->fkPortd->name}} - {{$quotationdetail->fkQuotation->fkPortd->fkCountry->name}}</td>
									        	<td>ETD : {{$quotationdetail->fkQuotation->etd}}</td>
									            <td>ETA : {{$quotationdetail->fkQuotation->eta}}</td>
									        </tr>
									        <tr>
									            <td>Qty Need :
									            	{{number_format($quotationdetail->qty_kg, 0, ',' , '.')}}
									            	@if($quotationdetail->qty_amount == 0)
									            		<i class="glyphicon glyphicon-ok"></i>
									            	@endif
									            </td>
									            <td>Qty Plan : {{number_format($quotationdetail->qty_plan, 0, ',' , '.')}}</td>
									            <td>Qty Booking : {{number_format($quotationdetail->qty_book, 0, ',' , '.')}}</td>
									            <td>Qty Other : {{number_format($quotationdetail->qty_other, 0, ',' , '.')}}</td>
									        </tr>

										    <input type="hidden" id="notice" value="{{$quotationdetail->fkQuotation->notice}}">
									  	</table>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6">
										<table class="table table-bordered table-hover nowrap" width="100%">
										    <thead style="background: #00a65a !important; font-weight: bold; text-align: center; color: white;">
										        <tr>
										          	<th width="30%" style="text-align: center;">No. Batch</th>
										          	<th width="30%">Warehouse</th>
										          	<th width="30%">Supplier</th>
										          	<th width="10%" style="text-align: center;">Plan</th>
										        </tr>
										    </thead>

										    <tbody>
										    	@php ($total = 0)
										    	@foreach($plans as $plan)
										    		<tr>
											            <td style="text-align: center;">{{$plan->no_batch}}</td>
											            <td>{{$plan->fkStockin->fkWarehouse->name}}</td>
											            <td>
											            	{{$plan->fkStockin->fkSupplierdetail->name}} <br>
											            	Inc. Date : {{$plan->fkStockin->date_in}}
											            </td>
											            <td style="text-align: right;">{{number_format($plan->qty_plan, 0, ',' , '.')}} &nbsp;</td>
													</tr>
											        @php ($total = $total + $plan->qty_plan)
										        @endforeach
										        <tr>
										        	<td colspan="3" style="text-align: right; font-weight: bold;">Total Qty</td>
										        	<td style="text-align: right; font-weight: bold;">{{$total}} &nbsp;</td>
									        	</tr>
										    </tbody>
									  	</table>
									</div>

									<div class="col-md-6">
										<table class="table table-bordered table-hover nowrap" width="100%">
										    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
										        <tr>
										          	<th width="30%" style="text-align: center;">No. PR</th>
										          	<th width="30%">Information</th>
										          	<th width="30%">Note</th>
										          	<th width="10%" style="text-align: center;">Booking</th>
										        </tr>
										    </thead>

										    <tbody>
										    	@php ($total = 0)
										    	@foreach($purchaserequests as $purchaserequest)
										    		<tr>
											            <td>
											            	{{$purchaserequest->no_pr}} <br>
											            	Date : {{$purchaserequest->date_request}}
											            </td>
											            <td>
											            	@if($purchaserequest->supplierdetail == 0)
											            		All Supplier
											            	@else
											            		{{$purchaserequest->fkSupplierdetail->name}}
										            		@endif
									            			<br>
										            		@if($purchaserequest->warehouse == 0)
											            		All Warehouse
											            	@else
											            		{{$purchaserequest->fkWarehouse->name}}
										            		@endif
											            </td>
											            <td><pre>{{$purchaserequest->notice}}</pre></td>
											            <td style="text-align: right;">{{number_format($purchaserequest->qty_request, 0, ',' , '.')}} &nbsp;</td>
											        </tr>
											        @php ($total = $total + $purchaserequest->qty_request)
										        @endforeach
										        <tr>
										        	<td colspan="3" style="text-align: right; font-weight: bold;">Total Qty</td>
										        	<td style="text-align: right; font-weight: bold;">{{$total}} &nbsp;</td>
									        	</tr>
										    </tbody>
									  	</table>
									</div>
								</div>
							@else
								<div class="row">
									<center>
					  					<b>{{$company->name}}</b> <br>
						  				DELIVERY PLAN <br>
						  				DOCUMENT INTERNAL <br>
						  				DATE : {{ date('d F Y', strtotime("now")) }} <br>
						  			</center><br>

									<div class="col-md-4">
										<table class="table table-bordered table-hover nowrap" width="100%">
										    <tr style="background: #001F3F !important; font-weight: bold; text-align: center; color: white;">
									          	<th colspan="2">Quotation</th>
									        </tr>
										    <tr>
									            <td width="50%">{{$quotationdetail->fkQuotation->no_sp}}</td>
									            <td width="50%">Date : {{$quotationdetail->fkQuotation->date_sp}}</td>
									        </tr>
									        <tr>
									            <td>{{$quotationdetail->fkQuotation->fkCustomerdetail->name}}</td>
									            <td>
									            	@if(empty($quotationdetail->alias_name))
									            		{{$quotationdetail->fkProduct->name}}
								            		@else
								            			{{$quotationdetail->alias_name}}
								            		@endif
									            </td>
									        </tr>
									        <tr>
									            <td>POL : {{$quotationdetail->fkQuotation->fkPortl->name}} - {{$quotationdetail->fkQuotation->fkPortl->fkCountry->name}}</td>
									            <td>POD : {{$quotationdetail->fkQuotation->fkPortd->name}} - {{$quotationdetail->fkQuotation->fkPortd->fkCountry->name}}</td>
									        </tr>
									        <tr>
									            <td>ETD : {{$quotationdetail->fkQuotation->etd}}</td>
									            <td>ETA &nbsp;: {{$quotationdetail->fkQuotation->eta}}</td>
									        </tr>
									        <tr>
									            <td>Qty Need :
									            	{{number_format($quotationdetail->qty_kg, 0, ',' , '.')}}
									            	@if($quotationdetail->qty_amount == 0)
									            		<i class="glyphicon glyphicon-ok"></i>
									            	@endif
									            </td>
									            <td>Qty Plan : {{number_format($quotationdetail->qty_plan, 0, ',' , '.')}}</td>
									        </tr>
									        <tr>
									            <td>Qty Booking : {{number_format($quotationdetail->qty_book, 0, ',' , '.')}}</td>
									            <td>Qty Other : {{number_format($quotationdetail->qty_other, 0, ',' , '.')}}</td>
									        </tr>

										    <input type="hidden" id="notice" value="{{$quotationdetail->fkQuotation->notice}}">
									  	</table>
									</div>

									<div class="col-md-8">
										<table class="table table-bordered table-hover nowrap" width="100%">
										    <thead style="background: #00a65a !important; font-weight: bold; text-align: center; color: white;">
										        <tr>
										          	<th width="25%" style="text-align: center;">No. Batch</th>
										          	<th width="30%">Warehouse</th>
										          	<th width="35%">Supplier</th>
										          	<th width="10%" style="text-align: center;">Plan</th>
										        </tr>
										    </thead>

										    <tbody>
										    	@php ($total = 0)
										    	@foreach($plans as $plan)
										    		<tr>
											            <td style="text-align: center;">{{$plan->no_batch}}</td>
											            <td>{{$plan->fkStockin->fkWarehouse->name}}</td>
											            <td>[{{$plan->fkStockin->date_in}}] {{$plan->fkStockin->fkSupplierdetail->name}}</td>
											            <td style="text-align: right;">{{number_format($plan->qty_plan, 0, ',' , '.')}} &nbsp;</td>
											        </tr>
											        @php ($total = $total + $plan->qty_plan)
										        @endforeach
										        <tr>
										        	<td colspan="3" style="text-align: right; font-weight: bold;">Total Qty</td>
										        	<td style="text-align: right; font-weight: bold;">{{$total}} &nbsp;</td>
									        	</tr>
										    </tbody>
									  	</table>
									</div>
								</div>
							@endif
						</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop