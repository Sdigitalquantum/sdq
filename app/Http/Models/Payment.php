<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
	    'value',
	    'condition',
	    'created_user',
	    'updated_user'
  	];
}
