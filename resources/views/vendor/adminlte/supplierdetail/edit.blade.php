@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="active"><a href="#tab_detail" data-toggle="tab">Detail</a></li>
            <li class="disabled"><a>Account</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_detail">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Supplier Detail</h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<div class="row">
				  			<div class="col-md-3">
				  				<form method="post" action="{{ route('supplierdetail.update', $supplierdetail->id) }}">
						      		@method('PATCH')
						      		@csrf
						      		<input type="hidden" class="form-control" name="supplier" value="{{ $supplierdetail->supplier }}">
						      		<div class="form-group">
						          		<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ $supplierdetail->name }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="address">Address</label>
						              	<input type="text" class="form-control" name="address" value="{{ $supplierdetail->address }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="City">City</label>
						              	<select class="form-control select2" name="city" required>
						              		@foreach($citys as $city)
											    @if ($supplierdetail->city == $city->id)
												    <option value="{{ $city->id }}" selected>{{ $city->code }} - {{ $city->name }}</option>
												@else
												    <option value="{{ $city->id }}">{{ $city->code }} - {{ $city->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="mobile">Mobile</label>
						              	<input type="number" class="form-control" name="mobile" value="{{ $supplierdetail->mobile }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="alias_name">Alias Name</label>
						              	<input type="text" class="form-control" name="alias_name" value="{{ $supplierdetail->alias_name }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="alias_mobile">Alias Mobile</label>
						              	<input type="number" class="form-control" name="alias_mobile" value="{{ $supplierdetail->alias_mobile }}">
						          	</div>
						          	<button type="submit" class="btn btn-sm btn-success">Update</button>
					          		<a href="{{ url('/supplierdetail/'.$supplierdetail->supplier) }}" class="btn btn-sm btn-danger">Cancel</a>
						      	</form>
			  				</div>

			  				<div class="col-md-9">
			  					<br><table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="20%">Name</th>
								          	<th width="25%">Address</th>
								          	<th width="20%">Mobile</th>
								          	<th width="20%">Alias</th>
								          	<th width="15%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($supplierdetails as $supplierdetail)
								    		<tr>
									            <td>{{$supplierdetail->name}}</td>
									            <td>
									            	{{$supplierdetail->address}} <br>
									            	{{$supplierdetail->fkCity->name}} - {{$supplierdetail->fkCity->fkCountry->name}}
									            </td>
									            <td>{{$supplierdetail->mobile}}</td>
									            <td>
									            	{{$supplierdetail->alias_name}} <br> {{$supplierdetail->alias_mobile}}
									            </td>
									            <td>
									            	@if($supplierdetail->status == 1) <a href="{{ route('supplierdetail.edit',$supplierdetail->id)}}" class="btn btn-xs btn-warning">Edit</a>
									            	@else <font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
									            	@endif
									            </td>
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
			  				</div>
		  				</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop