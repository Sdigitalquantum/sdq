<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Purchaseorder;
use App\Http\Models\Purchasedetail;
use App\Http\Models\Purchasedetailpr;
use App\Http\Models\Quotationdetail;
use App\Http\Models\Product;
use App\Http\Models\Supplierdetail;
use App\Http\Models\Letter;
use App\Http\Models\Employeeset;

class PurchaseorderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $purchaseorders = Purchaseorder::orderBy('created_at','desc')->get();
        $purchasedetails = Purchasedetail::where('status', 1)->orderBy('created_at','desc')->get();
        $purchasedetailprs = Purchasedetailpr::orderBy('purchaserequest','asc')->get();

        return view('vendor/adminlte/purchaseorder.index', compact('purchaseorders','purchasedetails','purchasedetailprs','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        return view('vendor/adminlte/purchaseorder.create', compact('disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        $no = Purchaseorder::select('no_inc')->whereYear('date_order', date('Y'))->max('no_inc');
        if (empty($no)) {
            $no_inc = 1;
        } else {
            $no_inc = $no + 1;
        }

        $letter = Letter::where('menu', 'Purchase Order')->first();
        if (empty($letter->company)) {
            $company = "";
        } else {
            $company = $letter->company."/";
        }

        if (empty($letter->code)) {
            $code = "";
        } else {
            $code = $letter->code;
        }

        if ($letter->romawi == 0) {
            $romawi = "";
        } else {
            if (date("m") == 1) {
                $romawi = "I/";
            } elseif (date("m") == 2) {
                $romawi = "II/";
            } elseif (date("m") == 3) {
                $romawi = "III/";
            } elseif (date("m") == 4) {
                $romawi = "IV/";
            } elseif (date("m") == 5) {
                $romawi = "V/";
            } elseif (date("m") == 6) {
                $romawi = "VI/";
            } elseif (date("m") == 7) {
                $romawi = "VII/";
            } elseif (date("m") == 8) {
                $romawi = "VIII/";
            } elseif (date("m") == 9) {
                $romawi = "IX/";
            } elseif (date("m") == 10) {
                $romawi = "X/";
            } elseif (date("m") == 11) {
                $romawi = "XI/";
            } elseif (date("m") == 12) {
                $romawi = "XII/";
            }
        }

        if ($letter->year == 0) {
            $year = "";
        } else {
            $year = date("Y");
        }

        $no_po = $company.$code.sprintf("%05s", $no_inc)."/".$romawi.$year;

        $purchaseorder = new Purchaseorder([
            'no_inc'            => $no_inc,
            'no_po'             => $no_po,
            'tax'               => !empty($request->get('tax')) ? $request->get('tax') : 0,
            'date_order'        => $request->get('date_order'),
            'notice'            => !empty($request->get('notice')) ? $request->get('notice') : '',
            'reason'            => '',
            'created_user'      => $session_id,
            'updated_user'      => $session_id
        ]);
        $purchaseorder->save();

        return redirect('/purchasedetail/create/'.$purchaseorder->id)->with('success', 'Purchase Order has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $purchaseorder = Purchaseorder::find($id);

        return view('vendor/adminlte/purchaseorder.edit', compact('purchaseorder','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $purchaseorder = Purchaseorder::find($id);
        $purchaseorder->tax               = !empty($request->get('tax')) ? $request->get('tax') : 0;
        $purchaseorder->date_order        = $request->get('date_order');
        $purchaseorder->notice            = !empty($request->get('notice')) ? $request->get('notice') : '';
        $purchaseorder->updated_user      = $session_id;
        $purchaseorder->save();

        return redirect('/purchaseorder')->with('success', 'Purchase Order has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $purchaseorder = Purchaseorder::find($id);
        $status = $purchaseorder->status;

        if ($status == 1) {
            $purchaseorder->status       = 0;
            $purchaseorder->updated_user = $session_id;
            $purchaseorder->save();

            return redirect('/purchaseorder')->with('error', 'Purchase Order has been deleted');
        } elseif ($status == 0) {
            $purchaseorder->status       = 1;
            $purchaseorder->updated_user = $session_id;
            $purchaseorder->save();

            return redirect('/purchaseorder')->with('success', 'Purchase Order has been activated');
        }
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function set(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $purchaseorder = Purchaseorder::find($id);
        $approve = $purchaseorder->approve;

        if ($approve == 1) {
            $purchaseorder->approve           = 0;
            $purchaseorder->status_approve    = 1;
            $purchaseorder->reason            = 'Set None Approval';
            $purchaseorder->updated_user      = $session_id;
            $purchaseorder->save();

            return redirect('/purchaseorder')->with('error', 'Purchase Order has been set none approval');
        } elseif ($approve == 0) {
            $purchaseorder->approve           = 1;
            $purchaseorder->status_approve    = 0;
            $purchaseorder->reason            = '';
            $purchaseorder->updated_user      = $session_id;
            $purchaseorder->save();

            return redirect('/purchaseorder')->with('success', 'Purchase Order has been set used approval');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function approval(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $purchaseorders = Purchaseorder::where([['status', 1], ['approve', 1], ['status_release', 1]])->orderBy('created_at','desc')->get();
        $purchasedetails = Purchasedetail::where('status', 1)->orderBy('created_at','desc')->get();
        $purchasedetailprs = Purchasedetailpr::orderBy('purchaserequest','asc')->get();

        return view('vendor/adminlte/purchaseorder.approval', compact('purchaseorders','purchasedetails','purchasedetailprs','disable','employeeroots','employeemenus','employeesubs'));
    }
    
    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $purchaseorder = Purchaseorder::find($id);
        $purchaseorder->status_approve    = 1;
        $purchaseorder->reason            = $request->get('reason');
        $purchaseorder->approved_at       = new \DateTime();
        $purchaseorder->approved_user     = $session_id;
        $purchaseorder->save();

        return redirect('/approvalpo')->with('success', 'Purchase Order has been approved');
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reject(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $purchaseorder = Purchaseorder::find($id);
        $purchaseorder->status_approve    = 2;
        $purchaseorder->reason            = $request->get('reason');
        $purchaseorder->approved_at       = new \DateTime();
        $purchaseorder->approved_user     = $session_id;
        $purchaseorder->save();

        return redirect('/approvalpo')->with('error', 'Purchase Order has been rejected');
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function release(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $purchaseorder = Purchaseorder::find($id);
        $purchaseorder->status_release  = 1;
        $purchaseorder->updated_user    = $session_id;
        $purchaseorder->save();

        return redirect('/purchaseorder')->with('success', 'Purchase Order has been released');
    }
}
