@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
          	<li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="active"><a href="#tab_detail" data-toggle="tab">Detail</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_detail">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Detail Purchase General &nbsp;<a href="{{ url('purchasegeneral') }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('purchasedetailprg.store') }}">
				          	@csrf
				          	<div class="row">
    							<div class="col-md-6">
    								<div class="form-group">
						              	<label for="no_prg">No. PR</label>
						              	<input type="hidden" name="purchasegeneral" value="{{ $purchasegeneral->id }}">
						              	<input type="text" class="form-control" name="no_prg" value="{{ $purchasegeneral->no_prg }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="product">Product</label>
						              	<select class="form-control select2" name="product" required>
						              		<option value="{{ old('product') }}">Choose One</option>
					                    	@foreach($products as $product)
											    <option value="{{ $product->id }}">{{ $product->code }} - {{ $product->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="supplierdetail">Supplier</label>
						              	<select class="form-control select2" name="supplierdetail" required>
						              		<option value="0">All Supplier</option>
					                    	@foreach($supplierdetails as $supplierdetail)
											    <option value="{{ $supplierdetail->id }}">{{ $supplierdetail->name }}</option>
											@endforeach
					                	</select>
						          	</div>
								</div>

								<div class="col-md-6">
						          	<div class="form-group">
						              	<label for="warehouse">Warehouse</label>
						              	<select class="form-control select2" name="warehouse" required>
						              		@php ($array = [])
									    	@foreach($employeewarehouses as $employeewarehouse)
									    		@php ($array[] = $employeewarehouse->warehouse)
									    	@endforeach

						              		<option value="0">All Warehouse</option>
					                    	@foreach($warehouses as $warehouse)
					                    		@if(in_array($warehouse->id, $array))
											    	<option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
										    	@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_request">Qty Request (Kg)</label>
						              	<input type="number" class="form-control" name="qty_request" value="{{ old('qty_request') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="button">&nbsp;</label> <br>
						              	<button type="submit" class="btn btn-sm btn-success">Save</button>
						          	</div>
								</div>
							</div>
						</form>

						<br><table class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #001F3F !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="20%">Product</th>
						          	<th width="25%">Supplier</th>
						          	<th width="25%">Warehouse</th>
						          	<th width="10%">Qty (Kg)</th>
						          	<th width="10%">Unit</th>
						          	<th width="10%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@foreach($purchasedetailprgs as $purchasedetailprg)
						    		<tr>
							            <td>{{$purchasedetailprg->fkProduct->name}}</td>
							            <td>
							            	@if($purchasedetailprg->supplierdetail == 0)
							            		All Supplier
							            	@else
							            		{{$purchasedetailprg->fkSupplierdetail->name}}
						            		@endif
						            	</td>
							            <td>
							            	@if($purchasedetailprg->warehouse == 0)
							            		All Warehouse
							            	@else
							            		{{$purchasedetailprg->fkWarehouse->name}}
						            		@endif
						            	</td>
							            <td style="text-align: right;">{{number_format($purchasedetailprg->qty_request, 0, ',' , '.')}} &nbsp;</td>
							            <td>{{$purchasedetailprg->fkProduct->fkUnit->name}}</td>

							            @if($purchasedetailprg->status == 1)
								            <td>
								            	<a href="{{ route('purchasedetailprg.edit',$purchasedetailprg->id)}}" class="btn btn-xs btn-warning">Edit</a>&nbsp;

								            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$purchasedetailprg->id}}">Delete</button></a>

							            		<form action="{{ route('purchasedetailprg.destroy', $purchasedetailprg->id)}}" method="post">
								                  	@csrf
								                  	@method('DELETE')
								                  	<div class="modal fade bs-example-modal{{$purchasedetailprg->id}}" tabindex="-1" role="dialog" aria-hidden="true">
					                                    <div class="modal-dialog modal-sm">
					                                        <div class="modal-content">
					                                            <div class="modal-header btn-danger">
					                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
					                                                </button>
					                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
					                                            </div>
					                                            <div class="modal-body">
					                                                Are you sure to delete this data?
					                                            </div>
					                                            <div class="modal-footer">
					                                                <button type="submit" class="btn btn-success">Yes</button>
					                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
					                                            </div>
					                                        </div>
					                                    </div>
					                                </div>
								                </form>
								            </td>
							            @else
							            	<td>
							            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$purchasedetailprg->id}}">Activated</button></a>

							            		<form action="{{ route('purchasedetailprg.destroy', $purchasedetailprg->id)}}" method="post">
								                  	@csrf
								                  	@method('DELETE')
								                  	<div class="modal fade bs-example-modal{{$purchasedetailprg->id}}" tabindex="-1" role="dialog" aria-hidden="true">
					                                    <div class="modal-dialog modal-sm">
					                                        <div class="modal-content">
					                                            <div class="modal-header btn-success">
					                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
					                                                </button>
					                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
					                                            </div>
					                                            <div class="modal-body">
					                                                Are you sure to active this data?
					                                            </div>
					                                            <div class="modal-footer">
					                                                <button type="submit" class="btn btn-success">Yes</button>
					                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
					                                            </div>
					                                        </div>
					                                    </div>
					                                </div>
								                </form>
							            	</td>
							            @endif
							        </tr>
						        @endforeach
						    </tbody>
					  	</table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop