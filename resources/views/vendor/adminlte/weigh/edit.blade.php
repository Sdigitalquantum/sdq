@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Weighing</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
				<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Weighing Slip - Shipping
				  			&nbsp;<a href="{{ url('weighemkl') }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a>
				  		</h4>
			  		</div>
				  	
				  	<div class="box-body">
				  		<div id="printable">
					  		Split Proforma <b>{{ $stuff->fkSchedulestuff->fkSchedulebook->fkQuotationsplit->no_split }}</b>
					  		<table class="table table-bordered table-hover nowrap" width="100%">
							    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
							        <tr>
							          	<th width="40%" style="vertical-align: top;">Container / Seal</th>
							          	<th width="20%">Netto</th>
							          	<th width="20%">Bruto</th>
							          	<th width="20%">Empty</th>
							          	<th width="10%">Action</th>
							        </tr>
							    </thead>

							    <tbody>
							    	@foreach($stuffs as $stuff)
							    		<tr>
								            <td>{{$stuff->container}} / {{$stuff->seal}}</td>
								            <td>{{$stuff->container_netto}}</td>
								            <form method="post" action="{{ route('weigh.update', $stuff->id) }}">
									      		@method('PATCH')
									      		@csrf
									            <td><input type="number" class="form-control" name="container_bruto" value="{{ $stuff->container_bruto }}" required></td>
									            <td><input type="number" class="form-control" name="container_empty" value="{{ $stuff->container_empty }}" required></td>
									            <td><button type="submit" class="btn btn-sm btn-success">Save</button></td>
								            </form>
										</tr>
						    		@endforeach
							    </tbody>
						    </table>
					    </div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop