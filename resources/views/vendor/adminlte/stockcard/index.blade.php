@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
          	<li class="disabled"><a>Cut Off</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
				<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Cut Off &nbsp;<a href="{{ route('cutoff.create')}}" class="btn btn-xs btn-success" style="text-decoration: none;">Input Balance</a></h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="35%">Product</th>
						          	<th width="10%">Year</th>
						          	<th width="15%">Month</th>
						          	<th width="10%">Qty In (Kg)</th>
						          	<th width="10%">Qty Out (Kg)</th>
						          	<th width="15%">Price @Kg</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($stockcards as $stockcard)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>{{$stockcard->fkProduct->name}}</td>
							            <td style="text-align: right;">{{$stockcard->year}} &nbsp;</td>
							            <td>
							            	@if($stockcard->month == 1) January
											@elseif($stockcard->month == 2) February
											@elseif($stockcard->month == 3) March
											@elseif($stockcard->month == 4) April
											@elseif($stockcard->month == 5) May
											@elseif($stockcard->month == 6) June
											@elseif($stockcard->month == 7) July
											@elseif($stockcard->month == 8) August
											@elseif($stockcard->month == 9) September
											@elseif($stockcard->month == 10) October
											@elseif($stockcard->month == 11) November
											@elseif($stockcard->month == 12) Desember
											@endif
							            </td>
							            <td style="text-align: right;">{{number_format($stockcard->qty, 0, ',' , '.')}} &nbsp;</td>
							            <td style="text-align: right;">{{number_format($stockcard->qty_out, 0, ',' , '.')}} &nbsp;</td>
							            <td style="text-align: right;">{{number_format($stockcard->price, 0, ',' , '.')}} &nbsp;</td>
						            </tr>
					            @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Product</td>
						          	<td>Year</td>
						          	<td>Month</td>
						          	<td>Qty In (Kg)</td>
						          	<td>Qty Out (Kg)</td>
						          	<td>Price</td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop