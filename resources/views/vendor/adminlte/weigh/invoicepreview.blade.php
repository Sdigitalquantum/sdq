@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
            <li class="disabled"><a>List</a></li>
          	<li class="active"><a href="#tab_preview" data-toggle="tab">Print</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_preview">
			    <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Preview Invoice - Shipping
				  			&nbsp;<a href="javascript:window.print()" class="btn btn-xs btn-default" style="text-decoration: none;"><i class="glyphicon glyphicon-print"></i></a>
				  			&nbsp;<a href="{{ url('invoice') }}" class="btn btn-xs btn-danger" style="text-decoration: none;">Back</a>
				  		</h4>
			  		</div>
				  	
				  	<div class="box-body">
				  		<div id="printable">
						  	<table width="100%">
						  		<tr>
						  			<td rowspan="2" width="20%" style="text-align: right;"><img src="{{ asset('sdq_logo.jpg') }}" width="100"></td>
						  			<td width="70%" style="text-align: center; font-weight: bold; font-size: 20pt;">{{ $company->name }}</td>
						  			<td width="10%">&nbsp;</td>
					  			</tr>
					  			<tr>
						  			<td style="text-align: center; font-weight: bold;">
						  				{{ $company->address }} <br>
						  				{{ $company->fkCity->name }} - {{ $company->fkCity->fkCountry->name }}
						  			</td>
						  			<td>&nbsp;</td>
					  			</tr>
				  			</table><hr>

				  			<font style="font-size: 1.2vw;">
				  				<center>
				  					<b>INVOICE</b> <br>
					  				PO : @if(!empty($schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->no_poc)) {{ $schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->no_poc }} @else N/A @endif <br>
					  				DATE : {{ date('d F Y', strtotime("now")) }} <br>
					  				NO : {{ $schedulestuff->fkSchedulebook->fkQuotationsplit->no_split }}
					  			</center><br>

					  			<table class="border" width="100%">
					  				<tr>
					  					<td width="30%" valign="top" style="font-weight: bold;">Shipper</td>
					  					<td width="70%">
					  						{{ $company->name }} <br>
					  						{{ $company->address }} <br>
					  						{{ $company->fkCity->name }} - {{ $company->fkCity->fkCountry->name }}
					  					</td>
				  					</tr>
				  					<tr>
				  						<td colspan="2">&nbsp;</td>
									</tr>
				  					<tr>
					  					<td valign="top" style="font-weight: bold;">Consignee</td>
					  					<td>
					  						{{ $schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->fkCustomerdetail->name }} <br>
					  						{{ $schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->fkCustomerdetail->address }} <br>
					  						{{ $schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->fkCustomerdetail->fkCity->name }} - {{ $schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->fkCustomerdetail->fkCity->fkCountry->name }}
					  					</td>
				  					</tr>
				  				</table><br>

				  				<table class="border" width="100%">
					  				<tr>
					  					<td width="30%" valign="top" style="font-weight: bold;">
					  						BENEFICIARY NAME <br>
					  						BENEFICIARY BANK <br>
					  						BANK ACCOUNT ({{ $schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->fkCompanyaccount->fkCurrency->code }}) <br>
					  						SWIFT CODE <br>
					  						BANK ADDRESS
					  					</td>
					  					<td width="70%">
					  						{{ $schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->fkCompanyaccount->account_name }} <br>
					  						{{ $schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->fkCompanyaccount->fkBank->name }} <br>
					  						{{ $schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->fkCompanyaccount->account_no }} <br>
					  						{{ $schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->fkCompanyaccount->account_swift }} <br>
					  						{{ $schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->fkCompanyaccount->account_address }} <br>
					  						{{ $schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->fkCompanyaccount->fkCity->name }} - {{ $schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->fkCompanyaccount->fkCity->fkCountry->name }}
					  					</td>
				  					</tr>
				  				</table><br>

				  				<table class="border" width="100%">
					  				<tr>
					  					<td width="30%" valign="top" style="font-weight: bold;">Item Description</td>
					  					<td width="70%">
					  						@foreach($quotationdetails as $quotationdetail)
					  							@if(empty($quotationdetail->alias_name))
								            		{{$quotationdetail->fkProduct->name}} <br>
							            		@else
							            			{{$quotationdetail->alias_name}} <br>
							            		@endif
					  						@endforeach
					  					</td>
				  					</tr>
				  					
				  					@foreach($stuffs as $stuff)
				  					@endforeach
				  					
				  					<tr>
					  					<td style="font-weight: bold;">Date of Shipment</td>
					  					<td>{{ date('d F Y', strtotime($stuff->etd)) }}</td>
				  					</tr>
				  					<tr>
					  					<td style="font-weight: bold;">Port of Loading</td>
					  					<td>{{ $stuff->fkPortl->name }} - {{ $stuff->fkPortl->fkCountry->name }}</td>
				  					</tr>
				  					<tr>
					  					<td style="font-weight: bold;">Port of Discharge</td>
					  					<td>{{ $stuff->fkPortd->name }} - {{ $stuff->fkPortd->fkCountry->name }}</td>
				  					</tr>
				  					<tr>
					  					<td colspan="2" style="font-weight: bold;">Detail item</td>
				  					</tr>
				  					<tr>
					  					<td colspan="2">
					  						<table border="3" width="100%" style="text-align: center; font-weight: bold;">
					  							<tr>
					  								<td width="30%">COMMODITY</td>
					  								<td width="10%">QTY <br>(Fcl)</td>
					  								<td width="10%">QTY <br>(Bags)</td>
					  								<td width="10%">NUT QTY <br>(Pcs)</td>
					  								<td width="10%">NUT QTY <br>(Kgs)</td>
					  								<td width="10%">PRICE <br>(Kgs)</td>
					  								<td width="20%">AMOUNT</td>
				  								</tr>

				  								@php ($kg = 0)
				  								@php ($total = 0)
				  								@php ($cur = '')
				  								@foreach($quotationdetails as $quotationdetail)
						  							<tr>
						  								<td style="text-align: left;">
						  									@if(empty($quotationdetail->alias_name))
											            		{{$quotationdetail->fkProduct->name}}
										            		@else
										            			{{$quotationdetail->alias_name}}
										            		@endif
						  								</td>
						  								<td style="text-align: right;">{{ $quotationdetail->qty_fcl }} &nbsp;</td>
						  								<td style="text-align: right;">{{ $quotationdetail->qty_bag }} &nbsp;</td>
						  								<td style="text-align: right;">{{number_format($quotationdetail->qty_pcs, 0, ',' , '.')}} &nbsp;</td>
						  								<td style="text-align: right;">{{number_format($quotationdetail->qty_plan, 0, ',' , '.')}} &nbsp;</td>
						  								<td style="text-align: right;">
						  									@if($quotationdetail->fkQuotation->fkCurrency->code == 'IDR')
											            		Rp. {{number_format($quotationdetail->price, 0, ',' , '.')}} &nbsp;
										            		@elseif($quotationdetail->fkQuotation->fkCurrency->code == 'USD')
										            			$ {{$quotationdetail->price}} &nbsp;
										            		@endif
						  								</td>
						  								<td style="text-align: right;">
						  									@if($quotationdetail->fkQuotation->fkCurrency->code == 'IDR')
											            		@if($quotationdetail->disc_type == 0)
												            		Rp. {{number_format($quotationdetail->price*$quotationdetail->qty_plan, 0, ',' , '.')}} &nbsp;
												            	@elseif($quotationdetail->disc_type == 2)
												            		Rp. {{number_format($quotationdetail->price*$quotationdetail->qty_plan - ($quotationdetail->disc_value), 0, ',' , '.')}} &nbsp;
												            	@elseif($quotationdetail->disc_type == 1)
												            		Rp. {{number_format($quotationdetail->price*$quotationdetail->qty_plan - ($quotationdetail->disc_value/100*($quotationdetail->price*$quotationdetail->qty_plan)), 0, ',' , '.')}} &nbsp;
												            	@endif
										            		@elseif($quotationdetail->fkQuotation->fkCurrency->code == 'USD')
										            			@if($quotationdetail->disc_type == 0)
												            		$ {{number_format($quotationdetail->price*$quotationdetail->qty_plan, 0, ',' , '.')}} &nbsp;
												            	@elseif($quotationdetail->disc_type == 2)
												            		$ {{number_format($quotationdetail->price*$quotationdetail->qty_plan - ($quotationdetail->disc_value), 0, ',' , '.')}} &nbsp;
												            	@elseif($quotationdetail->disc_type == 1)
												            		$ {{number_format($quotationdetail->price*$quotationdetail->qty_plan - ($quotationdetail->disc_value/100*($quotationdetail->price*$quotationdetail->qty_plan)), 0, ',' , '.')}} &nbsp;
												            	@endif
										            		@endif
						  								</td>

						  								@if($quotationdetail->disc_type == 0)
										            		@php ($total = $total + ($quotationdetail->price*$quotationdetail->qty_plan))
										            	@elseif($quotationdetail->disc_type == 2)
										            		@php ($total = $total + ($quotationdetail->price*$quotationdetail->qty_plan) - ($quotationdetail->disc_value))
										            	@elseif($quotationdetail->disc_type == 1)
										            		@php ($total = $total + ($quotationdetail->price*$quotationdetail->qty_plan) - ($quotationdetail->disc_value/100*($quotationdetail->price*$quotationdetail->qty_plan)))
									            		@endif
					  								</tr>

					  								@php ($kg = $kg + $quotationdetail->qty_plan)
					  								@php ($cur = $quotationdetail->fkQuotation->fkCurrency->code)
						  						@endforeach

						  						<tr>
					  								<td colspan="6" style="text-align: right;">FREIGHT &nbsp;</td>
					  								<td style="text-align: right;">
					  									@if($quotationdetail->fkQuotation->fkCurrency->code == 'IDR')
										            		Rp. {{number_format($schedulestuff->freight, 0, ',' , '.')}} &nbsp;
									            		@elseif($quotationdetail->fkQuotation->fkCurrency->code == 'USD')
									            			$ {{$schedulestuff->freight}} &nbsp;
									            		@endif
					  								</td>
				  								</tr>
				  								<tr>
					  								<td colspan="6" style="text-align: right;">FOB &nbsp;</td>
					  								<td style="text-align: right;">
					  									@if($quotationdetail->fkQuotation->fkCurrency->code == 'IDR')
										            		Rp. {{number_format($total-$schedulestuff->freight, 0, ',' , '.')}} &nbsp;
									            		@elseif($quotationdetail->fkQuotation->fkCurrency->code == 'USD')
									            			$ {{number_format($total-$schedulestuff->freight, 0, ',' , '.')}} &nbsp;
									            		@endif
					  								</td>
				  								</tr>
				  							</table>
					  					</td>
				  					</tr>
				  				</table><br>

				  				<table width="100%">
					  				<tr>
					  					<td width="30%" style="font-weight: bold;">{{ $quotation->fkPaymentterm->code }}</td>
					  					<td width="40%">{{ $stuff->fkPortd->name }} - {{ $stuff->fkPortd->fkCountry->name }}</td>
					  					<td width="30%">&nbsp;</td>
				  					</tr>
				  					<tr>
					  					<td style="font-weight: bold;">ETD</td>
					  					<td>{{ date('d F Y', strtotime($stuff->etd)) }}</td>
					  					<td style="text-align: center;">Surabaya, {{ date('d F Y', strtotime($schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->date_sp)) }}</td>
				  					</tr>
				  					<tr>
					  					<td style="font-weight: bold;">ETA</td>
					  					<td>{{ date('d F Y', strtotime($stuff->eta)) }}</td>
					  					<td style="text-align: center;">{{ $company->name }}</td>
				  					</tr>
				  					<tr>
					  					<td valign="top" style="font-weight: bold;">Payment Term</td>
					  					<td>
					  						@foreach($quotationpayments as $quotationpayment)
					  							{{ $quotationpayment->fkPayment->value }}% - {{ $quotationpayment->fkPayment->condition }} <br>
					  						@endforeach	
					  					</td>
					  					<td rowspan="3" style="text-align: center;"><img src="{{ asset('sdq_logo.jpg') }}" width="75"></td>
				  					</tr>
				  					<tr>
					  					<td style="font-weight: bold;">GROSS WEIGHT (Kgs)</td>
					  					<td>{{ $schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->qty_gross }}</td>
				  					</tr>
				  					<tr>
					  					<td style="font-weight: bold;">NETT WEIGHT (Kgs)</td>
					  					<td>{{ $kg }}</td>
				  					</tr>
				  					<tr>
					  					<td style="font-weight: bold;">CBM</td>
					  					<td>{{ $schedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->qty_cbm }} CBM</td>
					  					<td style="text-align: center;">AUTHORIZED SIGNATURE</td>
				  					</tr>
								</table>
							</font>

							<br><font style="font-size: 1.0vw;">[ Cetakan ke - {{ $schedulestuff->qty_invoice }} ]</font>
			  			</div>
				  	</div>
				<div>
			</div>
		</div>
	</div>
@stop