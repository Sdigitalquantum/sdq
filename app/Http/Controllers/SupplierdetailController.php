<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Supplierdetail;
use App\Http\Models\City;
use App\Http\Models\Employeeset;

class SupplierdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $supplierdetail = new Supplierdetail([
                    'supplier'      => $json->supplier,
                    'city'          => $json->city,
                    'name'          => $json->name,
                    'address'       => $json->address,
                    'mobile'        => $json->mobile,
                    'alias_name'    => !empty($json->alias_name) ? $json->alias_name : '',
                    'alias_mobile'  => !empty($json->alias_mobile) ? $json->alias_mobile : '',
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $supplierdetail->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $supplierdetails = Supplierdetail::where('supplier', $id)->orderBy('created_at','desc')->get();
        $citys = City::where('status', 1)->orderBy('name', 'asc')->get();

        return view('vendor/adminlte/supplierdetail.show', compact('id','supplierdetails','citys','disable'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $supplierdetail = Supplierdetail::find($id);
        $supplierdetails = Supplierdetail::where('supplier', $supplierdetail->supplier)->orderBy('created_at','desc')->get();
        $citys = City::where('status', 1)->orderBy('name', 'asc')->get();

        return view('vendor/adminlte/supplierdetail.edit', compact('supplierdetail','supplierdetails','citys','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $supplierdetail = Supplierdetail::find($id);
                $supplierdetail->supplier      = $json->supplier;
                $supplierdetail->city          = $json->city;
                $supplierdetail->name          = $json->name;
                $supplierdetail->address       = $json->address;
                $supplierdetail->mobile        = $json->mobile;
                $supplierdetail->alias_name    = !empty($json->alias_name) ? $request->get('alias_name') : '';
                $supplierdetail->alias_mobile  = !empty($json->alias_mobile) ? $request->get('alias_mobile') : '';
                $supplierdetail->updated_user  = $session_id;
                $result = $supplierdetail->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $supplierdetail = Supplierdetail::find($id);
        $status = $supplierdetail->status;

        if ($status == 1) {
        	$supplierdetail->status       = 0;
            $supplierdetail->updated_user = $session_id;
	        $result = $supplierdetail->save();

        } elseif ($status == 0) {
        	$supplierdetail->status       = 1;
            $supplierdetail->updated_user = $session_id;
	        $result = $supplierdetail->save();
            
        };
        return $this->jsonSuccess( $result );
    }
}
