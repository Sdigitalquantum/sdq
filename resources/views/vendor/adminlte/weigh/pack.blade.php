@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>Print</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
				<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Packing List - Shipping</h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="15%">Split Proforma</th>
						          	<th width="15%">Schedule</th>
						          	<th width="15%">Stuffing</th>
						          	<th width="25%">Note sdq</th>
						          	<th width="20%">Reason EMKL</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($schedulestuffs as $schedulestuff)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>{{$schedulestuff->fkSchedulebook->fkQuotationsplit->no_split}}</td>
							            <td>
							            	POL : {{$schedulestuff->fkSchedulebook->pol}} <br>
							            	POD : {{$schedulestuff->fkSchedulebook->pod}} <br>
							            	ETD : {{$schedulestuff->fkSchedulebook->etd}} <br>
							            	ETA : {{$schedulestuff->fkSchedulebook->eta}}
							            </td>
							            <td>
							            	Date : {{$schedulestuff->date}} <br>
							            	PIC &nbsp;: {{$schedulestuff->person}} <br>
							            	Qty Fcl : {{$schedulestuff->qty_approve}}
							            </td>
							            <td><pre>{{$schedulestuff->notice}}</pre></td>
							            <td><pre>{{$schedulestuff->reason}}</pre></td>
										<td style="text-align: center;">
											@if($schedulestuff->status_reroute == 1)
												<font color="red">Re-Route <i class="glyphicon glyphicon-remove"></i></font>
											@elseif($schedulestuff->status_reschedule == 1)
												<font color="red">Canceled <i class="glyphicon glyphicon-remove"></i></font>
											@else
												<a href="{{ url('weigh/packlist/preview',$schedulestuff->id)}}" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-print"></i></a>
				            				@endif
										</td>
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Split Proforma</td>
						          	<td>Schedule</td>
						          	<td>Stuffing</td>
						          	<td>Note sdq</td>
						          	<td>Reason EMKL</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop