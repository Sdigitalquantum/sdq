@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="active"><a href="#tab_detail" data-toggle="tab">Detail</a></li>
            <li class="disabled"><a>Account</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_detail">
            	<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Supplier Detail &nbsp;<a href="{{ route('supplier.edit', $id) }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4>
			  		</div>
				  	
				  	<div class="box-body">
				  		<div class="row">
				  			<div class="col-md-3">
				  				<form method="post" action="{{ route('supplierdetail.store') }}">
						          	@csrf
						          	<input type="hidden" class="form-control" name="supplier" value="{{ $id }}">
						          	<div class="form-group">
						          		<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="address">Address</label>
						              	<input type="text" class="form-control" name="address" value="{{ old('address') }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="City">City</label>
						              	<select class="form-control select2" name="city" required>
						              		<option value="{{ old('city') }}">Choose One</option>
					                    	@foreach($citys as $city)
											    <option value="{{ $city->id }}">{{ $city->code }} - {{ $city->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="mobile">Mobile</label>
						              	<input type="number" class="form-control" name="mobile" value="{{ old('mobile') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="alias_name">Alias Name</label>
						              	<input type="text" class="form-control" name="alias_name" value="{{ old('alias_name') }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="alias_mobile">Alias Mobile</label>
						              	<input type="number" class="form-control" name="alias_mobile" value="{{ old('alias_mobile') }}">
						          	</div>
						          	<button type="submit" class="btn btn-sm btn-success">Save</button>
						      	</form>
			  				</div>

			  				<div class="col-md-9">
			  					<br><table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="20%">Name</th>
								          	<th width="25%">Address</th>
								          	<th width="20%">Mobile</th>
								          	<th width="20%">Alias</th>
								          	<th width="15%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($supplierdetails as $supplierdetail)
								    		<tr>
									            <td>{{$supplierdetail->name}}</td>
									            <td>
									            	{{$supplierdetail->address}} <br>
									            	{{$supplierdetail->fkCity->name}} - {{$supplierdetail->fkCity->fkCountry->name}}
									            </td>
									            <td>{{$supplierdetail->mobile}}</td>
									            <td>
									            	{{$supplierdetail->alias_name}} <br> {{$supplierdetail->alias_mobile}}
									            </td>

									            @if($supplierdetail->status == 1)
										            <td>
										            	<a href="{{ route('supplierdetail.edit',$supplierdetail->id)}}" class="btn btn-xs btn-warning">Edit</a> &nbsp;
										            	
										            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$supplierdetail->id}}">Delete</button>

										            	<form action="{{ route('supplierdetail.destroy', $supplierdetail->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$supplierdetail->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-danger">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to delete this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
										            </td>
									            @else
									            	<td>
									            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$supplierdetail->id}}">Activated</button>

									            		<form action="{{ route('supplierdetail.destroy', $supplierdetail->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$supplierdetail->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-success">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to active this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
									            	</td>
									            @endif
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
			  				</div>
		  				</div>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop