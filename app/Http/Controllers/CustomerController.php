<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Customer;
use App\Http\Models\Customerdetail;
use App\Http\Models\Customeraccount;
use App\Http\Models\Account;
use App\Http\Models\City;
use App\Http\Models\Company;
use App\Http\Models\Employeeset;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $customers = Customer::orderBy('created_at','desc')->get();
        // dd($customers);
        $customerdetails = Customerdetail::where('status', 1)->get();

        return view('vendor/adminlte/customer.index', compact('customers','customerdetails','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $accounts = Account::where('status', 1)->get();
        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/customer.create', compact('accounts','citys','disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                if (!empty($json->email)) {
                    $request->validate([
                        'email' => 'email:true'
                    ]);
                }
                
                $no = Customer::select('no_inc')->max('no_inc');
                if (empty($no)) {
                    $no_inc = 1;
                } else {
                    $no_inc = $no + 1;
                }
                $code = "CST".sprintf("%05s", $no_inc);

                $status_group   = 0; if (isset($json->status_group)) { $status_group  = 1; };
                $status_record  = 0; if (isset($json->status_record)){ $status_record = 1; };
                $customer = new Customer([
                    'no_inc'        => $no_inc,
                    'code'          => $code,
                    'accounting'    => $json->account,
                    'fax'           => !empty($json->fax) ? $json->fax : '',
                    'email'         => !empty($json->email) ? $json->email : '',
                    'tax'           => ($json->tax == 1) ? 1 : 0,
                    'npwp_no'       => !empty($json->npwp_no) ? $json->npwp_no : '',
                    'npwp_name'     => !empty($json->npwp_name) ? $json->npwp_name : '',
                    'npwp_address'  => !empty($json->npwp_address) ? $json->npwp_address : '',
                    'npwp_city'     => !empty($json->npwp_city) ? $json->npwp_city : 0,
                    'limit'         => $json->limit,
                    'note1'         => !empty($json->note1) ? $json->note1 : '',
                    'note2'         => !empty($json->note2) ? $json->note2 : '',
                    'note3'         => !empty($json->note3) ? $json->note3 : '',
                    'notice'        => !empty($json->notice) ? $json->notice : '',
                    'status_group'  => $status_group,
                    'status_record' => $status_record,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $customer->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $customer = Customer::find($id);
        $customerdetails = Customerdetail::where('customer', $id)->get();
        $customeraccounts = Customeraccount::where('customer', $id)->get();
        $accounts = Account::where('status', 1)->get();
        $citys = City::where('status', 1)->get();
        $company = Company::first();

        return view('vendor/adminlte/customer.show', compact('id','customer','customerdetails','customeraccounts','accounts','citys','company','disable'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $customer = Customer::find($id);
        $accounts = Account::where('status', 1)->get();
        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/customer.edit', compact('id','customer','accounts','citys','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                if (!empty($json->email)) {
                    $request->validate([
                        'email' => 'email:true'
                    ]);
                }

                $customer = Customer::find($id);
                $customer->accounting    = $json->account;
                $customer->fax           = !empty($json->fax) ? $json->fax : '';
                $customer->email         = !empty($json->email) ? $json->email : '';
                $customer->tax           = ($json->tax == 1) ? 1 : 0;
                $customer->npwp_no       = !empty($json->npwp_no) ? $json->npwp_no : '';
                $customer->npwp_name     = !empty($json->npwp_name) ? $json->npwp_name : '';
                $customer->npwp_address  = !empty($json->npwp_address) ? $json->npwp_address : '';
                $customer->npwp_city     = !empty($json->npwp_city) ? $json->npwp_city : 0;
                $customer->limit         = $json->limit;
                $customer->note1         = !empty($json->note1) ? $json->note1 : '';
                $customer->note2         = !empty($json->note2) ? $json->note2 : '';
                $customer->note3         = !empty($json->note3) ? $json->note3 : '';
                $customer->notice        = !empty($json->notice) ? $json->notice : '';
                $customer->status_group  = ($json->status_group == 1) ? 1 : 0;
                $customer->status_record = ($json->status_record == 1) ? 1 : 0;
                $customer->updated_user  = $session_id;
                $result = $customer->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $customer = Customer::find($id);
        $status = $customer->status;

        if ($status == 1) {
        	$customer->status       = 0;
            $customer->updated_user = $session_id;
	        $result = $customer->save();

        } elseif ($status == 0) {
        	$customer->status       = 1;
            $customer->updated_user = $session_id;
	        $result = $customer->save();

        };
        return $this->jsonSuccess( $result );
    }
}
