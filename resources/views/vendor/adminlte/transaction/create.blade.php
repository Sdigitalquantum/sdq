@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">New Transaction</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	 <form  id="countryFrm" > <!--method="post" action="{{ route('transaction.store') }}"> -->
						   @method('POST')
					      	@csrf
				          	<div class="form-group">
				              	<label for="department">Department</label>
				              	<select class="form-control select2" name="department" required>
				              		<option value="{{ old('department') }}">Choose One</option>
			                    	@foreach($departments as $department)
									    <option value="{{ $department->id }}">{{ $department->code }} - {{ $department->name }}</option>
									@endforeach
			                	</select>
				          	</div>
				          	<div class="form-group">
				              	<label for="account">Accounting</label>
				              	<select class="form-control select2" name="account" required>
				              		<option value="{{ old('account') }}">Choose One</option>
			                    	@foreach($accounts as $account)
									    <option value="{{ $account->id }}">{{ $account->code }} [{{ $account->name }}]</option>
									@endforeach
			                	</select>
				          	</div>
				          	<div class="form-group">
				              	<label for="code">Code</label>
				              	<input type="text" class="form-control" name="code" value="{{ old('code') }}" required>
				          	</div>
				          	<div class="form-group">
				              	<label for="name">Name</label>
				              	<input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
				          	</div>
				          	<div class="form-group">
				          		<label for="check">&nbsp;</label>
				              	<input type="checkbox" name="status_group" value="1"> Status Group &nbsp;&nbsp;&nbsp;
				              	<input type="checkbox" name="status_record" value="1"> Status Record
			              	</div>						          		
			          		<div class="form-group">
			          		<button id="saveBtn" value="{{ route('transaction.store') }}" class="btn btn-sm btn-success">Save</button>
				       		<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
			          		</div>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.transaction.prepare_create());
    </script>
@stop