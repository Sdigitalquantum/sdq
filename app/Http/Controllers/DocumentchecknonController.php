<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Documentcheck;
use App\Http\Models\Document;
use App\Http\Models\Schedulestuff;
use App\Http\Models\Schedulebook;
use App\Http\Models\Quotation;
use App\Http\Models\Quotationsplit;
use App\Http\Models\Employeeset;

class DocumentchecknonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $schedulestuffs = Quotationsplit::where([['status','<>',0], ['status_stuff', 1], ['status_send', 1], ['status_pack', 1]])->orderBy('quotation','desc')->get();

        return view('vendor/adminlte/documentchecknon.index', compact('schedulestuffs','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $schedulestuff = Quotationsplit::find($id);
        $quotation = Quotation::where('id', $schedulestuff->quotation)->first();
        
        if ($quotation->status_flow == 1) {
            $documents = Document::where([['export', 1], ['status', 1]])->get();
            foreach ($documents as $document) {
                $cek = Documentcheck::where([['quotationsplit', $id], ['document', $document->id]])->first();
                if (empty($cek)) {
                    $documentcheck = new Documentcheck([
                        'schedulestuff'     => 0,
                        'quotationsplit'    => $id,
                        'document'          => $document->id,
                        'status'            => 0,
                        'created_user'      => $session_id,
                        'updated_user'      => $session_id
                    ]);
                    $documentcheck->save();
                }
            }
        } elseif ($quotation->status_flow == 2) {
            $documents = Document::where([['local', 1], ['status', 1]])->get();
            foreach ($documents as $document) {
                $cek = Documentcheck::where([['quotationsplit', $id], ['document', $document->id]])->first();
                if (empty($cek)) {
                    $documentcheck = new Documentcheck([
                        'schedulestuff'     => 0,
                        'quotationsplit'    => $id,
                        'document'          => $document->id,
                        'status'            => 0,
                        'created_user'      => $session_id,
                        'updated_user'      => $session_id
                    ]);
                    $documentcheck->save();
                }
            }
        } elseif ($quotation->status_flow == 3) {
            $documents = Document::where([['internal', 1], ['status', 1]])->get();
            foreach ($documents as $document) {
                $cek = Documentcheck::where([['quotationsplit', $id], ['document', $document->id]])->first();
                if (empty($cek)) {
                    $documentcheck = new Documentcheck([
                        'schedulestuff'     => 0,
                        'quotationsplit'    => $id,
                        'document'          => $document->id,
                        'status'            => 0,
                        'created_user'      => $session_id,
                        'updated_user'      => $session_id
                    ]);
                    $documentcheck->save();
                }
            }
        }

        $documentchecks = Documentcheck::where('quotationsplit', $id)->orderBy('document')->get();

        return view('vendor/adminlte/documentchecknon.edit', compact('schedulestuff','documentchecks','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $documentcheck = Documentcheck::find($id);
        $documentcheck->notice = !empty($request->get('notice')) ? $request->get('notice') : '';
        $documentcheck->status = ($request->get('status') == 1) ? 1 : 0;
        $documentcheck->save();

        $schedulestuff = Quotationsplit::where('id', $documentcheck->quotationsplit)->first();
        $schedulestuff->status_document = 1;
        $schedulestuff->save();

        return redirect('/documentchecknon/'.$documentcheck->quotationsplit.'/edit')->with('success', 'Document has been checked');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
