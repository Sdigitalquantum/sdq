<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quotationdetail')->unsigned();
            $table->integer('stockin')->unsigned();
            $table->integer('no_inc');
            $table->string('no_batch');
            $table->integer('qty_plan');
            $table->tinyInteger('status_split')->default('0');
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->foreign('quotationdetail')->references('id')->on('quotationdetails');
            $table->foreign('stockin')->references('id')->on('stockins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
