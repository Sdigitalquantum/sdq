@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Approval Booking Schedule</h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="5%">No</th>
			          	<th width="15%">Split Proforma</th>
			          	<th width="15%">Schedule</th>
			          	<th width="10%">Lines</th>
			          	<th width="25%">Note sdq</th>
			          	<th width="20%">Reason EMKL</th>
			          	<th width="10%">Action</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@php ($no = 0)
			    	@foreach($schedulebooks as $schedulebook)
			    		@php ($no++)
			        	<tr>
				            <td>{{$no}}</td>
				            <td>{{$schedulebook->fkQuotationsplit->no_split}}</td>
				            <td>
				            	POL : {{$schedulebook->pol}} <br>
				            	POD : {{$schedulebook->pod}} <br>
				            	ETD : {{$schedulebook->etd}} <br>
				            	ETA : {{$schedulebook->eta}}
				            </td>
				            <td>{{$schedulebook->lines}}</td>
				            <td><pre>{{$schedulebook->notice}}</pre></td>
				            <td><pre>{{$schedulebook->reason}}</pre></td>
							
							@if($schedulebook->status_approve == 0)
								<td>
					            	<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal-approve{{$schedulebook->id}}">Approve</button> &nbsp;
					                <button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal-reject{{$schedulebook->id}}">Reject</button>

					            	<form action="{{ url('schedulebook/approve', $schedulebook->id)}}" method="post">
					                  	@csrf
					                  	@method('DELETE')
					                  	<div class="modal fade bs-example-modal-approve{{$schedulebook->id}}" tabindex="-1" role="dialog" aria-hidden="true">
		                                    <div class="modal-dialog modal-sm">
		                                        <div class="modal-content">
		                                            <div class="modal-header btn-success">
		                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
		                                                </button>
		                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Approve</h4>
		                                            </div>
		                                            <div class="modal-body">
		                                            	Are you sure to approve this data?
		                                            	<br><textarea class="form-control" name="reason" cols="30" rows="3" maxlength="150" required></textarea>
		                                            </div>
		                                            <div class="modal-footer">
		                                                <button type="submit" class="btn btn-success">Yes</button>
		                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
					                </form>

					            	<form action="{{ url('schedulebook/reject', $schedulebook->id)}}" method="post">
					                  	@csrf
					                  	@method('DELETE')
					                  	<div class="modal fade bs-example-modal-reject{{$schedulebook->id}}" tabindex="-1" role="dialog" aria-hidden="true">
		                                    <div class="modal-dialog modal-sm">
		                                        <div class="modal-content">
		                                            <div class="modal-header btn-danger">
		                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
		                                                </button>
		                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Reject</h4>
		                                            </div>
		                                            <div class="modal-body">
		                                            	Are you sure to reject this data?
		                                            	<br><textarea class="form-control" name="reason" cols="30" rows="3" maxlength="150" required></textarea>
		                                            </div>
		                                            <div class="modal-footer">
		                                                <button type="submit" class="btn btn-success">Yes</button>
		                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
					                </form>
				            	</td>
			            	@elseif($schedulebook->status_approve == 1)
			            		<td><font color="green">Approved <i class="glyphicon glyphicon-ok"></i></font></td>
		            		@elseif($schedulebook->status_approve == 2)
			            		<td><font color="red">Rejected <i class="glyphicon glyphicon-remove"></i></font></td>
							@endif
				        </tr>
			        @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			          	<td class="noShow"></td>
			          	<td>Split Proforma</td>
			          	<td>Schedule</td>
			          	<td>Lines</td>
			          	<td>Note sdq</td>
			          	<td>Reason EMKL</td>
			          	<td>Action</td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	</div>
@stop