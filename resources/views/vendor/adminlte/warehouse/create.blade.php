@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #6b6e70 !important; font-weight: bold; color: white;">
			    		<h4 class="box-title">New Warehouse</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form id="warehouseFrm">
				          	@csrf
				          	@method('POST')
				          	<div class="form-group">
				              	<label for="code">Code</label>
				              	<input type="text" class="form-control" name="code" disabled>
				          	</div>
				          	<div class="form-group">
				              	<label for="name">Name</label>
				              	<input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
				          	</div>
				          	<div class="form-group">
				              	<label for="address">Address</label>
				              	<input type="text" class="form-control" name="address" value="{{ old('address') }}" required>
				          	</div>
				          	<div class="form-group">
				          		<label for="city">City</label>
				              	<select class="form-control select2" name="city" required>
				              		<option value="{{ old('city') }}">Choose One</option>
			                    	@foreach($citys as $city)
									    <option value="{{ $city->id }}">{{ $city->name }} - {{$city->fkCountry->name}}</option>
									@endforeach
			                	</select>
				          	</div>
				          	<button id="saveBtn" value="{{ route('warehouse.store') }}" class="btn btn-sm btn-success">Save</button>
				          	<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.warehouse.prepare_create());
    </script>
@stop