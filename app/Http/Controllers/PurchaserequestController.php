<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Purchaserequest;
use App\Http\Models\Quotationdetail;
use App\Http\Models\Product;
use App\Http\Models\Supplierdetail;
use App\Http\Models\Warehouse;
use App\Http\Models\Letter;
use App\Http\Models\Company;
use App\Http\Models\Employeeset;
use App\Http\Models\Employeewarehouse;

class PurchaserequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $purchaserequests = Purchaserequest::orderBy('created_at','desc')->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/purchaserequest.index', compact('purchaserequests','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $products = Product::where([['status', 1], ['general', 0]])->get();
        $supplierdetails = Supplierdetail::where('status', 1)->get();
        $warehouses = Warehouse::where('status', 1)->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/purchaserequest.create', compact('id','products','supplierdetails','warehouses','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        $request->validate([
            'qty_request' => 'numeric|min:0|not_in:0'
        ]);

        if (!empty($request->get('quotationdetail'))) {
            $quotationdetail = Quotationdetail::where('id', $request->get('quotationdetail'))->first();

            if ($request->get('qty_request') > $quotationdetail->qty_amount) {
                return redirect()->back()->withErrors('Qty Request greater than Qty Amount');
            } else {
                $no = Purchaserequest::select('no_inc')->whereMonth('date_request', date('m'))->whereYear('date_request', date('Y'))->max('no_inc');
                if (empty($no)) {
                    $no_inc = 1;
                } else {
                    $no_inc = $no + 1;
                }

                $letter = Letter::where('menu', 'Purchase Request')->first();
                if (empty($letter->company)) {
                    $company = "";
                } else {
                    $company = $letter->company."/";
                }

                if (empty($letter->code)) {
                    $code = "";
                } else {
                    $code = $letter->code;
                }

                if ($letter->romawi == 0) {
                    $romawi = "";
                } else {
                    if (date("m") == 1) {
                        $romawi = "I/";
                    } elseif (date("m") == 2) {
                        $romawi = "II/";
                    } elseif (date("m") == 3) {
                        $romawi = "III/";
                    } elseif (date("m") == 4) {
                        $romawi = "IV/";
                    } elseif (date("m") == 5) {
                        $romawi = "V/";
                    } elseif (date("m") == 6) {
                        $romawi = "VI/";
                    } elseif (date("m") == 7) {
                        $romawi = "VII/";
                    } elseif (date("m") == 8) {
                        $romawi = "VIII/";
                    } elseif (date("m") == 9) {
                        $romawi = "IX/";
                    } elseif (date("m") == 10) {
                        $romawi = "X/";
                    } elseif (date("m") == 11) {
                        $romawi = "XI/";
                    } elseif (date("m") == 12) {
                        $romawi = "XII/";
                    }
                }

                if ($letter->year == 0) {
                    $year = "";
                } else {
                    $year = date("Y");
                }

                $no_pr = $company.$code.sprintf("%05s", $no_inc)."/".$romawi.$year;

                $purchaserequest = new Purchaserequest([
                    'product'           => $quotationdetail->product,
                    'quotationdetail'   => $request->get('quotationdetail'),
                    'supplierdetail'    => $request->get('supplierdetail'),
                    'warehouse'         => $request->get('warehouse'),
                    'no_inc'            => $no_inc,
                    'no_pr'             => $no_pr,
                    'date_request'      => $request->get('date_request'),
                    'qty_request'       => $request->get('qty_request'),
                    'qty_remain'        => $request->get('qty_request'),
                    'no_reference'      => $quotationdetail->fkQuotation->no_sp,
                    'notice'            => !empty($request->get('notice')) ? $request->get('notice') : '',
                    'reason'            => !empty($request->get('reason')) ? $request->get('reason') : '',
                    'approve'           => 1,
                    'status_approve'    => 1,
                    'status_plan'       => 1,
                    'created_user'      => $session_id,
                    'updated_user'      => $session_id
                ]);
                $purchaserequest->save();

                $quotationdetail->status_book   = 1;
                $quotationdetail->qty_book      = $quotationdetail->qty_book+$request->get('qty_request');
                $quotationdetail->save();

                return redirect('/plan/create/'.$request->get('quotationdetail'))->with('success', 'Booking Order has been added');
            }
        } else {
            $no = Purchaserequest::select('no_inc')->whereMonth('date_request', date('m'))->whereYear('date_request', date('Y'))->max('no_inc');
            if (empty($no)) {
                $no_inc = 1;
            } else {
                $no_inc = $no + 1;
            }

            $letter = Letter::where('menu', 'Purchase Request')->first();
            if (empty($letter->company)) {
                $company = "";
            } else {
                $company = $letter->company."/";
            }

            if (empty($letter->code)) {
                $code = "";
            } else {
                $code = $letter->code;
            }

            if ($letter->romawi == 0) {
                $romawi = "";
            } else {
                if (date("m") == 1) {
                    $romawi = "I/";
                } elseif (date("m") == 2) {
                    $romawi = "II/";
                } elseif (date("m") == 3) {
                    $romawi = "III/";
                } elseif (date("m") == 4) {
                    $romawi = "IV/";
                } elseif (date("m") == 5) {
                    $romawi = "V/";
                } elseif (date("m") == 6) {
                    $romawi = "VI/";
                } elseif (date("m") == 7) {
                    $romawi = "VII/";
                } elseif (date("m") == 8) {
                    $romawi = "VIII/";
                } elseif (date("m") == 9) {
                    $romawi = "IX/";
                } elseif (date("m") == 10) {
                    $romawi = "X/";
                } elseif (date("m") == 11) {
                    $romawi = "XI/";
                } elseif (date("m") == 12) {
                    $romawi = "XII/";
                }
            }

            if ($letter->year == 0) {
                $year = "";
            } else {
                $year = date("Y");
            }

            $no_pr = $company.$code.sprintf("%05s", $no_inc)."/".$romawi.$year;

            $purchaserequest = new Purchaserequest([
                'product'           => $request->get('product'),
                'quotationdetail'   => 0,
                'supplierdetail'    => $request->get('supplierdetail'),
                'warehouse'         => $request->get('warehouse'),
                'no_inc'            => $no_inc,
                'no_pr'             => $no_pr,
                'date_request'      => $request->get('date_request'),
                'qty_request'       => $request->get('qty_request'),
                'qty_remain'        => $request->get('qty_request'),
                'no_reference'      => '',
                'notice'            => !empty($request->get('notice')) ? $request->get('notice') : '',
                'reason'            => !empty($request->get('reason')) ? $request->get('reason') : '',
                'approve'           => 1,
                'status_approve'    => 0,
                'status_plan'       => 0,
                'created_user'      => $session_id,
                'updated_user'      => $session_id
            ]);
            $purchaserequest->save();

            return redirect('/purchaserequest')->with('success', 'Purchase Request has been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $purchaserequest = Purchaserequest::find($id);
        $products = Product::where('status', 1)->get();
        $supplierdetails = Supplierdetail::where('status', 1)->get();
        $warehouses = Warehouse::where('status', 1)->get();
        $company = Company::first();

        return view('vendor/adminlte/purchaserequest.show', compact('purchaserequest','products','supplierdetails','warehouses','company','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $purchaserequest = Purchaserequest::find($id);
        $products = Product::where([['status', 1], ['general', 0]])->get();
        $supplierdetails = Supplierdetail::where('status', 1)->get();
        $warehouses = Warehouse::where('status', 1)->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/purchaserequest.edit', compact('purchaserequest','products','supplierdetails','warehouses','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $request->validate([
            'qty_request' => 'numeric|min:0|not_in:0'
        ]);

        $purchaserequest = Purchaserequest::find($id);
        $purchaserequest->product           = $request->get('product');
        $purchaserequest->quotationdetail   = 0;
        $purchaserequest->supplierdetail    = $request->get('supplierdetail');
        $purchaserequest->warehouse         = $request->get('warehouse');
        $purchaserequest->date_request      = $request->get('date_request');
        $purchaserequest->qty_request       = $request->get('qty_request');
        $purchaserequest->notice            = !empty($request->get('notice')) ? $request->get('notice') : '';
        $purchaserequest->reason            = !empty($request->get('reason')) ? $request->get('reason') : '';
        $purchaserequest->status_plan       = 0;
        $purchaserequest->created_user      = $session_id;
        $purchaserequest->save();

        return redirect('/purchaserequest')->with('success', 'Purchase Request has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $purchaserequest = Purchaserequest::find($id);
        $status = $purchaserequest->status;

        if ($status == 1) {
            $purchaserequest->status       = 0;
            $purchaserequest->updated_user = $session_id;
            $purchaserequest->save();

            return redirect('/purchaserequest')->with('error', 'Purchase Request has been deleted');
        } elseif ($status == 0) {
            $purchaserequest->status       = 1;
            $purchaserequest->updated_user = $session_id;
            $purchaserequest->save();

            return redirect('/purchaserequest')->with('success', 'Purchase Request has been activated');
        }
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function set(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $purchaserequest = Purchaserequest::find($id);
        $approve = $purchaserequest->approve;

        if ($approve == 1) {
            $purchaserequest->approve           = 0;
            $purchaserequest->status_approve    = 1;
            $purchaserequest->reason            = 'Set None Approval';
            $purchaserequest->updated_user      = $session_id;
            $purchaserequest->save();

            return redirect('/purchaserequest')->with('error', 'Purchase Request has been set none approval');
        } elseif ($approve == 0) {
            $purchaserequest->approve           = 1;
            $purchaserequest->status_approve    = 0;
            $purchaserequest->reason            = '';
            $purchaserequest->updated_user      = $session_id;
            $purchaserequest->save();

            return redirect('/purchaserequest')->with('success', 'Purchase Request has been set used approval');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function approval(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $purchaserequests = Purchaserequest::where([['status', 1], ['approve', 1], ['status_release', 1], ['status_plan', 0]])->orderBy('created_at','desc')->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/purchaserequest.approval', compact('purchaserequests','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }
    
    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $purchaserequest = Purchaserequest::find($id);
        $purchaserequest->status_approve    = 1;
        $purchaserequest->reason            = $request->get('reason');
        $purchaserequest->approved_at       = new \DateTime();
        $purchaserequest->approved_user     = $session_id;
        $purchaserequest->save();

        return redirect('/approvalpr')->with('success', 'Purchase Request has been approved');
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reject(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $purchaserequest = Purchaserequest::find($id);
        $purchaserequest->status_approve    = 2;
        $purchaserequest->reason            = $request->get('reason');
        $purchaserequest->approved_at       = new \DateTime();
        $purchaserequest->approved_user     = $session_id;
        $purchaserequest->save();

        return redirect('/approvalpr')->with('error', 'Purchase Request has been rejected');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $purchaserequests = Purchaserequest::where([['status', 1], ['status_approve', 1]])->orderBy('created_at','desc')->get();

        return view('vendor/adminlte/purchaserequest.list', compact('purchaserequests','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function release(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $purchaserequest = Purchaserequest::find($id);
        $purchaserequest->status_release  = 1;
        $purchaserequest->updated_user    = $session_id;
        $purchaserequest->save();

        return redirect('/purchaserequest')->with('success', 'Purchase Request has been released');
    }
}
