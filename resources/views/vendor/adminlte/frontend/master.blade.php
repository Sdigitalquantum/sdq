<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>S.D.Q</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" href="{{ asset('sdq_logo.jpg') }}">
    
    <link rel="stylesheet" href="{{ asset('vendor/caliber/css/pace-loading-bar.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/caliber/css/animate.caliber.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/caliber/css/materialdesignicons.caliber.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/caliber/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/caliber/css/jquery.fancybox.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/caliber/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/caliber/css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/caliber/css/owl.transitions.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/caliber/css/style.css') }}">
    
    @yield('adminlte_css')
</head>
<body class="angled yellow">

@yield('body')

<script src="{{ asset('vendor/caliber/js/pace.min.js') }}"></script>
<script src="{{ asset('vendor/caliber/js/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/caliber/js/inviewport-1.3.2.js') }}"></script>
<script src="{{ asset('vendor/caliber/js/jquery.mixitup.min.js') }}"></script>
<script src="{{ asset('vendor/caliber/js/jquery.fancybox.pack.js') }}"></script>
<script src="{{ asset('vendor/caliber/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('vendor/caliber/js/main.js') }}"></script>



@yield('adminlte_js')

</body>
</html>
