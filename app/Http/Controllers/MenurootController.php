<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Menuroot;
use App\Http\Models\Groupset;
use App\Http\Models\Employeeset;

class MenurootController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $no = Menuroot::select('nomor')->max('nomor');
        if (empty($no)) {
            $nomor = 1;
        } else {
            $nomor = $no + 1;
        }

        return view('vendor/adminlte/menuroot.create', compact('nomor','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $cek = Menuroot::where('nomor', $request->get('nomor'))->first();

        if ($request->get('url') != '#') {
            $request->validate([
                'url' => 'unique:menuroots'
            ]);
        }

        $request->validate([
            'nomor' => 'numeric|min:0|not_in:0'
        ]);

        if (!empty($cek)) {
            return redirect()->back()->withErrors('Nomor already exist');
        } else {
            $menuroot = new Menuroot([
                'name'          => $request->get('name'),
                'url'           => !empty($request->get('url')) ? $request->get('url') : '#',
                'icon'          => !empty($request->get('icon')) ? $request->get('icon') : 'folder',
                'icon_color'    => !empty($request->get('icon_color')) ? $request->get('icon_color') : 'purple',
                'nomor'         => $request->get('nomor'),
                'created_user'  => $session_id,
                'updated_user'  => $session_id
            ]);
            $menuroot->save();

            return redirect('/menu')->with('success', 'Root Menu has been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $menuroot = Menuroot::find($id);

        return view('vendor/adminlte/menuroot.edit', compact('menuroot','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
    	$menuroot = Menuroot::find($id);

        if ($menuroot->url != $request->get('url')) {
            if ($request->get('url') != '#') {
                $request->validate([
                    'url' => 'unique:menus'
                ]);
            }
        }

        $request->validate([
            'nomor' => 'numeric|min:0|not_in:0'
        ]);

        if ($menuroot->nomor != $request->get('nomor')) {
            $ceks = Menuroot::where([['id','<>',$id], ['nomor','>=',$request->get('nomor')]])->orderBy('nomor','asc')->get();
            foreach ($ceks as $cek) {
                $cek->nomor = $cek->nomor+1;
                $cek->save();
            }

            $menuroot->name            = $request->get('name');
            $menuroot->url             = !empty($request->get('url')) ? $request->get('url') : '#';
            $menuroot->icon            = !empty($request->get('icon')) ? $request->get('icon') : 'folder';
            $menuroot->icon_color      = !empty($request->get('icon_color')) ? $request->get('icon_color') : 'purple';
            $menuroot->nomor           = $request->get('nomor');
            $menuroot->updated_user    = $session_id;
            $menuroot->save();

            return redirect('/menu')->with('success', 'Root Menu has been updated');
        } else {
            $menuroot->name            = $request->get('name');
            $menuroot->url             = !empty($request->get('url')) ? $request->get('url') : '#';
            $menuroot->icon            = !empty($request->get('icon')) ? $request->get('icon') : 'folder';
            $menuroot->icon_color      = !empty($request->get('icon_color')) ? $request->get('icon_color') : 'purple';
            $menuroot->updated_user    = $session_id;
            $menuroot->save();

            return redirect('/menu')->with('success', 'Root Menu has been updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $menuroot = Menuroot::find($id);
        $status = $menuroot->status;

        if ($status == 1) {
            $cek = Groupset::where('menuroot', $id)->first();
            if (!empty($cek)) {
                return redirect()->back()->withErrors('Root Menu already used');
            } else {
                $menuroot->status       = 0;
                $menuroot->updated_user = $session_id;
                $menuroot->save();

                return redirect('/menu')->with('error', 'Root Menu has been deleted');
            }
        } elseif ($status == 0) {
        	$menuroot->status       = 1;
            $menuroot->updated_user = $session_id;
	        $menuroot->save();

		    return redirect('/menu')->with('success', 'Root Menu has been activated');
        }
    }
}
