<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Stockdetail;
use App\Http\Models\Stockqc;
use App\Http\Models\Product;
use App\Http\Models\Warehouse;
use App\Http\Models\Supplierdetail;
use App\Http\Models\Employeeset;

class StockdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $stockqc = Stockqc::find($id);
        $stockdetails = Stockdetail::where('stockqc', $id)->get();
        $products = Product::where('status', 1)->get();
        $warehouses = Warehouse::where('status', 1)->get();
        $supplierdetails = Supplierdetail::where('status', 1)->get();

        $no = Stockdetail::select('no_inc')->where('stockqc', $id)->max('no_inc');
        if (empty($no)) {
            $no_inc = 1;
        } else {
            $no_inc = $no+1;
        }

        $tgl = explode("-", $stockqc->date_in);
        $no_bag = $stockqc->fkSupplierdetail->fkSupplier->code.$tgl[0].$tgl[1].$tgl[2].sprintf("%03s", $no_inc);

        return view('vendor/adminlte/stockdetail.create', compact('stockqc','stockdetails','products','warehouses','supplierdetails','no_inc','no_bag','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        $request->validate([
            'qty_bag' => 'numeric|min:0|not_in:0',
            'qty_pcs' => 'numeric|min:0|not_in:0',
            'qty_kg'  => 'numeric|min:0|not_in:0'
        ]);

        $stockdetail = new Stockdetail([
            'stockqc'           => $request->get('stockqc'),
            'no_inc'            => $request->get('no_inc'),
            'no_bag'            => $request->get('no_bag'),
            'qty_bag'           => $request->get('qty_bag'),
            'qty_pcs'           => $request->get('qty_pcs'),
            'qty_kg'            => $request->get('qty_kg'),
            'barcode'           => '',
            'created_user'      => $session_id,
            'updated_user'      => $session_id
        ]);
        $stockdetail->save();

        $stockqc = Stockqc::find($request->get('stockqc'));
        $stockqc->detail_count = $stockqc->detail_count+$request->get('qty_bag');
        $stockqc->detail_total = $stockqc->detail_total+$request->get('qty_kg');
        $stockqc->save();

        return redirect('/stockdetail/create/'.$request->get('stockqc'))->with('success', 'Detail Quality Control has been added');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $stockdetail = Stockdetail::find($id);
        $stockqc = Stockqc::where('id', $stockdetail->stockqc)->first();
        $stockdetails = Stockdetail::where('stockqc', $stockdetail->stockqc)->get();
        $products = Product::where('status', 1)->get();
        $warehouses = Warehouse::where('status', 1)->get();
        $supplierdetails = Supplierdetail::where('status', 1)->get();

        $no = Stockdetail::select('no_inc')->where('stockqc', $id)->max('no_inc');
        if (empty($no)) {
            $no_inc = 1;
        } else {
            $no_inc = $no+1;
        }

        $tgl = explode("-", $stockqc->date_in);
        $no_bag = $stockqc->fkSupplierdetail->fkSupplier->code.$tgl[0].$tgl[1].$tgl[2].sprintf("%03s", $no_inc);

        return view('vendor/adminlte/stockdetail.edit', compact('stockqc','stockdetail','stockdetails','products','warehouses','supplierdetails','no_inc','no_bag','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $stockdetail = Stockdetail::find($id);

        $request->validate([
            'qty_bag' => 'numeric|min:0|not_in:0',
            'qty_pcs' => 'numeric|min:0|not_in:0',
            'qty_kg'  => 'numeric|min:0|not_in:0'
        ]);

        $stockqc = Stockqc::find($stockdetail->stockqc);
        $stockqc->detail_count = $stockqc->detail_count+$request->get('qty_bag')-$stockdetail->qty_bag;
        $stockqc->detail_total = $stockqc->detail_total+$request->get('qty_kg')-$stockdetail->qty_kg;
        $stockqc->save();

        $stockdetail->qty_bag       = $request->get('qty_bag');
        $stockdetail->qty_pcs       = $request->get('qty_pcs');
        $stockdetail->qty_kg        = $request->get('qty_kg');
        $stockdetail->updated_user  = $session_id;
        $stockdetail->save();

        return redirect('/stockdetail/create/'.$stockdetail->stockqc)->with('success', 'Detail Quality Control has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stockdetail = Stockdetail::find($id);
        $stockqc = Stockqc::where('id', $stockdetail->stockqc)->first();
        $stockqc->detail_count = $stockqc->detail_count-$stockdetail->qty_bag;
        $stockqc->detail_total = $stockqc->detail_total-$stockdetail->qty_kg;
        $stockqc->save();
        $stockdetail->delete();

        return redirect('/stockdetail/create/'.$stockdetail->stockqc)->with('error', 'Detail Quality Control has been deleted');
    }
}
