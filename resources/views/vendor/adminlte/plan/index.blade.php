@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>Plan</a></li>
            <li class="disabled"><a>Booking</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
                <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Quotation Detail</h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="15%">Quotation</th>
						          	<th width="30%">Product</th>
						          	<th width="10%">Qty Need (Kg)</th>
						          	<th width="10%">Qty Plan (Kg)</th>
						          	<th width="10%">Qty Booking (Kg)</th>
						          	<th width="10%">Qty Other (Kg)</th>
						          	<th width="10%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($quotationdetails as $quotationdetail)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>{{$quotationdetail->fkQuotation->no_sp}}</td>
							            <td>
							            	@if(empty($quotationdetail->alias_name))
							            		{{$quotationdetail->fkProduct->name}}
						            		@else
						            			{{$quotationdetail->alias_name}}
						            		@endif
							            </td>
							            <td style="text-align: right;">
							            	{{$quotationdetail->qty_kg}}
							            	@if($quotationdetail->qty_amount == 0)
							            		<i class="glyphicon glyphicon-ok"></i>
							            	@endif &nbsp;
							            </td>
							            <td style="text-align: right;">{{$quotationdetail->qty_plan}} &nbsp;</td>
							            <td style="text-align: right;">{{$quotationdetail->qty_book}} &nbsp;</td>
							            <td style="text-align: right;">{{$quotationdetail->qty_other}} &nbsp;</td>
										<td>
											@if($quotationdetail->qty_amount == 0)
												<a href="{{ route('plan.show',$quotationdetail->id)}}" class="btn btn-xs btn-primary">Show</a>
											@elseif($quotationdetail->qty_amount != 0 && $quotationdetail->status_other == 1)
												<a href="{{ route('plan.edit',$quotationdetail->id)}}" class="btn btn-xs btn-success">Plan</a>
											@else
												<a href="{{ route('plan.edit',$quotationdetail->id)}}" class="btn btn-xs btn-success">Plan</a> &nbsp;
								            	<a href="{{ url('plan/create',$quotationdetail->id)}}" class="btn btn-xs btn-warning">Booking</a> &nbsp;
								            	
								            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$quotationdetail->id}}">Other</button>
												<form action="{{ url('plan/other', $quotationdetail->id)}}" method="post">
								                  	@csrf
								                  	@method('DELETE')
								                  	<div class="modal fade bs-example-modal{{$quotationdetail->id}}" tabindex="-1" role="dialog" aria-hidden="true">
					                                    <div class="modal-dialog modal-sm">
					                                        <div class="modal-content">
					                                            <div class="modal-header btn-danger">
					                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
					                                                </button>
					                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
					                                            </div>
					                                            <div class="modal-body">
					                                                Are you sure to complete this data?
					                                            </div>
					                                            <div class="modal-footer">
					                                                <button type="submit" class="btn btn-success">Yes</button>
					                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
					                                            </div>
					                                        </div>
					                                    </div>
					                                </div>
								                </form>
											@endif
							            </td>
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Quotation</td>
						          	<td>Product</td>
						          	<td>Qty Need</td>
						          	<td>Qty Plan</td>
						          	<td>Qty Booking</td>
						          	<td>Qty Other</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop