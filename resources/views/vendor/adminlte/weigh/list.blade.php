@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Packing List - Shipping</h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="5%">No</th>
			          	<th width="15%">Split Proforma</th>
			          	<th width="15%">Schedule</th>
			          	<th width="15%">Stuffing</th>
			          	<th width="25%">Note sdq</th>
			          	<th width="20%">Reason EMKL</th>
			          	<th width="5%">Action</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@php ($no = 0)
			    	@foreach($schedulestuffs as $schedulestuff)
			    		@php ($no++)
			        	<tr>
				            <td>{{$no}}</td>
				            <td>{{$schedulestuff->fkSchedulebook->fkQuotationsplit->no_split}}</td>
				            <td>
				            	POL : {{$schedulestuff->fkSchedulebook->pol}} <br>
				            	POD : {{$schedulestuff->fkSchedulebook->pod}} <br>
				            	ETD : {{$schedulestuff->fkSchedulebook->etd}} <br>
				            	ETA : {{$schedulestuff->fkSchedulebook->eta}}
				            </td>
				            <td>
				            	Date : {{$schedulestuff->date}} <br>
				            	PIC &nbsp;: {{$schedulestuff->person}} <br>
				            	Qty Fcl : {{$schedulestuff->qty_approve}}
				            </td>
				            <td><pre>{{$schedulestuff->notice}}</pre></td>
				            <td><pre>{{$schedulestuff->reason}}</pre></td>
							<td>
								@if($schedulestuff->status_reroute == 1)
									<font color="red">Re-Route <i class="glyphicon glyphicon-remove"></i></font>
								@elseif($schedulestuff->status_reschedule == 1)
									<font color="red">Canceled <i class="glyphicon glyphicon-remove"></i></font>
								@else
									@if($schedulestuff->status_send == 0)
										<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$schedulestuff->id}}">Sending</button>

										<form action="{{ route('weigh.destroy', $schedulestuff->id)}}" method="post">
						                  	@csrf
						                  	@method('DELETE')
						                  	<div class="modal fade bs-example-modal{{$schedulestuff->id}}" tabindex="-1" role="dialog" aria-hidden="true">
			                                    <div class="modal-dialog modal-sm">
			                                        <div class="modal-content">
			                                            <div class="modal-header btn-success">
			                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
			                                                </button>
			                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
			                                            </div>
			                                            <div class="modal-body">
			                                            	Are you sure to send this data?
			                                            </div>
			                                            <div class="modal-footer">
			                                                <button type="submit" class="btn btn-success">Yes</button>
			                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
						                </form>
									@else
										@if($schedulestuff->status_pack == 0)
											<font color="blue">Waiting Confirmation <i class="glyphicon glyphicon-ok"></i></font>
										@else
											<font color="green">Confirmed <i class="glyphicon glyphicon-ok"></i></font>
										@endif
									@endif
	            				@endif
		            		</td>
				        </tr>
			        @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			          	<td class="noShow"></td>
			          	<td>Split Proforma</td>
			          	<td>Schedule</td>
			          	<td>Stuffing</td>
			          	<td>Note sdq</td>
			          	<td>Reason EMKL</td>
			          	<td class="noShow"></td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	</div>
@stop