<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceivablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receivables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_sp');
            $table->string('code_customer');
            $table->string('name_customer');
            $table->string('code_product');
            $table->string('name_product');
            $table->date('date_sp');
            $table->date('date_sj');
            $table->integer('qty');
            $table->bigInteger('price');
            $table->bigInteger('total');
            $table->bigInteger('payment');
            $table->bigInteger('adjustment');
            $table->tinyInteger('status')->default('0');
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receivables');
    }
}
