<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Stockqc;
use App\Http\Models\Stockin;
use App\Http\Models\Product;
use App\Http\Models\Warehouse;
use App\Http\Models\Supplierdetail;
use App\Http\Models\Letter;
use App\Http\Models\Employeeset;
use App\Http\Models\Employeewarehouse;

class PurchasederivativeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $stockqcs = Stockqc::where('no_inc','<>',0)->orderBy('created_at','desc')->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/purchasederivative.index', compact('stockqcs','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $stockqc = Stockqc::find($id);
        $stockqcs = Stockqc::where('purchasedetail', $stockqc->purchasedetail)->get();
        $products = Product::where('status', 1)->get();
        $warehouses = Warehouse::where('status', 1)->get();
        $supplierdetails = Supplierdetail::where('status', 1)->get();

        return view('vendor/adminlte/purchasederivative.edit', compact('stockqc','stockqcs','products','warehouses','supplierdetails','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $request->validate([
            'price' => 'numeric|min:0|not_in:0'
        ]);

        $stockqc = Stockqc::find($id);
        $stockqc->price             = $request->get('price');
        $stockqc->notice            = $request->get('notice');
        $stockqc->updated_user      = $session_id;
        $stockqc->save();

        return redirect('/purchasederivative')->with('success', 'Purchase Derivative has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function set(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $stockqc = Stockqc::find($id);
        $approve = $stockqc->approve;

        if ($approve == 1) {
            $stockqc->approve           = 0;
            $stockqc->status_approve    = 1;
            $stockqc->reason            = 'Set None Approval';
            $stockqc->updated_user      = $session_id;
            $stockqc->save();

            return redirect('/purchasederivative')->with('error', 'Purchase Derivative has been set none approval');
        } elseif ($approve == 0) {
            $stockqc->approve           = 1;
            $stockqc->status_approve    = 0;
            $stockqc->reason            = '';
            $stockqc->updated_user      = $session_id;
            $stockqc->save();

            return redirect('/purchasederivative')->with('success', 'Purchase Derivative has been set used approval');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function approval(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $stockqcs = Stockqc::where([['no_inc','<>',0], ['approve', 1], ['status_release', 1]])->orderBy('created_at','desc')->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/purchasederivative.approval', compact('stockqcs','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }
    
    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $stockqc = Stockqc::find($id);
        $stockqc->status_approve    = 1;
        $stockqc->reason            = $request->get('reason');
        $stockqc->approved_at       = new \DateTime();
        $stockqc->approved_user     = $session_id;
        $stockqc->save();

        $no = Stockin::select('no_inc')->whereYear('created_at', date('Y'))->max('no_inc');
        if (empty($no)) {
            $no_inc = 1;
        } else {
            $no_inc = $no + 1;
        }

        $letter = Letter::where('menu', 'Incoming Stock')->first();
        if (empty($letter->company)) {
            $company = "";
        } else {
            $company = $letter->company."/";
        }

        if (empty($letter->code)) {
            $code = "";
        } else {
            $code = $letter->code;
        }

        if ($letter->romawi == 0) {
            $romawi = "";
        } else {
            if (date("m") == 1) {
                $romawi = "I/";
            } elseif (date("m") == 2) {
                $romawi = "II/";
            } elseif (date("m") == 3) {
                $romawi = "III/";
            } elseif (date("m") == 4) {
                $romawi = "IV/";
            } elseif (date("m") == 5) {
                $romawi = "V/";
            } elseif (date("m") == 6) {
                $romawi = "VI/";
            } elseif (date("m") == 7) {
                $romawi = "VII/";
            } elseif (date("m") == 8) {
                $romawi = "VIII/";
            } elseif (date("m") == 9) {
                $romawi = "IX/";
            } elseif (date("m") == 10) {
                $romawi = "X/";
            } elseif (date("m") == 11) {
                $romawi = "XI/";
            } elseif (date("m") == 12) {
                $romawi = "XII/";
            }
        }

        if ($letter->year == 0) {
            $year = "";
        } else {
            $year = date("Y");
        }

        $nomor = $company.$code.sprintf("%05s", $no_inc)."/".$romawi.$year;

        $stockin = new Stockin([
            'transaction'       => 1,
            'product'           => $stockqc->product,
            'warehouse'         => $stockqc->warehouse,
            'supplierdetail'    => $stockqc->supplierdetail,
            'no_inc'            => $no_inc,
            'nomor'             => $nomor,
            'date_in'           => $stockqc->date_in,
            'price'             => $stockqc->price,
            'qty_bag'           => $stockqc->qty_bag,
            'qty_pcs'           => $stockqc->qty_pcs,
            'qty_kg'            => $stockqc->qty_kg,
            'qty_available'     => $stockqc->qty_kg,
            'noref_in'          => $stockqc->no_po,
            'transaction_prod'  => 0,
            'status_move'       => 0,
            'created_user'      => $session_id,
            'updated_user'      => $session_id
        ]);
        $stockin->save();

        return redirect('/approvalpd')->with('success', 'Purchase Derivative has been approved');
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reject(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $stockqc = Stockqc::find($id);
        $stockqc->status_approve    = 2;
        $stockqc->reason            = $request->get('reason');
        $stockqc->approved_at       = new \DateTime();
        $stockqc->approved_user     = $session_id;
        $stockqc->save();

        return redirect('/approvalpd')->with('error', 'Purchase Derivative has been rejected');
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function release(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $stockqc = Stockqc::find($id);
        $stockqc->status_release  = 1;
        $stockqc->updated_user    = $session_id;
        $stockqc->save();

        return redirect('/purchasederivative')->with('success', 'Purchase Derivative has been released');
    }
}
