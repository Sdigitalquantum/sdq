@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>Stuffing</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
				<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Booking Schedule &nbsp;<a href="{{ url('schedulestufflist') }}" class="btn btn-xs btn-primary" style="text-decoration: none;">Stuffing Schedule</a></h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="15%">Split Proforma</th>
						          	<th width="15%">Schedule</th>
						          	<th width="10%">Lines</th>
						          	<th width="25%">Note sdq</th>
						          	<th width="20%">Reason EMKL</th>
						          	<th width="10%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($schedulebooks as $schedulebook)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>{{$schedulebook->fkQuotationsplit->no_split}}</td>
							            <td>
							            	POL : {{$schedulebook->pol}} <br>
							            	POD : {{$schedulebook->pod}} <br>
							            	ETD : {{$schedulebook->etd}} <br>
							            	ETA : {{$schedulebook->eta}}
							            </td>
							            <td>{{$schedulebook->lines}}</td>
							            <td><pre>{{$schedulebook->notice}}</pre></td>
							            <td><pre>{{$schedulebook->reason}}</pre></td>
										
										<td>
											@if($schedulebook->status_stuff_acc == 0)
												@if($schedulebook->status_stuff == 0)
													<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$schedulebook->id}}">Stuffing</button>
												@else
													<button type="button" class="btn btn-xs btn-warning" data-toggle='modal' data-target=".bs-example-modal{{$schedulebook->id}}">Stuffed <i class="glyphicon glyphicon-ok"></i></button>
												@endif

								            	<form action="{{ url('schedulestuff/change', $schedulebook->id)}}" method="post">
								                  	@csrf
								                  	@method('DELETE')
								                  	<div class="modal fade bs-example-modal{{$schedulebook->id}}" tabindex="-1" role="dialog" aria-hidden="true">
					                                    <div class="modal-dialog modal-sm">
					                                        <div class="modal-content">
					                                            <div class="modal-header btn-success">
					                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
					                                                </button>
					                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Stuffing</h4>
					                                            </div>
					                                            <div class="modal-body">
					                                            	<input type="hidden" name="schedulebook" value="{{ $schedulebook->id }}">
					                                            	@if(count($schedulestuffs) == 0)
					                                            		<table width="100%">
															          		<tr>
															          			<td width="20%">ETD</td>
															          			<td width="80%">
															          				<input type="text" class="form-control" value="{{ $schedulebook->etd }}" disabled>
															          				<input type="hidden" name="etd" value="{{ $schedulebook->etd }}">
															          			</td>
														          			</tr>
														          			<tr><td colspan="2">&nbsp;</td></tr>
														          			<tr>
															          			<td width="20%">Date</td>
															          			<td width="80%"><input type="text" class="form-control datepicker" name="date" value="{{ old('date') }}" required></td>
														          			</tr>
														          			<tr><td colspan="2">&nbsp;</td></tr>
														          			<tr>
															          			<td>Qty Fcl</td>
															          			<td><input type="number" class="form-control" name="qty_fcl" value="{{ old('qty_fcl') }}" required></td>
														          			</tr>
														          			<tr><td colspan="2">&nbsp;</td></tr>
														          			<tr>
															          			<td>Person</td>
															          			<td><input type="text" class="form-control" name="person" value="{{ old('person') }}" required></td>
														          			</tr>
														          			<tr><td colspan="2">&nbsp;</td></tr>
														          			<tr>
															          			<td>Note</td>
															          			<td><textarea class="form-control" name="notice" cols="25" rows="3" maxlength="150">{{ old('notice') }}</textarea></td>
														          			</tr>
														          		</table>
													          		@else
						                                            	@foreach($schedulestuffs as $schedulestuff)						                                            	
															          		@if($schedulestuff->schedulebook == $schedulebook->id)
														          				<table width="100%">
														          					<tr>
																	          			<td width="20%">ETD</td>
																	          			<td width="80%">
																	          				<input type="text" class="form-control" value="{{ $schedulebook->etd }}" disabled>
																	          				<input type="hidden" name="etd" value="{{ $schedulebook->etd }}">
																	          			</td>
																          			</tr>
																          			<tr><td colspan="2">&nbsp;</td></tr>
																	          		<tr>
																	          			<td width="20%">Date</td>
																	          			<td width="80%"><input type="text" class="form-control datepicker" name="date" value="{{ $schedulestuff->date }}" required></td>
																          			</tr>
																          			<tr><td colspan="2">&nbsp;</td></tr>
																          			<tr>
																	          			<td>Qty Fcl</td>
																	          			<td><input type="number" class="form-control" name="qty_fcl" value="{{ $schedulestuff->qty_fcl }}" required></td>
																          			</tr>
																          			<tr><td colspan="2">&nbsp;</td></tr>
																          			<tr>
																	          			<td>Person</td>
																	          			<td><input type="text" class="form-control" name="person" value="{{ $schedulestuff->person }}" required></td>
																          			</tr>
																          			<tr><td colspan="2">&nbsp;</td></tr>
																          			<tr>
																	          			<td>Note</td>
																	          			<td><textarea class="form-control" name="notice" cols="25" rows="3" maxlength="150">{{ $schedulestuff->notice }}</textarea></td>
																          			</tr>
																          		</table>
															          		@else
														          				<table width="100%">
														          					<tr>
																	          			<td width="20%">ETD</td>
																	          			<td width="80%">
																	          				<input type="text" class="form-control" value="{{ $schedulebook->etd }}" disabled>
																	          				<input type="hidden" name="etd" value="{{ $schedulebook->etd }}">
																	          			</td>
																          			</tr>
																          			<tr><td colspan="2">&nbsp;</td></tr>
																	          		<tr>
																	          			<td width="20%">Date</td>
																	          			<td width="80%"><input type="text" class="form-control datepicker" name="date" value="{{ old('date') }}" required></td>
																          			</tr>
																          			<tr><td colspan="2">&nbsp;</td></tr>
																          			<tr>
																	          			<td>Qty Fcl</td>
																	          			<td><input type="number" class="form-control" name="qty_fcl" value="{{ old('qty_fcl') }}" required></td>
																          			</tr>
																          			<tr><td colspan="2">&nbsp;</td></tr>
																          			<tr>
																	          			<td>Person</td>
																	          			<td><input type="text" class="form-control" name="person" value="{{ old('person') }}" required></td>
																          			</tr>
																          			<tr><td colspan="2">&nbsp;</td></tr>
																          			<tr>
																	          			<td>Note</td>
																	          			<td><textarea class="form-control" name="notice" cols="25" rows="3" maxlength="150">{{ old('notice') }}</textarea></td>
																          			</tr>
																          		</table>
														          			@endif
														          		@endforeach
													          		@endif
					                                            </div>
					                                            <div class="modal-footer">
					                                                <button type="submit" class="btn btn-success">Yes</button>
					                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
					                                            </div>
					                                        </div>
					                                    </div>
					                                </div>
								                </form>
							                @elseif($schedulebook->status_stuff_acc == 1)
							                	<font color="green">Stuffing Approved <i class="glyphicon glyphicon-ok"></i></font>
							                @elseif($schedulebook->status_stuff_acc == 2)
							                	<font color="red">Stuffing Rejected <i class="glyphicon glyphicon-remove"></i></font>
							                @endif
										</td>
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Split Proforma</td>
						          	<td>Schedule</td>
						          	<td>Lines</td>
						          	<td>Note sdq</td>
						          	<td>Reason EMKL</td>
						          	<td>Action</td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop