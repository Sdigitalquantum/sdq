@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Report Plan &nbsp;<a href="{{ url('exportplan')}}" class="btn btn-xs btn-default" style="text-decoration: none;"><i class="fa fa-file-excel-o"></i></a></h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			        	<th width="5%">No</th>
			          	<th width="15%">No. Batch</th>
			          	<th width="15%">Product</th>
			          	<th width="15%">Warehouse</th>
			          	<th width="15%">Supplier</th>
			          	<th width="15%">Time Departure</th>
			          	<th width="15%">Time Arrival</th>
			          	<th width="5%" style="text-align: center;">Qty (Kg)</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@php ($array = [])
			    	@foreach($employeewarehouses as $employeewarehouse)
			    		@php ($array[] = $employeewarehouse->warehouse)
			    	@endforeach

			    	@php ($no = 0)
			    	@foreach($plans as $plan)
			    		@if(in_array($plan->fkStockin->warehouse, $array))
				    		@php ($no++)
				    		<tr>
					            <td style="text-align: center;">{{$no}}</td>
					            <td>{{$plan->no_batch}}</td>
					            <td>{{$plan->fkStockin->fkProduct->name}}</td>
					            <td>{{$plan->fkStockin->fkWarehouse->name}}</td>
					            <td>{{$plan->fkStockin->fkSupplierdetail->name}}</td>
					            <td>{{$plan->fkQuotationdetail->fkQuotation->etd}}</td>
					            <td>{{$plan->fkQuotationdetail->fkQuotation->eta}}</td>
					            <td style="text-align: right;">{{number_format($plan->qty_plan, 0, ',' , '.')}} &nbsp;</td>
					        </tr>
				        @endif
			        @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			          	<td class="noShow"></td>
			          	<td>No. Batch</td>
			          	<td>Product</td>
			          	<td>Warehouse</td>
			          	<td>Supplier</td>
			          	<td>Time Departure</td>
			          	<td>Time Arrival</td>
			          	<td>Qty (Kg)</td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	<div>
@stop