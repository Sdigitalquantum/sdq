<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Quotation;
use App\Http\Models\Quotationdetail;
use App\Http\Models\Quotationpayment;
use App\Http\Models\Quotationcharge;
use App\Http\Models\Customer;
use App\Http\Models\Customerdetail;
use App\Http\Models\Company;
use App\Http\Models\Companyaccount;
use App\Http\Models\Paymentterm;
use App\Http\Models\Container;
use App\Http\Models\Port;
use App\Http\Models\Currency;
use App\Http\Models\Sales;
use App\Http\Models\Letter;
use App\Http\Models\Employeeset;

class QuotationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $quotations = Quotation::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/quotation.index', compact('quotations','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $customerdetails = Customerdetail::where('status', 1)->get();
        $companyaccounts = Companyaccount::where('status', 1)->get();
        $paymentterms = Paymentterm::where('status', 1)->get();
        $containers = Container::where('status', 1)->get();
        $ports = Port::where('status', 1)->get();
        $currencys = Currency::where('status', 1)->get();
        $saless = Sales::where('status', 1)->get();

        return view('vendor/adminlte/quotation.create', compact('customerdetails','companyaccounts','paymentterms','containers','ports','currencys','saless','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        $request->validate([
            'qty_gross' => 'numeric|min:0|not_in:0',
            'qty_cbm'   => 'numeric|min:0|not_in:0',
            'kurs'      => 'nullable|numeric|min:0',
            'disc_value'   => 'nullable|numeric|min:0'
        ]);

        $no = Quotation::select('no_inc')->whereYear('date_sp', date('Y'))->max('no_inc');
        if (empty($no)) {
            $no_inc = 1;
        } else {
            $no_inc = $no + 1;
        }

        $letter = Letter::where('menu', 'Quotation')->first();
        if (empty($letter->company)) {
            $company = "";
        } else {
            $company = $letter->company."/";
        }

        if (empty($letter->code)) {
            $code = "";
        } else {
            $code = $letter->code;
        }

        if ($letter->romawi == 0) {
            $romawi = "";
        } else {
            if (date("m") == 1) {
                $romawi = "I/";
            } elseif (date("m") == 2) {
                $romawi = "II/";
            } elseif (date("m") == 3) {
                $romawi = "III/";
            } elseif (date("m") == 4) {
                $romawi = "IV/";
            } elseif (date("m") == 5) {
                $romawi = "V/";
            } elseif (date("m") == 6) {
                $romawi = "VI/";
            } elseif (date("m") == 7) {
                $romawi = "VII/";
            } elseif (date("m") == 8) {
                $romawi = "VIII/";
            } elseif (date("m") == 9) {
                $romawi = "IX/";
            } elseif (date("m") == 10) {
                $romawi = "X/";
            } elseif (date("m") == 11) {
                $romawi = "XI/";
            } elseif (date("m") == 12) {
                $romawi = "XII/";
            }
        }

        if ($letter->year == 0) {
            $year = "";
        } else {
            $year = date("Y");
        }

        $no_sp = $company.$code.sprintf("%05s", $no_inc)."/".$romawi.$year;

        if ($request->get('eta') < $request->get('etd')) {
            return redirect()->back()->withErrors('Time Arrival lower than Time Departure');
        } else {
            $quotation = new Quotation([
                'customerdetail'    => $request->get('customerdetail'),
                'companyaccount'    => $request->get('companyaccount'),
                'paymentterm'       => $request->get('paymentterm'),
                'container'         => $request->get('container'),
                'pol'               => $request->get('pol'),
                'pod'               => $request->get('pod'),
                'currency'          => $request->get('currency'),
                'sales'             => $request->get('sales'),
                'tax'               => !empty($request->get('tax')) ? $request->get('tax') : 0,
                'status_flow'       => $request->get('status_flow'),
                'status_sp'         => 1,
                'no_inc'            => $no_inc,
                'no_sp'             => $no_sp,
                'date_sp'           => $request->get('date_sp'),
                'no_poc'            => !empty($request->get('no_poc')) ? $request->get('no_poc') : '',
                'disc_type'         => !empty($request->get('disc_type')) ? $request->get('disc_type') : 0,
                'disc_value'        => !empty($request->get('disc_value')) ? $request->get('disc_value') : '',
                'notice'            => !empty($request->get('notice')) ? $request->get('notice') : '',
                'etd'               => $request->get('etd'),
                'eta'               => $request->get('eta'),
                'qty_gross'         => $request->get('qty_gross'),
                'qty_cbm'           => $request->get('qty_cbm'),
                'kurs'              => !empty($request->get('kurs')) ? $request->get('kurs') : 0,
                'vessel'            => !empty($request->get('vessel')) ? $request->get('vessel') : '',
                'created_user'      => $session_id,
                'updated_user'      => $session_id
            ]);
            $quotation->save();

            if (!empty($request->get('notice'))) {
                $customerdetail = Customerdetail::where('id', $request->get('customerdetail'))->first();
                $customer = Customer::where('id', $customerdetail->customer)->first();
                $customer->notice = $request->get('notice');
                $customer->save();
            }

            return redirect('/quotationdetail/'.$quotation->id)->with('success', 'Quotation has been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $quotation = Quotation::find($id);
        $quotation->qty_print = $quotation->qty_print+1;
        $quotation->save();
        
        $quotationdetails = Quotationdetail::where([['quotation', $id], ['status', 1]])->get();
        $quotationpayments = Quotationpayment::where([['quotation', $id], ['status', 1]])->orderBy('payment','desc')->get();
        $quotationcharges = Quotationcharge::where([['quotation', $id], ['status', 1]])->get();
        $company = Company::where('status', 1)->first();
        $currencys = Currency::where('status', 1)->get();
        $saless = Sales::where('status', 1)->get();

        return view('vendor/adminlte/quotation.show', compact('id','quotation','quotationdetails','quotationpayments','quotationcharges','company','currencys','saless','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $quotation = Quotation::find($id);
        $customerdetails = Customerdetail::where('status', 1)->get();
        $companyaccounts = Companyaccount::where('status', 1)->get();
        $paymentterms = Paymentterm::where('status', 1)->get();
        $containers = Container::where('status', 1)->get();
        $ports = Port::where('status', 1)->get();
        $currencys = Currency::where('status', 1)->get();
        $saless = Sales::where('status', 1)->get();

        return view('vendor/adminlte/quotation.edit', compact('id','quotation','customerdetails','companyaccounts','paymentterms','containers','ports','currencys','saless','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $request->validate([
            'qty_gross' => 'numeric|min:0|not_in:0',
            'qty_cbm'   => 'numeric|min:0|not_in:0',
            'kurs'      => 'nullable|numeric|min:0',
            'disc_value'   => 'nullable|numeric|min:0'
        ]);

        if ($request->get('eta') < $request->get('etd')) {
            return redirect()->back()->withErrors('Time Arrival lower than Time Departure');
        } else {
            $quotation = Quotation::find($id);
            $quotation->customerdetail    = $request->get('customerdetail');
            $quotation->companyaccount    = $request->get('companyaccount');
            $quotation->paymentterm       = $request->get('paymentterm');
            $quotation->container         = $request->get('container');
            $quotation->pol               = $request->get('pol');
            $quotation->pod               = $request->get('pod');
            $quotation->currency          = $request->get('currency');
            $quotation->sales             = $request->get('sales');
            $quotation->tax               = !empty($request->get('tax')) ? $request->get('tax') : 0;
            $quotation->status_flow       = $request->get('status_flow');
            $quotation->status_sp         = 1;
            $quotation->date_sp           = $request->get('date_sp');
            $quotation->no_poc            = !empty($request->get('no_poc')) ? $request->get('no_poc') : '';
            $quotation->disc_type         = !empty($request->get('disc_type')) ? $request->get('disc_type') : 0;
            $quotation->disc_value        = !empty($request->get('disc_value')) ? $request->get('disc_value') : '';
            $quotation->notice            = !empty($request->get('notice')) ? $request->get('notice') : '';
            $quotation->etd               = $request->get('etd');
            $quotation->eta               = $request->get('eta');
            $quotation->qty_gross         = $request->get('qty_gross');
            $quotation->qty_cbm           = $request->get('qty_cbm');
            $quotation->kurs              = !empty($request->get('kurs')) ? $request->get('kurs') : 0;
            $quotation->vessel            = !empty($request->get('vessel')) ? $request->get('vessel') : '';
            $quotation->updated_user      = $session_id;
            $quotation->save();

            if (!empty($request->get('notice'))) {
                $customerdetail = Customerdetail::where('id', $request->get('customerdetail'))->first();
                $customer = Customer::where('id', $customerdetail->customer)->first();
                $customer->notice = $request->get('notice');
                $customer->save();
            }

            return redirect('/quotation')->with('success', 'Quotation has been updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $quotation = Quotation::find($id);
        $quotation->status       = 0;
        $quotation->status_close = 1;
        $quotation->updated_user = $session_id;
        $quotation->save();

	    return redirect('/quotation')->with('error', 'Quotation has been closed');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ajax($id_url, $id)
    {
        $detail = Customerdetail::where('id', $id)->first();
        $notice = Customer::where('id', $detail->customer)->first();
        
        return json_encode($notice);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function dummy(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $quotations = Quotation::where([['payment_term', 100], ['status_product', 1], ['status', 1], ['status_release', 1]])->orderBy('created_at','desc')->get();

        return view('vendor/adminlte/quotation.dummy', compact('quotations','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function release(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $quotation = Quotation::find($id);
        $quotation->status_release  = 1;
        $quotation->updated_user    = $session_id;
        $quotation->save();

        return redirect('/quotation')->with('success', 'Quotation has been released');
    }
}
