<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
	    'accounting',
	    'no_inc',
	    'code',
	    'fax',
	    'email',
	    'tax',
	    'npwp_no',
	    'npwp_name',
	    'npwp_address',
	    'npwp_city',
	    'limit',
	    'note1',
	    'note2',
	    'note3',
	    'notice',
	    'status_group',
	    'status_record',	
	    'created_user',
	    'updated_user'
  	];

  	public function fkAccount(){
	    return $this->belongsTo('\App\Http\Models\Account', 'accounting', 'id');
	}

	public function fkCustomerdetail(){
	    return $this->belongsTo('\App\Http\Models\Customerdetail', 'id', 'customer');
	}
}
