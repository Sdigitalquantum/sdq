<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Quotationcharge extends Model
{
    protected $fillable = [
	    'quotation',
	    'name',
	    'type',
	    'value',
	    'created_user',
	    'updated_user'
  	];

  	public function fkQuotation(){
	    return $this->belongsTo('\App\Http\Models\Quotation', 'quotation', 'id');
	}
}
