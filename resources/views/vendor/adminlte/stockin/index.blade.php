@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>New</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
                <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Incoming Stock &nbsp;<a href="{{ route('stockin.create')}}" class="btn btn-xs btn-success" style="text-decoration: none;">New</a></h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="10%">Transaction</th>
						          	<th width="20%">Product</th>
						          	<th width="15%">Warehouse</th>
						          	<th width="20%">Supplier</th>
						          	<th width="10%">Date</th>
						          	<th width="5%">Qty Bag</th>
						          	<th width="5%">Qty Pcs</th>
						          	<th width="5%">Qty Kg</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($array = [])
						    	@foreach($employeewarehouses as $employeewarehouse)
						    		@php ($array[] = $employeewarehouse->warehouse)
						    	@endforeach

						    	@php ($no = 0)
						    	@foreach($stockins as $stockin)
						    		@if(in_array($stockin->warehouse, $array))
							    		@php ($no++)
							        	<tr>
								            <td>{{$no}}</td>
								            <td>{{$stockin->fkTransaction->code}}</td>
								            <td>{{$stockin->fkProduct->name}}</td>
								            <td>{{$stockin->fkWarehouse->name}}</td>
								            <td>{{$stockin->fkSupplierdetail->name}}</td>
								            <td>{{$stockin->date_in}}</td>
								            <td style="text-align: right;">{{number_format($stockin->qty_bag, 0, ',' , '.')}} &nbsp;</td>
								            <td style="text-align: right;">{{number_format($stockin->qty_pcs, 0, ',' , '.')}} &nbsp;</td>
								            <td style="text-align: right;">{{number_format($stockin->qty_kg, 0, ',' , '.')}} &nbsp;</td>

								            @if($stockin->status_validate == 1)
								            	@if($stockin->qty_available == 0)
									            	@if($stockin->qty_out == 0)
									            		@if($stockin->status_move == 1)
										            		<td><font color="blue">Moved <i class="glyphicon glyphicon-ok"></i></font></td>
									            		@else
									            			<td><font color="blue">Used <i class="glyphicon glyphicon-ok"></i></font></td>
								            			@endif
								            		@else
								            			<td><font color="red">Out <i class="glyphicon glyphicon-ok"></i></font></td>
							            			@endif
									            @else
									            	<td><font color="green">Valid <i class="glyphicon glyphicon-ok"></i></font></td>
									            @endif
						            		@else
						            			@if($stockin->price == 0)
						            				<td><font color="blue">Waiting Price <i class="glyphicon glyphicon-ok"></i></font></td>
						            			@else
							            			<td>
										            	<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$stockin->id}}">Validation</button>

										            	<form action="{{ url('stockin/validation', $stockin->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$stockin->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-success">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Validation</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to validate this data?
							                                                
							                                                <br><br>Type <br>
							                                                <select class="form-control select2" name="journal" required>
															              		<option value="{{ old('journal') }}">Choose One</option>
														                    	@foreach($journals as $journal)
																				    <option value="{{ $journal->id }}">{{ $journal->name }}</option>
																				@endforeach
														                	</select>
							                                                <br><br>QC Date <br><input type="hidden" name="date_qc" value="{{ $stockin->date_in }}"><input type="text" class="form-control datepicker" name="date_qc" value="{{ $stockin->date_in }}" disabled>
							                                                <br><br>Date <br><input type="text" class="form-control datepicker" name="date_in" value="{{ old('date_in') }}">
														                	<br><br>Qty Bag <br><input type="number" class="form-control" name="qty_bag" value="{{ old('qty_bag') }}">
														                	<br><br>Qty Pcs <br><input type="number" class="form-control" name="qty_pcs" value="{{ old('qty_pcs') }}">
														                	<br><br>Qty Kg <br><input type="number" class="form-control" name="qty_kg" value="{{ old('qty_kg') }}">
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
									                </td>
								                @endif
					            			@endif
				            			@endif
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Transaction</td>
						          	<td>Product</td>
						          	<td>Warehouse</td>
						          	<td>Supplier</td>
						          	<td>Date</td>
						          	<td>Qty Bag</td>
						          	<td>Qty Pcs</td>
						          	<td>Qty Kg</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop