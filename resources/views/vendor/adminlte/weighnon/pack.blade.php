@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>Print</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
				<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Packing List - Non Shipping</h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="20%">Split Proforma</th>
						          	<th width="20%">Port Loading</th>
						          	<th width="20%">Port Discharge</th>
						          	<th width="15%">Time Departure</th>
						          	<th width="15%">Time Arrival</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($schedulestuffs as $schedulestuff)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>{{$schedulestuff->no_split}}</td>
							            <td>{{$schedulestuff->fkPortl->name}} - {{$schedulestuff->fkPortl->fkCountry->name}}</td>
							            <td>{{$schedulestuff->fkPortd->name}} - {{$schedulestuff->fkPortd->fkCountry->name}}</td>
							            <td>{{$schedulestuff->etd}}</td>
							            <td>{{$schedulestuff->eta}}</td>
										<td style="text-align: center;">
											@if($schedulestuff->status == 1)
												<a href="{{ url('weighnon/packlist/preview',$schedulestuff->id)}}" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-print"></i></a>
											@else
												<font color="red">Re-Route <i class="glyphicon glyphicon-remove"></i></font>
											@endif
										</td>
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Split Proforma</td>
						          	<td>Port Loading</td>
						          	<td>Port Discharge</td>
						          	<td>Time Departure</td>
						          	<td>Time Arrival</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop