<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Customeraccount;
use App\Http\Models\Currency;
use App\Http\Models\Bank;
use App\Http\Models\Employeeset;

class CustomeraccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $customeraccount = new Customeraccount([
                    'customer'      => $json->customer,
                    'currency'      => $json->currency,
                    'bank'          => $json->bank,
                    'account_no'    => $json->account_no,
                    'account_swift' => $json->account_swift,
                    'account_name'  => $json->account_name,
                    'alias'         => !empty($json->alias) ? $json->alias : '',
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $customeraccount->save();

            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $customeraccounts = Customeraccount::where('customer', $id)->orderBy('created_at','desc')->get();
        $currencys = Currency::where('status', 1)->get();
        $banks = Bank::where('status', 1)->get();

        return view('vendor/adminlte/customeraccount.show', compact('id','customeraccounts','currencys','banks','disable'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $customeraccount = Customeraccount::find($id);
        $customeraccounts = Customeraccount::where('customer', $customeraccount->customer)->orderBy('created_at','desc')->get();
        $currencys = Currency::where('status', 1)->get();
        $banks = Bank::where('status', 1)->get();

        return view('vendor/adminlte/customeraccount.edit', compact('customeraccount','customeraccounts','currencys','banks','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $customeraccount = Customeraccount::find($id);
                $customeraccount->customer      = $json->customer;
                $customeraccount->currency      = $json->currency;
                $customeraccount->bank          = $json->bank;
                $customeraccount->account_no    = $json->account_no;
                $customeraccount->account_swift = $json->account_swift;
                $customeraccount->account_name  = $json->account_name;
                $customeraccount->alias         = !empty($request->get('alias')) ? $request->get('alias') : '';
                $customeraccount->updated_user  = $session_id;
                $result = $customeraccount->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $customeraccount = Customeraccount::find($id);
        $status = $customeraccount->status;

        if ($status == 1) {
        	$customeraccount->status       = 0;
            $customeraccount->updated_user = $session_id;
	        $result = $customeraccount->save();

        } elseif ($status == 0) {
        	$customeraccount->status       = 1;
            $customeraccount->updated_user = $session_id;
	        $result = $customeraccount->save();

        };
        return $this->jsonSuccess( $result );
    }
}
