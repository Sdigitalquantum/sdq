@extends('adminlte::page')

@section('adminlte_css')
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('sdq.skin', 'blue') . '.min.css')}} ">
          
  	<style>
		btn-breadcrumb>.btn.disabled {
		    opacity: 1 !important;
		    color: #999;
		}

		.btn-breadcrumb .btn:not(:last-child):after {
			content: " ";
			display: block;
			width: 0;
			height: 0;
			border-top: 17px solid transparent;
			border-bottom: 17px solid transparent;
			border-left: 10px solid white;
			position: absolute;
			top: 50%;
			margin-top: -17px;
			left: 100%;
			z-index: 3;
		}
		.btn-breadcrumb .btn:not(:last-child):before {
			content: " ";
			display: block;
			width: 0;
			height: 0;
			border-top: 17px solid transparent;
			border-bottom: 17px solid transparent;
			border-left: 10px solid rgb(173, 173, 173);
			position: absolute;
			top: 50%;
			margin-top: -17px;
			margin-left: 1px;
			left: 100%;
			z-index: 3;
		}

		/** The Spacing **/
		.btn-breadcrumb .btn {
		  	padding:6px 12px 6px 24px;
		}
		.btn-breadcrumb .btn:first-child {
		  	padding:6px 6px 6px 10px;
		}
		.btn-breadcrumb .btn:last-child {
		  	padding:6px 18px 6px 24px;
		}

		/** Primary button **/
		.btn-breadcrumb .btn.btn-primary:not(:last-child):after {
		  	border-left: 10px solid #428bca;
		}
		.btn-breadcrumb .btn.btn-primary:not(:last-child):before {
		  	border-left: 10px solid #357ebd;
		}
		.btn-breadcrumb .btn.btn-primary:hover:not(:last-child):after {
		  	border-left: 10px solid #3276b1;
		}
		.btn-breadcrumb .btn.btn-primary:hover:not(:last-child):before {
		  	border-left: 10px solid #285e8e;
		}

		/** Success button **/
		.btn-breadcrumb .btn.btn-success:not(:last-child):after {
		  	border-left: 10px solid #5cb85c;
		}
		.btn-breadcrumb .btn.btn-success:not(:last-child):before {
		  	border-left: 10px solid #4cae4c;
		}
		.btn-breadcrumb .btn.btn-success:hover:not(:last-child):after {
		  	border-left: 10px solid #47a447;
		}
		.btn-breadcrumb .btn.btn-success:hover:not(:last-child):before {
		  	border-left: 10px solid #398439;
		}

		/** Danger button **/
		.btn-breadcrumb .btn.btn-danger:not(:last-child):after {
		  	border-left: 10px solid #d9534f;
		}
		.btn-breadcrumb .btn.btn-danger:not(:last-child):before {
		  	border-left: 10px solid #d43f3a;
		}
		.btn-breadcrumb .btn.btn-danger:hover:not(:last-child):after {
		  	border-left: 10px solid #d2322d;
		}
		.btn-breadcrumb .btn.btn-danger:hover:not(:last-child):before {
		  	border-left: 10px solid #ac2925;
		}

		/** Warning button **/
		.btn-breadcrumb .btn.btn-warning:not(:last-child):after {
		  	border-left: 10px solid #ffff00;
		}
		.btn-breadcrumb .btn.btn-warning:not(:last-child):before {
		  	border-left: 10px solid #eea236;
		}
		.btn-breadcrumb .btn.btn-warning:hover:not(:last-child):after {
		  	border-left: 10px solid #ed9c28;
		}
		.btn-breadcrumb .btn.btn-warning:hover:not(:last-child):before {
		  	border-left: 10px solid #d58512;
		}

		.badge {
	        display: inline-block;
	        min-width: 10px;
	        padding: 3px 7px;
	        font-size: 12px;
	        font-weight: bold;
	        line-height: 1;
	        color: #fff;
	        text-align: center;
	        white-space: nowrap;
	        vertical-align: baseline;
	        background-color: #777;
	        border-radius: 10px;
	    }
	</style>

    @stack('css')
    @yield('css')
@stop

@section('content')
	<div class="row">
    	<div class="col-md-6">
	    	<div class="box box-primary">
			  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			  		<h4 class="box-title">EMKL</h4>
		  		</div>

		  		<div class="box-body">
			    	<div class="row">
						<div class="col-lg-3 col-xs-6">
				          	<div class="small-box bg-green">
				            	<div class="inner">
					              	<h3>{{ $scheduleorder }}</h3>
					              	<p>&nbsp;</p>
					            </div>
				            
					            <div class="icon">
					              	<i class="ion ion-android-car"></i>
					            </div>
					            <a href="{{ url('/stuffconfirm') }}" class="small-box-footer">Going Order <i class="fa fa-arrow-circle-right"></i></a>
				          	</div>
				        </div>

				        <div class="col-lg-3 col-xs-6">
				          	<div class="small-box bg-red">
				            	<div class="inner">
					              	<h3>{{ $goingorder }}</h3>
					              	<p>&nbsp;</p>
					            </div>
				            
					            <div class="icon">
					              	<i class="ion ion-android-calendar"></i>
					            </div>
					            <a href="{{ url('/schedulebook') }}" class="small-box-footer">Schedule Order <i class="fa fa-arrow-circle-right"></i></a>
				          	</div>
				        </div>
			        </div>
				</div>
			</div>
		</div>

		<div class="col-md-6">
	    	<div class="box box-primary">
			  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			  		<h4 class="box-title">Sales 456</h4>
		  		</div>

		  		<div class="box-body">
			    	<div class="row">
						<div class="col-lg-3 col-xs-6">
				          	<div class="small-box bg-yellow">
				            	<div class="inner">
					              	<h3>{{ $bookingorder }}</h3>
					              	<p>&nbsp;</p>
					            </div>
				            
					            <div class="icon">
					              	<i class="ion ion-android-phone-portrait"></i>
					            </div>
					            <a href="{{ url('/purchaserequest') }}" class="small-box-footer">Booking Order <i class="fa fa-arrow-circle-right"></i></a>
				          	</div>
				        </div>
			        </div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
    	<div class="col-md-12">
	    	<div class="box box-primary">
			  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			  		<h4 class="box-title">Sales Order</h4>
		  		</div>

		  		<div class="box-body">
			    	<div class="btn-group btn-breadcrumb">
			    		<a class="btn btn-xs btn-primary" style="font-weight: bold;"><span class="badge">1</span> Quotation</a>
			    		<a class="btn btn-xs btn-success" style="font-weight: bold;"><span class="badge">2</span> Dummy Proforma</a>
			            <a class="btn btn-xs btn-danger" style="font-weight: bold;"><span class="badge">3</span> Delivery Plan</a>
			            <a class="btn btn-xs btn-warning" style="font-weight: bold;"><span class="badge">4</span> Split Proforma</a>
			            <a class="btn btn-xs btn-default" style="font-weight: bold;"><span class="badge">5</span> Verify Order</a>
					</div>
					<br><br>
					<div class="btn-group btn-breadcrumb">
						<a class="btn btn-xs btn-primary" style="font-weight: bold;"><span class="badge">18</span> Packing List</a>
			    		<a class="btn btn-xs btn-success" style="font-weight: bold;"><span class="badge">19</span> Invoice</a>
			            <a class="btn btn-xs btn-danger" style="font-weight: bold;"><span class="badge">22</span> DO Confirmation</a>
			            <a class="btn btn-xs btn-warning" style="font-weight: bold;"><span class="badge">23</span> Customer Check</a>
			            <a class="btn btn-xs btn-default" style="font-weight: bold;"><span class="badge">24</span> DO Re-Route</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
	    	<div class="box box-primary">
			  	<div class="box-header with-header" style="background: #00c0ef !important; font-weight: bold;">
			  		<h4 class="box-title">Shipping</h4>
		  		</div>

		  		<div class="box-body">
		  			<div class="btn-group btn-breadcrumb">
						<a class="btn btn-xs btn-primary" style="font-weight: bold;"><span class="badge">6</span> Shipping Schedule</a>
			    		<a class="btn btn-xs btn-success" style="font-weight: bold;"><span class="badge">7</span> Booking Schedule</a>
			            <a class="btn btn-xs btn-danger" style="font-weight: bold;"><span class="badge">8</span> Booking Approval</a>
			            <a class="btn btn-xs btn-warning" style="font-weight: bold;"><span class="badge">9</span> Stuffing Schedule</a>
			            <a class="btn btn-xs btn-default" style="font-weight: bold;"><span class="badge">10</span> Stuffing Approval</a>
					</div>
					<br><br>
			    	<div class="btn-group btn-breadcrumb">
			    		<a class="btn btn-xs btn-primary" style="font-weight: bold;"><span class="badge">13</span> Stuffing</a>
			    		<a class="btn btn-xs btn-success" style="font-weight: bold;"><span class="badge">14</span> Delivery Order</a>
			            <a class="btn btn-xs btn-danger" style="font-weight: bold;"><span class="badge">15</span> Weighing</a>
			            <a class="btn btn-xs btn-warning" style="font-weight: bold;"><span class="badge">16</span> Packing List</a>
			            <a class="btn btn-xs btn-default" style="font-weight: bold;"><span class="badge">17</span> PL Approval</a>
					</div>
					<br><br>
			    	<div class="btn-group btn-breadcrumb">
			    		<a class="btn btn-xs btn-primary" style="font-weight: bold;"><span class="badge">20</span> Document Check</a>
			    		<a class="btn btn-xs btn-success" style="font-weight: bold;"><span class="badge">21</span> Stuffing Re-Schedule</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
	    	<div class="box box-primary">
			  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			  		<h4 class="box-title">Purchasing</h4>
		  		</div>

		  		<div class="box-body">
			    	<div class="btn-group btn-breadcrumb">
			    		<a class="btn btn-xs btn-primary" style="font-weight: bold;"><span class="badge">25</span> Purchase Request</a>
			    		<a class="btn btn-xs btn-success" style="font-weight: bold;"><span class="badge">26</span> PR Approval</a>
			            <a class="btn btn-xs btn-danger" style="font-weight: bold;"><span class="badge">27</span> Purchase Order</a>
			            <a class="btn btn-xs btn-warning" style="font-weight: bold;"><span class="badge">28</span> PO Approval</a>
					</div>
					<br><br>
			    	<div class="btn-group btn-breadcrumb">
			    		<a class="btn btn-xs btn-primary" style="font-weight: bold;"><span class="badge">31</span> Purchase Derivative</a>
			    		<a class="btn btn-xs btn-success" style="font-weight: bold;"><span class="badge">32</span> PD Approval</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
	    	<div class="box box-primary">
			  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			  		<h4 class="box-title">Inventory</h4>
		  		</div>

		  		<div class="box-body">
		  			<div class="btn-group btn-breadcrumb">
						<a class="btn btn-xs btn-primary" style="font-weight: bold;"><span class="badge">11</span> Request Stock</a>
			    		<a class="btn btn-xs btn-success" style="font-weight: bold;"><span class="badge">12</span> Loading Stock</a>
					</div>
					<br><br>
			    	<div class="btn-group btn-breadcrumb">
			    		<a class="btn btn-xs btn-primary" style="font-weight: bold;"><span class="badge">29</span> Quality Control</a>
			    		<a class="btn btn-xs btn-success" style="font-weight: bold;"><span class="badge">30</span> Incoming Stock</a>
			            <a class="btn btn-xs btn-danger" style="font-weight: bold;"><span class="badge">33</span> Moving Stock</a>
			            <a class="btn btn-xs btn-warning" style="font-weight: bold;"><span class="badge">34</span> Outgoing Stock</a>
			            <a class="btn btn-xs btn-default" style="font-weight: bold;"><span class="badge">35</span> Stock Opname</a>
					</div>
					<br><br>
					<div class="btn-group btn-breadcrumb">
						<a class="btn btn-xs btn-primary" style="font-weight: bold;"><span class="badge">36</span> Assembly / Production</a>
			    		<a class="btn btn-xs btn-success" style="font-weight: bold;"><span class="badge">37</span> Monitoring Stock</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop