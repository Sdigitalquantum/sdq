@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
            <li class="disabled"><a>Account</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #6b6e70 !important; font-weight: bold; color: white;">
			    		<h4 class="box-title">Edit Company &nbsp;<a href="{{ route('companyaccount.show', $id) }}" class="btn btn-xs btn-warning" style="text-decoration: none;">Account</a></h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form id="companyFrm">
				      		@method('PATCH')
				      		@csrf
				      		<div class="row">
    							<div class="col-md-6">
    								<div class="form-group">
						              	<label for="code">Code</label>
						              	<input type="text" class="form-control" name="code" value="{{ $company->code }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ $company->name }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="address">Address</label>
						              	<input type="text" class="form-control" name="address" value="{{ $company->address }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="city">City</label>
						              	<select class="form-control select2" name="city" required>
						              		@foreach($citys as $city)
											    @if ($company->city == $city->id)
												    <option value="{{ $city->id }}" selected>{{ $city->name }} - {{ $city->fkCountry->name }}</option>
												@else
												    <option value="{{ $city->id }}">{{ $city->name }} - {{ $city->fkCountry->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						          		<label for="email">Email</label>
						              	<input type="text" class="form-control" name="email" value="{{ $company->email }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="mobile">Mobile</label>
						              	<input type="number" class="form-control" name="mobile" value="{{ $company->mobile }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="fax">Fax</label>
						              	<input type="number" class="form-control" name="fax" value="{{ $company->fax }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="button">&nbsp;</label> <br>
						          		<button id="updateBtn" value="{{ route('company.update', $company->id) }}" class="btn btn-sm btn-success">Update</button>
				          				<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
						          	</div>
								</div>
							</div>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.company.prepare_edit());
    </script>
@stop