@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
            <li class="disabled"><a>Payment</a></li>
            <li class="disabled"><a>Charges</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
                <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Quotation &nbsp;<a href="{{ route('quotation.create')}}" class="btn btn-xs btn-success" style="text-decoration: none;">New</a></h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="15%">Nomor</th>
						          	<th width="10%">Date</th>
						          	<th width="15%">Customer</th>
						          	<th width="15%">Port Loading</th>
						          	<th width="15%">Port Discharge</th>
						          	<th width="10%">Time Departure</th>
						          	<th width="10%">Time Arrival</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($quotations as $quotation)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>{{$quotation->no_sp}}</td>
							            <td>{{$quotation->date_sp}}</td>
							            <td>{{$quotation->fkCustomerdetail->name}}</td>
							            <td>{{$quotation->fkPortl->name}} - {{$quotation->fkPortl->fkCountry->name}}</td>
							            <td>{{$quotation->fkPortd->name}} - {{$quotation->fkPortd->fkCountry->name}}</td>
							            <td>{{$quotation->etd}}</td>
							            <td>{{$quotation->eta}}</td>

							            @if($quotation->status_close == 1)
							            	<td><font color="green">Closed <i class="glyphicon glyphicon-ok"></i></font></td>
					            		@elseif($quotation->status_release == 1)
					            			<td><font color="blue">Released <i class="glyphicon glyphicon-ok"></i></font></td>
							            @elseif($quotation->status_release == 0)
								            <td>
								            	<a href="{{ route('quotation.edit',$quotation->id)}}" class="btn btn-xs btn-warning">Edit</a> &nbsp;
								            	
								            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$quotation->id}}">Close</button>

								            	<form action="{{ route('quotation.destroy', $quotation->id)}}" method="post">
								                  	@csrf
								                  	@method('DELETE')
								                  	<div class="modal fade bs-example-modal{{$quotation->id}}" tabindex="-1" role="dialog" aria-hidden="true">
					                                    <div class="modal-dialog modal-sm">
					                                        <div class="modal-content">
					                                            <div class="modal-header btn-danger">
					                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
					                                                </button>
					                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Closing</h4>
					                                            </div>
					                                            <div class="modal-body">
					                                                Are you sure to close this data?
					                                            </div>
					                                            <div class="modal-footer">
					                                                <button type="submit" class="btn btn-success">Yes</button>
					                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
					                                            </div>
					                                        </div>
					                                    </div>
					                                </div>
								                </form>
								            </td>
							            @endif
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Nomor</td>
						          	<td>Date</td>
						          	<td>Customer</td>
						          	<td>Port Loading</td>
						          	<td>Port Discharge</td>
						          	<td>Time Departure</td>
						          	<td>Time Arrival</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop