<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasedetailprsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchasedetailprs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchasedetail')->unsigned();
            $table->integer('purchaserequest');
            $table->integer('purchasedetailprg');
            $table->integer('qty');
            $table->timestamps();
            $table->integer('created_user');
            $table->foreign('purchasedetail')->references('id')->on('purchasedetails');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchasedetailprs');
    }
}
