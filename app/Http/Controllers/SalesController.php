<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Sales;
use App\Http\Models\Account;
use App\Http\Models\City;
use App\Http\Models\Employeeset;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $saless = Sales::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/sales.index', compact('saless','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $accounts = Account::where('status', 1)->orderBy('name')->get();
        $citys = City::where('status', 1)->orderBy('name')->get();

        return view('vendor/adminlte/sales.create', compact('accounts','citys','disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'code' => 'unique:sales'
                ]);

                $status_group   = 0; if (isset($json->status_group)) { $status_group  = 1; };
                $status_record  = 0; if (isset($json->status_record)){ $status_record = 1; };
                
                $sales = new Sales([
                    'accounting'    => $json->account,
                    'city'          => $json->city,
                    'code'          => $json->code,
                    'name'          => $json->name,
                    'address'       => $json->address,
                    'mobile'        => $json->mobile,
                    'birthday'      => $json->birthday,
                    'status_group'  => $status_group,
                    'status_record' => $status_record,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $sales->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $sales  = Sales::find($id);
        $accounts = Account::where('status', 1)->orderBy('name')->get();
        $citys = City::where('status', 1)->orderBy('name')->get();

        return view('vendor/adminlte/sales.edit', compact('id','sales','accounts','citys','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            $sales = Sales::find($id);
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                if ($sales->code != $json->code) {
                    $request->validate([
                        'code' => 'unique:sales'
                    ]);
                };

                $status_group   = 0; if (isset($json->status_group)) { $status_group  = 1; };
                $status_record  = 0; if (isset($json->status_record)){ $status_record = 1; };

                $sales->accounting    = $json->account;
                $sales->city          = $json->city;
                $sales->code          = $json->code;
                $sales->name          = $json->name;
                $sales->address       = $json->address;
                $sales->mobile        = $json->mobile;
                $sales->birthday      = $json->birthday;
                $sales->status_group  = $status_group;
                $sales->status_record = $status_record;
                $sales->updated_user  = $session_id;
                $result = $sales->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $sales = Sales::find($id);
        $status = $sales->status;

        if ($status == 1) {
        	$sales->status       = 0;
            $sales->updated_user = $session_id;
	        $result = $sales->save();

        } elseif ($status == 0) {
        	$sales->status       = 1;
            $sales->updated_user = $session_id;
	        $result = $sales->save();

        };
        return $this->jsonSuccess( $result );
    }
}
