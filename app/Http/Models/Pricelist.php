<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Pricelist extends Model
{
    protected $fillable = [
	    'product',
	    'currency',
	    'price',
	    'date',
	    'created_user',
	    'updated_user'
  	];

  	public function fkProduct(){
	    return $this->belongsTo('\App\Http\Models\Product', 'product', 'id');
	}

	public function fkCurrency(){
	    return $this->belongsTo('\App\Http\Models\Currency', 'currency', 'id');
	}
}
