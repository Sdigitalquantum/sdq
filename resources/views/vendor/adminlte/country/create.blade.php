@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #6b6e70 !important; font-weight: bold; color: white;">
			    		<h4 class="box-title">New Country</h4>
			    	</div>

				  	<div class="box-body">
				      	<form id="countryFrm" > <!-- method="post" action="{{ route('country.store') }}" -->
				      		@method('POST')
				          	@csrf
				          	<div class="form-group">
				              	<label for="code">Code</label>
				              	<input type="text" class="form-control" name="code" disabled>
				          	</div>
				          	<div class="form-group">
				              	<label for="name">Name</label>
				              	<input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
				          	</div>
				          	<div class="form-group">
				              	<label for="mobile">Mobile (+)</label>
				              	<input type="number" class="form-control" name="mobile" value="{{ old('mobile') }}" required>
				          	</div>
				          	<!-- <button onclick="sdq.core.submitForm('countryFrm', '{{ route("country.store", $country->id) }}', 'country' );return false;" class="btn btn-sm btn-success" >Save</button>
				          	<a href="#" onclick="sdq.core.load('country');return false;" class="btn btn-sm btn-danger">Cancel</a> -->
				          	
			          		<button id="saveBtn" value="{{ route('country.store') }}" class="btn btn-sm btn-success">Save</button>

			          		<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.country.prepare_create());
    </script>
@stop