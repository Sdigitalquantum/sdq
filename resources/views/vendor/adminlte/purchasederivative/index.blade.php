@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
                <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Purchase Derivative</h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="15%">No. PO</th>
						          	<th width="30%">Detail PO</th>
						          	<th width="15%">Information</th>
						          	<th width="30%">Note</th>
						          	<th width="5%">Approval</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($array = [])
						    	@foreach($employeewarehouses as $employeewarehouse)
						    		@php ($array[] = $employeewarehouse->warehouse)
						    	@endforeach

						    	@php ($no = 0)
						    	@foreach($stockqcs as $stockqc)
						    		@if(in_array($stockqc->warehouse, $array))
							    		@php ($no++)
							    		<tr>
							    			<td>{{$no}}</td>
								            <td>{{$stockqc->no_po}}</td>
								            <td>
								            	Product &nbsp;: {{$stockqc->fkProduct->name}} <br>
								            	Supplier : {{$stockqc->fkSupplierdetail->name}} <br>
								            	Warehouse : {{$stockqc->fkWarehouse->name}}
								            </td>
								            <td>
								            	Date : {{$stockqc->date_in}} <br>
								            	Qty &nbsp; : {{number_format($stockqc->qty_kg, 0, ',' , '.')}} Kg <br>
								            	Price : Rp. {{number_format($stockqc->price, 0, ',' , '.')}} @Kg
								            </td>
								            <td><pre>{{$stockqc->notice}}</pre></td>
											<td>
								            	@if($stockqc->approve == 1 and $stockqc->status_approve == 0)
								            		@if($stockqc->status_release == 1)
										            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal-set{{$stockqc->id}}">Set None</button>

										            	<form action="{{ url('purchasederivative/set', $stockqc->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal-set{{$stockqc->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-danger">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to set none approval this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
									                @else
								            			<font color="red">None <i class="glyphicon glyphicon-remove"></i></font>
								            		@endif
								                @elseif($stockqc->approve == 0 and $stockqc->status_approve == 1)
								                	<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal-set{{$stockqc->id}}">Set Used</button>

									            	<form action="{{ url('purchasederivative/set', $stockqc->id)}}" method="post">
									                  	@csrf
									                  	@method('DELETE')
									                  	<div class="modal fade bs-example-modal-set{{$stockqc->id}}" tabindex="-1" role="dialog" aria-hidden="true">
						                                    <div class="modal-dialog modal-sm">
						                                        <div class="modal-content">
						                                            <div class="modal-header btn-success">
						                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
						                                                </button>
						                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
						                                            </div>
						                                            <div class="modal-body">
						                                                Are you sure to set used approval this data?
						                                            </div>
						                                            <div class="modal-footer">
						                                                <button type="submit" class="btn btn-success">Yes</button>
						                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
						                                            </div>
						                                        </div>
						                                    </div>
						                                </div>
									                </form>
								                @else
								                	<font color="blue">Yes <i class="glyphicon glyphicon-ok"></i></font>
								                @endif
								            </td>

								            @if($stockqc->status_approve == 1)
			            						<td>
			            							<font color="green">Approved <i class="glyphicon glyphicon-ok"></i></font>
			            						</td>
		            						@elseif($stockqc->status_approve == 2)
				            					<td>
				            						<font color="red">Rejected <i class="glyphicon glyphicon-remove"></i></font>
			            						</td>
		            						@else
												<td>
													@if($stockqc->status_release == 1)
									            		<font color="green">Released <i class="glyphicon glyphicon-ok"></i></font> <br>
									            		<font color="blue">Waiting Approval</font>
									            	@else
									            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal-rl{{$stockqc->id}}">Release</button> &nbsp;
									            		<a href="{{ route('purchasederivative.edit',$stockqc->id)}}" class="btn btn-xs btn-warning">Edit Price</a>

									            		<form action="{{ url('purchasederivative/release', $stockqc->id) }}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal-rl{{$stockqc->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-success">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Release</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to release this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
								            		@endif
												</td>
								            @endif
								        </tr>
							        @endif
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						        	<td class="noShow"></td>
						          	<td>No. PO</td>
						          	<td>Product</td>
						          	<td>Information</td>
						          	<td>Note</td>
						          	<td class="noShow"></td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop