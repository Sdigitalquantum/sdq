<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Container extends Model
{
    protected $fillable = [
	    'size',
	    'name',
	    'qty',
	    'created_user',
	    'updated_user'
  	];
}
