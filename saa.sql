-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 28, 2019 at 10:18 AM
-- Server version: 10.1.40-MariaDB-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saa`
--

-- --------------------------------------------------------

--
-- Table structure for table `accountgroups`
--

CREATE TABLE `accountgroups` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accountgroups`
--

INSERT INTO `accountgroups` (`id`, `code`, `name`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 'H', 'Header', 1, '2019-02-20 09:19:00', '2019-02-20 09:19:00', 1, 1),
(2, 'D', 'Detail', 1, '2019-02-20 09:19:10', '2019-02-20 09:19:10', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `currency` int(10) UNSIGNED NOT NULL,
  `group` int(10) UNSIGNED NOT NULL,
  `type` int(10) UNSIGNED NOT NULL,
  `code` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level1` int(11) DEFAULT NULL,
  `level2` int(11) DEFAULT NULL,
  `level3` int(11) DEFAULT NULL,
  `level4` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `currency`, `group`, `type`, `code`, `name`, `level1`, `level2`, `level3`, `level4`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, 1, 1000000, 'AKTIVA LANCAR', 0, 0, 0, 1000000, 1, '2019-02-20 09:21:00', '2019-02-20 09:22:12', 1, 1),
(2, 2, 2, 1, 1012002, 'USD', 1012000, 1012000, 1010000, 1000000, 1, '2019-02-20 09:21:48', '2019-02-20 09:21:48', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `accounttypes`
--

CREATE TABLE `accounttypes` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accounttypes`
--

INSERT INTO `accounttypes` (`id`, `code`, `name`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 'A', 'Activa', 1, '2019-02-20 09:19:23', '2019-02-20 09:19:23', 1, 1),
(2, 'P', 'Passiva', 1, '2019-02-20 09:19:41', '2019-02-20 09:19:41', 1, 1),
(3, 'I', 'Income', 1, '2019-02-20 09:19:53', '2019-02-20 09:19:53', 1, 1),
(4, 'C', 'Cost', 1, '2019-02-20 09:20:03', '2019-02-20 09:20:03', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `no_inc`, `code`, `name`, `alias`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 'B00001', 'Bank Central Asia', 'BCA', 1, '2019-02-20 09:07:33', '2019-07-15 09:48:00', 1, 2),
(2, 2, 'B00002', 'Bank Mandiri', 'Mandiri', 1, '2019-02-20 09:07:46', '2019-02-20 09:07:46', 1, 1),
(3, 3, 'B00003', 'Bank Rakyat Indonesia', 'BRI', 1, '2019-02-20 09:07:55', '2019-02-20 09:07:55', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `no_inc`, `code`, `name`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 'CAB00001', 'Kantor Pusat', 1, '2019-02-20 09:09:58', '2019-02-20 09:09:58', 1, 1),
(2, 2, 'CAB00002', 'Kantor Cabang', 1, '2019-02-20 09:10:07', '2019-02-20 09:10:07', 1, 1),
(3, 3, 'CAB00003', 'Gudang', 1, '2019-02-20 09:10:16', '2019-02-20 09:10:16', 1, 1),
(4, 4, 'CAB00004', 'Batam', 0, '2019-07-16 02:28:05', '2019-07-16 02:28:53', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `country` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `country`, `no_inc`, `code`, `name`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, 'K00001', 'Jakarta', 1, '2019-02-20 06:36:21', '2019-02-20 06:36:21', 1, 1),
(2, 1, 2, 'K00002', 'Semarang', 1, '2019-02-20 06:36:33', '2019-02-20 06:36:33', 1, 1),
(3, 1, 3, 'K00003', 'Surabaya', 1, '2019-02-20 06:36:41', '2019-02-20 06:36:41', 1, 1),
(4, 2, 4, 'K00004', 'Bangkok', 1, '2019-02-20 06:36:50', '2019-02-20 06:36:50', 1, 1),
(5, 2, 5, 'K00005', 'Laem Chabang', 1, '2019-02-25 13:19:39', '2019-02-25 13:19:39', 1, 1),
(6, 3, 6, 'K00006', 'Florida', 1, '2019-07-15 10:05:31', '2019-07-15 10:05:31', 2, 2),
(7, 7, 7, 'K00007', 'Ekaterinburg', 1, '2019-07-16 02:30:10', '2019-07-16 02:30:30', 2, 2),
(8, 1, 8, 'K00008', 'Palu', 1, '2019-07-16 02:32:51', '2019-07-16 02:32:51', 2, 2),
(9, 1, 9, 'K00009', 'Batam', 1, '2019-07-16 02:33:09', '2019-07-16 02:33:09', 2, 2),
(10, 1, 10, 'K00010', 'Lampung', 1, '2019-07-16 02:33:24', '2019-07-16 02:33:24', 2, 2),
(11, 1, 11, 'K00011', 'Malang', 1, '2019-07-16 02:35:11', '2019-07-16 02:35:11', 2, 2),
(12, 1, 12, 'K00012', 'Pasuruan', 1, '2019-07-16 03:29:35', '2019-07-16 03:29:35', 2, 2),
(13, 1, 13, 'K00013', 'Flores', 1, '2019-07-16 03:29:53', '2019-07-16 03:29:53', 2, 2),
(14, 1, 14, 'K00014', 'Luwuk', 1, '2019-07-16 03:30:00', '2019-07-16 03:30:00', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `city` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `city`, `code`, `name`, `address`, `email`, `mobile`, `fax`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 3, 'SAA', 'PT. STARINDO ANUGERAH ABADI', 'Jl. Diamond Hill DR 6/23 Citraland', 'info@pt-saa.co.id', '6281999112233', '6281999445566', 1, '2019-02-20 09:15:40', '2019-07-16 03:16:16', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `companyaccounts`
--

CREATE TABLE `companyaccounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `company` int(10) UNSIGNED NOT NULL,
  `currency` int(10) UNSIGNED NOT NULL,
  `bank` int(10) UNSIGNED NOT NULL,
  `city` int(10) UNSIGNED NOT NULL,
  `account_no` bigint(20) NOT NULL,
  `account_swift` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `export` tinyint(4) NOT NULL,
  `local` tinyint(4) NOT NULL,
  `internal` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companyaccounts`
--

INSERT INTO `companyaccounts` (`id`, `company`, `currency`, `bank`, `city`, `account_no`, `account_swift`, `account_name`, `account_address`, `alias`, `export`, `local`, `internal`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 2, 1, 3, 8290757569, 'CENAIDJA', 'PT. STARINDO ANUGERAH ABADI', 'Jln. HR Muhammad 17', 'SAA USD', 1, 0, 0, 1, '2019-02-20 09:16:17', '2019-02-20 09:16:17', 1, 1),
(2, 1, 1, 2, 3, 1234567890, 'CENAIDJA', 'PT. STARINDO ANUGERAH ABADI', 'Jln. HR Muhammad 17', 'SAA IDR', 0, 1, 1, 1, '2019-02-20 09:16:47', '2019-02-20 09:16:47', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `containers`
--

CREATE TABLE `containers` (
  `id` int(10) UNSIGNED NOT NULL,
  `size` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `containers`
--

INSERT INTO `containers` (`id`, `size`, `name`, `qty`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 20, 'Containers', 15000, 1, '2019-02-21 13:34:35', '2019-05-16 08:41:44', 1, 2),
(2, 40, 'Containers', 26000, 1, '2019-02-21 13:34:45', '2019-05-16 08:41:59', 1, 2),
(3, 0, 'Colt Diesel', 6000, 1, '2019-05-16 08:42:17', '2019-05-16 08:42:17', 2, 2),
(4, 0, 'Fuso', 15000, 1, '2019-05-16 08:42:35', '2019-05-16 08:42:35', 2, 2),
(5, 0, 'Pickup', 2000, 1, '2019-05-16 08:42:52', '2019-05-16 08:42:52', 2, 2),
(6, 40, 'Reefer Container WFF Frozen Coconut Chunk', 25000, 1, '2019-07-15 09:55:20', '2019-07-15 09:58:54', 2, 2),
(7, 40, 'Reefer Container WFF Cassava', 22000, 1, '2019-07-16 02:47:09', '2019-07-16 02:48:22', 2, 2),
(8, 40, 'Reefer Container WFF Coconut Dice', 27000, 1, '2019-07-16 02:49:11', '2019-07-16 02:49:11', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `no_inc`, `code`, `name`, `mobile`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 'C00001', 'Indonesia', 62, 1, '2019-02-20 06:35:43', '2019-02-20 06:35:43', 1, 1),
(2, 2, 'C00002', 'Thailand', 66, 1, '2019-02-20 06:35:59', '2019-02-20 06:35:59', 1, 1),
(3, 3, 'C00003', 'USA', 1, 1, '2019-07-15 10:04:51', '2019-07-15 10:04:51', 2, 2),
(4, 4, 'C00004', 'Dominican Republic', 809, 1, '2019-07-15 10:12:02', '2019-07-15 10:12:02', 2, 2),
(5, 5, 'C00005', 'Canada', 12, 0, '2019-07-16 02:27:04', '2019-07-16 02:27:09', 2, 2),
(6, 6, 'C00006', 'Sri Lanka', 94, 1, '2019-07-16 02:28:34', '2019-07-16 02:28:34', 2, 2),
(7, 7, 'C00007', 'Russia', 7, 1, '2019-07-16 02:28:56', '2019-07-16 02:28:56', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `code`, `name`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 'IDR', 'Indonesia Rupiah', 1, '2019-02-20 09:08:08', '2019-07-16 02:36:19', 1, 2),
(2, 'USD', 'US Dollar', 1, '2019-02-20 09:08:20', '2019-07-16 02:35:40', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `customeraccounts`
--

CREATE TABLE `customeraccounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer` int(10) UNSIGNED NOT NULL,
  `currency` int(10) UNSIGNED NOT NULL,
  `bank` int(10) UNSIGNED NOT NULL,
  `account_no` bigint(20) NOT NULL,
  `account_swift` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customeraccounts`
--

INSERT INTO `customeraccounts` (`id`, `customer`, `currency`, `bank`, `account_no`, `account_swift`, `account_name`, `alias`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, 2, 3216549870, 'BERKAH', 'CV. BERKAH', 'Berkah Jaya', 1, '2019-02-20 09:25:21', '2019-02-20 09:25:21', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `customerdetails`
--

CREATE TABLE `customerdetails` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer` int(10) UNSIGNED NOT NULL,
  `city` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias_mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customerdetails`
--

INSERT INTO `customerdetails` (`id`, `customer`, `city`, `name`, `address`, `mobile`, `alias_name`, `alias_mobile`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, 'CV. Berkah', 'Jl. Raya No. 100', '6281999223344', 'Berkatun', '6221343536', 1, '2019-02-20 09:24:10', '2019-02-20 09:24:10', 1, 1),
(2, 2, 4, 'VaraFood', '280 SIRINTHORN ROAD, BANGPLAD', '66812030', 'VFD', '', 1, '2019-05-21 05:04:30', '2019-05-21 05:07:07', 2, 2),
(3, 3, 6, 'World Foods&Flavors', '400 PGA Blvd., Suite 201. Palm Beach Gardens, FL 33410', '561619365', 'WFF', '', 1, '2019-07-15 10:06:17', '2019-07-15 10:06:17', 2, 2),
(4, 5, 4, 'Vara Food & Drink Co. Ltd', '280 Sirinthorn Road, Bangplad, Bangkok 10700, Thailand.', '+668812030', '', '', 1, '2019-07-16 03:49:18', '2019-07-16 03:49:18', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `accounting` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax` tinyint(4) NOT NULL,
  `npwp_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `npwp_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `npwp_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `npwp_city` int(11) DEFAULT NULL,
  `limit` bigint(20) NOT NULL,
  `piutang_idr` bigint(20) NOT NULL DEFAULT '0',
  `piutang_usd` bigint(20) NOT NULL DEFAULT '0',
  `note1` text COLLATE utf8mb4_unicode_ci,
  `note2` text COLLATE utf8mb4_unicode_ci,
  `note3` text COLLATE utf8mb4_unicode_ci,
  `notice` text COLLATE utf8mb4_unicode_ci,
  `status_group` tinyint(4) NOT NULL,
  `status_record` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `accounting`, `no_inc`, `code`, `fax`, `email`, `tax`, `npwp_no`, `npwp_name`, `npwp_address`, `npwp_city`, `limit`, `piutang_idr`, `piutang_usd`, `note1`, `note2`, `note3`, `notice`, `status_group`, `status_record`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, 'CST00001', '', 'berkah@gmail.com', 1, '1324576890', 'CV. Berkah', 'Jl. Urip Sumoharjo X / 13', 3, 250000000, 0, 0, '', '', '', 'Testing data', 1, 1, 1, '2019-02-20 09:23:20', '2019-06-27 01:17:16', 1, 1),
(2, 2, 2, 'CST00002', '', '', 0, '', '', '', 1, 100000000, 0, 0, '', '', '', 'hapus lagi setelah selesai', 0, 0, 1, '2019-05-21 05:03:06', '2019-06-26 13:17:14', 2, 2),
(3, 2, 3, 'CST00003', '', 'tridajat@ptsathya.com', 0, '', '', '', 1, 100000000, 0, 0, '', '', '', '', 0, 0, 1, '2019-07-15 10:03:09', '2019-07-15 10:06:37', 2, 2),
(4, 2, 4, 'CST00004', '', '', 0, '', '', '', 0, 1000000000, 0, 0, '', '', '', '', 0, 0, 0, '2019-07-16 03:47:56', '2019-07-16 03:51:25', 2, 2),
(5, 2, 5, 'CST00005', '', 'sunee_klom@varafood.com', 0, '', '', '', 1, 1000000000, 0, 0, '', '', '', '', 0, 0, 1, '2019-07-16 03:48:30', '2019-07-16 03:49:28', 2, 2),
(6, 2, 6, 'CST00006', '', '', 0, '', '', '', 1, 2000000000, 0, 0, '', '', '', '', 0, 0, 0, '2019-07-16 03:50:08', '2019-07-16 03:50:21', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `debtcards`
--

CREATE TABLE `debtcards` (
  `id` int(10) UNSIGNED NOT NULL,
  `supplierdetail` int(10) UNSIGNED NOT NULL,
  `year` int(11) NOT NULL,
  `month` tinyint(4) NOT NULL,
  `debit` bigint(20) NOT NULL,
  `kredit` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `debts`
--

CREATE TABLE `debts` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_po` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_supplier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_supplier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_product` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_product` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_po` date NOT NULL,
  `date_qc` date NOT NULL,
  `qty` int(11) NOT NULL,
  `price` bigint(20) NOT NULL,
  `total` bigint(20) NOT NULL,
  `payment` bigint(20) NOT NULL,
  `adjustment` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `no_inc`, `code`, `name`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 'DEP00001', 'Information Technology', 1, '2019-02-20 09:10:28', '2019-02-20 09:10:28', 1, 1),
(2, 2, 'DEP00002', 'Sales Order', 1, '2019-02-20 09:10:36', '2019-02-20 09:10:36', 1, 1),
(3, 3, 'DEP00003', 'Shipping', 1, '2019-02-20 09:10:44', '2019-02-20 09:10:44', 1, 1),
(4, 4, 'DEP00004', 'Purchasing', 1, '2019-02-20 09:10:59', '2019-02-20 09:10:59', 1, 1),
(5, 5, 'DEP00005', 'Inventory Control', 1, '2019-02-20 09:11:10', '2019-07-16 02:29:44', 1, 2),
(6, 6, 'DEP00006', 'Finance', 0, '2019-07-16 02:29:35', '2019-07-16 02:30:06', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `documentchecks`
--

CREATE TABLE `documentchecks` (
  `id` int(10) UNSIGNED NOT NULL,
  `schedulestuff` int(10) NOT NULL,
  `quotationsplit` int(11) NOT NULL,
  `document` int(10) UNSIGNED NOT NULL,
  `notice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `export` tinyint(4) NOT NULL,
  `local` tinyint(4) NOT NULL,
  `internal` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `no_inc`, `code`, `name`, `export`, `local`, `internal`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 'DOC00001', 'PO Customer', 1, 1, 0, 1, '2019-02-20 09:13:14', '2019-02-20 09:13:14', 1, 1),
(2, 2, 'DOC00002', 'Quotation', 1, 1, 1, 1, '2019-02-20 09:13:26', '2019-02-20 09:13:26', 1, 1),
(3, 3, 'DOC00003', 'Delivery Plan', 1, 1, 1, 1, '2019-02-20 09:13:40', '2019-02-20 09:13:40', 1, 1),
(4, 4, 'DOC00004', 'Proforma Invoice', 1, 1, 1, 1, '2019-02-20 09:13:58', '2019-02-20 09:13:58', 1, 1),
(5, 5, 'DOC00005', 'Stuffing', 1, 0, 0, 1, '2019-02-20 09:14:15', '2019-02-20 09:14:15', 1, 1),
(6, 6, 'DOC00006', 'Weighing', 1, 0, 0, 1, '2019-02-27 09:40:15', '2019-02-27 09:40:15', 1, 1),
(7, 7, 'DOC00007', 'Packing List', 1, 1, 1, 0, '2019-07-16 02:41:13', '2019-07-16 02:42:07', 2, 2),
(8, 8, 'DOC00008', 'Warehouse weighing', 0, 0, 1, 1, '2019-07-16 02:42:29', '2019-07-16 02:42:29', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `group` int(10) UNSIGNED NOT NULL,
  `branch` int(10) UNSIGNED NOT NULL,
  `department` int(10) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `group`, `branch`, `department`, `position`, `name`, `email`, `password`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, 1, 1, 'Wajar Jaya', 'wajar@karyaku.id', '064cc5a92b512c96803a337f550c7336', 1, '2019-04-25 13:38:25', '2019-04-25 14:09:57', 1, 2),
(2, 2, 1, 1, 1, 'PT. SAA', 'saa@pt-saa.co.id', 'f9ac02984d9cbda4b5f51ec00280d6d8', 1, '2019-04-25 13:39:38', '2019-04-25 14:10:08', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `employeesets`
--

CREATE TABLE `employeesets` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee` int(10) UNSIGNED NOT NULL,
  `menuroot` int(10) UNSIGNED NOT NULL,
  `nomorroot` int(11) NOT NULL,
  `menu` int(11) NOT NULL,
  `nomormenu` int(11) NOT NULL,
  `menusub` int(11) NOT NULL,
  `nomorsub` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employeesets`
--

INSERT INTO `employeesets` (`id`, `employee`, `menuroot`, `nomorroot`, `menu`, `nomormenu`, `menusub`, `nomorsub`, `created_at`, `updated_at`, `created_user`) VALUES
(1, 2, 1, 1, 0, 0, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(2, 2, 2, 2, 0, 0, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(3, 2, 2, 2, 1, 1, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(4, 2, 2, 2, 1, 1, 1, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(5, 2, 2, 2, 1, 1, 2, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(6, 2, 2, 2, 1, 1, 3, 3, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(7, 2, 2, 2, 1, 1, 4, 4, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(8, 2, 2, 2, 1, 1, 5, 5, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(9, 2, 2, 2, 1, 1, 6, 6, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(10, 2, 2, 2, 1, 1, 7, 7, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(11, 2, 2, 2, 1, 1, 8, 8, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(12, 2, 2, 2, 1, 1, 9, 9, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(13, 2, 2, 2, 2, 2, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(14, 2, 2, 2, 2, 2, 10, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(15, 2, 2, 2, 2, 2, 11, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(16, 2, 2, 2, 2, 2, 12, 3, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(17, 2, 2, 2, 2, 2, 13, 4, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(18, 2, 2, 2, 2, 2, 14, 5, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(19, 2, 2, 2, 2, 2, 15, 6, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(20, 2, 2, 2, 2, 2, 16, 7, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(21, 2, 2, 2, 3, 3, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(22, 2, 2, 2, 3, 3, 17, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(23, 2, 2, 2, 3, 3, 18, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(24, 2, 2, 2, 3, 3, 19, 3, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(25, 2, 2, 2, 3, 3, 20, 4, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(26, 2, 2, 2, 3, 3, 21, 5, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(27, 2, 2, 2, 3, 3, 22, 6, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(28, 2, 2, 2, 3, 3, 67, 7, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(29, 2, 2, 2, 4, 4, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(30, 2, 2, 2, 4, 4, 23, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(31, 2, 2, 2, 4, 4, 24, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(32, 2, 2, 2, 4, 4, 25, 3, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(33, 2, 2, 2, 4, 4, 26, 4, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(34, 2, 2, 2, 4, 4, 27, 5, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(35, 2, 2, 2, 4, 4, 28, 6, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(36, 2, 2, 2, 4, 4, 29, 7, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(37, 2, 2, 2, 4, 4, 30, 8, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(38, 2, 2, 2, 4, 4, 68, 9, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(39, 2, 3, 3, 0, 0, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(40, 2, 3, 3, 5, 1, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(41, 2, 3, 3, 6, 2, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(42, 2, 3, 3, 7, 3, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(43, 2, 3, 3, 57, 4, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(44, 2, 3, 3, 8, 5, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(45, 2, 3, 3, 9, 6, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(46, 2, 3, 3, 10, 7, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(47, 2, 3, 3, 10, 7, 31, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(48, 2, 3, 3, 10, 7, 32, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(49, 2, 3, 3, 11, 8, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(50, 2, 3, 3, 11, 8, 33, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(51, 2, 3, 3, 11, 8, 34, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(52, 2, 3, 3, 12, 9, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(53, 2, 3, 3, 12, 9, 35, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(54, 2, 3, 3, 12, 9, 36, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(55, 2, 3, 3, 13, 10, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(56, 2, 3, 3, 13, 10, 37, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(57, 2, 3, 3, 13, 10, 38, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(58, 2, 4, 4, 0, 0, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(59, 2, 4, 4, 14, 1, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(60, 2, 4, 4, 15, 2, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(61, 2, 4, 4, 15, 2, 39, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(62, 2, 4, 4, 15, 2, 40, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(63, 2, 4, 4, 16, 3, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(64, 2, 4, 4, 16, 3, 41, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(65, 2, 4, 4, 16, 3, 42, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(66, 2, 4, 4, 22, 4, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(67, 2, 4, 4, 22, 4, 49, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(68, 2, 4, 4, 22, 4, 50, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(69, 2, 5, 5, 0, 0, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(70, 2, 5, 5, 17, 1, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(71, 2, 5, 5, 18, 2, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(72, 2, 5, 5, 19, 3, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(73, 2, 5, 5, 19, 3, 43, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(74, 2, 5, 5, 19, 3, 44, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(75, 2, 5, 5, 20, 4, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(76, 2, 5, 5, 20, 4, 45, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(77, 2, 5, 5, 20, 4, 46, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(78, 2, 5, 5, 21, 5, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(79, 2, 5, 5, 21, 5, 47, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(80, 2, 5, 5, 21, 5, 48, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(81, 2, 5, 5, 23, 7, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(82, 2, 5, 5, 23, 7, 51, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(83, 2, 5, 5, 23, 7, 52, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(84, 2, 5, 5, 24, 8, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(85, 2, 6, 6, 0, 0, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(86, 2, 6, 6, 25, 1, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(87, 2, 6, 6, 58, 2, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(88, 2, 6, 6, 26, 3, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(89, 2, 6, 6, 27, 4, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(90, 2, 6, 6, 28, 5, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(91, 2, 7, 7, 0, 0, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(92, 2, 7, 7, 29, 1, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(93, 2, 7, 7, 29, 1, 53, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(94, 2, 7, 7, 29, 1, 54, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(95, 2, 7, 7, 30, 2, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(96, 2, 7, 7, 31, 3, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(97, 2, 7, 7, 32, 4, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(98, 2, 7, 7, 33, 5, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(99, 2, 7, 7, 34, 6, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(100, 2, 7, 7, 35, 7, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(101, 2, 7, 7, 36, 8, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(102, 2, 7, 7, 37, 9, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(103, 2, 7, 7, 38, 10, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(104, 2, 8, 8, 0, 0, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(105, 2, 8, 8, 39, 1, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(106, 2, 8, 8, 39, 1, 55, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(107, 2, 8, 8, 39, 1, 56, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(108, 2, 8, 8, 51, 2, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(109, 2, 8, 8, 51, 2, 59, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(110, 2, 8, 8, 51, 2, 60, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(111, 2, 9, 9, 0, 0, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(112, 2, 9, 9, 40, 1, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(113, 2, 9, 9, 40, 1, 57, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(114, 2, 9, 9, 40, 1, 58, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(115, 2, 9, 9, 41, 2, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(116, 2, 9, 9, 59, 3, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(117, 2, 9, 9, 42, 4, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(118, 2, 9, 9, 43, 5, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(119, 2, 10, 10, 0, 0, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(120, 2, 10, 10, 44, 1, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(121, 2, 10, 10, 45, 2, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(122, 2, 10, 10, 46, 3, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(123, 2, 10, 10, 47, 4, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(124, 2, 10, 10, 48, 5, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(125, 2, 10, 10, 60, 6, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(126, 2, 11, 11, 0, 0, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(127, 2, 11, 11, 49, 1, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(128, 2, 11, 11, 50, 2, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(129, 2, 12, 12, 0, 0, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(130, 2, 12, 12, 52, 1, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(131, 2, 12, 12, 53, 2, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(132, 2, 12, 12, 54, 3, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(133, 2, 12, 12, 54, 3, 61, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(134, 2, 12, 12, 54, 3, 62, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(135, 2, 12, 12, 55, 4, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(136, 2, 12, 12, 55, 4, 63, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(137, 2, 12, 12, 55, 4, 64, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(138, 2, 12, 12, 56, 5, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(139, 2, 12, 12, 56, 5, 65, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(140, 2, 12, 12, 56, 5, 66, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(141, 2, 12, 12, 61, 6, 0, 0, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(142, 2, 12, 12, 61, 6, 69, 1, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(143, 2, 12, 12, 61, 6, 70, 2, '2019-06-11 05:33:07', '2019-06-11 05:33:07', 1),
(144, 1, 1, 1, 0, 0, 0, 0, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(145, 1, 2, 2, 0, 0, 0, 0, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(146, 1, 2, 2, 1, 1, 0, 0, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(147, 1, 2, 2, 1, 1, 1, 1, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(148, 1, 2, 2, 1, 1, 2, 2, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(149, 1, 2, 2, 1, 1, 3, 3, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(150, 1, 2, 2, 1, 1, 4, 4, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(151, 1, 2, 2, 1, 1, 5, 5, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(152, 1, 2, 2, 1, 1, 6, 6, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(153, 1, 2, 2, 1, 1, 7, 7, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(154, 1, 2, 2, 1, 1, 8, 8, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(155, 1, 2, 2, 1, 1, 9, 9, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(156, 1, 2, 2, 2, 2, 0, 0, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(157, 1, 2, 2, 2, 2, 10, 1, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(158, 1, 2, 2, 2, 2, 11, 2, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(159, 1, 2, 2, 2, 2, 12, 3, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(160, 1, 2, 2, 2, 2, 13, 4, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(161, 1, 2, 2, 2, 2, 14, 5, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(162, 1, 2, 2, 2, 2, 15, 6, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(163, 1, 2, 2, 2, 2, 16, 7, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(164, 1, 2, 2, 3, 3, 0, 0, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(165, 1, 2, 2, 3, 3, 17, 1, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(166, 1, 2, 2, 3, 3, 18, 2, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(167, 1, 2, 2, 3, 3, 19, 3, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(168, 1, 2, 2, 3, 3, 20, 4, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(169, 1, 2, 2, 3, 3, 21, 5, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(170, 1, 2, 2, 3, 3, 22, 6, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(171, 1, 2, 2, 3, 3, 67, 7, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(172, 1, 2, 2, 4, 4, 0, 0, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(173, 1, 2, 2, 4, 4, 23, 1, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(174, 1, 2, 2, 4, 4, 24, 2, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(175, 1, 2, 2, 4, 4, 25, 3, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(176, 1, 2, 2, 4, 4, 26, 4, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(177, 1, 2, 2, 4, 4, 27, 5, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(178, 1, 2, 2, 4, 4, 28, 6, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(179, 1, 2, 2, 4, 4, 29, 7, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(180, 1, 2, 2, 4, 4, 30, 8, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(181, 1, 2, 2, 4, 4, 68, 9, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(182, 1, 10, 10, 0, 0, 0, 0, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(183, 1, 10, 10, 44, 1, 0, 0, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(184, 1, 10, 10, 45, 2, 0, 0, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(185, 1, 10, 10, 46, 3, 0, 0, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(186, 1, 10, 10, 47, 4, 0, 0, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(187, 1, 10, 10, 48, 5, 0, 0, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1),
(188, 1, 10, 10, 60, 6, 0, 0, '2019-06-11 05:33:16', '2019-06-11 05:33:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `employeewarehouses`
--

CREATE TABLE `employeewarehouses` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee` int(10) UNSIGNED NOT NULL,
  `warehouse` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employeewarehouses`
--

INSERT INTO `employeewarehouses` (`id`, `employee`, `warehouse`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 2, 1, 1, '2019-05-16 08:57:25', '2019-05-16 08:57:25', 2, 2),
(2, 1, 1, 1, '2019-05-16 08:57:30', '2019-05-16 08:57:30', 2, 2),
(3, 2, 2, 1, '2019-05-16 08:57:38', '2019-05-16 08:57:38', 2, 2),
(4, 2, 3, 1, '2019-05-16 08:57:52', '2019-05-16 08:57:52', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 'Super Admin', 1, '2019-04-25 13:36:30', '2019-04-25 13:36:30', 1, 1),
(2, 'Administrator', 1, '2019-04-25 13:36:52', '2019-04-25 13:36:52', 1, 1),
(3, 'User EMKL', 1, '2019-05-16 08:30:05', '2019-05-16 08:30:05', 2, 2),
(4, 'User Sales Order', 1, '2019-05-16 08:30:17', '2019-05-16 08:30:17', 2, 2),
(5, 'User Delivery', 1, '2019-05-16 08:30:30', '2019-05-16 08:30:30', 2, 2),
(6, 'User Purchasing', 1, '2019-05-16 08:30:44', '2019-05-16 08:30:44', 2, 2),
(7, 'User Inventory', 1, '2019-05-16 08:31:01', '2019-05-16 08:31:01', 2, 2),
(8, 'User Monitoring', 1, '2019-05-16 08:31:12', '2019-05-16 08:31:12', 2, 2),
(9, 'User Approval', 1, '2019-05-16 08:31:24', '2019-05-16 08:31:24', 2, 2),
(10, 'User Mailbox', 1, '2019-05-16 08:31:34', '2019-05-16 08:31:34', 2, 2),
(11, 'User Finance', 1, '2019-05-16 08:31:43', '2019-05-16 08:31:43', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `groupsets`
--

CREATE TABLE `groupsets` (
  `id` int(10) UNSIGNED NOT NULL,
  `group` int(10) UNSIGNED NOT NULL,
  `menuroot` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groupsets`
--

INSERT INTO `groupsets` (`id`, `group`, `menuroot`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, 1, '2019-04-25 14:08:11', '2019-04-25 14:08:11', 2, 2),
(2, 1, 2, 1, '2019-04-25 14:08:14', '2019-04-25 14:08:14', 2, 2),
(3, 1, 10, 1, '2019-04-25 14:08:19', '2019-04-25 14:08:19', 2, 2),
(4, 2, 1, 1, '2019-04-25 14:08:43', '2019-04-25 14:08:43', 2, 2),
(5, 2, 2, 1, '2019-04-25 14:08:46', '2019-04-25 14:08:46', 2, 2),
(6, 2, 3, 1, '2019-04-25 14:08:48', '2019-04-25 14:08:48', 2, 2),
(7, 2, 4, 1, '2019-04-25 14:08:50', '2019-04-25 14:08:50', 2, 2),
(8, 2, 5, 1, '2019-04-25 14:08:53', '2019-04-25 14:08:53', 2, 2),
(9, 2, 6, 1, '2019-04-25 14:08:57', '2019-04-25 14:08:57', 2, 2),
(10, 2, 7, 1, '2019-04-25 14:09:04', '2019-04-25 14:09:04', 2, 2),
(11, 2, 8, 1, '2019-04-25 14:09:11', '2019-04-25 14:09:11', 2, 2),
(12, 2, 9, 1, '2019-04-25 14:09:17', '2019-04-25 14:09:17', 2, 2),
(13, 2, 10, 1, '2019-04-25 14:09:20', '2019-04-25 14:09:20', 2, 2),
(14, 2, 11, 1, '2019-04-25 14:09:23', '2019-04-25 14:09:23', 2, 2),
(15, 2, 12, 1, '2019-04-26 14:55:39', '2019-04-26 14:55:39', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `journaldetails`
--

CREATE TABLE `journaldetails` (
  `id` int(10) UNSIGNED NOT NULL,
  `journal` int(10) UNSIGNED NOT NULL,
  `accounting` int(10) UNSIGNED NOT NULL,
  `debit` tinyint(4) NOT NULL,
  `status_group` tinyint(4) NOT NULL,
  `status_record` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `journaldetails`
--

INSERT INTO `journaldetails` (`id`, `journal`, `accounting`, `debit`, `status_group`, `status_record`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, 1, 1, 1, 1, '2019-04-26 14:59:27', '2019-04-26 14:59:27', 2, 2),
(2, 1, 2, 0, 1, 1, 1, '2019-04-26 14:59:35', '2019-04-26 14:59:35', 2, 2),
(3, 2, 1, 1, 0, 0, 1, '2019-04-26 15:00:01', '2019-04-26 15:00:01', 2, 2),
(4, 2, 2, 0, 0, 0, 1, '2019-04-26 15:00:08', '2019-04-26 15:00:08', 2, 2),
(5, 3, 1, 1, 0, 0, 1, '2019-06-25 05:33:55', '2019-06-25 05:33:55', 2, 2),
(6, 3, 2, 0, 0, 0, 1, '2019-06-25 05:34:04', '2019-06-25 05:34:04', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `journals`
--

CREATE TABLE `journals` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `journals`
--

INSERT INTO `journals` (`id`, `code`, `name`, `notice`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 'INVM001', 'Inventory Masuk - Kelapa Butir', NULL, 1, '2019-04-26 14:58:29', '2019-04-26 14:58:29', 2, 2),
(2, 'INVM002', 'Inventory Masuk - Kopra', NULL, 1, '2019-04-26 14:59:50', '2019-04-26 14:59:50', 2, 2),
(3, 'INV003', 'INCOMING ALAT TULIS', 'ALAT TULIS', 1, '2019-06-25 05:33:42', '2019-06-25 05:33:42', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `journalsets`
--

CREATE TABLE `journalsets` (
  `id` int(10) UNSIGNED NOT NULL,
  `code_journal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_journal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_accounting` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_accounting` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` bigint(20) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kurs`
--

CREATE TABLE `kurs` (
  `id` int(10) UNSIGNED NOT NULL,
  `currency` int(10) UNSIGNED NOT NULL,
  `value` int(11) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kurs`
--

INSERT INTO `kurs` (`id`, `currency`, `value`, `start`, `end`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 2, 13700, '2019-02-18', '2019-02-22', 1, '2019-02-20 09:08:42', '2019-02-20 09:08:42', 1, 1),
(2, 2, 13900, '2019-02-25', '2019-02-28', 1, '2019-02-20 09:08:57', '2019-02-20 09:08:57', 1, 1),
(3, 2, 14100, '2019-07-16', '2019-07-16', 1, '2019-07-16 02:37:05', '2019-07-16 02:37:05', 2, 2),
(4, 2, 9000, '2015-07-17', '2015-07-17', 1, '2019-07-16 02:38:21', '2019-07-16 02:38:41', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `letters`
--

CREATE TABLE `letters` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `romawi` tinyint(4) NOT NULL,
  `year` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `letters`
--

INSERT INTO `letters` (`id`, `menu`, `company`, `code`, `romawi`, `year`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 'Quotation', 'SAA', 'PI', 1, 1, 1, '2019-02-23 15:11:45', '2019-07-16 02:51:18', 1, 2),
(2, 'Delivery Order', 'SAA', 'SJ', 1, 1, 1, '2019-02-23 15:11:57', '2019-02-23 15:11:57', 1, 1),
(3, 'Purchase Request', 'SAA', 'PR', 1, 1, 1, '2019-02-23 15:12:16', '2019-02-23 15:12:16', 1, 1),
(4, 'Purchase Order', 'SAA', 'PO', 1, 1, 1, '2019-03-05 05:16:58', '2019-03-05 05:16:58', 1, 1),
(5, 'Purchase General', 'SAA', 'PG', 1, 1, 1, '2019-05-16 08:46:19', '2019-05-16 08:46:19', 2, 2),
(6, 'Request Stock', 'SAA', 'RS', 1, 1, 1, '2019-05-16 08:46:41', '2019-05-16 08:46:41', 2, 2),
(7, 'Loading Stock', 'SAA', 'LS', 1, 1, 1, '2019-05-16 08:47:07', '2019-05-16 08:47:07', 2, 2),
(8, 'Incoming Stock', 'SAA', 'IS', 1, 1, 1, '2019-05-16 08:47:37', '2019-05-16 08:47:37', 2, 2),
(9, 'Outgoing Stock', 'SAA', 'OS', 1, 1, 1, '2019-05-16 08:48:00', '2019-05-16 08:48:00', 2, 2),
(10, 'Stock Opname', 'SAA', 'SO', 1, 1, 1, '2019-05-16 08:48:31', '2019-05-16 08:48:31', 2, 2),
(11, 'Assembly / Production', 'SAA', 'AP', 1, 1, 1, '2019-05-16 08:48:52', '2019-05-16 08:48:52', 2, 2),
(12, 'Proforma  Invoice', 'SAA', 'I', 1, 1, 0, '2019-07-16 02:56:30', '2019-07-16 02:57:46', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `mailcontacts`
--

CREATE TABLE `mailcontacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` bigint(20) NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mailcontacts`
--

INSERT INTO `mailcontacts` (`id`, `name`, `email`, `phone`, `subject`, `message`, `status`, `created_at`, `updated_at`) VALUES
(1, 'testing', 'tes@tes.com', 81234173436, 'Tes', 'tes', 1, '2019-03-08 06:43:12', '2019-06-06 12:08:48');

-- --------------------------------------------------------

--
-- Table structure for table `materialdetails`
--

CREATE TABLE `materialdetails` (
  `id` int(10) UNSIGNED NOT NULL,
  `material` int(10) UNSIGNED NOT NULL,
  `product` int(10) UNSIGNED NOT NULL,
  `unit` int(10) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `materialdetails`
--

INSERT INTO `materialdetails` (`id`, `material`, `product`, `unit`, `qty`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, 2, 5, 1, '2019-04-26 14:58:01', '2019-04-26 14:58:01', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `materialothers`
--

CREATE TABLE `materialothers` (
  `id` int(10) UNSIGNED NOT NULL,
  `material` int(10) UNSIGNED NOT NULL,
  `product` int(10) UNSIGNED NOT NULL,
  `unit` int(10) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `materials`
--

CREATE TABLE `materials` (
  `id` int(10) UNSIGNED NOT NULL,
  `product` int(10) UNSIGNED NOT NULL,
  `status_group` tinyint(4) NOT NULL,
  `status_record` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `materials`
--

INSERT INTO `materials` (`id`, `product`, `status_group`, `status_record`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, 1, 1, '2019-04-26 14:57:51', '2019-04-26 14:57:51', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `menuroots`
--

CREATE TABLE `menuroots` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menuroots`
--

INSERT INTO `menuroots` (`id`, `name`, `url`, `icon`, `icon_color`, `nomor`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 'Dashboard', 'home', 'home', 'purple', 1, 1, '2019-04-25 13:41:56', '2019-04-25 13:41:56', 2, 2),
(2, 'Master Data', '#', 'book', 'purple', 2, 1, '2019-04-25 13:42:20', '2019-04-25 13:42:20', 2, 2),
(3, 'Sales Order', '#', 'shopping-cart', 'purple', 3, 1, '2019-04-25 13:48:52', '2019-04-25 13:48:52', 2, 2),
(4, 'EMKL', '#', 'ship', 'purple', 4, 1, '2019-04-25 13:53:24', '2019-04-25 13:53:24', 2, 2),
(5, 'Delivery', '#', 'folder-open', 'purple', 5, 1, '2019-04-25 13:55:18', '2019-05-16 08:35:50', 2, 2),
(6, 'Purchasing', '#', 'tags', 'purple', 6, 1, '2019-04-25 13:59:34', '2019-04-25 13:59:34', 2, 2),
(7, 'Inventory', '#', 'edit', 'purple', 7, 1, '2019-04-25 14:00:51', '2019-04-25 14:00:51', 2, 2),
(8, 'Monitoring', '#', 'tasks', 'purple', 8, 1, '2019-04-25 14:03:29', '2019-04-25 14:03:29', 2, 2),
(9, 'Approval', '#', 'pencil', 'purple', 9, 1, '2019-04-25 14:04:31', '2019-04-25 14:04:31', 2, 2),
(10, 'User Management', '#', 'user', 'purple', 10, 1, '2019-04-25 14:05:57', '2019-04-25 14:05:57', 2, 2),
(11, 'Mailbox', '#', 'envelope', 'purple', 11, 1, '2019-04-25 14:07:04', '2019-04-25 14:07:04', 2, 2),
(12, 'Finance', '#', 'file', 'purple', 12, 1, '2019-04-26 14:53:11', '2019-04-26 14:53:11', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `menuroot` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `menuroot`, `name`, `url`, `icon`, `icon_color`, `nomor`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 2, 'General', '#', 'chevron-right', 'yellow', 1, 1, '2019-04-25 13:42:35', '2019-04-25 13:42:35', 2, 2),
(2, 2, 'Internal', '#', 'chevron-right', 'yellow', 2, 1, '2019-04-25 13:42:45', '2019-04-25 13:42:45', 2, 2),
(3, 2, 'Product', '#', 'chevron-right', 'yellow', 3, 1, '2019-04-25 13:42:53', '2019-04-25 13:42:53', 2, 2),
(4, 2, 'Accounting', '#', 'chevron-right', 'yellow', 4, 1, '2019-04-25 13:42:59', '2019-04-25 13:42:59', 2, 2),
(5, 3, 'Quotation', 'quotation', 'chevron-right', 'yellow', 1, 1, '2019-04-25 13:49:05', '2019-04-25 13:49:05', 2, 2),
(6, 3, 'Dummy Proforma', 'dummy', 'chevron-right', 'yellow', 2, 1, '2019-04-25 13:49:17', '2019-04-25 13:49:17', 2, 2),
(7, 3, 'Delivery Plan', 'plan', 'chevron-right', 'yellow', 3, 1, '2019-04-25 13:49:28', '2019-04-25 13:49:28', 2, 2),
(8, 3, 'Split Proforma', 'quotationsplit', 'chevron-right', 'yellow', 5, 1, '2019-04-25 13:49:40', '2019-05-16 08:34:51', 2, 2),
(9, 3, 'Verify Order', 'quotationplan', 'chevron-right', 'yellow', 6, 1, '2019-04-25 13:49:50', '2019-05-16 08:34:51', 2, 2),
(10, 3, 'Packing List', '#', 'chevron-right', 'yellow', 7, 1, '2019-04-25 13:50:00', '2019-05-16 08:34:52', 2, 2),
(11, 3, 'Invoice', '#', 'chevron-right', 'yellow', 8, 1, '2019-04-25 13:50:08', '2019-05-16 08:34:52', 2, 2),
(12, 3, 'DO Confirmation', '#', 'chevron-right', 'yellow', 9, 1, '2019-04-25 13:50:19', '2019-05-16 08:34:52', 2, 2),
(13, 3, 'DO Re-Route', '#', 'chevron-right', 'yellow', 10, 1, '2019-04-25 13:50:26', '2019-05-16 08:34:52', 2, 2),
(14, 4, 'Shipping Schedule', 'scheduleship', 'chevron-right', 'yellow', 1, 1, '2019-04-25 13:53:44', '2019-04-25 13:53:44', 2, 2),
(15, 4, 'Approval', '#', 'chevron-right', 'yellow', 2, 1, '2019-04-25 13:53:54', '2019-04-25 13:53:54', 2, 2),
(16, 4, 'Weighing', '#', 'chevron-right', 'yellow', 3, 1, '2019-04-25 13:53:59', '2019-04-25 13:53:59', 2, 2),
(17, 5, 'Booking Schedule', 'schedulebook', 'chevron-right', 'yellow', 1, 1, '2019-04-25 13:55:34', '2019-04-25 13:55:34', 2, 2),
(18, 5, 'Stuffing Schedule', 'schedulestuff', 'chevron-right', 'yellow', 2, 1, '2019-04-25 13:55:44', '2019-04-25 13:55:44', 2, 2),
(19, 5, 'Stuffing', '#', 'chevron-right', 'yellow', 3, 1, '2019-04-25 13:55:51', '2019-04-25 13:55:51', 2, 2),
(20, 5, 'Delivery Order', '#', 'chevron-right', 'yellow', 4, 1, '2019-04-25 13:55:59', '2019-04-25 13:55:59', 2, 2),
(21, 5, 'Weighing', '#', 'chevron-right', 'yellow', 5, 1, '2019-04-25 13:56:05', '2019-04-25 13:56:05', 2, 2),
(22, 4, 'Packing List', '#', 'chevron-right', 'yellow', 4, 1, '2019-04-25 13:56:15', '2019-05-16 08:36:03', 2, 2),
(23, 5, 'Document Check', '#', 'chevron-right', 'yellow', 7, 1, '2019-04-25 13:56:25', '2019-04-25 13:56:25', 2, 2),
(24, 5, 'Stuffing Re-Schedule', 'stuffreschedule', 'chevron-right', 'yellow', 8, 1, '2019-04-25 13:56:36', '2019-04-25 13:56:36', 2, 2),
(25, 6, 'Purchase Request', 'purchaserequest', 'chevron-right', 'yellow', 1, 1, '2019-04-25 13:59:50', '2019-04-25 13:59:50', 2, 2),
(26, 6, 'Purchase Order', 'purchaseorder', 'chevron-right', 'yellow', 3, 1, '2019-04-25 14:00:03', '2019-05-16 08:37:12', 2, 2),
(27, 6, 'Purchase Derivative', 'purchasederivative', 'chevron-right', 'yellow', 4, 1, '2019-04-25 14:00:15', '2019-05-16 08:37:12', 2, 2),
(28, 6, 'Incoming Stock', 'purchaseincome', 'chevron-right', 'yellow', 5, 1, '2019-04-25 14:00:28', '2019-05-16 08:37:12', 2, 2),
(29, 7, 'Request Stock', '#', 'chevron-right', 'yellow', 1, 1, '2019-04-25 14:01:03', '2019-04-25 14:01:03', 2, 2),
(30, 7, 'Loading Stock', 'stockload', 'chevron-right', 'yellow', 2, 1, '2019-04-25 14:01:14', '2019-04-25 14:01:14', 2, 2),
(31, 7, 'Quality Control', 'stockqc', 'chevron-right', 'yellow', 3, 1, '2019-04-25 14:01:26', '2019-04-25 14:01:26', 2, 2),
(32, 7, 'Incoming Stock', 'stockin', 'chevron-right', 'yellow', 4, 1, '2019-04-25 14:01:36', '2019-04-25 14:01:36', 2, 2),
(33, 7, 'Moving Stock', 'stockmove', 'chevron-right', 'yellow', 5, 1, '2019-04-25 14:01:47', '2019-04-25 14:01:47', 2, 2),
(34, 7, 'Outgoing Stock', 'stockout', 'chevron-right', 'yellow', 6, 1, '2019-04-25 14:02:01', '2019-04-25 14:02:01', 2, 2),
(35, 7, 'Stock Card', 'stockcard', 'chevron-right', 'yellow', 7, 1, '2019-04-25 14:02:12', '2019-04-25 14:02:12', 2, 2),
(36, 7, 'Stock Opname', 'stockopname', 'chevron-right', 'yellow', 8, 1, '2019-04-25 14:02:22', '2019-04-25 14:02:22', 2, 2),
(37, 7, 'Assembly / Production', 'production', 'chevron-right', 'yellow', 9, 1, '2019-04-25 14:02:34', '2019-04-25 14:02:34', 2, 2),
(38, 7, 'Cut Off', 'cutoff', 'chevron-right', 'yellow', 10, 1, '2019-04-25 14:02:45', '2019-04-25 14:02:45', 2, 2),
(39, 8, 'Stock', '#', 'chevron-right', 'yellow', 1, 1, '2019-04-25 14:03:43', '2019-04-25 14:03:43', 2, 2),
(40, 9, 'Packing List', '#', 'chevron-right', 'yellow', 1, 1, '2019-04-25 14:04:42', '2019-04-25 14:04:42', 2, 2),
(41, 9, 'Purchase Request', 'approvalpr', 'chevron-right', 'yellow', 2, 1, '2019-04-25 14:04:53', '2019-04-25 14:04:53', 2, 2),
(42, 9, 'Purchase Order', 'approvalpo', 'chevron-right', 'yellow', 4, 1, '2019-04-25 14:05:06', '2019-05-16 08:38:26', 2, 2),
(43, 9, 'Purchase Derivative', 'approvalpd', 'chevron-right', 'yellow', 5, 1, '2019-04-25 14:05:18', '2019-05-16 08:38:26', 2, 2),
(44, 10, 'Menu', 'menu', 'chevron-right', 'yellow', 1, 1, '2019-04-25 14:06:08', '2019-04-25 14:06:08', 2, 2),
(45, 10, 'Group', 'group', 'chevron-right', 'yellow', 2, 1, '2019-04-25 14:06:14', '2019-04-25 14:06:14', 2, 2),
(46, 10, 'Group Access', 'groupset', 'chevron-right', 'yellow', 3, 1, '2019-04-25 14:06:27', '2019-04-25 14:06:27', 2, 2),
(47, 10, 'User', 'employee', 'chevron-right', 'yellow', 4, 1, '2019-04-25 14:06:38', '2019-04-25 14:06:38', 2, 2),
(48, 10, 'User Access', 'employeeset', 'chevron-right', 'yellow', 5, 1, '2019-04-25 14:06:48', '2019-04-25 14:06:48', 2, 2),
(49, 11, 'Contact', 'mailcontact', 'chevron-right', 'yellow', 1, 1, '2019-04-25 14:07:19', '2019-04-25 14:07:19', 2, 2),
(50, 11, 'Order', 'mailorder', 'chevron-right', 'yellow', 2, 1, '2019-04-25 14:07:28', '2019-04-25 14:07:28', 2, 2),
(51, 8, 'Finance', '#', 'chevron-right', 'yellow', 2, 1, '2019-04-26 14:52:25', '2019-04-26 14:52:25', 2, 2),
(52, 12, 'Journal', 'journal', 'chevron-right', 'yellow', 1, 1, '2019-04-26 14:53:24', '2019-04-26 14:53:24', 2, 2),
(53, 12, 'Journal Process', 'journalset', 'chevron-right', 'yellow', 2, 1, '2019-04-26 14:53:33', '2019-04-26 14:53:33', 2, 2),
(54, 12, 'Transaction', '#', 'chevron-right', 'yellow', 3, 1, '2019-04-26 14:53:56', '2019-04-26 14:53:56', 2, 2),
(55, 12, 'Card', '#', 'chevron-right', 'yellow', 4, 1, '2019-04-26 14:54:02', '2019-04-26 14:54:02', 2, 2),
(56, 12, 'Cut Off', '#', 'chevron-right', 'yellow', 5, 1, '2019-04-26 14:54:09', '2019-04-26 14:54:09', 2, 2),
(57, 3, 'Report Plan', 'planreport', 'chevron-right', 'yellow', 4, 1, '2019-05-16 08:34:39', '2019-05-16 08:34:52', 2, 2),
(58, 6, 'Purchase General', 'purchasegeneral', 'chevron-right', 'yellow', 2, 1, '2019-05-16 08:37:04', '2019-05-16 08:37:12', 2, 2),
(59, 9, 'Purchase General', 'approvalprg', 'chevron-right', 'yellow', 3, 1, '2019-05-16 08:38:18', '2019-05-16 08:38:26', 2, 2),
(60, 10, 'User Warehouse', 'employeewarehouse', 'chevron-right', 'yellow', 6, 1, '2019-05-16 08:38:49', '2019-05-16 08:38:49', 2, 2),
(61, 12, 'DO Confirmation', '#', 'chevron-right', 'yellow', 6, 1, '2019-05-16 08:39:08', '2019-05-16 08:39:08', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `menusubs`
--

CREATE TABLE `menusubs` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menusubs`
--

INSERT INTO `menusubs` (`id`, `menu`, `name`, `url`, `icon`, `icon_color`, `nomor`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 'Country', 'country', 'circle-o', 'red', 1, 1, '2019-04-25 13:43:33', '2019-04-25 13:43:33', 2, 2),
(2, 1, 'City', 'city', 'circle-o', 'red', 2, 1, '2019-04-25 13:43:43', '2019-04-25 13:43:43', 2, 2),
(3, 1, 'Port', 'port', 'circle-o', 'red', 3, 1, '2019-04-25 13:43:52', '2019-04-25 13:43:52', 2, 2),
(4, 1, 'Bank', 'bank', 'circle-o', 'red', 4, 1, '2019-04-25 13:43:59', '2019-04-25 13:43:59', 2, 2),
(5, 1, 'Currency', 'currency', 'circle-o', 'red', 5, 1, '2019-04-25 13:44:08', '2019-04-25 13:44:08', 2, 2),
(6, 1, 'Kurs', 'kurs', 'circle-o', 'red', 6, 1, '2019-04-25 13:44:16', '2019-04-25 13:44:16', 2, 2),
(7, 1, 'Payment', 'payment', 'circle-o', 'red', 7, 1, '2019-04-25 13:44:25', '2019-04-25 13:44:25', 2, 2),
(8, 1, 'Payment Term', 'paymentterm', 'circle-o', 'red', 8, 1, '2019-04-25 13:44:41', '2019-04-25 13:44:41', 2, 2),
(9, 1, 'Container', 'container', 'circle-o', 'red', 9, 1, '2019-04-25 13:44:49', '2019-04-25 13:44:49', 2, 2),
(10, 2, 'Branch', 'branch', 'circle-o', 'red', 1, 1, '2019-04-25 13:45:10', '2019-04-25 13:45:10', 2, 2),
(11, 2, 'Department', 'department', 'circle-o', 'red', 2, 1, '2019-04-25 13:45:16', '2019-04-25 13:45:16', 2, 2),
(12, 2, 'Position', 'position', 'circle-o', 'red', 3, 1, '2019-04-25 13:45:25', '2019-04-25 13:45:25', 2, 2),
(13, 2, 'Warehouse', 'warehouse', 'circle-o', 'red', 4, 1, '2019-04-25 13:45:33', '2019-04-25 13:45:33', 2, 2),
(14, 2, 'Document', 'document', 'circle-o', 'red', 5, 1, '2019-04-25 13:45:42', '2019-04-25 13:45:42', 2, 2),
(15, 2, 'Letter', 'letter', 'circle-o', 'red', 6, 1, '2019-04-25 13:45:50', '2019-04-25 13:45:50', 2, 2),
(16, 2, 'Company', 'company', 'circle-o', 'red', 7, 1, '2019-04-25 13:45:59', '2019-04-25 13:45:59', 2, 2),
(17, 3, 'Variant', 'variant', 'circle-o', 'red', 1, 1, '2019-04-25 13:46:18', '2019-04-25 13:46:18', 2, 2),
(18, 3, 'Merk', 'merk', 'circle-o', 'red', 2, 1, '2019-04-25 13:46:26', '2019-04-25 13:46:26', 2, 2),
(19, 3, 'Size', 'size', 'circle-o', 'red', 3, 1, '2019-04-25 13:46:34', '2019-04-25 13:46:34', 2, 2),
(20, 3, 'Unit', 'unit', 'circle-o', 'red', 4, 1, '2019-04-25 13:46:43', '2019-04-25 13:46:43', 2, 2),
(21, 3, 'Type', 'type', 'circle-o', 'red', 5, 1, '2019-04-25 13:46:55', '2019-04-25 13:46:55', 2, 2),
(22, 3, 'Type Sub', 'typesub', 'circle-o', 'red', 6, 1, '2019-04-25 13:47:04', '2019-04-25 13:47:04', 2, 2),
(23, 4, 'Group Header', 'accountgroup', 'circle-o', 'red', 1, 1, '2019-04-25 13:47:22', '2019-04-25 13:47:22', 2, 2),
(24, 4, 'Account Type', 'accounttype', 'circle-o', 'red', 2, 1, '2019-04-25 13:47:30', '2019-04-25 13:47:30', 2, 2),
(25, 4, 'Accounting', 'account', 'circle-o', 'red', 3, 1, '2019-04-25 13:47:39', '2019-04-25 13:47:39', 2, 2),
(26, 4, 'Customer', 'customer', 'circle-o', 'red', 4, 1, '2019-04-25 13:47:47', '2019-04-25 13:47:47', 2, 2),
(27, 4, 'Supplier', 'supplier', 'circle-o', 'red', 5, 1, '2019-04-25 13:47:56', '2019-04-25 13:47:56', 2, 2),
(28, 4, 'Product', 'product', 'circle-o', 'red', 6, 1, '2019-04-25 13:48:05', '2019-04-25 13:48:05', 2, 2),
(29, 4, 'Transaction', 'transaction', 'circle-o', 'red', 7, 1, '2019-04-25 13:48:11', '2019-04-25 13:48:11', 2, 2),
(30, 4, 'Bill of Material', 'material', 'circle-o', 'red', 8, 1, '2019-04-25 13:48:23', '2019-04-25 13:48:23', 2, 2),
(31, 10, 'Shipping', 'packlist', 'circle-o', 'red', 1, 1, '2019-04-25 13:51:24', '2019-04-25 13:51:24', 2, 2),
(32, 10, 'Non Shipping', 'packlistnon', 'circle-o', 'red', 2, 1, '2019-04-25 13:51:33', '2019-04-25 13:51:33', 2, 2),
(33, 11, 'Shipping', 'invoice', 'circle-o', 'red', 1, 1, '2019-04-25 13:51:50', '2019-04-25 13:51:50', 2, 2),
(34, 11, 'Non Shipping', 'invoicenon', 'circle-o', 'red', 2, 1, '2019-04-25 13:51:58', '2019-04-25 13:51:58', 2, 2),
(35, 12, 'Shipping', 'stuffconfirm', 'circle-o', 'red', 1, 1, '2019-04-25 13:52:20', '2019-04-25 13:52:20', 2, 2),
(36, 12, 'Non Shipping', 'stuffconfirmnon', 'circle-o', 'red', 2, 1, '2019-04-25 13:52:28', '2019-04-25 13:52:28', 2, 2),
(37, 13, 'Shipping', 'stuffreroute', 'circle-o', 'red', 1, 1, '2019-04-25 13:52:53', '2019-04-25 13:52:53', 2, 2),
(38, 13, 'Non Shipping', 'stuffreroutenon', 'circle-o', 'red', 2, 1, '2019-04-25 13:53:00', '2019-04-25 13:53:00', 2, 2),
(39, 15, 'Booking Schedule', 'approvalbook', 'circle-o', 'red', 1, 1, '2019-04-25 13:54:22', '2019-04-25 13:54:22', 2, 2),
(40, 15, 'Stuffing Schedule', 'approvalstuff', 'circle-o', 'red', 2, 1, '2019-04-25 13:54:32', '2019-04-25 13:54:32', 2, 2),
(41, 16, 'Shipping', 'weighemkl', 'circle-o', 'red', 1, 1, '2019-04-25 13:54:51', '2019-04-25 13:54:51', 2, 2),
(42, 16, 'Non Shipping', 'weighemklnon', 'circle-o', 'red', 2, 1, '2019-04-25 13:54:58', '2019-04-25 13:54:58', 2, 2),
(43, 19, 'Shipping', 'stuff', 'circle-o', 'red', 1, 1, '2019-04-25 13:56:59', '2019-04-25 13:56:59', 2, 2),
(44, 19, 'Non Shipping', 'stuffnon', 'circle-o', 'red', 2, 1, '2019-04-25 13:57:09', '2019-04-25 13:57:09', 2, 2),
(45, 20, 'Shipping', 'stufforder', 'circle-o', 'red', 1, 1, '2019-04-25 13:57:43', '2019-04-25 13:57:43', 2, 2),
(46, 20, 'Non Shipping', 'stuffordernon', 'circle-o', 'red', 2, 1, '2019-04-25 13:57:50', '2019-04-25 13:57:50', 2, 2),
(47, 21, 'Shipping', 'weigh', 'circle-o', 'red', 1, 1, '2019-04-25 13:58:07', '2019-04-25 13:58:07', 2, 2),
(48, 21, 'Non Shipping', 'weighnon', 'circle-o', 'red', 2, 1, '2019-04-25 13:58:15', '2019-04-25 13:58:15', 2, 2),
(49, 22, 'Shipping', 'pack', 'circle-o', 'red', 1, 1, '2019-04-25 13:58:33', '2019-04-25 13:58:33', 2, 2),
(50, 22, 'Non Shipping', 'packnon', 'circle-o', 'red', 2, 1, '2019-04-25 13:58:45', '2019-04-25 13:58:45', 2, 2),
(51, 23, 'Shipping', 'documentcheck', 'circle-o', 'red', 1, 1, '2019-04-25 13:59:04', '2019-04-25 13:59:04', 2, 2),
(52, 23, 'Non Shipping', 'documentchecknon', 'circle-o', 'red', 2, 1, '2019-04-25 13:59:12', '2019-04-25 13:59:12', 2, 2),
(53, 29, 'Shipping', 'stockrequest', 'circle-o', 'red', 1, 1, '2019-04-25 14:03:04', '2019-04-25 14:03:04', 2, 2),
(54, 29, 'Non Shipping', 'stockrequestnon', 'circle-o', 'red', 2, 1, '2019-04-25 14:03:11', '2019-04-25 14:03:11', 2, 2),
(55, 39, 'Per Product', 'stockproduct', 'circle-o', 'red', 1, 1, '2019-04-25 14:04:03', '2019-04-25 14:04:03', 2, 2),
(56, 39, 'Per Warehouse', 'stockwarehouse', 'circle-o', 'red', 2, 1, '2019-04-25 14:04:14', '2019-04-25 14:04:14', 2, 2),
(57, 40, 'Shipping', 'approvalpack', 'circle-o', 'red', 1, 1, '2019-04-25 14:05:33', '2019-04-25 14:05:33', 2, 2),
(58, 40, 'Non Shipping', 'approvalpacknon', 'circle-o', 'red', 2, 1, '2019-04-25 14:05:40', '2019-04-25 14:05:40', 2, 2),
(59, 51, 'Debt', 'debtmonitor', 'circle-o', 'red', 1, 1, '2019-04-26 14:52:45', '2019-04-26 14:52:45', 2, 2),
(60, 51, 'Receivable', 'receivablemonitor', 'circle-o', 'red', 2, 1, '2019-04-26 14:52:54', '2019-04-26 14:52:54', 2, 2),
(61, 54, 'Debt', 'debt', 'circle-o', 'red', 1, 1, '2019-04-26 14:54:22', '2019-04-26 14:54:22', 2, 2),
(62, 54, 'Receivable', 'receivable', 'circle-o', 'red', 2, 1, '2019-04-26 14:54:31', '2019-04-26 14:54:31', 2, 2),
(63, 55, 'Debt', 'debtcard', 'circle-o', 'red', 1, 1, '2019-04-26 14:54:50', '2019-04-26 14:54:50', 2, 2),
(64, 55, 'Receivable', 'receivablecard', 'circle-o', 'red', 2, 1, '2019-04-26 14:54:57', '2019-04-26 14:54:57', 2, 2),
(65, 56, 'Debt', 'debtcutoff', 'circle-o', 'red', 1, 1, '2019-04-26 14:55:17', '2019-04-26 14:55:17', 2, 2),
(66, 56, 'Receivable', 'receivablecutoff', 'circle-o', 'red', 2, 1, '2019-04-26 14:55:26', '2019-04-26 14:55:26', 2, 2),
(67, 3, 'Price List', 'pricelist', 'circle-o', 'red', 7, 1, '2019-05-16 08:33:11', '2019-05-16 08:33:11', 2, 2),
(68, 4, 'Sales', 'sales', 'circle-o', 'red', 9, 1, '2019-05-16 08:33:59', '2019-05-16 08:33:59', 2, 2),
(69, 61, 'Shipping', 'stuffcheck', 'circle-o', 'red', 1, 1, '2019-05-16 08:39:25', '2019-05-16 08:39:25', 2, 2),
(70, 61, 'Non Shipping', 'stuffchecknon', 'circle-o', 'red', 2, 1, '2019-05-16 08:39:33', '2019-05-16 08:39:33', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `merks`
--

CREATE TABLE `merks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `merks`
--

INSERT INTO `merks` (`id`, `name`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, '-', 0, '2019-02-20 09:17:22', '2019-07-16 03:58:50', 1, 2),
(2, 'Merk', 1, '2019-02-20 09:17:30', '2019-02-20 09:17:30', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_02_18_000000_create_countries_table', 1),
(4, '2019_02_18_100000_create_cities_table', 1),
(5, '2019_02_18_200000_create_ports_table', 1),
(6, '2019_02_18_300000_create_banks_table', 1),
(7, '2019_02_18_400000_create_currencies_table', 1),
(8, '2019_02_18_500000_create_kurs_table', 1),
(9, '2019_02_18_600000_create_payments_table', 1),
(10, '2019_02_18_700000_create_branches_table', 1),
(11, '2019_02_18_800000_create_departments_table', 1),
(12, '2019_02_18_900000_create_positions_table', 1),
(13, '2019_02_19_000000_create_variants_table', 1),
(14, '2019_02_19_100000_create_merks_table', 1),
(15, '2019_02_19_200000_create_sizes_table', 1),
(16, '2019_02_19_300000_create_units_table', 1),
(17, '2019_02_19_400000_create_types_table', 1),
(18, '2019_02_19_500000_create_typesubs_table', 1),
(19, '2019_02_19_600000_create_warehouses_table', 1),
(20, '2019_02_19_700000_create_documents_table', 1),
(21, '2019_02_19_800000_create_accountgroups_table', 1),
(22, '2019_02_19_900000_create_accounttypes_table', 1),
(23, '2019_02_20_000000_create_accounts_table', 1),
(24, '2019_02_20_100000_create_customers_table', 1),
(25, '2019_02_20_200000_create_customerdetails_table', 1),
(26, '2019_02_20_300000_create_customeraccounts_table', 1),
(27, '2019_02_20_400000_create_suppliers_table', 1),
(28, '2019_02_20_500000_create_supplierdetails_table', 1),
(29, '2019_02_20_600000_create_supplieraccounts_table', 1),
(30, '2019_02_20_700000_create_products_table', 1),
(31, '2019_02_20_800000_create_productaliases_table', 1),
(32, '2019_02_20_900000_create_letters_table', 1),
(33, '2019_02_21_000000_create_companies_table', 1),
(34, '2019_02_21_100000_create_companyaccounts_table', 1),
(36, '2019_02_21_300000_create_containers_table', 2),
(37, '2019_02_21_400000_create_quotations_table', 3),
(38, '2019_02_21_500000_create_quotationdetails_table', 4),
(39, '2019_02_21_600000_create_quotationpayments_table', 5),
(40, '2019_02_23_000000_create_plans_table', 6),
(41, '2019_02_23_100000_create_purchaserequests_table', 7),
(42, '2019_02_24_000000_create_quotationsplits_table', 8),
(43, '2019_02_24_100000_create_quotationplans_table', 9),
(46, '2019_02_25_000000_create_scheduleships_table', 10),
(47, '2019_02_25_100000_create_schedulebooks_table', 11),
(48, '2019_02_25_200000_create_schedulestuffs_table', 12),
(49, '2019_02_26_000000_create_stockrequests_table', 13),
(50, '2019_02_26_100000_create_stuffs_table', 14),
(51, '2019_02_26_200000_create_stufforders_table', 15),
(52, '2019_02_27_000000_create_documentchecks_table', 16),
(54, '2019_03_06_000000_create_mailcontacts_table', 18),
(55, '2019_03_28_000000_create_paymentterms_table', 19),
(58, '2019_03_18_200000_create_materialdetails_table', 22),
(59, '2019_03_27_000000_create_stockloads_table', 23),
(60, '2019_03_05_000000_create_purchaseorders_table', 24),
(61, '2019_03_05_100000_create_purchasedetails_table', 25),
(62, '2019_03_05_200000_create_purchasedetailprs_table', 26),
(63, '2019_04_01_000000_create_stuffreschedules_table', 27),
(64, '2019_04_03_000000_create_stockqcs_table', 28),
(65, '2019_04_04_000000_create_stockdetails_table', 29),
(66, '2019_04_04_100000_create_stockopnames_table', 30),
(67, '2019_04_05_000000_create_productions_table', 31),
(68, '2019_04_05_100000_create_productiondetails_table', 32),
(69, '2019_04_06_000000_create_orders_table', 33),
(70, '2019_04_06_100000_create_orderdetails_table', 34),
(72, '2019_04_16_000000_create_stockcards_table', 36),
(73, '2019_03_18_000000_create_transactions_table', 37),
(74, '2019_03_18_100000_create_materials_table', 38),
(75, '2019_02_21_200000_create_stockins_table', 39),
(76, '2019_04_23_000000_create_menuroots_table', 40),
(77, '2019_04_23_100000_create_menus_table', 41),
(78, '2019_04_23_200000_create_menusubs_table', 42),
(79, '2019_04_23_300000_create_groups_table', 43),
(80, '2019_04_23_400000_create_groupsets_table', 44),
(81, '2019_04_24_000000_create_employees_table', 45),
(82, '2019_04_24_100000_create_employeesets_table', 46),
(83, '2019_04_26_000000_create_journals_table', 47),
(84, '2019_04_26_100000_create_journaldetails_table', 48),
(85, '2019_04_26_200000_create_journalsets_table', 49),
(86, '2019_04_26_300000_create_debts_table', 50),
(87, '2019_04_26_400000_create_debtcards_table', 51),
(88, '2019_04_26_500000_create_receivables_table', 52),
(89, '2019_04_26_600000_create_receivablecards_table', 53),
(90, '2019_05_02_000000_create_purchasegenerals_table', 54),
(91, '2019_05_02_100000_create_purchasedetailprgs_table', 55),
(92, '2019_05_02_200000_create_employeewarehouses_table', 56),
(93, '2019_05_02_300000_create_pricelists_table', 57),
(94, '2019_05_02_400000_create_sales_table', 58),
(95, '2019_03_18_300000_create_materialothers_table', 59),
(96, '2019_02_21_700000_create_quotationcharges_table', 60),
(97, '2019_04_05_200000_create_productionothers_table', 61);

-- --------------------------------------------------------

--
-- Table structure for table `orderdetails`
--

CREATE TABLE `orderdetails` (
  `id` int(10) UNSIGNED NOT NULL,
  `order` int(10) UNSIGNED NOT NULL,
  `product` int(10) UNSIGNED NOT NULL,
  `qty_order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orderdetails`
--

INSERT INTO `orderdetails` (`id`, `order`, `product`, `qty_order`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 1, '2019-07-23 12:58:56', '2019-07-23 12:58:56');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_po` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` bigint(20) NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer`, `no_po`, `email`, `phone`, `address`, `notice`, `status`, `created_at`, `updated_at`) VALUES
(1, 'A', 'B', 'C@a.com', 123, 'Wer', 'Qerdgv', 0, '2019-07-23 12:58:37', '2019-07-23 12:58:37');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `value` int(11) NOT NULL,
  `condition` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `value`, `condition`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 10, 'After the quality checking', 1, '2019-02-20 09:09:15', '2019-02-20 09:09:15', 1, 1),
(2, 40, 'After BL + Inv + PL by email', 1, '2019-02-20 09:09:24', '2019-02-20 09:09:24', 1, 1),
(3, 50, 'Down Payment', 1, '2019-02-20 09:09:34', '2019-07-16 02:44:11', 1, 2),
(4, 100, '10 days after arrival', 1, '2019-02-20 09:09:45', '2019-07-15 09:54:58', 1, 2),
(5, 70, 'Down Payment', 1, '2019-07-16 02:41:25', '2019-07-16 02:41:25', 2, 2),
(6, 30, 'After QC Checking', 1, '2019-07-16 02:41:32', '2019-07-16 02:41:50', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `paymentterms`
--

CREATE TABLE `paymentterms` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `paymentterms`
--

INSERT INTO `paymentterms` (`id`, `code`, `name`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 'CNF', 'Cost and Freight', 1, '2019-04-06 14:20:59', '2019-04-06 14:20:59', 1, 1),
(2, 'FOB', 'Free On Board', 1, '2019-04-06 14:21:07', '2019-04-06 14:21:07', 1, 1),
(3, 'CIF', 'Container Insurance Freight', 1, '2019-07-16 02:46:16', '2019-07-16 02:46:27', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `id` int(10) UNSIGNED NOT NULL,
  `quotationdetail` int(10) UNSIGNED NOT NULL,
  `stockin` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `no_batch` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty_plan` int(11) NOT NULL,
  `status_split` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `quotationdetail`, `stockin`, `no_inc`, `no_batch`, `qty_plan`, `status_split`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 2, 1, 1, 'SAA/PI00002/VI/2019-1', 3, 1, '2019-06-25 04:10:52', '2019-06-25 06:42:21', 2, 2),
(4, 1, 3, 1, 'SAA/PI00001/VI/2019-1', 26000, 1, '2019-06-25 06:07:14', '2019-06-25 06:44:07', 2, 2),
(5, 1, 4, 2, 'SAA/PI00001/VI/2019-2', 52000, 1, '2019-06-25 06:07:24', '2019-06-25 06:43:57', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ports`
--

CREATE TABLE `ports` (
  `id` int(10) UNSIGNED NOT NULL,
  `country` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ports`
--

INSERT INTO `ports` (`id`, `country`, `no_inc`, `code`, `name`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, 'P00001', 'Any Port of', 1, '2019-02-20 06:37:04', '2019-02-20 06:37:04', 1, 1),
(2, 1, 2, 'P00002', 'Semarang', 1, '2019-02-20 06:37:14', '2019-02-20 06:37:14', 1, 1),
(3, 2, 3, 'P00003', 'Bangkok', 1, '2019-02-20 06:37:22', '2019-02-20 06:37:22', 1, 1),
(4, 1, 4, 'P00004', 'Jakarta', 1, '2019-02-24 12:57:05', '2019-02-24 12:57:05', 1, 1),
(5, 1, 5, 'P00005', 'Surabaya', 1, '2019-02-24 12:57:17', '2019-02-24 12:57:17', 1, 1),
(6, 2, 6, 'P00006', 'Laem Chabang', 1, '2019-06-27 03:03:19', '2019-06-27 03:03:19', 2, 2),
(7, 4, 7, 'P00007', 'Caucedo', 1, '2019-07-15 10:13:14', '2019-07-15 10:13:14', 2, 2),
(8, 7, 8, 'P00008', 'Ekaterinburg', 1, '2019-07-16 02:31:15', '2019-07-16 02:31:32', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `no_inc`, `code`, `name`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 'POS00001', 'Administrator', 1, '2019-02-20 09:11:24', '2019-02-20 09:11:24', 1, 1),
(2, 2, 'POS00002', 'Owner', 1, '2019-02-20 09:11:35', '2019-02-20 09:11:35', 1, 1),
(3, 3, 'POS00003', 'Director', 1, '2019-02-20 09:11:43', '2019-02-20 09:11:43', 1, 1),
(4, 4, 'POS00004', 'Manager', 1, '2019-02-20 09:11:52', '2019-02-20 09:11:52', 1, 1),
(5, 5, 'POS00005', 'Supervisor', 1, '2019-02-20 09:12:02', '2019-02-20 09:12:02', 1, 1),
(6, 6, 'POS00006', 'Staff', 1, '2019-02-20 09:12:10', '2019-02-20 09:12:10', 1, 1),
(7, 7, 'POS00007', 'General', 0, '2019-07-16 02:30:33', '2019-07-16 02:30:48', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `pricelists`
--

CREATE TABLE `pricelists` (
  `id` int(10) UNSIGNED NOT NULL,
  `product` int(10) UNSIGNED NOT NULL,
  `currency` int(10) UNSIGNED NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pricelists`
--

INSERT INTO `pricelists` (`id`, `product`, `currency`, `price`, `date`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, '1100', '2019-07-19', 1, '2019-05-16 08:50:14', '2019-05-16 08:50:14', 2, 2),
(2, 4, 1, '850000', '2019-07-19', 1, '2019-07-16 03:56:06', '2019-07-16 03:57:56', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `productaliases`
--

CREATE TABLE `productaliases` (
  `id` int(10) UNSIGNED NOT NULL,
  `product` int(10) UNSIGNED NOT NULL,
  `customerdetail` int(10) UNSIGNED NOT NULL,
  `alias_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productaliases`
--

INSERT INTO `productaliases` (`id`, `product`, `customerdetail`, `alias_name`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, 'kelapa export', 1, '2019-02-20 09:28:36', '2019-06-25 03:51:48', 1, 1),
(2, 3, 2, 'kelapa santan', 1, '2019-06-26 12:28:59', '2019-06-26 12:49:35', 2, 2),
(3, 4, 3, 'Frozen Cassava(2)', 1, '2019-07-16 03:01:03', '2019-07-16 03:01:03', 2, 2),
(4, 5, 3, 'Frozen Coconut Chunk', 1, '2019-07-16 03:04:21', '2019-07-16 03:04:21', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `productiondetails`
--

CREATE TABLE `productiondetails` (
  `id` int(10) UNSIGNED NOT NULL,
  `production` int(10) UNSIGNED NOT NULL,
  `product` int(11) NOT NULL,
  `unit` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productiondetails`
--

INSERT INTO `productiondetails` (`id`, `production`, `product`, `unit`, `qty`, `price`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, 2, 100000, 110000000, 1, '2019-06-25 05:13:44', '2019-06-25 05:13:44', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `productionothers`
--

CREATE TABLE `productionothers` (
  `id` int(10) UNSIGNED NOT NULL,
  `production` int(10) UNSIGNED NOT NULL,
  `product` int(11) NOT NULL,
  `unit` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `productions`
--

CREATE TABLE `productions` (
  `id` int(10) UNSIGNED NOT NULL,
  `material` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `nomor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productions`
--

INSERT INTO `productions` (`id`, `material`, `no_inc`, `nomor`, `qty`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, 'SAA/AP00001/VI/2019', 20000, 1, '2019-06-25 05:13:44', '2019-06-25 05:13:44', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `accounting` int(10) UNSIGNED NOT NULL,
  `department` int(10) UNSIGNED NOT NULL,
  `variant` int(10) NOT NULL,
  `merk` int(10) NOT NULL,
  `typesub` int(10) NOT NULL,
  `size` int(10) NOT NULL,
  `unit` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `general` tinyint(4) NOT NULL,
  `status_group` tinyint(4) NOT NULL,
  `status_record` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `accounting`, `department`, `variant`, `merk`, `typesub`, `size`, `unit`, `code`, `name`, `general`, `status_group`, `status_record`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 5, 2, 2, 2, 2, 2, 'KLP_BTR', 'Kelapa Butir', 0, 1, 1, 1, '2019-02-20 09:28:26', '2019-05-16 08:55:26', 1, 2),
(2, 1, 4, 2, 2, 2, 2, 2, 'ATK', 'Alat Tulis Kantor', 1, 0, 0, 1, '2019-05-16 08:55:55', '2019-05-16 08:55:55', 2, 2),
(3, 1, 5, 2, 2, 2, 2, 2, 'BB001', 'Kelapa Pandan Wangi', 1, 1, 0, 1, '2019-06-26 12:22:58', '2019-06-26 12:31:04', 2, 2),
(4, 1, 5, 1, 1, 1, 1, 3, 'FC', 'Frozen Cassava', 1, 0, 0, 1, '2019-07-16 02:59:19', '2019-07-16 02:59:19', 2, 2),
(5, 1, 5, 1, 1, 1, 1, 3, 'FCC', 'Frozen Coconut Chunk', 1, 0, 0, 1, '2019-07-16 03:03:32', '2019-07-16 03:12:41', 2, 2),
(6, 1, 5, 1, 1, 1, 1, 3, 'FCD', 'Frozen Coconut Dice', 1, 0, 0, 1, '2019-07-16 03:07:25', '2019-07-16 03:07:25', 2, 2),
(7, 1, 5, 1, 1, 1, 1, 3, 'CB', 'Coconut Carbon', 1, 0, 0, 1, '2019-07-16 03:14:05', '2019-07-16 03:14:05', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `purchasedetailprgs`
--

CREATE TABLE `purchasedetailprgs` (
  `id` int(10) UNSIGNED NOT NULL,
  `purchasegeneral` int(10) UNSIGNED NOT NULL,
  `product` int(10) UNSIGNED NOT NULL,
  `supplierdetail` int(11) NOT NULL,
  `warehouse` int(11) NOT NULL,
  `qty_request` int(11) NOT NULL,
  `qty_remain` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchasedetailprs`
--

CREATE TABLE `purchasedetailprs` (
  `id` int(10) UNSIGNED NOT NULL,
  `purchasedetail` int(10) UNSIGNED NOT NULL,
  `purchaserequest` int(11) NOT NULL,
  `purchasedetailprg` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchasedetailprs`
--

INSERT INTO `purchasedetailprs` (`id`, `purchasedetail`, `purchaserequest`, `purchasedetailprg`, `qty`, `created_at`, `updated_at`, `created_user`) VALUES
(1, 5, 2, 0, 199997, '2019-06-25 04:48:37', '2019-06-25 04:48:37', 2),
(2, 6, 5, 0, 1000, '2019-06-26 11:04:47', '2019-06-26 11:04:47', 2),
(3, 7, 6, 0, 4000, '2019-06-26 11:19:02', '2019-06-26 11:19:19', 2);

-- --------------------------------------------------------

--
-- Table structure for table `purchasedetails`
--

CREATE TABLE `purchasedetails` (
  `id` int(10) UNSIGNED NOT NULL,
  `purchaseorder` int(10) UNSIGNED NOT NULL,
  `product` int(10) UNSIGNED NOT NULL,
  `supplierdetail` int(10) UNSIGNED NOT NULL,
  `warehouse` int(10) UNSIGNED NOT NULL,
  `qty_order` int(11) NOT NULL,
  `qty_buffer` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `qty_qc` int(11) NOT NULL DEFAULT '0',
  `status_qc` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchasedetails`
--

INSERT INTO `purchasedetails` (`id`, `purchaseorder`, `product`, `supplierdetail`, `warehouse`, `qty_order`, `qty_buffer`, `price`, `qty_qc`, `status_qc`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, 2, 2, 0, 5, 1000, 5, 1, 1, '2019-05-21 03:43:25', '2019-05-21 03:49:34', 2, 2),
(2, 2, 1, 2, 2, 0, 100000, 1000, 0, 0, 1, '2019-06-25 04:39:54', '2019-06-25 04:40:34', 2, 2),
(3, 2, 1, 2, 3, 0, 0, 0, 0, 0, 1, '2019-06-25 04:41:36', '2019-06-25 04:41:36', 2, 2),
(4, 4, 1, 2, 2, 0, 100, 20, 0, 0, 1, '2019-06-25 04:46:44', '2019-06-25 04:47:35', 2, 2),
(5, 5, 1, 2, 3, 199997, 20000, 2200, 160000, 1, 1, '2019-06-25 04:47:32', '2019-06-25 04:52:43', 2, 2),
(6, 6, 1, 2, 1, 1000, 0, 0, 0, 0, 1, '2019-06-26 11:02:38', '2019-06-26 11:04:47', 2, 2),
(7, 7, 1, 2, 1, 4000, 1000, 10000, 0, 0, 1, '2019-06-26 11:16:19', '2019-06-26 11:19:19', 2, 2),
(8, 8, 1, 2, 1, 0, 0, 0, 0, 0, 1, '2019-06-26 11:33:20', '2019-06-26 11:33:20', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `purchasegenerals`
--

CREATE TABLE `purchasegenerals` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `no_prg` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_order` date NOT NULL,
  `notice` text COLLATE utf8mb4_unicode_ci,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `approve` tinyint(4) NOT NULL DEFAULT '1',
  `status_approve` tinyint(4) NOT NULL DEFAULT '0',
  `status_order` tinyint(4) NOT NULL DEFAULT '0',
  `status_release` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL,
  `approved_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchaseorders`
--

CREATE TABLE `purchaseorders` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `no_po` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_order` date NOT NULL,
  `tax` tinyint(4) NOT NULL,
  `notice` text COLLATE utf8mb4_unicode_ci,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `approve` tinyint(4) NOT NULL DEFAULT '1',
  `status_approve` tinyint(4) NOT NULL DEFAULT '0',
  `status_release` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL,
  `approved_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchaseorders`
--

INSERT INTO `purchaseorders` (`id`, `no_inc`, `no_po`, `date_order`, `tax`, `notice`, `reason`, `approve`, `status_approve`, `status_release`, `status`, `created_at`, `updated_at`, `approved_at`, `created_user`, `updated_user`, `approved_user`) VALUES
(1, 1, 'SAA/PO00001/V/2019', '2019-05-21', 0, '', 'lanjut', 1, 1, 1, 1, '2019-05-21 03:42:26', '2019-05-21 03:46:55', '2019-05-21 03:46:55', 2, 2, 2),
(2, 2, 'SAA/PO00002/VI/2019', '2019-06-25', 0, 'baru', 'oke', 1, 1, 1, 1, '2019-06-25 04:39:06', '2019-06-25 04:49:00', '2019-06-25 04:49:00', 2, 2, 2),
(3, 3, 'SAA/PO00003/VI/2019', '2019-06-27', 0, 'ere', '', 1, 0, 1, 1, '2019-06-25 04:46:24', '2019-06-25 04:48:18', NULL, 2, 2, NULL),
(4, 4, 'SAA/PO00004/VI/2019', '2019-06-27', 0, 'ere', 'oke', 1, 1, 1, 1, '2019-06-25 04:46:30', '2019-06-25 04:48:50', '2019-06-25 04:48:50', 2, 2, 2),
(5, 5, 'SAA/PO00005/VI/2019', '2019-06-25', 0, '', 'tidak lengkap', 1, 2, 1, 1, '2019-06-25 04:47:11', '2019-06-25 04:48:37', '2019-06-25 04:48:37', 2, 2, 2),
(6, 6, 'SAA/PO00006/VI/2019', '2019-06-26', 0, '', '', 1, 0, 0, 1, '2019-06-26 11:00:43', '2019-06-26 11:00:43', NULL, 2, 2, NULL),
(7, 7, 'SAA/PO00007/VI/2019', '2019-06-26', 0, 'uji coba ke 2', '', 1, 0, 0, 1, '2019-06-26 11:15:38', '2019-06-26 11:15:38', NULL, 2, 2, NULL),
(8, 8, 'SAA/PO00008/VI/2019', '2019-06-26', 0, 'coba ke 2-1', '', 1, 0, 0, 0, '2019-06-26 11:33:06', '2019-06-26 11:33:58', NULL, 2, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `purchaserequests`
--

CREATE TABLE `purchaserequests` (
  `id` int(10) UNSIGNED NOT NULL,
  `product` int(10) UNSIGNED NOT NULL,
  `quotationdetail` int(11) NOT NULL,
  `supplierdetail` int(11) NOT NULL,
  `warehouse` int(11) NOT NULL,
  `no_inc` int(11) NOT NULL,
  `no_pr` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_request` date NOT NULL,
  `qty_request` int(11) NOT NULL,
  `qty_remain` int(11) NOT NULL,
  `no_reference` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notice` text COLLATE utf8mb4_unicode_ci,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `approve` tinyint(4) NOT NULL,
  `status_approve` tinyint(4) NOT NULL,
  `status_plan` tinyint(4) NOT NULL,
  `status_order` tinyint(4) NOT NULL DEFAULT '0',
  `status_release` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL,
  `approved_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchaserequests`
--

INSERT INTO `purchaserequests` (`id`, `product`, `quotationdetail`, `supplierdetail`, `warehouse`, `no_inc`, `no_pr`, `date_request`, `qty_request`, `qty_remain`, `no_reference`, `notice`, `reason`, `approve`, `status_approve`, `status_plan`, `status_order`, `status_release`, `status`, `created_at`, `updated_at`, `approved_at`, `created_user`, `updated_user`, `approved_user`) VALUES
(1, 1, 0, 2, 2, 1, 'SAA/PR00001/V/2019', '2019-05-21', 100, 100, '', '', 'ok deh', 1, 1, 0, 0, 1, 1, '2019-05-21 03:45:05', '2019-05-21 03:46:11', '2019-05-21 03:46:11', 2, 2, 2),
(2, 1, 2, 0, 0, 1, 'SAA/PR00001/VI/2019', '2019-06-25', 199997, 0, 'SAA/PI00002/VI/2019', 'supplier sembarang', 'oke', 1, 1, 1, 0, 1, 1, '2019-06-25 04:11:51', '2019-06-25 04:48:37', '2019-06-25 04:33:56', 2, 2, 2),
(3, 1, 0, 0, 0, 2, 'SAA/PR00002/VI/2019', '2019-06-25', 2000, 2000, '', 'tt', '', 1, 0, 0, 0, 0, 1, '2019-06-25 04:44:44', '2019-06-25 04:44:44', NULL, 2, 2, NULL),
(4, 1, 1, 0, 2, 3, 'SAA/PR00003/VI/2019', '2019-06-25', 122000, 122000, 'SAA/PI00001/VI/2019', '', '', 1, 1, 1, 0, 1, 1, '2019-06-25 06:45:52', '2019-06-25 06:49:08', NULL, 2, 2, NULL),
(5, 1, 0, 2, 1, 4, 'SAA/PR00004/VI/2019', '2019-06-26', 1000, 0, '', 'uji coba', '.', 1, 1, 0, 0, 1, 1, '2019-06-26 10:50:25', '2019-06-26 11:04:47', '2019-06-26 10:59:10', 2, 2, 2),
(6, 1, 0, 2, 1, 5, 'SAA/PR00005/VI/2019', '2019-06-26', 4000, 0, '', 'uji coba ke 2', 'coba ke 2', 1, 1, 0, 0, 1, 1, '2019-06-26 11:11:54', '2019-06-26 11:19:19', '2019-06-26 11:13:29', 2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `quotationcharges`
--

CREATE TABLE `quotationcharges` (
  `id` int(10) UNSIGNED NOT NULL,
  `quotation` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quotationdetails`
--

CREATE TABLE `quotationdetails` (
  `id` int(10) UNSIGNED NOT NULL,
  `quotation` int(10) UNSIGNED NOT NULL,
  `product` int(10) UNSIGNED NOT NULL,
  `unit` int(10) UNSIGNED NOT NULL,
  `qty_fcl` int(11) NOT NULL,
  `qty_bag` int(11) NOT NULL,
  `qty_kg` int(11) NOT NULL,
  `qty_pcs` int(11) NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kurs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disc_type` tinyint(4) NOT NULL,
  `disc_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias_qty` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty_amount` int(11) NOT NULL,
  `status_plan` tinyint(4) NOT NULL DEFAULT '0',
  `qty_plan` int(11) NOT NULL DEFAULT '0',
  `status_book` tinyint(4) NOT NULL DEFAULT '0',
  `qty_book` int(11) NOT NULL DEFAULT '0',
  `status_other` tinyint(4) NOT NULL DEFAULT '0',
  `qty_other` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quotationdetails`
--

INSERT INTO `quotationdetails` (`id`, `quotation`, `product`, `unit`, `qty_fcl`, `qty_bag`, `qty_kg`, `qty_pcs`, `price`, `kurs`, `disc_type`, `disc_value`, `alias_name`, `alias_qty`, `qty_amount`, `status_plan`, `qty_plan`, `status_book`, `qty_book`, `status_other`, `qty_other`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, 2, 8, 8000, 200000, 200000, '0.2', '14500', 0, '', 'kelapa export', 'kg', 122000, 1, 78000, 1, 122000, 0, 0, 1, '2019-06-25 03:51:48', '2019-06-25 06:45:52', 2, 2),
(2, 2, 1, 2, 8, 10000, 200000, 200000, '0.2', '14500', 0, '', '', '', 199997, 1, 3, 1, 199997, 0, 0, 1, '2019-06-25 04:06:07', '2019-06-25 04:11:51', 2, 2),
(3, 4, 3, 2, 1000, 10, 1000, 10, '5000', '1', 0, '', 'kelapa santan', 'KG', 1000, 0, 0, 0, 0, 0, 0, 1, '2019-06-26 12:49:35', '2019-06-26 12:49:35', 2, 2),
(4, 4, 2, 2, 1000, 1000, 100, 10, '5000', '0', 2, '100', 'coba lagi', 'unit kopasus', 100, 0, 0, 0, 0, 0, 0, 0, '2019-06-26 12:55:48', '2019-06-26 13:02:47', 2, 2),
(5, 5, 1, 2, 10000, 1000, 20, 10, '1100', '0', 2, '', 'kelapa santan', 'KG', 20, 0, 0, 0, 0, 0, 0, 1, '2019-06-26 13:18:40', '2019-06-26 13:19:56', 2, 2),
(6, 8, 1, 2, 1000, 100, 200, 1000, '1100', '0', 2, '100', '', '', 200, 0, 0, 0, 0, 0, 0, 1, '2019-06-27 00:49:47', '2019-06-27 00:51:45', 2, 2),
(7, 3, 1, 2, 1000, 100, 500, 500, '1100', '0', 0, '', '', '', 500, 0, 0, 0, 0, 0, 0, 1, '2019-06-27 01:15:43', '2019-06-27 01:19:34', 2, 2),
(8, 9, 1, 2, 12, 1000, 25000, 1, '885000', '0', 0, '', '', '', 25000, 0, 0, 0, 0, 0, 0, 1, '2019-07-15 10:24:43', '2019-07-15 10:24:43', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `quotationpayments`
--

CREATE TABLE `quotationpayments` (
  `id` int(10) UNSIGNED NOT NULL,
  `quotation` int(10) UNSIGNED NOT NULL,
  `payment` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quotationpayments`
--

INSERT INTO `quotationpayments` (`id`, `quotation`, `payment`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 3, 1, '2019-06-25 03:55:06', '2019-06-25 03:55:06', 2, 2),
(2, 1, 2, 1, '2019-06-25 03:55:13', '2019-06-25 03:55:13', 2, 2),
(3, 1, 1, 1, '2019-06-25 03:55:18', '2019-06-25 03:55:18', 2, 2),
(4, 2, 3, 1, '2019-06-25 04:06:25', '2019-06-25 04:06:25', 2, 2),
(5, 2, 2, 1, '2019-06-25 04:06:42', '2019-06-25 04:06:42', 2, 2),
(6, 2, 1, 1, '2019-06-25 04:06:47', '2019-06-25 04:06:47', 2, 2),
(7, 5, 1, 1, '2019-06-26 13:38:53', '2019-06-26 13:38:53', 2, 2),
(8, 8, 1, 1, '2019-06-27 01:00:00', '2019-06-27 01:00:00', 2, 2),
(9, 9, 4, 1, '2019-07-15 10:27:25', '2019-07-15 10:27:25', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `quotationplans`
--

CREATE TABLE `quotationplans` (
  `id` int(10) UNSIGNED NOT NULL,
  `quotationsplit` int(10) UNSIGNED NOT NULL,
  `plan` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quotationplans`
--

INSERT INTO `quotationplans` (`id`, `quotationsplit`, `plan`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, '2019-06-25 06:42:21', '2019-06-25 06:42:21', 2, 2),
(2, 2, 5, '2019-06-25 06:43:57', '2019-06-25 06:43:57', 2, 2),
(3, 2, 4, '2019-06-25 06:44:07', '2019-06-25 06:44:07', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `quotations`
--

CREATE TABLE `quotations` (
  `id` int(10) UNSIGNED NOT NULL,
  `customerdetail` int(10) UNSIGNED NOT NULL,
  `companyaccount` int(10) UNSIGNED NOT NULL,
  `paymentterm` int(10) UNSIGNED NOT NULL,
  `container` int(10) UNSIGNED NOT NULL,
  `pol` int(10) UNSIGNED NOT NULL,
  `pod` int(10) UNSIGNED NOT NULL,
  `currency` int(10) UNSIGNED NOT NULL,
  `sales` int(10) UNSIGNED NOT NULL,
  `tax` tinyint(4) NOT NULL,
  `status_flow` tinyint(4) NOT NULL,
  `status_sp` tinyint(4) NOT NULL,
  `no_inc` int(11) NOT NULL,
  `no_sp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_sp` date NOT NULL,
  `no_poc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disc_type` tinyint(4) NOT NULL,
  `disc_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notice` text COLLATE utf8mb4_unicode_ci,
  `etd` date NOT NULL,
  `eta` date NOT NULL,
  `qty_gross` int(11) NOT NULL,
  `qty_cbm` int(11) NOT NULL,
  `kurs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vessel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_term` int(11) NOT NULL DEFAULT '0',
  `qty_print` int(11) NOT NULL DEFAULT '0',
  `qty_product` tinyint(4) NOT NULL DEFAULT '0',
  `qty_amount` tinyint(4) NOT NULL DEFAULT '0',
  `status_product` tinyint(11) NOT NULL DEFAULT '0',
  `status_close` tinyint(4) NOT NULL DEFAULT '0',
  `status_release` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quotations`
--

INSERT INTO `quotations` (`id`, `customerdetail`, `companyaccount`, `paymentterm`, `container`, `pol`, `pod`, `currency`, `sales`, `tax`, `status_flow`, `status_sp`, `no_inc`, `no_sp`, `date_sp`, `no_poc`, `disc_type`, `disc_value`, `notice`, `etd`, `eta`, `qty_gross`, `qty_cbm`, `kurs`, `vessel`, `payment_term`, `qty_print`, `qty_product`, `qty_amount`, `status_product`, `status_close`, `status_release`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, 1, 2, 1, 3, 2, 1, 0, 1, 1, 1, 'SAA/PI00001/VI/2019', '2019-06-26', 'berkah001', 0, '', 'Testing data', '2019-06-25', '2019-06-25', 200000, 40, '14500', '', 100, 0, 1, 4, 1, 1, 0, 1, '2019-06-25 03:50:13', '2019-06-25 06:07:24', 2, 2),
(2, 1, 1, 1, 2, 1, 3, 2, 1, 0, 1, 1, 2, 'SAA/PI00002/VI/2019', '2019-06-26', 'BKH00111', 0, '', 'Testing data', '2019-06-25', '2019-06-25', 200000, 40, '14500', '', 100, 1, 1, 1, 1, 1, 1, 1, '2019-06-25 04:05:21', '2019-06-25 04:10:52', 2, 2),
(3, 1, 1, 2, 2, 1, 1, 1, 1, 0, 2, 1, 3, 'SAA/PI00003/VI/2019', '2019-06-25', 'PO01', 2, '', 'Testing data', '2019-06-25', '2019-07-25', 1000, 40, '0', '', 0, 0, 1, 0, 1, 0, 0, 1, '2019-06-25 06:02:28', '2019-06-27 01:19:23', 2, 2),
(4, 2, 1, 2, 4, 1, 1, 1, 1, 0, 2, 1, 4, 'SAA/PI00004/VI/2019', '2019-06-26', 'PO001/VaraFood', 0, '', 'hapus setelah selesai', '2019-06-26', '2019-06-27', 1000, 5000, '0', 'kapal', 0, 0, 1, 0, 1, 0, 0, 1, '2019-06-26 12:48:40', '2019-06-26 13:02:47', 2, 2),
(5, 2, 1, 1, 2, 2, 5, 1, 2, 0, 2, 1, 5, 'SAA/PI00005/VI/2019', '2019-06-26', 'PO002/varafood', 0, '', 'hapus lagi setelah selesai', '2019-06-26', '2019-06-27', 10000, 1000, '0', 'kapal', 10, 0, 1, 0, 1, 1, 0, 0, '2019-06-26 13:17:14', '2019-06-27 00:35:58', 2, 2),
(6, 1, 1, 1, 4, 1, 1, 1, 1, 0, 2, 1, 6, 'SAA/PI00006/VI/2019', '2019-06-27', 'PO005', 0, '', 'test data', '2019-06-27', '2019-06-29', 10000, 1000, '0', '', 0, 0, 0, 0, 0, 0, 0, 1, '2019-06-27 00:44:48', '2019-06-27 00:44:48', 2, 2),
(7, 1, 1, 1, 4, 1, 1, 1, 1, 0, 2, 1, 7, 'SAA/PI00007/VI/2019', '2019-06-27', 'PO005', 0, '', 'test data', '2019-06-27', '2019-06-29', 10000, 1000, '0', '', 0, 0, 0, 0, 0, 0, 0, 1, '2019-06-27 00:45:33', '2019-06-27 00:45:33', 2, 2),
(8, 1, 1, 1, 4, 1, 1, 1, 1, 0, 2, 1, 8, 'SAA/PI00008/VI/2019', '2019-06-27', 'PO005', 0, '', 'test data pertama', '2019-06-27', '2019-06-29', 10000, 1000, '0', '', 10, 0, 1, 0, 1, 0, 0, 1, '2019-06-27 00:47:48', '2019-06-27 01:00:00', 2, 2),
(9, 3, 1, 1, 6, 5, 7, 2, 1, 0, 1, 1, 9, 'SAA/PI00009/VII/2019', '2019-07-01', '1907021', 0, '', '', '2019-07-15', '2019-07-15', 300000, 40, '14100', '', 100, 0, 1, 0, 1, 0, 0, 1, '2019-07-15 10:17:26', '2019-07-15 10:27:25', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `quotationsplits`
--

CREATE TABLE `quotationsplits` (
  `id` int(10) UNSIGNED NOT NULL,
  `quotation` int(10) UNSIGNED NOT NULL,
  `pol` int(10) UNSIGNED NOT NULL,
  `pod` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `no_split` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `etd` date NOT NULL,
  `eta` date NOT NULL,
  `qty_print` int(11) NOT NULL DEFAULT '0',
  `qty_invoice` int(11) NOT NULL DEFAULT '0',
  `status_request` tinyint(4) NOT NULL DEFAULT '0',
  `status_load` tinyint(4) NOT NULL DEFAULT '0',
  `status_stuff` tinyint(4) NOT NULL DEFAULT '0',
  `status_send` tinyint(4) NOT NULL DEFAULT '0',
  `status_pack` tinyint(4) NOT NULL DEFAULT '0',
  `status_document` tinyint(4) NOT NULL DEFAULT '0',
  `status_confirm` tinyint(4) NOT NULL DEFAULT '0',
  `status_reroute` tinyint(4) NOT NULL DEFAULT '0',
  `status_book` tinyint(4) NOT NULL DEFAULT '0',
  `status_journal` tinyint(4) NOT NULL DEFAULT '0',
  `status_release` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sent_at` timestamp NULL DEFAULT NULL,
  `packed_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL,
  `sent_user` int(11) DEFAULT NULL,
  `packed_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quotationsplits`
--

INSERT INTO `quotationsplits` (`id`, `quotation`, `pol`, `pod`, `no_inc`, `no_split`, `etd`, `eta`, `qty_print`, `qty_invoice`, `status_request`, `status_load`, `status_stuff`, `status_send`, `status_pack`, `status_document`, `status_confirm`, `status_reroute`, `status_book`, `status_journal`, `status_release`, `status`, `created_at`, `updated_at`, `sent_at`, `packed_at`, `created_user`, `updated_user`, `sent_user`, `packed_user`) VALUES
(1, 2, 5, 3, 1, 'SAA/PI00002/VI/2019-A', '2019-06-25', '2019-06-25', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, '2019-06-25 06:42:01', '2019-06-25 07:06:38', NULL, NULL, 2, 2, NULL, NULL),
(2, 1, 5, 3, 1, 'SAA/PI00001/VI/2019-A', '2019-06-25', '2019-07-25', 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, '2019-06-25 06:43:50', '2019-06-25 07:05:12', NULL, NULL, 2, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `receivablecards`
--

CREATE TABLE `receivablecards` (
  `id` int(10) UNSIGNED NOT NULL,
  `customerdetail` int(10) UNSIGNED NOT NULL,
  `year` int(11) NOT NULL,
  `month` tinyint(4) NOT NULL,
  `debit` bigint(20) NOT NULL,
  `kredit` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `receivables`
--

CREATE TABLE `receivables` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_sp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_customer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_customer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_product` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_product` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_sp` date NOT NULL,
  `date_sj` date NOT NULL,
  `qty` int(11) NOT NULL,
  `price` bigint(20) NOT NULL,
  `total` bigint(20) NOT NULL,
  `payment` bigint(20) NOT NULL,
  `adjustment` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(10) UNSIGNED NOT NULL,
  `accounting` int(10) UNSIGNED NOT NULL,
  `city` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `status_group` tinyint(4) NOT NULL,
  `status_record` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `accounting`, `city`, `code`, `name`, `address`, `mobile`, `birthday`, `status_group`, `status_record`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 3, 'SALES01', 'Ferry Wijaya', 'Jl. Pisang Raja No. 30', '6281999887766', '1980-10-23', 1, 1, 1, '2019-05-16 08:56:42', '2019-07-16 03:54:13', 2, 2),
(2, 2, 1, 'SALES02', 'Sales Kedua', 'Perumahan Medokan Ayu', '6281999887766', '1977-03-05', 1, 1, 0, '2019-05-16 08:56:59', '2019-07-16 03:54:21', 2, 2),
(3, 1, 12, 'SLS003', 'RENGGA', 'Jl. Veteran X', '6281999887766', '2000-08-30', 0, 1, 0, '2019-06-26 12:33:04', '2019-07-16 03:54:24', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `schedulebooks`
--

CREATE TABLE `schedulebooks` (
  `id` int(10) UNSIGNED NOT NULL,
  `quotationsplit` int(10) UNSIGNED NOT NULL,
  `pol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pod` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lines` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `etd` date NOT NULL,
  `eta` date NOT NULL,
  `notice` text COLLATE utf8mb4_unicode_ci,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `status_approve` tinyint(4) NOT NULL DEFAULT '0',
  `status_stuff` tinyint(4) NOT NULL DEFAULT '0',
  `status_stuff_acc` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `schedulebooks`
--

INSERT INTO `schedulebooks` (`id`, `quotationsplit`, `pol`, `pod`, `lines`, `etd`, `eta`, `notice`, `reason`, `status_approve`, `status_stuff`, `status_stuff_acc`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(4, 2, 'Surabaya', 'Laem Chabang', 'ONE', '2019-12-27', '2018-01-03', '', 'batal', 2, 0, 0, '2019-06-25 07:05:12', '2019-06-25 07:13:02', 2, 2),
(5, 1, 'Surabaya', 'Laem Chabang', 'ONE', '2019-12-27', '2018-01-03', '', '1', 1, 1, 1, '2019-06-25 07:06:38', '2019-06-25 07:20:17', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `scheduleships`
--

CREATE TABLE `scheduleships` (
  `id` int(10) UNSIGNED NOT NULL,
  `pol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pod` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lines` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `etd` date NOT NULL,
  `eta` date NOT NULL,
  `status_book` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scheduleships`
--

INSERT INTO `scheduleships` (`id`, `pol`, `pod`, `lines`, `etd`, `eta`, `status_book`, `created_at`, `updated_at`, `created_user`) VALUES
(1, 'Surabaya', 'Laem Chabang', 'ONE', '2019-12-27', '2018-01-03', 1, '2019-06-25 06:56:05', '2019-06-25 07:05:12', 1),
(2, 'Surabaya', 'Bangkok', 'OOCL', '2019-12-29', '2018-01-05', 0, '2019-06-25 06:56:05', '2019-06-25 06:56:05', 1),
(3, 'Surabaya', 'Bangkok', 'SITC', '2020-02-20', '2019-02-28', 0, '2019-06-25 06:56:05', '2019-06-25 06:56:05', 1),
(4, 'Surabaya', 'Bangkok', 'RJP', '2020-03-01', '2019-03-07', 0, '2019-06-25 06:56:05', '2019-06-25 06:56:05', 1),
(5, 'Jakarta', 'Laem Chabang', 'ONE', '2020-02-20', '2019-02-28', 0, '2019-06-25 06:56:05', '2019-06-25 06:56:05', 1),
(6, 'Jakarta', 'Bangkok', 'OOCL', '2020-02-20', '2019-02-28', 0, '2019-06-25 06:56:05', '2019-06-25 06:56:05', 1),
(7, 'Jakarta', 'Bangkok', 'RJP', '2020-03-01', '2019-03-07', 0, '2019-06-25 06:56:05', '2019-06-25 06:56:05', 1),
(8, 'Semarang', 'Laem Chabang', 'ONE', '2019-12-27', '2018-01-03', 0, '2019-06-25 06:56:05', '2019-06-25 06:56:05', 1),
(9, 'Semarang', 'Bangkok', 'OOCL', '2019-12-29', '2018-01-05', 0, '2019-06-25 06:56:05', '2019-06-25 06:56:05', 1);

-- --------------------------------------------------------

--
-- Table structure for table `schedulestuffs`
--

CREATE TABLE `schedulestuffs` (
  `id` int(10) UNSIGNED NOT NULL,
  `schedulebook` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `person` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice` text COLLATE utf8mb4_unicode_ci,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `vessel` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `freight` int(11) NOT NULL,
  `qty_fcl` int(11) NOT NULL,
  `qty_approve` int(11) NOT NULL DEFAULT '0',
  `qty_amount` int(11) NOT NULL DEFAULT '0',
  `qty_print` int(11) NOT NULL DEFAULT '0',
  `qty_invoice` int(11) NOT NULL DEFAULT '0',
  `status_approve` tinyint(4) NOT NULL DEFAULT '0',
  `status_request` tinyint(4) NOT NULL DEFAULT '0',
  `status_load` tinyint(4) NOT NULL DEFAULT '0',
  `status_stuff` tinyint(4) NOT NULL DEFAULT '0',
  `status_delivery` tinyint(4) NOT NULL DEFAULT '0',
  `status_send` tinyint(4) NOT NULL DEFAULT '0',
  `status_pack` tinyint(4) NOT NULL DEFAULT '0',
  `status_document` tinyint(4) NOT NULL DEFAULT '0',
  `status_reschedule` tinyint(4) NOT NULL DEFAULT '0',
  `status_confirm` tinyint(4) NOT NULL DEFAULT '0',
  `status_reroute` tinyint(4) NOT NULL DEFAULT '0',
  `status_journal` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sent_at` timestamp NULL DEFAULT NULL,
  `packed_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL,
  `sent_user` int(11) DEFAULT NULL,
  `packed_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `schedulestuffs`
--

INSERT INTO `schedulestuffs` (`id`, `schedulebook`, `date`, `person`, `notice`, `reason`, `vessel`, `freight`, `qty_fcl`, `qty_approve`, `qty_amount`, `qty_print`, `qty_invoice`, `status_approve`, `status_request`, `status_load`, `status_stuff`, `status_delivery`, `status_send`, `status_pack`, `status_document`, `status_reschedule`, `status_confirm`, `status_reroute`, `status_journal`, `created_at`, `updated_at`, `sent_at`, `packed_at`, `created_user`, `updated_user`, `sent_user`, `packed_user`) VALUES
(1, 5, '2019-06-25', 'Sono', '', 'Ok', 'Kapal', 1, 2, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2019-06-25 07:16:48', '2019-06-25 07:20:17', NULL, NULL, 2, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `name`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, '-', 1, '2019-02-20 09:17:40', '2019-02-20 09:17:40', 1, 1),
(2, 'Size', 1, '2019-02-20 09:17:48', '2019-02-20 09:17:48', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `stockcards`
--

CREATE TABLE `stockcards` (
  `id` int(10) UNSIGNED NOT NULL,
  `product` int(10) UNSIGNED NOT NULL,
  `year` int(11) NOT NULL,
  `month` tinyint(4) NOT NULL,
  `qty` int(11) NOT NULL,
  `qty_out` int(11) NOT NULL DEFAULT '0',
  `price` varbinary(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stockcards`
--

INSERT INTO `stockcards` (`id`, `product`, `year`, `month`, `qty`, `qty_out`, `price`, `created_at`, `updated_at`) VALUES
(1, 1, 2019, 5, 3, 0, 1000, '2019-05-21 03:54:28', '2019-05-21 03:54:28'),
(2, 1, 1, 1, 1, 0, 1, '2019-06-06 13:45:35', '2019-06-06 13:45:35'),
(3, 1, 2019, 6, 180000, 0, 2200, '2019-06-25 05:01:15', '2019-06-25 05:02:17'),
(4, 2, 2019, 6, 50000, 0, 1000, '2019-06-25 05:08:19', '2019-06-25 05:08:19');

-- --------------------------------------------------------

--
-- Table structure for table `stockdetails`
--

CREATE TABLE `stockdetails` (
  `id` int(10) UNSIGNED NOT NULL,
  `stockqc` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `no_bag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty_bag` int(11) NOT NULL,
  `qty_pcs` int(11) NOT NULL,
  `qty_kg` int(11) NOT NULL,
  `barcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stockins`
--

CREATE TABLE `stockins` (
  `id` int(10) UNSIGNED NOT NULL,
  `transaction` int(10) UNSIGNED NOT NULL,
  `product` int(10) UNSIGNED NOT NULL,
  `warehouse` int(10) UNSIGNED NOT NULL,
  `supplierdetail` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `nomor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_in` date NOT NULL,
  `date_move` date DEFAULT NULL,
  `date_out` date DEFAULT NULL,
  `date_opname` date DEFAULT NULL,
  `noref_in` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noref_out` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty_bag` int(11) NOT NULL,
  `qty_pcs` int(11) NOT NULL,
  `qty_kg` int(11) NOT NULL,
  `qty_qc` int(11) NOT NULL DEFAULT '0',
  `qty_validate` int(11) NOT NULL DEFAULT '0',
  `qty_available` int(11) NOT NULL,
  `qty_booked` int(11) NOT NULL DEFAULT '0',
  `qty_transit` int(11) NOT NULL DEFAULT '0',
  `qty_move` int(11) NOT NULL DEFAULT '0',
  `qty_out` int(11) NOT NULL DEFAULT '0',
  `qty_opname` int(11) NOT NULL DEFAULT '0',
  `transaction_out` int(11) NOT NULL DEFAULT '0',
  `transaction_prod` int(11) NOT NULL,
  `status_qc` tinyint(4) NOT NULL DEFAULT '0',
  `status_validate` tinyint(4) NOT NULL DEFAULT '0',
  `status_move` tinyint(4) NOT NULL,
  `status_out` tinyint(4) NOT NULL DEFAULT '0',
  `status_opname` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `validated_at` timestamp NULL DEFAULT NULL,
  `moved_at` timestamp NULL DEFAULT NULL,
  `outed_at` timestamp NULL DEFAULT NULL,
  `opnamed_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL,
  `validated_user` int(11) DEFAULT NULL,
  `moved_user` int(11) DEFAULT NULL,
  `outed_user` int(11) DEFAULT NULL,
  `opnamed_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stockins`
--

INSERT INTO `stockins` (`id`, `transaction`, `product`, `warehouse`, `supplierdetail`, `no_inc`, `nomor`, `date_in`, `date_move`, `date_out`, `date_opname`, `noref_in`, `noref_out`, `price`, `qty_bag`, `qty_pcs`, `qty_kg`, `qty_qc`, `qty_validate`, `qty_available`, `qty_booked`, `qty_transit`, `qty_move`, `qty_out`, `qty_opname`, `transaction_out`, `transaction_prod`, `status_qc`, `status_validate`, `status_move`, `status_out`, `status_opname`, `status`, `created_at`, `updated_at`, `validated_at`, `moved_at`, `outed_at`, `opnamed_at`, `created_user`, `updated_user`, `validated_user`, `moved_user`, `outed_user`, `opnamed_user`) VALUES
(1, 1, 1, 2, 2, 1, 'SAA/IS00001/V/2019', '2019-05-22', NULL, NULL, NULL, 'SAA/PO00001/V/2019', NULL, '1000', 1, 1, 3, 0, 3, 0, 3, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, '2019-05-21 03:49:03', '2019-06-25 04:10:52', '2019-05-21 03:54:28', NULL, NULL, NULL, 2, 2, 2, NULL, NULL, NULL),
(2, 1, 2, 2, 2, 2, 'SAA/IS00002/V/2019', '2019-05-21', NULL, NULL, NULL, 'SAA/PO00001/V/2019-A', NULL, '500', 11, 2, 2, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '2019-05-21 03:52:48', '2019-05-21 03:52:48', NULL, NULL, NULL, NULL, 2, 2, NULL, NULL, NULL, NULL),
(3, 1, 1, 2, 2, 3, 'SAA/IS00003/VI/2019', '2019-06-25', NULL, NULL, NULL, 'SAA/PO00005/VI/2019', NULL, '2200', 20, 50000, 50000, 0, 0, 24000, 26000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '2019-06-25 04:50:45', '2019-06-25 06:07:14', NULL, NULL, NULL, NULL, 2, 2, NULL, NULL, NULL, NULL),
(4, 1, 1, 2, 2, 4, 'SAA/IS00004/VI/2019', '2019-06-28', NULL, NULL, NULL, 'SAA/PO00005/VI/2019', NULL, '2200', 20, 60000, 60000, 0, 60000, 8000, 52000, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, '2019-06-25 04:51:43', '2019-06-25 06:07:24', '2019-06-25 05:02:17', NULL, NULL, NULL, 2, 2, 2, NULL, NULL, NULL),
(5, 1, 2, 2, 2, 5, 'SAA/IS00005/VI/2019', '2019-06-25', NULL, NULL, NULL, 'SAA/PO00005/VI/2019-A', NULL, '1000', 20, 50000, 50000, 0, 50000, 50000, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, '2019-06-25 05:06:44', '2019-06-25 05:08:19', '2019-06-25 05:08:19', NULL, NULL, NULL, 2, 2, 2, NULL, NULL, NULL),
(6, 2, 1, 1, 1, 6, 'SAA/IS00006/VI/2019', '2019-06-25', NULL, NULL, NULL, 'PRODUCTION', NULL, '5500', 1, 1, 20000, 0, 0, 20000, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, '2019-06-25 05:13:44', '2019-06-25 06:06:54', NULL, NULL, NULL, NULL, 2, 2, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stockloads`
--

CREATE TABLE `stockloads` (
  `id` int(10) UNSIGNED NOT NULL,
  `stuff` int(10) UNSIGNED NOT NULL,
  `stockin` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `nomor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty_load` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stockopnames`
--

CREATE TABLE `stockopnames` (
  `id` int(10) UNSIGNED NOT NULL,
  `stockin` int(10) UNSIGNED NOT NULL,
  `qty_kg` int(11) NOT NULL,
  `qty_in` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stockqcs`
--

CREATE TABLE `stockqcs` (
  `id` int(10) UNSIGNED NOT NULL,
  `purchasedetail` int(10) UNSIGNED NOT NULL,
  `product` int(10) UNSIGNED NOT NULL,
  `warehouse` int(10) UNSIGNED NOT NULL,
  `supplierdetail` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `no_po` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_in` date NOT NULL,
  `qty_bag` int(11) NOT NULL,
  `qty_pcs` int(11) NOT NULL,
  `qty_kg` int(11) NOT NULL,
  `qty_avg` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `notice` text COLLATE utf8mb4_unicode_ci,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `detail_count` int(11) NOT NULL DEFAULT '0',
  `detail_total` int(11) NOT NULL DEFAULT '0',
  `approve` tinyint(4) NOT NULL DEFAULT '1',
  `status_approve` tinyint(4) NOT NULL DEFAULT '0',
  `status_validasi` tinyint(4) NOT NULL DEFAULT '0',
  `status_release` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL,
  `approved_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stockqcs`
--

INSERT INTO `stockqcs` (`id`, `purchasedetail`, `product`, `warehouse`, `supplierdetail`, `no_inc`, `no_po`, `date_in`, `qty_bag`, `qty_pcs`, `qty_kg`, `qty_avg`, `price`, `notice`, `reason`, `detail_count`, `detail_total`, `approve`, `status_approve`, `status_validasi`, `status_release`, `status`, `created_at`, `updated_at`, `approved_at`, `created_user`, `updated_user`, `approved_user`) VALUES
(1, 1, 1, 2, 2, 0, 'SAA/PO00001/V/2019', '2019-05-21', 1, 1, 3, 3, 1000, '', '', 0, 0, 1, 0, 0, 0, 1, '2019-05-21 03:49:03', '2019-05-21 03:49:03', NULL, 2, 2, NULL),
(2, 1, 2, 2, 2, 1, 'SAA/PO00001/V/2019-A', '2019-05-21', 11, 2, 2, 0, 500, 'kualitas kurang apik', 'terima aja', 0, 0, 1, 1, 0, 1, 1, '2019-05-21 03:49:34', '2019-05-21 03:52:48', '2019-05-21 03:52:48', 2, 2, 2),
(3, 5, 1, 2, 2, 0, 'SAA/PO00005/VI/2019', '2019-06-25', 20, 50000, 50000, 2500, 2200, '', '', 0, 0, 1, 0, 0, 0, 1, '2019-06-25 04:50:45', '2019-06-25 04:50:45', NULL, 2, 2, NULL),
(4, 5, 1, 2, 2, 0, 'SAA/PO00005/VI/2019', '2019-06-28', 20, 60000, 60000, 3000, 2200, 'kirim ke 2', '', 0, 0, 1, 0, 0, 0, 1, '2019-06-25 04:51:43', '2019-06-25 04:51:43', NULL, 2, 2, NULL),
(5, 5, 2, 2, 2, 1, 'SAA/PO00005/VI/2019-A', '2019-06-25', 20, 50000, 50000, 2500, 1000, 'kopra', 'ok', 0, 0, 1, 1, 0, 1, 1, '2019-06-25 04:52:43', '2019-06-25 05:06:44', '2019-06-25 05:06:44', 2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `stufforders`
--

CREATE TABLE `stufforders` (
  `id` int(10) UNSIGNED NOT NULL,
  `schedulestuff` int(10) NOT NULL,
  `quotationsplit` int(11) NOT NULL,
  `no_inc` int(11) NOT NULL,
  `no_letter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `check_quality` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `check_weigh` int(11) NOT NULL,
  `notice` text COLLATE utf8mb4_unicode_ci,
  `qty_print` int(11) NOT NULL DEFAULT '0',
  `status_confirm` tinyint(4) NOT NULL DEFAULT '0',
  `status_reroute` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stuffreschedules`
--

CREATE TABLE `stuffreschedules` (
  `id` int(10) UNSIGNED NOT NULL,
  `schedulestuff` int(10) UNSIGNED NOT NULL,
  `reason` tinyint(4) NOT NULL,
  `date_change` date NOT NULL,
  `date_old` date NOT NULL,
  `notice` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stuffs`
--

CREATE TABLE `stuffs` (
  `id` int(10) UNSIGNED NOT NULL,
  `schedulestuff` int(10) NOT NULL,
  `quotationsplit` int(11) NOT NULL,
  `pol` int(10) UNSIGNED NOT NULL,
  `pod` int(10) UNSIGNED NOT NULL,
  `container` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `nomor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `etd` date NOT NULL,
  `eta` date NOT NULL,
  `vehicle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty_fcl` int(11) NOT NULL,
  `qty_bag` int(11) NOT NULL,
  `qty_pcs` int(11) NOT NULL,
  `qty_kg` int(11) NOT NULL,
  `nopol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `driver` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice` text COLLATE utf8mb4_unicode_ci,
  `container_empty` int(11) NOT NULL,
  `container_bruto` int(11) NOT NULL,
  `container_netto` int(11) NOT NULL,
  `qty_load` int(11) NOT NULL DEFAULT '0',
  `status_load` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `supplieraccounts`
--

CREATE TABLE `supplieraccounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `supplier` int(10) UNSIGNED NOT NULL,
  `currency` int(10) UNSIGNED NOT NULL,
  `bank` int(10) UNSIGNED NOT NULL,
  `account_no` bigint(20) NOT NULL,
  `account_swift` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supplieraccounts`
--

INSERT INTO `supplieraccounts` (`id`, `supplier`, `currency`, `bank`, `account_no`, `account_swift`, `account_name`, `alias`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, 2, 6543219870, 'SAA', 'PT. STARINDO ANUGERAH ABADI', 'PT. SAA', 1, '2019-02-20 09:27:51', '2019-05-16 08:55:00', 1, 2),
(2, 2, 1, 3, 6543219870, 'SANTO', 'PT. Sentosa', 'Sentosa', 1, '2019-02-20 09:27:51', '2019-02-20 09:27:51', 1, 1),
(3, 4, 1, 1, 7455152020, '-', 'PT Kelapa Batam Jaya', '', 1, '2019-06-26 12:10:56', '2019-07-16 03:22:13', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `supplierdetails`
--

CREATE TABLE `supplierdetails` (
  `id` int(10) UNSIGNED NOT NULL,
  `supplier` int(10) UNSIGNED NOT NULL,
  `city` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias_mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supplierdetails`
--

INSERT INTO `supplierdetails` (`id`, `supplier`, `city`, `name`, `address`, `mobile`, `alias_name`, `alias_mobile`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 3, 'PT. Starindo Anugerah Abadi', 'Diamond Hill', '6281999554433', 'SAA', '6231232425', 1, '2019-02-20 09:27:07', '2019-05-16 08:54:30', 1, 2),
(2, 2, 2, 'PT. Sentosa', 'Perumahan Graha Asri', '6281999334455', 'Santoso', '6231787970', 1, '2019-02-20 09:27:07', '2019-02-20 09:27:07', 1, 1),
(3, 3, 8, 'PT Kelapa Palu Jaya', 'Jl. Raya Kayumalue Blok - No. - RT: 001 RW: 001 Kel. Kayumalue Ngapa Kec. Palu Utara. Kota/Kab. PALU SULAWESI TENGAH.', '+6285232005777', 'KPJ', '', 1, '2019-06-26 11:42:52', '2019-07-16 03:28:57', 2, 2),
(4, 4, 9, 'PT Kelapa Batam Jaya', 'Batam', '081266424604', 'KBJ', '', 1, '2019-06-26 11:52:34', '2019-07-16 03:20:21', 2, 2),
(5, 5, 10, 'PT Kelapa Sumatera Jaya', 'Jl. Ratu Dibalau Perum TJ Raya Perumahan Blok 16 No. 00', '08117233274', 'KSJ', '', 1, '2019-07-16 03:34:26', '2019-07-16 03:34:26', 2, 2),
(6, 6, 12, 'PT Olahan Kelapa Nusantara', 'Jl. Kabupaten No 101, RT. 005, RW 004, Desa Tambakrejo, Kec. Kraton, Pasuruan', '082232421050', 'OKN', '', 1, '2019-07-16 03:37:16', '2019-07-16 03:37:16', 2, 2),
(7, 7, 12, 'PT Aneka Kelapa Nusantara', 'Jl. Kabupaten RT. 005 RW. 004 Tambakrejo, Kraton, Pasuruan, Jawa Timur', '085815140001', '', '', 1, '2019-07-16 03:41:53', '2019-07-16 03:41:53', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `accounting` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax` tinyint(4) NOT NULL,
  `npwp_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `npwp_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `npwp_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `npwp_city` int(11) DEFAULT NULL,
  `limit` bigint(20) NOT NULL,
  `piutang_idr` bigint(20) NOT NULL DEFAULT '0',
  `piutang_usd` bigint(20) NOT NULL DEFAULT '0',
  `note1` text COLLATE utf8mb4_unicode_ci,
  `note2` text COLLATE utf8mb4_unicode_ci,
  `note3` text COLLATE utf8mb4_unicode_ci,
  `notice` text COLLATE utf8mb4_unicode_ci,
  `status_group` tinyint(4) NOT NULL,
  `status_record` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `accounting`, `no_inc`, `code`, `fax`, `email`, `tax`, `npwp_no`, `npwp_name`, `npwp_address`, `npwp_city`, `limit`, `piutang_idr`, `piutang_usd`, `note1`, `note2`, `note3`, `notice`, `status_group`, `status_record`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, 1, 'S00001', '', 'saa@pt-saa.co.id', 0, '', '', '', 0, 200000000, 0, 0, '', '', '', '', 0, 0, 1, '2019-02-20 09:26:21', '2019-05-16 08:53:43', 1, 2),
(2, 1, 2, 'S00002', NULL, 'sentosa@gmail.com', 0, NULL, NULL, NULL, 0, 15000000, 0, 0, NULL, NULL, NULL, NULL, 1, 1, 1, '2019-02-20 09:26:21', '2019-02-20 09:26:21', 1, 1),
(3, 1, 3, 'S00003', '', 'admin.kpj@pt-saa.co.id', 0, '826744047831000', 'PT Kelapa Palu Jaya', 'Jl. Raya Kayumalue Blok - No.  - RT: 001 RW: 001 Kel. Kayumalue Ngapa Kec. Palu Utara. Kota/Kab. PALU SULAWESI TENGAH.', 0, 1000000000, 0, 0, '', '', '', '', 0, 0, 1, '2019-06-26 11:41:38', '2019-07-16 03:31:32', 2, 2),
(4, 1, 4, 'S00004', '', 'admin.kbj@pt-saa.co.id', 0, '', 'PT Kelapa Batam Jaya', '', 0, 100000000, 0, 0, '', '', '', '', 0, 0, 1, '2019-06-26 11:50:18', '2019-07-16 03:30:38', 2, 2),
(5, 1, 5, 'S00005', '', 'admin.ksj@pt-saa.co.id', 0, '760419804323000', 'Kelapa Sumatera Jaya', 'Jl. Ratu Dibalau Perum TJ Raya Perumahan Blok 16 No. 00', 10, 100000000, 0, 0, '', '', '', '', 0, 0, 1, '2019-07-16 03:31:56', '2019-07-16 03:35:06', 2, 2),
(6, 1, 6, 'S00006', '', 'admin.process@pt-saa.co.id', 0, '', '', '', 0, 100000000, 0, 0, '', '', '', '', 0, 0, 1, '2019-07-16 03:35:56', '2019-07-16 03:35:56', 2, 2),
(7, 1, 7, 'S00007', '', 'listaniwinoto@gmail.com', 0, '849093935624000', 'PT Aneka Kelapa Nusantara', 'Jl. Kabupaten RT. 005 RW. 004 Tambakrejo, Kraton, Pasuruan, Jawa Timur', 0, 100000000, 0, 0, '', '', '', '', 0, 0, 1, '2019-07-16 03:41:24', '2019-07-16 03:42:02', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `department` int(10) UNSIGNED NOT NULL,
  `accounting` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_group` tinyint(4) NOT NULL,
  `status_record` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `department`, `accounting`, `code`, `name`, `status_group`, `status_record`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 4, 1, 'PURCHASE', 'Purchase Order', 1, 1, 1, '2019-04-26 14:56:24', '2019-04-26 14:56:24', 2, 2),
(2, 5, 1, 'INVENTORY', 'Assembly / Production', 1, 1, 1, '2019-04-26 14:56:46', '2019-04-26 14:56:46', 2, 2),
(3, 2, 1, 'DELIVERY', 'Delivery Order', 1, 1, 1, '2019-04-26 14:57:09', '2019-04-26 14:57:09', 2, 2),
(4, 3, 1, 'OTHER', 'Other Transaction', 0, 0, 1, '2019-04-26 14:57:31', '2019-04-26 14:57:31', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `name`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, '-', 1, '2019-02-20 09:18:16', '2019-02-20 09:18:16', 1, 1),
(2, 'Type', 1, '2019-02-20 09:18:26', '2019-02-20 09:18:26', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `typesubs`
--

CREATE TABLE `typesubs` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `typesubs`
--

INSERT INTO `typesubs` (`id`, `type`, `name`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 1, '-', 1, '2019-02-20 09:18:36', '2019-02-20 09:18:36', 1, 1),
(2, 2, 'Sub Type', 1, '2019-02-20 09:18:47', '2019-02-20 09:18:47', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 'Tons', 1, '2019-02-20 09:17:57', '2019-07-16 03:59:26', 1, 2),
(2, 'Unit', 1, '2019-02-20 09:18:05', '2019-02-20 09:18:05', 1, 1),
(3, 'kg', 1, '2019-07-15 10:22:59', '2019-07-15 10:22:59', 2, 2),
(4, 'MT', 1, '2019-07-15 10:23:05', '2019-07-15 10:23:05', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'PT. Starindo Anugerah Abadi', 'saa@pt-saa.co.id', NULL, '$2y$10$YJLT2jS.eN5bGMY4sOYAkO5Dm0AK.P7QnEv5aqIhC3ByQofAqNe2i', 'Zbiu6kSC1bz97UegYFutHtxzjtdOTpeqf5YRBW81op9Am0ysBjZQh3CpnDqd', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `variants`
--

CREATE TABLE `variants` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `variants`
--

INSERT INTO `variants` (`id`, `name`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, '-', 1, '2019-02-20 09:17:02', '2019-02-20 09:17:02', 1, 1),
(2, 'Variant', 1, '2019-02-20 09:17:11', '2019-02-20 09:17:11', 1, 1),
(3, 'Frozen Coconut Chunk', 0, '2019-07-15 10:18:03', '2019-07-16 03:09:36', 2, 2),
(4, 'Frozen Shredded Coconut', 0, '2019-07-15 10:18:50', '2019-07-16 03:09:18', 2, 2),
(5, 'Frozen Cassava', 0, '2019-07-15 10:18:57', '2019-07-16 03:09:14', 2, 2),
(6, 'Frozen Coconut Dice', 0, '2019-07-15 10:19:03', '2019-07-16 03:09:12', 2, 2),
(7, 'Frozen Organic Coconut Chunk', 0, '2019-07-15 10:19:12', '2019-07-16 03:09:08', 2, 2),
(8, 'Coconut Carbon', 0, '2019-07-15 10:19:19', '2019-07-16 03:09:41', 2, 2),
(9, 'Organic', 0, '2019-07-16 03:08:48', '2019-07-16 03:12:52', 2, 2),
(10, 'Standard', 0, '2019-07-16 03:08:53', '2019-07-16 03:12:49', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `warehouses`
--

CREATE TABLE `warehouses` (
  `id` int(10) UNSIGNED NOT NULL,
  `city` int(10) UNSIGNED NOT NULL,
  `no_inc` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `updated_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `warehouses`
--

INSERT INTO `warehouses` (`id`, `city`, `no_inc`, `code`, `name`, `address`, `status`, `created_at`, `updated_at`, `created_user`, `updated_user`) VALUES
(1, 3, 1, 'G00001', 'Gudang Induk', 'Pasuruan, Jawa Timur', 1, '2019-02-20 09:12:30', '2019-05-16 08:44:24', 1, 2),
(2, 3, 2, 'G00002', 'Pasuruan Gudang Pertama', 'Pasuruan, Jawa Timur', 1, '2019-02-20 09:12:43', '2019-05-16 08:44:39', 1, 2),
(3, 3, 3, 'G00003', 'Pasuruan Gudang Kedua', 'Pasuruan, Jawa Timur', 1, '2019-02-20 09:12:59', '2019-05-16 08:44:52', 1, 2),
(4, 8, 4, 'G00004', 'Gudang Palu', 'Palu Dong', 0, '2019-07-16 02:33:25', '2019-07-16 02:34:00', 2, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accountgroups`
--
ALTER TABLE `accountgroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `accountgroups_code_unique` (`code`);

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `accounts_code_unique` (`code`),
  ADD KEY `accounts_currency_foreign` (`currency`),
  ADD KEY `accounts_group_foreign` (`group`),
  ADD KEY `accounts_type_foreign` (`type`);

--
-- Indexes for table `accounttypes`
--
ALTER TABLE `accounttypes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `accounttypes_code_unique` (`code`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `banks_name_unique` (`name`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `branches_name_unique` (`name`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cities_name_unique` (`name`),
  ADD KEY `cities_country_foreign` (`country`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `companies_name_unique` (`name`),
  ADD KEY `companies_city_foreign` (`city`);

--
-- Indexes for table `companyaccounts`
--
ALTER TABLE `companyaccounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `companyaccounts_company_foreign` (`company`),
  ADD KEY `companyaccounts_currency_foreign` (`currency`),
  ADD KEY `companyaccounts_bank_foreign` (`bank`),
  ADD KEY `companyaccounts_city_foreign` (`city`);

--
-- Indexes for table `containers`
--
ALTER TABLE `containers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `countries_name_unique` (`name`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `currencies_code_unique` (`code`);

--
-- Indexes for table `customeraccounts`
--
ALTER TABLE `customeraccounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customeraccounts_customer_foreign` (`customer`),
  ADD KEY `customeraccounts_currency_foreign` (`currency`),
  ADD KEY `customeraccounts_bank_foreign` (`bank`);

--
-- Indexes for table `customerdetails`
--
ALTER TABLE `customerdetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customerdetails_customer_foreign` (`customer`),
  ADD KEY `customerdetails_city_foreign` (`city`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customers_accounting_foreign` (`accounting`);

--
-- Indexes for table `debtcards`
--
ALTER TABLE `debtcards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `debtcards_supplierdetail_foreign` (`supplierdetail`);

--
-- Indexes for table `debts`
--
ALTER TABLE `debts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `departments_name_unique` (`name`);

--
-- Indexes for table `documentchecks`
--
ALTER TABLE `documentchecks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documentchecks_document_foreign` (`document`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `documents_name_unique` (`name`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_email_unique` (`email`),
  ADD KEY `employees_group_foreign` (`group`),
  ADD KEY `employees_branch_foreign` (`branch`),
  ADD KEY `employees_department_foreign` (`department`),
  ADD KEY `employees_position_foreign` (`position`);

--
-- Indexes for table `employeesets`
--
ALTER TABLE `employeesets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employeesets_employee_foreign` (`employee`),
  ADD KEY `employeesets_menuroot_foreign` (`menuroot`);

--
-- Indexes for table `employeewarehouses`
--
ALTER TABLE `employeewarehouses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employeewarehouses_employee_foreign` (`employee`),
  ADD KEY `employeewarehouses_warehouse_foreign` (`warehouse`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `groups_name_unique` (`name`);

--
-- Indexes for table `groupsets`
--
ALTER TABLE `groupsets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `groupsets_group_foreign` (`group`),
  ADD KEY `groupsets_menuroot_foreign` (`menuroot`);

--
-- Indexes for table `journaldetails`
--
ALTER TABLE `journaldetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `journaldetails_journal_foreign` (`journal`),
  ADD KEY `journaldetails_accounting_foreign` (`accounting`);

--
-- Indexes for table `journals`
--
ALTER TABLE `journals`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `journals_code_unique` (`code`);

--
-- Indexes for table `journalsets`
--
ALTER TABLE `journalsets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kurs`
--
ALTER TABLE `kurs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kurs_currency_foreign` (`currency`);

--
-- Indexes for table `letters`
--
ALTER TABLE `letters`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `letters_code_unique` (`code`);

--
-- Indexes for table `mailcontacts`
--
ALTER TABLE `mailcontacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `materialdetails`
--
ALTER TABLE `materialdetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `materialdetails_material_foreign` (`material`),
  ADD KEY `materialdetails_product_foreign` (`product`),
  ADD KEY `materialdetails_unit_foreign` (`unit`);

--
-- Indexes for table `materialothers`
--
ALTER TABLE `materialothers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `materialothers_material_foreign` (`material`),
  ADD KEY `materialothers_product_foreign` (`product`),
  ADD KEY `materialothers_unit_foreign` (`unit`);

--
-- Indexes for table `materials`
--
ALTER TABLE `materials`
  ADD PRIMARY KEY (`id`),
  ADD KEY `materials_product_foreign` (`product`);

--
-- Indexes for table `menuroots`
--
ALTER TABLE `menuroots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menus_menuroot_foreign` (`menuroot`);

--
-- Indexes for table `menusubs`
--
ALTER TABLE `menusubs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menusubs_menu_foreign` (`menu`);

--
-- Indexes for table `merks`
--
ALTER TABLE `merks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `merks_name_unique` (`name`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orderdetails_order_foreign` (`order`),
  ADD KEY `orderdetails_product_foreign` (`product`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paymentterms`
--
ALTER TABLE `paymentterms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `paymentterms_code_unique` (`code`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `plans_quotationdetail_foreign` (`quotationdetail`),
  ADD KEY `plans_stockin_foreign` (`stockin`);

--
-- Indexes for table `ports`
--
ALTER TABLE `ports`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ports_name_unique` (`name`),
  ADD KEY `ports_country_foreign` (`country`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `positions_name_unique` (`name`);

--
-- Indexes for table `pricelists`
--
ALTER TABLE `pricelists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pricelists_product_foreign` (`product`),
  ADD KEY `pricelists_currency_foreign` (`currency`);

--
-- Indexes for table `productaliases`
--
ALTER TABLE `productaliases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productaliases_product_foreign` (`product`),
  ADD KEY `productaliases_customerdetail_foreign` (`customerdetail`);

--
-- Indexes for table `productiondetails`
--
ALTER TABLE `productiondetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productiondetails_production_foreign` (`production`);

--
-- Indexes for table `productionothers`
--
ALTER TABLE `productionothers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productionothers_production_foreign` (`production`);

--
-- Indexes for table `productions`
--
ALTER TABLE `productions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productions_material_foreign` (`material`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_code_unique` (`code`),
  ADD KEY `products_accounting_foreign` (`accounting`),
  ADD KEY `products_department_foreign` (`department`),
  ADD KEY `products_unit_foreign` (`unit`);

--
-- Indexes for table `purchasedetailprgs`
--
ALTER TABLE `purchasedetailprgs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchasedetailprgs_purchasegeneral_foreign` (`purchasegeneral`),
  ADD KEY `purchasedetailprgs_product_foreign` (`product`);

--
-- Indexes for table `purchasedetailprs`
--
ALTER TABLE `purchasedetailprs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchasedetailprs_purchasedetail_foreign` (`purchasedetail`);

--
-- Indexes for table `purchasedetails`
--
ALTER TABLE `purchasedetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchasedetails_purchaseorder_foreign` (`purchaseorder`),
  ADD KEY `purchasedetails_product_foreign` (`product`),
  ADD KEY `purchasedetails_supplierdetail_foreign` (`supplierdetail`),
  ADD KEY `warehouse` (`warehouse`);

--
-- Indexes for table `purchasegenerals`
--
ALTER TABLE `purchasegenerals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchaseorders`
--
ALTER TABLE `purchaseorders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchaserequests`
--
ALTER TABLE `purchaserequests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchaserequests_product_foreign` (`product`);

--
-- Indexes for table `quotationcharges`
--
ALTER TABLE `quotationcharges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quotationcharges_quotation_foreign` (`quotation`);

--
-- Indexes for table `quotationdetails`
--
ALTER TABLE `quotationdetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quotationdetails_quotation_foreign` (`quotation`),
  ADD KEY `quotationdetails_product_foreign` (`product`),
  ADD KEY `quotationdetails_unit_foreign` (`unit`);

--
-- Indexes for table `quotationpayments`
--
ALTER TABLE `quotationpayments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quotationpayments_quotation_foreign` (`quotation`),
  ADD KEY `quotationpayments_payment_foreign` (`payment`);

--
-- Indexes for table `quotationplans`
--
ALTER TABLE `quotationplans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quotationplans_quotationsplit_foreign` (`quotationsplit`),
  ADD KEY `quotationplans_plan_foreign` (`plan`);

--
-- Indexes for table `quotations`
--
ALTER TABLE `quotations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quotations_customerdetail_foreign` (`customerdetail`),
  ADD KEY `quotations_companyaccount_foreign` (`companyaccount`),
  ADD KEY `quotations_container_foreign` (`container`),
  ADD KEY `quotations_pol_foreign` (`pol`),
  ADD KEY `quotations_pod_foreign` (`pod`),
  ADD KEY `paymentterm` (`paymentterm`),
  ADD KEY `currency` (`currency`),
  ADD KEY `sales` (`sales`);

--
-- Indexes for table `quotationsplits`
--
ALTER TABLE `quotationsplits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quotationsplits_quotation_foreign` (`quotation`),
  ADD KEY `quotationsplits_pol_foreign` (`pol`),
  ADD KEY `quotationsplits_pod_foreign` (`pod`);

--
-- Indexes for table `receivablecards`
--
ALTER TABLE `receivablecards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `receivablecards_customerdetail_foreign` (`customerdetail`);

--
-- Indexes for table `receivables`
--
ALTER TABLE `receivables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sales_code_unique` (`code`),
  ADD KEY `sales_accounting_foreign` (`accounting`),
  ADD KEY `sales_city_foreign` (`city`);

--
-- Indexes for table `schedulebooks`
--
ALTER TABLE `schedulebooks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `schedulebooks_quotationsplit_foreign` (`quotationsplit`);

--
-- Indexes for table `scheduleships`
--
ALTER TABLE `scheduleships`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedulestuffs`
--
ALTER TABLE `schedulestuffs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `schedulestuffs_schedulebook_foreign` (`schedulebook`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sizes_name_unique` (`name`);

--
-- Indexes for table `stockcards`
--
ALTER TABLE `stockcards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stockcards_product_foreign` (`product`);

--
-- Indexes for table `stockdetails`
--
ALTER TABLE `stockdetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stockdetails_stockqc_foreign` (`stockqc`);

--
-- Indexes for table `stockins`
--
ALTER TABLE `stockins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stockins_transaction_foreign` (`transaction`),
  ADD KEY `stockins_product_foreign` (`product`),
  ADD KEY `stockins_warehouse_foreign` (`warehouse`),
  ADD KEY `stockins_supplierdetail_foreign` (`supplierdetail`);

--
-- Indexes for table `stockloads`
--
ALTER TABLE `stockloads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stockloads_stuff_foreign` (`stuff`),
  ADD KEY `stockloads_stockin_foreign` (`stockin`);

--
-- Indexes for table `stockopnames`
--
ALTER TABLE `stockopnames`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stockopnames_stockin_foreign` (`stockin`);

--
-- Indexes for table `stockqcs`
--
ALTER TABLE `stockqcs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stockqcs_purchasedetail_foreign` (`purchasedetail`),
  ADD KEY `stockqcs_product_foreign` (`product`),
  ADD KEY `stockqcs_warehouse_foreign` (`warehouse`),
  ADD KEY `stockqcs_supplierdetail_foreign` (`supplierdetail`);

--
-- Indexes for table `stockrequests`
--
ALTER TABLE `stockrequests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stockrequests_schedulestuff_foreign` (`schedulestuff`),
  ADD KEY `stockrequests_pol_foreign` (`pol`),
  ADD KEY `stockrequests_pod_foreign` (`pod`);

--
-- Indexes for table `stufforders`
--
ALTER TABLE `stufforders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stuffreschedules`
--
ALTER TABLE `stuffreschedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stuffreschedules_schedulestuff_foreign` (`schedulestuff`);

--
-- Indexes for table `stuffs`
--
ALTER TABLE `stuffs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stuffs_pol_foreign` (`pol`),
  ADD KEY `stuffs_pod_foreign` (`pod`),
  ADD KEY `container` (`container`);

--
-- Indexes for table `supplieraccounts`
--
ALTER TABLE `supplieraccounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supplieraccounts_supplier_foreign` (`supplier`),
  ADD KEY `supplieraccounts_currency_foreign` (`currency`),
  ADD KEY `supplieraccounts_bank_foreign` (`bank`);

--
-- Indexes for table `supplierdetails`
--
ALTER TABLE `supplierdetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supplierdetails_supplier_foreign` (`supplier`),
  ADD KEY `supplierdetails_city_foreign` (`city`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `suppliers_accounting_foreign` (`accounting`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `transactions_code_unique` (`code`),
  ADD KEY `transactions_department_foreign` (`department`),
  ADD KEY `transactions_accounting_foreign` (`accounting`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `types_name_unique` (`name`);

--
-- Indexes for table `typesubs`
--
ALTER TABLE `typesubs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `typesubs_name_unique` (`name`),
  ADD KEY `typesubs_type_foreign` (`type`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `units_name_unique` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `variants`
--
ALTER TABLE `variants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `variants_name_unique` (`name`);

--
-- Indexes for table `warehouses`
--
ALTER TABLE `warehouses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `warehouses_name_unique` (`name`),
  ADD KEY `warehouses_city_foreign` (`city`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accountgroups`
--
ALTER TABLE `accountgroups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `accounttypes`
--
ALTER TABLE `accounttypes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `companyaccounts`
--
ALTER TABLE `companyaccounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `containers`
--
ALTER TABLE `containers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customeraccounts`
--
ALTER TABLE `customeraccounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customerdetails`
--
ALTER TABLE `customerdetails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `debtcards`
--
ALTER TABLE `debtcards`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `debts`
--
ALTER TABLE `debts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `documentchecks`
--
ALTER TABLE `documentchecks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employeesets`
--
ALTER TABLE `employeesets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=189;

--
-- AUTO_INCREMENT for table `employeewarehouses`
--
ALTER TABLE `employeewarehouses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `groupsets`
--
ALTER TABLE `groupsets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `journaldetails`
--
ALTER TABLE `journaldetails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `journals`
--
ALTER TABLE `journals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `journalsets`
--
ALTER TABLE `journalsets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kurs`
--
ALTER TABLE `kurs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `letters`
--
ALTER TABLE `letters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `mailcontacts`
--
ALTER TABLE `mailcontacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `materialdetails`
--
ALTER TABLE `materialdetails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `materialothers`
--
ALTER TABLE `materialothers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `materials`
--
ALTER TABLE `materials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menuroots`
--
ALTER TABLE `menuroots`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `menusubs`
--
ALTER TABLE `menusubs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `merks`
--
ALTER TABLE `merks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `orderdetails`
--
ALTER TABLE `orderdetails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `paymentterms`
--
ALTER TABLE `paymentterms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ports`
--
ALTER TABLE `ports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pricelists`
--
ALTER TABLE `pricelists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `productaliases`
--
ALTER TABLE `productaliases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `productiondetails`
--
ALTER TABLE `productiondetails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `productionothers`
--
ALTER TABLE `productionothers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `productions`
--
ALTER TABLE `productions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `purchasedetailprgs`
--
ALTER TABLE `purchasedetailprgs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchasedetailprs`
--
ALTER TABLE `purchasedetailprs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `purchasedetails`
--
ALTER TABLE `purchasedetails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `purchasegenerals`
--
ALTER TABLE `purchasegenerals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchaseorders`
--
ALTER TABLE `purchaseorders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `purchaserequests`
--
ALTER TABLE `purchaserequests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `quotationcharges`
--
ALTER TABLE `quotationcharges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quotationdetails`
--
ALTER TABLE `quotationdetails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `quotationpayments`
--
ALTER TABLE `quotationpayments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `quotationplans`
--
ALTER TABLE `quotationplans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `quotations`
--
ALTER TABLE `quotations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `quotationsplits`
--
ALTER TABLE `quotationsplits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `receivablecards`
--
ALTER TABLE `receivablecards`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `receivables`
--
ALTER TABLE `receivables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `schedulebooks`
--
ALTER TABLE `schedulebooks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `scheduleships`
--
ALTER TABLE `scheduleships`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `schedulestuffs`
--
ALTER TABLE `schedulestuffs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stockcards`
--
ALTER TABLE `stockcards`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `stockdetails`
--
ALTER TABLE `stockdetails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stockins`
--
ALTER TABLE `stockins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `stockloads`
--
ALTER TABLE `stockloads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stockopnames`
--
ALTER TABLE `stockopnames`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stockqcs`
--
ALTER TABLE `stockqcs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `stockrequests`
--
ALTER TABLE `stockrequests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `stufforders`
--
ALTER TABLE `stufforders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stuffreschedules`
--
ALTER TABLE `stuffreschedules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stuffs`
--
ALTER TABLE `stuffs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `supplieraccounts`
--
ALTER TABLE `supplieraccounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `supplierdetails`
--
ALTER TABLE `supplierdetails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `typesubs`
--
ALTER TABLE `typesubs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `variants`
--
ALTER TABLE `variants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `warehouses`
--
ALTER TABLE `warehouses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `accounts_currency_foreign` FOREIGN KEY (`currency`) REFERENCES `currencies` (`id`),
  ADD CONSTRAINT `accounts_group_foreign` FOREIGN KEY (`group`) REFERENCES `accountgroups` (`id`),
  ADD CONSTRAINT `accounts_type_foreign` FOREIGN KEY (`type`) REFERENCES `accounttypes` (`id`);

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_country_foreign` FOREIGN KEY (`country`) REFERENCES `countries` (`id`);

--
-- Constraints for table `companies`
--
ALTER TABLE `companies`
  ADD CONSTRAINT `companies_city_foreign` FOREIGN KEY (`city`) REFERENCES `cities` (`id`);

--
-- Constraints for table `companyaccounts`
--
ALTER TABLE `companyaccounts`
  ADD CONSTRAINT `companyaccounts_bank_foreign` FOREIGN KEY (`bank`) REFERENCES `banks` (`id`),
  ADD CONSTRAINT `companyaccounts_city_foreign` FOREIGN KEY (`city`) REFERENCES `cities` (`id`),
  ADD CONSTRAINT `companyaccounts_company_foreign` FOREIGN KEY (`company`) REFERENCES `companies` (`id`),
  ADD CONSTRAINT `companyaccounts_currency_foreign` FOREIGN KEY (`currency`) REFERENCES `currencies` (`id`);

--
-- Constraints for table `customeraccounts`
--
ALTER TABLE `customeraccounts`
  ADD CONSTRAINT `customeraccounts_bank_foreign` FOREIGN KEY (`bank`) REFERENCES `banks` (`id`),
  ADD CONSTRAINT `customeraccounts_currency_foreign` FOREIGN KEY (`currency`) REFERENCES `currencies` (`id`),
  ADD CONSTRAINT `customeraccounts_customer_foreign` FOREIGN KEY (`customer`) REFERENCES `customers` (`id`);

--
-- Constraints for table `customerdetails`
--
ALTER TABLE `customerdetails`
  ADD CONSTRAINT `customerdetails_city_foreign` FOREIGN KEY (`city`) REFERENCES `cities` (`id`),
  ADD CONSTRAINT `customerdetails_customer_foreign` FOREIGN KEY (`customer`) REFERENCES `customers` (`id`);

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_accounting_foreign` FOREIGN KEY (`accounting`) REFERENCES `accounts` (`id`);

--
-- Constraints for table `debtcards`
--
ALTER TABLE `debtcards`
  ADD CONSTRAINT `debtcards_supplierdetail_foreign` FOREIGN KEY (`supplierdetail`) REFERENCES `supplierdetails` (`id`);

--
-- Constraints for table `documentchecks`
--
ALTER TABLE `documentchecks`
  ADD CONSTRAINT `documentchecks_document_foreign` FOREIGN KEY (`document`) REFERENCES `documents` (`id`);

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_branch_foreign` FOREIGN KEY (`branch`) REFERENCES `branches` (`id`),
  ADD CONSTRAINT `employees_department_foreign` FOREIGN KEY (`department`) REFERENCES `departments` (`id`),
  ADD CONSTRAINT `employees_group_foreign` FOREIGN KEY (`group`) REFERENCES `groups` (`id`),
  ADD CONSTRAINT `employees_position_foreign` FOREIGN KEY (`position`) REFERENCES `positions` (`id`);

--
-- Constraints for table `employeesets`
--
ALTER TABLE `employeesets`
  ADD CONSTRAINT `employeesets_employee_foreign` FOREIGN KEY (`employee`) REFERENCES `employees` (`id`),
  ADD CONSTRAINT `employeesets_menuroot_foreign` FOREIGN KEY (`menuroot`) REFERENCES `menuroots` (`id`);

--
-- Constraints for table `employeewarehouses`
--
ALTER TABLE `employeewarehouses`
  ADD CONSTRAINT `employeewarehouses_employee_foreign` FOREIGN KEY (`employee`) REFERENCES `employees` (`id`),
  ADD CONSTRAINT `employeewarehouses_warehouse_foreign` FOREIGN KEY (`warehouse`) REFERENCES `warehouses` (`id`);

--
-- Constraints for table `groupsets`
--
ALTER TABLE `groupsets`
  ADD CONSTRAINT `groupsets_group_foreign` FOREIGN KEY (`group`) REFERENCES `groups` (`id`),
  ADD CONSTRAINT `groupsets_menuroot_foreign` FOREIGN KEY (`menuroot`) REFERENCES `menuroots` (`id`);

--
-- Constraints for table `journaldetails`
--
ALTER TABLE `journaldetails`
  ADD CONSTRAINT `journaldetails_accounting_foreign` FOREIGN KEY (`accounting`) REFERENCES `accounts` (`id`),
  ADD CONSTRAINT `journaldetails_journal_foreign` FOREIGN KEY (`journal`) REFERENCES `journals` (`id`);

--
-- Constraints for table `kurs`
--
ALTER TABLE `kurs`
  ADD CONSTRAINT `kurs_currency_foreign` FOREIGN KEY (`currency`) REFERENCES `currencies` (`id`);

--
-- Constraints for table `materialdetails`
--
ALTER TABLE `materialdetails`
  ADD CONSTRAINT `materialdetails_material_foreign` FOREIGN KEY (`material`) REFERENCES `materials` (`id`),
  ADD CONSTRAINT `materialdetails_product_foreign` FOREIGN KEY (`product`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `materialdetails_unit_foreign` FOREIGN KEY (`unit`) REFERENCES `units` (`id`);

--
-- Constraints for table `materialothers`
--
ALTER TABLE `materialothers`
  ADD CONSTRAINT `materialothers_material_foreign` FOREIGN KEY (`material`) REFERENCES `materials` (`id`),
  ADD CONSTRAINT `materialothers_product_foreign` FOREIGN KEY (`product`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `materialothers_unit_foreign` FOREIGN KEY (`unit`) REFERENCES `units` (`id`);

--
-- Constraints for table `materials`
--
ALTER TABLE `materials`
  ADD CONSTRAINT `materials_product_foreign` FOREIGN KEY (`product`) REFERENCES `products` (`id`);

--
-- Constraints for table `menus`
--
ALTER TABLE `menus`
  ADD CONSTRAINT `menus_menuroot_foreign` FOREIGN KEY (`menuroot`) REFERENCES `menuroots` (`id`);

--
-- Constraints for table `menusubs`
--
ALTER TABLE `menusubs`
  ADD CONSTRAINT `menusubs_menu_foreign` FOREIGN KEY (`menu`) REFERENCES `menus` (`id`);

--
-- Constraints for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD CONSTRAINT `orderdetails_order_foreign` FOREIGN KEY (`order`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `orderdetails_product_foreign` FOREIGN KEY (`product`) REFERENCES `products` (`id`);

--
-- Constraints for table `plans`
--
ALTER TABLE `plans`
  ADD CONSTRAINT `plans_quotationdetail_foreign` FOREIGN KEY (`quotationdetail`) REFERENCES `quotationdetails` (`id`),
  ADD CONSTRAINT `plans_stockin_foreign` FOREIGN KEY (`stockin`) REFERENCES `stockins` (`id`);

--
-- Constraints for table `ports`
--
ALTER TABLE `ports`
  ADD CONSTRAINT `ports_country_foreign` FOREIGN KEY (`country`) REFERENCES `countries` (`id`);

--
-- Constraints for table `pricelists`
--
ALTER TABLE `pricelists`
  ADD CONSTRAINT `pricelists_currency_foreign` FOREIGN KEY (`currency`) REFERENCES `currencies` (`id`),
  ADD CONSTRAINT `pricelists_product_foreign` FOREIGN KEY (`product`) REFERENCES `products` (`id`);

--
-- Constraints for table `productaliases`
--
ALTER TABLE `productaliases`
  ADD CONSTRAINT `productaliases_customerdetail_foreign` FOREIGN KEY (`customerdetail`) REFERENCES `customerdetails` (`id`),
  ADD CONSTRAINT `productaliases_product_foreign` FOREIGN KEY (`product`) REFERENCES `products` (`id`);

--
-- Constraints for table `productiondetails`
--
ALTER TABLE `productiondetails`
  ADD CONSTRAINT `productiondetails_production_foreign` FOREIGN KEY (`production`) REFERENCES `productions` (`id`);

--
-- Constraints for table `productionothers`
--
ALTER TABLE `productionothers`
  ADD CONSTRAINT `productionothers_production_foreign` FOREIGN KEY (`production`) REFERENCES `productions` (`id`);

--
-- Constraints for table `productions`
--
ALTER TABLE `productions`
  ADD CONSTRAINT `productions_material_foreign` FOREIGN KEY (`material`) REFERENCES `materials` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_accounting_foreign` FOREIGN KEY (`accounting`) REFERENCES `accounts` (`id`),
  ADD CONSTRAINT `products_department_foreign` FOREIGN KEY (`department`) REFERENCES `departments` (`id`),
  ADD CONSTRAINT `products_unit_foreign` FOREIGN KEY (`unit`) REFERENCES `units` (`id`);

--
-- Constraints for table `purchasedetailprgs`
--
ALTER TABLE `purchasedetailprgs`
  ADD CONSTRAINT `purchasedetailprgs_product_foreign` FOREIGN KEY (`product`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `purchasedetailprgs_purchasegeneral_foreign` FOREIGN KEY (`purchasegeneral`) REFERENCES `purchasegenerals` (`id`);

--
-- Constraints for table `purchasedetailprs`
--
ALTER TABLE `purchasedetailprs`
  ADD CONSTRAINT `purchasedetailprs_purchasedetail_foreign` FOREIGN KEY (`purchasedetail`) REFERENCES `purchasedetails` (`id`);

--
-- Constraints for table `purchasedetails`
--
ALTER TABLE `purchasedetails`
  ADD CONSTRAINT `purchasedetails_ibfk_1` FOREIGN KEY (`warehouse`) REFERENCES `warehouses` (`id`),
  ADD CONSTRAINT `purchasedetails_product_foreign` FOREIGN KEY (`product`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `purchasedetails_purchaseorder_foreign` FOREIGN KEY (`purchaseorder`) REFERENCES `purchaseorders` (`id`),
  ADD CONSTRAINT `purchasedetails_supplierdetail_foreign` FOREIGN KEY (`supplierdetail`) REFERENCES `supplierdetails` (`id`);

--
-- Constraints for table `purchaserequests`
--
ALTER TABLE `purchaserequests`
  ADD CONSTRAINT `purchaserequests_product_foreign` FOREIGN KEY (`product`) REFERENCES `products` (`id`);

--
-- Constraints for table `quotationcharges`
--
ALTER TABLE `quotationcharges`
  ADD CONSTRAINT `quotationcharges_quotation_foreign` FOREIGN KEY (`quotation`) REFERENCES `quotations` (`id`);

--
-- Constraints for table `quotationdetails`
--
ALTER TABLE `quotationdetails`
  ADD CONSTRAINT `quotationdetails_product_foreign` FOREIGN KEY (`product`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `quotationdetails_quotation_foreign` FOREIGN KEY (`quotation`) REFERENCES `quotations` (`id`),
  ADD CONSTRAINT `quotationdetails_unit_foreign` FOREIGN KEY (`unit`) REFERENCES `units` (`id`);

--
-- Constraints for table `quotationpayments`
--
ALTER TABLE `quotationpayments`
  ADD CONSTRAINT `quotationpayments_payment_foreign` FOREIGN KEY (`payment`) REFERENCES `payments` (`id`),
  ADD CONSTRAINT `quotationpayments_quotation_foreign` FOREIGN KEY (`quotation`) REFERENCES `quotations` (`id`);

--
-- Constraints for table `quotationplans`
--
ALTER TABLE `quotationplans`
  ADD CONSTRAINT `quotationplans_plan_foreign` FOREIGN KEY (`plan`) REFERENCES `plans` (`id`),
  ADD CONSTRAINT `quotationplans_quotationsplit_foreign` FOREIGN KEY (`quotationsplit`) REFERENCES `quotationsplits` (`id`);

--
-- Constraints for table `quotations`
--
ALTER TABLE `quotations`
  ADD CONSTRAINT `quotations_companyaccount_foreign` FOREIGN KEY (`companyaccount`) REFERENCES `companyaccounts` (`id`),
  ADD CONSTRAINT `quotations_container_foreign` FOREIGN KEY (`container`) REFERENCES `containers` (`id`),
  ADD CONSTRAINT `quotations_customerdetail_foreign` FOREIGN KEY (`customerdetail`) REFERENCES `customerdetails` (`id`),
  ADD CONSTRAINT `quotations_ibfk_1` FOREIGN KEY (`paymentterm`) REFERENCES `paymentterms` (`id`),
  ADD CONSTRAINT `quotations_ibfk_2` FOREIGN KEY (`currency`) REFERENCES `currencies` (`id`),
  ADD CONSTRAINT `quotations_ibfk_3` FOREIGN KEY (`sales`) REFERENCES `sales` (`id`),
  ADD CONSTRAINT `quotations_pod_foreign` FOREIGN KEY (`pod`) REFERENCES `ports` (`id`),
  ADD CONSTRAINT `quotations_pol_foreign` FOREIGN KEY (`pol`) REFERENCES `ports` (`id`);

--
-- Constraints for table `quotationsplits`
--
ALTER TABLE `quotationsplits`
  ADD CONSTRAINT `quotationsplits_pod_foreign` FOREIGN KEY (`pod`) REFERENCES `ports` (`id`),
  ADD CONSTRAINT `quotationsplits_pol_foreign` FOREIGN KEY (`pol`) REFERENCES `ports` (`id`),
  ADD CONSTRAINT `quotationsplits_quotation_foreign` FOREIGN KEY (`quotation`) REFERENCES `quotations` (`id`);

--
-- Constraints for table `receivablecards`
--
ALTER TABLE `receivablecards`
  ADD CONSTRAINT `receivablecards_customerdetail_foreign` FOREIGN KEY (`customerdetail`) REFERENCES `customerdetails` (`id`);

--
-- Constraints for table `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_accounting_foreign` FOREIGN KEY (`accounting`) REFERENCES `accounts` (`id`),
  ADD CONSTRAINT `sales_city_foreign` FOREIGN KEY (`city`) REFERENCES `cities` (`id`);

--
-- Constraints for table `schedulebooks`
--
ALTER TABLE `schedulebooks`
  ADD CONSTRAINT `schedulebooks_quotationsplit_foreign` FOREIGN KEY (`quotationsplit`) REFERENCES `quotationsplits` (`id`);

--
-- Constraints for table `schedulestuffs`
--
ALTER TABLE `schedulestuffs`
  ADD CONSTRAINT `schedulestuffs_schedulebook_foreign` FOREIGN KEY (`schedulebook`) REFERENCES `schedulebooks` (`id`);

--
-- Constraints for table `stockcards`
--
ALTER TABLE `stockcards`
  ADD CONSTRAINT `stockcards_product_foreign` FOREIGN KEY (`product`) REFERENCES `products` (`id`);

--
-- Constraints for table `stockdetails`
--
ALTER TABLE `stockdetails`
  ADD CONSTRAINT `stockdetails_stockqc_foreign` FOREIGN KEY (`stockqc`) REFERENCES `stockqcs` (`id`);

--
-- Constraints for table `stockins`
--
ALTER TABLE `stockins`
  ADD CONSTRAINT `stockins_product_foreign` FOREIGN KEY (`product`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `stockins_supplierdetail_foreign` FOREIGN KEY (`supplierdetail`) REFERENCES `supplierdetails` (`id`),
  ADD CONSTRAINT `stockins_transaction_foreign` FOREIGN KEY (`transaction`) REFERENCES `transactions` (`id`),
  ADD CONSTRAINT `stockins_warehouse_foreign` FOREIGN KEY (`warehouse`) REFERENCES `warehouses` (`id`);

--
-- Constraints for table `stockloads`
--
ALTER TABLE `stockloads`
  ADD CONSTRAINT `stockloads_stockin_foreign` FOREIGN KEY (`stockin`) REFERENCES `stockins` (`id`),
  ADD CONSTRAINT `stockloads_stuff_foreign` FOREIGN KEY (`stuff`) REFERENCES `stuffs` (`id`);

--
-- Constraints for table `stockopnames`
--
ALTER TABLE `stockopnames`
  ADD CONSTRAINT `stockopnames_stockin_foreign` FOREIGN KEY (`stockin`) REFERENCES `stockins` (`id`);

--
-- Constraints for table `stockqcs`
--
ALTER TABLE `stockqcs`
  ADD CONSTRAINT `stockqcs_product_foreign` FOREIGN KEY (`product`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `stockqcs_purchasedetail_foreign` FOREIGN KEY (`purchasedetail`) REFERENCES `purchasedetails` (`id`),
  ADD CONSTRAINT `stockqcs_supplierdetail_foreign` FOREIGN KEY (`supplierdetail`) REFERENCES `supplierdetails` (`id`),
  ADD CONSTRAINT `stockqcs_warehouse_foreign` FOREIGN KEY (`warehouse`) REFERENCES `warehouses` (`id`);

--
-- Constraints for table `stockrequests`
--
ALTER TABLE `stockrequests`
  ADD CONSTRAINT `stockrequests_pod_foreign` FOREIGN KEY (`pod`) REFERENCES `ports` (`id`),
  ADD CONSTRAINT `stockrequests_pol_foreign` FOREIGN KEY (`pol`) REFERENCES `ports` (`id`),
  ADD CONSTRAINT `stockrequests_schedulestuff_foreign` FOREIGN KEY (`schedulestuff`) REFERENCES `schedulestuffs` (`id`);

--
-- Constraints for table `stuffreschedules`
--
ALTER TABLE `stuffreschedules`
  ADD CONSTRAINT `stuffreschedules_schedulestuff_foreign` FOREIGN KEY (`schedulestuff`) REFERENCES `schedulestuffs` (`id`);

--
-- Constraints for table `stuffs`
--
ALTER TABLE `stuffs`
  ADD CONSTRAINT `stuffs_ibfk_1` FOREIGN KEY (`container`) REFERENCES `containers` (`id`),
  ADD CONSTRAINT `stuffs_pod_foreign` FOREIGN KEY (`pod`) REFERENCES `ports` (`id`),
  ADD CONSTRAINT `stuffs_pol_foreign` FOREIGN KEY (`pol`) REFERENCES `ports` (`id`);

--
-- Constraints for table `supplieraccounts`
--
ALTER TABLE `supplieraccounts`
  ADD CONSTRAINT `supplieraccounts_bank_foreign` FOREIGN KEY (`bank`) REFERENCES `banks` (`id`),
  ADD CONSTRAINT `supplieraccounts_currency_foreign` FOREIGN KEY (`currency`) REFERENCES `currencies` (`id`),
  ADD CONSTRAINT `supplieraccounts_supplier_foreign` FOREIGN KEY (`supplier`) REFERENCES `suppliers` (`id`);

--
-- Constraints for table `supplierdetails`
--
ALTER TABLE `supplierdetails`
  ADD CONSTRAINT `supplierdetails_city_foreign` FOREIGN KEY (`city`) REFERENCES `cities` (`id`),
  ADD CONSTRAINT `supplierdetails_supplier_foreign` FOREIGN KEY (`supplier`) REFERENCES `suppliers` (`id`);

--
-- Constraints for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD CONSTRAINT `suppliers_accounting_foreign` FOREIGN KEY (`accounting`) REFERENCES `accounts` (`id`);

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_accounting_foreign` FOREIGN KEY (`accounting`) REFERENCES `accounts` (`id`),
  ADD CONSTRAINT `transactions_department_foreign` FOREIGN KEY (`department`) REFERENCES `departments` (`id`);

--
-- Constraints for table `typesubs`
--
ALTER TABLE `typesubs`
  ADD CONSTRAINT `typesubs_type_foreign` FOREIGN KEY (`type`) REFERENCES `types` (`id`);

--
-- Constraints for table `warehouses`
--
ALTER TABLE `warehouses`
  ADD CONSTRAINT `warehouses_city_foreign` FOREIGN KEY (`city`) REFERENCES `cities` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
