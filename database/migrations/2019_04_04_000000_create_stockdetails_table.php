<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stockdetails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stockqc')->unsigned();
            $table->integer('no_inc');
            $table->string('no_bag');
            $table->integer('qty_bag');
            $table->integer('qty_pcs');
            $table->integer('qty_kg');
            $table->string('barcode')->nullable();
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->foreign('stockqc')->references('id')->on('stockqcs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stockdetails');
    }
}
