@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Receivable Card</a></h4>
  		</div>
	  	
	  	<div class="box-body">
	  		<form method="post" action="{{ url('receivablefilter') }}">
	          	@csrf
	          	<div class="row">
					<div class="col-md-6">
						<div class="form-group">
			              	<label for="customerdetail">Customer</label>
			              	<select class="form-control select2" name="customerdetail" required>
			              		<option value="{{ old('customerdetail') }}">Choose One</option>
		                    	@foreach($customerdetails as $customerdetail)
								    <option value="{{ $customerdetail->id }}">{{ $customerdetail->fkcustomer->code }} - {{ $customerdetail->name }}</option>
								@endforeach
		                	</select>
			          	</div>
			          	<div class="form-group">
			              	<label for="year">Year</label>
			              	<input type="number" class="form-control" name="year" value="{{ old('year') }}" required>
			          	</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
			              	<label for="month">Month</label>
			              	<select class="form-control select2" name="month" required>
			              		<option value="{{ old('month') }}">Choose One</option>
			              		<option value="1">January</option>
			              		<option value="2">February</option>
			              		<option value="3">March</option>
			              		<option value="4">April</option>
			              		<option value="5">May</option>
			              		<option value="6">June</option>
			              		<option value="7">July</option>
			              		<option value="8">August</option>
			              		<option value="9">September</option>
			              		<option value="10">October</option>
			              		<option value="11">November</option>
			              		<option value="12">Desember</option>
		                	</select>
			          	</div>
			          	<div class="form-group">
			          		<label for="button">&nbsp;</label> <br>
			              	<button type="submit" class="btn btn-sm btn-success">Filter</button>
			          	</div>
					</div>
				</div>
	      	</form>
	  	</div>
	<div>
@stop