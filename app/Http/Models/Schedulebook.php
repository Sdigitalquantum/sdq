<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Schedulebook extends Model
{
    protected $fillable = [
	    'quotationsplit',
	    'pol',
	    'pod',
	    'lines',
	    'etd',
	    'eta',
	    'notice',
	    'reason',
	    'created_user',
	    'updated_user'
  	];

  	public function fkQuotationsplit(){
	    return $this->belongsTo('\App\Http\Models\Quotationsplit', 'quotationsplit', 'id');
	}
}
