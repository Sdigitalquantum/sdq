<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Purchaserequest extends Model
{
    protected $fillable = [
	    'product',
	    'quotationdetail',
	    'supplierdetail',
	    'warehouse',
	    'no_inc',
	    'no_pr',
	    'date_request',
	    'qty_request',
	    'qty_remain',
	    'no_reference',
	    'notice',
	    'reason',
	    'approve',
	    'status_approve',
	    'status_plan',
	    'created_user',
	    'updated_user',
	    'approved_at',
	    'approved_user'
  	];

  	public function fkProduct(){
	    return $this->belongsTo('\App\Http\Models\Product', 'product', 'id');
	}

	public function fkQuotationdetail(){
	    return $this->belongsTo('\App\Http\Models\Quotationdetail', 'quotationdetail', 'id');
	}

	public function fkSupplierdetail(){
	    return $this->belongsTo('\App\Http\Models\Supplierdetail', 'supplierdetail', 'id');
	}

	public function fkWarehouse(){
	    return $this->belongsTo('\App\Http\Models\Warehouse', 'warehouse', 'id');
	}
}
