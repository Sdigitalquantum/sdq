<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Stuffreschedule extends Model
{
    protected $fillable = [
	    'schedulestuff',
	    'reason',
	    'date_change',
	    'date_old',
	    'notice',
	    'created_user',
	    'updated_user'
  	];

  	public function fkSchedulestuff(){
	    return $this->belongsTo('\App\Http\Models\Schedulestuff', 'schedulestuff', 'id');
	}
}
