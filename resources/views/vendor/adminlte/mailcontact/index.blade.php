@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Mailbox Contact</h4>
  		</div>
	  	
	  	<div class="box-body">
	  		<div class="row">
	  			<div class="col-md-2">
		          	<div class="box box-solid">
		            	<div class="box-header with-border">
		              		<ul class="nav nav-pills nav-stacked">
		               		 	<li class="active">
		               		 		<a><i class="fa fa-inbox"></i> Inbox
		               		 			@if($inbox != 0)
				                  			<span class="label label-success pull-right">{{ $inbox }}</span>
			                  			@endif
		                  			</a>
		                  		</li>
		              		</ul>
		            	</div>
		          	</div>
		        </div>

		        <div class="col-md-10">
		        	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
					    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
					        <tr>
					          	<th width="5%">No</th>
					          	<th width="15%">Name</th>
					          	<th width="15%">Email</th>
					          	<th width="15%">Phone</th>
					          	<th width="30%">Subject</th>
					          	<th width="20%">Date</th>
					        </tr>
					    </thead>

					    <tbody>
					    	@php ($no = 0)
					    	@foreach($mailcontacts as $mailcontact)
					    		@php ($no++)
					    		@if($mailcontact->status == 1)
				            		<tr>
							            <td>{{$no}}</td>
							            <td><a href="{{ route('mailcontact.show', $mailcontact->id) }}">{{$mailcontact->name}}</a></td>
							            <td><a href="{{ route('mailcontact.show', $mailcontact->id) }}">{{$mailcontact->email}}</a></td>
							            <td style="text-align: right;">{{$mailcontact->phone}} &nbsp;</td>
							            <td><a href="{{ route('mailcontact.show', $mailcontact->id) }}">{{$mailcontact->subject}}</a></td>
							            <td>{{ date('d F Y - H:m:s', strtotime($mailcontact->created_at)) }}</td>
							        </tr>
			            		@else
			            			<tr style="font-weight: bold;">
							            <td>{{$no}}</td>
							            <td><a href="{{ route('mailcontact.show', $mailcontact->id) }}">{{$mailcontact->name}}</a></td>
							            <td><a href="{{ route('mailcontact.show', $mailcontact->id) }}">{{$mailcontact->email}}</a></td>
							            <td>{{$mailcontact->phone}}</td>
							            <td><a href="{{ route('mailcontact.show', $mailcontact->id) }}">{{$mailcontact->subject}}</a></td>
							            <td>{{ date('d F Y - H:m:s', strtotime($mailcontact->created_at)) }}</td>
							        </tr>
			            		@endif
					        @endforeach
					    </tbody>

					    <tfoot>
					        <tr>
					          	<td class="noShow"></td>
					          	<td>Name</td>
					          	<td>Email</td>
					          	<td>Phone</td>
					          	<td>Subject</td>
					          	<td>Date</td>
					        </tr>
					    </tfoot>
				  	</table>
	        	</div>
  			</div>
	  	</div>
	</div>
@stop