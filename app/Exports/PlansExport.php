<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;

class PlansExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $results = DB::table('plans')
            ->join('quotationdetails', 'plans.quotationdetail', '=', 'quotationdetails.id')
            ->join('quotations', 'quotationdetails.quotation', '=', 'quotations.id')
            ->join('stockins', 'plans.stockin', '=', 'stockins.id')
            ->join('products', 'stockins.product', '=', 'products.id')
            ->join('warehouses', 'stockins.warehouse', '=', 'warehouses.id')
            ->join('supplierdetails', 'stockins.supplierdetail', '=', 'supplierdetails.id')
            ->select('plans.no_batch','products.name','supplierdetails.name','warehouses.name','quotations.etd','quotations.eta','plans.qty_plan')
            ->where('plans.status_split', 0)
            ->get();

        return $results;
    }
}
