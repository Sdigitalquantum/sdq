@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>Print</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
                <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Dummy Proforma</h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="15%">Nomor</th>
						          	<th width="10%">Date</th>
						          	<th width="15%">Customer</th>
						          	<th width="15%">Port Loading</th>
						          	<th width="15%">Port Discharge</th>
						          	<th width="10%">Time Departure</th>
						          	<th width="10%">Time Arrival</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($quotations as $quotation)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>{{$quotation->no_sp}}</td>
							            <td>{{$quotation->date_sp}}</td>
							            <td>{{$quotation->fkCustomerdetail->name}}</td>
							            <td>{{$quotation->fkPortl->name}} - {{$quotation->fkPortl->fkCountry->name}}</td>
							            <td>{{$quotation->fkPortd->name}} - {{$quotation->fkPortd->fkCountry->name}}</td>
							            <td>{{$quotation->etd}}</td>
							            <td>{{$quotation->eta}}</td>
										<td style="text-align: center;">
							            	<a href="{{ route('quotation.show',$quotation->id)}}" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-print"></i></a>
							            </td>
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>No. SP</td>
						          	<td>Date</td>
						          	<td>PO Customer</td>
						          	<td>Port Loading</td>
						          	<td>Port Discharge</td>
						          	<td>Time Departure</td>
						          	<td>Time Arrival</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop