@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="active"><a href="#tab_detail" data-toggle="tab">Detail</a></li>
            <li class="disabled"><a>Account</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_detail">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #6b6e70 !important; font-weight: bold; color: white;">
			    		<h4 class="box-title">Edit Customer Detail</h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<div class="row">
				  			<div class="col-md-3">
				  				<form id="customerdetailFrm">
						      		@method('PATCH')
						      		@csrf
						      		<input type="hidden" class="form-control" name="customer" value="{{ $customerdetail->customer }}">
						      		<div class="form-group">
						          		<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ $customerdetail->name }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="address">Address</label>
						              	<input type="text" class="form-control" name="address" value="{{ $customerdetail->address }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="City">City</label>
						              	<select class="form-control select2" name="city" required>
						              		@foreach($citys as $city)
											    @if ($customerdetail->city == $city->id)
												    <option value="{{ $city->id }}" selected>{{ $city->code }} - {{ $city->name }}</option>
												@else
												    <option value="{{ $city->id }}">{{ $city->code }} - {{ $city->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="mobile">Mobile</label>
						              	<input type="number" class="form-control" name="mobile" value="{{ $customerdetail->mobile }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="alias_name">Alias Name</label>
						              	<input type="text" class="form-control" name="alias_name" value="{{ $customerdetail->alias_name }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="alias_mobile">Alias Mobile</label>
						              	<input type="number" class="form-control" name="alias_mobile" value="{{ $customerdetail->alias_mobile }}">
						          	</div>
						          	<button id="updateBtn" value="{{ route('customerdetail.update', $customerdetail->id) }}" class="btn btn-sm btn-success">Update</button>
				          			<button id="cancelBtn" value="{{ url('/customerdetail/'.$customerdetail->customer) }}"class="btn btn-sm btn-danger">Cancel</button>
						      	</form>
			  				</div>

			  				<div class="col-md-9">
			  					<br><table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="20%">Name</th>
								          	<th width="25%">Address</th>
								          	<th width="20%">Mobile</th>
								          	<th width="20%">Alias</th>
								          	<th width="15%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($customerdetails as $customerdetail)
								    		<tr>
									            <td>{{$customerdetail->name}}</td>
									            <td>
									            	{{$customerdetail->address}} <br>
									            	{{$customerdetail->fkCity->name}} - {{$customerdetail->fkCity->fkCountry->name}}
									            </td>
									            <td>{{$customerdetail->mobile}}</td>
									            <td>
									            	{{$customerdetail->alias_name}} <br> {{$customerdetail->alias_mobile}}
									            </td>
									            <td>
									            	@if($customerdetail->status == 1) <a href="{{ route('customerdetail.edit',$customerdetail->id)}}" class="btn btn-xs btn-warning">Edit</a>
									            	@else <font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
									            	@endif
									            </td>
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
			  				</div>
		  				</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.customer.prepare_editdetail());
    </script>
@stop