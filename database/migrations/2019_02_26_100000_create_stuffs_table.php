<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStuffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stuffs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('schedulestuff');
            $table->integer('quotationsplit');
            $table->integer('pol')->unsigned();
            $table->integer('pod')->unsigned();
            $table->integer('container')->unsigned();
            $table->integer('no_inc');
            $table->string('nomor');
            $table->date('etd');
            $table->date('eta');
            $table->string('vehicle');
            $table->string('seal');
            $table->integer('qty_fcl');
            $table->integer('qty_bag');
            $table->integer('qty_pcs');
            $table->integer('qty_kg');
            $table->string('nopol');
            $table->string('driver');
            $table->string('mobile');
            $table->text('notice')->nullable();
            $table->integer('container_empty');
            $table->integer('container_bruto');
            $table->integer('container_netto');
            $table->integer('qty_load')->default('0');
            $table->tinyInteger('status_load')->default('0');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->foreign('pol')->references('id')->on('ports');
            $table->foreign('pod')->references('id')->on('ports');
            $table->foreign('container')->references('id')->on('containers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stuffs');
    }
}
