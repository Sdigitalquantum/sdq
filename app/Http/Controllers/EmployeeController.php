<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Employee;
use App\Http\Models\Employeeset;
use App\Http\Models\Group;
use App\Http\Models\Groupset;
use App\Http\Models\Branch;
use App\Http\Models\Department;
use App\Http\Models\Position;
use App\Http\Models\Menuroot;
use App\Http\Models\Menu;
use App\Http\Models\Menusub;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $employees = Employee::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/employee.index', compact('employees','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $groups = Group::where('status', 1)->orderBy('name','asc')->get();
        $branchs = Branch::where('status', 1)->orderBy('name','asc')->get();
        $departments = Department::where('status', 1)->orderBy('name','asc')->get();
        $positions = Position::where('status', 1)->orderBy('name','asc')->get();

        return view('vendor/adminlte/employee.create', compact('groups','branchs','departments','positions','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        
        $request->validate([
            'email' => 'unique:employees|email:true'
        ]);

        $employee = new Employee([
            'group'         => $request->get('group'),
            'branch'        => $request->get('branch'),
            'department'    => $request->get('department'),
            'position'      => $request->get('position'),
            'name'          => $request->get('name'),
            'email'         => $request->get('email'),
            'password'      => md5($request->get('password')),
            'created_user'  => $session_id,
            'updated_user'  => $session_id
        ]);
        $employee->save();

        $groupsets = Groupset::where([['group', $request->get('group')], ['status', 1]])->get();
        foreach ($groupsets as $groupset) {
            $employeeroot = new Employeeset([
                'employee'      => $employee->id,
                'menuroot'      => $groupset->menuroot,
                'nomorroot'     => $groupset->fkMenuroot->nomor,
                'menu'          => 0,
                'nomormenu'     => 0,
                'menusub'       => 0,
                'nomorsub'      => 0,
                'created_user'  => $session_id
            ]);
            $employeeroot->save();

            $menus = Menu::where([['menuroot', $groupset->menuroot], ['status', 1]])->get();
            foreach ($menus as $menu) {
                $employeemenu = new Employeeset([
                    'employee'      => $employee->id,
                    'menuroot'      => $groupset->menuroot,
                    'nomorroot'     => $groupset->fkMenuroot->nomor,
                    'menu'          => $menu->id,
                    'nomormenu'     => $menu->nomor,
                    'menusub'       => 0,
                    'nomorsub'      => 0,
                    'created_user'  => $session_id
                ]);
                $employeemenu->save();

                $menusubs = Menusub::where([['menu', $menu->id], ['status', 1]])->get();
                foreach ($menusubs as $menusub) {
                    $employeesub = new Employeeset([
                        'employee'      => $employee->id,
                        'menuroot'      => $groupset->menuroot,
                        'nomorroot'     => $groupset->fkMenuroot->nomor,
                        'menu'          => $menu->id,
                        'nomormenu'     => $menu->nomor,
                        'menusub'       => $menusub->id,
                        'nomorsub'      => $menusub->nomor,
                        'created_user'  => $session_id
                    ]);
                    $employeesub->save();
                }
            }
        }

        return redirect('/employee')->with('success', 'User has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $employee = Employee::find($id);
        $groups = Group::where('status', 1)->orderBy('name','asc')->get();
        $branchs = Branch::where('status', 1)->orderBy('name','asc')->get();
        $departments = Department::where('status', 1)->orderBy('name','asc')->get();
        $positions = Position::where('status', 1)->orderBy('name','asc')->get();

        return view('vendor/adminlte/employee.edit', compact('employee','groups','branchs','departments','positions','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $employee = Employee::find($id);

        if ($request->get('email') != $employee->email) {
            $request->validate([
                'email' => 'unique:employees|email:true'
            ]);
        }

        if ($request->get('password') == $employee->password) {
            $password = $request->get('password');
        } else {
            $password = md5($request->get('password'));
        }

        if ($request->get('group') != $employee->group) {
            $groups = Employeeset::where('employee', $id)->get();
            foreach ($groups as $group) {
                $group->delete();
            }

            $groupsets = Groupset::where([['group', $request->get('group')], ['status', 1]])->get();
            foreach ($groupsets as $groupset) {
                $employeeroot = new Employeeset([
                    'employee'      => $employee->id,
                    'menuroot'      => $groupset->menuroot,
                    'nomorroot'     => $groupset->fkMenuroot->nomor,
                    'menu'          => 0,
                    'nomormenu'     => 0,
                    'menusub'       => 0,
                    'nomorsub'      => 0,
                    'created_user'  => $session_id
                ]);
                $employeeroot->save();

                $menus = Menu::where([['menuroot', $groupset->menuroot], ['status', 1]])->get();
                foreach ($menus as $menu) {
                    $employeemenu = new Employeeset([
                        'employee'      => $employee->id,
                        'menuroot'      => $groupset->menuroot,
                        'nomorroot'     => $groupset->fkMenuroot->nomor,
                        'menu'          => $menu->id,
                        'nomormenu'     => $menu->nomor,
                        'menusub'       => 0,
                        'nomorsub'      => 0,
                        'created_user'  => $session_id
                    ]);
                    $employeemenu->save();

                    $menusubs = Menusub::where([['menu', $menu->id], ['status', 1]])->get();
                    foreach ($menusubs as $menusub) {
                        $employeesub = new Employeeset([
                            'employee'      => $employee->id,
                            'menuroot'      => $groupset->menuroot,
                            'nomorroot'     => $groupset->fkMenuroot->nomor,
                            'menu'          => $menu->id,
                            'nomormenu'     => $menu->nomor,
                            'menusub'       => $menusub->id,
                            'nomorsub'      => $menusub->nomor,
                            'created_user'  => $session_id
                        ]);
                        $employeesub->save();
                    }
                }
            }
        }

        $employee->group           = $request->get('group');
        $employee->branch          = $request->get('branch');
        $employee->department      = $request->get('department');
        $employee->position        = $request->get('position');
        $employee->name            = $request->get('name');
        $employee->email           = $request->get('email');
        $employee->password        = $password;
        $employee->updated_user    = $session_id;
        $employee->save();

        return redirect('/employee')->with('success', 'User has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $employee = Employee::find($id);
        $status = $employee->status;

        if ($status == 1) {
        	$employee->status       = 0;
            $employee->updated_user = $session_id;
	        $employee->save();

		    return redirect('/employee')->with('error', 'User has been deleted');
        } elseif ($status == 0) {
        	$employee->status       = 1;
            $employee->updated_user = $session_id;
	        $employee->save();

		    return redirect('/employee')->with('success', 'User has been activated');
        }
    }
}
