<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Typesub extends Model
{
    protected $fillable = [
	    'type',
	    'name',
	    'created_user',
	    'updated_user'
  	];

  	public function fkType(){
	    return $this->belongsTo('\App\Http\Models\Type', 'type', 'id');
	}
}