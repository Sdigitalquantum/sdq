<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Warehouse;
use App\Http\Models\City;
use App\Http\Models\Employeeset;

class WarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $warehouses = Warehouse::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/warehouse.index', compact('warehouses','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/warehouse.create', compact('citys','disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	$json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'name' => 'unique:warehouses'
                ]);

                $no = Warehouse::select('no_inc')->max('no_inc');
                if (empty($no)) {
                    $no_inc = 1;
                } else {
                    $no_inc = $no + 1;
                }
                $code = "G".sprintf("%05s", $no_inc);

                $warehouse = new Warehouse([
                    'city'          => $json->city,
                    'no_inc'        => $no_inc,
                    'code'          => $code,
                    'name'          => $json->name,
                    'address'       => $json->address,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $warehouse->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $warehouse = Warehouse::find($id);
        $cities = City::where('status', 1)->get();
       
        return view('vendor/adminlte/warehouse.edit', compact('warehouse','cities','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $warehouse = Warehouse::find($id);
                $name = $warehouse->name;

                if ($name != $json->name) {
                    $request->validate([
                        'name' => 'unique:warehouses'
                    ]);
                }
                
                $warehouse->city            = $json->city;
                $warehouse->name            = $json->name;
                $warehouse->address         = $json->address;
                $warehouse->updated_user    = $session_id;
                $result = $warehouse->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $warehouse = Warehouse::find($id);
        $status = $warehouse->status;

        if ($status == 1) {
        	$warehouse->status       = 0;
            $warehouse->updated_user = $session_id;
	        $result = $warehouse->save();

        } elseif ($status == 0) {
        	$warehouse->status       = 1;
            $warehouse->updated_user = $session_id;
	        $result = $warehouse->save();
        };
        return $this->jsonSuccess( $result );
    }
}
