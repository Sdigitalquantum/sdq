<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Debt extends Model
{
    protected $fillable = [
	    'no_po',
	    'code_supplier',
	    'name_supplier',
	    'code_product',
	    'name_product',
	    'date_po',
	    'date_qc',
	    'qty',
	    'price',
	    'total',
	    'payment',
	    'adjustment',
	    'created_user',
	    'updated_user'
  	];
}
