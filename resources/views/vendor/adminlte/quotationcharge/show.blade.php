@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
            <li class="disabled"><a>Payment</a></li>
            <li class="active"><a href="#tab_charge" data-toggle="tab">Charges</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_charge">
            	<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Quotation Charges <b>{{ $quotation->no_sp }}</b>
				  			&nbsp;<a href="{{ route('quotationpayment.show', $quotation->id) }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a>
				  		</h4>
			  		</div>
				  	
				  	<div class="box-body">
				  		<div class="row">
				  			<div class="col-md-3">
				  				<form method="post" action="{{ route('quotationcharge.store') }}">
						          	@csrf
						          	<input type="hidden" class="form-control" name="quotation" value="{{ $quotation->id }}">
						          	<div class="form-group">
						          		<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="type">Action</label> <br>
						              	<input type="radio" name="type" value="1" required> Plus &nbsp;&nbsp;&nbsp;
						              	<input type="radio" name="type" value="2"> Minus
						          	</div>
						          	<div class="form-group">
						          		<label for="value">Value</label>
						              	<input type="number" class="form-control" name="value" value="{{ old('value') }}" step=".01" required>
						          	</div>
						          	<button type="submit" class="btn btn-sm btn-success">Save</button>
						      	</form>
			  				</div>

			  				<div class="col-md-9">
			  					<br><table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="45%">Name</th>
								          	<th width="40%">Value</th>
								          	<th width="15%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($quotationcharges as $quotationcharge)
								    		<tr>
									            <td>{{$quotationcharge->name}}</td>
									            <td style="text-align: right;">
									            	@if($quotationcharge->type == 1) (+)
									            	@elseif($quotationcharge->type == 2) (-)
									            	@endif

									            	{{$quotationcharge->value}} &nbsp;
									            </td>

									            @if($quotationcharge->status == 1)
										            <td>
										            	<a href="{{ route('quotationcharge.edit',$quotationcharge->id)}}" class="btn btn-xs btn-warning">Edit</a> &nbsp;
										            	
										            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$quotationcharge->id}}">Delete</button>

										            	<form action="{{ route('quotationcharge.destroy', $quotationcharge->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$quotationcharge->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-danger">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to delete this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
										            </td>
									            @else
									            	<td>
									            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$quotationcharge->id}}">Activated</button>

									            		<form action="{{ route('quotationcharge.destroy', $quotationcharge->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$quotationcharge->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-success">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to active this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
									            	</td>
									            @endif
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
			  				</div>
		  				</div>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop