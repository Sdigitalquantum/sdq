<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Document;
use App\Http\Models\Employeeset;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $documents = Document::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/document.index', compact('documents','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;
        $document = new Document();
        return view('vendor/adminlte/document.create', compact('document','disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	$json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'name' => 'unique:documents'
                ]);
                
                $no = Document::select('no_inc')->max('no_inc');
                if (empty($no)) {
                    $no_inc = 1;
                } else {
                    $no_inc = $no + 1;
                }
                $code = "DOC".sprintf("%05s", $no_inc);

                $export   = 0; if (isset($json->export))  { $export   = $json->export; };
                $local    = 0; if (isset($json->local))   { $local    = $json->local;  };
                $internal = 0; if (isset($json->internal)){ $internal = $json->internal; };

                $document = new Document([
                    'no_inc'        => $no_inc,
                    'code'          => $code,
                    'name'          => $json->name,
                    'export'        => $export,
                    'local'         => $local,
                    'internal'      => $internal,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $document->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $document = Document::find($id);

        return view('vendor/adminlte/document.edit', compact('document','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $document = Document::find($id);
                $name = $document->name;

                if ($name != $json->name) {
                    $request->validate([
                        'name' => 'unique:documents'
                    ]);
                }
                
                $document->name             = $json->name;
                $document->export           = ($json->export == 1) ? 1 : 0;
                $document->local            = ($json->local == 1) ? 1 : 0;
                $document->internal         = ($json->internal == 1) ? 1 : 0;
                $document->updated_user     = $session_id;
                $result = $document->save();

            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $document = Document::find($id);
        $status = $document->status;

        if ($status == 1) {
        	$document->status       = 0;
            $document->updated_user = $session_id;
	        $result = $document->save();

        } elseif ($status == 0) {
        	$document->status       = 1;
            $document->updated_user = $session_id;
	        $result = $document->save();

        };
        return $this->jsonSuccess( $result );
    }
}
