<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStuffreschedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stuffreschedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('schedulestuff')->unsigned();
            $table->tinyInteger('reason');
            $table->date('date_change');
            $table->date('date_old');
            $table->text('notice');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->foreign('schedulestuff')->references('id')->on('schedulestuffs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stuffreschedules');
    }
}
