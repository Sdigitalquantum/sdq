<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Kurs extends Model
{
    protected $fillable = [
	    'currency',
	    'value',
	    'start',
	    'end',
	    'created_user',
	    'updated_user'
  	];

  	public function fkCurrency(){
	    return $this->belongsTo('\App\Http\Models\Currency', 'currency', 'id');
	}
}
