<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Menuroot;
use App\Http\Models\Menu;
use App\Http\Models\Menusub;
use App\Http\Models\Group;
use App\Http\Models\Groupset;
use App\Http\Models\Employee;
use App\Http\Models\Employeeset;

class GroupsetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $groups = Group::orderBy('created_at','desc')->get();
        $groupsets = Groupset::where('status', 1)->orderBy('menuroot','asc')->get();

        return view('vendor/adminlte/groupset.index', compact('groups','groupsets','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $group = Group::find($id);
        $menuroots = Menuroot::where('status', 1)->orderBy('nomor','asc')->get();
        $groupsets = Groupset::where('group', $id)->orderBy('group','asc')->get();

        return view('vendor/adminlte/groupset.edit', compact('group','menuroots','groupsets','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $cek = Groupset::where([['menuroot', $request->get('menuroot')], ['group', $request->get('group')]])->first();

        if (!empty($cek)) {
            return redirect()->back()->withErrors('Access Menu already exist');
        } else {
            $groupset = new Groupset([
                'menuroot'      => $request->get('menuroot'),
                'group'         => $request->get('group'),
                'created_user'  => $session_id,
                'updated_user'  => $session_id
            ]);
            $groupset->save();

            return redirect('/groupset/'.$request->get('group').'/edit')->with('success', 'Access Menu has been added');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $groupset = Groupset::find($id);
        $status = $groupset->status;

        if ($status == 1) {
            $cek = Employeeset::where('menuroot', $groupset->menuroot)->first();
            if (!empty($cek)) {
                return redirect()->back()->withErrors('Access Menu already used');
            } else {
                $groupset->status       = 0;
                $groupset->updated_user = $session_id;
                $groupset->save();

                return redirect('/groupset/'.$groupset->group.'/edit')->with('error', 'Access Menu has been deleted');
            }
        } elseif ($status == 0) {
        	$groupset->status       = 1;
            $groupset->updated_user = $session_id;
	        $groupset->save();

		    return redirect('/groupset/'.$groupset->group.'/edit')->with('success', 'Access Menu has been activated');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sync(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $employees = Employee::where('group', $id)->get();
        foreach ($employees as $employee) {
            $groupsets = Groupset::where([['group', $id], ['status', 1]])->orderBy('menuroot','asc')->get();
            foreach ($groupsets as $groupset) {
                $cekroot = Employeeset::where([['employee', $employee->id], ['menuroot', $groupset->menuroot]])->first();
                if (!empty($cekroot)) {
                    $employeesetroots = Employeeset::where([['employee', $employee->id], ['menuroot', $groupset->menuroot]])->get();
                    foreach ($employeesetroots as $employeesetroot) {
                        if (empty($employeesetroot->id)) {
                            $employeeroot = new Employeeset([
                                'employee'      => $employee->id,
                                'menuroot'      => $groupset->menuroot,
                                'nomorroot'     => $groupset->fkMenuroot->nomor,
                                'menu'          => 0,
                                'nomormenu'     => 0,
                                'menusub'       => 0,
                                'nomorsub'      => 0,
                                'created_user'  => $session_id
                            ]);
                            $employeeroot->save();
                        } else {
                            $employeesetroot->nomorroot = $groupset->fkMenuroot->nomor;
                            $employeesetroot->save();
                        }
                    }

                    $menus = Menu::where([['menuroot', $groupset->menuroot], ['status', 1]])->orderBy('nomor','asc')->get();
                    foreach ($menus as $menu) {
                        $cekmenu = Employeeset::where([['employee', $employee->id], ['menu', $menu->id]])->first();
                        if (!empty($cekmenu)) {
                            $employeesetmenus = Employeeset::where([['employee', $employee->id], ['menu', $menu->id]])->get();
                            foreach ($employeesetmenus as $employeesetmenu) {
                                if (empty($employeesetmenu->id)) {
                                    $employeemenu = new Employeeset([
                                        'employee'      => $employee->id,
                                        'menuroot'      => $groupset->menuroot,
                                        'nomorroot'     => $groupset->fkMenuroot->nomor,
                                        'menu'          => $menu->id,
                                        'nomormenu'     => $menu->nomor,
                                        'menusub'       => 0,
                                        'nomorsub'      => 0,
                                        'created_user'  => $session_id
                                    ]);
                                    $employeemenu->save();
                                } else {
                                    $employeesetmenu->nomormenu = $menu->nomor;
                                    $employeesetmenu->save();
                                }
                            }

                            $menusubs = Menusub::where([['menu', $menu->id], ['status', 1]])->orderBy('nomor','asc')->get();
                            foreach ($menusubs as $menusub) {
                                $ceksub = Employeeset::where([['employee', $employee->id], ['menusub', $menusub->id]])->first();
                                if (!empty($ceksub)) {
                                    $employeesetsubs = Employeeset::where([['employee', $employee->id], ['menusub', $menusub->id]])->get();
                                    foreach ($employeesetsubs as $employeesetsub) {
                                        if (empty($employeesetsub->id)) {
                                            $employeesub = new Employeeset([
                                                'employee'      => $employee->id,
                                                'menuroot'      => $groupset->menuroot,
                                                'nomorroot'     => $groupset->fkMenuroot->nomor,
                                                'menu'          => $menu->id,
                                                'nomormenu'     => $menu->nomor,
                                                'menusub'       => $menusub->id,
                                                'nomorsub'      => $menusub->nomor,
                                                'created_user'  => $session_id
                                            ]);
                                            $employeesub->save();
                                        } else {
                                            $employeesetsub->nomorsub = $menusub->nomor;
                                            $employeesetsub->save();
                                        }
                                    }
                                } else {
                                    $employeesub = new Employeeset([
                                        'employee'      => $employee->id,
                                        'menuroot'      => $groupset->menuroot,
                                        'nomorroot'     => $groupset->fkMenuroot->nomor,
                                        'menu'          => $menu->id,
                                        'nomormenu'     => $menu->nomor,
                                        'menusub'       => $menusub->id,
                                        'nomorsub'      => $menusub->nomor,
                                        'created_user'  => $session_id
                                    ]);
                                    $employeesub->save();
                                }
                            }
                        } else {
                            $employeemenu = new Employeeset([
                                'employee'      => $employee->id,
                                'menuroot'      => $groupset->menuroot,
                                'nomorroot'     => $groupset->fkMenuroot->nomor,
                                'menu'          => $menu->id,
                                'nomormenu'     => $menu->nomor,
                                'menusub'       => 0,
                                'nomorsub'      => 0,
                                'created_user'  => $session_id
                            ]);
                            $employeemenu->save();

                            $menusubs = Menusub::where([['menu', $menu->id], ['status', 1]])->orderBy('nomor','asc')->get();
                            foreach ($menusubs as $menusub) {
                                $employeesub = new Employeeset([
                                    'employee'      => $employee->id,
                                    'menuroot'      => $groupset->menuroot,
                                    'nomorroot'     => $groupset->fkMenuroot->nomor,
                                    'menu'          => $menu->id,
                                    'nomormenu'     => $menu->nomor,
                                    'menusub'       => $menusub->id,
                                    'nomorsub'      => $menusub->nomor,
                                    'created_user'  => $session_id
                                ]);
                                $employeesub->save();
                            }
                        }
                    }
                } else {
                    $employeeroot = new Employeeset([
                        'employee'      => $employee->id,
                        'menuroot'      => $groupset->menuroot,
                        'nomorroot'     => $groupset->fkMenuroot->nomor,
                        'menu'          => 0,
                        'nomormenu'     => 0,
                        'menusub'       => 0,
                        'nomorsub'      => 0,
                        'created_user'  => $session_id
                    ]);
                    $employeeroot->save();

                    $menus = Menu::where([['menuroot', $groupset->menuroot], ['status', 1]])->orderBy('nomor','asc')->get();
                    foreach ($menus as $menu) {
                        $employeemenu = new Employeeset([
                            'employee'      => $employee->id,
                            'menuroot'      => $groupset->menuroot,
                            'nomorroot'     => $groupset->fkMenuroot->nomor,
                            'menu'          => $menu->id,
                            'nomormenu'     => $menu->nomor,
                            'menusub'       => 0,
                            'nomorsub'      => 0,
                            'created_user'  => $session_id
                        ]);
                        $employeemenu->save();

                        $menusubs = Menusub::where([['menu', $menu->id], ['status', 1]])->orderBy('nomor','asc')->get();
                        foreach ($menusubs as $menusub) {
                            $employeesub = new Employeeset([
                                'employee'      => $employee->id,
                                'menuroot'      => $groupset->menuroot,
                                'nomorroot'     => $groupset->fkMenuroot->nomor,
                                'menu'          => $menu->id,
                                'nomormenu'     => $menu->nomor,
                                'menusub'       => $menusub->id,
                                'nomorsub'      => $menusub->nomor,
                                'created_user'  => $session_id
                            ]);
                            $employeesub->save();
                        }
                    }
                }
            }
        }

        return redirect('/groupset')->with('success', 'User Access has been synchronized');
    }
}
