<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Productalias extends Model
{
    protected $fillable = [
	    'product',
	    'customerdetail',
	    'alias_name',
	    'created_user',
	    'updated_user'
  	];

	public function fkProduct(){
	    return $this->belongsTo('\App\Http\Models\Product', 'product', 'id');
	}

  	public function fkCustomerdetail(){
	    return $this->belongsTo('\App\Http\Models\Customerdetail', 'customerdetail', 'id');
	}
}
