<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Debtcard extends Model
{
    protected $fillable = [
	    'supplierdetail',
	    'year',
	    'month',
	    'debit',
	    'kredit'
  	];

  	public function fkSupplierdetail(){
	    return $this->belongsTo('\App\Http\Models\Supplierdetail', 'supplierdetail', 'id');
	}
}
