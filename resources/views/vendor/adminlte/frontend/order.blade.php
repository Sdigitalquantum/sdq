@extends('adminlte::frontend/page')

@section('content')
    <section class='section-heading padding-top-100 padding-bottom-0'>
        <div class="angled_down_inside white">
            <div class="slope upleft"></div>
            <div class="slope upright"></div>
        </div>
        
        <div class="container">
            <div class="row">
                <h1 class="heading">Order Product</h1>
                <div class="headul"></div>
                <p class="subheading">Silahkan memesan produk kami dengan cara mengisi form di bawah ini</p>
            </div>
        </div>
        
        <div class="angled_up_inside white">
            <div class="slope upleft"></div>
            <div class="slope upright"></div>
        </div>
    </section>
    
    <section class="parallax contact padding-top-150" id="order">
        <div class="bg-overlay opacity-85"></div>
        <div class="container">
            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 animated delay1" data-effect="fadeInUp">
                <form method="post" action="{{ route('mailorder.store') }}">
                    @csrf
                    <input type='text' placeholder='Customer' class='col-xs-12 transition' name='customer' value="{{ old('customer') }}" required>
                    <input type='text' placeholder='No. PO' class='col-xs-12 transition' name='no_po' value="{{ old('no_po') }}">
                    <input type='text' placeholder='Email' class='col-xs-12 transition' name='email' value="{{ old('email') }}" required>
                    <input type='number' placeholder='Phone' class='col-xs-12 transition' name='phone' value="{{ old('phone') }}" required>
                    <input type='text' placeholder='Address' class='col-xs-12 transition' name='address' value="{{ old('address') }}" required>
                    <textarea class='col-xs-12 transition' placeholder='Note' name='notice'>{{ old('notice') }}</textarea>
                    <button type='submit' class='btn btn-block btn-primary transition'>Choose Product</button>
                </form>
            </div>
        </div>
    </section>
@stop
