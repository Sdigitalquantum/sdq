<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentchecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentchecks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('schedulestuff');
            $table->integer('quotationsplit');
            $table->integer('document')->unsigned();
            $table->string('notice')->nullable();
            $table->tinyInteger('status');
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->foreign('document')->references('id')->on('documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documentchecks');
    }
}
