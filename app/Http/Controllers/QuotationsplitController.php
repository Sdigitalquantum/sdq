<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Quotation;
use App\Http\Models\Quotationdetail;
use App\Http\Models\Quotationsplit;
use App\Http\Models\Quotationplan;
use App\Http\Models\Plan;
use App\Http\Models\Port;
use App\Http\Models\Employeeset;

class QuotationsplitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $quotations = Quotation::where('status', 1)->orderBy('created_at','desc')->get();

        return view('vendor/adminlte/quotationsplit.index', compact('quotations','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $quotation = Quotation::find($id);
        $quotationsplits = Quotationsplit::where('quotation', $id)->orderBy('no_split','desc')->get();
        $quotationplans = Quotationplan::orderBy('plan')->get();
        $ports = Port::where('status', 1)->get();

        return view('vendor/adminlte/quotationsplit.create', compact('quotation','quotationsplits','quotationplans','ports','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        if ($request->get('eta') < $request->get('etd')) {
            return redirect()->back()->withErrors('Time Arrival lower than Time Departure');
        } else {
            $quotation = Quotation::find($request->get('quotation'));
            $no = Quotationsplit::select('no_inc')->where('quotation', $request->get('quotation'))->max('no_inc');
            
            if (empty($no)) {
                $no_inc = 1;
            } else {
                $no_inc = $no + 1;
            }

            if ($no_inc == 1) { $code = "A"; }
            elseif ($no_inc == 2) { $code = "B"; }
            elseif ($no_inc == 3) { $code = "C"; }
            elseif ($no_inc == 4) { $code = "D"; }
            elseif ($no_inc == 5) { $code = "E"; }
            elseif ($no_inc == 6) { $code = "F"; }
            elseif ($no_inc == 7) { $code = "G"; }
            elseif ($no_inc == 8) { $code = "H"; }
            elseif ($no_inc == 9) { $code = "I"; }
            elseif ($no_inc == 10) { $code = "J"; }
            elseif ($no_inc == 11) { $code = "K"; }
            elseif ($no_inc == 12) { $code = "L"; }
            elseif ($no_inc == 13) { $code = "M"; }
            elseif ($no_inc == 14) { $code = "N"; }
            elseif ($no_inc == 15) { $code = "O"; }
            elseif ($no_inc == 16) { $code = "P"; }
            elseif ($no_inc == 17) { $code = "Q"; }
            elseif ($no_inc == 18) { $code = "R"; }
            elseif ($no_inc == 19) { $code = "S"; }
            elseif ($no_inc == 20) { $code = "T"; }
            elseif ($no_inc == 21) { $code = "U"; }
            elseif ($no_inc == 22) { $code = "V"; }
            elseif ($no_inc == 23) { $code = "W"; }
            elseif ($no_inc == 24) { $code = "X"; }
            elseif ($no_inc == 25) { $code = "Y"; }
            elseif ($no_inc == 26) { $code = "Z"; }

            $no_split = $quotation->no_sp.'-'.$code;

            $quotationsplit = new Quotationsplit([
                'quotation'         => $request->get('quotation'),
                'pol'               => $request->get('pol'),
                'pod'               => $request->get('pod'),
                'no_inc'            => $no_inc,
                'no_split'          => $no_split,
                'etd'               => $request->get('etd'),
                'eta'               => $request->get('eta'),
                'created_user'      => $session_id,
                'updated_user'      => $session_id
            ]);
            $quotationsplit->save();
            $quotationsplit->id;

            return redirect('/quotationplan/'.$quotationsplit->id.'/edit')->with('success', 'Split Proforma has been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $quotationsplit = Quotationsplit::find($id);
        $quotationplans = Quotationplan::orderBy('plan')->get();
        $quotationsplits = Quotationsplit::where('quotation', $quotationsplit->quotation)->orderBy('no_split','desc')->get();
        $ports = Port::where('status', 1)->get();

        return view('vendor/adminlte/quotationsplit.edit', compact('quotationsplit','quotationsplits','quotationplans','ports','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        if ($request->get('eta') < $request->get('etd')) {
            return redirect()->back()->withErrors('Time Arrival lower than Time Departure');
        } else {
            $quotationsplit = Quotationsplit::find($id);
            $quotationsplit->pol          = $request->get('pol');
            $quotationsplit->pod          = $request->get('pod');
            $quotationsplit->etd          = $request->get('etd');
            $quotationsplit->eta          = $request->get('eta');
            $quotationsplit->updated_user = $session_id;
            $quotationsplit->save();

            return redirect('/quotationsplit/create/'.$quotationsplit->quotation)->with('success', 'Quotation Split has been updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $quotationsplit = Quotationsplit::find($id);
        $status = $quotationsplit->status;

        if ($status == 1) {
        	$quotationsplit->status       = 0;
            $quotationsplit->updated_user = $session_id;
	        $quotationsplit->save();

		    return redirect('/quotationsplit/create/'.$quotationsplit->quotation)->with('error', 'Quotation Split has been deleted');
        } elseif ($status == 0) {
        	$quotationsplit->status       = 1;
            $quotationsplit->updated_user = $session_id;
	        $quotationsplit->save();

		    return redirect('/quotationsplit/create/'.$quotationsplit->quotation)->with('success', 'Quotation Split has been activated');
        }
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function release(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $quotationsplit = Quotationsplit::find($id);
        $quotationsplit->status_release  = 1;
        $quotationsplit->updated_user    = $session_id;
        $quotationsplit->save();

        return redirect('/quotationsplit/create/'.$quotationsplit->quotation)->with('success', 'Quotation Split has been released');
    }
}
