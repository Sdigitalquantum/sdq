@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #6b6e70 !important; font-weight: bold; color: white;">
			    		<h4 class="box-title">New City</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form id="cityFrm" > <!-- method="post" action="{{ route('city.store') }}" -->
				          	@method('POST')
				          	@csrf
				          	<div class="form-group">
				              	<label for="code">Code</label>
				              	<input type="text" class="form-control" name="code" disabled>
				          	</div>
				          	<div class="form-group">
				              	<label for="name">Name</label>
				              	<input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
				          	</div>
				          	<div class="form-group">
				          		<label for="country">Country</label>
				              	<select class="form-control select2" name="country" required>
				              		<option value="{{ old('country') }}">Choose One</option>
			                    	@foreach($countries as $country)
									    <option value="{{ $country->id }}">{{ $country->name }}</option>
									@endforeach
			                	</select>
				          	</div>
				          	<!-- <button onclick="sdq.core.submitForm('cityFrm', '{{ route("city.store", "") }}', 'city' );return false;" class="btn btn-sm btn-success" >Save</button>
				          	<a href="#" onclick="sdq.core.load('city');return false;" class="btn btn-sm btn-danger">Cancel</a> -->

				          	<button id="saveBtn" value="{{ route('city.store') }}" class="btn btn-sm btn-success">Save</button>
				          	<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.city.prepare_create());
    </script>
@stop