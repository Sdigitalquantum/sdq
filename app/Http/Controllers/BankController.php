<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Bank;
use App\Http\Models\Employeeset;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $banks = Bank::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/bank.index', compact('banks','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        // $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        // $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        // $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $bank = new Bank();

        return view('vendor/adminlte/bank.create', compact('bank', 'disable'));
        // ,'employeeroots','employeemenus','employeesubs'
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'name'    => 'unique:banks',
                    'alias'   => 'unique:banks'
                ]);

                $no = Bank::select('no_inc')->max('no_inc');
                if (empty($no)) {
                    $no_inc = 1;
                } else {
                    $no_inc = $no + 1;
                }
                $code = "B".sprintf("%05s", $no_inc);

                $bank = new Bank([
                    'no_inc'        => $no_inc,
                    'code'          => $code,
                    'name'          => $json->name,
                    'alias'         => $json->alias,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $bank->save();
            };
        };
        return $this->jsonSuccess( $result );
	    // return redirect('/bank')->with('success', 'Bank has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        // $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        // $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        // $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $bank = Bank::find($id);

        return view('vendor/adminlte/bank.edit', compact('bank','disable'));
        //,'employeeroots','employeemenus','employeesubs'
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $bank   = Bank::find($id);
                $name   = $bank->name;
                $alias  = $bank->alias;

                if ($name != $json->name && $alias != $json->alias) {
                    $request->validate([
                        'name'      => 'unique:banks',
                        'alias'     => 'unique:banks'
                    ]);
                } elseif ($name != $json->name) {
                    $request->validate([
                        'name'      => 'unique:banks'
                    ]);
                } elseif ($alias != $json->alias) {
                    $request->validate([
                        'alias'     => 'unique:banks'
                    ]);
                }
                
                $bank->name             = $json->name;
                $bank->alias            = $json->alias;
                $bank->updated_user     = $session_id;
                $result = $bank->save();
            };
        };
        return $this->jsonSuccess( $result );
	    // return redirect('/bank')->with('success', 'Bank has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $bank = Bank::find($id);
        $status = $bank->status;

        if ($status == 1) {
        	$bank->status       = 0;
            $bank->updated_user = $session_id;
	        $result = $bank->save();

		    // return redirect('/bank')->with('error', 'Bank has been deleted');
        } elseif ($status == 0) {
        	$bank->status       = 1;
            $bank->updated_user = $session_id;
	        $result = $bank->save();

		    // return redirect('/bank')->with('success', 'Bank has been activated');
        };
        return $this->jsonSuccess( $result );
    }
}
