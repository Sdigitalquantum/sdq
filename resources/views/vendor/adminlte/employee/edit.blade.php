@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit User</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('employee.update', $employee->id) }}">
				      		@method('PATCH')
				      		@csrf
				      		<div class="row">
    							<div class="col-md-6">
    								<div class="form-group">
						              	<label for="group">Group</label>
						              	<select class="form-control select2" name="group" required>
						              		<option value="{{ old('group') }}">Choose One</option>
					                    	@foreach($groups as $group)
											    @if ($employee->group == $group->id)
												    <option value="{{ $group->id }}" selected>{{ $group->name }}</option>
												@else
												    <option value="{{ $group->id }}">{{ $group->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="branch">Branch</label>
						              	<select class="form-control select2" name="branch" required>
						              		<option value="{{ old('branch') }}">Choose One</option>
					                    	@foreach($branchs as $branch)
											    @if ($employee->branch == $branch->id)
												    <option value="{{ $branch->id }}" selected>{{ $branch->name }}</option>
												@else
												    <option value="{{ $branch->id }}">{{ $branch->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="department">Department</label>
						              	<select class="form-control select2" name="department" required>
						              		<option value="{{ old('department') }}">Choose One</option>
					                    	@foreach($departments as $department)
											    @if ($employee->department == $department->id)
												    <option value="{{ $department->id }}" selected>{{ $department->name }}</option>
												@else
												    <option value="{{ $department->id }}">{{ $department->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="position">Position</label>
						              	<select class="form-control select2" name="position" required>
						              		<option value="{{ old('position') }}">Choose One</option>
					                    	@foreach($positions as $position)
											    @if ($employee->position == $position->id)
												    <option value="{{ $position->id }}" selected>{{ $position->name }}</option>
												@else
												    <option value="{{ $position->id }}">{{ $position->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						          		<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ $employee->name }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="email">Email</label>
						              	<input type="text" class="form-control" name="email" value="{{ $employee->email }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="password">Password</label>
						              	<input type="password" class="form-control" name="password" value="{{ $employee->password }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="button">&nbsp;</label> <br>
						              	<button type="submit" class="btn btn-sm btn-success">Update</button>
		          						<a href="{{ url('/employee') }}" class="btn btn-sm btn-danger">Cancel</a>
						          	</div>
								</div>
							</div>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop