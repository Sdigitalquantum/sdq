@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Approval Purchase Derivative</h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="5%">No</th>
			          	<th width="15%">No. PO</th>
			          	<th width="25%">Product</th>
			          	<th width="15%">Information</th>
			          	<th width="20%">Note</th>
			          	<th width="20%">Reason</th>
			          	<th width="5%">Action</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@php ($array = [])
			    	@foreach($employeewarehouses as $employeewarehouse)
			    		@php ($array[] = $employeewarehouse->warehouse)
			    	@endforeach

			    	@php ($no = 0)
			    	@foreach($stockqcs as $stockqc)
			    		@if(in_array($stockqc->warehouse, $array))
				    		@php ($no++)
				    		<tr>
				    			<td>{{$no}}</td>
					            <td>{{$stockqc->no_po}}</td>
					            <td>
					            	{{$stockqc->fkProduct->name}} <br>
					            	Supplier : {{$stockqc->fkSupplierdetail->name}} <br>
					            	Warehouse : {{$stockqc->fkWarehouse->name}}
					            </td>
					            <td>
					            	Date : {{$stockqc->date_in}} <br>
					            	Qty &nbsp; : {{number_format($stockqc->qty_kg, 0, ',' , '.')}} Kg <br>
					            	Price : Rp. {{number_format($stockqc->price, 0, ',' , '.')}} @Kg
					            </td>
					            <td><pre>{{$stockqc->notice}}</pre></td>
								<td><pre>{{$stockqc->reason}}</pre></td>

					            @if($stockqc->status_approve == 0)
									<td>
						            	<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal-approve{{$stockqc->id}}">Approve</button> &nbsp;
						                <button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal-reject{{$stockqc->id}}">Reject</button>

						            	<form action="{{ url('purchasederivative/approve', $stockqc->id)}}" method="post">
						                  	@csrf
						                  	@method('DELETE')
						                  	<div class="modal fade bs-example-modal-approve{{$stockqc->id}}" tabindex="-1" role="dialog" aria-hidden="true">
			                                    <div class="modal-dialog modal-sm">
			                                        <div class="modal-content">
			                                            <div class="modal-header btn-success">
			                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
			                                                </button>
			                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Approve</h4>
			                                            </div>
			                                            <div class="modal-body">
			                                            	Are you sure to approve this data?
			                                            	<br><textarea class="form-control" name="reason" cols="30" rows="3" maxlength="150" required></textarea>
			                                            </div>
			                                            <div class="modal-footer">
			                                                <button type="submit" class="btn btn-success">Yes</button>
			                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
						                </form>

						            	<form action="{{ url('purchasederivative/reject', $stockqc->id)}}" method="post">
						                  	@csrf
						                  	@method('DELETE')
						                  	<div class="modal fade bs-example-modal-reject{{$stockqc->id}}" tabindex="-1" role="dialog" aria-hidden="true">
			                                    <div class="modal-dialog modal-sm">
			                                        <div class="modal-content">
			                                            <div class="modal-header btn-danger">
			                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
			                                                </button>
			                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Reject</h4>
			                                            </div>
			                                            <div class="modal-body">
			                                            	Are you sure to reject this data?
			                                            	<br><textarea class="form-control" name="reason" cols="30" rows="3" maxlength="150" required></textarea>
			                                            </div>
			                                            <div class="modal-footer">
			                                                <button type="submit" class="btn btn-success">Yes</button>
			                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
						                </form>
					            	</td>
				            	@elseif($stockqc->status_approve == 1)
				            		<td><font color="green">Approved <i class="glyphicon glyphicon-ok"></i></font></td>
			            		@elseif($stockqc->status_approve == 2)
				            		<td><font color="red">Rejected <i class="glyphicon glyphicon-remove"></i></font></td>
								@endif
					        </tr>
				        @endif
			        @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			        	<td class="noShow"></td>
			          	<td>No. PO</td>
			          	<td>Product</td>
			          	<td>Information</td>
			          	<td>Note</td>
			          	<td>Reason</td>
			          	<td class="noShow"></td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	<div>
@stop