<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Stockload;
use App\Http\Models\Stuff;
use App\Http\Models\Schedulestuff;
use App\Http\Models\Quotationsplit;
use App\Http\Models\Plan;
use App\Http\Models\Stockin;
use App\Http\Models\Letter;
use App\Http\Models\Employeeset;
use App\Http\Models\Employeewarehouse;

class StockloadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $stuffs = Stuff::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/stockload.index', compact('stuffs','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $stuff = Stuff::find($id);
        $plans = Plan::where('status_split', 1)->get();
        $stockloads = Stockload::where('stuff', $id)->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/stockload.create', compact('stuff','plans','stockloads','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $stuff = Stuff::find($request->get('stuff'));
        $stockin = Stockin::find($request->get('stockin'));
        
        $request->validate([
            'qty_load' => 'numeric|min:0|not_in:0'
        ]);

        if ($request->get('qty_load') > ($stuff->qty_kg-$stuff->qty_load)) {
            return redirect()->back()->withErrors('Qty Real greater than Qty Remain');
        } else {
            $no = Stockload::select('no_inc')->whereYear('created_at', date('Y'))->max('no_inc');
            if (empty($no)) {
                $no_inc = 1;
            } else {
                $no_inc = $no + 1;
            }

            $letter = Letter::where('menu', 'Loading Stock')->first();
            if (empty($letter->company)) {
                $company = "";
            } else {
                $company = $letter->company."/";
            }

            if (empty($letter->code)) {
                $code = "";
            } else {
                $code = $letter->code;
            }

            if ($letter->romawi == 0) {
                $romawi = "";
            } else {
                if (date("m") == 1) {
                    $romawi = "I/";
                } elseif (date("m") == 2) {
                    $romawi = "II/";
                } elseif (date("m") == 3) {
                    $romawi = "III/";
                } elseif (date("m") == 4) {
                    $romawi = "IV/";
                } elseif (date("m") == 5) {
                    $romawi = "V/";
                } elseif (date("m") == 6) {
                    $romawi = "VI/";
                } elseif (date("m") == 7) {
                    $romawi = "VII/";
                } elseif (date("m") == 8) {
                    $romawi = "VIII/";
                } elseif (date("m") == 9) {
                    $romawi = "IX/";
                } elseif (date("m") == 10) {
                    $romawi = "X/";
                } elseif (date("m") == 11) {
                    $romawi = "XI/";
                } elseif (date("m") == 12) {
                    $romawi = "XII/";
                }
            }

            if ($letter->year == 0) {
                $year = "";
            } else {
                $year = date("Y");
            }

            $nomor = $company.$code.sprintf("%05s", $no_inc)."/".$romawi.$year;

            $cek = Stockload::where([['stuff', $request->get('stuff')], ['stockin', $request->get('stockin')]])->first();
            if (empty($cek)) {
                $stockload = new Stockload([
                    'stuff'             => $request->get('stuff'),
                    'stockin'           => $request->get('stockin'),
                    'no_inc'            => $no_inc,
                    'nomor'             => $nomor,
                    'qty_load'          => $request->get('qty_load'),
                    'created_user'      => $session_id
                ]);
                $stockload->save();
            } else {
                $cek->qty_load = $cek->qty_load+$request->get('qty_load');
                $cek->save();
            }

            $stuff->status_load     = 1;
            $stuff->qty_load        = $stuff->qty_load+$request->get('qty_load');
            $stuff->save();

            $stockin->qty_transit   = $stockin->qty_transit+$request->get('qty_load');
            $stockin->qty_booked    = $stockin->qty_booked-$request->get('qty_load');
            $stockin->save();

            if ($stuff->schedulestuff != 0) {
                $schedulestuff = Schedulestuff::where('id', $stuff->schedulestuff)->first();
                $schedulestuff->status_load = 1;
                $schedulestuff->save();
            } else {
                $quotationsplit = Quotationsplit::where('id', $stuff->quotationsplit)->first();
                $quotationsplit->status_load = 1;
                $quotationsplit->save();
            }

            return redirect('/stockload/create/'.$request->get('stuff'))->with('success', 'Stock has been loaded');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stockload = Stockload::find($id);
        $stockload->delete();

        $stockin = Stockin::where('id', $stockload->stockin)->first();
        $stockin->qty_transit   = $stockin->qty_transit-$stockload->qty_load;
        $stockin->qty_booked    = $stockin->qty_booked+$stockload->qty_load;
        $stockin->save();

        $stuff = Stuff::where('id', $stockload->stuff)->first();
        $stuff->qty_load = $stuff->qty_load-$stockload->qty_load;
        $stuff->save();

        return redirect('/stockload/create/'.$stockload->stuff)->with('error', 'Loading stock has been deleted');
    }
}
