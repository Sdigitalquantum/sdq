<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Port extends Model
{
    protected $fillable = [
	    'country',
	    'no_inc',
	    'code',
	    'name',
	    'created_user',
	    'updated_user'
  	];

  	public function fkCountry(){
	    return $this->belongsTo('\App\Http\Models\Country', 'country', 'id');
	}
}
