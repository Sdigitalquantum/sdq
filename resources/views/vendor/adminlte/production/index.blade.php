@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
                <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Assembly / Production &nbsp;<a href="{{ route('production.create')}}" class="btn btn-xs btn-success" style="text-decoration: none;">New</a></h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="15%">Code</th>
						          	<th width="25%">Product</th>
						          	<th width="10%">Qty (Kg)</th>
						          	<th width="20%">Raw Material</th>
						          	<th width="20%">Excess</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($productions as $production)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>{{$production->nomor}}</td>
							            <td>[{{$production->fkMaterial->fkProduct->code}}] {{$production->fkMaterial->fkProduct->name}}</td>
							            <td style="text-align: right;">{{number_format($production->qty, 0, ',' , '.')}} &nbsp;</td>
							            <td>
							            	@foreach($productiondetails as $productiondetail)
							            		@if($productiondetail->production == $production->id)
							            			<ul>
								            			<li>
								            				{{$productiondetail->fkProduct->name}} <br>
								            				Qty : {{number_format($productiondetail->qty, 0, ',' , '.')}} Kg
								            			</li>
								            		</ul>
							            		@endif
							            	@endforeach
							            </td>
							            <td>
							            	@foreach($productionothers as $productionother)
							            		@if($productionother->production == $production->id)
							            			<ul>
								            			<li>
								            				{{$productionother->fkProduct->name}} <br>
								            				Qty : {{number_format($productionother->qty, 0, ',' , '.')}} Kg
								            			</li>
								            		</ul>
							            		@endif
							            	@endforeach
							            </td>
							            <td>
							            	@if($production->status == 1)
							            		<a href="{{ route('production.edit',$production->id)}}" class="btn btn-xs btn-warning">Edit</a>
						            		@else
						            			<font color="blue">Valid <i class="glyphicon glyphicon-ok"></i></font>
						            		@endif
							            </td>
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Code</td>
						          	<td>Product</td>
						          	<td>Qty (Kg)</td>
						          	<td>Raw Material</td>
						          	<td>Excess</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop