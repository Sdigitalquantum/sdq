@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_request" data-toggle="tab">Request</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_request">
				<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		@php($netto = 0)
  						@foreach($quotationplans as $quotationplan)
	  						@foreach($plans as $plan)
	  							@if($plan->id == $quotationplan->plan)
	  								@php($netto = $netto + $plan->qty_plan)
  								@endif
	  						@endforeach
  						@endforeach

				  		<h4 class="box-title">Split Proforma <b>{{ $schedulestuff->fkSchedulebook->fkQuotationsplit->no_split }}</b>
				  			&nbsp;<font color="white">[ <b>{{ $schedulestuff->qty_approve }} Fcl - {{ $netto }} Kg</b> ]</font>
				  			&nbsp;<a href="{{ url('stockrequest') }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a>				  			
				  		</h4>
			  		</div>
				  	
				  	<div class="box-body">
				  		<form method="post" action="{{ route('stockrequest.store') }}">
				          	@csrf
				          	<input type="hidden" name="schedulestuff" value="{{ $schedulestuff->id }}">
				          	<div class="row">
    							<div class="col-md-6">
    								<div class="form-group">
						              	<label for="container">Vehicle</label>
						              	<select class="form-control select2" name="container" required>
						              		<option value="{{ old('container') }}">Choose One</option>
					                    	@foreach($containers as $container)
					                    		@if($container->size == 0)
					                    			<option value="{{ $container->id }}">{{ $container->name }}</option>
											    @else
											    	<option value="{{ $container->id }}">{{ $container->size }}" {{ $container->name }}</option>
										    	@endif
											@endforeach
					                	</select>
						          	</div>
    								<div class="form-group">
						          		<label for="vehicle">Container</label>
						              	<input type="text" class="form-control" name="vehicle" value="{{ old('vehicle') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_bag">Qty Bag</label>
						              	<input type="number" class="form-control" name="qty_bag" value="{{ old('qty_bag') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_pcs">Qty Pcs</label>
						              	<input type="number" class="form-control" name="qty_pcs" value="{{ old('qty_pcs') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_kg">Qty Kg</label>
						              	<input type="number" class="form-control" name="qty_kg" value="{{ old('qty_kg') }}" required>
						          	</div>
								</div>

								<div class="col-md-6">
						          	<div class="form-group">
						          		<label for="nopol">Nopol</label>
						              	<input type="text" class="form-control" name="nopol" value="{{ old('nopol') }}">
						          	</div>
									<div class="form-group">
						          		<label for="driver">Driver</label>
						              	<input type="text" class="form-control" name="driver" value="{{ old('driver') }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="mobile">Mobile</label>
						              	<input type="number" class="form-control" name="mobile" value="{{ old('mobile') }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="notice">Note</label>
						              	<textarea class="form-control" name="notice" rows="5" maxlength="150">{{ old('notice') }}</textarea>
						          	</div>
								</div>
							</div>
							
							<button type="submit" class="btn btn-sm btn-success">Save</button>
						</form>

		          		<br><table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="15%">Container</th>
						          	<th width="15%">Quantity</th>
						          	<th width="25%">Vehicle</th>
						          	<th width="35%">Note</th>
						          	<th width="10%" style="text-align: center;">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@foreach($stuffs as $stuff)
						    		<tr>
							            <td>
							            	@if($stuff->fkContainer->size == 0)
				                    			{{ $stuff->fkContainer->name }}
										    @else
										    	{{ $stuff->fkContainer->size }}" {{ $stuff->fkContainer->name }}
									    	@endif
							            	
							            	<br>{{$stuff->vehicle}}
							            </td>
							            <td style="text-align: right;">
							            	Qty Bag : {{$stuff->qty_bag}} &nbsp; <br>
							            	Qty Pcs : {{$stuff->qty_pcs}} &nbsp; <br>
							            	Qty Kg : {{$stuff->qty_kg}} &nbsp;&nbsp;
							            </td>
							            <td>
							            	<table>
							            		<tr>
							            			<td>Nopol</td>
							            			<td>&nbsp;: {{$stuff->nopol}}</td>
						            			</tr>
						            			<tr>
							            			<td>Driver</td>
							            			<td>&nbsp;: {{$stuff->driver}}</td>
						            			</tr>
						            			<tr>
							            			<td>Phone</td>
							            			<td>&nbsp;: {{$stuff->mobile}}</td>
						            			</tr>
						            		</table>
							            </td>
							            <td><pre>{{$stuff->notice}}</pre></td>
							            
							            @if($stuff->fkSchedulestuff->status_weigh == 1)
							            	<td><font color="green">Weighed <i class="glyphicon glyphicon-ok"></i></font></td>
							            @else
								            @if($stuff->status == 1)
									            <td>
									            	<a href="{{ route('stockrequest.edit',$stuff->id)}}" class="btn btn-xs btn-warning">Edit</a> &nbsp;
									            	
									            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$stuff->id}}">Delete</button>

									            	<form action="{{ route('stockrequest.destroy', $stuff->id)}}" method="post">
									                  	@csrf
									                  	@method('DELETE')
									                  	<div class="modal fade bs-example-modal{{$stuff->id}}" tabindex="-1" role="dialog" aria-hidden="true">
						                                    <div class="modal-dialog modal-sm">
						                                        <div class="modal-content">
						                                            <div class="modal-header btn-danger">
						                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
						                                                </button>
						                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
						                                            </div>
						                                            <div class="modal-body">
						                                                Are you sure to delete this data?
						                                            </div>
						                                            <div class="modal-footer">
						                                                <button type="submit" class="btn btn-success">Yes</button>
						                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
						                                            </div>
						                                        </div>
						                                    </div>
						                                </div>
									                </form>
									            </td>
								            @elseif($stuff->status == 0)
								            	<td>
								            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$stuff->id}}">Activated</button>

								            		<form action="{{ route('stockrequest.destroy', $stuff->id)}}" method="post">
									                  	@csrf
									                  	@method('DELETE')
									                  	<div class="modal fade bs-example-modal{{$stuff->id}}" tabindex="-1" role="dialog" aria-hidden="true">
						                                    <div class="modal-dialog modal-sm">
						                                        <div class="modal-content">
						                                            <div class="modal-header btn-success">
						                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
						                                                </button>
						                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
						                                            </div>
						                                            <div class="modal-body">
						                                                Are you sure to active this data?
						                                            </div>
						                                            <div class="modal-footer">
						                                                <button type="submit" class="btn btn-success">Yes</button>
						                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
						                                            </div>
						                                        </div>
						                                    </div>
						                                </div>
									                </form>
								            	</td>
							            	@else
							            		<td><font color="red">Canceled <i class="glyphicon glyphicon-remove"></i></font></td>
								            @endif
							            @endif
									</tr>
					    		@endforeach
						    </tbody>

						    <tfoot>
						    	<tr>
						    		<td>Container</td>
						    		<td>Quantity</td>
						    		<td>Vehicle</td>
						    		<td>Notice</td>
						    		<td class="noShow"></td>
					    		</tr>
					    	</tfoot>
					    </table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop