@extends('adminlte::page')

@section('content')
    <div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
                <div class="box box-primary">
                    <div class="box-header with-border" style="background: #6b6e70 !important; font-weight: bold; color: white;">
                        <h4 class="box-title">BANK IN  &nbsp;
                            <a href="{{ route('bankin.create')}}" class="btn btn-xs btn-primary">NEW</button>
                        </h4>
                    </div>
                    
                    <div class="box-body">
                        <table id="example" class="table table-bordered table-hover nowrap" width="100%">
                            <thead style="background: #86c232 !important; font-weight: bold; text-align: center; color: white;">
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="5%">Code</th>
                                    <th width="10%">Bank Code</th>
                                    <th width="20%">Description</th>
                                    <th width="15%">Total</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td class="noShow"></td>
                                    <td>Code</td>
                                    <td>Bank Code</td>
                                    <td>Description</td>
                                    <td>Total</td>
                                    <td class="noShow"></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                <div>
            </div>
        </div>
    </div>
@stop
