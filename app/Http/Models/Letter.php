<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Letter extends Model
{
    protected $fillable = [
	    'menu',
	    'company',
	    'code',
	    'romawi',
	    'year',
	    'created_user',
	    'updated_user'
  	];
}
