@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">New Root Menu</h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<form method="post" action="{{ route('menuroot.store') }}">
				          	@csrf
				          	<div class="row">
								<div class="col-md-6">
									<div class="form-group">
						              	<label for="nomor">Nomor</label>
						              	<input type="hidden" name="nomor" value="{{ $nomor }}">
						              	<input type="number" class="form-control" value="{{ $nomor }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="url">URL</label>
						              	<input type="text" class="form-control" name="url" value="{{ old('url') }}">
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						              	<label for="icon">Icon</label>
						              	<input type="text" class="form-control" name="icon" value="{{ old('icon') }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="icon_color">Icon Color</label>
						              	<input type="text" class="form-control" name="icon_color" value="{{ old('icon_color') }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="button">&nbsp;</label> <br>
						              	<button type="submit" class="btn btn-sm btn-success">Save</button>
						          		<a href="{{ url('/menu') }}" class="btn btn-sm btn-danger">Cancel</a>
						          	</div>
								</div>
							</div>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop