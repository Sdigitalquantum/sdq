@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Delivery Order Reroute - Shipping</h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="5%">No</th>
			          	<th width="15%">Nomor</th>
			          	<th width="20%">Split Proforma</th>
			          	<th width="15%">Quotation</th>
			          	<th width="45%">Note</th>
			          	<th width="10%">Action</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@php ($no = 0)
			    	@foreach($stufforders as $stufforder)
			    		@php ($no++)
			        	<tr>
				            <td>{{$no}}</td>
				            <td>{{$stufforder->no_letter}}</td>
				            <td>{{$stufforder->fkSchedulestuff->fkSchedulebook->fkQuotationsplit->no_split}}</td>
				            <td>{{$stufforder->fkSchedulestuff->fkSchedulebook->fkQuotationsplit->fkQuotation->no_sp}}</td>
				            <td>{{$stufforder->notice}}</td>
							
							<td>
								@if($stufforder->status_reroute == 0)
									<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$stufforder->id}}">Re-Route</button>
					                
					                <form action="{{ url('stuff/cancel', $stufforder->id)}}" method="post">
					                  	@csrf
					                  	@method('DELETE')
					                  	<div class="modal fade bs-example-modal{{$stufforder->id}}" tabindex="-1" role="dialog" aria-hidden="true">
		                                    <div class="modal-dialog modal-sm">
		                                        <div class="modal-content">
		                                            <div class="modal-header btn-danger">
		                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
		                                                </button>
		                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
		                                            </div>
		                                            <div class="modal-body">
		                                            	Are you sure to re-route this data?
		                                            	<br><textarea class="form-control" name="notice" cols="30" rows="3" maxlength="150" required></textarea>
		                                            </div>
		                                            <div class="modal-footer">
		                                                <button type="submit" class="btn btn-success">Yes</button>
		                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
					                </form>
				                @elseif($stufforder->status_reroute == 1)
				            		<font color="red">Re-Route <i class="glyphicon glyphicon-remove"></i></font>
								@endif
							</td>
				        </tr>
			        @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			          	<td class="noShow"></td>
			          	<td>Nomor</td>
			          	<td>Split Proforma</td>
			          	<td>Quotation</td>
			          	<td>Note</td>
			          	<td class="noShow"></td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	</div>
@stop