<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Purchasegeneral;
use App\Http\Models\Purchasedetailprg;
use App\Http\Models\Quotationdetail;
use App\Http\Models\Product;
use App\Http\Models\Supplierdetail;
use App\Http\Models\Letter;
use App\Http\Models\Employeeset;
use App\Http\Models\Employeewarehouse;

class PurchasegeneralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $purchasegenerals = Purchasegeneral::orderBy('created_at','desc')->get();
        $purchasedetailprgs = Purchasedetailprg::where('status', 1)->orderBy('created_at','desc')->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/purchasegeneral.index', compact('purchasegenerals','purchasedetailprgs','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        return view('vendor/adminlte/purchasegeneral.create', compact('disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        $no = Purchasegeneral::select('no_inc')->whereYear('date_order', date('Y'))->max('no_inc');
        if (empty($no)) {
            $no_inc = 1;
        } else {
            $no_inc = $no + 1;
        }

        $letter = Letter::where('menu', 'Purchase General')->first();
        if (empty($letter->company)) {
            $company = "";
        } else {
            $company = $letter->company."/";
        }

        if (empty($letter->code)) {
            $code = "";
        } else {
            $code = $letter->code;
        }

        if ($letter->romawi == 0) {
            $romawi = "";
        } else {
            if (date("m") == 1) {
                $romawi = "I/";
            } elseif (date("m") == 2) {
                $romawi = "II/";
            } elseif (date("m") == 3) {
                $romawi = "III/";
            } elseif (date("m") == 4) {
                $romawi = "IV/";
            } elseif (date("m") == 5) {
                $romawi = "V/";
            } elseif (date("m") == 6) {
                $romawi = "VI/";
            } elseif (date("m") == 7) {
                $romawi = "VII/";
            } elseif (date("m") == 8) {
                $romawi = "VIII/";
            } elseif (date("m") == 9) {
                $romawi = "IX/";
            } elseif (date("m") == 10) {
                $romawi = "X/";
            } elseif (date("m") == 11) {
                $romawi = "XI/";
            } elseif (date("m") == 12) {
                $romawi = "XII/";
            }
        }

        if ($letter->year == 0) {
            $year = "";
        } else {
            $year = date("Y");
        }

        $no_prg = $company.$code.sprintf("%05s", $no_inc)."/".$romawi.$year;

        $purchasegeneral = new Purchasegeneral([
            'no_inc'            => $no_inc,
            'no_prg'            => $no_prg,
            'date_order'        => $request->get('date_order'),
            'notice'            => !empty($request->get('notice')) ? $request->get('notice') : '',
            'reason'            => '',
            'created_user'      => $session_id,
            'updated_user'      => $session_id
        ]);
        $purchasegeneral->save();

        return redirect('/purchasedetailprg/create/'.$purchasegeneral->id)->with('success', 'Purchase General has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $purchasegeneral = Purchasegeneral::find($id);

        return view('vendor/adminlte/purchasegeneral.edit', compact('purchasegeneral','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $purchasegeneral = Purchasegeneral::find($id);
        $purchasegeneral->date_order        = $request->get('date_order');
        $purchasegeneral->notice            = !empty($request->get('notice')) ? $request->get('notice') : '';
        $purchasegeneral->updated_user      = $session_id;
        $purchasegeneral->save();

        return redirect('/purchasegeneral')->with('success', 'Purchase General has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $purchasegeneral = Purchasegeneral::find($id);
        $status = $purchasegeneral->status;

        if ($status == 1) {
            $purchasegeneral->status       = 0;
            $purchasegeneral->updated_user = $session_id;
            $purchasegeneral->save();

            return redirect('/purchasegeneral')->with('error', 'Purchase General has been deleted');
        } elseif ($status == 0) {
            $purchasegeneral->status       = 1;
            $purchasegeneral->updated_user = $session_id;
            $purchasegeneral->save();

            return redirect('/purchasegeneral')->with('success', 'Purchase General has been activated');
        }
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function set(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $purchasegeneral = Purchasegeneral::find($id);
        $approve = $purchasegeneral->approve;

        if ($approve == 1) {
            $purchasegeneral->approve           = 0;
            $purchasegeneral->status_approve    = 1;
            $purchasegeneral->reason            = 'Set None Approval';
            $purchasegeneral->updated_user      = $session_id;
            $purchasegeneral->save();

            return redirect('/purchasegeneral')->with('error', 'Purchase General has been set none approval');
        } elseif ($approve == 0) {
            $purchasegeneral->approve           = 1;
            $purchasegeneral->status_approve    = 0;
            $purchasegeneral->reason            = '';
            $purchasegeneral->updated_user      = $session_id;
            $purchasegeneral->save();

            return redirect('/purchasegeneral')->with('success', 'Purchase General has been set used approval');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function approval(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $purchasegenerals = Purchasegeneral::where([['status', 1], ['approve', 1], ['status_release', 1]])->orderBy('created_at','desc')->get();
        $purchasedetailprgs = Purchasedetailprg::where('status', 1)->orderBy('created_at','desc')->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/purchasegeneral.approval', compact('purchasegenerals','purchasedetailprgs','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }
    
    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $purchasegeneral = Purchasegeneral::find($id);
        $purchasegeneral->status_approve    = 1;
        $purchasegeneral->reason            = $request->get('reason');
        $purchasegeneral->approved_at       = new \DateTime();
        $purchasegeneral->approved_user     = $session_id;
        $purchasegeneral->save();

        return redirect('/approvalprg')->with('success', 'Purchase General has been approved');
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reject(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $purchasegeneral = Purchasegeneral::find($id);
        $purchasegeneral->status_approve    = 2;
        $purchasegeneral->reason            = $request->get('reason');
        $purchasegeneral->approved_at       = new \DateTime();
        $purchasegeneral->approved_user     = $session_id;
        $purchasegeneral->save();

        return redirect('/approvalprg')->with('error', 'Purchase General has been rejected');
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function release(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $purchasegeneral = Purchasegeneral::find($id);
        $purchasegeneral->status_release  = 1;
        $purchasegeneral->updated_user    = $session_id;
        $purchasegeneral->save();

        return redirect('/purchasegeneral')->with('success', 'Purchase General has been released');
    }
}
