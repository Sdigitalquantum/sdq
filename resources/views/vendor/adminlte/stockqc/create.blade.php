@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_qc" data-toggle="tab">Quality Control</a></li>
            <li class="disabled"><a>Detail</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_qc">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Quality Control &nbsp;<a href="{{ url('stockqc') }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('stockqc.store') }}">
				          	@csrf
				          	<div class="row">
    							<div class="col-md-6">
    								<div class="form-group">
						              	<label for="no_po">No. PO</label>
						              	<input type="hidden" name="purchasedetail" value="{{ $purchasedetail->id }}">
						              	<input type="text" class="form-control" name="no_po" value="{{ $purchasedetail->fkPurchaseorder->no_po }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="qty_po">Qty (Kg)</label>
						              	<input type="text" class="form-control" value="PO : {{ $purchasedetail->qty_order+$purchasedetail->qty_buffer }} QC : {{ $purchasedetail->qty_qc }}" disabled>
						          	</div>
									<div class="form-group">
						              	<label for="product">Product</label>
						              	<select class="form-control select2" name="product" required>
						              		<option value="{{ old('product') }}">Choose One</option>
					                    	@foreach($products as $product)
											    <option value="{{ $product->id }}">{{ $product->code }} - {{ $product->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="warehouse">Warehouse</label>
						              	<select class="form-control select2" name="warehouse" required>
						              		@php ($array = [])
									    	@foreach($employeewarehouses as $employeewarehouse)
									    		@php ($array[] = $employeewarehouse->warehouse)
									    	@endforeach

						              		<option value="{{ old('warehouse') }}">Choose One</option>
					                    	@foreach($warehouses as $warehouse)
					                    		@if(in_array($warehouse->id, $array))
											    	<option value="{{ $warehouse->id }}">{{ $warehouse->code }} - {{ $warehouse->name }}</option>
										    	@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="supplierdetail">Supplier</label>
						              	<input type="hidden" name="supplierdetail" value="{{ $supplierdetail->id }}">
						              	<input type="text" class="form-control" value="{{ $supplierdetail->name }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="date_in">Date</label>
						              	<input type="text" class="form-control datepicker" name="date_in" value="{{ old('date_in') }}" required>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						              	<label for="qty_bag">Qty Bag</label>
						              	<input type="number" class="form-control" name="qty_bag" value="{{ old('qty_bag') }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="qty_pcs">Qty Pcs</label>
						              	<input type="number" class="form-control" name="qty_pcs" value="{{ old('qty_pcs') }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="qty_kg">Qty Kg</label>
						              	<input type="number" class="form-control" name="qty_kg" value="{{ old('qty_kg') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="notice">Note</label>
						          		<textarea class="form-control" name="notice" rows="5" maxlength="150"></textarea>
						          	</div>
						          	<div class="form-group">
						          		<label for="button"></label> <br>
						          		<button type="submit" class="btn btn-sm btn-success">Save</button>
						          	</div>
								</div>
							</div>
				      	</form>

				      	<br><table class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #001F3F !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="30%">Detail PO</th>
						          	<th width="20%">Information</th>
						          	<th width="15%">Quantity</th>
						          	<th width="20%">Note</th>
						          	<th width="10%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($tot_bag = 0)
						    	@php ($tot_pcs = 0)
						    	@php ($tot_kg = 0)
						    	@php ($tot_avg = 0)
						    	@foreach($stockqcs as $stockqc)
						    		<tr>
							            <td>
							            	Product &nbsp;: {{$stockqc->fkProduct->name}} <br>
							            	Supplier : {{$stockqc->fkSupplierdetail->name}} <br>
						            		Warehouse : {{$stockqc->fkWarehouse->name}}
						            	</td>
							            <td>
							            	No. PO : {{$stockqc->no_po}} <br>
							            	Date : {{$stockqc->date_in}}
						            	</td>
							            <td style="text-align: right;">
							            	Bag : {{number_format($stockqc->qty_bag, 0, ',' , '.')}} <br>
							            	Pcs : {{number_format($stockqc->qty_pcs, 0, ',' , '.')}} <br>
							            	Kg : {{number_format($stockqc->qty_kg, 0, ',' , '.')}} <br>

							            	@if($stockqc->status == 1)
							            		<a href="{{ url('stockdetail/create',$stockqc->id)}}" class="btn btn-xs btn-warning">Detail</a>&nbsp;
						            		@endif

							            	@if($stockqc->detail_count == 0)
							            		Avg : {{number_format($stockqc->qty_avg, 0, ',' , '.')}}&nbsp;
							            		@php ($tot_avg = $tot_avg + $stockqc->qty_avg)
						            		@else
						            			Avg : {{round($stockqc->detail_total/$stockqc->detail_count,2)}}&nbsp;
						            			@php ($tot_avg = $tot_avg + ($stockqc->detail_total/$stockqc->detail_count))
						            		@endif
							            </td>
						            	<td>
							            	<pre>{{$stockqc->notice}}</pre>

							            	@if($stockqc->no_inc != 0 && $stockqc->status_approve == 1)
							            		<br>Reason : <pre>{{$stockqc->reason}}</pre>
							            	@endif
							            </td>

							            @if($stockqc->status == 1)
								            <td>
								            	@if($stockqc->no_inc == 0)
									            	<a href="{{ route('stockqc.edit',$stockqc->id)}}" class="btn btn-xs btn-warning">Edit</a>&nbsp;

									            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$stockqc->id}}">Delete</button></a>

								            		<form action="{{ route('stockqc.destroy', $stockqc->id)}}" method="post">
									                  	@csrf
									                  	@method('DELETE')
									                  	<div class="modal fade bs-example-modal{{$stockqc->id}}" tabindex="-1" role="dialog" aria-hidden="true">
						                                    <div class="modal-dialog modal-sm">
						                                        <div class="modal-content">
						                                            <div class="modal-header btn-danger">
						                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
						                                                </button>
						                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
						                                            </div>
						                                            <div class="modal-body">
						                                                Are you sure to delete this data?
						                                            </div>
						                                            <div class="modal-footer">
						                                                <button type="submit" class="btn btn-success">Yes</button>
						                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
						                                            </div>
						                                        </div>
						                                    </div>
						                                </div>
									                </form>
								                @endif

								                @if($stockqc->no_inc != 0 && $stockqc->status_release == 0)
								                	<a href="{{ route('stockqc.edit',$stockqc->id)}}" class="btn btn-xs btn-warning">Edit</a>&nbsp;

									            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$stockqc->id}}">Delete</button></a>

								            		<form action="{{ route('stockqc.destroy', $stockqc->id)}}" method="post">
									                  	@csrf
									                  	@method('DELETE')
									                  	<div class="modal fade bs-example-modal{{$stockqc->id}}" tabindex="-1" role="dialog" aria-hidden="true">
						                                    <div class="modal-dialog modal-sm">
						                                        <div class="modal-content">
						                                            <div class="modal-header btn-danger">
						                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
						                                                </button>
						                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
						                                            </div>
						                                            <div class="modal-body">
						                                                Are you sure to delete this data?
						                                            </div>
						                                            <div class="modal-footer">
						                                                <button type="submit" class="btn btn-success">Yes</button>
						                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
						                                            </div>
						                                        </div>
						                                    </div>
						                                </div>
									                </form>

								                	<br><font color="blue">Waiting Release</font>
								                @elseif($stockqc->no_inc != 0 && $stockqc->status_approve == 1)
								                	<font color="green">Derivative Approved <i class="glyphicon glyphicon-ok"></i></font>
								                @elseif($stockqc->no_inc != 0 && $stockqc->status_approve == 2)
								                	<font color="red">Derivative Rejected <i class="glyphicon glyphicon-remove"></i></font>
								                @endif
								            </td>
							            @else
							            	<td>
							            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$stockqc->id}}">Activated</button></a>

							            		<form action="{{ route('stockqc.destroy', $stockqc->id)}}" method="post">
								                  	@csrf
								                  	@method('DELETE')
								                  	<div class="modal fade bs-example-modal{{$stockqc->id}}" tabindex="-1" role="dialog" aria-hidden="true">
					                                    <div class="modal-dialog modal-sm">
					                                        <div class="modal-content">
					                                            <div class="modal-header btn-success">
					                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
					                                                </button>
					                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
					                                            </div>
					                                            <div class="modal-body">
					                                                Are you sure to active this data?
					                                            </div>
					                                            <div class="modal-footer">
					                                                <button type="submit" class="btn btn-success">Yes</button>
					                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
					                                            </div>
					                                        </div>
					                                    </div>
					                                </div>
								                </form>
							            	</td>
							            @endif
							        </tr>

							        @php ($tot_bag = $tot_bag + $stockqc->qty_bag)
							    	@php ($tot_pcs = $tot_pcs + $stockqc->qty_pcs)
							    	@php ($tot_kg = $tot_kg + $stockqc->qty_kg)
						        @endforeach
						    </tbody>
						    	@if($tot_bag != 0)
							    	<tr>
							    		<td style="text-align: right; font-weight: bold;" colspan="2">Total Qty &nbsp;</td>
							    		<td style="text-align: right;">
							    			Bag : {{number_format($tot_bag, 0, ',' , '.')}} <br>
							            	Pcs : {{number_format($tot_pcs, 0, ',' , '.')}} <br>
							            	Kg : {{number_format($tot_kg, 0, ',' , '.')}} <br>
							            	Avg : {{round($tot_avg,2)}} <br>
							    		</td>
						    			<td colspan="2">&nbsp;</td>
						    		</tr>
					    		@endif
					  	</table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop