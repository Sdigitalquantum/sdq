<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Purchasedetailpr extends Model
{
    protected $fillable = [
	    'purchasedetail',
	    'purchaserequest',
	    'purchasedetailprg',
	    'qty',
	    'created_user'
  	];

  	public function fkPurchasedetail(){
	    return $this->belongsTo('\App\Http\Models\Purchasedetail', 'purchasedetail', 'id');
	}

	public function fkPurchaserequest(){
	    return $this->belongsTo('\App\Http\Models\Purchaserequest', 'purchaserequest', 'id');
	}

	public function fkPurchasedetailprg(){
	    return $this->belongsTo('\App\Http\Models\Purchasedetailprg', 'purchasedetailprg', 'id');
	}
}
