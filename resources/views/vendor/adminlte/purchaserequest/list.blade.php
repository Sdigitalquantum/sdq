@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li><a href="{{ url('purchaseorder') }}">List</a></li>
          	<li class="active"><a href="#tab_request" data-toggle="tab">Request</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_request">
                <div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Purchase Request</h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="15%">No. PR</th>
						          	<th width="10%">Date</th>
						          	<th width="20%">Product</th>
						          	<th width="20%">Supplier</th>
						          	<th width="15%">No. Reference</th>
						          	<th width="10%">Qty (Kg)</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($purchaserequests as $purchaserequest)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>{{$purchaserequest->no_pr}}</td>
							            <td>{{$purchaserequest->date_request}}</td>
							            <td>
							            	@if(empty($purchaserequest->fkQuotationdetail->alias_name))
							            		{{$purchaserequest->fkProduct->name}}
						            		@else
						            			{{$purchaserequest->fkQuotationdetail->alias_name}}
						            		@endif
						            	</td>
							            <td>
							            	@if($purchaserequest->supplierdetail == 0)
							            		All Supplier
							            	@else
							            		{{$purchaserequest->fkSupplierdetail->name}}
						            		@endif
							            </td>
							            <td>{{$purchaserequest->no_reference}}</td>
							            <td style="text-align: right;">{{$purchaserequest->qty_request}} &nbsp;</td>
							            
							            <td>
							            	@if($purchaserequest->status_order == 0)
	            								<a href="{{ url('purchaseorder/list',$purchaserequest->id)}}" class="btn btn-xs btn-success">Choose</a>
            								@else
            									<font color="green">PO <i class="glyphicon glyphicon-ok"></i></font>
            								@endif
	            						</td>
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>No. PR</td>
						          	<td>Date</td>
						          	<td>Product</td>
						          	<td>Supplier</td>
						          	<td>No. Reference</td>
						          	<td>Qty (Kg)</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop