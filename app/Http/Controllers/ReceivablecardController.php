<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Exports\ReceivablecardsExport;
use App\Http\Models\Receivablecard;
use App\Http\Models\Customerdetail;
use App\Http\Models\Employeeset;

class ReceivablecardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $receivablecards = Receivablecard::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/receivablecard.index', compact('receivablecards','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $customerdetails = Customerdetail::where('status', 1)->get();

        return view('vendor/adminlte/receivablecard.create', compact('customerdetails','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cek = Receivablecard::where([['customerdetail', $request->get('customerdetail')], ['year', $request->get('year')], ['month', $request->get('month')]])->first();

        $request->validate([
            'year'   => 'numeric|min:0|not_in:0',
            'debit'  => 'numeric|min:0|not_in:0',
            'kredit' => 'numeric|min:0'
        ]);

        if (!empty($cek)) {
            return redirect()->back()->withErrors('Cut Off already exist');
        } else {
            $receivablecard = new Receivablecard([
                'customerdetail'    => $request->get('customerdetail'),
                'year'              => $request->get('year'),
                'month'             => $request->get('month'),
                'debit'             => $request->get('debit'),
                'kredit'            => $request->get('kredit')
            ]);
            $receivablecard->save();

            return redirect('/receivablecutoff')->with('success', 'Cut Off has been added');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export() 
    {
        return Excel::download(new ReceivablecardsExport, 'ReceivableCard.xlsx');
    }
}
