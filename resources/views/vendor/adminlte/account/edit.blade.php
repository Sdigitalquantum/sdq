@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #6b6e70 !important; font-weight: bold; color: white;">
			    		<h4 class="box-title">Edit account</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form id="accountFrm">
				      		@method('PATCH')
				      		@csrf
				          	<div class="row">
    							<div class="col-md-6">
    								<div class="form-group">
						              	<label for="code">Code</label>
						              	<input type="number" class="form-control" name="code" value="{{ $account->code }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ $account->name }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="group">Group Header</label>
						              	<select class="form-control select2" name="group" required>
						              		@foreach($groups as $group)
											    @if ($account->group == $group->id)
												    <option value="{{ $group->id }}" selected>{{ $group->code }} - {{ $group->name }}</option>
												@else
												    <option value="{{ $group->id }}">{{ $group->code }} - {{ $group->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="type">Account Type</label>
						              	<select class="form-control select2" name="type" required>
						              		@foreach($types as $type)
											    @if ($account->type == $type->id)
												    <option value="{{ $type->id }}" selected>{{ $type->code }} - {{ $type->name }}</option>
												@else
												    <option value="{{ $type->id }}">{{ $type->code }} - {{ $type->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="currency">Currency</label>
						              	<select class="form-control select2" name="currency" required>
						              		@foreach($currencys as $currency)
											    @if ($account->currency == $currency->id)
												    <option value="{{ $currency->id }}" selected>{{ $currency->code }} - {{ $currency->name }}</option>
												@else
												    <option value="{{ $currency->id }}">{{ $currency->code }} - {{ $currency->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						              	<label for="level1">Level 1</label>
						              	<input type="number" class="form-control" name="level1" value="{{ $account->level1 }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="level2">Level 2</label>
						              	<input type="number" class="form-control" name="level2" value="{{ $account->level2 }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="level3">Level 3</label>
						              	<input type="number" class="form-control" name="level3" value="{{ $account->level3 }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="level4">Level 4</label>
						              	<input type="number" class="form-control" name="level4" value="{{ $account->level4 }}">
						          	</div>
						          	<div class="form-group">
						              	<button id="updateBtn" value="{{ route('account.update', $account->id) }}" class="btn btn-sm btn-success">Update</button>
				          				<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
						          	</div>
								</div>
							</div>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.account.prepare_edit());
    </script>
@stop