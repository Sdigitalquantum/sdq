@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Menu</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('menu.update', $menu->id) }}">
				      		@method('PATCH')
				      		@csrf
				          	<div class="row">
								<div class="col-md-6">
									<div class="form-group">
						          		<label for="root">Root Menu</label>
						              	<select name="menuroot" class="form-control select2" required>
						              		@foreach($menuroots as $menuroot)
						              			@if ($menuroot->id == $menu->menuroot)
												    <option value="{{ $menuroot->id }}" selected>{{ $menuroot->name }}</option>
												@else
												    <option value="{{ $menuroot->id }}">{{ $menuroot->name }}</option>
												@endif
											@endforeach
										</select>
						          	</div>
									<div class="form-group">
						              	<label for="nomor">Nomor</label>
						              	<input type="number" class="form-control" name="nomor" value="{{ $menu->nomor }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ $menu->name }}" required>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						              	<label for="url">URL</label>
						              	<input type="text" class="form-control" name="url" value="{{ $menu->url }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="icon_color">Icon Color</label>
						              	<input type="text" class="form-control" name="icon_color" value="{{ $menu->icon_color }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="button">&nbsp;</label> <br>
						              	<button type="submit" class="btn btn-sm btn-success">Update</button>
						          		<a href="{{ url('/menu/create/'.$menu->menuroot) }}" class="btn btn-sm btn-danger">Cancel</a>
						          	</div>
								</div>
							</div>
				      	</form>

				      	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="25%">Name</th>
						          	<th width="25%">URL</th>
						          	<th width="20%">Icon</th>
						          	<th width="20%">Icon Color</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@foreach($menus as $menu)
						    		<tr>
							            <td>{{$menu->nomor}}</td>
							            <td>{{$menu->name}}</td>
							            <td>{{$menu->url}}</td>
							            <td>{{$menu->icon}}</td>
							            <td>{{$menu->icon_color}}</td>

							            @if($menu->status == 1)
								            <td>
								            	<a href="{{ route('menu.edit',$menu->id)}}" class="btn btn-xs btn-warning">Edit</a>
								            </td>
							            @else
							            	<td>
							            		<font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
							            	</td>
							            @endif
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Name</td>
						          	<td>URL</td>
						          	<td>Icon</td>
						          	<td>Icon Color</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop