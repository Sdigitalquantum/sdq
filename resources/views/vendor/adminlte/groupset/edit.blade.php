@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Menu</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Menu &nbsp;<a href="{{ url('groupset')}}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<div class="row">
							<div class="col-md-6">
								<table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="10%" style="text-align: center;">No</th>
								          	<th width="50%">Root Menu</th>
								          	<th width="40%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@php($no = 0)
								    	@foreach($menuroots as $menuroot)
								    		@php($no++)
								        	<tr>
								        		<td style="text-align: center;">{{$no}}</td>
									            <td>
									            	<i class="fa fa-fw fa-{{$menuroot->icon}} text-{{$menuroot->icon_color}}"></i> {{$menuroot->name}}
									            </td>

									            <td>
									            	<form method="post" action="{{ route('groupset.update', $group->id) }}">
											      		@method('PATCH')
											      		@csrf
											      		<input type="hidden" name="group" value="{{$group->id}}">
											      		<input type="hidden" name="menuroot" value="{{$menuroot->id}}">
											          	<button type="submit" class="btn btn-xs btn-success">Choose</button>
											      	</form>
									            </td>
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
							</div>

							<div class="col-md-6">
								<table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #00a65a !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="80%">Access Menu</th>
								          	<th width="20%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($groupsets as $groupset)
								    		@php($no++)
								        	<tr>
								        		<td>
									            	<i class="fa fa-fw fa-{{$groupset->fkMenuroot->icon}} text-{{$groupset->fkMenuroot->icon_color}}"></i>
									            	{{$groupset->fkMenuroot->name}} <i class="glyphicon glyphicon-ok"></i>
									            </td>

									            @if($groupset->status == 1)
										            <td>
										            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$groupset->id}}">Delete</button>

										            	<form action="{{ route('groupset.destroy', $groupset->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$groupset->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-danger">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to delete this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
										            </td>
									            @else
									            	<td>
									            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$groupset->id}}">Activated</button>

									            		<form action="{{ route('groupset.destroy', $groupset->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$groupset->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-success">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to active this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
									            	</td>
									            @endif
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
							</div>
						</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop