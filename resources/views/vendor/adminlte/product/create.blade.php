@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Alias</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">New Product</h4>
			    	</div>
			    	
				  	<div class="box-body">
					  <form id="productFrm">
					  @method('POST')
			          	@csrf
				          	<div class="row">
				          		<div class="col-md-6">
				          			<div class="form-group">
						              	<label for="account">Accounting</label>
						              	<select class="form-control select2" name="account" required>
						              		<option value="{{ old('account') }}">Choose One</option>
					                    	@foreach($accounts as $account)
											    <option value="{{ $account->id }}">{{ $account->code }} - {{ $account->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="code">Code</label>
						              	<input type="text" class="form-control" name="code" value="{{ old('code') }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="department">Department</label>
						              	<select class="form-control select2" name="department" required>
						              		<option value="{{ old('department') }}">Choose One</option>
					                    	@foreach($departments as $department)
											    <option value="{{ $department->id }}">{{ $department->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="variant">Variant</label>
						              	<select class="form-control select2" name="variant">
						              		<option value="{{ old('variant') }}">Choose One</option>
					                    	@foreach($variants as $variant)
											    <option value="{{ $variant->id }}">{{ $variant->name }}</option>
											@endforeach
					                	</select>
						          	</div>
			          			</div>

			          			<div class="col-md-6">
		          					<div class="form-group">
						              	<label for="merk">Merk</label>
						              	<select class="form-control select2" name="merk">
						              		<option value="{{ old('merk') }}">Choose One</option>
					                    	@foreach($merks as $merk)
											    <option value="{{ $merk->id }}">{{ $merk->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="typesub">Type Sub</label>
						              	<select class="form-control select2" name="typesub">
						              		<option value="{{ old('typesub') }}">Choose One</option>
					                    	@foreach($typesubs as $typesub)
											    <option value="{{ $typesub->id }}">{{ $typesub->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="size">Size</label>
						              	<select class="form-control select2" name="size">
						              		<option value="{{ old('size') }}">Choose One</option>
					                    	@foreach($sizes as $size)
											    <option value="{{ $size->id }}">{{ $size->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="unit">Unit</label>
						              	<select class="form-control select2" name="unit" required>
						              		<option value="{{ old('unit') }}">Choose One</option>
					                    	@foreach($units as $unit)
											    <option value="{{ $unit->id }}">{{ $unit->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="check">&nbsp;</label>
						          		<input type="checkbox" name="general" value="1"> General &nbsp;&nbsp;&nbsp;
						              	<input type="checkbox" name="status_group" value="1"> Status Group &nbsp;&nbsp;&nbsp;
						              	<input type="checkbox" name="status_record" value="1"> Status Record <br>
									</div>
									<div class="form-group">
										  <button id="saveBtn" value="{{ route('product.store') }}" class="btn btn-sm btn-success">Save</button>
										  <button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
						          		<!-- <button type="submit" class="btn btn-sm btn-success">Save</button>
						          		<a href="{{ url('/product') }}" class="btn btn-sm btn-danger">Cancel</a> -->
					          		</div>
			          			</div>
		          			</div>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.product.prepare_create());
    </script>
@stop
