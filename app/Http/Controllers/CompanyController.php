<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Company;
use App\Http\Models\Companyaccount;
use App\Http\Models\City;
use App\Http\Models\Employeeset;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $companies = Company::orderBy('created_at','desc')->get();
        $company = Company::first();

        return view('vendor/adminlte/company.index', compact('companies','company','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/company.create', compact('citys','disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            if (!empty($json->email)) {
                $request->validate([
                    'name'  => 'unique:companies',
                    'email' => 'email:true'
                ]);
            } else {
                $request->validate([
                    'name'  => 'unique:companies'
                ]);
            }
            
            $company = new Company([
                'city'          => $json->city,
                'code'          => $json->code,
                'name'          => $json->name,
                'address'       => $json->address,
                'email'         => $json->email,
                'mobile'        => $json->mobile,
                'fax'           => $json->fax,
                'created_user'  => $session_id,
                'updated_user'  => $session_id
            ]);
            $result = $company->save();
        };
        return $this->jsonSuccess( $result );

    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $company = Company::find($id);
        $companyaccounts = Companyaccount::where('company', $id)->get();
        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/company.show', compact('id','company','companyaccounts','citys','disable'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $company = Company::find($id);
        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/company.edit', compact('id','company','citys','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $company = Company::find($id);

                if ($company->name != $json->name) {
                    if (!empty($json->email)) {
                        $request->validate([
                            'name'  => 'unique:companies',
                            'email' => 'email:true'
                        ]);
                    } else {
                        $request->validate([
                            'name'  => 'unique:companies'
                        ]);
                    }
                } else {
                    if (!empty($json->email)) {
                        $request->validate([
                            'email' => 'email:true'
                        ]);
                    }
                }

                $company->city          = $json->city;
                $company->code          = $json->code;
                $company->name          = $json->name;
                $company->address       = $json->address;
                $company->email         = $json->email;
                $company->mobile        = $json->mobile;
                $company->fax           = $json->fax;
                $company->updated_user  = $session_id;
                $result = $company->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $company = Company::find($id);
        $status = $company->status;

        if ($status == 1) {
        	$company->status       = 0;
            $company->updated_user = $session_id;
	        $result = $company->save();

        } elseif ($status == 0) {
        	$company->status       = 1;
            $company->updated_user = $session_id;
	        $result = $company->save();

        };
        return $this->jsonSuccess( $result );
    }
}
