<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockqcsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stockqcs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchasedetail')->unsigned();
            $table->integer('product')->unsigned();
            $table->integer('warehouse')->unsigned();
            $table->integer('supplierdetail')->unsigned();
            $table->integer('no_inc');
            $table->string('no_po');
            $table->date('date_in');
            $table->integer('qty_bag');
            $table->integer('qty_pcs');
            $table->integer('qty_kg');
            $table->integer('qty_avg');
            $table->integer('price');
            $table->text('notice')->nullable();
            $table->text('reason')->nullable();
            $table->integer('detail_count')->default('0');
            $table->integer('detail_total')->default('0');
            $table->tinyInteger('approve')->default('1');
            $table->tinyInteger('status_approve')->default('0');
            $table->tinyInteger('status_validasi')->default('0');
            $table->tinyInteger('status_release')->default('0');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->timestamp('approved_at')->nullable();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->integer('approved_user')->nullable();
            $table->foreign('purchasedetail')->references('id')->on('purchasedetails');
            $table->foreign('product')->references('id')->on('products');
            $table->foreign('warehouse')->references('id')->on('warehouses');
            $table->foreign('supplierdetail')->references('id')->on('supplierdetails');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stockqcs');
    }
}
