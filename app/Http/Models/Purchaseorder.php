<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Purchaseorder extends Model
{
    protected $fillable = [
	    'no_inc',
	    'no_po',
	    'date_order',
	    'tax',
	    'notice',
	    'reason',
	    'created_user',
	    'updated_user',
	    'approved_at',
	    'approved_user'
  	];
}
