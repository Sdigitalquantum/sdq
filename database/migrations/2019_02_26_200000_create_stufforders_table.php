<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStuffordersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stufforders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('schedulestuff');
            $table->integer('quotationsplit');
            $table->integer('no_inc');
            $table->string('no_letter');
            $table->text('check_quality');
            $table->integer('check_weigh');
            $table->text('notice')->nullable();
            $table->integer('qty_print')->default('0');
            $table->tinyInteger('status_confirm')->default('0');
            $table->tinyInteger('status_reroute')->default('0');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->integer('created_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stufforders');
    }
}
