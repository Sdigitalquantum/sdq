@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="active"><a href="#tab_alias" data-toggle="tab">Alias</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_alias">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Product Alias</h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<div class="row">
				  			<div class="col-md-3">
				  				<form method="post" action="{{ route('productalias.update', $productalias->id) }}">
						      		@method('PATCH')
						      		@csrf
						      		<input type="hidden" class="form-control" name="product" value="{{ $productalias->product }}">
						      		<div class="form-group">
						              	<label for="customerdetail">Customer</label>
						              	<select class="form-control select2" name="customerdetail" required>
						              		@foreach($customerdetails as $customerdetail)
											    @if ($productalias->customerdetail == $customerdetail->id)
												    <option value="{{ $customerdetail->id }}" selected>{{ $customerdetail->name }}</option>
												@else
												    <option value="{{ $customerdetail->id }}">{{ $customerdetail->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="alias_name">Alias Name</label>
						              	<input type="text" class="form-control" name="alias_name" value="{{ $productalias->alias_name }}" required>
						          	</div>
						          	<button type="submit" class="btn btn-sm btn-success">Update</button>
					          		<a href="{{ url('/productalias/'.$productalias->product) }}" class="btn btn-sm btn-danger">Cancel</a>
						      	</form>
			  				</div>

			  				<div class="col-md-9">
			  					<br><table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="30%">Customer</th>
								          	<th width="30%">Alias</th>
								          	<th width="20%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($productaliass as $productalias)
								    		<tr>
									            <td>{{$productalias->fkCustomerdetail->name}}</td>
									            <td>{{$productalias->alias_name}}</td>
									            <td>
									            	@if($productalias->status == 1) <a href="{{ route('productalias.edit',$productalias->id)}}" class="btn btn-xs btn-warning">Edit</a>
									            	@else <font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
									            	@endif
									            </td>
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
			  				</div>
		  				</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop