@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Monitoring Finance - Debt</h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="5%">No</th>
			          	<th width="20%">Supplier</th>
			          	<th width="20%">No. PO</th>
			          	<th width="10%">Date PO</th>
			          	<th width="15%">Total</th>
			          	<th width="15%">Payment</th>
			          	<th width="15%">Saldo</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@php ($no = 0)
			    	@foreach($debts as $debt)
			    		@php ($no++)
			        	<tr>
				            <td>{{$no}}</td>
				            <td>{{$debt->name_supplier}}</td>
				            <td>{{$debt->no_po}}</td>
				            <td>{{$debt->date_po}}</td>
				            <td style="text-align: right;">{{number_format($debt->total, 0, ',' , '.')}} &nbsp;</td>
				            <td style="text-align: right;">{{number_format($debt->payment, 0, ',' , '.')}} &nbsp;</td>
				            <td style="text-align: right;">{{number_format($debt->total-$debt->payment, 0, ',' , '.')}} &nbsp;</td>
				        </tr>
			        @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			          	<td class="noShow"></td>
			          	<td>Supplier</td>
			          	<td>No. PO</td>
			          	<td>Date PO</td>
			          	<td>Total</td>
			          	<td>Payment</td>
			          	<td>Saldo</td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	<div>
@stop