@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #6b6e70 !important; font-weight: bold; color: white;">
			    		<h4 class="box-title">New Kurs</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form id="kursFrm">
				          	@csrf
				          	@method('POST')
				          	<div class="form-group">
				              	<label for="currency">Currency</label>
				              	<select class="form-control select2" name="currency" required>
				              		<option value="{{ old('currency') }}">Choose One</option>
			                    	@foreach($currencies as $currency)
									    <option value="{{ $currency->id }}">{{ $currency->code }} - {{ $currency->name }}</option>
									@endforeach
			                	</select>
				          	</div>
				          	<div class="form-group">
				              	<label for="value">Value</label>
				              	<input type="number" class="form-control" name="value" value="{{ old('value') }}" required>
				          	</div>
				          	<div class="form-group">
				          		<label for="date_start">Start</label>
				              	<input type="text" class="form-control datepicker" name="start" value="{{ old('start') }}" required>
				          	</div>
				          	<div class="form-group">
				          		<label for="date_end">End</label>
				              	<input type="text" class="form-control datepicker" name="end" value="{{ old('end') }}" required>
				          	</div>
				          	<button id="saveBtn" value="{{ route('kurs.store') }}" class="btn btn-sm btn-success">Save</button>
				          	<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.kurs.prepare_create());
    </script>
@stop