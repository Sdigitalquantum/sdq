@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="active"><a href="#tab_detail" data-toggle="tab">Account</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_detail">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Company Account</h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<form method="post" action="{{ route('companyaccount.update', $companyaccount->id) }}">
				      		@method('PATCH')
				      		@csrf
				      		<input type="hidden" class="form-control" name="company" value="{{ $companyaccount->company }}">
					  		<div class="row">
					  			<div class="col-md-6">
						      		<div class="form-group">
						              	<label for="currency">Currency</label>
						              	<select class="form-control select2" name="currency" required>
						              		@foreach($currencys as $currency)
											    @if ($companyaccount->currency == $currency->id)
												    <option value="{{ $currency->id }}" selected>{{ $currency->code }} - {{ $currency->name }}</option>
												@else
												    <option value="{{ $currency->id }}">{{ $currency->code }} - {{ $currency->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="bank">Bank</label>
						              	<select class="form-control select2" name="bank" required>
						              		@foreach($banks as $bank)
											    @if ($companyaccount->bank == $bank->id)
												    <option value="{{ $bank->id }}" selected>{{ $bank->alias }} - {{ $bank->name }}</option>
												@else
												    <option value="{{ $bank->id }}">{{ $bank->alias }} - {{ $bank->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="account_no">No. Rek</label>
						              	<input type="number" class="form-control" name="account_no" value="{{ $companyaccount->account_no }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="account_swift">Swift Code</label>
						              	<input type="text" class="form-control" name="account_swift" value="{{ $companyaccount->account_swift }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="account_name">Name Rek</label>
						              	<input type="text" class="form-control" name="account_name" value="{{ $companyaccount->account_name }}" required>
						          	</div>
				  				</div>

			  					<div class="col-md-6">
			  						<div class="form-group">
						          		<label for="account_address">Address Rek</label>
						              	<input type="text" class="form-control" name="account_address" value="{{ $companyaccount->account_address }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="city">City</label>
						              	<select class="form-control select2" name="city" required>
						              		@foreach($citys as $city)
											    @if ($companyaccount->city == $city->id)
												    <option value="{{ $city->id }}" selected>{{ $city->name }} - {{ $city->fkCountry->name }}</option>
												@else
												    <option value="{{ $city->id }}">{{ $city->name }} - {{ $city->fkCountry->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="alias">Alias</label>
						              	<input type="text" class="form-control" name="alias" value="{{ $companyaccount->alias }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="flow">Flow</label> <br>
						          		@if($companyaccount->export == 1)
						          			<input type="checkbox" name="export" value="1" checked> Export <br>
						          		@else
						          			<input type="checkbox" name="export" value="1"> Export <br>
						          		@endif

						          		@if($companyaccount->local == 1)
						          			<input type="checkbox" name="local" value="1" checked> Local <br>
						          		@else
						          			<input type="checkbox" name="local" value="1"> Local <br>
						          		@endif

						          		@if($companyaccount->internal == 1)
						          			<input type="checkbox" name="internal" value="1" checked> Internal
						          		@else
						          			<input type="checkbox" name="internal" value="1"> Internal
						          		@endif
						          	</div>
						          	<button type="submit" class="btn btn-sm btn-success">Update</button>
					          		<a href="{{ url('/companyaccount/'.$companyaccount->company) }}" class="btn btn-sm btn-danger">Cancel</a>
		  						</div>
	  						</div>
  						</form>

	  					<table class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="20%">Bank</th>
						          	<th width="25%">Account</th>
						          	<th width="20%">Name</th>
						          	<th width="20%">Alias</th>
						          	<th width="15%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@foreach($companyaccounts as $companyaccount)
						    		<tr>
							            <td>
							            	{{$companyaccount->fkBank->name}} <br>
							            	@if($companyaccount->export == 1) Export <i class="glyphicon glyphicon-ok"></i> @else Export <i class="glyphicon glyphicon-remove"></i> @endif <br>
							            	@if($companyaccount->local == 1) Local <i class="glyphicon glyphicon-ok"></i> @else Local <i class="glyphicon glyphicon-remove"></i> @endif <br>
							            	@if($companyaccount->internal == 1) Internal <i class="glyphicon glyphicon-ok"></i> @else Internal <i class="glyphicon glyphicon-remove"></i> @endif
							            </td>
							            <td>
							            	{{$companyaccount->fkCurrency->code}} : {{$companyaccount->account_no}} <br>
							            	Swift : {{$companyaccount->account_swift}}
							            </td>
							            <td>
							            	{{$companyaccount->account_name}} <br>
							            	{{$companyaccount->account_address}} <br>
							            	{{$companyaccount->fkCity->name}} - {{$companyaccount->fkCity->fkCountry->name}}
							            </td>
							            <td>{{$companyaccount->alias}}</td>
										<td>
							            	@if($companyaccount->status == 1) <a href="{{ route('companyaccount.edit',$companyaccount->id)}}" class="btn btn-xs btn-warning">Edit</a>
							            	@else <font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
							            	@endif
							            </td>
							        </tr>
						        @endforeach
						    </tbody>
					  	</table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop