<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Purchasedetail extends Model
{
    protected $fillable = [
	    'purchaseorder',
	    'product',
	    'supplierdetail',
	    'warehouse',
	    'qty_order',
	    'qty_buffer',
	    'price',
	    'created_user',
	    'updated_user'
  	];

	public function fkPurchaseorder(){
	    return $this->belongsTo('\App\Http\Models\Purchaseorder', 'purchaseorder', 'id');
	}

  	public function fkProduct(){
	    return $this->belongsTo('\App\Http\Models\Product', 'product', 'id');
	}

	public function fkSupplierdetail(){
	    return $this->belongsTo('\App\Http\Models\Supplierdetail', 'supplierdetail', 'id');
	}

	public function fkWarehouse(){
	    return $this->belongsTo('\App\Http\Models\Warehouse', 'warehouse', 'id');
	}
}
