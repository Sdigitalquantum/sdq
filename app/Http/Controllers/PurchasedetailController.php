<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Purchasedetail;
use App\Http\Models\Purchaseorder;
use App\Http\Models\Product;
use App\Http\Models\Supplierdetail;
use App\Http\Models\Warehouse;
use App\Http\Models\Employeeset;
use App\Http\Models\Employeewarehouse;

class PurchasedetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $purchaseorder = Purchaseorder::find($id);
        $purchasedetails = Purchasedetail::where('purchaseorder', $id)->get();
        $products = Product::where('status', 1)->get();

        $cek = Purchasedetail::where('purchaseorder', $id)->first();
        if (!empty($cek)) {
            $supplierdetails = Supplierdetail::where('id', $cek->supplierdetail)->first();
            $supplier = 1;
        } else {
            $supplierdetails = Supplierdetail::where([['status', 1], ['id','<>',1]])->get();
            $supplier = 0;
        }

        $warehouses = Warehouse::where('status', 1)->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/purchasedetail.create', compact('purchaseorder','purchasedetails','products','supplierdetails','supplier','warehouses','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $cek = Purchasedetail::where([['purchaseorder', $request->get('purchaseorder')], ['product', $request->get('product')], ['supplierdetail', $request->get('supplierdetail')], ['warehouse', $request->get('warehouse')]])->first();
        
        if (!empty($cek)) {
            return redirect()->back()->withErrors('Detail Purchase Order already exist');
        } else {
            $purchasedetail = new Purchasedetail([
                'purchaseorder'     => $request->get('purchaseorder'),
                'product'           => $request->get('product'),
                'supplierdetail'    => $request->get('supplierdetail'),
                'warehouse'         => $request->get('warehouse'),
                'qty_order'         => 0,
                'qty_buffer'        => 0,
                'price'             => 0,
                'created_user'      => $session_id,
                'updated_user'      => $session_id
            ]);
            $purchasedetail->save();

            return redirect('/purchasedetailpr/'.$purchasedetail->id.'/edit/')->with('success', 'Detail Purchase Order has been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $purchasedetail = Purchasedetail::find($id);
        $purchasedetails = Purchasedetail::where('purchaseorder', $purchasedetail->purchaseorder)->get();
        $products = Product::where('status', 1)->get();
        $supplierdetails = Supplierdetail::where('id', $purchasedetail->supplierdetail)->first();
        $warehouses = Warehouse::where('status', 1)->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/purchasedetail.edit', compact('purchasedetail','purchasedetails','products','supplierdetails','warehouses','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $purchasedetail = Purchasedetail::find($id);

        if (empty($request->get('purchaseorder'))) {
            if ($request->get('qty_order') == 0) {
                $request->validate([
                    'qty_buffer' => 'numeric|min:0|not_in:0',
                    'price'      => 'numeric|min:0|not_in:0'
                ]);
            } else {
                $request->validate([
                    'qty_buffer' => 'numeric|min:0',
                    'price'      => 'numeric|min:0|not_in:0'
                ]);
            }

            $purchasedetail->qty_buffer        = $request->get('qty_buffer');
            $purchasedetail->price             = $request->get('price');
            $purchasedetail->updated_user      = $session_id;
            $purchasedetail->save();

            return redirect('/purchasedetail/create/'.$purchasedetail->purchaseorder)->with('success', 'Detail Purchase Order has been updated');
        } else {
            if ($request->get('purchaseorder') == $purchasedetail->purchaseorder && $request->get('product') == $purchasedetail->product && $request->get('supplierdetail') == $purchasedetail->supplierdetail && $request->get('warehouse') == $purchasedetail->warehouse) {
                $purchasedetail->product           = $request->get('product');
                $purchasedetail->supplierdetail    = $request->get('supplierdetail');
                $purchasedetail->warehouse         = $request->get('warehouse');
                $purchasedetail->updated_user      = $session_id;
                $purchasedetail->save();

                return redirect('/purchasedetailpr/'.$purchasedetail->id.'/edit/')->with('success', 'Detail Purchase Order has been updated');
            } else {
                $cek = Purchasedetail::where([['purchaseorder', $request->get('purchaseorder')], ['product', $request->get('product')], ['supplierdetail', $request->get('supplierdetail')], ['warehouse', $request->get('warehouse')]])->first();
                
                if (!empty($cek)) {
                    return redirect()->back()->withErrors('Detail Purchase Order already exist');
                } else {
                    $purchasedetail->product           = $request->get('product');
                    $purchasedetail->supplierdetail    = $request->get('supplierdetail');
                    $purchasedetail->warehouse         = $request->get('warehouse');
                    $purchasedetail->updated_user      = $session_id;
                    $purchasedetail->save();

                    return redirect('/purchasedetailpr/'.$purchasedetail->id.'/edit/')->with('success', 'Detail Purchase Order has been updated');
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $purchasedetail = Purchasedetail::find($id);
        $status = $purchasedetail->status;

        if ($status == 1) {
            $purchasedetail->status       = 0;
            $purchasedetail->updated_user = $session_id;
            $purchasedetail->save();

            return redirect('/purchasedetail/create/'.$purchasedetail->purchaseorder)->with('error', 'Detail Purchase Order has been deleted');
        } elseif ($status == 0) {
            $purchasedetail->status       = 1;
            $purchasedetail->updated_user = $session_id;
            $purchasedetail->save();

            return redirect('/purchasedetail/create/'.$purchasedetail->purchaseorder)->with('success', 'Detail Purchase Order has been activated');
        }
    }
}
