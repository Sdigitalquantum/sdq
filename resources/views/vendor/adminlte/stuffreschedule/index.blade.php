@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>Re-Schedule</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
				<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Stuffing &nbsp;<a href="{{ route('stuffreschedule.create') }}" class="btn btn-xs btn-primary" style="text-decoration: none;">Re-Schedule</a></h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="15%">Split Proforma</th>
						          	<th width="15%">Schedule</th>
						          	<th width="15%">Stuffing</th>
						          	<th width="15%">EMKL</th>
						          	<th width="30%">Note</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($stuffreschedules as $stuffreschedule)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>{{$stuffreschedule->fkSchedulestuff->fkSchedulebook->fkQuotationsplit->no_split}}</td>
							            <td>
							            	POL : {{$stuffreschedule->fkSchedulestuff->fkSchedulebook->pol}} <br>
							            	POD : {{$stuffreschedule->fkSchedulestuff->fkSchedulebook->pod}} <br>
							            	ETD : {{$stuffreschedule->fkSchedulestuff->fkSchedulebook->etd}} <br>
							            	ETA : {{$stuffreschedule->fkSchedulestuff->fkSchedulebook->eta}}
							            </td>
							            <td>
							            	Date : {{$stuffreschedule->date_old}} <br>
							            	PIC &nbsp;: {{$stuffreschedule->fkSchedulestuff->person}} <br>
							            	Qty Fcl : {{$stuffreschedule->fkSchedulestuff->qty_approve}} Container
							            </td>
							            <td>
							            	@if($stuffreschedule->fkSchedulestuff->status_approve == 1 || $stuffreschedule->fkSchedulestuff->status_reschedule == 1)
							            		Qty Fcl : {{$stuffreschedule->fkSchedulestuff->qty_approve}} Container
							            		<br>Freight : {{$stuffreschedule->fkSchedulestuff->freight}}
							            		<br>Vessel : {{$stuffreschedule->fkSchedulestuff->vessel}}
						            		@endif
							            </td>
							            <td>
							            	Re-Schedule : {{$stuffreschedule->date_change}} <br>
							            	sdq &nbsp;&nbsp;&nbsp;: {{$stuffreschedule->fkSchedulestuff->notice}} 

							            	@if($stuffreschedule->reason == 1)
							            		<br>EMKL : {{$stuffreschedule->fkSchedulestuff->reason}}
						            		@endif
							            </td>
										<td>
											@if($stuffreschedule->reason == 1)
					            				@if($stuffreschedule->fkSchedulestuff->status_approve == 0)
													<font color="blue">Waiting Approval</font>
								            	@elseif($stuffreschedule->fkSchedulestuff->status_approve == 1)
								            		<font color="green">Approved <i class="glyphicon glyphicon-ok"></i></font>
							            		@else
							            			<font color="red">Re-Route <i class="glyphicon glyphicon-remove"></i></font>
												@endif
				            				@else
				            					<font color="red">Canceled <i class="glyphicon glyphicon-remove"></i></font>
				            				@endif
					            		</td>
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Split Proforma</td>
						          	<td>Schedule</td>
						          	<td>Stuffing</td>
						          	<td>EMKL</td>
						          	<td>Note</td>
						          	<td>Action</td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop