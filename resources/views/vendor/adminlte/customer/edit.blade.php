@extends('adminlte::pagecontent')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
            <li class="disabled"><a>Account</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #6b6e70 !important; font-weight: bold; color: white;">
			    		<h4 class="box-title">Edit Customer
			    			&nbsp;<button id="detailBtn" class="btn btn-xs btn-warning" value="{{ route('customerdetail.show', $id) }}">Detail</button>
							&nbsp;<button id="accountBtn" class="btn btn-xs btn-warning" value="{{ route('customeraccount.show', $id) }}">Account</button>
			    		</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form id="customerFrm">
				      		@method('PATCH')
				      		@csrf
				      		<div class="row">
    							<div class="col-md-6">
    								<div class="form-group">
						              	<label for="code">Code</label>
						              	<input type="text" class="form-control" name="code" value="{{ $customer->code }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="account">Accounting</label>
						              	<select class="form-control select2" name="account" required>
						              		@foreach($accounts as $account)
											    @if ($customer->accounting == $account->id)
												    <option value="{{ $account->id }}" selected>{{ $account->code }} - {{ $account->name }}</option>
												@else
												    <option value="{{ $account->id }}">{{ $account->code }} - {{ $account->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="email">Email</label>
						              	<input type="text" class="form-control" name="email" value="{{ $customer->email }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="fax">Fax</label>
						              	<input type="number" class="form-control" name="fax" value="{{ $customer->fax }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="check">&nbsp;</label>
						          		@if($customer->tax == 1)
						          			<input type="checkbox" name="tax" value="1" checked> Tax &nbsp;&nbsp;&nbsp;
						          		@else
						          			<input type="checkbox" name="tax" value="1"> Tax &nbsp;&nbsp;&nbsp;
						          		@endif

						          		@if($customer->status_group == 1)
						          			<input type="checkbox" name="status_group" value="1" checked> Status Group &nbsp;&nbsp;&nbsp;
						          		@else
						          			<input type="checkbox" name="status_group" value="1"> Status Group &nbsp;&nbsp;&nbsp;
						          		@endif

						          		@if($customer->status_record == 1)
						          			<input type="checkbox" name="status_record" value="1" checked> Status Record
						          		@else
						          			<input type="checkbox" name="status_record" value="1"> Status Record
						          		@endif
						          	</div>
									<div class="form-group">
						          		<label for="npwp_no">No. NPWP</label>
						              	<input type="number" class="form-control" name="npwp_no" value="{{ $customer->npwp_no }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="npwp_name">Name NPWP</label>
						              	<input type="text" class="form-control" name="npwp_name" value="{{ $customer->npwp_name }}">
						          	</div>
						          	<div class="form-group">
						          		<label for="npwp_address">Address NPWP</label>
						              	<input type="text" class="form-control" name="npwp_address" value="{{ $customer->npwp_address }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="npwp_city">City NPWP</label>
						              	<select class="form-control select2" name="npwp_city">
						              		@foreach($citys as $city)
											    @if ($customer->city == $city->id)
												    <option value="{{ $city->id }}" selected>{{ $city->name }} - {{ $city->fkCountry->name }}</option>
												@else
												    <option value="{{ $city->id }}">{{ $city->name }} - {{ $city->fkCountry->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						          		<label for="limit">Limit</label>
						              	<input type="number" class="form-control" name="limit" value="{{ $customer->limit }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="note1">Note 1</label>
						          		<textarea class="form-control" name="note1" rows="4">{{ $customer->note1 }}</textarea>
						          	</div>
						          	<div class="form-group">
						          		<label for="note2">Note 2</label>
						          		<textarea class="form-control" name="note2" rows="4">{{ $customer->note2 }}</textarea>
						          	</div>
						          	<div class="form-group">
						          		<label for="note3">Note 3</label>
						          		<textarea class="form-control" name="note3" rows="4">{{ $customer->note3 }}</textarea>
						          	</div>
						          	<div class="form-group">
						          		<label for="notice">Notice</label>
						          		<textarea class="form-control" name="notice" rows="3">{{ $customer->notice }}</textarea>
						          	</div>
						          	<div class="form-group">
						          		<button id="updateBtn" value="{{ route('customer.update', $customer->id) }}" class="btn btn-sm btn-success">Update</button>
				          				<button id="cancelBtn" class="btn btn-sm btn-danger">Cancel</button>
						          	</div>
								</div>
							</div>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('content_js')
	<script>
    	$(document).ready(sdq.customer.prepare_edit());
    </script>
@stop