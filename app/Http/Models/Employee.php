<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
	    'group',
	    'branch',
	    'department',
	    'position',
	    'name',
	    'email',
	    'password',
	    'created_user',
	    'updated_user'
  	];

  	public function fkGroup(){
	    return $this->belongsTo('\App\Http\Models\Group', 'group', 'id');
	}

  	public function fkBranch(){
	    return $this->belongsTo('\App\Http\Models\Branch', 'branch', 'id');
	}

  	public function fkDepartment(){
	    return $this->belongsTo('\App\Http\Models\Department', 'department', 'id');
	}

  	public function fkPosition(){
	    return $this->belongsTo('\App\Http\Models\Position', 'position', 'id');
	}
}
