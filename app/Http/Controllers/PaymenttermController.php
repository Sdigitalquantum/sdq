<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Paymentterm;
use App\Http\Models\Employeeset;

class PaymenttermController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $paymentterms = Paymentterm::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/paymentterm.index', compact('paymentterms','disable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;
        $paymentterm = new Paymentterm();

        return view('vendor/adminlte/paymentterm.create', compact('paymentterm', 'disable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	$json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $request->validate([
                    'code' => 'unique:paymentterms'
                ]);

                $paymentterm = new Paymentterm([
                    'code'          => $json->code,
                    'name'          => $json->name,
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $result = $paymentterm->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $paymentterm = Paymentterm::find($id);

        return view('vendor/adminlte/paymentterm.edit', compact('paymentterm','disable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $json_data = json_decode(stripslashes($request->json));
        if (count($json_data) > 0)
        {   
            foreach ($json_data as $json) 
            {   //if ($json->created_date == ''){}else{};
                $paymentterm = Paymentterm::find($id);
                $code = $paymentterm->code;

                if ($code != $request->get('code')) {
                    $request->validate([
                        'code' => 'unique:paymentterms'
                    ]);
                }
                
                $paymentterm->code          = $json->code;
                $paymentterm->name          = $json->name;
                $paymentterm->updated_user  = $session_id;
                $result = $paymentterm->save();
            };
        };
        return $this->jsonSuccess( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $paymentterm = Paymentterm::find($id);
        $status = $paymentterm->status;

        if ($status == 1) {
        	$paymentterm->status       = 0;
            $paymentterm->updated_user = $session_id;
	        $result = $paymentterm->save();
        } elseif ($status == 0) {
        	$paymentterm->status       = 1;
            $paymentterm->updated_user = $session_id;
	        $result = $paymentterm->save();
        };
        return $this->jsonSuccess( $result );
    }
}
