@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Root Menu</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('menuroot.update', $menuroot->id) }}">
				      		@method('PATCH')
				      		@csrf
				          	<div class="row">
								<div class="col-md-6">
									<div class="form-group">
						              	<label for="nomor">Nomor</label>
						              	<input type="number" class="form-control" name="nomor" value="{{ $menuroot->nomor }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ $menuroot->name }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="url">URL</label>
						              	<input type="text" class="form-control" name="url" value="{{ $menuroot->url }}">
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						              	<label for="icon">Icon</label>
						              	<input type="text" class="form-control" name="icon" value="{{ $menuroot->icon }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="icon_color">Icon Color</label>
						              	<input type="text" class="form-control" name="icon_color" value="{{ $menuroot->icon_color }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="button">&nbsp;</label> <br>
						              	<button type="submit" class="btn btn-sm btn-success">Update</button>
						          		<a href="{{ url('/menu') }}" class="btn btn-sm btn-danger">Cancel</a>
						          	</div>
								</div>
							</div>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop